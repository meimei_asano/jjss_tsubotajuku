<?php
namespace Deployer;

require 'recipe/laravel.php';

// Project name
set('application', 'jjss');

// Configuration
set('allow_anonymous_stats', false);

set('ssh_type', 'native');
set('ssh_multiplexing', true);
set('writable_use_sudo', false);
set('repository', 'presso@presso.git.backlog.com:/TSUBOTA_JJSS/jjss.git');
set('writable_dirs', ['']);
add('shared_files', [
    '.env',
    'html/.htaccess',
    'html/php.ini',
    'html/.user.ini',
]);
add('shared_dirs', [
]);
set('http_user', 'jjssmeimei');

set('default_stage', 'staging');

// Hosts
host('cptest.prsvr.net')
    ->user('jjssmeimei')
    ->set('deploy_path', '/home/jjssmeimei/jjss')
    ->set('branch', 'staging')
    ->stage('staging');
;
host('blue.ssdsvr.net')
    ->user('trystage')
    ->set('deploy_path', '/home/trystage/jjss_test')
    ->set('branch', 'test')
    ->stage('test');
;

// php version
set('bin/php', '/usr/local/bin/ea-php73 -c /opt/cpanel/ea-php73/root/etc/php.ini -d allow_url_fopen=On');

// Tasks
task('artisan:storage:link', function () {
    run('ln -s {{release_path}}/storage/app/public {{release_path}}/html/storage');

    run('mv {{release_path}}/vendor/mpdf/mpdf/ttfonts {{release_path}}/vendor/mpdf/mpdf/ttfonts_orig');
    run('ln -s {{release_path}}/../../shared/ttfonts {{release_path}}/vendor/mpdf/mpdf/ttfonts');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.
before('deploy:symlink', 'artisan:migrate');

