/*!
 jquery.number-format.js
  extended from
    https://qiita.com/ShibuyaKosuke/items/8c8d881c446eff191499

 This software is released under the MIT License
 http://opensource.org/licenses/mit-license.php
 */
(function ($) {

    $.fn.numberformat = function (options) {

        // option
        var setting = $.extend({
            align: 'right',
        }, options);

        // remove comma
        var rmcomma = function (val) {
            return val.toString().replace(/,/g, '');
        };

        // format number
        var fmcomma = function (val) {
            var v = rmcomma(val);
            return v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
        };

        // make the string formatted on changed
        this.change(function () {
            var val = $(this).val();
            $(this).val(fmcomma($(this).val()))
                .attr('data-value', rmcomma(val))
                .css({
                    textAlign: setting.align
                });
        }).change();

        // Move caret to end of string on focused
        this.focus(function () {
            var v = $(this).val();
            $(this).val('').val(rmcomma(v));
        });

        // format number on blur
        this.blur(function () {
            var v = $(this).val();
            $(this).val(fmcomma(v));
        });

        // Remove comma on submit
        this.parents('form').submit(function () {
            $(this).find('[data-format="number"]').each(function () {
                $(this).val(rmcomma($(this).val()));
            });
        });

        return this;
    };
})(jQuery);
