$(function() {

    $('.save_pif').on('click', function() {
        $.ajax({
            url: "/api/classroom/pif/save",
            type: "PUT",
                data: {
                data: $('#form_pif').serialize(),
            },
            dataType: 'json',
                beforeSend: function () {
                $('.loading').fadeIn();
            },
        })
        .done((data, statusText, xhr) => {
                //入力内容がそのまま登録された
                if($('input[name="pif_id"]').val() == "") {
                    $('input[name="pif_id"]').val(data.id);
                }
                var options = {
                    text: "生徒情報を登録しました。",
                    icon: "success",
                    timer: 1000,
                    buttons: false,
                };
                swal(options);
            })
            .fail((data) => {
                var options = {
                    text: "処理中にエラーが発生しました。",
                    icon: "error",
                    timer: 2000,
                    buttons: false,
                };
                swal(options);
            })
            .always((data) => {
                $('.loading').fadeOut();
            });
    });

    $(document).on('click', '.add_memo', function() {
        var moto_val = $('select[name="pif_memo_kamoku_id"]').val();
        $('select[name="pif_memo_kamoku_id"]').val($(this).attr('data-kamoku_id'));
        if($(this).attr('data-kamoku_id') != moto_val) {
            set_histories();
        }
    });

    $(document).on('click', '.save_pif_memo', function() {
        var o = $(this);
        if($('.pif_memo_naiyou').val() == "") {
            var options = {
                text: "内容に入力がありません。",
                icon: "error",
                timer: 2000,
                buttons: false,
            };
            swal(options);
            return;
        }
        $.ajax({
            url: "/api/classroom/pif/save_memo",
            type: "PUT",
            data: {
                student_id: $('input[name="pif_memo_student_id"]').val(),
                pif_memo_id: $('input[name="pif_memo_id"]').val(),
                pif_memo_naiyou: $('.pif_memo_naiyou').val(),
                pif_memo_teacher_id: $('input[name="pif_memo_teacher_id"]').val(),
                pif_memo_kamoku_id: $('select[name="pif_memo_kamoku_id"]').val(),
            },
            dataType: 'json',
            beforeSend: function () {
                $('.loading').fadeIn();
            },
        })
        .done((data, statusText, xhr) => {
            var options = {
                text: "PIFメモを登録しました。",
                icon: "success",
                timer: 1000,
                buttons: false,
            };
            swal(options);
            clear_pif_memo_edit();
            set_histories();
        })
        .fail((data) => {
            var options = {
                text: "処理中にエラーが発生しました。",
                icon: "error",
                timer: 2000,
                buttons: false,
            };
            swal(options);
        })
        .always((data) => {
            $('.loading').fadeOut();
        });

    });

    $(document).on('change', 'select[name="pif_memo_kamoku_id"]', function() {
        set_histories();
    });

    function set_histories()
    {
        $.ajax({
            url: "/api/classroom/pif/list_memo",
            type: "PUT",
            data: {
                student_id: $('input[name="pif_memo_student_id"]').val(),
                pif_memo_kamoku_id: $('select[name="pif_memo_kamoku_id"]').val(),
            },
            dataType: 'json',
            beforeSend: function () {
                $('.loading').fadeIn();
            },
        })
        .done((data, statusText, xhr) => {

            var list_str = [];
            for(var i = 0; i < data.length; i++) {

                var memo_str = '';
                memo_str += '<tr>';
                memo_str += '<td>' + data[i]['memo_date'] + '</td>';
                memo_str += '<td>'
                 if(data[i]['kamoku'] != null) {
                    memo_str += data[i]['kamoku']['name'];
                }
                memo_str += '</td>';
                memo_str += '<td>' + data[i]['naiyou'] + '</td>';
                memo_str += '<td>' + data[i]['teacher']['name'] + '</td>';
                memo_str += '<td>';
                if(data[i]['teacher_id'] == $('input[name="pif_memo_login_teacher_id"]').val()) {
                    memo_str += '<i class="bx bx-edit pif_memo_henkou" pif_memo_id="' + data[i]['id'] + '"></i>';//編集
                    memo_str += '<i class="bx bx-trash pif_memo_delete" pif_memo_id="' + data[i]['id'] + '"></i>';//ごみ箱
                }
                memo_str += '</td>';
                memo_str += '</tr>';

                list_str.push(memo_str);
            }
            $('.pif_memo_list').html('');
            $('.pif_memo_list').html(list_str.join(""));
        })
        .fail((data) => {
            var options = {
                text: "処理中にエラーが発生しました。",
                icon: "error",
                timer: 2000,
                buttons: false,
            };
            swal(options);
        })
        .always((data) => {
            $('.loading').fadeOut();
        });
    }

    function clear_pif_memo_edit()
    {
        $('.pif_memo_naiyou').val('');
        $('input[name="pif_memo_id"]').val('');
    }

    $(document).on('click', '.pif_memo_henkou', function() {
        var o = $(this);
        var pif_memo_id = o.attr('pif_memo_id');
        if(pif_memo_id == "") {
            return;
        }
        $.ajax({
            url: "/api/classroom/pif/list_memo",
            type: "PUT",
            data: {
                pif_memo_id: pif_memo_id,
            },
            dataType: 'json',
            beforeSend: function () {
                $('.loading').fadeIn();
            },
        })
        .done((data, statusText, xhr) => {
            if(data[0]) {
                $('select[name="pif_memo_kamoku_id"]').val(data[0]['kamoku_id']);
                $('textarea[name="pif_memo_naiyou"]').val(data[0]['naiyou']);
                $('input[name="pif_memo_id"]').val(data[0]['id']);
                $('input[name="pif_teacher_id"').val(data[0]['teacher_id']);
            }
        })
        .fail((data) => {
            var options = {
                text: "処理中にエラーが発生しました。",
                icon: "error",
                timer: 2000,
                buttons: false,
            };
            swal(options);
        })
        .always((data) => {
            $('.loading').fadeOut();
        });
    });

    $(document).on('click', '.pif_memo_delete', function() {
        var o = $(this);
        var pif_memo_id = o.attr('pif_memo_id');
        if(pif_memo_id == "") {
            return;
        }

        swal({
            text: "削除します。よろしいですか？",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: "/api/classroom/pif/delete_memo",
                    type: "PUT",
                    async: false,
                    data: {
                        pif_memo_id: pif_memo_id,
                    },
                    dataType: 'json',
                    beforeSend: function () {
                        $('.loading').fadeIn();
                    },
                })
                .done((data, statusText, xhr) => {
                    set_histories();
                })
                .fail((data) => {
                    var options = {
                        text: "処理中にエラーが発生しました。",
                        icon: "error",
                        timer: 2000,
                        buttons: false,
                    };
                    swal(options);
                })
                .always((data) => {
                    $('.loading').fadeOut();
                });
            }
        });

    });

    //初期表示
    set_histories();

    $(document).on('click', '.more_pif_data, .hide_pif_data', function() {
        var nstatus = $('#form_pif').find('.table-pif').find('tr:not(.default_display)').css('display');
        $('#form_pif').find('.table-pif').find('tr:not(.default_display)').toggle('slow');
        if(nstatus == "none") {
            $('.more_pif_data').hide();
            $('.hide_pif_data').show();
        } else {
            $('.more_pif_data').show();
            $('.hide_pif_data').hide();
        }
    });
});