$(function(){
  $('.gnav--sub_menu:not(.current)').on({
    'mouseenter' : function(){
      $(this).addClass('is_active');
      $(this).find('.gnav--child').slideDown(100);
    },
    'mouseleave' : function(){
      $(this).removeClass('is_active');
      $(this).find('.gnav--child').slideUp(100);
    }
  });

  // ドロワーメニュー
  $('.menu-trigger_open').on('click',function(){
    $(this).addClass('active');
    $('.main_contents').addClass('open');
    $('aside').addClass('open');
    $('.overlay').addClass('open');
  });
  $('.menu-trigger_close').on('click',function(){
    $('.menu-trigger_open').removeClass('active');
    $('.main_contents').removeClass('open');
    $('aside').removeClass('open');
    $('.overlay').removeClass('open');
  });

  $('table #check_all').on('click', function() {
    var o = $(this);
    var check_status = o.prop('checked');
    o.closest("table").find('input[type="checkbox"]').prop('checked', check_status);
  });

  // input type="text"のうち、data-format="number"を持つものをカンマ区切りに
  $('[data-format="number"]').numberformat();
});
