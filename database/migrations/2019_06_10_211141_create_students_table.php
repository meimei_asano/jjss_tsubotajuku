<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('kana', 50);
            $table->integer('school_id');
            $table->string('gakkaou_name', 50);
            $table->string('grade_code', 50);
            $table->date('nyujyuku_date')->nullable();
            $table->date('taijyuku_date')->nullable();
            $table->date('sotsujuku_date')->nullable();
            $table->date('hikiotoshi_uketori_date')->nullable();
            $table->date('birth_date');
            $table->string('zip_code', 50);
            $table->string('pref', 50);
            $table->string('address1', 255);
            $table->string('address2', 255);
            $table->string('home_tel', 50);
            $table->string('student_tel', 50);
            $table->string('hogosya_name', 50);
            $table->string('hogosya_kana', 50);
            $table->string('hogosya_tel', 50);
            $table->string('hogosya_enquete_file', 255);
            $table->string('student_enquete_file', 255);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
