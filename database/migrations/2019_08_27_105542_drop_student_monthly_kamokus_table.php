<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropStudentMonthlyKamokusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('student_monthly_kamokus');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('student_monthly_kamokus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_monthly_id');//月間計画ID
            $table->integer('kamoku_id');//科目ID
            $table->integer('start_text_unit_id');//開始学習回ID
            $table->integer('planned_units');//計画数
            $table->integer('end_text_unit_id')->nullable();//終了学習回ID
            $table->integer('actual_units')->nullable();//達成数
            $table->decimal('actual_units_rate', 3, 2)->nullable();//達成率
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
