<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStudentAttendBasesTable20190628 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('student_attend_bases');

        Schema::create('student_attend_bases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');//生徒ID
            $table->date('start_date');//適用開始日
            $table->date('end_date')->nullable();//適用終了日
            $table->integer('course_id');//コースID
            $table->time('mon_start_time')->nullable();//月曜入室予定時間
            $table->time('mon_end_time')->nullable();//月曜退室予定時間
            $table->time('tue_start_time')->nullable();//火曜入室予定時間
            $table->time('tue_end_time')->nullable();//火曜退室予定時間
            $table->time('wed_start_time')->nullable();//水曜入室予定時間
            $table->time('wed_end_time')->nullable();//水曜退室予定時間
            $table->time('thu_start_time')->nullable();//木曜入室予定時間
            $table->time('thu_end_time')->nullable();//木曜退室予定時間
            $table->time('fri_start_time')->nullable();//金曜入室予定時間
            $table->time('fri_end_time')->nullable();//金曜退室予定時間
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_attend_bases', function (Blueprint $table) {
            //
        });
    }
}
