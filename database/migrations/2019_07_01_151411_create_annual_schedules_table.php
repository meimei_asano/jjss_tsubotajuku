<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnualSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annual_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id')->nullable();//校舎ID (nullの場合は全校舎へ適用、任意の数値の場合は対象校舎のみ適用)
            $table->date('schedule_date');//適用開始日
            $table->string('schedule_name', 50);//日程
            $table->string('date_type', 50);//種別(holiday:休日 / event:任意のイベント日かつ、授業は行う日)
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('annual_schedules');
    }
}
