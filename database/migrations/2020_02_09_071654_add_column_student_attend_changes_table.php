<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStudentAttendChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_attend_changes', function (Blueprint $table) {
            $table->integer('student_attend_change_tsuika_id')->nullable()->after('end_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_attend_changes', function (Blueprint $table) {
            $table->dropColumn('student_attend_change_tsuika_id');
        });
    }
}
