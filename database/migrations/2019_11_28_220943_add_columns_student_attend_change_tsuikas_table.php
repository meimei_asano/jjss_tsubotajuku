<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsStudentAttendChangeTsuikasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_attend_change_tsuikas', function (Blueprint $table) {
            $table->Integer('student_attend_change_id')->nullable()->after('hanei_flag');
            $table->tinyInteger('syounin_torikeshi_shinsei_flag')->default(0)->after('student_attend_change_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_attend_change_tsuikas', function (Blueprint $table) {
            $table->dropColumn('student_attend_change_id');
            $table->dropColumn('syounin_torikeshi_shinsei_flag');
        });
    }
}
