<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStudentMonthlyTexts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_monthly_texts', function (Blueprint $table) {
            $table->tinyinteger('reverse_test')->default('0')->after('text_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_monthly_texts', function (Blueprint $table) {
            $table->dropColumn('reverse_test');
        });
    }
}
