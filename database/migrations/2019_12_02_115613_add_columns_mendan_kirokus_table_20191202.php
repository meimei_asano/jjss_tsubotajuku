<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsMendanKirokusTable20191202 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mendan_kirokus', function (Blueprint $table) {
            $table->integer('teacher_id')->after('process_finished_flag');
            $table->integer('syounin_teacher_id')->nullable()->default(NULL)->after('teacher_id');
            $table->integer('created_student_id')->nullable()->default(NULL)->after('syounin_teacher_id');
            $table->tinyInteger('syounin_torikeshi_shinsei_flag')->default(0)->after('seikyu_finished_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mendan_kirokus', function (Blueprint $table) {
            $table->dropColumn('teacher_id');
            $table->dropColumn('syounin_teacher_id');
            $table->dropColumn('created_student_id');
            $table->dropColumn('syounin_torikeshi_shinsei_flag');
        });
    }
}
