<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMendanKirokusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mendan_kirokus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id');
            $table->date('mendan_date');
            $table->integer('mendan_teacher_id');
            $table->integer('manager_teacher_id');
            $table->string('name', 50);
            $table->string('kana', 50);
            $table->date('birth_date');
            $table->string('gender', 50);
            $table->string('gakkou_name', 50);
            $table->string('grade_code', 50);
            $table->string('zip_code', 50);
            $table->string('pref', 50);
            $table->string('address1', 50);
            $table->string('address2', 50);
            $table->string('address_kana', 50);
            $table->string('tel', 50);
            $table->string('email', 255);
            $table->string('hogosya_name', 50);
            $table->string('hogosya_kana', 50);
            $table->string('hogosya_tsudukigara', 50);
            $table->string('hogosya_home_tel', 50);
            $table->string('hogosya_fax', 50);
            $table->string('hogosya_tel', 50);
            $table->string('hogosya_email', 255);
            $table->dateTime('orientation_datetime')->nullable();
            $table->date('first_shidou_date')->nullable();
            $table->text('kamoku_ids')->comment('save kamoku_ids by comma separated');
            $table->integer('course_id');
            $table->time('mon_start_time')->nullable();
            $table->time('mon_end_time')->nullable();
            $table->time('tue_start_time')->nullable();
            $table->time('tue_end_time')->nullable();
            $table->time('wed_start_time')->nullable();
            $table->time('wed_end_time')->nullable();
            $table->time('thu_start_time')->nullable();
            $table->time('thu_end_time')->nullable();
            $table->time('fri_start_time')->nullable();
            $table->time('fri_end_time')->nullable();
            $table->date('unlimited_start_date')->nullable();
            $table->date('unlimited_end_date')->nullable();
            $table->boolean('unlimited_discount_flag')->nullable()->comment('boolean');
            $table->text('seikyu_data')->comment('save invoice data by json format');
            $table->string('furikomi_info', 255);
            $table->integer('process_finished_flag')->default(0);
            $table->integer('seikyu_finished_flag')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mendan_kirokus');
    }
}
