<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStudentRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_requests', function (Blueprint $table) {
            //クーリングオフフラグ （ 0: クーリングオフではない、1: クーリングオフである）
            $table->tinyInteger('cooling_off_flag')->default(0)->after('reason');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_requests', function (Blueprint $table) {
            $table->dropColumn('cooling_off_flag');
        });
    }
}
