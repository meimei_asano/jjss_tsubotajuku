<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentMonthlyTextUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_monthly_text_units', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('learning_flag')->default('0');//0: 学習する, 1: 学習しない
            $table->integer('student_monthly_id');
            $table->integer('student_monthly_text_id');
            $table->integer('kamoku_id');
            $table->integer('text_id');
            $table->integer('text_unit_num');
            $table->string('text_unit_name', 255)->default('');
            $table->timestamps();
            //$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_monthly_text_units');
    }
}
