<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnStudentRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_requests', function (Blueprint $table) {
            $table->dropColumn('reason');
        });
        Schema::table('student_requests', function (Blueprint $table) {
            $table->text('reason')->default('')->after('kyujyuku_end_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_requests', function (Blueprint $table) {
            $table->dropColumn('reason');
        });
        Schema::table('student_requests', function (Blueprint $table) {
            $table->string('reason', 50)->after('kyujyuku_end_date');
        });
    }
}
