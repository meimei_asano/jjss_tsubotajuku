<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetNullableColumnsMendanKirokusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mendan_kirokus', function (Blueprint $table) {
            $table->date('mendan_date')->nullable()->change();
            $table->date('birth_date')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mendan_kirokus', function (Blueprint $table) {
            $table->date('mendan_date')->nullable(false)->change();
            $table->date('birth_date')->nullable(false)->change();
        });
    }
}
