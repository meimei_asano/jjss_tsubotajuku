<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->string('bank_code', 10)->default('')->after('student_enquete_file');//銀行コード
            $table->string('bank_name', 255)->default('')->after('bank_code');//銀行名
            $table->string('branch_code', 10)->default('')->after('bank_name');//支店コード
            $table->string('branch_name', 255)->default('')->after('branch_code');//支店名
            $table->tinyInteger('deposit_type')->nullable()->after('branch_name');//預金種目
            $table->string('account_number', 10)->default('')->after('deposit_type');//通帳番号
            $table->string('account_holder', 255)->default('')->after('account_number');//名義人名
            $table->string('customer_number', 10)->default('')->after('account_holder');//顧客番号
            $table->string('itaku_number', 10)->default('')->after('customer_number');//委託者番号
            $table->date('hikiotoshi_start_date')->nullable()->after('itaku_number');//引落開始日
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropColumn('bank_code');
            $table->dropColumn('bank_name');
            $table->dropColumn('branch_code');
            $table->dropColumn('branch_name');
            $table->dropColumn('deposit_type');
            $table->dropColumn('account_number');
            $table->dropColumn('account_holder');
            $table->dropColumn('customer_number');
            $table->dropColumn('itaku_number');
            $table->dropColumn('hikiotoshi_start_date');
        });
    }
}
