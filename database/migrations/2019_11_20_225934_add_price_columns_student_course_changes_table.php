<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceColumnsStudentCourseChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_course_changes', function (Blueprint $table) {
            $table->integer('current_price')->after('current_course_id');
            $table->integer('price')->after('course_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_course_changes', function (Blueprint $table) {
            $table->dropColumn('current_price');
            $table->dropColumn('price');
        });
    }
}
