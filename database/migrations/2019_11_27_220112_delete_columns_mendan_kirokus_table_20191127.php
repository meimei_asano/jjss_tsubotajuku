<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteColumnsMendanKirokusTable20191127 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mendan_kirokus', function (Blueprint $table) {
            $table->dropColumn('address_kana');
            $table->dropColumn('furikomi_info');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mendan_kirokus', function (Blueprint $table) {
            $table->string('address_kana', 50)->after('address2');
            $table->string('furikomi_info', 255)->after('seikyu_total');
        });
    }
}
