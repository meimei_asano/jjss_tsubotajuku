<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentMonthliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_monthlies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');//生徒ID
            $table->integer('term_id');//期間ID
            $table->time('planned_hours')->nullable();//通塾予定時間
            $table->time('attend_hours')->nullable();//出席数
            $table->decimal('attend_hours_rate', 3, 2)->nullable();//出席率
            $table->text('furikaeri_teacher');//今月の振り返り（担当から）
            $table->text('furikaeri_student');//今月の振り返り（生徒から）
            $table->text('mokuhyou_teacher');//来月の学習目標（担当から）
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_monthlies');
    }
}
