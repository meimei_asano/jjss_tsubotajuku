<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schools', function (Blueprint $table) {
            $table->string('school_code',50)->default('')->after('id');
            $table->string('zip_code', 50)->default('')->after('name');
            $table->string('address', 255)->default('')->after('pref');
            $table->string('building', 255)->defaulg('')->after('address');
            $table->string('tel', 50)->default('')->after('building');
            $table->string('fax', 50)->default('')->after('tel');
            $table->text('furikomisaki')->nullable()->after('price_table');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schools', function (Blueprint $table) {
            $table->dropColumn('school_code');
            $table->dropColumn('zip_code');
            $table->dropColumn('address');
            $table->dropColumn('building');
            $table->dropColumn('tel');
            $table->dropColumn('fax');
            $table->dropColumn('furikomisaki');
        });
    }
}
