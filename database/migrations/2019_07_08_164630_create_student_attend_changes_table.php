<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentAttendChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_attend_changes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');//生徒ID
            $table->string('change_type', 50);//追加コマ'tsuka', 振替'furikae'
            $table->date('before_date')->nullable();//変更前の日付(振替'furikae'の場合)
            $table->date('attend_date');//変更後の日付
            $table->time('start_time')->nullable();//入室予定時間
            $table->time('end_time')->nullable();//退室予定時間
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_attend_changes');
    }
}
