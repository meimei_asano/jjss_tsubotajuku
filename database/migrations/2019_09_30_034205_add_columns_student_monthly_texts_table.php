<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsStudentMonthlyTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_monthly_texts', function (Blueprint $table) {
            $table->tinyinteger('maikai_kaisuu')->nullable()->after('keikaku_suu');

            $table->tinyinteger('maikai_kaisuu_1')->nullable()->after('maikai_kaisuu');
            $table->tinyinteger('maikai_kaisuu_2')->nullable()->after('maikai_kaisuu_1');
            $table->tinyinteger('maikai_kaisuu_3')->nullable()->after('maikai_kaisuu_2');
            $table->tinyinteger('maikai_kaisuu_4')->nullable()->after('maikai_kaisuu_3');
            $table->tinyinteger('maikai_kaisuu_5')->nullable()->after('maikai_kaisuu_4');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_monthly_texts', function (Blueprint $table) {
            $table->dropColumn('maikai_kaisuu');
            $table->dropColumn('maikai_kaisuu_1');
            $table->dropColumn('maikai_kaisuu_2');
            $table->dropColumn('maikai_kaisuu_3');
            $table->dropColumn('maikai_kaisuu_4');
            $table->dropColumn('maikai_kaisuu_5');
        });
    }
}
