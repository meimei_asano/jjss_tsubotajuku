<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPriceTableOverrideColumnStudentAttendBasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_attend_bases', function (Blueprint $table) {
            $table->string('price_table_override', 50)->nullable()->after('course_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_attend_bases', function (Blueprint $table) {
            $table->dropColumn('price_table_override');
        });
    }
}
