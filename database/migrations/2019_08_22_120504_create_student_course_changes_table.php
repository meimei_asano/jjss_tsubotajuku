<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentCourseChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_course_changes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->date('change_yotei_date');
            $table->integer('course_id');
            $table->time('mon_start_time')->nullable()->default(NULL);
            $table->time('mon_end_time')->nullable()->default(NULL);
            $table->time('tue_start_time')->nullable()->default(NULL);
            $table->time('tue_end_time')->nullable()->default(NULL);
            $table->time('wed_start_time')->nullable()->default(NULL);
            $table->time('wed_end_time')->nullable()->default(NULL);
            $table->time('thu_start_time')->nullable()->default(NULL);
            $table->time('thu_end_time')->nullable()->default(NULL);
            $table->time('fri_start_time')->nullable()->default(NULL);
            $table->time('fri_end_time')->nullable()->default(NULL);
            $table->integer('teacher_id');
            $table->integer('syounin_teacher_id')->nullable()->default(NULL);
            $table->boolean('hanei_flag')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_course_changes');
    }
}
