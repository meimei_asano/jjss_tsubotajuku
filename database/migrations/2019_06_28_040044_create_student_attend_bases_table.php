<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentAttendBasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_attend_bases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');//生徒ID
            $table->date('start_date');//適用開始日
            $table->date('end_date')->nullable();//適用終了日
            $table->integer('course_id');//コースID
            $table->date('mon_start_time')->nullable();//月曜入室予定時間
            $table->date('mon_end_time')->nullable();//月曜退室予定時間
            $table->date('tue_start_time')->nullable();//火曜入室予定時間
            $table->date('tue_end_time')->nullable();//火曜退室予定時間
            $table->date('wed_start_time')->nullable();//水曜入室予定時間
            $table->date('wed_end_time')->nullable();//水曜退室予定時間
            $table->date('thu_start_time')->nullable();//木曜入室予定時間
            $table->date('thu_end_time')->nullable();//木曜退室予定時間
            $table->date('fri_start_time')->nullable();//金曜入室予定時間
            $table->date('fri_end_time')->nullable();//金曜退室予定時間
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_attend_bases');
    }
}
