<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('text_units', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('text_id');
            $table->string('unit_num', 50);
            $table->string('name', 50);
            $table->string('has_reverse_test', 50);
            $table->string('test_type', 50);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('text_units');
    }
}
