<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStudentKyuujyukus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_kyuujyukus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->date('start_date');
            $table->date('end_date')->nullable()->default(NULL);;
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_kyuujyukus');
    }
}
