<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsStudentShibousTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_shibous', function (Blueprint $table) {
            $table->text('kamokus')->nullable()->after('shibou_gakubu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_shibous', function (Blueprint $table) {
            $table->dropColumn('kamokus');
        });
    }
}
