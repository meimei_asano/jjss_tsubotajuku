<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterInvoiceTablesForTaxSystemChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->decimal('subtotal', 10, 0)->nullable()->change();
            $table->decimal('tax_rate', 3, 2)->nullable()->change();
            $table->decimal('tax', 10, 0)->nullable()->change();
        });
        Schema::table('invoice_details', function (Blueprint $table) {
            $table->decimal('tax_rate', 3, 2)->nullable()->after('price');
            $table->decimal('tax', 10, 0)->nullable()->after('tax_rate');
            $table->decimal('included', 10, 0)->nullable()->after('tax');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_details', function (Blueprint $table) {
            $table->dropColumn('tax_rate');
            $table->dropColumn('tax');
            $table->dropColumn('included');
        });
    }
}
