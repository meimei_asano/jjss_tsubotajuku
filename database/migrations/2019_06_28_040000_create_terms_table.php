<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('term_name', 50)->default('');//期間名
            $table->string('display_name', 50)->default('');//表示名
            $table->string('term_type', 50)->default('');//期間種別 (通常/特別講習)
            $table->date('start_date');//期間開始日
            $table->date('end_date');//期間終了日
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terms');
    }
}
