<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');//生徒ID
            $table->date('note_date');//日付
            $table->boolean('checked_flag');//承認フラグ
            $table->time('plan_attend_start_time')->nullable();//入室予定時間
            $table->time('plan_attend_end_time')->nullable();//退室予定時間
            $table->time('actual_attend_start_time')->nullable();//入室実績時間
            $table->time('actual_attend_end_time')->nullable();//退室実績時間
            $table->text('remarks');//備考
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_notes');
    }
}
