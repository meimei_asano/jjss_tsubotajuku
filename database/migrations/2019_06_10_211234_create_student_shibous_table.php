<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentShibousTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_shibous', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->string('grade_code', 50);
            $table->string('shibou_school', 50);
            $table->string('shibou_gakubu', 50);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_shibous');
    }
}
