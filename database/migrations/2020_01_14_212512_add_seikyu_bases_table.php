<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeikyuBasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seikyu_bases', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedTinyInteger('seikyu_type')->comment('請求種別');
            $table->integer('student_id')->comment('生徒ID');
            $table->date('sales_date')->comment('売上日');
            $table->unsignedInteger('referrer_id')->comment('参照元ID');
            $table->string('title')->comment('タイトル');
            $table->decimal('subtotal', 10, 0)->comment('税抜金額');
            $table->decimal('tax_rate', 3, 2)->comment('税率');
            $table->decimal('tax', 10, 0)->comment('税額');
            $table->decimal('total', 10, 0)->comment('税込金額');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seikyu_bases');
    }
}
