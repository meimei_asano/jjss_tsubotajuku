<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextUnitsByStudents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('text_units_by_students', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('text_id');  // テキストID
            $table->integer('student_id');  // 生徒ID
            $table->integer('units');  // 学習全回数
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('text_units_by_students');
    }
}
