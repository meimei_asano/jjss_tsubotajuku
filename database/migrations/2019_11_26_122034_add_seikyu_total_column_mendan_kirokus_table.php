<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeikyuTotalColumnMendanKirokusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mendan_kirokus', function (Blueprint $table) {
            $table->text('seikyu_total')->after('seikyu_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mendan_kirokus', function (Blueprint $table) {
            $table->dropColumn('seikyu_total');
        });
    }
}
