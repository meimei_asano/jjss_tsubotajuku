<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentPifsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_pifs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->text('kazoku_kousei');
            $table->text('katei_jijyou');
            $table->text('yume');
            $table->text('syumi');
            $table->text('bukatsu');
            $table->text('gakkou_tokki');
            $table->text('hometai');
            $table->text('iwanai');
            $table->text('akaten_shinkyu');
            $table->text('teiki_test');
            $table->text('hairyo_jikou');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_pifs');
    }
}
