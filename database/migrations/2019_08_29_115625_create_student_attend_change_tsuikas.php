<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentAttendChangeTsuikas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_attend_change_tsuikas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->date('request_date');
            $table->text('tsuika_request');
            $table->integer('tsuika_hours');
            $table->decimal('unit_price', 10, 0);
            $table->integer('teacher_id');
            $table->integer('syounin_teacher_id')->nullable()->default(NULL);
            $table->boolean('hanei_flag')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_attend_change_tsuikas');
    }
}
