<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsTextsTable20190711 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('texts', function (Blueprint $table) {
            $table->dropColumn('sequence');
            $table->integer('next_text_id')->nullable()->after('price');  // 次のテキストID
            $table->boolean('has_reverse_test')->after('next_text_id');  // 逆テスト有無
            $table->boolean('units_by_student')->after('has_reverse_test');  // 生徒別テキスト別学習回数設定
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('texts', function (Blueprint $table) {
            $table->dropColumn('next_text_id');
            $table->dropColumn('has_reverse_test');
            $table->dropColumn('units_by_student');
            $table->integer('sequence')->after('price');
        });
    }
}
