<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentPifMemosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_pif_memos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->date('memo_date')->nullable();
            $table->integer('kamoku_id')->nullable();
            $table->integer('teacher_id')->nullable();
            $table->text('naiyou')->nullable();
            $table->tinyInteger('hogosya_dentatsu_flag')->default(0);//0:伝えない、1:伝えたい！
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_pif_memos');
    }
}
