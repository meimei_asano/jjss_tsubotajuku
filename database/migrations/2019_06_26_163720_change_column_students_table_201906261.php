<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnStudentsTable201906261 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {

            $table->dropColumn('gakkaou_name');
            $table->dropColumn('sotsujuku_date');

            $table->string('gakkou_name', 50)->after('school_id');
            $table->date('sotsujyuku_date')->nullable()->after('taijyuku_date');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {

            $table->dropColumn('gakkou_name');
            $table->dropColumn('sotsujyuku_date');

            $table->string('gakkaou_name', 50)->after('school_id');
            $table->date('sotsujuku_date')->nullable()->after('taijyuku_date');
        });
    }
}
