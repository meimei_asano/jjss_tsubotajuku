<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsStudentPifMemosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_pif_memos', function (Blueprint $table) {
            $table->dropColumn('hogosya_dentatsu_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_pif_memos', function (Blueprint $table) {
            $table->tinyInteger('hogosya_dentatsu_flag')->default(0)->after('naiyou');//0:伝えない、1:伝えたい！
        });
    }
}
