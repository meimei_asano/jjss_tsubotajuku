<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStudentCourseChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_course_changes', function (Blueprint $table) {
            //反映先
            $table->integer('student_attend_base_id')->nullable()->after('hanei_flag');
            //反映したことによって終了日を登録したレコード
            $table->integer('current_student_attend_base_id')->nullable()->after('student_attend_base_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_course_changes', function (Blueprint $table) {
            $table->dropColumn('student_attend_base_id');
            $table->dropColumn('current_student_attend_base_id');
        });
    }
}
