<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id');  // 校舎ID
            $table->integer('student_id');  // 生徒ID
            $table->date('invoice_date');  // 請求日
            $table->date('payment_date')->nullable();  // 支払期日
            $table->date('hikiotoshi_yotei_date')->nullable();  // 引落予定日
            $table->boolean('hikiotoshi_flag');  // 引落フラグ
            $table->decimal('subtotal', 10, 0);  // 小計金額
            $table->decimal('tax_rate', 3, 2);  // 消費税率
            $table->decimal('tax', 10, 0);  // 消費税額
            $table->decimal('total', 10, 0);  // 請求合計
            $table->boolean('checked_teacher_id');  // 請求承認先生ID
            $table->boolean('printed_flag');  // 請求書発行済フラグ
            $table->boolean('confirmed_flag');  // 入金確認済フラグ
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('invoice_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice_id');  // 請求ID
            $table->integer('row_num');  // 行番号
            $table->string('item', 50);  // 内訳
            $table->decimal('price', 10, 0);  // 金額
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
        Schema::dropIfExists('invoice_details');
    }
}
