<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsTextUnitsTable20190711 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('text_units', function (Blueprint $table) {
            $table->dropColumn('has_reverse_test');  // text_unitsからtextsに移す

            $sql = '
                alter table text_units
                    modify unit_num integer(11) not null
            ';
            DB::connection()->getPdo()->exec($sql);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('text_units', function (Blueprint $table) {
            $table->string('has_reverse_test', 50)->after('name');

            $sql = '
                alter table text_units
                    modify unit_num varchar(50) not null default ""
            ';
            DB::connection()->getPdo()->exec($sql);
        });
    }
}
