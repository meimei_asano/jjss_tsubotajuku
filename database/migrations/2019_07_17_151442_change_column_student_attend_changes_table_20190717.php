<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnStudentAttendChangesTable20190717 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_attend_changes', function (Blueprint $table) {
            $sql = '
                alter table student_attend_changes
                    modify attend_date date default null
            ';
            DB::connection()->getPdo()->exec($sql);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_attend_changes', function (Blueprint $table) {
            $sql = '
                alter table student_attend_changes
                    modify attend_date date not null
            ';
            DB::connection()->getPdo()->exec($sql);
        });
    }
}
