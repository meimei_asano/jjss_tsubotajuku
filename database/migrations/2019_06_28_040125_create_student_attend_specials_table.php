<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentAttendSpecialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_attend_specials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');//生徒ID
            $table->date('attend_date');//日付
            $table->time('start_time')->nullable();//入室予定時間
            $table->time('end_time')->nullable();//退室予定時間
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_attend_specials');
    }
}
