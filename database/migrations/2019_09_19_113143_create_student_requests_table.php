<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->date('request_date');
            $table->string('request_type', 10);
            $table->date('target_date');
            $table->date('kyujyuku_end_date')->nullable();
            $table->string('reason', 50);
            $table->integer('teacher_id');
            $table->integer('syounin_teacher_id')->nullable()->default(NULL);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_requests');
    }
}
