<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentAttendSpecialTsuikas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_attend_special_tsuikas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->date('request_date');
            $table->integer('term_id');
            $table->integer('total_hours');
            $table->integer('base_hours');
            $table->decimal('unit_price', 10, 0);
            $table->decimal('subtotal', 10, 0);
            $table->integer('teacher_id');
            $table->integer('syounin_teacher_id')->nullable()->default(NULL);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_attend_special_tsuikas');
    }
}
