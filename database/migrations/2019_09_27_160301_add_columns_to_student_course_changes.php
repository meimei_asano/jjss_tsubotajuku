<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToStudentCourseChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_course_changes', function (Blueprint $table) {
            $table->date('request_date')->default(date('Y-m-d'))->after('student_id');
            $table->integer('current_course_id')->default(0)->after('change_yotei_date');
        });
        Schema::table('student_course_changes', function (Blueprint $table) {
            $table->date('request_date')->default(NULL)->change();
            $table->integer('current_course_id')->default(NULL)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_course_changes', function (Blueprint $table) {
            $table->dropColumn(['request_date', 'current_course_id']);
        });
    }
}
