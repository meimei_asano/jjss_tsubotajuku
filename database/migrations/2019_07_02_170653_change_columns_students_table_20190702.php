<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsStudentsTable20190702 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $sql = '
                alter table students
                    modify kana varchar(50) not null default "",
                    modify birth_date date default null,
                    modify gakkou_name varchar(50) not null default "",
                    modify grade_code varchar(50) not null default "",
                    modify zip_code varchar(50) not null default "",
                    modify pref varchar(50) not null default "",
                    modify address1 varchar(255) not null default "",
                    modify address2 varchar(255) not null default "",
                    modify home_tel varchar(50) not null default "",
                    modify student_tel varchar(50) not null default "",
                    modify hogosya_name varchar(50) not null default "",
                    modify hogosya_kana varchar(50) not null default "",
                    modify hogosya_tel varchar(50) not null default "",
                    modify hogosya_enquete_file varchar(255) not null default "",
                    modify student_enquete_file varchar(255) not null default ""
            ';
            DB::connection()->getPdo()->exec($sql);

            $sql = '
                alter table students
                    add hogosya_email varchar(255) not null default "" after hogosya_tel,
                    add teacher_id int(11) default null after school_id
            ';
            DB::connection()->getPdo()->exec($sql);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $sql = '
                alter table students
                    modify kana varchar(50) not null,
                    modify birth_date date,
                    modify gakkou_name varchar(50) not null,
                    modify grade_code varchar(50) not null,
                    modify zip_code varchar(50) not null,
                    modify pref varchar(50) not null,
                    modify address1 varchar(255) not null,
                    modify address2 varchar(255) not null,
                    modify home_tel varchar(50) not null,
                    modify student_tel varchar(50) not null,
                    modify hogosya_name varchar(50) not null,
                    modify hogosya_kana varchar(50) not null,
                    modify hogosya_tel varchar(50) not null,
                    modify hogosya_enquete_file varchar(255) not null,
                    modify student_enquete_file varchar(255) not null
            ';
            DB::connection()->getPdo()->exec($sql);

            $sql = '
                alter table students
                    drop hogosya_email,
                    drop teacher_id
            ';
            DB::connection()->getPdo()->exec($sql);
        });
    }
}
