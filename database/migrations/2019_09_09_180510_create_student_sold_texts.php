<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentSoldTexts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_sold_texts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->date('request_date');
            $table->text('text_request');
            $table->decimal('subtotal', 10, 0);
            $table->decimal('tax_rate', 3, 2)->nullable();
            $table->decimal('tax', 10, 0);
            $table->decimal('total', 10, 0);
            $table->integer('teacher_id');
            $table->integer('syounin_teacher_id')->nullable()->default(NULL);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_sold_texts');
    }
}
