<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsStudentNoteRowsTable20190711 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_note_rows', function (Blueprint $table) {
            $table->boolean('reverse_test')->after('additional_text_name');  // 逆テストかどうか
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_note_rows', function (Blueprint $table) {
            $table->dropColumn('reverse_test');
        });
    }
}
