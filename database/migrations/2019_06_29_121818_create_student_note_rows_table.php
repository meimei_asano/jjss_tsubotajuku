<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentNoteRowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_note_rows', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_note_id');//子別ノートID
            $table->integer('text_id');//テキストID
            $table->string('additional_text_name', 50)->nullable();//任意テキスト名
            $table->integer('text_unit_id');//学習回ID
            $table->string('display_text_unit', 50);//学習回表示名
            $table->time('actual_start_time')->nullable();//開始時間
            $table->time('actual_end_time')->nullable();//終了時間
            $table->string('score', 50)->nullable();//得点
            $table->boolean('result_flag')->nullable();//合否フラグ
            $table->integer('teacher_id')->nullable();//指導先生ID
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_note_rows');
    }
}
