<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsStudentRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_requests', function (Blueprint $table) {
            $table->tinyInteger('syounin_torikeshi_shinsei_flag')->default(0)->after('syounin_teacher_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_requests', function (Blueprint $table) {
            $table->dropColumn('syounin_torikeshi_shinsei_flag');
        });
    }
}
