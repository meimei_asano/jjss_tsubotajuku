<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStudentCourseChangesTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_course_changes', function (Blueprint $table) {
            $table->date('unlimited_updated_end_date')->nullable()->after('current_student_attend_base_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_course_changes', function (Blueprint $table) {
            $table->dropColumn('unlimited_updated_end_date');
        });
    }
}
