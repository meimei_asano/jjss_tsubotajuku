<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentNinetypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_ninetypes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->string('ninetype_codes', 50);
            $table->integer('score_type1');
            $table->integer('score_type2');
            $table->integer('score_type3');
            $table->integer('score_type4');
            $table->integer('score_type5');
            $table->integer('score_type6');
            $table->integer('score_type7');
            $table->integer('score_type8');
            $table->integer('score_type9');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_ninetypes');
    }
}
