<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentMonthlyTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_monthly_texts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_monthly_id');
            $table->integer('kamoku_id');
            $table->integer('kamoku_sequence');
            $table->integer('text_id');
            $table->string('text_name', 255)->default('');
            $table->integer('text_all_kaisuu')->nullable();
            $table->integer('keikaku_suu')->nullable();
            $table->integer('actual_units')->nullable();
            $table->decimal('actual_units_rate',3, 2)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_monthly_texts');
    }
}
