<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnsStudentNinetypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('student_ninetypes', function (Blueprint $table) {
            $sql = '
                alter table student_ninetypes
                    modify score_type1 int(11) default null,
                    modify score_type2 int(11) default null,
                    modify score_type3 int(11) default null,
                    modify score_type4 int(11) default null,
                    modify score_type5 int(11) default null,
                    modify score_type6 int(11) default null,
                    modify score_type7 int(11) default null,
                    modify score_type8 int(11) default null,
                    modify score_type9 int(11) default null
            ';
            DB::connection()->getPdo()->exec($sql);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('student_ninetypes', function (Blueprint $table) {
            $sql = '
                alter table student_ninetypes
                    modify score_type1 int(11) not null,
                    modify score_type2 int(11) not null,
                    modify score_type3 int(11) not null,
                    modify score_type4 int(11) not null,
                    modify score_type5 int(11) not null,
                    modify score_type6 int(11) not null,
                    modify score_type7 int(11) not null,
                    modify score_type8 int(11) not null,
                    modify score_type9 int(11) not null
            ';
            DB::connection()->getPdo()->exec($sql);
        });
    }
}
