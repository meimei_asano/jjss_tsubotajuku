FROM php:7.3-apache-stretch
ENV DEBIAN_FRONTEND noninteractive
RUN docker-php-source extract
RUN apt-get update && apt-get install -y git mysql-client
RUN apt-get update
RUN apt-get install -y build-essential wget curl git git-core

RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install mysqli
RUN apt-get update && apt-get install -y \
        libzip-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
    && docker-php-ext-install -j$(nproc) iconv \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-install zip
RUN docker-php-ext-install bcmath
RUN a2enmod rewrite
