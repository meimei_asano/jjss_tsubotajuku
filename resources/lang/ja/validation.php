<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attributeを承認してください。',
    'active_url'           => ':attributeには有効なURLを指定してください。',
    'after'                => ':attributeには:date以降の日付を指定してください。',
    'after_or_equal'       => ':attributeには:dateかそれ以降の日付を指定してください。',
    'alpha'                => ':attributeには英字のみからなる文字列を指定してください。',
    'alpha_dash'           => ':attributeには英数字・ハイフン・アンダースコアのみからなる文字列を指定してください。',
    'alpha_num'            => ':attributeには英数字のみからなる文字列を指定してください。',
    'array'                => ':attributeには配列を指定してください。',
    'before'               => ':attributeには:date以前の日付を指定してください。',
    'before_or_equal'      => ':attributeには:dateかそれ以前の日付を指定してください。',
    'between'              => [
        'numeric' => ':attributeには:min〜:maxまでの数値を指定してください。',
        'file'    => ':attributeには:min〜:max KBのファイルを指定してください。',
        'string'  => ':attributeには:min〜:max文字の文字列を指定してください。',
        'array'   => ':attributeには:min〜:max個の要素を持つ配列を指定してください。',
    ],
    'boolean'              => ':attributeには真偽値を指定してください。',
    'confirmed'            => ':attributeが確認用の値と一致しません。',
    'date'                 => ':attributeには正しい形式の日付を指定してください。',
    'date_format'          => ':attributeは:formatの形式で時刻を指定してください。',
    'different'            => ':attributeには:otherとは異なる値を指定してください。',
    'digits'               => ':attributeには:digits桁の数値を指定してください。',
    'digits_between'       => ':attributeには:min〜:max桁の数値を指定してください。',
    'dimensions'           => ':attributeの画像サイズが不正です。',
    'distinct'             => '指定された:attributeは既に存在しています。',
    'email'                => ':attributeには正しい形式のメールアドレスを指定してください。',
    'exists'               => '指定された:attributeは存在しません。',
    'file'                 => ':attributeにはファイルを指定してください。',
    'filled'               => ':attributeには空でない値を指定してください。',
    'image'                => ':attributeには画像ファイルを指定してください。',
    'in'                   => ':attributeには:valuesのうちいずれかの値を指定してください。',
    'in_array'             => ':attributeが:otherに含まれていません。',
    'integer'              => ':attributeには整数を指定してください。',
    'ip'                   => ':attributeには正しい形式のIPアドレスを指定してください。',
    'ipv4'                 => ':attributeには正しい形式のIPv4アドレスを指定してください。',
    'ipv6'                 => ':attributeには正しい形式のIPv6アドレスを指定してください。',
    'json'                 => ':attributeには正しい形式のJSON文字列を指定してください。',
    'max'                  => [
        'numeric' => ':attributeには:max以下の数値を指定してください。',
        'file'    => ':attributeには:max KB以下のファイルを指定してください。',
        'string'  => ':attributeには:max文字以下の文字列を指定してください。',
        'array'   => ':attributeには:max個以下の要素を持つ配列を指定してください。',
    ],
    'mimes'                => ':attributeには:valuesのうちいずれかの形式のファイルを指定してください。',
    'mimetypes'            => ':attributeには:valuesのうちいずれかの形式のファイルを指定してください。',
    'min'                  => [
        'numeric' => ':attributeには:min以上の数値を指定してください。',
        'file'    => ':attributeには:min KB以上のファイルを指定してください。',
        'string'  => ':attributeには:min文字以上の文字列を指定してください。',
        'array'   => ':attributeには:min個以上の要素を持つ配列を指定してください。',
    ],
    'not_in'               => ':attributeには:valuesのうちいずれとも異なる値を指定してください。',
    'numeric'              => ':attributeには数値を指定してください。',
    'present'              => ':attributeには現在時刻を指定してください。',
    'regex'                => '正しい形式の:attributeを指定してください。',
    'required'             => ':attributeは必須です。',
    'required_if'          => ':otherが:valueの時:attributeは必須です。',
    'required_unless'      => ':otherが:values以外の時:attributeは必須です。',
    'required_with'        => ':valuesのうちいずれかが指定された時:attributeは必須です。',
    'required_with_all'    => ':valuesのうちすべてが指定された時:attributeは必須です。',
    'required_without'     => ':valuesのうちいずれかが指定されなかった時:attributeは必須です。',
    'required_without_all' => ':valuesのうちすべてが指定されなかった時:attributeは必須です。',
    'same'                 => ':attributeが:otherと一致しません。',
    'size'                 => [
        'numeric' => ':attributeには:sizeを指定してください。',
        'file'    => ':attributeには:size KBのファイルを指定してください。',
        'string'  => ':attributeには:size文字の文字列を指定してください。',
        'array'   => ':attributeには:size個の要素を持つ配列を指定してください。',
    ],
    'string'               => ':attributeには文字列を指定してください。',
    'timezone'             => ':attributeには正しい形式のタイムゾーンを指定してください。',
    'unique'               => 'その:attributeはすでに使われています。',
    'uploaded'             => ':attributeのアップロードに失敗しました。',
    'url'                  => ':attributeには正しい形式のURLを指定してください。',
    'old_password'         => ':attributeが一致しません。',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'request_date' => [
            1 => '申請済みの申込日を変更することはできません。',
        ],
        'tsuika_request' => [
            'duplicate' => '追加コマ情報の追加授業日が重複しています。',
            'date_nendo' => '年度をまたぐ日付登録があります。同一年度で分けて登録してください。',
            1 => ':valueの時間数が1時間単位になるように入室／退室予定時間を設定してください。',
            ':valueの入室／退室予定時間がすでにある来塾予定と競合しています。',
            3 => ':valueは休塾日です。',
            4 => ':valueの期間登録がないため、通常時期なのか特別講習時期なのか判定できません。',//termに登録がない
            5 => '時間帯指定が不正です。',
            6 => '契約コース情報がありません。',
            7 => '無制限コースの契約の生徒です。'
        ],
        'change_yotei_date' => [
            'error_one' => '変更の予定日は、少なくとも申請日の翌月でなければなりません。',
            'error_two' => '変更予定日が翌月の場合、申請日は月の16日より前でなければなりません。',
        ],
        'school_schedule' => '学校のスケジュールには、開始時刻と終了時刻の両方が必要です。終了時刻は開始時刻よりも後でなければなりません。',
        'compare_school_schedule_to_course' => '通塾予定の合計時間は、選択したコースで利用可能な合計時間と等しくなければなりません。',
        'holiday' => ':attributeで指定の日付は休塾日です。',
        /*'seikyu_data' => [
            'size' => '',
        ],*/
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        //common?
        'start_date' => '開始日',
        'end_date' => '終了日',
        'display_name' => '表示名',
        'grade_code' => '学年',
        'name' => '名前',
        'kana' => 'カナ',
        'email' => 'メールアドレス',
        'password' => 'パスワード',
        //terms
        'term_name' => '講習期間名',
        'term_type' => '講習種別',
        //student_attend_bases
        'mon_start_time' => '月曜開始時間',
        'mon_end_time' => '月曜終了時間',
        'tue_start_time' => '火曜開始時間',
        'tue_end_time' => '火曜終了時間',
        'wed_start_time' => '水曜開始時間',
        'wed_end_time' => '水曜終了時間',
        'thu_start_time' => '木曜開始時間',
        'thu_end_time' => '木曜終了時間',
        'fri_start_time' => '金曜開始時間',
        'fri_end_time' => '金曜終了時間',
        //student_attend_specials
        'attend_date' => '通塾日',
        //student_ninetypes
        'ninetype_codes' => '9タイプ',
        //student_shibous
        'shibou_school' => '志望校',
        'shibou_gakubu' => '志望学部',
        //students
        'gakkou_name' => '学校名',
        'nyujyuku_date' => '入塾日',
        'taijyuku_date' => '退塾日',
        'sotsujyuku_date' => '卒塾日',
        'hikiotoshi_uketori_date' => '引落申込み用紙受取日',
        'birth_date' => '生年月日',
        'zip_code' => '郵便番号',
        'pref' => '都道府県',
        'address1' => '市区町村・番地',
        'address2' => 'マンション名・部屋番号',
        'home_tel' => '自宅電話番号',
        'student_tel' => '生徒電話番号',
        'hogosya_name' => '保護者名',
        'hogosya_kana' => '保護者名カナ',
        'hogosya_tel' => '保護者電話番号',
        'hogosya_email' => '保護者メールアドレス',
      //  'hogosya_enquete_file' => '',
      //  'student_enquete_file' => '',
        //student_shingakus
        'shingakusaki' => '進学先',
        //teachers
        'nyusya_date' => '入社日',
        'tantou_school_id' => '担当校舎',
        'koyou_keitai' => '雇用形態',
        'permission' => '権限',
        //texts
        'price' => '価格',
        //text_units
      //  'unit_num' => '',
      //  'has_reverse_test' => '',
      //  'test_type' => '',
        'price_table' => '金額テーブル',
        'old_password' => '現在のパスワード',
        'seats' => '座席数',
        'student_code' => '生徒コード',
        'school_code' => '校舎コード',
        'invoice_date' => '請求日',
        'payment_date' => '支払期日',
        'hikiotoshi_yotei_date' => '引落予定日',
        'hikiotoshi_flag' => '引落フラグ',
        'subtotal' => '請求小計',
        'tax_rate' => '消費税率',
        'tax' => '消費税額',
        'total' => '請求合計',
        'row_num' => '内訳行番号',
        'item' => '内訳',
        'price' => '金額',
        'school_id' => '入塾校舎',
        'mendan_date' => '面談日',
        'mendan_teacher_id' => '面談者',
        'manager_teacher_id' => '校長',
        'gender' => '性別',
        'tel' => '携帯TEL',
        'hogosya_tsudukigara' => '続柄',
        'hogosya_home_tel' => '自宅TEL',
        'hogosya_fax' => '自宅FAX',
        'orientation_date' => '初回OT実施日付',
        'orientation_time' => '初回OT実施時刻',
        'first_shidou_date' => '初回授業日',
        'kamoku_ids' => '学習科目',
        'kamoku_ids.*' => '学習科目',
        'current_course_id' => '現在のコース',
        'course_id' => '通塾コース',
        'unlimited_start_date' => '無制限開始日',
        'unlimited_end_date' => '無制限終了日',
        'unlimited_discount_flag' => '無制限割引有無',
        'seikyu_data' => '請求内訳',
        'seikyu_breakdown' => '請求内訳',
        'seikyu_amount' => '請求金額',
        'seikyu_amount.*' => '請求金額',
        'student_id' => '生徒',
        'change_yotei_date' => '変更予定日',
        'request_date' => '申込日',
        'tsuika_request' => '追加コマ情報',
        'tsuika_request.*.add_date' => '追加授業日',
        'tsuika_request.*.start_time' => '入室予定時間',
        'tsuika_request.*.end_time' => '退室予定時間',
        'tsuika_request.*.hours' => '時間数',
        'tsuika_request.*.kyouka' => '教科',
        'tsuika_hours' => '追加時間数',
        'unit_price' => '単価',
        'text_request' => '受け渡し教材情報',
        'text_request.*.text' => '教材名',
        'text_request.*.price' => '金額',
        'term_id' => '特別講習期間',
        'total_hours' => '総合計申し込み時間数',
        'base_hours' => '期間中通常授業時間数',
        'request_type' => '願い種別',
        'target_date' => '適用日付',
        'kyujyuku_end_date' => '休塾最終日付',
        'reason' => '理由',
    ],

];
