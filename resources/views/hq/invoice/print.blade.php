<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    table-layout: fixed;
    font-size: 11pt;
}
th, td {
    padding: 3px 5px;
}
table.main {
    width: 800px;
}
.bordered {
    border: 1px solid black;
}
.col-1 {
    width: 20px;
}
.height-sm {
    height: 0.8em;
    padding: 1px auto;
}
.height-mn {
    height: 2em;
}
.text-center {
    text-align: center;
}
.text-right {
    text-align: right;
}
.font-size-14pt {
    font-size: 14pt;
}
.font-size-16pt {
    font-size: 16pt;
}
.font-size-18pt {
    font-size: 18pt;
}
.bcolor-silver {
    background-color: silver;
}
.invoice-header {
    font-size: 24pt;
    text-decoration: underline;
    font-weight: bold;
}
.text-bold {
    font-weight: bold;
}
.text-underlined {
    text-decoration: underline;
}
.valign-btm {
    vertical-align: bottom;
}
</style>
<table class="main">
    <tr>
        <td colspan="23"></td>
        <td colspan="9" rowspan="2" class="text-right invoice-header">ご請求書</td>
        <td colspan="4" rowspan="2" class="font-size-18pt valign-btm text-bold">{{ ($is_hikae) ? '(控)' : '　' }}</td>
    </tr>
    <tr>
        <td colspan="4" class="height-sm"></td>
        <td colspan="12" class="height-sm">〒{{ $invoice->student->zip_code }}</td>
        <td colspan="7" class="height-sm"></td>
        {{-- rowspan 9 --}}
        {{-- rowspan 4 --}}
    </tr>
    <tr>
        <td colspan="4" class="height-sm"></td>
        <td colspan="12" class="height-sm">{{ $invoice->student->pref }}{{ $invoice->student->address1 }}</td>
        <td colspan="6" class="height-sm"></td>
        <td colspan="14" class="height-sm text-right">{{ \JJSS::printDate($invoice->invoice_date) }}</td>
    </tr>
    <tr>
        <td colspan="4" class="height-sm"></td>
        <td colspan="12" class="height-sm">{{ $invoice->student->address2 }}</td>
        <td colspan="20" class="height-sm"></td>
    </tr>
    <tr>
        <td colspan="5" class="height-sm"></td>
        <td colspan="11" class="font-size-16pt height-sm">{{ $invoice->student->name }}　様</td>
        <td colspan="20" class="height-sm"></td>
    </tr>
    <tr>
        <td colspan="5" class="height-sm"></td>
        <td colspan="11" class="font-size-16pt height-sm">保護者様</td>
        <td colspan="7" class="height-sm"></td>
        <td colspan="13" class="font-size-18pt height-sm">坪田塾　{{ $invoice->school->name }}</td>
    </tr>
    <tr>
        <td colspan="24" class="height-sm"></td>
        <td colspan="12" class="height-sm">{{ $invoice->school->pref }}{{ $invoice->school->address }}</td>
    </tr>
    <tr>
        <td colspan="24" class="height-sm"></td>
        <td colspan="12" class="height-sm">{{ $invoice->school->building }}</td>
    </tr>
    <tr>
        <td colspan="24" class="height-sm"></td>
        <td colspan="12" class="height-sm"></td>
    </tr>
    <tr>
        <td colspan="24" class="height-sm"></td>
        <td colspan="12" class="height-sm"></td>
    </tr>
    <tr>
        <td colspan="36">平素は格別のご高配に賜り、誠にありがとうございます。</td>
    </tr>
    <tr>
        <td colspan="36">お月謝につきまして、下記のとおりご請求申し上げます。</td>
    </tr>
    <tr>
        <td colspan="36" class="font-size-14pt">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="36">下記のご請求金額を当塾指定の口座までお振込お願い申し上げます。</td>
    </tr>
    <tr>
        <td colspan="36" class="font-size-14pt">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="8" class="bordered bcolor-silver font-size-16pt text-center">ご請求金額合計</td>
        <td colspan="10" class="bordered font-size-16pt text-center">{{ number_format($invoice->total) }}円</td>
        <td colspan="18"></td>
    </tr>
    <tr>
        <td colspan="36" class="font-size-14pt">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="36">お支払い期日：　　<span class="font-size-16pt text-underlined">{{ \JJSS::printDate($invoice->payment_date, true) }}</span></td>
    </tr>
    <tr>
        <td colspan="36" class="font-size-14pt">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="16" class="bordered text-center height-mn">内訳</td>
        <td colspan="5" class="bordered text-center height-mn">税抜</td>
        <td colspan="5" class="bordered text-center height-mn">税率</td>
        <td colspan="5" class="bordered text-center height-mn">消費税額</td>
        <td colspan="5" class="bordered text-center height-mn">税込</td>
    </tr>
    @foreach ($invoice->invoice_details as $detail)
    <tr>
        <td colspan="16" class="bordered height-mn">{{ $detail->item }}</td>
        <td colspan="5" class="bordered text-right height-mn">{{ number_format($detail->price) }}円</td>
        <td colspan="5" class="bordered text-right height-mn">{{ ($detail->tax_rate * 100) }}％</td>
        <td colspan="5" class="bordered text-right height-mn">{{ number_format($detail->tax) }}円</td>
        <td colspan="5" class="bordered text-right height-mn">{{ number_format($detail->included) }}円</td>
    </tr>
    @endforeach
    @php
        $row = (isset($detail)) ? $detail->row_num : 0;
    @endphp
    @if ($row <= 5)
        @for ($i = $row; $i < 5 ; $i++)
    <tr>
        <td colspan="16" class="bordered height-mn"></td>
        <td colspan="5" class="bordered text-right height-mn"></td>
        <td colspan="5" class="bordered text-right height-mn"></td>
        <td colspan="5" class="bordered text-right height-mn"></td>
        <td colspan="5" class="bordered text-right height-mn"></td>
    </tr>
        @endfor
    @endif
    <tr>
        <td colspan="26"></td>
        <td colspan="5" class="bordered text-center bcolor-silver height-mn">合計</td>
        <td colspan="5" class="bordered text-right height-mn">{{ number_format($invoice->total) }}円</td>
    </tr>
    <tr>
        <td colspan="36" class="font-size-14pt">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="36">※恐れ入りますが、振込手数料はご負担頂きますよう、お願い申し上げます。</td>
    </tr>
    <tr>
        <td colspan="36">※すでにお振込済みの場合は、大変申し訳ございません。その場合は、本請求書を破棄して頂きますよう</td>
    </tr>
    <tr>
        <td colspan="36">　お願いいたします。</td>
    </tr>
    <tr>
        <td colspan="36" class="font-size-14pt">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="6" class="bordered text-center">お振込み口座</td>
        <td colspan="15" class="bordered">
            {!! nl2br($invoice->school->furikomisaki) !!}
        </td>
        <td colspan="15"></td>
    </tr>
    <tr>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>

        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>

        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>

        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
        <td class="col-1"></td>
    </tr>

</table>
