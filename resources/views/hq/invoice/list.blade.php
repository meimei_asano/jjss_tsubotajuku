@extends('layouts.default')
@section('content')
<style>
  .set_orderby {cursor:pointer;}
</style>
<section class="scroll_table-wrap wht_bloc">
    <h2 class="heading-1">請求一覧</h2>
    <div class="flex flex-j-between flex-a-ctr mgn-btm16">
        <form id="search_form" action="{{ url('hq/invoice') }}" method="get" style="width: 80%">
            <div class="form-group flex" style="flex-flow: wrap;">
                <input type="text" id="student_name" name="student_name" placeholder="生徒" class="input-sm" value="{{ $search['student_name'] ?? '' }}"/>
                &nbsp;
                <select id="school_id" name="school_id" >
                  <option value="">校舎</option>
                  {!! \JJSS::options($schools, 'id', 'name', ($search['school_id']) ?? '') !!}
                </select>
                &nbsp;
                <input type="text" id="invoice_date_start" name="invoice_date_start" autocomplete="off" placeholder="請求日" class="datepicker-here input-sm" data-language="jp" data-auto-close="true" value="{{ $search['invoice_date_start'] ?? '' }}"/>
                &nbsp;〜&nbsp;
                <input type="text" id="invoice_date_end" name="invoice_date_end" autocomplete="off" placeholder="請求日" class="datepicker-here input-sm" data-language="jp" data-auto-close="true" value="{{ $search['invoice_date_end'] ?? '' }}"/>
                <hr style="width: 100%; height: 0; border: 0; margin: 5px 0; padding: 0;">
                <input type="text" id="payment_date_start" name="payment_date_start" autocomplete="off" placeholder="支払日" class="datepicker-here input-sm" data-language="jp" data-auto-close="true" value="{{ $search['payment_date_start'] ?? '' }}"/>
                &nbsp;〜&nbsp;
                <input type="text" id="payment_date_end" name="payment_date_end" autocomplete="off" placeholder="支払日" class="datepicker-here input-sm" data-language="jp" data-auto-close="true" value="{{ $search['payment_date_end'] ?? '' }}"/>
                {{--
                &nbsp;
                <ul>
                    <li>
                        <input type="checkbox" id="checked_teacher_id" name="checked_teacher_id" value="1" {{ (isset($search['checked_teacher_id']) && $search['checked_teacher_id'] == '1') ? 'checked' : '' }}>
                        <label for="checked_teacher_id">承認済</label>
                    </li>
                </ul>
                --}}
                &nbsp;
                <ul>
                    <li>
                        <input type="checkbox" id="printed_flag" name="printed_flag" value="1" {{ (isset($search['printed_flag']) && $search['printed_flag'] == '1') ? 'checked' : '' }}>
                        <label for="printed_flag">発行済</label>
                    </li>
                </ul>
                &nbsp;
                {{--
                <ul>
                    <li>
                        <input type="checkbox" id="confirmed_flag" name="confirmed_flag" value="1" {{ (isset($search['confirmed_flag']) && $search['confirmed_flag'] == '1') ? 'checked' : '' }}>
                        <label for="confirmed_flag">入金済</label>
                    </li>
                </ul>
                &nbsp;
                --}}
                <button id="btn-search">検索<i class='bx bx-search'></i></button>
                <input type="hidden" name="orderby_column_name" value="{{ ($orderby['column_name']) ?? '' }}">
                <input type="hidden" name="orderby_type" value="{{ ($orderby['type']) ?? '' }}">
            </div>
        </form>
        <div class="form-group flex">
            <button id="modal-open"><i class='bx bx-import'></i>請求データ取込</button>
        </div>
    </div>

    <div class="scroll_table mgn-btm4">
        <table class="table-horizon table-striped datatable">
            <thead>
            <tr>
                <th class="th_check">
                    <input type="checkbox" class="check" id="check_all" value="all">
                    <label for="check_all"></label>
                </th>
                <th class="set_orderby" dt-column="school_id">校舎名</th>
                <th class="set_orderby" dt-column="students.kana">生徒名</th>
                <th class="set_orderby" dt-column="invoice_date">請求日</th>
                <th class="set_orderby" dt-column="payment_date">支払期日</th>
                <th class="set_orderby" dt-column="total">請求額</th>
                {{--
                <th>承認済</th>
                --}}
                <th>発行済</th>
                {{--
                <th>入金済</th>
                --}}
                <th></th>
                <th style="width:300px;"></th>
            </tr>
            </thead>
            <tbody>
            @if(count($invoices) == 0)
                <tr>
                    <td></td>
                    <td colspan="8">
                        表示条件に合致する請求情報が登録されていません。
                    </td>
                </tr>
            @else
                @foreach ($invoices as $invoice)
                    <tr style="">
                        <td class="td_check">
                            <input type="checkbox" class="check" data-invoice_id="{{ $invoice->id }}">
                            <label></label>
                        </td>
                        <td class="">{{ $invoice->school->name or null }}</td>
                        <td class="">{{ $invoice->student->name or null }}</td>
                        <td class="td_time">{{ \JJSS::displayDate($invoice->invoice_date) }}</td>
                        <td class="td_time">{{ \JJSS::displayDate($invoice->payment_date) }}</td>
                        <td class="txt-rgt">{{ number_format($invoice->total) }}円</td>
                        {{--
                        <td class="">{{ ($invoice->checked_teacher_id) ? '済' : '' }}</td>
                        --}}
                        <td class="">{{ ($invoice->printed_flag) ? '済' : '' }}</td>
                        {{--
                        <td class="">{{ ($invoice->confirmed_flag) ? '済' : '' }}</td>
                        --}}
                        <td><button onclick="window.open('{{ url("hq/invoice/print/{$invoice->id}") }}', '_blank')"><i class='bx bxs-printer'></i>印刷</button></td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
    <hr>
    <div class="form-group">
        チェックしたものを
        {{-- <button id="check"><i class='bx bx-show'></i>承認</button> --}}
        <button id="print"><i class='bx bxs-printer'></i>印刷</button>
        {{-- <button id="confirm"><i class='bx bx-check-circle'></i>入金済</button> --}}
    </div>
    <div class="flex flex-j-between">
        <div class="btn_list">
        </div>
        <div class="pager txt-ctr">
            @if(count($invoices))
            {{ $invoices->appends([
                'student_name' => (isset($search['student_name'])===true ? $search['student_name'] : ''),
                'school_id' => (isset($search['school_id'])===true ? $search['school_id'] : ''),
                'invoice_date_start' => (isset($search['invoice_date_start'])===true ? $search['invoice_date_start'] : ''),
                'invoice_date_end' => (isset($search['invoice_date_end'])===true ? $search['invoice_date_end'] : ''),
                'payment_date_start' => (isset($search['payment_date_start'])===true ? $search['payment_date_start'] : ''),
                'payment_date_end' => (isset($search['payment_date_end'])===true ? $search['payment_date_end'] : ''),
                'printed_flag' => (isset($search['printed_flag'])===true ? $search['printed_flag'] : ''),
                'orderby_column_name' => (isset($orderby['column_name'])===true ? $orderby['column_name'] : ''),
                'orderby_type' => (isset($orderby['type'])===true ? $orderby['type'] : '')
                ])->links() }}
            @endif
        </div>
    </div>
</section>
<form id="form" method="post" action="{{ url('hq/invoice/print') }}" target="_blank">
    {{ csrf_field() }}
    <input type="hidden" id="invoice_ids" name="invoice_ids">
</form>
@endsection
@section('modal')
    <link href="{{ asset('css/modal.css') }}" rel="stylesheet" defer>
    <style>
        #modal {
            height: 260px;
            top: calc(50% - 130px);
        }
        .drag-drop-inside {
            margin: 5px;
            padding: 5px;
            border: 3px dotted #ffc646;
            background-color: #ffe6a6;
        }
    </style>
    <div id="background" style="display: none;">
        <div id="modal">
            <form id="upload_form" method="post" enctype="multipart/form-data" action="{{ url("hq/invoice/import") }}">
                {{ csrf_field() }}
                <div class="flex flex-j-between flex-a-ctr mgn-btm8">
                    <div></div>
                    <div>
                        <span><i id="modal-close" class="bx bx-x"></i></span>
                    </div>
                </div>
                <div id="modal-panel">
                    <div class="form-group">
                        アップロードするファイルを指定してください：
                    </div>
                    <div id="drag-drop-area" class="mgn-btm16">
                        <div class="drag-drop-inside txt-ctr">
                            ここにドラッグ＆ドロップ<br>
                            または<br>
                            <input type="file" id="file" name="file">
                        </div>
                    </div>
                    <div id="import_submit_div" class="txt-ctr">
                        <button>取込</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
<script>
$(function() {
    $('#btn-search').on('click', function(){
        $('#search_form').submit();
    });

    $('.set_orderby').on('click', function() {
        var current_orderby = $('input[name="orderby_type"]').val();
        var current_column = $('input[name="orderby_column_name"]').val();
        var new_column = $(this).attr('dt-column');

        if (current_column == new_column && current_orderby == "asc") {
            $('input[name="orderby_type"]').val('desc');
        } else {
            $('input[name="orderby_column_name"]').val(new_column);
            $('input[name="orderby_type"]').val('asc');
        }

        $('#search_form').submit();
    });

    $('#modal-open').on('click', function() {
        $('#background').fadeIn();
    });
    $('#modal-close').on('click', function (){
        $('#background').hide();
    });
    $('#import_submit_div button').on('click', function(event) {
        event.preventDefault();
        event.stopPropagation();

        if (fileInput.files.length == 0) {
            var options = {
                text: "ファイルを指定してください。",
                icon: "error",
                timer: 2000,
                buttons: false,
            };
            swal(options);
        } else {
            $('#upload_form').submit();
        }
    });

    // ドラッグ＆ドロップでアップロード
    var fileInput = document.getElementById('file');

    $('#drag-drop-area').on('dragover', function(event) {
        event.preventDefault();
        event.stopPropagation();
        $('#drag-drop-area').addClass('dragover');
    });

    $('#drag-drop-area').on('dragleave', function(event) {
        event.preventDefault();
        event.stopPropagation();
        $('#drag-drop-area').removeClass('dragover');
    });
    $('#drag-drop-area').on('drop', function(_e) {
        var event = _e;
        if( _e.originalEvent ){
            event = _e.originalEvent;
        }
        event.preventDefault();
        event.stopPropagation();
        $('#drag-drop-area').removeClass('dragenter');

        var files = event.dataTransfer.files;
        fileInput.files = files;
    });

    // 操作
    $('#print').on('click', function() {
        var selected_id = [];
        $('.check').each(function() {
            if($(this).prop('checked')) {
                if($(this).attr('data-invoice_id') !== undefined) {
                    selected_id.push($(this).attr('data-invoice_id'));
                }
            }
        });

        if(selected_id.length == 0) {
            var options = {
                text: "対象の請求情報にチェックしてください。",
                icon: "info",
                timer: 2000,
                buttons: false,
            }
            swal(options);
            return;
        }

        $('#invoice_ids').val(selected_id.join());
        $('#form').submit();
    });
});
</script>
@endsection
