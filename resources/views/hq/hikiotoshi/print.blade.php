<style>
.main {
    width: 800px;
    font-size: 15px;
}
.title {
    font-size: 30px;
    font-weight: bold;
    text-decoration: underline;
    text-align: center;
    margin-top: 70px;
}
p {
    margin: 0;
}
p.big {
    font-size: 20px;
    font-weight: bold;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
    table-layout: fixed;
    font-size: 15px;
}
th, td {
    padding: 3px 5px;
}
table.main {
    width: 700px;
}
.bordered {
    border: 1px solid black;
}
table tfoot th {
    background-color: #d8d8d8;
}
.text-right {
    text-align: right;
}

</style>
<div class="main">
    <div class="text-right">
        {{ \Carbon\Carbon::today()->format('Y年m月d日') }}
    </div>
    <div class="title">
        お引き落とし金額変更のお知らせ{{ ($is_hikae) ? '　(控)' : '' }}
    </div>
    <div style="margin-top: 50px;">
        <div style="float:left;width:70%;">
            <p>〒000-0000{{-- $invoice->student->postal_code --}}</p>
            <p>東京都××区〇〇町〇ー×－△{{-- $invoice->student->pref --}}{{-- $invoice->student->address1 --}}</p>
            <p>△△△△△△△△{{-- $invoice->student->address2 --}}</p>
            <p class="big">〇〇〇〇〇〇〇〇{{-- $invoice->student->name --}}　様</p>
            <p class="big">保護者様</p>
        </div>
        <div style="float:right;width:30%;">
            <p>　</p>
            <p>　</p>
            <p class="big">坪田塾　××校{{-- $invoice->school->name --}}</p>
            <p>東京都××区〇〇町{{-- $invoice->school->pref --}}{{-- $invoice->school->address --}}</p>
            <p>××ビル〇階{{-- $invoice->school->building --}}</p>
            <p>TEL：</p>
            <p>FAX：</p>
        </div>
    </div>
    <div style="margin-top: 50px;">
        <p>　平素は格別のご高配に賜り、誠にありがとうございます。</p>
        <p>今月は通常の月謝以外のお引落しがございますのでご連絡いたします。</p>
        <table class="main">
            <thead>
                <tr>
                    <th style="width:50%;">　</th>
                    <th style="width:20%;">　</th>
                    <th style="width:30%;">　</th>
                </tr>
                <tr>
                    <th class="bordered" colspan="2">内訳</th>
                    <th class="bordered">金額</th>
                </tr>
            </thead>
@php
    $l = [];
    $l['通常'][] = ['title' => '〇〇コース　×月分', 'price' => 'XXXXX'];
    $l['追加'][] = ['title' => 'コース変更に伴う差額精算分', 'price' => 'XXXXX'];
    $l['追加'][] = ['title' => '教材費', 'price' => 'XXXXX'];
@endphp
            <tbody>
                @if(isset($l['通常']) === true)
                    <tr>
                        <td class="bordered" colspan="2">通常月のお引落し</td>
                        <td class="bordered">　</td>
                    </tr>
                    @foreach($l['通常'] as $list)
                        <tr>
                            <td class="bordered" colspan="2">　{{ $list['title'] }}</td>
                            <td  class="bordered text-right">{{ $list['price'] }}</td>
                        </tr>
                    @endforeach
                @endif
                @if(isset($l['追加']) === true)
                    <tr>
                        <td class="bordered" colspan="2">　</td>
                        <td class="bordered">　</td>
                    </tr>
                    <tr>
                        <td class="bordered" colspan="2">追加のお引落し</td>
                        <td class="bordered">　</td>
                    </tr>
                    @foreach($l['追加'] as $list)
                        <tr>
                            <td class="bordered" colspan="2">　{{ $list['title'] }}</td>
                            <td class="bordered text-right">{{ $list['price'] }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
            <tfoot>
                <tr>
                    <td>　</td>
                    <th class="bordered">小計</th>
                    <td class="bordered">　</td>
                </tr>
                <tr>
                    <td>　</td>
                    <th class="bordered">消費税</th>
                    <td class="bordered">　</td>
                </tr>
                <tr>
                    <td>　</td>
                    <th class="bordered">合計</th>
                    <td class="bordered">　</td>
                </tr>
            </tfoot>
        </table>
        <p style="margin-top:30px;">※ご連絡は通常の月謝以外のお引落しがある月のみとさせて頂きます。</p>
    </div>
</div>


