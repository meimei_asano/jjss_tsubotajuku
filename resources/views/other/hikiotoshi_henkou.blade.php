<style>
body {
    font-size: 11pt;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
    table-layout: fixed;
    text-align: center;
    width: 100%;
}
th, td {
    padding: 0;
}
.main {
    width: 800px;
}
.bordered {
    border: solid 1px black;
}
.bg-color-silver {
    background-color: silver;
}
.clr {
    clear: both;
    margin: 0;
    padding: 0;
}
.row-indent {
    float: left;
    width: 3.5%;
}
.row-indent-right {
    float: right;
    width: 3.5%;
}
.row-1 .col-1 {
    float: right;
    text-align: right;
    width: 20%;
}
.row-2 {
    font-size: 21.5pt;
    font-weight: bold;
    text-align: center;
    text-decoration: underline;
}
.row-3 .col-1,
.row-9 .col-1,
.row-10 .col-1,
.row-21 .col-1
{
    float: left;
}
.group-row .col-1 {
    float: left;
    width: 65%;
}
.group-row .col-1 div div.student-name,
.group-row .col-1 div div.under-student-name,
{
    float: left;
    font-size: 13pt;
}
.group-row .col-2 {
    float: right;
    width: 23%;
}
.group-row .col-2 div.school-name {
    font-size: 15pt;
}
.row-11 .col-1,
.row-12 .col-1,
.row-13 .col-1,
.row-14 .col-1,
.row-15 .col-1,
.row-16 .col-1,
.row-17 .col-1
{
    float: left;
    width: 72%;
}
.row-11 .col-2,
.row-12 .col-2,
.row-13 .col-2,
.row-14 .col-2,
.row-15 .col-2,
.row-16 .col-2,
.row-17 .col-2
{
    float: left;
    width: 20%;
}
.row-18 .col-1,
.row-19 .col-1,
.row-20 .col-1
{
    float: left;
    width: 58%;
}
.row-18 .col-2,
.row-19 .col-2,
.row-20 .col-2
{
    float: left;
    width: 14%;
}
.row-18 .col-3,
.row-19 .col-3,
.row-20 .col-3
{
    float: left;
    width: 20%;
}
.text-center {
    text-align: center;
}
.text-right {
    text-align: right;
}
</style>
<div class="main">
    <div class="row-1">
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-1">20XX年×月〇日</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-2">お引落し金額変更のお知らせ</div>
    <br>
    <div class="row-3">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">〒××－〇〇</div>
        <div class="clr"></div>
    </div>
    <div class="group-row">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">
            <div>東京都××区〇〇町〇ー×－△</div>
            <div>××</div>
            <div>
                <div style="float: left; width: 5%">&nbsp;</div>
                <div class="student-name">××　様</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 5%">&nbsp;</div>
                <div class="under-student-name">保護者様</div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-2">
            <div class="school-name">坪田塾　××校</div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>東京都××区〇〇町</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>××ビル〇階</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div></div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div></div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-9">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">平素は格別のご高配に賜り、誠にありがとうございます。</div>
        <div class="clr"></div>
    </div>
    <div class="row-10">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">今月は通常の月謝以外のお引落しがございますのでご連絡いたします。</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-11">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1 bordered text-center">内訳</div>
        <div class="col-2 bordered text-center">金額</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    <div class="row-12">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1 bordered">通常月のお引落し</div>
        <div class="col-2 bordered">&nbsp;</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    <div class="row-13">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1 bordered" style="float: left; width: 72%;">　〇〇コース　×月分</div>
        <div class="col-2 bordered text-right">××</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    <div class="row-14">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1 bordered">&nbsp;</div>
        <div class="col-2 bordered">&nbsp;</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    <div class="row-15">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1 bordered">追加のお引落し</div>
        <div class="col-2 bordered">&nbsp;</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    <div class="row-16">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1 bordered">　コース変更に伴う差額精算分</div>
        <div class="col-2 bordered text-right">××</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    <div class="row-17">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1 bordered">　教材費</div>
        <div class="col-2 bordered text-right">××</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    <div class="row-18">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1" style="float: left; width: 58%;">&nbsp;</div>
        <div class="col-2 bordered bg-color-silver text-center">小計</div>
        <div class="col-3 bordered text-right">&nbsp;</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    <div class="row-19">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">&nbsp;</div>
        <div class="col-2 bordered bg-color-silver text-center">消費税</div>
        <div class="col-3 bordered text-right">&nbsp;</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    <div class="row-20">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">&nbsp;</div>
        <div class="col-2 bordered bg-color-silver text-center">合計</div>
        <div class="col-3 bordered text-right">&nbsp;</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-21">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">※ご連絡は通常の月謝以外のお引落しがある月のみとさせて頂きます。</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
</div>
