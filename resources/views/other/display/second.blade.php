<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    table-layout: fixed;
    font-size: 11pt;
}
th, td {
    padding: 0;
}
table.main {
    width: 800px;
}
table.main table {
    width: 100%;
}
.bordered {
    border: 1px solid black;
}
.bordered-bottom {
    border-bottom: 1px solid black;
}
.text-center {
    text-align: center;
}
.text-right {
    text-align: right;
}
.font-size-14pt {
    font-size: 14pt;
}
.font-size-16pt {
    font-size: 16pt;
}
.font-size-18pt {
    font-size: 18pt;
}
.no-border-top {
    border-top: 0;
}
.no-border-bottom {
    border-bottom: 0;
}
.no-border-right {
    border-right: 0;
}
.no-border-left {
    border-left: 0;
}
.bcolor-silver {
    background-color: silver;
}
.file-header {
    font-size: 24pt;
    text-decoration: underline;
    font-weight: bold;
}
.text-bold {
    font-weight: bold;
}
.text-underlined {
    text-decoration: underline;
}
.text-right {
    text-align: right;
}
</style>
<table class="main">
    <tr>
        <td colspan="24">&nbsp;</td>
        <td colspan="5" class="text-right">20XX年×月〇日</td>
    </tr>
    <tr>
        <td colspan="29" class="font-size-14pt">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="16">&nbsp;</td>
        <td colspan="4">生徒氏名：</td>
        <td colspan="9">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="16">&nbsp;</td>
        <td colspan="4">保護者氏名：</td>
        <td colspan="8">&nbsp;</td>
        <td class="text-right">印</td>
    </tr>
    <tr>
        <td colspan="29" class="font-size-14pt">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="29" class="text-center file-header">追加コマ申込書</td>
    </tr>
    <tr>
        <td colspan="29" class="font-size-14pt">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td colspan="28">坪田塾では、通常の授業時間以外に追加で授業を受けて頂くことができます。</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td colspan="28">ご希望の方は下記に必要事項をご記入の上、お申込み下さい。</td>
    </tr>
    <tr>
        <td colspan="29" class="font-size-14pt">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
        <td colspan="27">
            <table>
                <tr>
                    <td colspan="4" style="width: 13.8%">◆通塾コース：</td>
                    <td colspan="7" class="bordered-bottom" style="width: 24.15%">&nbsp;</td>
                    <td colspan="17" class="bordered-bottom text-right">月・火・水・木・金）時間：　　　；　　　～　　　：　　　</td>
                    <td style="width: 27px">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="6">◆追加コマ希望教科【</td>
                    <td colspan="22">&nbsp;</td>
                    <td class="text-right">】</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="29" class="font-size-14pt">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
        <td colspan="27">
            <table>
                <tr>
                    <td colspan="5" class="bordered text-center" style="width: 17.25%">追加授業日</td>
                    <td colspan="4" class="bordered text-center">曜日</td>
                    <td colspan="10" class="bordered text-center">時間帯</td>
                    <td colspan="4" class="bordered text-center">時間数</td>
                    <td colspan="4" class="bordered text-center">教科</td>
                </tr>
                <tr>
                    <td colspan="5" class="bordered text-center">&nbsp;</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                    <td colspan="10" class="bordered text-center">～</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="5" class="bordered text-center">&nbsp;</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                    <td colspan="10" class="bordered text-center">～</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="5" class="bordered text-center">&nbsp;</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                    <td colspan="10" class="bordered text-center">～</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="5" class="bordered text-center">&nbsp;</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                    <td colspan="10" class="bordered text-center">～</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="5" class="bordered text-center">&nbsp;</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                    <td colspan="10" class="bordered text-center">～</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="5" class="bordered text-center">&nbsp;</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                    <td colspan="10" class="bordered text-center">～</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="5" class="bordered text-center">&nbsp;</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                    <td colspan="10" class="bordered text-center">～</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                    <td colspan="4" class="bordered text-center">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="29" class="font-size-14pt">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td colspan="28">以下の時間を追加で申し込みます。</td>
    </tr>
    <tr>
        <td colspan="29" class="font-size-14pt">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
        <td colspan="4" style="width: 13.8%">◆追加時間数&nbsp;計</td>
        <td colspan="6" class="bordered-bottom">&nbsp;</td>
        <td colspan="5" class="bordered-bottom">時間 × 単価</td>
        <td colspan="4" class="bordered-bottom">&nbsp;</td>
        <td colspan="2" class="bordered-bottom">円＝</td>
        <td colspan="4" class="bordered-bottom">&nbsp;</td>
        <td class="bordered-bottom text-right">円</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td colspan="29" class="font-size-14pt">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td colspan="28">追加コマ単価表</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td colspan="4" class="bordered" style="width: 13.8%">&nbsp;</td>
        <td colspan="8" class="bordered text-center" style="width: 27.6%">中学・高校１年生</td>
        <td colspan="8" class="bordered text-center" style="width: 27.6%">中学・高校２年生</td>
        <td colspan="8" class="bordered text-center" style="width: 27.6%">中学・高校３年生、既卒生</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td colspan="4" class="bordered">塾生</td>
        <td colspan="8" class="bordered text-center">2,100円</td>
        <td colspan="8" class="bordered text-center">2,200円</td>
        <td colspan="8" class="bordered text-center">2,400円</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="4" class="bordered">塾外生</td>
        <td colspan="8" class="bordered text-center">2,300円</td>
        <td colspan="8" class="bordered text-center">2,400円</td>
        <td colspan="8" class="bordered text-center">2,600円</td>
    </tr>
    <tr>
        <td colspan="29" class="font-size-14pt">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td colspan="5" rowspan="4" class="bordered text-center">お振込み口座</td>
        <td colspan="11" class="bordered no-border-bottom">三菱UFJ銀行　新富町支店</td>
        <td colspan="12">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <!-- rowspan 4 above -->
        <td colspan="11" class="bordered no-border-bottom no-border-top">店番号　749</td>
        <td colspan="12">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <!-- rowspan 4 above -->
        <td colspan="11" class="bordered no-border-bottom no-border-top">普通　××</td>
        <td colspan="12">&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <!-- rowspan 4 above -->
        <td colspan="11" class="bordered no-border-top">株式会社　NEXT EDUCATION　××</td>
        <td colspan="12">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="29" class="font-size-14pt">&nbsp;</td>
    </tr>
</table>
