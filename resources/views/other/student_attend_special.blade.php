<style>
body {
    font-size: 11pt;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
    table-layout: fixed;
    text-align: center;
    width: 100%;
}
th, td {
    padding: 0;
}
.main {
    width: 800px;
}
.bordered {
    border: solid 1px black;
}
.bg-color-silver {
    background-color: silver;
}
.clr {
    clear: both;
    margin: 0;
    padding: 0;
}
.row-indent {
    float: left;
    width: 3.5%;
}
.row-indent-right {
    float: right;
    width: 3.5%;
}
.row-1 .col-1 {
    background-color: silver;
    border: solid 1px black;
    float: left;
    font-size: 16pt;
    margin: 0;
    padding: 3.5px 0;
    text-align: center;
    width: 20%;
}
.row-1 .col-2 {
    float: right;
    text-align: right;
    width: 20%;
}
.row-2 {
    font-size: 21.5pt;
    font-weight: bold;
    text-align: center;
    text-decoration: underline;
}
.row-3 .col-1,
.row-9 .col-1,
.row-10 .col-1,
.row-13 .col-1,
{
    float: left;
}
.row-11 .col-1 {
    float: left;
    width: 92%;
}
.row-12 .col-1 {
    float: left;
    width: 14.5%;
}
.row-12 .col-2 {
    float: left;
    width: 3%;
    text-align: center;
}
.row-12 .col-3 {
    float: left;
    width: 15%;
    text-align: center;
    border-bottom: solid 1px black;
}
.row-12 .col-4 {
    float: left;
    width: 17%;
    text-align: center;
    border-bottom: solid 1px black;
}
.row-12 .col-5 {
    float: left;
    width: 15%;
    text-align: center;
    border-bottom: solid 1px black;
}

.row-12 .col-6 {
    float: left;
    width: 7%;
    text-align: center;
    border-bottom: solid 1px black;
}
.row-12 .col-7 {
    float: left;
    width: 9%;
    text-align: center;
    border-bottom: solid 1px black;
}
.row-12 .col-8 {
    float: left;
    width: 5%;
    text-align: center;
    border-bottom: solid 1px black;
}
.group-row .col-1 {
    float: left;
    width: 65%;
}
.group-row .col-1 div div.student-name,
.group-row .col-1 div div.under-student-name,
{
    float: left;
    font-size: 13pt;
}
.group-row .col-2 {
    float: right;
    width: 23%;
}
.group-row .col-2 div.school-name {
    font-size: 15pt;
}
.row-last .col-1 {
    float: right;
    width: 22%;
}
.top-section-left {
    float: left;
    width: 50%;
}
.top-section-left .labels {
    float: left;
    width: 42%;
}
.top-section-left .labels div {
    line-height: 20px;
    text-align: center;
}
.top-section-left .answer-box {
    border: solid 1px black;
    float: left;
    line-height: 40px;
    text-align: center;
    vertical-align: middle;
    width: 28%;
}
.top-section-left .unit {
    float: left;
    line-height: 40px;
    text-align: center;
    vertical-align: middle;
    width: 14%;
}
.top-section-left .line-area {
    float: right;
    width: 14%;
}
.top-section-left .line-area div {
    line-height: 20px;
}
.top-section-left .line-area .line {
    border-right: solid 2px black;
    border-top: solid 2px black;
}
.top-section-right {
    float: right;
}
.space-section-left {
    border-right: solid 1.9px black;
    float: left;
    width: 49.70%;
}
.space-section-right {
    float: right;
}
.mid-section-left {
    border-right: solid 1.9px black;
    float: left;
    line-height: 42px;
    width: 49.70%;
}
.mid-section-right {
    float: right;
}
.mid-section-right .line-area {
    float: left;
    width: 14%;
}
.mid-section-right .line-area div {
    line-height: 20px;
}
.mid-section-right .line-area .line {
    border-top: solid 2px black;
}
.mid-section-right .label {
    float: left;
    width: 36%;
}
.mid-section-right .label div {
    line-height: 40px;
    text-align: right;
}
.mid-section-right .answer-box {
    border: solid 1px black;
    float: left;
    line-height: 40px;
    text-align: center;
    vertical-align: middle;
    width: 28%;
}
.mid-section-right .unit {
    float: left;
    line-height: 40px;
    text-align: center;
    vertical-align: middle;
    width: 14%;
}
.mid-section-right .section-right-indent {
    float: right;
    width: 6%
}
.bot-section-left {
    float: left;
    width: 50%;
}
.bot-section-left .labels {
    float: left;
    width: 42%;
}
.bot-section-left .labels div {
    line-height: 20px;
    text-align: center;
}
.bot-section-left .answer-box {
    border: solid 1px black;
    float: left;
    line-height: 40px;
    text-align: center;
    vertical-align: middle;
    width: 28%;
}
.bot-section-left .unit {
    float: left;
    line-height: 40px;
    text-align: center;
    vertical-align: middle;
    width: 14%;
}
.bot-section-left .line-area {
    float: right;
    width: 14%;
}
.bot-section-left .line-area div {
    line-height: 20px;
}
.bot-section-left .line-area .line {
    border-right: solid 2px black;
    border-bottom: solid 2px black;
}
.bot-section-right {
    float: right;
}
.bot-section-right div {
    line-height: 20px;
}
.bot-section-right div .space {
    float: left;
    width: 14%;
}
.bot-section-right div .label {
    float: left;
    text-align: center;
    width: 38%;
}
</style>
<div class="main">
    <div class="row-1">
        <div class="col-1">{{ $copyFor === 'home' ? '自宅保管用' : '校舎保管用' }}</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-2">20XX年×月〇日</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-2">2019年度 〇〇特別講習申込受付</div>
    <br>
    <div class="row-3">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">〒××－〇〇</div>
        <div class="clr"></div>
    </div>
    <div class="group-row">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">
            <div>東京都××区〇〇町〇ー×－△</div>
            <div>××</div>
            <div>
                <div style="float: left; width: 5%">&nbsp;</div>
                <div class="student-name">××　様</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 5%">&nbsp;</div>
                <div class="under-student-name">保護者様</div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-2">
            <div class="school-name">坪田塾　××校</div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>東京都××区〇〇町</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>××ビル〇階</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>TEL：</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>FAX：</div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-9">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">平素は格別のご高配に賜り、誠にありがとうございます。</div>
        <div class="clr"></div>
    </div>
    <div class="row-10">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">下記の通り〇〇特別講習のお申込みを受付いたしましたので、ご連絡させて頂きます。</div>
        <div class="clr"></div>
    </div>
    <br>
    <br>
    <div class="row-11">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">
            <!-- Top Section -->
            <div class="top-section-left">
                <div class="labels">
                    <div>①総合計</div>
                    <div>申込時間数</div>
                </div>
                <div class="answer-box">&nbsp;</div>
                <div class="unit">h</div>
                <div class="line-area">
                    <div>&nbsp;</div>
                    <div class="line">&nbsp;</div>
                </div>
                <div class="clr"></div>
            </div>
            <div class="top-section-right">&nbsp;</div>
            <div class="clr"></div>
            <!-- Space Section -->
            <div class="space-section-left">&nbsp;</div>
            <div class="space-section-right">&nbsp;</div>
            <div class="clr"></div>
            <!-- Middle Section -->
            <div class="mid-section-left">&nbsp;</div>
            <div class="mid-section-right">
                <div class="line-area">
                    <div>&nbsp;</div>
                    <div class="line">&nbsp;</div>
                </div>
                <div class="label">
                    <div>③追加時間数</div>
                </div>
                <div class="answer-box">&nbsp;</div>
                <div class="unit" style="">h</div>
                <div class="section-right-indent">&nbsp;</div>
                <div class="clr"></div>
            </div>
            <div class="clr"></div>
            <!-- Space Section -->
            <div class="space-section-left">&nbsp;</div>
            <div class="space-section-right">&nbsp;</div>
            <div class="clr"></div>
            <!-- Bottom Section -->
            <div class="bot-section-left">
                <div class="labels">
                    <div>②×／×～〇／〇の</div>
                    <div>通常授業時間数</div>
                </div>
                <div class="answer-box">&nbsp;</div>
                <div class="unit">h</div>
                <div class="line-area">
                    <div class="line">&nbsp;</div>
                    <div>&nbsp;</div>
                </div>
                <div class="clr"></div>
            </div>
            <div class="bot-section-right">
                <div>&nbsp;</div>
                <div>
                    <div class="space">&nbsp;</div>
                    <div class="label">①－②＝③</div>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="clr"></div>
        </div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    <br>
    <br>
    <div class="row-12">
        <div class="row-indent">&nbsp;</div>
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">◆追加時間数</div>
        <div class="col-2">計</div>
        <div class="col-3">&nbsp;</div>
        <div class="col-4">時間　× 単価</div>
        <div class="col-5">&nbsp;</div>
        <div class="col-6">円 ＝</div>
        <div class="col-7">&nbsp;</div>
        <div class="col-8">円</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    @if ($copyFor === 'home')
    <br>
    <div class="row-13">
        <div class="row-indent">&nbsp;</div>
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">代金は、次回請求時に合わせてご請求させて頂きます。</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-last">
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-1">
            <table>
                <tr>
                    <th class="bordered">担当</th>
                    <th class="bordered">校長</th>
                </tr>
                <tr>
                    <td class="bordered" style="height: 60px;">&nbsp;</td>
                    <td class="bordered">&nbsp;</td>
                </tr>
            </table>
        </div>
        <div class="clr"></div>
    </div>
    @endif
</div>
