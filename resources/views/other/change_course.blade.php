<style>
body {
    font-size: 11pt;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
    table-layout: fixed;
    text-align: center;
    width: 100%;
}
th, td {
    padding: 0;
}
.main {
    width: 800px;
}
.bordered {
    border: solid 1px black;
}
.bordered-bottom {
    border-bottom: solid 1px black;
}
.bg-color-silver {
    background-color: silver;
}
.text-centered {
    text-align: center;
}
.clr {
    clear: both;
    margin: 0;
    padding: 0;
}
.row-indent {
    float: left;
    width: 3.5%;
}
.row-indent-right {
    float: right;
    width: 3.5%;
}
.row-1 .col-1 {
    background-color: silver;
    border: solid 1px black;
    float: left;
    font-size: 16pt;
    margin: 0;
    padding: 3.5px 0;
    text-align: center;
    width: 20%;
}
.row-1 .col-2 {
    float: right;
    text-align: right;
    width: 20%;
}
.row-2 {
    font-size: 21.5pt;
    font-weight: bold;
    text-align: center;
    text-decoration: underline;
}
.row-3 .col-1,
.row-9 .col-1,
.row-10 .col-1,
{
    float: left;
}
.group-row .col-1 {
    float: left;
    width: 65%;
}
.group-row .col-1 div div.student-name,
.group-row .col-1 div div.under-student-name,
{
    float: left;
    font-size: 13pt;
}
.group-row .col-2 {
    float: right;
    width: 23%;
}
.group-row .col-2 div.school-name {
    font-size: 15pt;
}
.row-11 .col-1 {
    float: left;
    width: 17%;
}
.row-11 .col-2 {
    float: left;
    width: 14%;
}
.row-11 .col-3 {
    float: left;
    width: 64%;
}
.row-12 .col-1,
.row-18 .col-1,
{
    float: left;
    width: 12%;
}
.row-12 .col-2,
.row-18 .col-2,
{
    float: left;
    width: 80%;
}
.row-13 .col-1,
.row-19 .col-1,
{
    float: left;
    width: 80%;
}
.row-14 .col-1 {
    float: left;
    width: 17%;
}
.row-14 .col-2 {
    float: left;
    width: 14%;
}
.row-14 .col-3 {
    float: left;
    width: 64%;
}
.row-15 .col-1,
.row-16 .col-1,
.row-17 .col-1,
{
    float: left;
    width: 16%;
}
.row-15 .col-2,
.row-15 .col-3,
.row-15 .col-4,
.row-15 .col-5,
.row-15 .col-6,
.row-16 .col-2,
.row-16 .col-3,
.row-16 .col-4,
.row-16 .col-5,
.row-16 .col-6,
.row-17 .col-2,
.row-17 .col-3,
.row-17 .col-4,
.row-17 .col-5,
.row-17 .col-6,
{
    float: left;
    width: 13.5%;
}
.row-last .col-1 {
    float: right;
    width: 22%;
}
</style>
<div class="main">
    <div class="row-1">
        <div class="col-1">{{ $copyFor === 'home' ? '自宅保管用' : '校舎保管用' }}</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-2">20XX年×月〇日</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-2">コ ー ス 変 更 受 付</div>
    <br>
    <div class="row-3">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">〒××－〇〇</div>
        <div class="clr"></div>
    </div>
    <div class="group-row">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">
            <div>東京都××区〇〇町〇ー×－△</div>
            <div>××</div>
            <div>
                <div style="float: left; width: 5%">&nbsp;</div>
                <div class="student-name">××　様</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 5%">&nbsp;</div>
                <div class="under-student-name">保護者様</div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-2">
            <div class="school-name">坪田塾　××校</div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>東京都××区〇〇町</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>××ビル〇階</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>TEL：</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>FAX：</div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-9">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">平素は格別のご高配に賜り、誠にありがとうございます。</div>
        <div class="clr"></div>
    </div>
    <div class="row-10">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">下記の通りコースの変更を受付いたしましたので、ご連絡させて頂きます。</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-11">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">◆現在のコース：</div>
        <div class="col-2 bordered-bottom">&nbsp;</div>
        <div class="col-3">コース（週×回、１回×時間）</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-12">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">&nbsp;</div>
        <div class="col-2">（月謝金額：××××円）</div>
        <div class="clr"></div>
    </div>
    <br>
    <br>
    <div class="row-13">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">20××年×月×日より</div>
        <div class="clr"></div>
    </div>
    <div class="row-14">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">◆新しいコース：</div>
        <div class="col-2 bordered-bottom">&nbsp;</div>
        <div class="col-3">コース（週×回、１回×時間）</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-15">
        <div class="row-indent">&nbsp;</div>
        <div class="row-indent">&nbsp;</div>
        <div class="col-1 bordered">&nbsp;</div>
        <div class="col-2 bordered text-centered">月</div>
        <div class="col-3 bordered text-centered">火</div>
        <div class="col-4 bordered text-centered">水</div>
        <div class="col-5 bordered text-centered">木</div>
        <div class="col-6 bordered text-centered">金</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    <div class="row-16">
        <div class="row-indent">&nbsp;</div>
        <div class="row-indent">&nbsp;</div>
        <div class="col-1 bordered text-centered">開始時間</div>
        <div class="col-2 bordered text-centered">：</div>
        <div class="col-3 bordered text-centered">：</div>
        <div class="col-4 bordered text-centered">：</div>
        <div class="col-5 bordered text-centered">：</div>
        <div class="col-6 bordered text-centered">：</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    <div class="row-17">
        <div class="row-indent">&nbsp;</div>
        <div class="row-indent">&nbsp;</div>
        <div class="col-1 bordered text-centered">終了時間</div>
        <div class="col-2 bordered text-centered">：</div>
        <div class="col-3 bordered text-centered">：</div>
        <div class="col-4 bordered text-centered">：</div>
        <div class="col-5 bordered text-centered">：</div>
        <div class="col-6 bordered text-centered">：</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-18">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">&nbsp;</div>
        <div class="col-2">（月謝金額：××××円）</div>
        <div class="clr"></div>
    </div>
    <br>
    <br>
    <div class="row-19">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">従来のコースとの差額は、次回請求時にご精算させて頂きます。</div>
        <div class="clr"></div>
    </div>
    @if ($copyFor === 'home')
    <br>
    <div class="row-last">
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-1">
            <table>
                <tr>
                    <th class="bordered">担当</th>
                    <th class="bordered">校長</th>
                </tr>
                <tr>
                    <td class="bordered" style="height: 60px;">&nbsp;</td>
                    <td class="bordered">&nbsp;</td>
                </tr>
            </table>
        </div>
        <div class="clr"></div>
    </div>
    @endif
</div>
