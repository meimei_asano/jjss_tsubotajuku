<style>
body {
    font-size: 11pt;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
    table-layout: fixed;
    text-align: center;
    width: 100%;
}
th, td {
    padding: 0;
}
.main {
    width: 800px;
}
.bordered {
    border: solid 1px black;
}
.bg-color-silver {
    background-color: silver;
}
.clr {
    clear: both;
    margin: 0;
    padding: 0;
}
.row-indent {
    float: left;
    width: 3.5%;
}
.row-indent-right {
    float: right;
    width: 3.5%;
}
.row-1 .col-1 {
    background-color: silver;
    border: solid 1px black;
    float: left;
    font-size: 16pt;
    margin: 0;
    padding: 3.5px 0;
    text-align: center;
    width: 20%;
}
.row-1 .col-2 {
    float: right;
    text-align: right;
    width: 20%;
}
.row-2 {
    font-size: 21.5pt;
    font-weight: bold;
    text-align: center;
    text-decoration: underline;
}
.row-3 .col-1,
.row-9 .col-1,
.row-10 .col-1,
.row-11 .col-1,
.row-12 .col-1,
{
    float: left;
}
.group-row .col-1 {
    float: left;
    width: 65%;
}
.group-row .col-1 div div.student-name,
.group-row .col-1 div div.under-student-name,
{
    float: left;
    font-size: 13pt;
}
.group-row .col-2 {
    float: right;
    width: 23%;
}
.group-row .col-2 div.school-name {
    font-size: 15pt;
}
.row-last .col-1 {
    float: right;
    width: 22%;
}
</style>
<div class="main">
    <div class="row-1">
        <div class="col-1">{{ $copyFor === 'home' ? '自宅保管用' : '校舎保管用' }}</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-2">20XX年×月〇日</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-2">
    @switch($format)
        @case('kyujuku')
            休 塾 願 受 理
            @break
        @case('sotsujuku')
            卒 塾 願 受 理
            @break
        @case('taijuku')
            退 塾 願 受 理
            @break
    @endswitch
    </div>
    <br>
    <div class="row-3">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">〒××－〇〇</div>
        <div class="clr"></div>
    </div>
    <div class="group-row">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">
            <div>東京都××区〇〇町〇ー×－△</div>
            <div>××</div>
            <div>
                <div style="float: left; width: 5%">&nbsp;</div>
                <div class="student-name">××　様</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 5%">&nbsp;</div>
                <div class="under-student-name">保護者様</div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-2">
            <div class="school-name">坪田塾　××校</div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>東京都××区〇〇町</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>××ビル〇階</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>TEL：</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>FAX：</div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-9">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">平素は格別のご高配に賜り、誠にありがとうございます。</div>
        <div class="clr"></div>
    </div>
    <div class="row-10">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">
        @switch($format)
            @case('kyujuku')
                下記の通り休塾願を受理いたしましたので、ご連絡させて頂きます。
                @break
            @case('sotsujuku')
                下記の通り卒塾願を受理いたしましたので、ご連絡させて頂きます。
                @break
            @case('taijuku')
                下記の通り退塾願を受理いたしましたので、ご連絡させて頂きます。
                @break
        @endswitch
        </div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-11">
        <div class="row-indent">&nbsp;</div>
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">
        @switch($format)
            @case('kyujuku')
                ◆休塾期間： ××年〇月×日～××年〇月×日まで
                @break
            @case('sotsujuku')
                ◆卒塾年月日： ××年〇月×日
                @break
            @case('taijuku')
                ◆退塾年月日： ××年〇月末
                @break
        @endswitch
        </div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-12">
        <div class="row-indent">&nbsp;</div>
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">
        @switch($format)
            @case('kyujuku')
                ◆休塾理由： 進学等
                @break
            @case('sotsujuku')
                ◆卒塾理由： 進学等
                @break
            @case('taijuku')
                ◆退塾理由： 進学等
                @break
        @endswitch
        </div>
        <div class="clr"></div>
    </div>
    @if ($copyFor === 'home')
    <br>
    <br>
    <div class="row-last">
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-1">
            <table>
                <tr>
                    <th class="bordered">担当</th>
                    <th class="bordered">校長</th>
                </tr>
                <tr>
                    <td class="bordered" style="height: 60px;">&nbsp;</td>
                    <td class="bordered">&nbsp;</td>
                </tr>
            </table>
        </div>
        <div class="clr"></div>
    </div>
    @endif
</div>
