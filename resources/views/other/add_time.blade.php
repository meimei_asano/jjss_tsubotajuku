<style>
body {
    font-size: 11pt;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
    table-layout: fixed;
}
th, td {
    padding: 0;
}
.main {
    width: 800px;
}
.main table {
    width: 100%;
}
.clr {
    clear: both;
    margin: 0;
    padding: 0;
}
.row-1 .col-1 {
    float: left;
    width: 20%;
    border: solid 1px black;
    padding: 3.5px 0;
    margin: 0;
    text-align: center;
    background-color: silver;
    font-size: 16pt;
}
.row-1 .col-2 {
    float: right;
    width: 20%;
    text-align: right;
}
.row-2 {
    font-size: 21.5pt;
    text-decoration: underline;
    font-weight: bold;
    text-align: center;
}
.row-indent {
    float: left;
    width: 3.5%;
}
.row-indent-right {
    float: right;
    width: 3.5%;
}
.row-3 .col-1 {
    float: left;
}
.row-9 .col-1 {
    float: left;
}
.row-10 .col-1 {
    float: left;
}
.row-11 .col-1 {
    float: left;
    width: 93%;
}
table.tsuika-request {
    border: solid 1px black;
    text-align: center;
}
table.tsuika-request td {
    border: solid 1px black;
}
.row-12 .col-1 {
    float: left;
    width: 15%;
}
.row-12 .col-2 {
    float: left;
    width: 3%;
    text-align: center;
}
.row-12 .col-3 {
    float: left;
    width: 16%;
    text-align: center;
    border-bottom: solid 1px black;
}
.row-12 .col-4 {
    float: left;
    width: 17%;
    text-align: center;
    border-bottom: solid 1px black;
}
.row-12 .col-5 {
    float: left;
    width: 16%;
    text-align: center;
    border-bottom: solid 1px black;
}

.row-12 .col-6 {
    float: left;
    width: 7%;
    text-align: center;
    border-bottom: solid 1px black;
}
.row-12 .col-7 {
    float: left;
    width: 10%;
    text-align: center;
    border-bottom: solid 1px black;
}
.row-12 .col-8 {
    float: left;
    width: 5%;
    text-align: center;
    border-bottom: solid 1px black;
}
.row-13 .col-1 {
    float: left;
}
.row-14 .col-1 {
    float: left;
    width: 93%;
}
table.unit-prices {
    border: solid 1px black;
    text-align: center;
}
table.unit-prices td {
    border: solid 1px black;
}
.group-row .col-1 {
    float: left;
    width: 65%;
}
.group-row .col-1 div div.student-name,
.group-row .col-1 div div.under-student-name,
{
    float: left;
    font-size: 13pt;
}
.group-row .col-2 {
    float: right;
    width: 23%;
}
.group-row .col-2 div.school-name {
    font-size: 15pt;
}
</style>
<div class="main">
    <div class="row-1">
        <div class="col-1">校舎保管用</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-2">20XX年×月〇日</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-2">追加コマ申込受付</div>
    <br>
    <div class="row-3">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">〒××－〇〇</div>
        <div class="clr"></div>
    </div>
    <div class="group-row">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">
            <div>東京都××区〇〇町〇ー×－△</div>
            <div>××</div>
            <div>
                <div style="float: left; width: 5%">&nbsp;</div>
                <div class="student-name">××　様</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 5%">&nbsp;</div>
                <div class="under-student-name">保護者様</div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-2">
            <div class="school-name">坪田塾　××校</div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>東京都××区〇〇町</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>××ビル〇階</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>TEL：</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>FAX：</div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-9">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">平素は格別のご高配に賜り、誠にありがとうございます。</div>
        <div class="clr"></div>
    </div>
    <div class="row-10">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">下記の通り追加コマのお申込みを受付いたしましたので、ご連絡させて頂きます。</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-11">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">
            <table class="tsuika-request">
                <tr>
                    <td style="width: 20%">追加授業日</td>
                    <td style="width: 15%">曜日</td>
                    <td style="width: 35%">時間帯</td>
                    <td style="width: 15%">時間数</td>
                    <td style="width: 15%">教科</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td> ～ </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td> ～ </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td> ～ </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td> ～ </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td> ～ </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
        <div class="row-indent-right"></div>
        <div class="clr"></div>
    </div>
    <br>
    <br>
    <div class="row-12">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">◆追加時間数</div>
        <div class="col-2">計</div>
        <div class="col-3">&nbsp;</div>
        <div class="col-4">時間　× 単価</div>
        <div class="col-5">&nbsp;</div>
        <div class="col-6">円 ＝</div>
        <div class="col-7">&nbsp;</div>
        <div class="col-8">円</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-13">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">追加コマ単価表</div>
        <div class="clr"></div>
    </div>
    <div class="row-14">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">
            <table class="unit-prices">
                <tr>
                    <td style="width: 10%">&nbsp;</td>
                    <td style="width: 30%">中学・高校１年生</td>
                    <td style="width: 30%">中学・高校２年生</td>
                    <td style="width: 30%">中学・高校３年生、既卒生</td>
                </tr>
                <tr>
                    <td style="text-align: left;">&nbsp;塾生</td>
                    <td>2,100円</td>
                    <td>2,200円</td>
                    <td>2,400円</td>
                </tr>
                <tr>
                    <td style="text-align: left;">&nbsp;塾外生</td>
                    <td>2,300円</td>
                    <td>2,400円</td>
                    <td>2,600円</td>
                </tr>
            </table>
        </div>
        <div class="row-indent-right"></div>
        <div class="clr"></div>
    </div>
</div>
