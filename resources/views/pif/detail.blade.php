
<link href="{{ url('css/pif.css') }}" rel="stylesheet">

<div class="form-group mgn-lft4" >

    @include('pif.memo_list')

    <form id="form_pif" >
        {{ csrf_field() }}
        <table class="table-pif mgn-btm8" >
            <thead>
                <colgroup>
                    <col style="width: 70px;">
                    <col style="width:100px;">
                    <col style="width: 70px;">
                    <col style="width:100px;">
                    <col style="width: 70px;">
                    <col style="width:100px;">
                </colgroup>
            </thead>
            <tbody>
                <tr>
                    <th>名前</th>
                    <td>{{ $student->name }}</td>
                    <th>フリガナ</th>
                    <td>{{ $student->kana }}</td>
                    <th>生年月日</th>
                    <td>{{ $student->birth_date or null }}</td>
                </tr>
                <tr>
                    <th>志望校</th>
                    <td>{{ $student->shibou->shibou_school or null }}</td>
                    <th>学部</th>
                    <td colspan="3">{{ $student->shibou->shibou_gakubu or null }}</td>
                </tr>
                <tr>
                    <th>学習科目</th>
                    <td colspan="5">
                        @if(count($student->kamokus) > 0)
                            @foreach($student->kamokus as $student_kamoku)
                                {{ $student_kamoku->kamoku->name }}&nbsp;
                            @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>学校</th>
                    <td>{{ $student->gakkou_name }}</td>
                    <th class="pif_editable" >部活</th>
                    <td colspan="3">
                        <textarea name="bukatsu" style="height:1.5em;">{{ $student->student_pif->bukatsu or null }}</textarea>
                    </td>
                </tr>
                <tr>
                    <th  class="pif_editable" style="font-size:70%;">学校関係特記</th>
                    <td colspan="5">
                        <textarea name="gakkou_tokki" style="height:1.5em;">{{ $student->student_pif->gakkou_tokki or null }}</textarea>
                    </td>
                </tr>
                <tr>
                    <th class="pif_editable" >夢</th>
                    <td>
                        <textarea name="yume" style="height:1.5em;">{{ $student->student_pif->yume or null }}</textarea>
                    </td>
                    <th class="pif_editable" >趣味</th>
                    <td colspan="3">
                        <textarea name="syumi" style="height:1.5em;">{{ $student->student_pif->syumi or null }}</textarea>
                    </td>
                </tr>
                <tr>
                    <th class="pif_editable" >家族構成</th>
                    <td colspan="5">
                        <textarea name="kazoku_kousei" style="height:1.5em;">{{ $student->student_pif->kazoku_kousei or null }}</textarea>
                    </td>
                </tr>
                <tr>
                    <th class="pif_editable" >家庭環境</th>
                    <td colspan="5">
                        <textarea name="katei_jijyou" style="height:1.5em;">{{ $student->student_pif->katei_jijyou or null }}</textarea>
                    </td>
                </tr>
                <tr>
                    <th class="pif_editable" >赤点・進級</th>
                    <td>
                        <textarea name="akaten_shinkyu">{{ $student->student_pif->akaten_shinkyu or null }}</textarea>
                    </td>
                    <th class="pif_editable" style="font-size: 70%;">定期(日付)</th>
                    <td colspan="3">
                        <textarea name="teiki_test">{{ $student->student_pif->teiki_test or null }}</textarea>
                    </td>
                </tr>
                <tr>
                    <th>コース</th>
                    <td colspan="5">
                        @if(count($student->attend_bases) > 0)
                            @foreach($student->attend_bases as $attend_base)
                                {{-- start_date < 今日 < end_date(or 未登録) なら表示 --}}
                                @if(Carbon\Carbon::parse($attend_base->start_date)->lt(Carbon\Carbon::today()) && (strlen($attend_base->end_date) == 0 || Carbon\Carbon::today()->lt(Carbon\Carbon::parse($attend_base->end_date))))
                                    {{ $attend_base->course->name }}
                                @endif
                            @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                    <th>予定時間</th>
                    <td colspan="5">
                        @if(count($student->attend_bases) > 0)
                            @foreach($student->attend_bases as $attend_base)
                                {{-- start_date < 今日 < end_date(or 未登録) なら表示 --}}
                                @if(Carbon\Carbon::parse($attend_base->start_date)->lt(Carbon\Carbon::today()) && (strlen($attend_base->end_date) == 0 || Carbon\Carbon::today()->lt(Carbon\Carbon::parse($attend_base->end_date))))
                                    <div class="flex">
                                        <div class="txt-ctr" style="width:20%;">
                                            <p>月</p><p>{{ \JJSS::displayTime($attend_base->mon_start_time) }} - {{ \JJSS::displayTime($attend_base->mon_end_time) }}</p>
                                        </div>
                                        <div class="txt-ctr" style="width:20%;">
                                            <p>火</p><p>{{ \JJSS::displayTime($attend_base->tue_start_time) }} - {{ \JJSS::displayTime($attend_base->tue_end_time) }}</p>
                                        </div>
                                        <div class="txt-ctr" style="width:20%;">
                                            <p>水</p><p>{{ \JJSS::displayTime($attend_base->wed_start_time) }} - {{ \JJSS::displayTime($attend_base->wed_end_time) }}</p>
                                        </div>
                                        <div class="txt-ctr" style="width:20%;">
                                            <p>木</p><p>{{ \JJSS::displayTime($attend_base->thu_start_time) }} - {{ \JJSS::displayTime($attend_base->thu_end_time) }}</p>
                                        </div>
                                        <div class="txt-ctr" style="width:20%;">
                                            <p>金</p><p>{{ \JJSS::displayTime($attend_base->fri_start_time) }} - {{ \JJSS::displayTime($attend_base->fri_end_time) }}</p>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </td>
                </tr>
                <tr>
                    <th class="pif_editable" style="font-size:70%;">帰宅時間配慮</th>
                    <td colspan="5">
                        <textarea name="hairyo_jikou">{{ $student->student_pif->hairyo_jikou or null }}</textarea>
                    </td>
                </tr>
                <tr>
                    <th>９タイプ</th>
                    <td colspan="5">
                        {{ ($student->ninetype) ? \JJSS::displayNineType($student->ninetype->ninetype_codes) : '' }}
                    </td>
                </tr>
                <tr class="default_display">
                    <th>タイプ別コツ</th>
                    <td colspan="5">
                        {!! ($student->ninetype) ? \JJSS::displayNineTypePoint($student->ninetype->ninetype_codes) : '' !!}
                    </td>
                </tr>
                <tr class="default_display">
                    <th class="daiji">　</th>
                    <td class="daiji" colspan="5">
                        <textarea name="iwanai" style="height:10em;" placeholder="言っちゃいけない">{{ $student->student_pif->iwanai or null }}</textarea>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="btn_list txt-rgt">
            <button type="button" class="more_pif_data btn-flat btn-sm flt-lft"><i class="bx bx-chevrons-down"></i>さらに情報を表示</button>
            <button type="button" class="hide_pif_data btn-flat btn-sm flt-lft" style="display:none;"><i class="bx bx-chevrons-up"></i>情報を閉じる</button>
            <button type="button" class="save_pif btn-sm">生徒情報を登録する</button>
        </div>
        <input type="hidden" name="student_id" value="{{ $student->id }}">
        <input type="hidden" name="pif_id" value="{{ $student->student_pif->id or null }}">
    </form>


</div>




