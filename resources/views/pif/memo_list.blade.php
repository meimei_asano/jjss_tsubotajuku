<link href="{{ url('css/pif.css') }}" rel="stylesheet">

<div class="add_memo_contents form-group">
    {{-- 登録フォーム部分 --}}
    <div class="pif_memo_edit mgn-btm16">
        科目：<select name="pif_memo_kamoku_id" >
                <option value="">- - - -</option>
                {!! \JJSS::options($kamokus->toArray(), 'id', 'name') !!}
             </select>
        <textarea name="pif_memo_naiyou" class="pif_memo_naiyou" style="height:8em;margin-top: 5px;"
            placeholder="指導の中で気づいた「伸びたところ・良かったところ」「指導においての注意点」など、他の先生にも共有したいことを記録しておきましょう。"></textarea>
        <ul>
            <li class="txt-rgt">
                <button type="button" class="save_pif_memo btn-line" style="margin-top:4px;">登録する</button>
            </li>
        </ul>
        <input type="hidden" name="pif_memo_student_id" value="{{ $student->id }}">
        <input type="hidden" name="pif_memo_id" value="">
        <input type="hidden" name="pif_memo_teacher_id" value="{{ $teacher_id }}">
        <input type="hidden" name="pif_memo_login_teacher_id" value="{{ $teacher_id }}">
    </div>

    {{-- 履歴表示部分 --}}
    <div class="scroll_table" style="height: 30vh; max-height: 30vh;background-color: #F1F2F5;">
        <table class="table-pif mgn-btm8" >
            <thead>
            <colgroup>
                <col style="width: 80px;">
                <col style="width: 90px;">
                <col style="width: 240px;">
                <col style="width: 80px;">
                <col style="width: 50px;">
            </colgroup>
            <tr>
                <th>日付</th>
                <th>科目</th>
                <th>内容</th>
                <th>先生</th>
                <th>　</th>
            </tr>
            </thead>
            <tbody class="pif_memo_list">
            </tbody>
        </table>
    </div>
</div>




