@extends('layouts.default')
@section('content')
<style>
    .booking_menu li:first-child { cursor : pointer; }
    .booking_menu li:not(:first-child) { display : none; }
    .set_orderby {cursor:pointer;}
</style>
<section class="scroll_table-wrap wht_bloc">
    <h2 class="heading-1">入退室生徒リスト</h2>
    <div class="flex flex-j-between flex-a-ctr mgn-btm16">
        <form id="search_form" action="{{ url('classroom/coming') }}" method="get">
            <div class="form-group flex">
                @if(in_array($permission, ['講師', '校長']))
                    <ul>
                        <li>
                            <input type="checkbox" id="tantou" value="1" {{ $search_tantou ? 'checked' : '' }}>
                            <label for="tantou">担当生徒のみ表示</label>
                            <input type="hidden" id="search_tantou" name="search_tantou"/>
                        </li>
                    </ul>
                @endif
                @if(in_array($permission, ['管理者', 'マネージャー', '本部スタッフ']))
                    <select id="school_id">
                        <option value="">選択してください</option>
                        @foreach ($schools as $school)
                            <option value="{{ $school->id }}"{{ ($school->id == $school_id) ? ' selected' : '' }}>{{ $school->name }}</option>
                        @endforeach
                    </select>
                @endif
                <input type="text" id="search_date" placeholder="日付選択" class="datepicker-here input-sm" data-language="jp"/>
                <button id="btn-search">検索<i class='bx bx-search'></i></button>

                <input type="hidden" name="orderby_column_name" value="{{ ($orderby['column_name']) ?? '' }}">
                <input type="hidden" name="orderby_type" value="{{ ($orderby['type']) ?? '' }}">
            </div>
        </form>
        <div class="btn_list">
            @php
                $target_school_id = $school_id;
                if(strlen($target_school_id) == 0) {
                    $target_school_id = 9999;
                }
            @endphp
            <button id="checking" onclick="location.href='{{ url("classroom/checking/{$target_school_id}/{$target_date}") }}';"><i class='bx bxs-printer'></i>印刷待ち子別ノート一覧</button>
        </div>


        <div class="account_info pos_rel flex flex-a-ctr" style="z-index : 100;">
            <div>
                <a class="account_info--user" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    追加コマ・振替<i class='bx bx-chevron-down'></i>
                </a>
                <ul class="account_info--nav header--subnav ts4">
                    <li>
                        <a id="booking"><i class='bx bx-rocket'></i>登録</a>
                    </li>
                    <li>
                        <a href="{{ url('/classroom/changing/') }}"><i class='bx bx-spreadsheet'></i>一覧</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="scroll_table mgn-btm16">
        <table class="table-horizon table-striped datatable">
            <thead>
            <tr>
                <th class="set_orderby" dt-column="kana">生徒名</th>
                <th class="txt-ctr">子別ノート</th>
                <th></th>
                <th colspan="2" class="txt-ctr">入退室予定</th>
                <th colspan="2" class="txt-ctr">入退室実績</th>
                <th class="txt-ctr">備考</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if(count($comings) == 0)
                <tr>
                    <td colspan="8">
                        登録されていません
                    </td>
                </tr>
            @else
                @foreach ($comings as $coming)
                    <tr style="">
                        <td class="">{{ $coming['student']['name'] }}</td>
                        <td class="td_control txt-ctr">
                            <button onclick="location.href='{{ url('classroom/note/'. $coming['student']['id']. '/'. $target_date) }}';"><i class='bx bxs-book-open'></i></button>
                        </td>
                        <td class="txt-ctr">
                            @if(empty($coming['note_id']))
                                未作成
                            @else
                                @if($coming['checked_flag'] == "0")
                                    未承認
                                @endif
                            @endif
                        </td>
                        <td class="td_time td_tilde">
                            {{ \JJSS::displayTime($coming['plan_attend_start_time']) }}
                        </td>
                        <td class="td_time">
                            {{ \JJSS::displayTime($coming['plan_attend_end_time']) }}
                        </td>
                        <td class="td_time td_tilde">
                            @if(strlen($coming['note_id']) > 0)
                                @if(strlen($coming['actual_attend_start_time']) == 0 && $coming['checked_flag'] == "1")
                                    <button class="timer_start" data-note_id="{{ $coming['note_id'] }}"><i class="bx bx-timer"></i>入室</button>
                                @else
                                    <span class="start_time" data-note_id="{{ $coming['note_id'] }}">{{ \JJSS::displayTime($coming['actual_attend_start_time']) }}</span>
                                @endif
                            @endif
                        </td>
                        <td class="td_time">
                            @if(strlen($coming['note_id']) > 0)
                                @if(strlen($coming['actual_attend_start_time']) > 0 && strlen($coming['actual_attend_end_time']) == 0)
                                    <button class="timer_end" data-note_id="{{ $coming['note_id'] }}"><i class="bx bx-timer"></i>退室</button>
                                @elseif(strlen($coming['actual_attend_end_time']) == 0 && $coming['checked_flag'] == "1")
                                    <button class="timer_end is_disabled" data-note_id="{{ $coming['note_id'] }}"><i class="bx bx-timer"></i>退室</button>
                                @else
                                    <span class="end_time" data-note_id="{{ $coming['note_id'] }}">{{ \JJSS::displayTime($coming['actual_attend_end_time']) }}</span>
                                @endif
                            @endif
                        </td>
                        <td class="txt-ctr">
                            {{ $coming['change_date'] }}
                        </td>
                        <td class="" style="width:400px;"></td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</section>
@endsection
@section('modal')
<link href="{{ asset('css/modal.css') }}" rel="stylesheet" defer>
<div id="background" style="display: none;">
    <div id="modal">
        <div class="flex flex-j-between flex-a-ctr mgn-btm8">
            <div></div>
            <div>
                <span><i id="modal-close" class="bx bx-x"></i></span>
            </div>
        </div>
        <div id="modal-panel">
            <div class="form-group">
                生徒：
                <select id="change_student_id">
                    <option class="default" value="">選択してください</option>
                    @foreach ($students as $student)
                        <option value="{{ $student->id }}">{{ $student->name }}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <label id="change_tsuika"><input type="radio" name="change_radio" value="tsuika">追加コマ</label>
            </div>
            <div>
                <label id="change_furikae"><input type="radio" name="change_radio" value="furikae">振替</label>
            </div>
            <div id="change_before_date_div" class="form-group">
                変更前の日付：
                <input type='text' id="change_before_date" name="change_before_date" class="datepicker-here" data-language="jp">
            </div>
            <div id="change_attend_date_div" class="form-group">
                変更後の日付：
                <input type='text' id="change_attend_date" name="change_attend_date" class="datepicker-here" data-language="jp">
            </div>
            <div id="change_time_div" class="form-group">
                入室予定〜退室予定：
                <input type='text' id="change_start_time" name="change_start_time" class="timepicker input-mn" value="">
                〜
                <input type='text' id="change_end_time" name="change_end_time" class="timepicker input-mn" value="">
            </div>
            <div id="change_submit_div" style="display: none;">
                <button>登録</button>
            </div>
            <div id="tsuika_message">
                <div class="alert alert-warning">
                    <i class="bx bx-info-circle"></i>メニュー：追加コマ にて登録いただけます。
                </div>
            </div>
        </div>
    </div>
</div>
<form id="form" method="post">
    {{ csrf_field() }}
</form>
@endsection
@section('script')
<script src="{{ asset('js/timepicker.min.js') }}" defer></script>
<script>
$(function() {
    var target_date = new Date('{{ $target_date }}');
    $('#search_date').datepicker({language: 'jp', autoClose: true}).data('datepicker').selectDate(target_date);

    $('#change_student_id').select2({ language: "ja"});

    $('#btn-search').on('click', function(){
        // 担当生徒のみ表示
        if ($('#tantou').length !== 0) {
            if ($('#tantou').prop('checked')) {
                $('#search_tantou').val(1);
            } else {
                $('#search_tantou').val(0);
            }
        }

        var action = $('#search_form').attr('action'),
            school_id = $('#school_id').val(),
            selected_date = $('#search_date').val();

        if (school_id === undefined) {
            school_id = '{{ $school_id }}';
        }

        if (school_id == "") {
            school_id = 9999;
        }

        action += '/' + school_id + '/' + selected_date;
        $('#search_form').attr('action', action);
        $('#search_form').submit();
    });

    $('.set_orderby').on('click', function() {
        var current_orderby = $('input[name="orderby_type"]').val();
        var current_column = $('input[name="orderby_column_name"]').val();
        var new_column = $(this).attr('dt-column');

        if (current_column == new_column && current_orderby == "asc") {
            $('input[name="orderby_type"]').val('desc');
        } else {
            $('input[name="orderby_column_name"]').val(new_column);
            $('input[name="orderby_type"]').val('asc');
        }

        $('#btn-search').trigger('click');
    });

    $('#create_today_all').on('click', function(){
        $('.loading').show();
    });

    $(document).on('click', '.timer_start', function(){
        var self = $(this);
        instruction('start', self, null);
    });

    $(document).on('click', '.timer_end', function(){
        var self = $(this);
        instruction('end', self, null);
    });

    function instruction(start_end, self, result_flag)
    {
        var student_note_id = self.data('note_id');
        $.ajax({
            url: "/api/classroom/coming/instruction/" + start_end + "/" + student_note_id,
            type: "PUT",
            data: {
            },
            dataType: 'json',
            beforeSend: function() {
                $('.loading').fadeIn();
                self.fadeOut();
            },
        })
        .done( (data, statusText, xhr) => {
            if (start_end == 'start') {
                var col = '<span class="start_time" data-note_id="' + student_note_id + '">' + data.start_time + '</span>';
                self.parent().parent().find('.timer_end').removeClass('is_disabled');
            } else if (start_end == 'end') {
                var col = '<span class="end_time" data-note_id="' + student_note_id + '">' + data.end_time + '</span>';
            }
            self.after(col);
            self.remove();
        })
        .fail( (data) => {
            var options = {
                text: "処理中にエラーが発生しました。",
                icon: "error",
                timer: 2000,
                buttons: false,
            };
            swal(options);
            self.show();
        })
        .always( (data) => {
            $('.loading').fadeOut();
        });
    }

    // 追加コマ・振替
    @if ($date_type == \JJSS::DATE_TYPE['basic'])
    $('.timepicker').timepicker({
        'timeFormat': 'H:i',
        'minTime': '21:40',
        'maxTime': '21:40',
        'scrollDefault': '16:00',
        'noneOption': [
            { 'label': '16:00', 'value': '16:00' },
            { 'label': '16:30', 'value': '16:30' },
            { 'label': '17:00', 'value': '17:00' },
            { 'label': '17:30', 'value': '17:30' },
            { 'label': '18:00', 'value': '18:00' },
            { 'label': '18:30', 'value': '18:30' },
            { 'label': '19:00', 'value': '19:00' },
            { 'label': '19:30', 'value': '19:30' },
            { 'label': '20:00', 'value': '20:00' },
            { 'label': '20:30', 'value': '20:30' },
            { 'label': '21:00', 'value': '21:00' },
            { 'label': '21:30', 'value': '21:30' },
        ],
    });
    @elseif ($date_type == \JJSS::DATE_TYPE['special'])
    $('.timepicker').timepicker({
        'timeFormat': 'H:i',
        'minTime': '22:00',
        'maxTime': '22:00',
        'scrollDefault': '13:00',
        'noneOption': [
            { 'label': '13:00 Ⅰ〜', 'value': '13:00' },
            { 'label': '14:00', 'value': '14:00' },
            { 'label': '15:00 〜Ⅰ', 'value': '15:00' },
            { 'label': '15:10 Ⅱ〜', 'value': '15:10' },
            { 'label': '16:10', 'value': '16:10' },
            { 'label': '17:10 〜Ⅱ', 'value': '17:10' },
            { 'label': '17:20 Ⅲ〜', 'value': '17:20' },
            { 'label': '18:20', 'value': '18:20' },
            { 'label': '19:20 〜Ⅲ', 'value': '19:20' },
            { 'label': '19:30 Ⅳ〜', 'value': '19:30' },
            { 'label': '20:30', 'value': '20:30' },
            { 'label': '21:30 〜Ⅳ', 'value': '21:30' },
        ]
    });
    @endif

    $('#booking').on('click', function() {
        close_modal_any_div();
        $('#background').fadeIn();
    });
    $('#modal-close').on('click', function (){
        close_modal_any_div();
        $('#background').hide();
        $('input[name=change_radio]').prop('checked', false);
    });
    $('#change_tsuika').on('click', function (){
        close_modal_any_div();
        //$('#change_attend_date_div').show();
        //$('#change_time_div').show();
        //$('#change_submit_div').show();
        $('#tsuika_message').show();
    });
    $('#change_furikae').on('click', function (){
        close_modal_any_div();
        $('#change_before_date_div').show();
        $('#change_attend_date_div').show();
        $('#change_time_div').show();
        $('#change_submit_div').show();
        $('#tsuika_message').hide();
    });

    $('#change_submit_div button').on('click', function() {
        var change_type = $('input[name=change_radio]:checked').val();
        switch (change_type) {
            case 'tsuika':
                var student_id = $('#change_student_id').val();
                var before_date = '';
                var attend_date = $('#change_attend_date').val();
                var start_time = $('#change_start_time').val();
                var end_time = $('#change_end_time').val();
                if (student_id == '' ||
                    attend_date == '' ||
                    start_time == '' ||
                    end_time == ''
                   ) {
                    var has_error = true;
                }
                break;
            case 'furikae':
                var student_id = $('#change_student_id').val();
                var before_date = $('#change_before_date').val();
                var attend_date = $('#change_attend_date').val();
                var start_time = $('#change_start_time').val();
                var end_time = $('#change_end_time').val();
                if (student_id == '' ||
                    before_date == '' //||
                    //attend_date == '' ||
                    //start_time == '' ||
                    //end_time == ''
                ) {
                    var has_error = true;
                }
                break;
        }
        if (has_error) {
            var options = {
                text: "正しく入力してください。",
                icon: "error",
                timer: 2000,
                buttons: false,
            };
            swal(options);
            return false;
        }

        $('#form').attr('action', '{{ url("classroom/coming/attend_change/{$school_id}/{$target_date}") }}');
        $('#form').append( $('<input>').attr({type : 'hidden', name : 'student_id', value : student_id}));
        $('#form').append( $('<input>').attr({type : 'hidden', name : 'change_type', value : change_type}));
        $('#form').append( $('<input>').attr({type : 'hidden', name : 'before_date', value : before_date}));
        $('#form').append( $('<input>').attr({type : 'hidden', name : 'attend_date', value : attend_date}));
        $('#form').append( $('<input>').attr({type : 'hidden', name : 'start_time', value : start_time}));
        $('#form').append( $('<input>').attr({type : 'hidden', name : 'end_time', value : end_time}));
        $('#form').submit();
    });

    function close_modal_any_div() {
        $('#change_before_date_div').hide();
        $('#change_attend_date_div').hide();
        $('#change_time_div').hide();
        $('#change_submit_div').hide();
        $('#tsuika_message').hide();
    }

    $(document).on('contextmenu', '.start_time, .end_time', function() {

        var o = $(this);
        var up_koumoku = '';
        if(o.hasClass('start_time')) {
            up_koumoku = 'actual_attend_start_time';
        } else if(o.hasClass('end_time')) {
            up_koumoku = 'actual_attend_end_time';
        }

        var c = document.createElement("input");
        c.type = "text";
        c.style.cssText = 'width : 100px;' + 'height: 40px;' + 'font-size: 200%;';
        c.setAttribute('class', 'value_set_time');

        swal({
            title : '時間変更',
            text : '時間(00:00の形式)を入力してOKボタンを押下ください。',
            content: c,
        })
        .then((value) => {
            if($('.value_set_time').val() == "") {
                return;
            }
            instruction_change(up_koumoku, o, $('.value_set_time').val());
        });

    });

    function instruction_change(koumoku, self, time_value)
    {
        var student_note_id = self.data('note_id');
        $.ajax({
            url: "/api/classroom/coming/instruction/update_something/" + student_note_id,
            type: "PUT",
            data: {
                'koumoku' : koumoku,
                'up_value' : time_value,
            },
            dataType: 'json',
            beforeSend: function() {
                $('.loading').fadeIn();
                self.fadeOut();
            },
        })
        .done( (data, statusText, xhr) => {
            self.html(data[koumoku]);
            self.show();
        })
        .fail( (data) => {
            var options = {
                text: "処理中にエラーが発生しました。",
                icon: "error",
                timer: 2000,
                buttons: false,
            };
            swal(options);
        })
        .always( (data) => {
            $('.loading').fadeOut();
        });
    }

    $('.booking_menu li:first-child').on('click', function() {
        $('.booking_menu li:not(:first-child)').toggle('slow');
    });
});
</script>
@endsection
