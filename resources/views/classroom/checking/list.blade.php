@extends('layouts.default')
@section('content')
<style>
    .set_orderby {cursor:pointer;}
</style>
<section class="scroll_table-wrap wht_bloc">
    <div class="flex flex-j-between flex-a-ctr mgn-btm8">
        <div class="btn_list">
            @php
                $target_school_id = $school_id;
                if(strlen($target_school_id) == 0) {
                    $target_school_id = 9999;
                }
            @endphp
            <button onclick="location.href='{{ url("classroom/coming/{$target_school_id}/{$target_date}") }}'"><i class='bx bx-chevron-left'></i>戻る</button>
        </div>
    </div>
    <h2 class="heading-1">印刷待ち子別ノート一覧</h2>
    <div class="flex flex-j-between flex-a-ctr mgn-btm16">
        <form id="search_form" action="{{ url('classroom/checking') }}" method="get">
            <div class="form-group flex">
                @if(in_array($permission, ['講師', '校長']))
                    <ul>
                        <li>
                            <input type="checkbox" id="tantou" value="1" {{ $search_tantou ? 'checked' : '' }}>
                            <label for="tantou">担当生徒のみ表示</label>
                            <input type="hidden" id="search_tantou" name="search_tantou"/>
                        </li>
                    </ul>
                @endif
                @if(in_array($permission, ['管理者', 'マネージャー', '本部スタッフ']))
                    <select id="school_id" >
                        <option value="">選択してください</option>
                        @foreach ($schools as $school)
                            <option value="{{ $school->id }}"{{ ($school->id == $school_id) ? ' selected' : '' }}>{{ $school->name }}</option>
                        @endforeach
                    </select>
                @endif
                <input type="text" id="search_date" placeholder="日付選択" class="datepicker-here input-sm" data-language="jp"/>
                &nbsp;<span style="margin: auto 0 0 0;">印刷状況：</span>
                <select name="search_printed_flag" id="search_printed_flag" >
                    <option value="">選択してください</option>
                    <option value="0" {{ $search_printed_flag == "0" ? ' selected' : '' }}>未</option>
                    <option value="1" {{ $search_printed_flag == "1" ? ' selected' : '' }}>済</option>
                </select>
                &nbsp;<button id="btn-search">検索<i class='bx bx-search'></i></button>
            </div>

            <input type="hidden" name="orderby_column_name" value="{{ ($orderby['column_name']) ?? '' }}">
            <input type="hidden" name="orderby_type" value="{{ ($orderby['type']) ?? '' }}">
        </form>
    </div>

    <div class="scroll_table mgn-btm32">
        <table class="table-horizon table-striped datatable">
            <thead>
            <tr>
                <th class="th_check">
                    <input type="checkbox" class="check" id="check_all" value="all">
                    <label for="check_all"></label>
                </th>
                <th class="set_orderby" dt-column="students.kana">生徒名</th>
                <th class="txt-ctr">印刷</th>
                <th colspan="2" class="txt-ctr">入退室予定</th>
                <th class="txt-ctr">計画数</th>
                <th colspan="2" class="txt-ctr">操作</th>
                <th style="width:300px;"></th>
            </tr>
            </thead>
            <tbody>
            @if(count($student_notes) == 0)
                <tr>
                    <td></td>
                    <td colspan="7">
                        印刷待ちの子別ノートはありません
                    </td>
                </tr>
            @else
                @foreach ($student_notes as $student_note)
                    <tr style="">
                        <td class="td_check">
                            <input type="checkbox" class="check" data-student_note_id="{{ $student_note->id }}">
                            <label></label>
                        </td>
                        <td class="">{{ $student_note->student->name }}</td>
                        <td class="txt-ctr">{{ ($student_note->printed_flag == '0' ? '未' : '済') }}</td>
                        <td class="td_time td_tilde">{{ \JJSS::displayTime($student_note->plan_attend_start_time) }}</td>
                        <td class="td_time">{{ \JJSS::displayTime($student_note->plan_attend_end_time) }}</td>
                        <td class="txt-ctr">{{ count($student_note->student_note_rows) }}</td>
                        <td class="td_control"><button onclick="location.href='{{ url("classroom/note/{$student_note->student_id}/{$target_date}") }}';"><i class='bx bxs-book-open'></i>開く</button></td>
                        <td class="td_control">
                            <button onclick="window.open('{{ url("classroom/note/print/{$student_note->student_id}/{$target_date}") }}', '_blank')"><i class='bx bxs-printer'></i>印刷</button>
                        {{-- <button class="check_this_note" id="" data-student_note_id="{{ $student_note->id }}"><i class='bx bxs-printer'></i>印刷</button> --}}
                        </td>
                        <td></td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
    <hr>
    <div class="form-group">
        <button type="button" class="check_selected_notes"><i class='bx bxs-printer'></i>選択した子別ノートを印刷する</button>
    </div>
</section>
<form id="form" method="post" action="{{ url('/classroom/note/print') }}" target="_blank">
    {{ csrf_field() }}
    <input type="hidden" id="student_note_ids" name="student_note_ids">
    <input type="hidden" name="school_id" value="{{ $school_id }}">
    <input type="hidden" name="target_date" value="{{ $target_date }}">
</form>
@endsection
@section('script')
<script>
$(function() {
    var target_date = new Date('{{ $target_date }}');
    $('#search_date').datepicker({language: 'jp', autoClose: true}).data('datepicker').selectDate(target_date);

    $('#btn-search').on('click', function(){
        // 担当生徒のみ表示
        if ($('#tantou').length !== 0) {
            if ($('#tantou').prop('checked')) {
                $('#search_tantou').val(1);
            } else {
                $('#search_tantou').val(0);
            }
        }

        var action = $('#search_form').attr('action'),
            school_id = $('#school_id').val(),
            selected_date = $('#search_date').val();

        if (school_id === undefined) {
            school_id = '{{ $school_id }}';
        }

        if (school_id == "") {
            school_id = 9999;
        }

        action += '/' + school_id + '/' + selected_date;
        $('#search_form').attr('action', action);
        $('#search_form').submit();
    });

{{--
    function check_student_note(student_note_id)
    {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "/classroom/note/print",
//          url: "/classroom/checking/check",
            type: 'POST',
            data: {
                'student_note_ids' : student_note_id
            },
        })
        // Ajaxリクエストが成功した場合
        .done(function(data) {
            if(data.f == "ok") {
                for(var i in data.student_note_ids) {
                    $('.check_this_note').each(function() {
                       if(data.student_note_ids[i] == $(this).attr('data-student_note_id')) {
                           $(this).remove();
                       }
                    });
                }
            }
        })
        // Ajaxリクエストが失敗した場合
        .fail(function(data) {
            //エラー
         //   swal(data);
        });
    }
--}}

    $('.check_this_note').on('click', function() {
        var o = $(this);
        check_student_note(o.attr('data-student_note_id'));
    });

    $('.check_selected_notes').on('click', function() {
       var selected_id = [];
       $('.check').each(function() {
          if($(this).prop('checked')) {
              if($(this).attr('data-student_note_id') !== undefined) {
                  selected_id.push($(this).attr('data-student_note_id'));
              }
          }
       });

       if(selected_id.length == 0) {
           var options = {
               text: "対象の子別ノートにチェックしてください。",
               icon: "info",
               timer: 2000,
               buttons: false,
           }
           swal(options)
           return;
       }

       $('#student_note_ids').val(selected_id.join(','));
       $('#form').submit();

        {{-- check_student_note(selected_id.join(',')); --}}

    });

    $('.set_orderby').on('click', function() {
        var current_orderby = $('input[name="orderby_type"]').val();
        var current_column = $('input[name="orderby_column_name"]').val();
        var new_column = $(this).attr('dt-column');

        if (current_column == new_column && current_orderby == "asc") {
            $('input[name="orderby_type"]').val('desc');
        } else {
            $('input[name="orderby_column_name"]').val(new_column);
            $('input[name="orderby_type"]').val('asc');
        }

        $('#btn-search').trigger('click');
    });

});
</script>
@endsection
