@extends('layouts.default')
@section('content')
<section class="scroll_table-wrap wht_bloc">
    <div class="flex flex-j-between flex-a-ctr mgn-btm8">
        <div class="btn_list">
            <button onclick="location.href='{{ url("classroom/coming") }}'"><i class='bx bx-chevron-left'></i>戻る</button>
        </div>
    </div>
    <div>
        <div class="flex flex-j-around flex-a-start{{--flex-a-ctr--}}">
            @php
                $note_name = '前回の';
                $is_old = true;
                $is_last = true;
                $is_using = false;
                $student_note = $last_student_note;
                $is_row_add_button = true;
            @endphp
            @include('note.left')
            @php
                $is_row_add_button = false;
                $note_name = '今回の';
                $is_old = (\Carbon\Carbon::parse($target_date)->lt( Carbon\Carbon::parse(date('Y-m-d')) ));  // 昨日以前ならtrue
                $is_using = false;
                $student_note = $current_student_note;
                $is_show_create_button = true;
            @endphp
            @include('note.left')
        </div>
    </div>
</section>
<form id="form" method="post">
    {{ csrf_field() }}
</form>
@endsection
@include('note.addrow')
@section('script')
@include('note.instruction')
<script>
$(function() {
    $('#create').on('click', function(){
        $('.loading').show();
        $('#form').attr('action', '{{ url("classroom/note/create/{$student_id}/{$target_date}") }}');
        $('#form').submit();
    });
});
</script>
@endsection
