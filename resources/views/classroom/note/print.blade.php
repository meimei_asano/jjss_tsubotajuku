<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
}

table.table-note {
    width: 420px;
    border: 3px solid black;
}
table.table-note tbody tr th {
    border-top: 3px solid black;
}
table.table-note .vlg {
    width: 80px !important;
}
table.table-note .lg {
    width: 71px !important;
}
table.table-note .sm {
    width: 40px !important;
}
table.table-note .mn {
    width: 40px !important;
}
table.table-note th, table.table-note td {
    height: 28px !important;
}
table.table-note .remarks {
    font-size: 10px !important;
   /* height: 80px !important; */
}
table.table-note td textarea.remarks {
    margin-top: 0;
}

table.table-note-right {
    width: 420px;
}
table.table-note-right tbody tr th {
    border-top: 1px solid black;
}
table.table-note-right thead tr td.head-bottom {
    border-bottom: 3px solid black;
}

th, td {
    font-size: 9.5px !important;
}
table.table-note td.inst_time {
    font-size: 9.5px !important;
}
table.table-note td.name {
    font-size: 14px !important;
}

td.glue-top {
    vertical-align: top;
}
td.center {
    width: 40px;
}
</style>

<table>
    <tr>
        <td class="glue-top">
            @php
                $is_old = true;
                $is_using = false;
                $is_row_add_button = false;
                $row_index = 0;
                $is_print = true;
            @endphp
            @include('note.left')
        </td>
        <td class="center"></td>
        <td class="glue-top">
            @include('note.right')
        </td>
    </tr>
</table>
@while (true)
    @php
        $row_index += 12;
        if (! isset($student_note->student_note_rows[$row_index])) break;
    @endphp
<pagebreak>
    <table>
        <tr>
            <td class="glue-top">
                @include('note.left')
            </td>
            <td class="center"></td>
            <td class="glue-top">
                @php
                    $row_index += 12;
                @endphp
                @if (isset($student_note->student_note_rows[$row_index]))
                    @include('note.left')
                @endif
            </td>
        </tr>
    </table>
@endwhile


