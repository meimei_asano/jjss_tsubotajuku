@extends('layouts.default')
@section('content')
<section class="scroll_table-wrap wht_bloc">
    <div class="flex flex-j-between flex-a-ctr mgn-btm8">
        <div class="btn_list">
            <button onclick="location.href='{{ url("classroom/coming") }}'"><i class='bx bx-chevron-left'></i>戻る</button>
        </div>
        <div class="btn_list">
            @if (! $current_student_note->checked_flag && \Carbon\Carbon::parse($target_date)->isToday())
                <button id="recreate"><i class='bx bx-repost'></i>子別ノート再生成</button>
                <button id="check"><i class='bx bx-check-circle'></i>子別ノート承認</button>
            @endif
        </div>
    </div>
    <div>
        <div class="flex flex-j-around flex-a-start{{--flex-a-ctr--}}">
            @php
                $note_name = '前回の';
                $is_old = true;
                $is_last = true;
                $is_using = false;
                $student_note = $last_student_note;
                $is_row_add_button = true;
            @endphp
            @include('note.left')
            @php
                $is_row_add_button = false;
                $note_name = '今回の';
                $is_old = (\Carbon\Carbon::parse($target_date)->lt( Carbon\Carbon::parse(date('Y-m-d')) ));  // 昨日以前ならtrue
                $is_using = false;
                $is_row_add_button = true;
                $student_note = $current_student_note;
            @endphp
            @include('note.left')
        </div>
    </div>
</section>
<form id="form" method="post">
    {{ csrf_field() }}
</form>
@endsection
@include('note.addrow')
@section('script')
@include('note.instruction')
<script>
$(function() {
    $('#recreate').on('click', function(){
        $('.loading').show();
        $('#form').attr('action', '{{ url("classroom/note/recreate/{$student_note->id}") }}');
        $('#form').submit();
    });

    $('#check').on('click', function(){
        $('.loading').show();
        $('#form').attr('action', '{{ url("classroom/note/check/{$student_note->id}") }}');
        $('#form').submit();
    });
});
</script>
@endsection
