@extends('layouts.default')
<link href="{{ url('css/jquery-ui.min.css') }}" rel="stylesheet">
@section('content')
<section class="scroll_table-wrap wht_bloc">
    <div class="flex flex-j-between flex-a-ctr mgn-btm8">
        <div class="btn_list">
            <button onclick="location.href='{{ url("classroom/coming") }}'"><i class='bx bx-chevron-left'></i>戻る</button>
        </div>
        <div class="btn_list">
            <button onclick="window.open('{{ url("classroom/note/print/{$student_id}/{$target_date}") }}', '_blank')"><i class='bx bxs-printer'></i>印刷</button>
        </div>
    </div>
    <div class="flex flex-a-start flex-j-start">
        @php
            $is_old = false;
            $is_using = true;
            $is_row_add_button = true;
        @endphp
        @include('note.left')
        @include('pif.detail')
    </div>
</section>
<form id="form" method="post">
    {{ csrf_field() }}
</form>
@endsection
@include('note.addrow')
@section('script')
@include('note.instruction')
<script src="{{ asset('js/jquery-ui.min.js') }}" defer></script>
<script src="{{ asset('js/pif.js') }}" defer></script>
@endsection
