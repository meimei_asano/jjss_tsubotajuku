@extends('layouts.default')
@section('content')
<style>
    .booking_menu li:first-child { cursor : pointer; }
    .booking_menu li:not(:first-child) { display : none; }
    .set_orderby {cursor:pointer;}
</style>
<section class="scroll_table-wrap wht_bloc">
    <h2 class="heading-1">追加コマ・振替リスト</h2>
    <div class="flex flex-j-between flex-a-ctr mgn-btm16">
        <form id="search_form" action="{{ url('classroom/changing') }}" method="get">
            <div class="flex">
                <div class="form-group">
                    <ul>
                        @if(in_array($permission, ['講師', '校長']))
                        <li>
                            <input type="checkbox" id="search_tantou" name="search_tantou" value="1" {{ (isset($search['tantou']) === true && $search['tantou'] == "1") ? 'checked' : '' }}>
                            <label for="search_tantou">担当生徒のみ表示</label>
                        </li>
                        @endif
                        <li>
                            <input type="checkbox" id="search_before_today" name="search_before_today" value="1" {{ (isset($search['before_today']) === true && $search['before_today'] == "1") ? 'checked' : '' }}>
                            <label for="search_before_today">今日より前のデータを表示</label>
                        </li>
                    </ul>
                </div>

                @if(in_array($permission, ['管理者', '本部スタッフ']))
                    <div class="form-group mgn-lft16">
                        <select id="search_school_id" name="search_school_id" class="mgn-lft8">
                            <option value="">校舎を選択してください</option>
                            @foreach ($schools as $school)
                                <option value="{{ $school->id }}"{{ ($school->id == $search_school_id) ? ' selected' : '' }}>{{ $school->name }}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
                <div class="form-group mgn-lft16">
                    <input type="text" id="search_name" name="search_name" placeholder="生徒名" value="{{ $search['name'] or null }}">
                </div>
                <button id="btn-search" class="mgn-lft16" >検索<i class='bx bx-search'></i></button>
            </div>

            <input type="hidden" name="orderby_column_name" value="{{ ($orderby['column_name']) ?? '' }}">
            <input type="hidden" name="orderby_type" value="{{ ($orderby['type']) ?? '' }}">
        </form>
    </div>

    <div class="scroll_table mgn-btm16">
        <table class="table-horizon table-striped datatable">
            <thead>
            <tr>
                <th class="set_orderby" dt-column="students.school_id" >校舎</th>
                <th class="set_orderby" dt-column="students.kana" >生徒名</th>
                <th class="set_orderby" dt-column="teachers.kana" >担当講師</th>
                <th class="txt-ctr set_orderby" dt-column="change_type">種別</th>
                <th class="txt-ctr set_orderby" dt-column="before_date">前日付</th>
                <th class="txt-ctr set_orderby" dt-column="attend_date">後日付</th>
                <th colspan="2" class="txt-ctr set_orderby" dt-column="start_time">時間</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @if(count($list) == 0)
                <tr>
                    <td colspan="8">
                        登録されていません
                    </td>
                </tr>
            @else
                @foreach ($list as $data)
                    <tr style="">
                        <td class="">{{ $data->student->school->name }}</td>
                        <td class="">{{ $data->student->name }}</td>
                        <td class="">{{ $data->student->teacher->name or null }}</td>
                        <td class="">{{ \JJSS::displayAttendChangeType($data->change_type) }}</td>
                        <td class="">{{ $data->before_date }}</td>
                        <td class="">{{ $data->attend_date }}</td>
                        <td class="td_tilde txt-ctr">
                            {{ \JJSS::displayTime($data->start_time) }}
                        </td>
                        <td>
                            {{ \JJSS::displayTime($data->end_time) }}
                        </td>
                        <td class="" style="width:400px;"></td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>

    <div class="flex flex-j-between">
        <div class="pager txt-ctr">
            {{ $list->appends(array('search_name' => (isset($search['name'])===true ? $search['name'] : ''),
                                        'search_school_id' => $search_school_id,
                                        'search_before_today' => (isset($search['before_today']) === true ? $search['before_today'] : ''),
                                        'search_tantou' => (isset($search['tantou']) === true ? $search['tantou'] : ''),
                                        'orderby_column_name' => (isset($orderby['column_name'])===true ? $orderby['column_name'] : ''),
                                        'orderby_type' => (isset($orderby['type'])===true ? $orderby['type'] : '')
                                        ))->links() }}
        </div>
    </div>
</section>
@endsection
@section('script')
<script>
$(function() {

    $('#btn-search').on('click', function() {
        $('#search_form').submit();
    });

    $('.set_orderby').on('click', function() {
        var current_orderby = $('input[name="orderby_type"]').val();
        var current_column = $('input[name="orderby_column_name"]').val();
        var new_column = $(this).attr('dt-column');

        if (current_column == new_column && current_orderby == "asc") {
            $('input[name="orderby_type"]').val('desc');
        } else {
            $('input[name="orderby_column_name"]').val(new_column);
            $('input[name="orderby_type"]').val('asc');
        }

        $('#search_form').submit();
    });

});
</script>
@endsection
