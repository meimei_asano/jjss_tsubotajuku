@extends('layouts.default')

@section('content')
    <!-- ▼ section コピー用 ▼ -->
    <section class="wht_bloc">
      <h2 class="heading-1">大見出しのスタイル</h2>
      <h3 class="heading-2">中見出しのスタイル</h3>
      <p class="bold">太字</p>
      <p>デフォルトのテキスト</p>
    </section>
    <!-- ▲ section コピー用 ▲ -->
    <section class="wht_bloc">
      <h2 class="heading-1">フィルター</h2>
      <div class="flex mgn-btm16">
        <dl class="filter"> <dt>メンバー</dt>
          <dd> <input type="radio" name="member" id="member1" checked=""> <label for="member1">自分</label> </dd>
          <dd> <input type="radio" name="member" id="member2"> <label for="member2">全員</label> </dd>
        </dl>
        <dl class="filter"> <dt>集計単位</dt>
          <dd> <input type="radio" name="shukei" id="shukei1" checked=""> <label for="shukei1">案件</label> </dd>
          <dd> <input type="radio" name="shukei" id="shukei2"> <label for="shukei2">顧客</label> </dd>
        </dl>
      </div>
      <div class="flex flex-c-wrap">
        <!-- ▼ filter コピー用（ラジオボタンver） ▼ -->
        <dl class="filter"> <dt>ラジオボタンで判定</dt>
          <dd> <input type="radio" name="filter_radio" id="filter_radio1" checked=""> <label for="filter_radio1">ラジオ1</label> </dd>
          <dd> <input type="radio" name="filter_radio" id="filter_radio2"> <label for="filter_radio2">ラジオ2</label> </dd>
          <dd> <input type="radio" name="filter_radio" id="filter_radio3"> <label for="filter_radio3">ラジオ3</label> </dd>
        </dl>
        <!-- ▲ filter コピー用（ラジオボタンver） ▲ -->
        <!-- ▼ filter コピー用（チェックボックスver） ▼ -->
        <dl class="filter"> <dt>チェックボックスで判定</dt>
          <dd> <input type="checkbox" name="filter_check" id="filter_check1" checked=""> <label for="filter_check1">チェック1</label> </dd>
          <dd> <input type="checkbox" name="filter_check" id="filter_check2"> <label for="filter_check2">チェック2</label> </dd>
          <dd> <input type="checkbox" name="filter_check" id="filter_check3"> <label for="filter_check3">チェック3</label> </dd>
        </dl>
        <!-- ▲ filter コピー用（チェックボックスver） ▲ -->
      </div>
    </section>
    <section class="wht_bloc">
      <h2 class="heading-1">ボタン</h2>
      <h3 class="heading-2">スタイルバリエーション</h3>
      <div class="btn_list"> <button>ボタンサンプル（デフォルト）</button> <button class="btn-thin">ボタンサンプル（薄）</button> <button class="btn-line">ボタンサンプル（ライン）</button> <button class="btn-flat">ボタンサンプル（フラット）</button> </div>
      <h3 class="heading-2">サイズバリエーション</h3>
      <div class="btn_list"> <button class="btn-lg">ボタンサンプル（大）</button> <button>ボタンサンプル（中）</button> <button class="btn-sm">ボタンサンプル（小）</button> </div>
      <h3 class="heading-2">カラーバリエーション</h3>
      <div class="btn_list"> <button>ボタンサンプル</button> <button class="btn-info">ボタンサンプル（info）</button> <button class="btn-warning">ボタンサンプル（warning）</button> <button class="btn-danger">ボタンサンプル（danger）</button> </div>
      <h3 class="heading-2">無効</h3>
      <div class="btn_list"> <button class="is_disabled">ボタンサンプル</button> <button class="btn-info is_disabled">ボタンサンプル</button> <button class="btn-warning is_disabled">ボタンサンプル</button> <button class="btn-danger is_disabled">ボタンサンプル</button> </div>
      <h3 class="heading-2">ページング</h3>
      <div class="pager"> <span id="pager_start">最初</span><span id="pager_before"><i class="bx bx-chevron-left"></i></span><span id="pager_1" class="page active" data-page="1">1</span><span id="pager_next"><i class="bx bx-chevron-right"></i></span><span id="pager_end">最後</span> </div>
    </section>
    <section class="wht_bloc">
      <h2 class="heading-1">フォーム</h2>
      <!-- ▼一行テキスト コピー用▼ -->
      <div class="form-group"> <label>一行テキスト（input）</label> <input type="text" name="#" placeholder="min-width 240px"> </div>
      <!-- ▲一行テキスト コピー用▲ -->
      <!-- ▼input幅広め コピー用▼ -->
      <div class="form-group"> <label>input幅広め（input-lg）</label> <input type="text" name="#" placeholder="min-width 420px" class="input-lg"> </div>
      <!-- ▲input幅広め コピー用▲ -->
      <!-- ▼input幅最大 コピー用▼ -->
      <div class="form-group"> <label>input幅最大（input-full）</label> <input type="text" name="#" placeholder="min-width 100%" class="input-full"> </div>
      <!-- ▲input幅最大 コピー用▲ -->
      <!-- ▼テキストエリア コピー用▼ -->
      <div class="form-group"> <label>テキストエリア（textarea）</label> <textarea name="#" placeholder="基本的には幅100%"></textarea> </div>
      <!-- ▲テキストエリア コピー用▲ -->
      <!-- ▼セレクトボックス コピー用▼ -->
      <div class="form-group"> <label>セレクトボックス（select）</label> <select name="#">
          <option value="" checked="">選択してください</option>
          <option value="#">オプション1</option>
          <option value="#">オプション2</option>
          <option value="#">オプション3</option>
        </select> </div>
      <!-- ▲セレクトボックス コピー用▲ -->
      <!-- ▼横並びラジオボタン コピー用▼ -->
      <div class="form-group">
        <p>横並びラジオボタン（radio）</p>
        <ul class="flex">
          <li> <input type="radio" name="radio_btn" id="radio_btn1" checked=""> <label for="radio_btn1">ラジオ1</label> </li>
          <li> <input type="radio" name="radio_btn" id="radio_btn2"> <label for="radio_btn2">ラジオ2</label> </li>
          <li> <input type="radio" name="radio_btn" id="radio_btn3"> <label for="radio_btn3">ラジオ3</label> </li>
        </ul>
      </div>
      <!-- ▲横並びラジオボタン コピー用▲ -->
      <!-- ▼縦並びラジオボタン コピー用▼ -->
      <div class="form-group">
        <p>縦並びラジオボタン（radio）</p>
        <ul>
          <li> <input type="radio" name="radio_btn-vertical" id="radio_btn-vertical1" checked=""> <label for="radio_btn-vertical1">ラジオ1</label> </li>
          <li> <input type="radio" name="radio_btn-vertical" id="radio_btn-vertical2"> <label for="radio_btn-vertical2">ラジオ2</label> </li>
          <li> <input type="radio" name="radio_btn-vertical" id="radio_btn-vertical3"> <label for="radio_btn-vertical3">ラジオ3</label> </li>
        </ul>
      </div>
      <!-- ▲縦並びラジオボタン コピー用▲ -->
      <!-- ▼横並びチェックボックス コピー用▼ -->
      <div class="form-group">
        <p>横並びチェックボックス（checkbox）</p>
        <ul class="flex">
          <li> <input type="checkbox" name="checkbox_btn" id="checkbox_btn1"> <label for="checkbox_btn1">チェックボックス1</label> </li>
          <li> <input type="checkbox" name="checkbox_btn" id="checkbox_btn2"> <label for="checkbox_btn2">チェックボックス2</label> </li>
          <li> <input type="checkbox" name="checkbox_btn" id="checkbox_btn3"> <label for="checkbox_btn3">チェックボックス3</label> </li>
        </ul>
      </div>
      <!-- ▲横並びチェックボックス コピー用▲ -->
      <!-- ▼縦並びチェックボックス コピー用▼ -->
      <div class="form-group">
        <p>縦並びチェックボックス（checkbox）</p>
        <ul>
          <li> <input type="checkbox" name="checkbox_btn-vertical" id="checkbox_btn1-vertical"> <label for="checkbox_btn1-vertical">チェックボックス1</label> </li>
          <li> <input type="checkbox" name="checkbox_btn-vertical" id="checkbox_btn2-vertical"> <label for="checkbox_btn2-vertical">チェックボックス2</label> </li>
          <li> <input type="checkbox" name="checkbox_btn-vertical" id="checkbox_btn3-vertical"> <label for="checkbox_btn3-vertical">チェックボックス3</label> </li>
        </ul>
      </div>
      <!-- ▲縦並びチェックボックス コピー用▲ -->
      <!-- ▼スイッチボタン コピー用▼ -->
      <div class="form-group">
        <p>スイッチボタン（switch）</p> <label class="switch__label">
          <input type="checkbox" name="#" class="input-switch">
          <span class="switch__content"></span>
          <span class="switch__circle"></span>
        </label> </div>
      <!-- ▲スイッチボタン コピー用▲ -->
      <!-- ▼電話番号 コピー用▼ -->
      <div class="form-group"> <label>電話番号</label>
        <div class="input-num"> <input type="tel" name="#" size="4" maxlength="4">-<input type="tel" name="#" size="4" maxlength="4">-<input type="tel" name="#" size="4" maxlength="4"> </div>
      </div>
      <!-- ▲電話番号 コピー用▲ -->
      <!-- ▼郵便番号 コピー用▼ -->
      <div class="form-group"> <label>郵便番号</label>
        <div class="input-num"> <input type="text" name="#" size="3" maxlength="4">-<input type="text" name="#" size="4" maxlength="4"> </div>
      </div>
      <!-- ▲郵便番号 コピー用▲ -->
      <!-- ▼カレンダー コピー用▼ -->
      <div class="form-group"> <label>カレンダー</label> <input type="text" class="datepicker-here" data-language="jp"> </div>
      <!-- ▲カレンダー コピー用▲ -->
    </section>
    <section class="wht_bloc">
      <h2 class="heading-1">テーブル（横向き）</h2>
      <h3 class="heading-2">デフォルト（table-horizon）</h3>
      <table class="table-horizon">
        <thead>
        <tr>
          <th>ヘッダーテキスト</th>
          <th>ヘッダーテキスト</th>
          <th>ヘッダーテキスト</th>
          <th>ヘッダーテキスト</th>
          <th>ヘッダーテキスト</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
        </tr>
        </tbody>
      </table>
      <h3 class="heading-2">ストライプテーブル（table-striped）</h3>
      <table class="table-horizon table-striped">
        <thead>
        <tr>
          <th>ヘッダーテキスト</th>
          <th>ヘッダーテキスト</th>
          <th>ヘッダーテキスト</th>
          <th>ヘッダーテキスト</th>
          <th>ヘッダーテキスト</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
          <td>サンプルテキスト</td>
        </tr>
        </tbody>
      </table>
      <h3 class="heading-2">ミニテーブル（table-sm）情報量が多い時などに使用</h3>
      <div class="scroll_table">
        <table class="table-horizon table-sm table-striped">
          <thead>
          <tr>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
          </tr>
          <tr>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
          </tr>
          <tr>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
          </tr>
          <tr>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
          </tr>
          <tr>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
          </tr>
          <tr>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
          </tr>
          </tbody>
        </table>
      </div>
      <h3 class="heading-2">ホバー解除（no_hov）</h3>
      <div class="scroll_table">
        <table class="table-horizon table-sm table-striped no_hov">
          <thead>
          <tr>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
          </tr>
          <tr>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
          </tr>
          <tr>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
          </tr>
          <tr>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
          </tr>
          <tr>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
          </tr>
          <tr>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
          </tr>
          </tbody>
        </table>
      </div>
      <h3 class="heading-2">スタイル削除（no_style）</h3>
      <div class="scroll_table">
        <table class="table-horizon table-sm table-striped no_hov no_style">
          <thead>
          <tr>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
            <th>ヘッダーテキスト</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
          </tr>
          <tr>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
          </tr>
          <tr>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
          </tr>
          <tr>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
          </tr>
          <tr>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
          </tr>
          <tr>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
            <td>サンプルテキスト</td>
          </tr>
          </tbody>
        </table>
      </div>
    </section>
    <section class="wht_bloc">
      <h2 class="heading-1">テーブル（縦向き）</h2>
      <h3 class="heading-2">デフォルト（table-vertical）</h3>
      <table class="table-vertical">
        <tbody>
        <tr>
          <th>ヘッダーテキスト</th>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <th>ヘッダーテキスト</th>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <th>ヘッダーテキスト</th>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <th>ヘッダーテキスト</th>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <th>ヘッダーテキスト</th>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <th>ヘッダーテキスト</th>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <th>ヘッダーテキスト</th>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <th>ヘッダーテキスト</th>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <th>ヘッダーテキスト</th>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <th>ヘッダーテキスト</th>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <th>ヘッダーテキスト</th>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <th>ヘッダーテキスト</th>
          <td>サンプルテキスト</td>
        </tr>
        <tr>
          <th>ヘッダーテキスト</th>
          <td>サンプルテキスト</td>
        </tr>
        </tbody>
      </table>
      <h3 class="heading-2">入力例</h3>
      <table class="table-vertical">
        <tbody>
        <tr>
          <th>案件No</th>
          <td>101</td>
        </tr>
        <tr>
          <th>顧客</th>
          <td>
            <div class="form-group"> <select name="kokyaku">
                <option value="" checked="">選択してください</option>
                <option value="kokyaku1">オプション1</option>
                <option value="kokyaku2">オプション2</option>
                <option value="kokyaku3">オプション3</option>
              </select> </div>
          </td>
        </tr>
        <tr>
          <th>案件名</th>
          <td>
            <div class="form-group"> <input type="text" name="#" placeholder="案件名を入力してください" class="input-lg"> </div>
          </td>
        </tr>
        <tr>
          <th>案件区分</th>
          <td>
            <div class="form-group"> <select name="kubun">
                <option value="" checked="">選択してください</option>
                <option value="kokyaku1">オプション1</option>
                <option value="kokyaku2">オプション2</option>
                <option value="kokyaku3">オプション3</option>
              </select> </div>
          </td>
        </tr>
        <tr>
          <th>見積日</th>
          <td>
            <div class="form-group"> <input type="text" class="datepicker-here" data-language="jp"> </div>
          </td>
        </tr>
        <tr>
          <th>検収日</th>
          <td>
            <div class="form-group"> <input type="text" class="datepicker-here" data-language="jp"> </div>
          </td>
        </tr>
        <tr>
          <th>受注状況</th>
          <td>
            <dl class="filter">
              <dd> <input type="radio" name="juchu" id="juchu1" checked=""> <label for="juchu1">見積もり中</label> </dd>
              <dd> <input type="radio" name="juchu" id="juchu2"> <label for="juchu2">受注確定</label> </dd>
              <dd> <input type="radio" name="juchu" id="juchu3"> <label for="juchu3">失注</label> </dd>
            </dl>
          </td>
        </tr>
        <tr>
          <th>進捗状況</th>
          <td>
            <dl class="filter">
              <dd> <input type="radio" name="sintyoku" id="sintyoku1" checked=""> <label for="sintyoku1">未着手</label> </dd>
              <dd> <input type="radio" name="sintyoku" id="sintyoku2"> <label for="sintyoku2">納品済</label> </dd>
              <dd> <input type="radio" name="sintyoku" id="sintyoku3"> <label for="sintyoku3">検収済</label> </dd>
            </dl>
          </td>
        </tr>
        <tr>
          <th>請求状況</th>
          <td>
            <dl class="filter">
              <dd> <input type="radio" name="seikyu" id="seikyu1" checked=""> <label for="seikyu1">未着手</label> </dd>
              <dd> <input type="radio" name="seikyu" id="seikyu2"> <label for="seikyu2">請求済</label> </dd>
              <dd> <input type="radio" name="seikyu" id="seikyu3"> <label for="seikyu3">入金確認</label> </dd>
            </dl>
          </td>
        </tr>
        <tr>
          <th>請求タイミング</th>
          <td>
            <dl class="filter">
              <dd> <input type="radio" name="timing" id="timing1" checked=""> <label for="timing1">一括請求</label> </dd>
              <dd> <input type="radio" name="timing" id="timing2"> <label for="timing2">分割請求</label> </dd>
              <dd> <input type="radio" name="timing" id="timing3"> <label for="timing3">定期請求</label> </dd>
            </dl>
          </td>
        </tr>
        <tr>
          <th>請求情報</th>
          <td>
            <div class="form-group"> <label>請求月</label> <input type="text" class="datepicker-here" data-language="jp"> </div>
            <div class="form-group"> <label>計上日</label> <input type="text" class="datepicker-here" data-language="jp"> </div>
            <div class="form-group"> <label>請求額<small>（税抜）</small></label> <input type="text" name="#"> </div>
            <div class="form-group"> <label>消費税</label> <input type="text" name="#"> </div>
          </td>
        </tr>
        <tr>
          <th>支払方法</th>
          <td>
            <dl class="filter">
              <dd> <input type="radio" name="shiharai" id="shiharai1" checked=""> <label for="shiharai1">銀行振込</label> </dd>
              <dd> <input type="radio" name="shiharai" id="shiharai2"> <label for="shiharai2">口座振替</label> </dd>
              <dd> <input type="radio" name="shiharai" id="shiharai3"> <label for="shiharai3">クレジットカード</label> </dd>
            </dl>
          </td>
        </tr>
        <tr>
          <th>社内メモ</th>
          <td>
            <div class="form-group"> <textarea name="#"></textarea> </div>
          </td>
        </tr>
        </tbody>
      </table>
    </section>
    <section class="wht_bloc">
      <h2 class="heading-1">アラート</h2>
      <h3 class="heading-2">ページ内に表示</h3>
      <div class="alert alert-success flex flex-a-ctr"> <i class="bx bx-check-circle"></i>
        <p>成功の文言が入ります。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。</p>
      </div>
      <div class="alert alert-help flex flex-a-ctr"> <i class="bx bx-help-circle"></i>
        <p>情報の文言が入ります。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。</p>
      </div>
      <div class="alert alert-warning flex flex-a-ctr"> <i class="bx bx-info-circle"></i>
        <p>注意の文言が入ります。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。</p>
      </div>
      <div class="alert alert-danger flex flex-a-ctr"> <i class="bx bx-x-circle"></i>
        <p>エラーの文言が入ります。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。</p>
      </div>
      <h3 class="heading-2">ポップアップで表示<small>（スタイルは例なので、プラグイン使用の場合は適宜変更してください。あくまで参考に…）</small></h3>
      <div class="mgn-btm8"> <button class="btn-popup btn-popup-success">成功のアラート</button> <button class="btn-popup btn-popup-success-timer">成功のアラート（2秒で消える）</button> </div>
      <div class="mgn-btm8"> <button class="btn-danger btn-popup btn-popup-error">エラーのアラート</button> <button class="btn-danger btn-popup btn-popup-error-timer">エラーのアラート（2秒で消える）</button> </div>
      <div class=""> <button class="btn-info btn-popup btn-popup-confirm">確認アラート（OK / キャンセル）</button> </div>
    </section>
@endsection

@section('script')
<script>
  $(function(){
    $('select').select2({ language: "ja"});

/*
    $('.gnav--sub_menu:not(.current)').on({
      'mouseenter' : function(){
        $(this).addClass('is_active');
        $(this).find('.gnav--child').slideDown(100);
      },
      'mouseleave' : function(){
        $(this).removeClass('is_active');
        $(this).find('.gnav--child').slideUp(100);
      }
    });

    // ドロワーメニュー
    $('.menu-trigger_open').on('click',function(){
      $(this).addClass('active');
      $('.main_contents').addClass('open');
      $('aside').addClass('open');
      $('.overlay').addClass('open');
    });
    $('.menu-trigger_close').on('click',function(){
      $('.menu-trigger_open').removeClass('active');
      $('.main_contents').removeClass('open');
      $('aside').removeClass('open');
      $('.overlay').removeClass('open');
    });
    // $('.overlay').on('click',function(){
    //   if($(this).hasClass('open')){
    //     $(this).removeClass('open');
    //     $('.menu-trigger').removeClass('active');
    //     $('.main_contents').removeClass('open');
    //     $('aside').removeClass('open');
    //   }
    // });
*/

    $(document).on('click', '.btn-popup-success', function(){
      // 成功（ボタンあり）
      var options = {
        text: "成功",
        icon: "success",
      }
      swal(options)
    });

    $(document).on('click', '.btn-popup-success-timer', function(){
      // 成功（ボタンなし&2秒で自動非表示）
      var options = {
        text: "成功",
        icon: "success",
        timer: 2000,
        buttons: false,
      }
      swal(options)
    });

    $(document).on('click', '.btn-popup-error', function(){
      // エラー（ボタンあり）
      var options = {
        text: "エラー",
        icon: "error",
      }
      swal(options)
    });

    $(document).on('click', '.btn-popup-error-timer', function(){
      // エラー（ボタンなし&2秒で自動非表示）
      var options = {
        text: "エラー",
        icon: "error",
        timer: 2000,
        buttons: false,
      }
      swal(options)
    });

    $(document).on('click', '.btn-popup-confirm', function(){
      // 確認アラート
      var options = {
        text: "本当に削除しますか？",
        icon: "info",
        dangerMode: true,
        buttons: {
          cancel: "キャンセル",
          ok: "OK",
        }
      };
      swal(options)
              .then(function(val) {
                if (val) {
                  // OKの時の処理
                  swal("OKボタンが押されました");
                }
              });
    });
  });
</script>
@endsection
