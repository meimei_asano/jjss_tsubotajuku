<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>自学自習サポートシステム</title>

  <!-- Icons -->
  <link href='//cdn.jsdelivr.net/npm/boxicons@1.9.3/css/boxicons.min.css' rel='stylesheet'>

  <!-- Styles -->
  <link href="{{ asset('css/reset.css') }}" rel="stylesheet">
  <link href="{{ asset('css/datepicker.min.css') }}" rel="stylesheet" >
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <link href="{{ asset('css/main.css') }}" rel="stylesheet" >

</head>
<body class="default">

  <!-- start main -->
  <main class="auth-pane">
    @if(session()->has('message'))
      <div class="alert alert-info mb-3">
        {{session('message')}}
      </div>
    @endif
    @if (session()->has('alert.success'))
      <div class="alert alert-success">
        {!! session('alert.success') !!}
      </div>
    @endif

    <div class="">
      <image src="{{ asset('images/title.png') }}" style="position: relative; margin: 0px 18px;">
      @yield('content')
    </div>
  </main>
  <!-- end main -->

  <!-- start script -->
  <script src="//code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js"></script>
  @yield('script')
  <!-- end footer -->

</body>
</html>
