<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>自学自習サポートシステム</title>

  <!-- Icons -->
  <link href='//cdn.jsdelivr.net/npm/boxicons@1.9.3/css/boxicons.min.css' rel='stylesheet'>

  <!-- Styles -->
  <link href="{{ asset('css/reset.css') }}" rel="stylesheet">
  <link href="{{ asset('css/datepicker.min.css') }}" rel="stylesheet" >
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <link href="{{ asset('css/main_old.css') }}" rel="stylesheet" >

</head>
<body class="default">

<!-- start header -->
<header class="header flex flex-a-ctr flex-j-between">
  <div class="header--lft flex flex-a-ctr">
    <h1 class="header--logo"><a href="{{ url('') }}"><image src="{{ asset('images/title.png') }}"></a></h1>
    <ul class="header--lft--nav flex bold" style="display: none;">
      <li class="ts4"><a href="#"><i class='bx bx-calendar'></i>予約確認</a></li>
    </ul>
  </div>
  <div class="header--rgt flex flex-a-ctr">
    <div class="account_info pos_rel flex flex-a-ctr">
      <div>
        {{--
                  @if (Auth::check())
                    <a class="account_info--user" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                      <!-- 画像があれば -->
                      <span class="account_info--user_icon"><i class='bx bxs-user'></i></span>
                      <!-- 画像がなければ -->
                      <!-- <span class="account_info--user_icon" style="background-image: url(★画像パス★):"></span> -->
                      <span class="name">{{ $login_user_name }}</span>
                      <i class='bx bx-chevron-down'></i>
                    </a>
                    <ul class="account_info--nav header--subnav ts4">
                      <li>
                        <a href="{{ url('mypage/profile') }}"><i class='bx bxs-user'></i>個人設定</a>
                      </li>
                      <li>
                        <a href="{{ url('member') }}"><i class='bx bxs-group'></i>メンバー管理</a>
                      </li>
                      <li>
                        <a href="{{ route('logout') }}"><i class='bx bx-log-out'></i>ログアウト</a>
                      </li>
                    </ul>
                  @else
                    <p>※ログインしていません。（<a href="/login">ログイン</a>｜
                    <a href="/register">登録</a>）</p>
                  @endif
        --}}

        <a class="account_info--user" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <span class="account_info--user_icon"><i class='bx bxs-user'></i></span>
          <span class="name">ユーザー名</span>
          <i class='bx bx-chevron-down'></i>
        </a>
        <ul class="account_info--nav header--subnav ts4">
          <li>
            <a href="{{ url('mypage/password') }}"><i class='bx bxs-user'></i>パスワード変更</a>
          </li>
          <li>
            <a href=""><i class='bx bx-log-out'></i>ログアウト</a>
          </li>
        </ul>

      </div>
    </div>
  </div>
</header>
<!-- end header -->

<!-- start main -->
<main>
  @if(session()->has('message'))
    <div class="alert alert-info mb-3">
      {{session('message')}}
    </div>
  @endif
  @if (session()->has('alert.success'))
    <div class="alert alert-success">
      {!! session('alert.success') !!}
    </div>
  @endif
  @yield('content')
</main>
<!-- end main -->

<!-- start aside -->
<aside style="width: 16px; background-color: navajowhite; border-right: 1px #FFCC80 solid;">
  <div style="margin-top: 300px; margin-bottom: auto;">
    <i class='bx bx-last-page'></i>
  </div>
</aside>
<aside style="display: none;">
  @if(Request::is('member','member/*'))
    @include('layouts.admin_menu')
  @else
    @include('layouts.menu')
  @endif
</aside>
<!-- end aside -->

<!-- start script -->
<script src="//code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/i18n/ja.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js"></script>
<script src="{{ asset('js/sweetalert.min.js') }}" defer></script>
<script src="{{ asset('js/datepicker.min.js') }}" defer></script>
<script src="{{ asset('js/datepicker.jp.js') }}" defer></script>
<script src="{{ asset('js/main.js') }}" defer></script>
@yield('script')
<!-- end footer -->

</body>
</html>
