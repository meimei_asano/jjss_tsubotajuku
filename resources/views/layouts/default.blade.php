<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>自学自習サポートシステム</title>

  <!-- Icons -->
  <link href='//cdn.jsdelivr.net/npm/boxicons@1.9.3/css/boxicons.min.css' rel='stylesheet'>

  <!-- Styles -->
  <link href="{{ asset('css/reset.css') }}" rel="stylesheet">
  <link href="{{ asset('css/datepicker.min.css') }}" rel="stylesheet" >
  <link href="{{ asset('css/timepicker.min.css') }}" rel="stylesheet" >
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <link href="{{ asset('css/main.css') }}" rel="stylesheet" >

</head>
<body class="default">
  <div class="loading" style="display: none;"><i class='bx bx-loader-alt'></i></div>
  <!-- start header -->
  <header class="header flex flex-a-ctr flex-j-between">
    <div class="header--lft flex flex-a-ctr">
      <h1 class="header--logo"><a href="{{ url('') }}"><image src="{{ asset('images/title.png') }}"></a></h1>
      <ul class="header--lft--nav flex bold" style="display: none;">
        <li class="ts4"><a href="#"><i class='bx bx-calendar'></i>予約確認</a></li>
      </ul>
    </div>
    <div class="header--rgt flex flex-a-ctr">
      <div class="account_info pos_rel flex flex-a-ctr">
        <div>
          @if (Auth::check())
            <a class="account_info--user" href="#" role="button" aria-haspopup="true" aria-expanded="false">
              <span class="account_info--user_icon"><i class='bx bxs-user'></i></span>
              <span class="name">{{ $login_name }}</span>
              <i class='bx bx-chevron-down'></i>
            </a>
            <ul class="account_info--nav header--subnav ts4">
              <li>
                <a href="{{ url('mypage/password') }}"><i class='bx bxs-user'></i>パスワード変更</a>
              </li>
              <li>
                <a href="{{ route('logout') }}"><i class='bx bx-log-out'></i>ログアウト</a>
              </li>
            </ul>
          @else
            <p><a href="{{ route('login') }}">ログイン</a></p>
          @endif
        </div>
      </div>
    </div>
  </header>
  <!-- end header -->

  <!-- start main -->
  <main>
    <div class="main_contents pos_rel">
      <div class="message_background">
        @if(session()->has('alert.message'))
            <div class="alert alert-info flex flex-a-ctr">
                <i class="bx bx-info-circle"></i>
                <p>
                    {!! session('alert.message') !!}
                </p>
            </div>
        @endif
        @if (session()->has('alert.success'))
            <div class="alert alert-success flex flex-a-ctr">
                <i class="bx bx-check-circle"></i>
                <p>
                    {!! session('alert.success') !!}
                </p>
            </div>
        @endif
        @if (isset($errors) && count($errors) > 0)
            <div class="alert alert-danger flex flex-a-ctr">
                <i class="bx bx-x-circle"></i>
                <p>
                    {!! implode('<br>', $errors->all()) !!}
                </p>
            </div>
        @endif
      </div>
@yield('content')
    </div>
    <!-- start aside -->
    <div class="menu-trigger menu-trigger_open flex flex-a-ctr flex-j-ctr"><i class='bx bx-last-page' ></i></div>
    <aside>
      <ul class="gnav gnav--manager">
        <li class="gnav--sub_menu "> <span class="btn-open"><i class="bx bx-chevron-down"></i></span>
          <p class="gnav--link"><i class="bx bxs-group"></i>生徒管理</p>
          <ul class="gnav--child">
            <li><a href="{{ url('/master/student') }}" class="">生徒一覧</a></li>
            <li><a href="{{ url('/classroom/checking//') }}" class="">印刷待ち子別ノート一覧</a></li>
            <li><a href="{{ url('/classroom/coming') }}" class="">入室予定生徒確認</a></li>
            <li><a href="{{ url('/prestudent/mendan') }}">判定面談</a></li>
            <li><a href="{{ url('/master/student_change_course') }}" class="">コース変更</a></li>
            <li><a href="{{ url('/master/student_attend_change_tsuika') }}" class="">追加コマ</a></li>
            <li><a href="{{ url('/master/student_sold_text') }}" class="">教材受け渡し</a></li>
            <li><a href="{{ url('/plan/monthly') }}">月間計画</a></li>
            <li><a href="{{ url('/master/student_attend_special_tsuika') }}" class="">特別講習申込</a></li>
            <li><a href="{{ url('/master/student_request') }}" class="">休塾、卒塾、退塾願</a></li>
          </ul>
        </li>
        <li class="gnav--sub_menu "> <span class="btn-open"><i class="bx bx-chevron-down"></i></span>
          <p class="gnav--link"><i class="bx bxs-group"></i>塾管理</p>
          <ul class="gnav--child">
            <li><a href="{{ url('/master/school') }}" class="">校舎一覧</a></li>
            <li><a href="{{ url('/master/teacher') }}" class="">先生一覧</a></li>
            <li><a href="{{ url('/master/text') }}" class="">教材一覧</a></li>
            <li><a href="" class="">年間スケジュール</a></li>
            <li><a href="{{ url('/master/term') }}" class="">講習期間一覧</a></li>
          </ul>
        </li>
        <li class="gnav--sub_menu "> <span class="btn-open"><i class="bx bx-chevron-down"></i></span>
          <p class="gnav--link"><i class="bx bx-wallet"></i>売上請求管理</p>
          <ul class="gnav--child">
            <li><a href="{{ url('/hq/invoice') }}" class="">請求一覧</a></li>
          </ul>
        </li>
        <li class="gnav--sub_menu "> <span class="btn-open"><i class="bx bx-chevron-down"></i></span>
          <a href="" class="gnav--link"><i class="bx bx-wallet"></i>アカウント管理</a>
        </li>
        <li class="gnav--sub_menu "> <span class="btn-open"><i class="bx bx-chevron-down"></i></span>
          <a href="" class="gnav--link"><i class="bx bx-wallet"></i>システム利用ログ</a>
        </li>
      </ul>
      <div class="menu-trigger menu-trigger_close"><i class='bx bx-first-page'></i></div>
    </aside>
    <!-- end aside -->
@yield('addrow')
@yield('add_memo')
@yield('modal')
  </main>
  <!-- end main -->

  <!-- start script -->
  <script src="//code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/i18n/ja.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/js-cookie/2.1.4/js.cookie.min.js"></script>
  <script src="{{ asset('js/sweetalert.min.js') }}" defer></script>
  <script src="{{ asset('js/datepicker.min.js') }}" defer></script>
  <script src="{{ asset('js/datepicker.jp.js') }}" defer></script>
  <script src="{{ asset('js/jquery.number-format.js') }}" defer></script>
  <script src="{{ asset('js/main.js') }}" defer></script>
@yield('script')
@yield('script_addrow')
  <!-- end footer -->

</body>
</html>
