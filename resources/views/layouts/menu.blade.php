<ul class="gnav clearfix">
  <li>
    @if(Request::is('movie','movie/*','movie_type','movie_type/*'))
      <a href="#" class="current"><i class='bx bxs-movie'></i>動画レッスン</a>
    @else
      <a href="#"><i class='bx bxs-movie'></i>動画レッスン</a>
    @endif
    <span class="btn-open"><i class='bx bx-chevron-down'></i></span>
    <ul class="gnav--child">
      <li><a href="{{ url('movie/1') }}" class="{{ Request::path() ==  'movie/1' ? 'current' : ''  }}">レッスン１</a></li>
      <li><a href="{{ url('movie/2') }}" class="{{ Request::path() ==  'movie/2' ? 'current' : ''  }}">レッスン２</a></li>
      <li><a href="{{ url('movie/3') }}" class="{{ Request::path() ==  'movie/3' ? 'current' : ''  }}">レッスン３</a></li>
    </ul>
  </li>
  <li>
    @if(Request::is('online','online/*'))
      <a href="{{ url('online') }}" class="current"><i class='bx bxs-video'></i>オンラインレッスン</a>
    @else
      <a href="{{ url('online') }}"><i class='bx bxs-video'></i>オンラインレッスン</a>
    @endif
    <span class="btn-open"><i class='bx bx-chevron-down'></i></span>
    <ul class="gnav--child">
      <li><a href="{{ url('online') }}" class="{{ Request::path() ==  'online' ? 'current' : ''  }}">レッスン予約</a></li>
    </ul>
  </li>
  <li>
    @if(Request::is('practice','practice/*'))
      <a href="{{ url('practice') }}" class="current"><i class='bx bx-female'></i>実践デートレッスン</a>
    @else
      <a href="{{ url('practice') }}"><i class='bx bx-female'></i>実践デートレッスン</a>
    @endif
    <span class="btn-open"><i class='bx bx-chevron-down'></i></span>
    <ul class="gnav--child">
      <li><a href="{{ url('practice') }}" class="{{ Request::path() ==  'practice' ? 'current' : ''  }}">レッスン予約</a></li>
    </ul>
  </li>
</ul>