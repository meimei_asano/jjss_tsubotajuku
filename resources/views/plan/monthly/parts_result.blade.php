<style>
  .monthly_plan_table { table-layout: fixed; }
  .monthly_plan_table select { min-width: 10px !important; width: 90%;}
  .monthly_plan_table input[type="text"] {
    min-width: 10px !important;
    width: 90%;
    text-align: right;
  }
  .monthly_plan_table .learning_items {
    width: 100%;
    height: 100%;
    white-space: nowrap;
    overflow-x: scroll;
    overflow-y: hidden;
  }
  .item
  {
    display: inline-block;
    vertical-align: top;
    white-space: normal;

    min-width: 50px;
    height: 100%;
    margin: 0;
    border: 1px solid #d3d3d3;
  }
  .item_mukou {
    background-color: #f5f5f5;
  }
  .item .date {
    background-color: #dcdcdc;
    border: 1px solid #d3d3d3;
  }
  .item .kekka {
    white-space: nowrap;
  }
  .item .kekka_item {
    width: 50px;
    border: 1px solid #d3d3d3;
    float: left;
  }
  .item .kekka_ok {
    background-color: #ffe4e2;
  }
</style>

  {{-- 計画と結果 --}}
<section class="wht_bloc">

  <p class="heading-2">計画に対しての学習実績</p>

  <table class="datatable table-vertical table-sm no_style form-group monthly_plan_table">
    <colgroup>
      <col style="width:20% !important;">
      <col style="width:70% !important;">
      <col style="width:10% !important;">
    </colgroup>
    <thead>
      <tr>
        <th>教材名・全回数・計画数</th>
        <th>計画と実績</th>
        <th>達成</th>
      </tr>
    </thead>
    <tbody>
    @if($monthly_plan->student_monthly_texts)
      @foreach($monthly_plan->student_monthly_texts as $monthly_text)
        <tr>
          <td>
            <p class="bold">{{ $monthly_text->text_name }}{{ ($monthly_text->reverse_test == "1" ? "（逆）" : "") }}</p>
            <p>全{{ $monthly_text->text_all_kaisuu }}回</p>
            <p>計画{{ $monthly_text->keikaku_suu }}回</p>
          </td>
          <td>
            <div class="learning_items">
        @php
          $tassei_suu = 0;//達成回数カウント用
        @endphp
        @if($monthly_text->student_monthly_text_units)
          @if(count($monthly_text->student_monthly_text_units) > 0)
            @foreach($monthly_text->student_monthly_text_units as $text_unit)
                    <div class="item {{ ($text_unit->learning_flag=="1" ? "item_mukou" : "") }}">
                      <div class="date txt-ctr"> {{ $text_unit->text_unit_name }}</div>
                      <div class="kekka">
             @if(isset($kekka[$monthly_text->text_id . $monthly_text->reverse_test][$text_unit->text_unit_num]) === true)
                @foreach($kekka[$monthly_text->text_id . $monthly_text->reverse_test][$text_unit->text_unit_num] as $k)
                          <div class="kekka_item txt-ctr {{ ($k['kekka']=="1" ? "kekka_ok" : "") }}">
                            {{ Carbon\Carbon::parse($k['date'])->format('m/d') }}<br>{{ config('jjss.learning_result')[$k['kekka']] }}
                          </div>
                  @php
                    if($text_unit->learning_flag!="1" && $k['kekka'] == "1") {
                      $tassei_suu++;
                    }
                  @endphp
                @endforeach
                @php
                  unset($kekka[$monthly_text->text_id . $monthly_text->reverse_test][$text_unit->text_unit_num]);
                @endphp
             @endif
                      </div>
                    </div>
            @endforeach
          @endif
        @endif
            </div>
          </td>
          <td>
            <p>　達成数：{{ $tassei_suu }}回</p>
            <p>　達成率：{{ ((int)$monthly_text->keikaku_suu != 0 ? round(((int)$tassei_suu/(int)$monthly_text->keikaku_suu) * 100) : 0) }}%</p>
          </td>
        </tr>
      @endforeach
    @else
      <tr>
        <td colspan="3">データがありません。</td>
      </tr>
    @endif
    </tbody>
  </table>
</section>



{{-- 実績（計画が無い） --}}
<section class="wht_bloc">

  <p class="heading-2">学習実績（計画無し）</p>

  <table class="datatable table-vertical table-sm no_style form-group monthly_plan_table">
    <colgroup>
      <col style="width:20% !important;">
      <col style="width:70% !important;">
      <col style="width:10% !important;">
    </colgroup>
    <thead>
    <tr>
      <th>教材名</th>
      <th>実績</th>
      <th>備考</th>
    </tr>
    </thead>
    <tbody>

    @if($kekka)
      @if(count($kekka) > 0)
        @foreach($kekka as $key => $kekka_text)
          @php
            if(count($kekka_text) == 0) {
              continue;
            }
          @endphp
          <tr>
            <td>
              <p class="bold">{{ (isset($sub_kekka_info[$key])===true ? $sub_kekka_info[$key]['text_name'] : '') }}</p>
            </td>
            <td>
              <div class="learning_items">
                @if(count($kekka_text) > 0)
                  @foreach($kekka_text as $kekka_unit_name => $kekka_datas)
                    <div class="item">
                      <div class="date txt-ctr">{{  (isset($sub_kekka_info[$key]['unit_name' . $kekka_unit_name]) ? $sub_kekka_info[$key]['unit_name' . $kekka_unit_name] :(strlen($kekka_unit_name) > 0 ? $kekka_unit_name : '　　')) }}</div>
                      <div class="kekka">
                        @foreach($kekka_datas as $kekka_data)
                          <div class="kekka_item txt-ctr {{ ($kekka_data['kekka']=="1" ? "kekka_ok" : "") }}">
                            {{ Carbon\Carbon::parse($kekka_data['date'])->format('m/d') }}<br>{{ config('jjss.learning_result')[$kekka_data['kekka']] }}
                          </div>
                        @endforeach
                      </div>
                    </div>
                  @endforeach
                @endif
              </div>
            </td>
            <td>

            </td>
          </tr>
        @endforeach
      @endif
    @else
      <tr>
        <td colspan="3">データがありません。</td>
      </tr>
    @endif
    </tbody>
  </table>
</section>