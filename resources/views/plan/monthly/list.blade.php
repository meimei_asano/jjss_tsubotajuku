@extends('layouts.default')
@section('content')

<style>
  .set_orderby {cursor:pointer;}
</style>

<section class="scroll_table-wrap wht_bloc">

  <h2 class="heading-1">月間計画一覧</h2>

  <div class="flex flex-j-between flex-a-ctr mgn-btm16">
    <form id="form" action="{{  url('plan/monthly') }}" class="mgn-btm16" >
      <div class="form-group flex">
        校舎：
        @php
          $search_school_id = '';
          if(isset($search['school_id'])) {
            $search_school_id = $search['school_id'];
          }
        @endphp
        <select name="search_school_id">
          <option class="default" value="">選択してください</option>
          @foreach ($schools as $school)
            <option value="{{ $school->id }}" {!! ($search_school_id == $school->id ? 'selected="selected"' : '') !!}>{{ $school->name }}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group flex flex-a-baseline">
        生徒：
        @php
          $search_student_id = '';
          if(isset($search['search_student_id'])) {
            $search_student_id = $search['search_student_id'];
          }
        @endphp
        <select name="search_student_id" id="search_student_id">
          <option class="default" value="">選択してください</option>
          @foreach ($students as $student)
            <option value="{{ $student->id }}" {!! ($search_student_id == $student->id ? 'selected="selected"' : '') !!}>{{ $student->name }}</option>
          @endforeach
        </select>
        &nbsp;&nbsp;
        期間：
        @php
          $search_term_id = '';
          if(isset($search['search_term_id'])) {
            $search_term_id = $search['search_term_id'];
          }
        @endphp
        <select name="search_term_id">
          <option class="default" value="">選択してください</option>
          @foreach ($terms as $term)
            <option value="{{ $term->id }}" {!! ($search_term_id == $term->id ? 'selected="selected"' : '') !!}>{{ $term->display_name }}</option>
          @endforeach
        </select>
        &nbsp;&nbsp;
        <button id="btn-search">検索<i class='bx bx-search'></i></button>
        <div class="loading" style="display: none;"><i class='bx bx-loader-alt' ></i></div>

        <input type="hidden" name="orderby_column_name" value="{{ ($orderby['column_name']) ?? '' }}">
        <input type="hidden" name="orderby_type" value="{{ ($orderby['type']) ?? '' }}">
      </div>
    </form>

    <div class="btn_list">
      <button id="create" onclick="location.href='{{ url('plan/monthly_add') }}';"><i class='bx bx-list-plus'></i>新規登録</button>
    </div>

  </div>

  <div class="scroll_table mgn-btm16">
    <table class="table-horizon table-striped datatable">
      <thead>
        <tr>
          <th class="th_check">
            <input type="checkbox" class="check" id="check_all" value="all">
            <label for="check_all"></label>
          </th>
          <th class="set_orderby" dt-column="school_id">校舎</th>
          <th class="set_orderby" dt-column="student_code">生徒コード</th>
          <th class="set_orderby" dt-column="student_name">生徒名</th>
          <th class="set_orderby" dt-column="term_id">期間</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      @if(count($student_monthlies) == 0)
        <tr>
          <td colspan="4">
            登録されていません
          </td>
        </tr>
      @else
        @foreach ($student_monthlies as $student_monthly)
        <tr style="">
          <td class="td_check">
            <input type="checkbox" class="check" data-student_monthly_id="{{ $student_monthly->id }}">
            <label></label>
          </td>
          <td class="">{{ $student_monthly->student->school->name or null }}</td>
          <td class="">{{ $student_monthly->student->student_code or null }}</td>
          <td class="">{{ $student_monthly->student->name or null }}</td>
          <td class="">{{ $student_monthly->term->display_name or null }}</td>
          <td class="txt-rgt">
            <button class="btn-sm" onclick="location.href='{{ url('plan/monthly'). '/'. $student_monthly->id }}';">変更</button>
            <button class="btn-sm" onclick="location.href='{{ url('plan/monthly_result'). '?monthly_id='. $student_monthly->id }}';">実績</button>
          </td>
        </tr>
        @endforeach
      @endif
      </tbody>
    </table>
  </div>
  <div class="flex flex-j-between">
    <div class="btn_list">
    </div>
    <div class="pager txt-ctr">
      {{ $student_monthlies->appends(Request::only(['search_school_id', 'search_student_id', 'search_term_id', 'orderby_column_name', 'orderby_type']))->links() }}
    </div>
  </div>

  <div class="form-group">
    <button class="print" type="yotei"><i class='bx bxs-printer'></i>計画印刷</button>
    <button class="print" type="jisseki"><i class='bx bxs-printer'></i>実績印刷</button>
  </div>
</section>
{{-- 月間計画PDF出力 --}}
<form id="print_form" method="post" action="" target="_blank">
  {{ csrf_field() }}
  <input type="hidden" id="student_monthly_ids" name="student_monthly_ids">
</form>

@endsection

@section('script')
<script>
$(function(){

  $('#search_student_id').select2({ language: "ja"});

  $('#btn-search').on('click', function() {
    $('#form').submit();
  });

  $('.set_orderby').on('click', function() {
    var current_orderby = $('input[name="orderby_type"]').val();
    var current_column = $('input[name="orderby_column_name"]').val();
    var new_column = $(this).attr('dt-column');

    if (current_column == new_column && current_orderby == "asc") {
      $('input[name="orderby_type"]').val('desc');
    } else {
      $('input[name="orderby_column_name"]').val(new_column);
      $('input[name="orderby_type"]').val('asc');
    }

    $('#form').submit();
  });

  $('.print').on('click', function() {
    var o = $(this);
    var selected_id = [];
    $('.check').each(function() {
      if($(this).prop('checked')) {
        if($(this).attr('data-student_monthly_id') !== undefined) {
          selected_id.push($(this).attr('data-student_monthly_id'));
        }
      }
    });

    if(selected_id.length == 0) {
      var options = {
        text: "対象の月間計画にチェックをしてください。",
        icon: "info",
        timer: 2000,
        buttons: false,
      }
      swal(options);
      return;
    }

    $('#student_monthly_ids').val(selected_id.join());

    if(o.attr('type') == "yotei") {
      $('#print_form').attr('action', "{{ url('/plan/monthly_print_yotei') }}");
    } else {
      $('#print_form').attr('action', "{{ url('/plan/monthly_print_result') }}");
    }

    $('#print_form').submit();
  });

});
</script>
@endsection