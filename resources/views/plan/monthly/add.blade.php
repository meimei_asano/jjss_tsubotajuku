@extends('layouts.default')
@section('content')
<style>
  .monthly_add_table {}
  .monthly_add_table .end_box {
    position: relative;
    margin: 1em 0;
    padding: 0.5em 1em;
    border: 1px solid #c3c3c3;
    border-radius: 8px;
  }
  .monthly_add_table .end_box .box_title {
    position: absolute;
    display: inline-block;
    top: -10px;
    left: 10px;
    padding: 0 9px;
    line-height: 1;
    background-color: #F4FFFA;
  }
  .monthly_add_table td { line-height: 2;}
  .monthly_add_table select { min-width: 10px !important; width: 90%;}
  .monthly_add_table input[type="text"] {
    min-width: 10px !important;
   /* width: 90%; */
    text-align: right;
  }
  .monthly_add_table .start_koumoku { background-color: #FFFBF4; }
  .monthly_add_table .end_koumoku { background-color: #F4FFFA; }
  .monthly_add_table .end_koumoku .no_day { background-color: #ebebeb; }
  }
</style>

<form id="form" method="post" action="{{ url('/plan/monthly_create_plan') }}">
  {{ csrf_field() }}
  <section class="scroll_table-wrap wht_bloc">
  <h2 class="heading-1">月間計画登録</h2>

  <div class="form-group">
    生徒：
    <select name="student_id" id="student_id">
      <option class="default" value="">選択してください</option>
      @foreach ($students as $student)
        <option value="{{ $student->id }}">{{ $student->name }}</option>
      @endforeach
    </select>
    &nbsp;&nbsp;
    期間：
    <select name="term_id">
      <option class="default" value="">選択してください</option>
      @foreach ($terms as $term)
        <option value="{{ $term->id }}">{{ $term->display_name }}</option>
      @endforeach
    </select>

    <button type="button" class="set_from_note btn-line flt-rgt" >最新の子別ノートからセット</button>
  </div>

  <table class="datatable table-vertical form-group monthly_add_table">
    <colgroup>
      <col style="width:20% !important;">
      <col style="width:30% !important;">
      <col style="width:40% !important;">
      <col style="">
    </colgroup>
    <thead>
      <tr>
        <th>科目</th>
        <th class="start_koumoku">開始教科書情報</th>
        <th class="end_koumoku">計画教科書情報</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
         <select name="kamoku_id[]" class="kamoku_id" >
            <option value="">選択してください</option>
            {!! \JJSS::options($kamokus->toArray(), 'id', 'name') !!}
          </select>
        </td>
        <td class="start_koumoku">
          <select name="start_text_id[]" class="start_text_id" style="margin-bottom: 2px;">
            <option value="">選択してください</option>
          </select>
          <select name="reverse_test[]" class="reverse_test" style="width: 30%; margin-bottom: 2px;">
            <option value="0">本</option>
            <option value="1">逆</option>
          </select>
          <input type="hidden" name="hon_flag[]" class="hon_flag" value="">
          <br>
          全回数：<input type="text" name="start_text_all_kaisuu[]" class="start_text_all_kaisuu" value="" style="width:25%;">
          開始回：<input type="text" name="start_text_kaisuu[]" class="start_text_kaisuu" value="" style="width:25%;">
        </td>
        <td class="end_koumoku">
          <div class="end_box mgn-btm4">
            <span class="box_title">毎回の学習回数を指定</span>
            @foreach(config('jjss.weeks') as $wk => $week)
              @php
                if(in_array($wk, array(0,6))) {
                  continue;
                }
              @endphp
              {{ $week }}<input type="text" name="maikai_kaisuu_{{ $wk }}[]" class="maikai_kaisuu_{{ $wk }}" value="" style="width:11%;">
            @endforeach
            &nbsp;&nbsp;&nbsp;毎回<input type="text" name="maikai_kaisuu[]" class="maikai_kaisuu" value="" style="width:11%;">
          </div>
          <div class="end_box">
            <span class="box_title">完了したい教科書と回数を指定</span>
            <select name="end_text_id[]" class="end_text_id" style="margin-bottom: 2px;">
              <option value="">選択してください</option>
            </select>
            <br>
            全回数：<input type="text" name="end_text_all_kaisuu[]" class="end_text_all_kaisuu" value="" style="width:25%;">
            完了回：<input type="text" name="end_text_kaisuu[]" class="end_text_kaisuu" value="" style="width:25%;">
          </div>
        </td>
        <td>
          <button type="button" class="add_row btn-line mgn-lft4" >＋</button>
          <button type="button" class="del_row btn-line" >−</button>
        </td>
      </tr>
    </tbody>
  </table>

  <div class="datatable btn_list txt-ctr">
    <button type="button" id="cancel" class="btn-info btn-thin">キャンセル</button>
    <button type="button" id="create_plan" class="btn-lg">詳細設定へ進む</button>
  </div>

</form>
@endsection

@section('script')
  <script>
    $(function(){

      $('#student_id').select2({ language: "ja"});

      $(document).on("change", "#student_id", function() {
        if($(".set_from_note").hasClass("is_disabled")) {
          $(".del_row").each(function() {
            $(this).trigger("click");
          });
          $(".set_from_note").removeClass("is_disabled");
        }
      });

      $(document).on("click", ".add_row", function(){
        var t = $(this).parent().parent();
        var o = $(this).parent().parent().clone(true);
        o.find('select, input[type="text"], input[type="hidden"]').val('');
        o.find('select').each(function() {
          if(!$(this).hasClass('kamoku_id') && !$(this).hasClass('reverse_test')) {
            $(this).children().remove();
            $(this).append($('<option value="">選択してください</option>'));
          }
        });
        o.insertAfter(t);
      });

      $(document).on("click", ".del_row", function(){
        if($(".monthly_add_table tbody").children().length == 1) {
          $(".add_row").trigger("click");
        }
        $(this).parent().parent().remove();
      });

      $(document).on("change", ".kamoku_id", function(){

        var t = $(this);
        var target1 = $(this).parent().parent().find('.start_text_id');
        var target2 = $(this).parent().parent().find('.end_text_id');

        if(t.val() == "") {
          t.parent().parent().find(".add_row").trigger("click");
          t.parent().parent().find(".del_row").trigger("click");
          return;
        } else {
          t.parent().parent().find('input').val("");
        }
/*
        //check
        var c = 0;
        $(".kamoku_id").each(function() {
          if($(this).val() == t.val()) {
            c++;
          }
        });
        if(c > 1) {
          var options = {
            text: "科目は重複できません。",
            icon: "info",
            timer: 2000,
            buttons: false,
          };
          swal(options);
          return;
        }
*/
        target1.children().remove();
        target1.append($('<option value="">選択してください</option>'));
        target2.children().remove();
        target2.append($('<option value="">選択してください</option>'));


        $.ajax({
          url: "/api/plan/monthly/get_texts",
          type: "PUT",
          data: {
            kamoku_id: t.val(),
          },
          dataType: 'json',
          beforeSend: function () {
            $('.loading').fadeIn();
          },
        })
        .done((data, statusText, xhr) => {
          if(data.length > 0) {
            var add_options = [];
            for(var i in data) {
              var all_kaisu_str = '';
              if(data[i]['units_by_student'] == "1") {
                all_kaisu_str = 'own';
              } else if(data[i]['units_by_student'] == "0"){
                all_kaisu_str = data[i]['text_units'].length;
              }
              add_options.push('<option value="' + data[i]['id'] + '" data-all_kaisu="' + all_kaisu_str + '">' + data[i]['name'] + '</option>')
            }
            target1.append(add_options.join(''));
            target2.append(add_options.join(''));
          }
        })
        .fail((data) => {
          var options = {
            text: "処理中にエラーが発生しました。",
            icon: "error",
            timer: 2000,
            buttons: false,
          };
          swal(options);
        })
        .always((data) => {
          $('.loading').fadeOut();
        });
      });

      $(document).on("change", ".start_text_id, .end_text_id", function() {
        var t = $(this);
        var all_kaisu_str = $("option:selected", this).attr('data-all_kaisu');
        var target = t.parent().find('input[name$="text_all_kaisuu[]"]');

        if(all_kaisu_str == "own") {
          target.prop('readonly', false);
        } else if(all_kaisu_str != "") {
          target.val(all_kaisu_str).prop('readonly', true);
        }
      });

      $('#create_plan').on('click', function() {
        //check
        if($('#student_id').val() == "" || $('select[name="term_id"]').val() == "") {
          var options = {
            text: "生徒と期間を選択してください。",
            icon: "info",
            timer: 2000,
            buttons: false,
          };
          swal(options);
          return;
        }
        var f = 0;//0:false, 1:true
        $('.monthly_add_table tbody tr').each(function() {
          var t = $(this);
          if(t.find(".kamoku_id").val() != "") {
            if (t.find(".start_text_id").val() == "" || t.find(".start_text_all_kaisuu").val() == "" || t.find(".start_text_kaisuu").val() == "") {
              f = 1;
            }
            var maikai_flag = 0;//毎回指定なし
            $("[class^='maikai_kaisuu']").each(function() {
              if($(this).val() != "") {
                maikai_flag = 1;
              }
            });
            if (maikai_flag == 0 && (t.find(".end_text_id").val() == "" || t.find(".end_text_all_kaisuu").val() == "" || t.find(".end_text_kaisuu").val() == "")) {
              f = 1;
            }
          }
        });

        if(f == 1) {
          var options = {
            text: "入力されていない項目があります。",
            icon: "info",
            timer: 2000,
            buttons: false,
          };
          swal(options);
          return;
        }

        if(duplicate_check() === false) {
          var options = {
            text: "既に対象期間の登録があります。",
            icon: "info",
            timer: 2000,
            buttons: false,
          };
          swal(options);
          return;
        }

        $('#form').submit();
      });

      $('#cancel').on('click', function() {
        location.href = '{{ url('/plan/monthly') }}';
      });


      function duplicate_check()
      {
        var f = true;
        $.ajax({
          url: "/api/plan/monthly/get_student_monthlies",
          type: "PUT",
          data: {
            student_id: $('select[name="student_id"]').val(),
            term_id: $('select[name="term_id"]').val()
          },
          dataType: 'json',
          async: false,
          beforeSend: function () {
            $('.loading').fadeIn();
          },
        })
        .done((data, statusText, xhr) => {
          if(data.length > 0) {
            f = false;
          }

        })
        .fail((data) => {
          var options = {
            text: "処理中にエラーが発生しました。",
            icon: "error",
            timer: 2000,
            buttons: false,
          };
          swal(options);
        })
        .always((data) => {
          $('.loading').fadeOut();
        });

        return f;
      }

      $(document).on('click', '.set_from_note', function() {

        if($('select[name="student_id"]').val() == "" || $('select[name="term_id"]').val() == "") {
          var options = {
            text: "生徒と期間の両方を選択してください。",
            icon: "info",
            timer: 2000,
            buttons: false,
          };
          swal(options);
          $('select[name="student_id"]').focus();
          return;
        }

        $.ajax({
          url: "/api/plan/monthly/get_from_note",
          type: "PUT",
          data: {
            student_id: $('select[name="student_id"]').val(),
            term_id: $('select[name="term_id"]').val()
          },
          dataType: 'json',
          async: false,
          beforeSend: function () {
            $('.set_from_note').addClass('is_disabled');
            $('.loading').fadeIn();
          },
        })
        .done((data, statusText, xhr) => {
          if(data['data'].length > 0) {
            for(var i in data['data']) {

              if($('.kamoku_id:last').val() != "") {
                $('.add_row:last').trigger('click');
              }

              var t = $('.add_row:last').parent().parent();
              var d = data['data'][i];

              t.find('.kamoku_id').val(d['kamoku_id']);

              var add_options = [];
              for(var j in d['texts']) {
                add_options.push('<option value="' + d['texts'][j]['text_id'] + '" data-all_kaisu="' + d['texts'][j]['all_kaisuu'] + '">' + d['texts'][j]['text_name'] + '</option>');
              }
              t.find('.start_text_id').append(add_options.join(''));
              t.find('.end_text_id').append(add_options.join(''));
              t.find('.start_text_id').val(d['text_id']);
              t.find('.reverse_test').val(d['reverse_test']);

              if(d['all_kaisuu'] == "own") {
                t.find('.start_text_all_kaisuu').prop('readonly', false);
              } else if(d['all_kaisuu'] != "") {
                t.find('.start_text_all_kaisuu').val(d['all_kaisuu']).prop('readonly', true);
              }
              t.find('.start_text_kaisuu').val(d['kaisuu']);
              t.find('.maikai_kaisuu').val(d['maikai_kaisuu']);
              t.find('.hon_flag').val(d['hon_flag']);

              $('.add_row:last').trigger('click');
            }
          }

          $('input[name^="maikai_kaisuu_"]').addClass("no_day");
          if(data['schedule_weekdays'].length > 0) {
            for(var i in data['schedule_weekdays']) {
              $(".maikai_kaisuu_" + data['schedule_weekdays'][i]).removeClass("no_day");
            }
          }
        })
        .fail((data) => {
          var options = {
            text: "処理中にエラーが発生しました。",
            icon: "error",
            timer: 2000,
            buttons: false,
          };
          swal(options);
        })
        .always((data) => {
          $('.loading').fadeOut();
        });
      });


    });
  </script>
@endsection
