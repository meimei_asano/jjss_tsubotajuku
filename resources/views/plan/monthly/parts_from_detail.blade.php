{{-- 月間計画の内容表示（詳細へ、からきた） --}}
@foreach($student_monthly->student_monthly_texts as $monthly_text)

  <tr>
    <td>
      {{ $monthly_text->kamoku->name or '' }}
      <input type="hidden" name="kamoku_id[]" class="kamoku_id" value="{{ $monthly_text->kamoku_id or '' }}">

    </td>
    <td>
      {{ $monthly_text->text_name or '' }}{{ (isset($monthly_text->reverse_test)===true ? ($monthly_text->reverse_test == "1" ? "（逆）" : "") : "") }}
      <input type="hidden" name="text_id[]" class="text_id" value="{{ $monthly_text->text_id or '' }}">
      <input type="hidden" name="text_name[]" class="text_name" value="{{ $monthly_text->text_name or '' }}">
      <input type="hidden" name="reverse_test[]" class="reverse_test" value="{{ $monthly_text->reverse_test or '' }}">

      <div class="maikai_info">
        <span class="box_title">毎回の計画数</span>
        @foreach(config('jjss.weeks') as $wk => $week)
          <?php if(in_array($wk, array(0,6))) { continue; } ?>
          <div class="maikai_info_item txt-ctr">{{ $week }}<br><input type="text" name="maikai_kaisuu_{{ $wk }}[]" class="maikai_kaisuu_{{ $wk }} {{ (in_array($wk, $visit_week_days)===false ? "no_day" : "") }}" value="{{ $monthly_text->{'maikai_kaisuu_' . $wk} or '' }}" ></div>
        @endforeach
        &nbsp;&nbsp;&nbsp;<div class="maikai_info_item txt-ctr">毎回<br><input type="text" name="maikai_kaisuu[]" class="maikai_kaisuu" value="{{ $monthly_text->maikai_kaisuu or '' }}" ></div>
      </div>
    </td>
    <td>
      <input type="text" name="text_all_kaisuu[]" class="text_all_kaisuu" value="{{ $monthly_text->text_all_kaisuu or '' }}" data-units_by_student="{{ $monthly_text->text->units_by_student or '1' }}"
        @if(isset($monthly_text->text) === true)
          @if($monthly_text->text->units_by_student != "1")
            readonly="readonly"
          @endif
        @endif
      >
    </td>
    <td>
      <input type="text" name="keikaku_suu[]" class="keikaku_suu" value="<?php echo $monthly_text->keikaku_suu; ?>" readonly="readonly">
    </td>
    <td>
      <div class="learning_item_area">
        <button type="button" class="add_kaisuu btn-thin btn-square" t="before" >◀︎</button>
        <button type="button" class="del_kaisuu btn-thin btn-square" t="before" >▶︎</button>
        <span class="learning_item">
  @if(isset($unit_info[$monthly_text->id]) === true)
    @foreach($unit_info[$monthly_text->id] as $text_unit)
      <input type="button" class="unit_num btn-line btn-warning {{ ($text_unit['learning_flag'] == "1" ? "btn-mukou" : "") }}" value="{{ $text_unit['text_unit_name'] }}" style="{{ ($text_unit['display_flag'] == "1" ? "display:none;" : "") }}">
      <input type="hidden" name="{{ $monthly_text->text_id or '' }}{{ $monthly_text->reverse_test or '' }}unit_num[]" value="{{ $text_unit['text_unit_num'] }}">
      <input type="hidden" name="{{ $monthly_text->text_id or '' }}{{ $monthly_text->reverse_test or '' }}unit_name[]" value="{{ $text_unit['text_unit_name'] }}">
      <input type="hidden" name="{{ $monthly_text->text_id or '' }}{{ $monthly_text->reverse_test or '' }}unit_num_learning[]" value="{{ $text_unit['learning_flag'] }}">
      <input type="hidden" name="{{ $monthly_text->text_id or '' }}{{ $monthly_text->reverse_test or '' }}unit_display_flag[]" value="{{ $text_unit['display_flag'] }}">
    @endforeach
  @endif
        </span>
        <button type="button" class="del_kaisuu btn-thin btn-square" t="after" >◀︎</button>
        <button type="button" class="add_kaisuu btn-thin btn-square" t="after" >▶︎</button>
      </div>
    </td>
    <td>
      <button type="button" class="del_row btn-line" >−</button>

      <input type="hidden" name="student_monthly_text_id[]" value="{{ $monthly_text->id or '' }}" >
    </td>
  </tr>
@endforeach
