<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    table-layout: fixed;
    border: 1px solid gray:
    font-size: 9pt;
}
th, td {
    padding: 3px 5px;
}

.main_header {
    width: 800px;
    margin-bottom: 30px;
}
.main_header_th {
    width: 10%;
    text-align: right;
    font-size: 14pt;
    font-weight: normal;
}
.main_header_td {
    width: 40%;
    text-align: left;
    font-size: 18pt;
    font-weight: bold;
}

.main_text {
    width: 800px;
}
.title {
    width: 30%;
    height: 20px;
    float: left;
    font-size: 10pt;
    text-align: right;
}
.naiyou {
    widht: 70%;
    float: right;
    font-size: 12pt;
    font-weight: bold;
}

.maikai_schedule_area {
    font-size: 9pt;
}
table.maikai_schedule {
    width: 70%;
    margin-bottom: 5px;
}
table.maikai_schedule th {
    font-weight: normal;
    font-size: 9pt;
    width: 12%;
}

table.text_units {
    width: 650px;
    margin-left: 40px;
    margin-bottom: 30px;
}
table.maikai_schedule, table.maikai_schedule th, table.maikai_schedule td,
table.text_units, table.text_units th, table.text_units td {
    border: 1px solid #b4b4b4;
}
table.text_units td {
    width: 6%;
    height: 30px;
    text-align: center;
    vertical-align: middle;
    font-size: 9pt;
}
.no_learn_unit, .no_visit_day {
    background-color: #e1e1e1;
}
</style>

{{-- 改ページ --}}
@php
    $all_page_height = 1000;//縦を1000としてマイナスして改ページ考える
    $page_height = $all_page_height;
    $page_height = $page_height - 500;/* カレンダー分 */
@endphp
@php
    $page_height = $page_height - 50;
@endphp
<br>
<table class="main_header">
    <tr>
        <td colspan="4" class="main_header_td">
            　＜学習計画＞
        </td>
    </tr>
    <tr>
        <th class="main_header_th">期間</th>
        <td class="main_header_td">{{ $student_monthly->term->term_name }}</td>
        <th class="main_header_th">生徒名<th>
        <td class="main_header_td">{{ $student_monthly->student->name }}</td>
    </tr>
</table>

@php
    $block_count = 0;
    $row_count = 0;
@endphp

@if(count($student_monthly->student_monthly_texts) > 0)
    @foreach($student_monthly->student_monthly_texts as $monthly_text)
        @php
            //1section 300 + (50 * 行数)
            $section_height = 125 + (30 * ceil(count($monthly_text->student_monthly_text_units)/15));
            $page_height = $page_height - $section_height;
            if($page_height < 0) {
                echo '<pagebreak>';
                $page_height = $all_page_height - $section_height;
            }
        @endphp
        <div class="main_text">
            <div style="width:100%;">
                <div>
                    <div class="title" style="width: 15%;">科目：</div>
                    <div class="naiyou" style="width: 85%;">{{ $monthly_text->kamoku->name or '' }}</div>
                </div>
                <div>
                    <div class="title" style="width: 15%;">教科書：</div>
                    <div class="naiyou" style="width: 85%;">{{ $monthly_text->text_name or '' }}{{ ($monthly_text->reverse_test == "1" ? "（逆）" : "") }}</div>
                </div>
            </div>
            <div>
                <div style="width:50%; float:left;">
                    <div>
                        <div class="title">全回数：</div>
                        <div class="naiyou">{{ $monthly_text->text_all_kaisuu or '' }}</div>
                    </div>
                    <div>
                        <div class="title">計画数：</div>
                        <div class="naiyou">{{ $monthly_text->keikaku_suu or '' }}</div>
                    </div>
                    <div>
                        <div class="title">学習回　</div>
                        <div class="naiyou">　</div>
                    </div>
                </div>
                <div class="maikai_schedule_area" style="width:50%; float:right;">
                    {{-- 毎回回数 --}}
                    ＜毎回の計画数＞<br>
                    <table class="maikai_schedule">
                        <tr>
                            @foreach(config('jjss.weeks') as $wk => $week)
                                @php
                                    if(in_array($wk, array(0,6))) {
                                        continue;
                                    }
                                @endphp
                                <th>{{ $week }}</th>
                            @endforeach
                            <th>毎回</th>
                        </tr>
                        <tr>
                            @foreach(config('jjss.weeks') as $wk => $week)
                                @php
                                    if(in_array($wk, array(0,6))) {
                                        continue;
                                    }
                                @endphp
                                <th class="{{ (in_array($wk, $visit_week_days)===false ? "no_visit_day" : "") }}">
                                    {{ $monthly_text->{'maikai_kaisuu_' . $wk} or '　' }}
                                </th>
                            @endforeach
                            <th>
                                {{ $monthly_text->maikai_kaisuu or '　' }}
                            </th>
                        </tr>
                    </table>
                </div>
            </div>

            <table class="text_units">
            @php
                $i = 0;
            @endphp
            @foreach($monthly_text->student_monthly_text_units as $text_unit)
                @php
                    if($i == 0) {
                        echo '<tr>';
                    }
                @endphp
                @php
                    $style_str = '';
                    if($text_unit->text_unit_name) {
                        if($text_unit->text_unit_name == "カウント"){
                            $text_unit->text_unit_name = mb_convert_kana($text_unit->text_unit_name, "k");
                        }
                    }
                @endphp
                <td class="{{ ($text_unit->learning_flag == "1" ? "no_learn_unit" : "") }}">
                    {{ $text_unit->text_unit_name or '' }}
                </td>
                @php
                    $i++;
                    if($i == 15) {
                        echo '</tr>';
                        $i = 0;
                    }
                @endphp
            @endforeach

            @php
                if($i > 0 && $i < 15) {
                    for($ii = $i; $ii < 15; $ii++) {
                        echo '<td>　</td>';
                    }
                    echo '</tr>';
                }
            @endphp
            </table>
        </div>

    @endforeach
@endif

