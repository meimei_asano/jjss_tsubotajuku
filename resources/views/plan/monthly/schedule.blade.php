<style>
  .schedule_table { table-layout: fixed; }
  .schedule_table select { min-width: 10px !important; width: 90%;}
  .schedule_table input[type="text"] {
    min-width: 10px !important;
    width: 90%;
    text-align: right;
  }
  .schedule_table .schedule_items {
    width: 100%;
    height: 100%;
    white-space: nowrap;
    overflow-x: scroll;
    overflow-y: hidden;
  }
  .item
  {
    display: inline-block;
    vertical-align: top;
    white-space: normal;

    min-width: 50px;
    height: 100%;
    margin: 0;
    border: 1px solid #d3d3d3;
  }
  .item .date {
    background-color: #dcdcdc;
    border: 1px solid #d3d3d3;
    line-height: 1.5em;
  }
  .item .kekka {
    white-space: nowrap;
  }
  .item .kekka_item {
    width: 50px;
    border: 1px solid #d3d3d3;
    float: left;
  }
  .item .kekka_ok {
    background-color: #ffe4e2;
  }
</style>

{{-- 期間内の出欠簿 --}}
<section class="wht_bloc">

  <p class="heading-2">来塾予定</p>

  <table class="datatable table-vertical table-sm no_style form-group schedule_table">
    <colgroup>
      <col style="width:10% !important;">
      <col style="width:80% !important;">
      <col style="width:10% !important;">
    </colgroup>
    <tbody>
        <tr>
          <td class="txt-rgt">
            <p>日付　　</p>
            <p>曜日　　</p>
            <p>出欠　　</p>
          </td>
          <td>
            <div class="schedule_items">
  @if(count($visit_dates) > 0)
    @foreach($visit_dates as $visit_date)
              <div class="item">
                <div class="date txt-ctr">
                  {{ Carbon\Carbon::parse($visit_date)->format("m/d") }}<br>（{{ config('jjss.weeks')[Carbon\Carbon::parse($visit_date)->dayOfWeek] }}）
                </div>
                <div class="txt-ctr">
                  @if(isset($visited_dates[$visit_date]) === true)
                    ○
                  @endif
                </div>
              </div>
    @endforeach
  @endif
            </div>
          </td>
          <td>
          </td>
        </tr>
    </tbody>
  </table>
</section>
