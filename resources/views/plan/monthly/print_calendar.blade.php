<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    table-layout: fixed;
    border: 1px solid gray:
    font-size: 11pt;
}
th, td {
    padding: 3px 5px;
}

.calendar_header {
    width: 800px;
    margin-bottom: 30px;
}
.calendar_header_th {
    width: 10%;
    text-align: right;
    font-size: 14pt;
    font-weight: normal;
}
.calendar_header_td {
    width: 40%;
    text-align: left;
    font-size: 18pt;
    font-weight: bold;
}

table.calendar {
    width: 600px;
    font-size: 12pt;/*16pt;*/
    margin-left: 30px;
    margin-bottom: 50px;
}
table.calendar, table.calendar th, table.calendar td {
    border: 1px solid #b4b4b4;
}
table.calendar td {
    height: 3.5em;
    text-align: center;
    vertical-align: top;
}
.come_day {
    background-color: #FFD28E;
}
.came_day {
    font-size: 14pt;/*20pt;*/
}
</style>

<br>
<br>
<table class="calendar_header">
    <tr>
        <td class="calendar_header_td" colspan="4">
            　＜来塾予定＞
        </td>
    </tr>
    <tr>
        <th class="calendar_header_th">期間</th>
        <td class="calendar_header_td">{{ $student_monthly->term->term_name }}</td>
        <th class="calendar_header_th">生徒名<th>
        <td class="calendar_header_td">{{ $student_monthly->student->name }}</td>
    </tr>
</table>

<div style="width:600px;text-align:right;font-size:10pt;margin-bottom: 5px;">
    来塾予定日：<span class="come_day">　　　</span>、来塾した日：○
</div>
<table class="calendar">
    <tr>
        <th>日</th>
        <th>月</th>
        <th>火</th>
        <th>水</th>
        <th>木</th>
        <th>金</th>
        <th>土</th>
    </tr>
    @php
        $first_weekday = reset($term_dates);
        $first_weekday_no = \Carbon\Carbon::parse($first_weekday)->dayOfWeek;
    @endphp
    {{-- 最初の余白 --}}
    @if($first_weekday_no > 0)
        <tr>
        @for($i = 0; $i < $first_weekday_no; $i++)
                <td>　</td>
        @endfor
    @endif

    {{-- 日にち --}}
    @php
        $k = $first_weekday_no;
        $day1_count = 0;
    @endphp
    @foreach($term_dates as $term_date)
        @if($k == 0)
            <tr>
        @endif
                <td class="{{ (in_array($term_date, $visit_dates)===true ? "come_day": "") }}" >
                    @if(\Carbon\Carbon::parse($term_date)->format('j') == "1")
                        @if($day1_count == 0)
                            {{ \Carbon\Carbon::parse($term_date)->format('j') }}
                        @else
                            {{ \Carbon\Carbon::parse($term_date)->format('m/j') }}
                        @endif
                        @php
                            $day1_count++;
                        @endphp
                    @else
                        {{ \Carbon\Carbon::parse($term_date)->format('j') }}
                    @endif
                    @if(isset($visited_dates[$term_date]) === true)
                        <br><p class="came_day" >○</p>
                    @endif
               </td>
        @if($k == 6)
            </tr>
        @endif
        @php
            $k++;
            if($k > 6) {
                $k = 0;
            }
        @endphp
    @endforeach

    {{-- 最後の余白 --}}
    @if($k > 0 && $k < 6)
        @for($i = $k; $i < 7; $i++)
                <td>　</td>
        @endfor
            </tr>
    @endif
</table>
