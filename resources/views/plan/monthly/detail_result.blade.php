@extends('layouts.default')
@section('content')
<section class="scroll_table-wrap wht_bloc">
  <h2 class="heading-1">月間学習実績</h2>

  <input type="hidden" name="monthly_id" value="{{ $monthly_id or '' }}">

  <div class="form-group">

    生徒：　<span class="heading-2">{{ $student->name }}</span>
    <input type="hidden" name="student_id" id="student_id" value="{{ $student->id }}">
    &nbsp;&nbsp;
    <span class="mgn-lft32">期間：　</span><span class="heading-2">{{ $monthly_plan->term->display_name }}</span>
    <input type="hidden" name="term_id" id="term_id" value="{{ $monthly_plan->term->id }}">

  </div>

  {{-- 出欠 --}}
  @include("plan.monthly.schedule")

  {{-- 計画と結果 --}}
  @include("plan.monthly.parts_result")

  <div class="datatable btn_list txt-ctr">
    <button type="button" id="cancel" class="btn-info btn-thin">戻る</button>
  </div>
</section>
@endsection

@section('script')
  <script>
    $(function(){

      $('#cancel').on('click', function() {
        location.href = '{{ url('/plan/monthly') }}';
      });

    });
  </script>
@endsection
