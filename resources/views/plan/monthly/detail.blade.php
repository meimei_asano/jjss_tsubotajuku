@extends('layouts.default')
<link href="{{ url('css/jquery-ui.min.css') }}" rel="stylesheet">
@section('content')
<style>
  .monthly_plan_table { table-layout: fixed; }
  .monthly_plan_table select { min-width: 10px !important; width: 90%;}
  .monthly_plan_table input[type="text"] {
    min-width: 10px !important;
    width: 90%;
    text-align: right;
  }
  .maikai_info {
    margin-top: 1.5em;
    position: relative;
  }
  .maikai_info .box_title {
    position: absolute;
    display: inline-block;
    top: -10px;
    left: 5px;
    padding: 0 9px;
    font-size: 80%;
    color: #00997E;
    line-height: 1;
    background-color: transparent;
  }
  .maikai_info .maikai_info_item {
    float: left;
    width: 15%;
    background-color: #ECFFFC;
  }
  .maikai_info input[type="text"] {
    width: 90%;
    padding-right: 5px !important;
    padding-left: 0 !important;
  }
  .maikai_info .no_day { background-color: #ebebeb !important; }

  .monthly_plan_table .learning_item_area {
    overflow-x:scroll;
    white-space: nowrap;
  }
  .monthly_plan_table .learning_item_area input[type="button"] {
    padding-top: 0;
    padding-bottom: 0;
    padding-left: 16px;
    padding-right: 16px;
    height: 32px;
    line-height: 32px;
    border-radius: 3px;
    box-sizing: border-box;
    font-size: 93%;
    cursor: pointer;
    border: 1px solid #b5b9b9;
    color: #3f464a;
  }
  .monthly_plan_table .learning_item_area .btn-square {
    padding-left: 10px;
    padding-right: 10px;
  }
  .monthly_plan_table .btn-mukou {
    background-color: #E6E6E6;
  }
</style>

{{-- 出欠 --}}
@include("plan.monthly.schedule")

<form id="form" method="post" action="{{ url('/plan/monthly') }}">
  {{ csrf_field() }}
  <section class="scroll_table-wrap wht_bloc">
  <h2 class="heading-1">月間計画登録</h2>

  <input type="hidden" name="student_monthly_id" value="{{ $student_monthly->id or '' }}">

  <div class="form-group">

    生徒：　<span class="heading-2">{{ $student->name }}</span>
    <input type="hidden" name="student_id" id="student_id" value="{{ $student->id }}">
    &nbsp;&nbsp;
    <span class="mgn-lft32">期間：　</span><span class="heading-2">{{ $term->display_name }}</span>
    <input type="hidden" name="term_id" id="term_id" value="{{ $term->id }}">

  </div>

  <table class="datatable table-vertical form-group monthly_plan_table">
    <colgroup>
      <col style="width:15% !important;">
      <col style="width:20% !important;">
      <col style="width:10% !important;">
      <col style="width: 5% !important;">
      <col style="width:40% !important;">
      <col style="width:10% !important;">
    </colgroup>
    <thead>
      <tr>
        <th>科目</th>
        <th>教科書</th>
        <th>全回数</th>
        <th>計画数</th>
        <th>学習回</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
    @if(isset($student_monthly_texts) === true)
      @if(count($student_monthly_texts) > 0)
        @include('plan.monthly.parts_from_create_plan')
      @endif
    @endif

    @if(isset($student_monthly->student_monthly_texts) === true)
      @if(count($student_monthly->student_monthly_texts) > 0)
        @include('plan.monthly.parts_from_detail')
      @endif
    @endif

      <tr>
        <td>
          <select name="kamoku_id[]" class="kamoku_id" >
            <option value="">選択してください</option>
            {!! \JJSS::options($kamokus->toArray(), 'id', 'name') !!}
          </select>
        </td>
        <td>
          <select name="text_id[]" class="text_id">
            <option value="">選択してください</option>
          </select>
          <input type="hidden" name="text_name[]" class="text_name" value="{{ $monthly_text['text_name'] or '' }}">
          <input type="hidden" name="reverse_test[]" class="reverse_test" value="0">

          <div class="maikai_info">
            <span class="box_title">毎回の計画数</span>
            @foreach(config('jjss.weeks') as $wk => $week)
              <?php if(in_array($wk, array(0,6))) { continue; } ?>
              <div class="maikai_info_item txt-ctr">{{ $week }}<br><input type="text" name="maikai_kaisuu_{{ $wk }}[]" class="maikai_kaisuu_{{ $wk }} {{ (in_array($wk, $visit_week_days)===false ? "no_day" : "") }}" value="" ></div>
            @endforeach
            &nbsp;&nbsp;&nbsp;<div class="maikai_info_item txt-ctr">毎回<br><input type="text" name="maikai_kaisuu[]" class="maikai_kaisuu" value="" ></div>
          </div>
        </td>
        <td>
          <input type="text" name="text_all_kaisuu[]" class="text_all_kaisuu" data-units_by_student="" value="" >
        </td>
        <td>
          <input type="text" name="keikaku_suu[]" class="keikaku_suu" value="" readonly="readonly">
        </td>
        <td>
          <div class="learning_item_area">
            <button type="button" class="add_kaisuu btn-thin btn-square" t="before" >◀︎</button>
            <button type="button" class="del_kaisuu btn-thin btn-square" t="before" >▶︎</button>

            <span class="learning_item">
            </span>

            <button type="button" class="del_kaisuu btn-thin btn-square" t="after" >◀︎</button>
            <button type="button" class="add_kaisuu btn-thin btn-square" t="after" >▶︎</button>
          </div>
        </td>
        <td>
          <button type="button" class="add_row btn-line" >＋</button>
          <button type="button" class="del_row btn-line" >−</button>

          <input type="hidden" name="student_monthly_text_id[]" value="" >
        </td>
      </tr>
    </tbody>
  </table>

  <div class="datatable btn_list txt-ctr">
    <button type="button" id="cancel" class="btn-info btn-thin">キャンセル</button>
    <button type="button" id="touroku" class="btn-lg">登録する</button>

  </div>

</form>
@endsection

@section('script')
  <script src="{{ asset('js/jquery-ui.min.js') }}" defer></script>
  <script>
    $(function(){

      $(".monthly_plan_table tbody").sortable();

      $(document).on("click", ".add_row", function(){
        var t = $(this).parent().parent();
        var o = $(this).parent().parent().clone(true);
        o.find('select, input[type="text"]').val('');
        o.find('select').each(function() {
          if(!$(this).hasClass('kamoku_id')) {
            $(this).children().remove();
            $(this).append($('<option value="">選択してください</option>'));
          }
        });
        o.find('.learning_item').children().remove();
        o.insertAfter(t);
      });


      $(document).on("click", ".del_row", function(){
        if($(".monthly_plan_table tbody").children().length == 1) {
          $(".add_row").trigger("click");
        }
        $(this).parent().parent().remove();
      });


      $(document).on("change", ".kamoku_id", function(){

        var t = $(this);
        var target = $(this).parent().parent().find('.text_id');

        if(t.val() == "") {
          t.parent().parent().find(".add_row").trigger("click");
          t.parent().parent().find(".del_row").trigger("click");
          return;
        }

        target.children().remove();
        target.append($('<option value="">選択してください</option>'));

        $.ajax({
          url: "/api/plan/monthly/get_texts",
          type: "PUT",
          data: {
            kamoku_id: t.val(),
          },
          dataType: 'json',
          beforeSend: function () {
            $('.loading').fadeIn();
          },
        })
        .done((data, statusText, xhr) => {
          if(data.length > 0) {
            var add_options = [];
            for(var i in data) {
              add_options.push('<option value="' + data[i]['id'] + '">' + data[i]['name'] + '</option>')
            }
            target.append(add_options.join(''));
          }
        })
        .fail((data) => {
          var options = {
            text: "処理中にエラーが発生しました。",
            icon: "error",
            timer: 2000,
            buttons: false,
          };
          swal(options);
        })
        .always((data) => {
          $('.loading').fadeOut();
        });
      });


      $(document).on("change", ".text_id", function() {
        var t = $(this);
        var target = t.parent().parent();

        var f = 0;//0:ok, 2以上:ng
        $('.text_id').each(function() {
          if($(this).val() != "" && $(this).val() == t.val()) {
            f++;
          }
        });
        if(f > 1) {
          t.val('');
          var options = {
            text: "教科書は重複できません。",
            icon: "error",
            timer: 2000,
            buttons: false,
          };
          swal(options);
          return;
        }

        //clear
        target.find('.text_name').val('');
        target.find('.text_all_kaisuu').val('');
        target.find('.text_all_kaisuu').attr("readonly", true);
        target.find('.keikaku_suu').val('');
        target.find('.text_all_kaisuu').attr('data-units_by_student', '');
        target.find('.learning_item').children().remove();

        $.ajax({
          url: "/api/plan/monthly/get_texts",
          type: "PUT",
          data: {
            text_id: t.val(),
          },
          dataType: 'json',
          beforeSend: function () {
            $('.loading').fadeIn();
          },
        })
        .done((data, statusText, xhr) => {
          if(data[0]) {
            target.find('.text_name').val(data[0]['name']);
            target.find('.text_all_kaisuu').attr('data-units_by_student', data[0]['units_by_student']);
            if(data[0]['units_by_student'] == "1") {
              target.find('.text_all_kaisuu').val('');
              target.find('.text_all_kaisuu').attr("readonly", false);
              target.find('.keikaku_suu').val('');
            } else {
              target.find('.text_all_kaisuu').val(data[0]['text_units'].length);
              target.find('.text_all_kaisuu').attr("readonly", true);
              target.find('.keikaku_suu').val(data[0]['text_units'].length);

              var add_items = [];
              add_items.push('<input type="button" class="unit_num btn-line btn-warning" value="' + (target.find('.kamoku_id').val() == "7" ? "カウント" : "初回") + '" >');
              add_items.push('<input type="hidden" name="' + data[0]['id'] + target.find('.reverse_test').val() + 'unit_num[]" value="0">');
              add_items.push('<input type="hidden" name="' + data[0]['id'] + target.find('.reverse_test').val() + 'unit_name[]" value="' + (target.find('.kamoku_id').val() == "7" ? "カウント" : "初回") + '">');
              add_items.push('<input type="hidden" name="' + data[0]['id'] + target.find('.reverse_test').val() + 'unit_num_learning[]" value="0">');
              add_items.push('<input type="hidden" name="' + data[0]['id'] + target.find('.reverse_test').val() + 'unit_display_flag[]" value="0">');
              for(var i in data[0]['text_units']) {
                add_items.push('<input type="button" class="unit_num btn-line btn-warning" value="' + data[0]['text_units'][i]['name'] + '" >');
                add_items.push('<input type="hidden" name="' + data[0]['id'] + target.find('.reverse_test').val() + 'unit_num[]" value="' + data[0]['text_units'][i]['unit_num'] + '">');
                add_items.push('<input type="hidden" name="' + data[0]['id'] + target.find('.reverse_test').val() + 'unit_name[]" value="' + data[0]['text_units'][i]['name'] + '">');
                add_items.push('<input type="hidden" name="' + data[0]['id'] + target.find('.reverse_test').val() + 'unit_num_learning[]" value="0">');
                add_items.push('<input type="hidden" name="' + data[0]['id'] + target.find('.reverse_test').val() + 'unit_display_flag[]" value="0">');
              }
              target.find('.learning_item').append(add_items.join(''));
            }
          }
        })
        .fail((data) => {
          var options = {
            text: "処理中にエラーが発生しました。",
            icon: "error",
            timer: 2000,
            buttons: false,
          };
          swal(options);
        })
        .always((data) => {
          $('.loading').fadeOut();
        });
      });


      $(document).on('change', '.text_all_kaisuu', function(){
        var t = $(this);
        var target = t.parent().parent();

        if(t.prop("readonly") == false && t.val() != "") {

          target.find('.learning_item').children().remove();

          target.find('.keikaku_suu').val(t.val());
          var text_id = target.find('.text_id').val();
          var add_items = [];
          add_items.push('<input type="button" class="unit_num btn-line btn-warning" value="' + (target.find('.kamoku_id').val() == "7" ? "カウント" : "初回") + '" >');
          add_items.push('<input type="hidden" name="' + text_id + target.find('.reverse_test').val() + 'unit_num[]" value="0">');
          add_items.push('<input type="hidden" name="' + text_id + target.find('.reverse_test').val() + 'unit_name[]" value="' + (target.find('.kamoku_id').val() == "7" ? "カウント" : "初回") + '">');
          add_items.push('<input type="hidden" name="' + text_id + target.find('.reverse_test').val() + 'unit_num_learning[]" value="0">');
          add_items.push('<input type="hidden" name="' + text_id + target.find('.reverse_test').val() + 'unit_display_flag[]" value="0">');
          for(var i = 1; i <= (t.val() - 0); i++) {
            add_items.push('<input type="button" class="unit_num btn-line btn-warning" value="' + i + '" >');
            add_items.push('<input type="hidden" name="' + text_id + target.find('.reverse_test').val() + 'unit_num[]" value="' + i + '">');
            add_items.push('<input type="hidden" name="' + text_id + target.find('.reverse_test').val() + 'unit_name[]" value="' + i + '">');
            add_items.push('<input type="hidden" name="' + text_id+ target.find('.reverse_test').val() + 'unit_num_learning[]" value="0">');
            add_items.push('<input type="hidden" name="' + text_id+ target.find('.reverse_test').val() + 'unit_display_flag[]" value="0">');
          }
          target.find('.learning_item').append(add_items.join(''));

        }
      });


      $(document).on('click', '.unit_num', function() {
        var t = $(this);
        var target = t.next().next().next();
        if(target.val() == "0") {
          target.val("1");
          t.addClass('btn-mukou');
        } else {
          target.val("0");
          t.removeClass('btn-mukou');
        }

        count_keikaku_suu(t.parent().parent().parent().parent());

      });

      function count_keikaku_suu(target) {
        var keikaku_suu = 0;
        target.find('.unit_num').each(function() {
          if($(this).hasClass("btn-mukou") === false && $(this).css("display") != "none") {
            keikaku_suu++;
          }
        });
        target.find('.keikaku_suu').val(keikaku_suu);

      }


      $(document).on('click', '.add_kaisuu', function() {
        var t = $(this);
        var target = null;

        if(t.attr('t') == "before") {
          var before_t = null;
          t.parent().find('.unit_num').each(function () {
            if($(this).css('display') != "none") {
              if(before_t) {
                target = before_t;
              }
              return false;
            }
            before_t = $(this);
          });
        } else {
          var after_t = null;
          $(t.parent().find('.unit_num').get().reverse()).each(function () {
            if($(this).css('display') != "none") {
              if(after_t) {
                target = after_t;
              }
              return false;
            }
            after_t = $(this);
          });
        }
        if(target == null) {
          target = t.parent().find('.unit_num:first');
        }
        if(target != null) {
          if(target.css('display') == "none") {
            target.css('display', 'inline-block');
            target.next().next().next().next().val("0");

            count_keikaku_suu(t.parent().parent().parent());
          }
        }
      });

      $(document).on('click', '.del_kaisuu', function() {
        var t = $(this);
        var target = null;

        if(t.attr('t') == "before") {
          t.parent().find('.unit_num').each(function () {
            if($(this).css('display') != "none") {
              target = $(this);
              return false;
            }
          });
        } else {
          $(t.parent().find('.unit_num').get().reverse()).each(function () {
            if($(this).css('display') != "none") {
              target = $(this);
              return false;
            }
          });
        }

        if(target != null) {
          if(target.css('display') != "none") {
            target.css('display', 'none');
            target.next().next().next().next().val("1");

            count_keikaku_suu(t.parent().parent().parent());
          }
        }
      });

      $('#touroku').on('click', function() {
        $('#form').submit();
      });


      $('#cancel').on('click', function() {
        location.href = '{{ url('/plan/monthly') }}';
      });

    });
  </script>
@endsection
