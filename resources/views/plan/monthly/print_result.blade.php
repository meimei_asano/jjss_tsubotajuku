<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    table-layout: fixed;
    border: 1px solid gray:
    font-size: 9pt;
}
th, td {
    padding: 3px 5px;
}

.main_header {
    width: 800px;
    margin-bottom: 30px;
}
.main_header_th {
    width: 10%;
    text-align: right;
    font-size: 14pt;
    font-weight: normal;
}
.main_header_td {
    width: 40%;
    text-align: left;
    font-size: 18pt;
    font-weight: bold;
}

.main_text {
    margin-top: 10px;
    width: 800px;
}
.title {
    width: 30%;
    height: 20px;
    float: left;
    font-size: 10pt;
    text-align: right;
}
.naiyou {
    widht: 70%;
    float: right;
    font-size: 12pt;
    font-weight: bold;
}

.maikai_schedule_area {
    font-size: 9pt;
}
table.maikai_schedule {
    width: 70%;
    margin-bottom: 10px;
}
table.maikai_schedule th {
    font-weight: normal;
    width: 12%;
    font-size: 9pt;
}

table.text_units {
    width: 650px;
    margin-left: 40px;
    margin-bottom: 25px;
}
table.maikai_schedule, table.maikai_schedule th, table.maikai_schedule td,
table.text_units, table.text_units th, table.text_units td {
    border: 1px solid #b4b4b4;
}
table.text_units td {
    width: 10%;
    height: 50px;
    text-align: center;
    vertical-align: top;
    font-size: 9pt;
}
.no_learn_unit, .no_visit_day {
    background-color: #e1e1e1;
}
.unit_name {
    margin-bottom: 5px;
}
.kekka_item {
    font-size: 10pt;
    float: left !important;
}
.kekka_ng {
    color: red;
}
.kekka_ok {
    color: blue;
}
</style>

{{-- 改ページ --}}
@php
    $all_page_height = 1000;//1200としてマイナスして改ページ考える
    $page_height = $all_page_height;
    $page_height = $page_height - 400;//カレンダー分
@endphp
@php
    $page_height = $page_height - 100;
@endphp
<table class="main_header">
    <tr>
        <td colspan="4" class="main_header_td">
            　＜学習実績＞
        </td>
    </tr>
    <tr>
        <th class="main_header_th">期間</th>
        <td class="main_header_td">{{ $student_monthly->term->term_name }}</td>
        <th class="main_header_th">生徒名<th>
        <td class="main_header_td">{{ $student_monthly->student->name }}</td>
    </tr>
</table>


@if(count($student_monthly->student_monthly_texts) > 0)
    @foreach($student_monthly->student_monthly_texts as $monthly_text)
        @php
            //1section 300 + (50 * 行数)
            $section_height = 165 + (45 * ceil(count($monthly_text->student_monthly_text_units)/10));
            $page_height = $page_height - $section_height;
            if($page_height < 0) {
                echo '<pagebreak>';
                $page_height = $all_page_height - $section_height;
            }
        @endphp
        {{-- 学習回数のテーブル --}}
        @php
            $tassei_suu = 0;//達成回数カウント用
            $html_ary = [];

            if($monthly_text->student_monthly_text_units) {
                if(count($monthly_text->student_monthly_text_units) > 0) {
                    foreach($monthly_text->student_monthly_text_units as $text_unit) {
                        $html_str = '';
                        $html_str.= '<td class="' . ($text_unit->learning_flag == "1" ? "no_learn_unit" : "") . '">';
                        //回数名
                        $html_str.= '<div class="unit_name">' . $text_unit->text_unit_name . '</div>';
                        //結果
                        if(isset($kekka[$monthly_text->text_id . $monthly_text->reverse_test][$text_unit->text_unit_num]) === true) {
                            foreach($kekka[$monthly_text->text_id . $monthly_text->reverse_test][$text_unit->text_unit_num] as $k) {
                                $html_str.= '<div class="kekka_item ' . ($k['kekka']=="1" ? "kekka_ok" : "kekka_ng") .'" >';
                                $html_str.= config('jjss.learning_result')[$k['kekka']] . ' ' . Carbon\Carbon::parse($k['date'])->format('n/j');
                                $html_str.= '</div>';
                                if($text_unit->learning_flag != "1" && $k['kekka'] == "1") {
                                    $tassei_suu++;
                                }
                            }
                            unset($kekka[$monthly_text->text_id . $monthly_text->reverse_test][$text_unit->text_unit_num]);
                        }
                        $html_str.= '</td>';
                        $html_ary[] = $html_str;
                    }
                }
            }
        @endphp

        <div class="main_text">
            <div style="width:100%;">
                <div>
                    <div class="title" style="width: 15%;">科目：</div>
                    <div class="naiyou" style="width: 85%;">{{ $monthly_text->kamoku->name or '' }}</div>
                </div>
                <div>
                    <div class="title" style="width: 15%;">教科書：</div>
                    <div class="naiyou" style="width: 85%;">{{ $monthly_text->text_name or '' }}{{ ($monthly_text->reverse_test == "1" ? "（逆）" : "") }}</div>
                </div>
            </div>
            <div>
                <div style="width:50%; float:left;">
                    <div>
                        <div class="title">全回数：</div>
                        <div class="naiyou">{{ $monthly_text->text_all_kaisuu or '' }}</div>
                    </div>
                    <div>
                        <div class="title">計画数：</div>
                        <div class="naiyou">{{ $monthly_text->keikaku_suu or '' }}</div>
                    </div>
                    <div>
                        <div class="title">達成：</div>
                        <div class="naiyou">
                            {{ $tassei_suu }}（　{{ ((int)$monthly_text->keikaku_suu != 0 ? round(((int)$tassei_suu/(int)$monthly_text->keikaku_suu) * 100) : 0) }}%　）
                            </td>
                        </div>
                    </div>
                    <div>
                        <div class="title">学習回　</div>
                        <div class="naiyou">　</div>
                    </div>
                </div>
                <div class="maikai_schedule_area" style="width:50%; float:right;">
                    {{-- 毎回回数 --}}
                    ＜毎回の計画数＞<br>
                    <table class="maikai_schedule">
                        <tr>
                            @foreach(config('jjss.weeks') as $wk => $week)
                                @php
                                    if(in_array($wk, array(0,6))) {
                                        continue;
                                    }
                                @endphp
                                <th>{{ $week }}</th>
                            @endforeach
                            <th>毎回</th>
                        </tr>
                        <tr>
                            @foreach(config('jjss.weeks') as $wk => $week)
                                @php
                                    if(in_array($wk, array(0,6))) {
                                        continue;
                                    }
                                @endphp
                                <th class="{{ (in_array($wk, $visit_week_days)===false ? "no_visit_day" : "") }}">
                                    {{ $monthly_text->{'maikai_kaisuu_' . $wk} or '　' }}
                                </th>
                            @endforeach
                            <th>
                                {{ $monthly_text->maikai_kaisuu or '　' }}
                            </th>
                        </tr>
                    </table>
                </div>
            </div>


            <table class="text_units">
            @php
                $i = 0;
                foreach($html_ary as $html_) {
                    if($i == 0) {
                        echo '<tr>';
                    }

                    echo $html_;

                    $i++;
                    if($i == 10) {
                        echo '</tr>';
                        $i = 0;
                    }
                }
                if($i > 0 && $i < 10) {
                    for($ii = $i; $ii < 10; $ii++) {
                        echo '<td>　</td>';
                    }
                    echo '</tr>';
                }
            @endphp
            </table>
        </div>
    @endforeach
@endif

{{-- 実績（計画が無い） --}}
@if($kekka)
    @if(count($kekka) > 0)
        {{-- 改ページ用の処理 --}}
        @php
            if($page_height < 150) {
                echo '<pagebreak>';
                $page_height = $all_page_height;
            }
        @endphp
        <table class="main_header" style="margin-top: 20px;">
            <tr>
                <td colspan="4" class="main_header_td">
                    　＜計画無し実績＞
                </td>
            </tr>
        </table>

        @foreach($kekka as $key => $kekka_text)
            @php
                if(count($kekka_text) == 0) {
                  continue;
                }
            @endphp
            {{-- 改ページ用の処理 --}}
            @php
                //1section 300 + (50 * 行数)
                $section_height = 110 + (45 * ceil(count($kekka_text)/10));
                $page_height = $page_height - $section_height;
                if($page_height <= 0) {
                    echo '<pagebreak>';
                    $page_height = $all_page_height;
                }
            @endphp
            <div class="main_text">
                <div style="width:100%;">
                    <div>
                        <div class="title" style="width: 15%;">教科書：</div>
                        <div class="naiyou" style="width: 85%;">{{ (isset($sub_kekka_info[$key])===true ? $sub_kekka_info[$key]['text_name'] : '') }}</div>
                    </div>
                </div>
                <div>
                    <div style="width:50%; float:left;">
                        <div>
                            <div class="title">学習回　</div>
                            <div class="naiyou">　</div>
                        </div>
                    </div>
                    <div style="width:50%; float:right;">
                    </div>
                </div>

                @if(count($kekka_text) > 0)
                    <table class="text_units">
                    @php
                        $i = 0;
                    @endphp
                    @foreach($kekka_text as $kekka_unit_name => $kekka_datas)
                        @php
                            if($i == 0) {
                                echo '<tr>';
                            }
                        @endphp
                        <td>
                            <div class="unit_name">
                                {{  (isset($sub_kekka_info[$key]['unit_name' . $kekka_unit_name]) ? $sub_kekka_info[$key]['unit_name' . $kekka_unit_name] :(strlen($kekka_unit_name) > 0 ? $kekka_unit_name : '　　')) }}
                            </div>
                            @foreach($kekka_datas as $kekka_data)
                                <div class="kekka_item {{ ($kekka_data['kekka']=="1" ? "kekka_ok" : "kekka_ng") }}">
                                    {{ config('jjss.learning_result')[$kekka_data['kekka']] }} {{ Carbon\Carbon::parse($kekka_data['date'])->format('n/j') }}
                                </div>
                            @endforeach
                        </td>
                        @php
                            $i++;
                            if($i == 10) {
                                echo '</tr>';
                                $i = 0;
                            }
                        @endphp
                    @endforeach

                    @php
                        if($i > 0 && $i < 10) {
                            for($ii = $i; $ii < 10; $ii++) {
                                echo '<td>　</td>';
                            }
                            echo '</tr>';
                        }
                    @endphp
                    </table>
                @endif
            </div>
        @endforeach
    @endif
@endif
