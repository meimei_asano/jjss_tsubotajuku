@extends('layouts.default')
@section('content')
<style>
.set_container {
  position: relative;
  display: inline-block;
}
.remove_tsuika_request_field {
  position: absolute;
  color: white;
  font-weight: bolder;
  background-color: red;
  border: solid 1px white;
  border-radius: 50%;
  padding: 3px 5px;
  cursor: pointer;
  right: -55px;
  top: 15px;
}
.remove_tsuika_request_field:hover {
  color: red;
  font-weight: bolder;
  background-color: white;
  border: solid 1px red;
}
.duplicate_tsuika_request_field {
  position: absolute;
  color: white;
  font-weight: bolder;
  background-color: #f29600;
  border: solid 1px white;
  border-radius: 50%;
  padding: 3px 5px;
  cursor: pointer;
  right: -20px;
  top: 15px;
}
.duplicate_tsuika_request_field.default {
  top: -14px;
}
.duplicate_tsuika_request_field:hover {
  color: #f29600;
  font-weight: bolder;
  background-color: white;
  border: solid 1px #f29600;
}
</style>
<form id="form" method="post" action="">
  {{ csrf_field() }}
  <section class="scroll_table-wrap wht_bloc">
  <h2 class="heading-1">追加コマ情報</h2>

  @if($studentAttendChangeTsuika->syounin_torikeshi_shinsei_flag == "1")
    <div class="alert alert-danger flex flex-a-ctr">
      承認取消申請中です。
    </div>
  @endif

    <div class="">
    <table class="datatable table-vertical">
      <tr>
        <th>生徒</th>
        <td class="form-group">
          @if($studentAttendChangeTsuika->id)
          {{ $studentAttendChangeTsuika->student->name }}
          <input type="hidden" name="student_id" value="{{ $studentAttendChangeTsuika->student_id }}">
          @else
          <select id="student_id" name="student_id">
            <option class="default" value="">選択してください</option>
            @if(count($students) > 0)
              @foreach($students as $student)
                <option value="{{ $student->id }}" {{ $student->id == old('student_id', $studentAttendChangeTsuika->student_id) ? 'selected="selected"' : '' }} >{{ $student->name }}</option>
              @endforeach
            @endif
          </select>
          @endif
        </td>
      </tr>
      <tr>
        <th>学年</th>
        <td class="form-group">
          @php
            $grade_code = old('grade_code', (isset($studentAttendChangeTsuika->student->grade_code)===true ? $studentAttendChangeTsuika->student->grade_code : ''));
          @endphp
          <p class="grade_code_name">{{  config('jjss.grade')[$grade_code] ?? '' }}</p>
          <input type="hidden" name="grade_code" id="grade_code" value="{{ $grade_code }}">
        </td>
      </tr>
      <tr>
        <th>申込日</th>
        <td class="form-group">
          <input type="text" id="request_date" name="request_date" class="datepicker-here" autocomplete="off" data-language="jp" data-auto-close="true" value="{{ old('request_date', $studentAttendChangeTsuika->request_date) }}"/>
        </td>
      </tr>
      <tr>
        <th>追加コマ情報</th>
        <td class="form-group">
          <div id="tsuika_request_group">
            @php
              $tsuikaRequest = old('tsuika_request', json_decode($studentAttendChangeTsuika->tsuika_request, JSON_UNESCAPED_UNICODE));
            @endphp
            @if($tsuikaRequest && is_array($tsuikaRequest) && count($tsuikaRequest) > 0)
              @foreach($tsuikaRequest as $key => $item)
                @if($key !== 0)
                  <br class="set_number_{{ $key }}">
                @endif
                <div class="set_container" data-set-number="{{ $key }}">
                  @if($key !== 0)
                    <hr>
                    <span class="duplicate_tsuika_request_field" title="複製する"><i class="bx bx-copy"></i></span>
                    <span class="remove_tsuika_request_field" title="削除する"><i class="bx bx-trash"></i></span>
                  @else
                    <span class="duplicate_tsuika_request_field default" title="複製する"><i class="bx bx-copy"></i></span>
                  @endif
                  <div class="mgn-btm8">
                    <b>追加授業日</b>
                    <input type="text" name="tsuika_request[{{ $key }}][add_date]" class="add_date datepicker-here" autocomplete="off" data-language="jp" data-auto-close="true" value="{{ $item['add_date'] }}"/>
                  </div>
                  <div class="mgn-btm8">
                    <b>入室予定時間</b>
                    <input type="text" name="tsuika_request[{{ $key }}][start_time]" class="start_time timepicker input-mn" autocomplete="off" value="{{ $item['start_time'] }}"/> ※手入力が可能です
                  </div>
                  <div class="mgn-btm8">
                    <b>退室予定時間</b>
                    <input type="text" name="tsuika_request[{{ $key }}][end_time]" class="end_time timepicker input-mn" autocomplete="off" value="{{ $item['end_time'] }}"/> ※手入力が可能です
                  </div>
                  <div class="mgn-btm8">
                    <b>時間数</b>
                    <input type="text" name="tsuika_request[{{ $key }}][hours]" class="tsuika_hours" placeholder="時間数" autocomplete="off" value="{{ $item['hours'] }}" readonly/>
                  </div>
                  <div class="mgn-btm8">
                    <b>教科</b>
                    <input type="text" name="tsuika_request[{{ $key }}][kyouka]" class="kyouka" placeholder="教科" autocomplete="off" value="{{ $item['kyouka'] }}"/>
                  </div>
                </div>
              @endforeach
            @else
              <div class="set_container" data-set-number="0">
                <span class="duplicate_tsuika_request_field default" title="複製する"><i class="bx bx-copy"></i></span>
                <div class="mgn-btm8">
                  <b>追加授業日</b>
                  <input type="text" name="tsuika_request[0][add_date]" class="add_date datepicker-here" autocomplete="off" data-language="jp" data-auto-close="true" value=""/>
                </div>
                <div class="mgn-btm8">
                  <b>入室予定時間</b>
                  <input type="text" name="tsuika_request[0][start_time]" class="start_time timepicker input-mn" autocomplete="off" value=""/> ※手入力が可能です
                </div>
                <div class="mgn-btm8">
                  <b>退室予定時間</b>
                  <input type="text" name="tsuika_request[0][end_time]" class="end_time timepicker input-mn" autocomplete="off" value=""/> ※手入力が可能です
                </div>
                <div class="mgn-btm8">
                  <b>時間数</b>
                  <input type="text" name="tsuika_request[0][hours]" class="tsuika_hours" placeholder="時間数" autocomplete="off" value=""/>
                </div>
                <div class="mgn-btm8">
                  <b>教科</b>
                  <input type="text" name="tsuika_request[0][kyouka]" class="kyouka" placeholder="教科" autocomplete="off" value=""/>
                </div>
              </div>
            @endif
          </div>
          <button type="button" id="add_tsuika_request_fields">内訳追加</button>
        </td>
      </tr>
      <tr>
        <th>追加時間数</th>
        <td class="form-group">
          <input type="text" id="tsuika_hours" name="tsuika_hours" placeholder="追加時間数" value="{{ old('tsuika_hours', $studentAttendChangeTsuika->tsuika_hours) }}" readonly/>
        </td>
      </tr>
      <tr>
        <th>追加単価</th>
        <td class="form-group">
          <input type="text" data-format="number" id="unit_price" name="unit_price" placeholder="追加単価" value="{{ old('unit_price', $studentAttendChangeTsuika->unit_price) }}" readonly/>
        </td>
      </tr>
      <tr>
        <th>登録先生</th>
        <td class="form-group">
          {{ $studentAttendChangeTsuika->teacher_id === null ? '' : (isset($studentAttendChangeTsuika->teacher->name)===true ? $studentAttendChangeTsuika->teacher->name : '') }}
        </td>
      </tr>
      <tr>
        <th>承認先生</th>
        <td class="form-group">
          {{ $studentAttendChangeTsuika->syounin_teacher_id === null ? '未承認' : $studentAttendChangeTsuika->syounin_teacher->name }}
        </td>
      </tr>
    </table>
  </div>
  <div class="datatable btn_list">
    {{-- キャンセル（一覧に遷移）--}}
    <button type="button" id="cancel" class="btn-info btn-line btn-lg btn-lg-slim">キャンセル</button>
    {{-- 申請（登録・更新） --}}
    @php
      $add_class = 'is_disabled';
      if(in_array($permission, ['校長', 'マネージャー', '本部スタッフ']) === false) {
        if($studentAttendChangeTsuika->id === null) {
          $add_class = '';
        }
        if($studentAttendChangeTsuika->id !== null && $studentAttendChangeTsuika->syounin_teacher_id === null
            && ($studentAttendChangeTsuika->teacher_id == $teacher->id || in_array($permission, ['管理者']))) {
          $add_class = '';
        }
      }
    @endphp
    <button type="button" id="save" class="btn-lg {{ $add_class }}">申請</button>
    @if($studentAttendChangeTsuika->id !== null)
      {{-- 申請取消（削除） --}}
      @php
        $add_class = 'is_disabled';
        if($studentAttendChangeTsuika->teacher_id == $teacher->id || in_array($permission, ['管理者'])) {
          if($studentAttendChangeTsuika->id !== null && $studentAttendChangeTsuika->syounin_teacher_id === null) {
            $add_class = '';
          }
        }
      @endphp
      <button type="button" id="torikeshi" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">申請取消</button>
      {{-- 承認 --}}
      @if(in_array($permission, ['校長', 'マネージャー', '管理者']))
        @if($studentAttendChangeTsuika->teacher_id != $teacher->id)
        <button type="button" id="syounin" class="btn-lg {{ ($studentAttendChangeTsuika->syounin_teacher_id !== null || $studentAttendChangeTsuika->id === null) ? 'is_disabled' : '' }}">承認</button>
        @endif
      @endif
      {{-- 印刷 --}}
      <button type="button" id="print" class="btn-lg btn-lg-slim {{ ($studentAttendChangeTsuika->syounin_teacher_id === null || $studentAttendChangeTsuika->id === null || $studentAttendChangeTsuika->syounin_torikeshi_shinsei_flag === 1) ? 'is_disabled' : '' }}">印刷</button>
      {{-- 承認取消申請 --}}
      @php
        $add_class = 'is_disabled';
        if($studentAttendChangeTsuika->teacher_id == $teacher->id || in_array($permission, ['管理者'])) {
          if($studentAttendChangeTsuika->syounin_teacher_id !== null && $studentAttendChangeTsuika->syounin_torikeshi_shinsei_flag !== 1) {
            $add_class = '';
          }
        }
      @endphp
      <button type="button" id="syounin_torikeshi_shinsei" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">承認取消申請</button>
      {{-- 承認取消 --}}
      @php
        $add_class = 'is_disabled';
        if(in_array($permission, ['校長', 'マネージャー', '管理者'])) {
          if($studentAttendChangeTsuika->teacher_id != $teacher->id) {
            if($studentAttendChangeTsuika->syounin_teacher_id !== null && $studentAttendChangeTsuika->syounin_torikeshi_shinsei_flag == "1") {
              $add_class = '';
            }
          }
      @endphp
      <button type="button" id="syounin_torikeshi" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">承認取消</button>
      @php
        }
      @endphp
    @endif
    <input type="hidden" id="id" name="id" value="{{ $studentAttendChangeTsuika->id }}">
  </div>
</div>
</form>
@endsection

@section('script')
  <script src="{{ asset('js/timepicker.min.js') }}" defer></script>
  <script>
/*
    var fx = {
      setTimepicker: function() {
        $('.timepicker').timepicker({
          'timeFormat': 'H:i',
          'minTime': false,
          'maxTime': false,
          'scrollDefault': '16:00',
          'noneOption': [
            { 'label': '16:00', 'value': '16:00' },
            { 'label': '17:00', 'value': '17:00' },
            { 'label': '18:00', 'value': '18:00' },
            { 'label': '19:00', 'value': '19:00' },
            { 'label': '20:00', 'value': '20:00' },
            { 'label': '21:00', 'value': '21:00' },
            { 'label': '22:00', 'value': '22:00' },
          ],
        });
      }
    }
*/
    $(function(){
      $('#student_id').select2({ language: "ja"});
      //fx.setTimepicker();

      $('.add_date').each(function() {
        var o = $(this);
        var atai = o.val();
        o.datepicker({
          language: 'jp',
          autoClose: true,
          onSelect: function(dateText) {
            get_date_type($(this), dateText);
          }
        });
        if(atai != "") {
          o.datepicker().data('datepicker').selectDate(new Date(atai));
        }
      });

      function setTimepickerNormal(t)
      {
        t.timepicker('remove');
        t.timepicker({
          'timeFormat': 'H:i',
          'minTime': false,
          'maxTime': false,
          'scrollDefault': '16:00',
          'noneOption': [
            { 'label': '16:00', 'value': '16:00' },
            { 'label': '17:00', 'value': '17:00' },
            { 'label': '18:00', 'value': '18:00' },
            { 'label': '19:00', 'value': '19:00' },
            { 'label': '20:00', 'value': '20:00' },
            { 'label': '21:00', 'value': '21:00' },
            { 'label': '22:00', 'value': '22:00' },
          ],
        });
      }

      function setTimepickerTokubetsu(t)
      {
        t.timepicker('remove');
        t.timepicker({
          'timeFormat': 'H:i',
          'minTime': '22:00',
          'maxTime': '22:00',
          'scrollDefault': '13:00',
          'noneOption': [
            { 'label': '13:00 Ⅰ〜', 'value': '13:00' },
            { 'label': '14:00', 'value': '14:00' },
            { 'label': '15:00 〜Ⅰ', 'value': '15:00' },
            { 'label': '15:10 Ⅱ〜', 'value': '15:10' },
            { 'label': '16:10', 'value': '16:10' },
            { 'label': '17:10 〜Ⅱ', 'value': '17:10' },
            { 'label': '17:20 Ⅲ〜', 'value': '17:20' },
            { 'label': '18:20', 'value': '18:20' },
            { 'label': '19:20 〜Ⅲ', 'value': '19:20' },
            { 'label': '19:30 Ⅳ〜', 'value': '19:30' },
            { 'label': '20:30', 'value': '20:30' },
            { 'label': '21:30 〜Ⅳ', 'value': '21:30' },
          ]
        });
      }

      function get_date_type(o, dateText) {

        var tdate = dateText;

        $.ajax({
          url: "/api/master/student_attend_change_tsuika/get_date_type",
          type: "GET",
          data: {
            student_id: $('[name="student_id"]').val(),
            target_date: tdate
          },
          dataType: 'json',
          beforeSend: function() {
            $('.loading').fadeIn();
          },
        })
        .done((data, statusText, xhr) => {
          var date_type = data.date_type;
          console.log(date_type);
          $('.add_date').each(function() {
            if($(this).val() == tdate) {
              if(date_type == "特別講習") {
                setTimepickerTokubetsu($(this).parent().parent().find('.start_time'));
                setTimepickerTokubetsu($(this).parent().parent().find('.end_time'));
              } else if(date_type == "通常") {
                setTimepickerNormal($(this).parent().parent().find('.start_time'));
                setTimepickerNormal($(this).parent().parent().find('.end_time'));
              }
            }
          });
          if(date_type == "休日") {
            var options = {
              text: "この日は休日です。",
              icon: "error",
              timer: 2000,
              buttons: false,
            };
            swal(options);
          }
        })
        .fail((data) => {
          var options = {
            text: "処理中にエラーが発生しました。",
            icon: "error",
            timer: 2000,
            buttons: false,
          };
          swal(options);
          //self.show();
          $('.loading').fadeOut();
        })
        .always((data) => {
          $('.loading').fadeOut();
        });
      //});
      }

      $('#save').on('click', function() {
        $('#form').attr('action', '{{ url("/master/student_attend_change_tsuika/save") }}');
        $('#form').submit();
      });

      $('#torikeshi').on('click', function() {
        swal({
          text: "申請を取消します。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/master/student_attend_change_tsuika_delete") }}');
            $('#form').submit();
          }
        });
      });

      $('#syounin_torikeshi_shinsei').on('click', function() {
        swal({
          text: "承認取消申請を行います。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/master/student_attend_change_tsuika_torikeshi_shinsei") }}');
            $('#form').submit();
          }
        });
      });

      $('#cancel').on('click', function() {
        location.href = '{{ url("/master/student_attend_change_tsuika") }}';
      });

      $(document).on('click', '#add_tsuika_request_fields, .duplicate_tsuika_request_field', function(e) {
        let isDuplicate = $(this).hasClass('duplicate_tsuika_request_field');
        let addDate = '';
        let startTime = '';
        let endTime = '';
        let tsuikaHours = '';
        let kyouka = '';

        if (isDuplicate) {
          parentDiv = $(this).closest('div.set_container');
          addDate = parentDiv.find('input.add_date').val();
          startTime = parentDiv.find('input.start_time').val();
          endTime = parentDiv.find('input.end_time').val();
          tsuikaHours = parentDiv.find('input.tsuika_hours').val();
          kyouka = parentDiv.find('input.kyouka').val();
        }

        let lastChildNumber = parseInt($('#tsuika_request_group').children().last().attr('data-set-number'));
        let nextNumber = (lastChildNumber + 1);
        let row = '<br class="set_number_' + nextNumber + '">' +
          '<div class="set_container" data-set-number="' + nextNumber + '">' +
          '<hr>' +
          '<span class="duplicate_tsuika_request_field" title="複製する"><i class="bx bx-copy"></i></span>' +
          '<span class="remove_tsuika_request_field" title="削除する"><i class="bx bx-trash"></i></span>' +
          '<div class="mgn-btm8">' +
            '<b>追加授業日</b>' +
            '<input type="text" name="tsuika_request[' + nextNumber + '][add_date]" class="add_date datepicker-here" autocomplete="off" data-language="jp" data-auto-close="true" value="' + addDate + '"/>' +
          '</div>' +
          '<div class="mgn-btm8">' +
            '<b>入室予定時間</b>' +
            '<input type="text" name="tsuika_request[' + nextNumber + '][start_time]" class="start_time timepicker input-mn" autocomplete="off" value="' + startTime + '"/> ※手入力が可能です' +
          '</div>' +
          '<div class="mgn-btm8">' +
            '<b>退室予定時間</b>' +
            '<input type="text" name="tsuika_request[' + nextNumber + '][end_time]" class="end_time timepicker input-mn" autocomplete="off" value="' + endTime + '"/> ※手入力が可能です' +
          '</div>' +
          '<div class="mgn-btm8">' +
            '<b>時間数</b>' +
            '<input type="text" name="tsuika_request[' + nextNumber + '][hours]" class="tsuika_hours" placeholder="時間数" autocomplete="off" value="' + tsuikaHours + '"/>' +
          '</div>' +
          '<div class="mgn-btm8">' +
            '<b>教科</b>' +
            '<input type="text" name="tsuika_request[' + nextNumber + '][kyouka]" class="kyouka" placeholder="教科" autocomplete="off" value="' + kyouka + '"/>' +
          '</div>' +
          '</div>';
        $('#tsuika_request_group').append(row);
        fx.setTimepicker();
        $('div[data-set-number=' + nextNumber + ']').find('.datepicker-here').datepicker({
              language: 'jp',
              autoClose: true,
              onSelect: function(dateText) {
                get_date_type($(this), dateText);
              }
        });

        if (isDuplicate) {
          $('.tsuika_hours').trigger('change');
        }
      });

      $(document).on('click', '.remove_tsuika_request_field', function(e) {
        let parentDiv = $(this).closest('div.set_container');
        let setNumber = parentDiv.attr('data-set-number');
        $('br.set_number_' + setNumber).remove();
        parentDiv.remove();
        $('.tsuika_hours').trigger('change');
      });

      $(document).on('change', '.tsuika_hours', function(e) {
        let computeTsuikaHours = new Promise(function(resolve, reject) {
          let tsuikaHoursFields = $('.tsuika_hours');
          let totalHours = 0;

          if (tsuikaHoursFields.length !== 0) {
            tsuikaHoursFields.each(function(key, item) {
              let val = parseFloat($(item).val());

              if (isNaN(val) === false) {
                totalHours += val;
              }

              if ((parseInt(key) + 1) === tsuikaHoursFields.length) {
                resolve(totalHours);
              }
            });
          } else {
            resolve(totalHours);
          }
        });

        computeTsuikaHours.then(function(value){
          $('#tsuika_hours').val(value);
        });
      });

      $(document).on('change', '.timepicker', function(e) {
        let parentContainer = $(this).closest('.set_container');
        let rawStartTime = parentContainer.find('.start_time').val();
        let rawEndTime = parentContainer.find('.end_time').val();

        if (rawStartTime && rawEndTime) {
          let startTime = rawStartTime.split(':');
          let endTime = rawEndTime.split(':');
          let dateTimeStart = new Date(0, 0, 0, startTime[0], startTime[1]);
          let dateTimeEnd = new Date(0, 0, 0, endTime[0], endTime[1]);
          let timeDifference = (dateTimeEnd - dateTimeStart) / 1000 / 60 / 60;

          if (timeDifference > 0) {
            parentContainer.find('.tsuika_hours').val(timeDifference);

          } else {
            parentContainer.find('.tsuika_hours').val("");
          }
        } else {
          parentContainer.find('.tsuika_hours').val("");
        }
        $('.tsuika_hours').trigger('change');
      });

      $(document).on('change', '#student_id', function (e){
        let student_id = $('#student_id').val();

        if (student_id == "") {
          $('#unit_price').val("");
          return false;
        }

        $.ajax({
          url: "/api/master/student_attend_change_tsuika/get_unit_price",
          type: "GET",
          data: {
            student_id: student_id,
            add_date: $('.add_date:first').val()
          },
          dataType: 'json',
          beforeSend: function() {
            $('.loading').fadeIn();
          },
        })
        .done((data, statusText, xhr) => {
          $('#unit_price').val(data.unit_price).change();
          $('#grade_code').val(data.grade_code);
          $('.grade_code_name').html(data.grade_code_name);
        })
        .fail((data) => {
          var options = {
            text: "処理中にエラーが発生しました。",
            icon: "error",
            timer: 2000,
            buttons: false,
          };
          swal(options);
          self.show();
          $('.loading').fadeOut();
        })
        .always((data) => {
          $('.loading').fadeOut();
        });
      });

      @if (session()->has('alert.success'))
        @if(session('alert.success') == "承認済み。" && isset($studentAttendChangeTsuika->id) === true)
          window.open('{{ url('/master/student_attend_change_tsuika/print'). '/'. $studentAttendChangeTsuika->id }}', '_blank');
          location.href = '{{ url("/master/student_attend_change_tsuika") }}';
        @endif
      @endif
      $('#print').on('click', function() {
        window.open('{{ url('master/student_attend_change_tsuika/print'). '/'. $studentAttendChangeTsuika->id }}', '_blank');
        return;
      });

    })
  </script>
  @if(in_array($permission, ['校長', 'マネージャー', '管理者']))
  <script>
    $(function(){
      $('#syounin').on('click', function() {
        $('#form').attr('action', '{{ url("/master/student_attend_change_tsuika/syounin") }}');
        $('#form').submit();
      });

      $('#syounin_torikeshi').on('click', function() {
        swal({
          text: "承認取消を行います。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/master/student_attend_change_tsuika_torikeshi") }}');
            $('#form').submit();
          }
        });
      });

    })
  </script>
  @endif
@endsection
