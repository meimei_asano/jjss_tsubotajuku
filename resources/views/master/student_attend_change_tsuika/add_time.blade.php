<style>
body {
    font-size: 11pt;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
    table-layout: fixed;
}
th, td {
    padding: 0;
}
.main {
    width: 800px;
}
.main table {
    width: 100%;
}
.bordered {
    border: solid 1px black;
}
.clr {
    clear: both;
    margin: 0;
    padding: 0;
}
.row-1 .col-1 {
    float: left;
    width: 20%;
    border: solid 1px black;
    padding: 3.5px 0;
    margin: 0;
    text-align: center;
    background-color: silver;
    font-size: 16pt;
}
.row-1 .col-2 {
    float: right;
    width: 20%;
    text-align: right;
}
.row-2 {
    font-size: 21.5pt;
    text-decoration: underline;
    font-weight: bold;
    text-align: center;
}
.row-indent {
    float: left;
    width: 3.5%;
}
.row-indent-right {
    float: right;
    width: 3.5%;
}
.row-3 .col-1 {
    float: left;
}
.row-9 .col-1 {
    float: left;
}
.row-10 .col-1 {
    float: left;
}
.row-11 .col-1 {
    float: left;
    width: 93%;
}
table.tsuika-request {
    border: solid 1px black;
    text-align: center;
}
table.tsuika-request td {
    border: solid 1px black;
}
.row-12 .col-1 {
    float: left;
    width: 15%;
}
.row-12 .col-2 {
    float: left;
    width: 3%;
    text-align: center;
}
.row-12 .col-3 {
    float: left;
    width: 14%;
    text-align: center;
    border-bottom: solid 1px black;
}
.row-12 .col-4 {
    float: left;
    width: 17%;
    text-align: center;
    border-bottom: solid 1px black;
}
.row-12 .col-5 {
    float: left;
    width: 14%;
    text-align: center;
    border-bottom: solid 1px black;
}

.row-12 .col-6 {
    float: left;
    width: 7%;
    text-align: center;
    border-bottom: solid 1px black;
}
.row-12 .col-7 {
    float: left;
    width: 10%;
    text-align: center;
    border-bottom: solid 1px black;
}
.row-12 .col-8 {
    float: left;
    width: 5%;
    text-align: center;
    border-bottom: solid 1px black;
}
.row-12 .col-8 {
    float: left;
    width: 4%;
}
.row-13 .col-1 {
    float: left;
}
.row-14 .col-1 {
    float: left;
    width: 93%;
}
table.unit-prices {
    border: solid 1px black;
    text-align: center;
}
table.unit-prices td {
    border: solid 1px black;
}
.group-row .col-1 {
    float: left;
    width: 60%;
}
.group-row .col-1 div div.student-name,
.group-row .col-1 div div.under-student-name
{
    float: left;
    font-size: 13pt;
}
.group-row .col-2 {
    float: right;
    width: 35%;
}
.group-row .col-2 div.school-name {
    font-size: 15pt;
}
.row-last .col-1 {
    float: right;
    width: 22%;
}
</style>
<div class="main">
    <div class="row-1">
        <div class="col-1">{{ $copyFor === 'home' ? '自宅保管用' : '校舎保管用' }}</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-2">{{ \JJSS::printDate($studentAttendChangeTsuika->request_date) }}</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-2">追加コマ申込受付</div>
    <br>
    <div class="row-3">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">〒{{ $studentAttendChangeTsuika->student->zip_code }}</div>
        <div class="clr"></div>
    </div>
    <div class="group-row">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">
            <div>{{ $studentAttendChangeTsuika->student->pref . ' ' . $studentAttendChangeTsuika->student->address1 }}</div>
            <div>{{ $studentAttendChangeTsuika->student->address2 }}</div>
            <div>
                <div style="float: left; width: 5%">&nbsp;</div>
                <div class="student-name">{{ $studentAttendChangeTsuika->student->name }}　様</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 5%">&nbsp;</div>
                <div class="under-student-name">保護者様</div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-2">
            <div class="school-name">坪田塾　{{ $studentAttendChangeTsuika->student_school[0]->name }}</div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>{{ $studentAttendChangeTsuika->student_school[0]->pref . ' ' . $studentAttendChangeTsuika->student_school[0]->address }}</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>{{ $studentAttendChangeTsuika->student_school[0]->building }}</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div></div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div></div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-9">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">平素は格別のご高配に賜り、誠にありがとうございます。</div>
        <div class="clr"></div>
    </div>
    <div class="row-10">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">下記の通り追加コマのお申込みを受付いたしましたので、ご連絡させて頂きます。</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-11">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">
            <table class="tsuika-request">
                <tr>
                    <td style="width: 20%">追加授業日</td>
                    <td style="width: 15%">曜日</td>
                    <td style="width: 35%">時間帯</td>
                    <td style="width: 15%">時間数</td>
                    <td style="width: 15%">教科</td>
                </tr>
                @php
                    $tsuikaRequests = json_decode($studentAttendChangeTsuika->tsuika_request, JSON_UNESCAPED_UNICODE);
                    $totalHours = array_sum(array_column($tsuikaRequests, 'hours'));
                @endphp
                @foreach($tsuikaRequests as $tsuikaRequest)
                <tr>
                    <td>{{ \JJSS::displayDate($tsuikaRequest['add_date']) }}</td>
                    <td>{{ \JJSS::displayWeek($tsuikaRequest['add_date']) }}</td>
                    <td>{{ \JJSS::displayTime($tsuikaRequest['start_time']) }} ～ {{ \JJSS::displayTime($tsuikaRequest['end_time']) }}</td>
                    <td>{{ $tsuikaRequest['hours'] }}</td>
                    <td>{{ $tsuikaRequest['kyouka'] }}</td>
                </tr>
                @endforeach
            </table>
        </div>
        <div class="row-indent-right"></div>
        <div class="clr"></div>
    </div>
    <br>
    <br>
    <div class="row-12">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">◆追加時間数</div>
        <div class="col-2">計</div>
        <div class="col-3">{{ $totalHours }}</div>
        <div class="col-4">時間　× 単価</div>
        <div class="col-5">{{ number_format($studentAttendChangeTsuika->unit_price) }}</div>
        <div class="col-6">円 ＝</div>
        <div class="col-7">{{ number_format($totalHours * $studentAttendChangeTsuika->unit_price) }}</div>
        <div class="col-8">円</div>
        <div class="col-9">（税抜）</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-13">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">
            ※追加コマの料金は、次回のお月謝と合わせてご請求させて頂きます。<br>
            ※別途消費税がかかります。
        </div>
        <div class="clr"></div>
    </div>
    @if ($copyFor === 'home')
    <br>
    <div class="row-last">
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-1">
            <table>
                <tr>
                    <th class="bordered">担当</th>
                    <th class="bordered">校長</th>
                </tr>
                <tr>
                    <td class="bordered" style="height: 60px;">&nbsp;</td>
                    <td class="bordered">&nbsp;</td>
                </tr>
            </table>
        </div>
        <div class="clr"></div>
    </div>
    @endif
</div>
