@extends('layouts.default')
@section('content')
<style>
  .set_orderby {cursor:pointer;}
</style>
<section class="scroll_table-wrap wht_bloc">

  <h2 class="heading-1">校舎一覧</h2>

  <div class="flex flex-j-between flex-a-ctr mgn-btm16">
    <form id="form" action="{{  url('master/school') }}" class="mgn-btm16" >
      <div class="form-group flex">
        <input type="text" id="search_name" name="search_name" placeholder="校舎名で検索" class="input-sm" value="{{ ($search['name']) ?? '' }}">
        &nbsp;
        <button id="btn-search">検索<i class='bx bx-search'></i></button>
        <div class="loading" style="display: none;"><i class='bx bx-loader-alt' ></i></div>

        <input type="hidden" name="orderby_column_name" value="{{ ($orderby['column_name']) ?? '' }}">
        <input type="hidden" name="orderby_type" value="{{ ($orderby['type']) ?? '' }}">
      </div>
    </form>
    <div class="btn_list">
      <button id="create" onclick="location.href='{{ url('master/school'). '/new' }}';"><i class='bx bxs-user-plus'></i>追加</button>
    </div>
  </div>

  <div class="scroll_table mgn-btm16">
    <table class="table-horizon table-striped datatable">
      <thead>
        <tr>
          <th class="set_orderby" dt-column="name">校舎名</th>
          <th class="set_orderby" dt-column="pref">都道府県</th>
          <th class="set_orderby" dt-column="price_table">金額テーブル</th>
          <th>座席数</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      @if(count($schools) == 0)
        <tr>
          <td colspan="4">
            登録されていません
          </td>
        </tr>
      @else
        @foreach ($schools as $school)
        <tr style="">
          <td class="">{{ $school->name }}</td>
          <td class="">{{ $school->pref }}</td>
          <td class="">{{ $school->price_table }}</td>
          <td class="">{{ $school->seats }}</td>
          <td class="txt-rgt">
            <button class="btn-sm" onclick="location.href='{{ url('master/school'). '/'. $school->id }}';">変更</button>
          </td>
        </tr>
        @endforeach
      @endif
      </tbody>
    </table>
  </div>
  <div class="flex flex-j-between">
    <div class="btn_list">
    </div>
    <div class="pager txt-ctr">
      {{ $schools->appends(Request::only(['search_name', 'orderby_column_name', 'orderby_type']))->links() }}
    </div>
  </div>
</section>
@endsection

@section('script')
<script>
$(function(){
  $('#btn-search').on('click', function() {
    $('#form').submit();
  });

  $('.set_orderby').on('click', function() {
    var current_orderby = $('input[name="orderby_type"]').val();
    var current_column = $('input[name="orderby_column_name"]').val();
    var new_column = $(this).attr('dt-column');

    if (current_column == new_column && current_orderby == "asc") {
      $('input[name="orderby_type"]').val('desc');
    } else {
      $('input[name="orderby_column_name"]').val(new_column);
      $('input[name="orderby_type"]').val('asc');
    }

    $('#form').submit();
  });
});
</script>
@endsection