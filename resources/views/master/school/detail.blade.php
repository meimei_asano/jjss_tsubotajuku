@extends('layouts.default')
@section('content')
<form id="form" method="post" action="{{ url('/master/school') }}">
  {{ csrf_field() }}
  <section class="scroll_table-wrap wht_bloc">
  <h2 class="heading-1">校舎情報</h2>

  <div class="">
    <table class="datatable table-vertical">
      <tr>
        <th>校舎名</th>
        <td class="form-group">
          <input type="text" id="name" name="name" class="" value="{{ old('name', $school->name) }}">
        </td>
      </tr>
      <tr>
        <th>校舎コード</th>
        <td class="form-group">
          <input type="text" id="school_code" name="school_code" value="{{ old('school_code', $school->school_code) }}" maxlength="3" placeholder="3桁の数値">
        </td>
      </tr>
      <tr>
        <th>郵便番号</th>
        <td class="form-group">
          <input type="text" id="zip_code" name="zip_code" value="{{ old('zip_code', $school->zip_code) }}" maxlength="7" placeholder="7桁の数値">
        </td>
      </tr>
      <tr>
        <th>都道府県</th>
        <td class="form-group">
          <select name="pref">
            <option value="">---</option>
            @foreach(config('pref') as $pref)
              <option value="{{ $pref }}" {{ old('pref', $school->pref) == $pref ? 'selected="selected"' : '' }} >{{ $pref }}</option>
            @endforeach
          </select>
        </td>
      </tr>
      <tr>
        <th>市区町村・番地</th>
        <td class="form-group">
          <input type="text" id="address" name="address" class="input-lg" value="{{ old('address', $school->address) }}">
        </td>
      </tr>
      <tr>
        <th>ビル名・階</th>
        <td class="form-group">
          <input type="text" id="building" name="building" class="input-lg" value="{{ old('building', $school->building) }}" >
        </td>
      </tr>
      <tr>
        <th>TEL</th>
        <td class="form-group">
          <input type="text" id="tel" name="tel" value="{{ old('tel', $school->tel) }}">
        </td>
      </tr>
      <tr>
        <th>FAX</th>
        <td class="form-group">
          <input type="text" id="fax" name="fax" value="{{ old('fax', $school->fax) }}">
        </td>
      </tr>
      <tr>
        <th>金額テーブル</th>
        <td class="form-group">
          <select name="price_table">
            <option value="">---</option>
            <option value="A" {{ old('price_table', $school->price_table) == 'A' ? 'selected="selected"' : '' }} >A</option>
            <option value="B" {{ old('price_table', $school->price_table) == 'B' ? 'selected="selected"' : '' }} >B</option>
          </select>
        </td>
      </tr>
      <tr>
        <th>座席数</th>
        <td class="form-group">
          <input type="text" id="seats" name="seats" class="" value="{{ old('seats', $school->seats) }}">
        </td>
      </tr>
      <tr>
        <th>振込口座</th>
        <td class="form-group">
          <textarea id="furikomisaki" name="furikomisaki">{{ old('furikomisaki', $school->furikomisaki) }}</textarea>
        </td>
      </tr>
    </table>
  </div>
  <div class="datatable btn_list">
    <button type="button" id="cancel" class="btn-info btn-line btn-lg">キャンセル</button>
    <button type="button" id="save" class="btn-lg">保存</button>
    <input type="hidden" id="school_id" name="school_id" value="{{ $school->id }}">
  </div>
</div>
</form>
@endsection

@section('script')
  <script>
    $(function(){
      $('#save').on('click', function() {
        $('#form').submit();
      });

      $('#cancel').on('click', function() {
        location.href = '{{ url('/master/school') }}';
      })
    })
  </script>
@endsection
