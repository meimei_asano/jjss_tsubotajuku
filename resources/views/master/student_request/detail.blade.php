@extends('layouts.default')
@section('content')
<style>
.set_container {
  position: relative;
  display: inline-block;
}
.remove_text_request_field {
  position: absolute;
  color: white;
  font-weight: bolder;
  background-color: red;
  border: solid 1px white;
  border-radius: 50%;
  padding: 0 8px;
  cursor: pointer;
  right: -20px;
  top: 15px;
}
.remove_text_request_field:hover {
  color: red;
  font-weight: bolder;
  background-color: white;
  border: solid 1px red;
}
</style>
<form id="form" method="post" action="">
  {{ csrf_field() }}
  <section class="scroll_table-wrap wht_bloc">
  <h2 class="heading-1">休塾、卒塾、退塾願情報</h2>

  @if($studentRequest->syounin_torikeshi_shinsei_flag == "1")
    <div class="alert alert-danger flex flex-a-ctr">
      承認取消申請中です。
    </div>
  @endif

  <div class="">
    <table class="datatable table-vertical">
      <tr>
        <th>生徒</th>
        <td class="form-group">
          <select id="student_id" name="student_id">
            <option class="default" value="">選択してください</option>
            @if(count($students) > 0)
              @foreach($students as $student)
                <option value="{{ $student->id }}" {{ $student->id == old('student_id', $studentRequest->student_id) ? 'selected="selected"' : '' }} >{{ $student->name }}</option>
              @endforeach
            @endif
          </select>
        </td>
      </tr>
      <tr>
        <th>申込日</th>
        <td class="form-group">
          <input type="text" id="request_date" name="request_date" class="datepicker-here" autocomplete="off" data-language="jp" data-auto-close="true" value="{{ old('request_date', $studentRequest->request_date) }}"/>
        </td>
      </tr>
      <tr>
        <th>願い種別</th>
        <td class="form-group">
          @php
            $requestType = old('request_type', $studentRequest->request_type);
          @endphp
          <select id="request_type" name="request_type">
            <option class="default" value="">願い種別</option>
            @foreach(config('jjss.request_type') as $val => $label)
              <option value="{{ $val }}" {{ ($val == $requestType) ? 'selected="selected"' : '' }} >{{ $label }}</option>
            @endforeach
          </select>
        </td>
      </tr>
      <tr class="cooling_off_flag_area" style="<?php echo (($requestType === null || $requestType != "退塾") ? "display:none;" : ""); ?>" >
        <th>クーリングオフ</th>
        <td class="form-group">
          @php
            $coolingOffFlag = old('cooling_off_flag', $studentRequest->cooling_off_flag);
          @endphp
          <ul class="flex">
            <li>
              <input type="checkbox" name="cooling_off_flag" id="cooling_off_flag" value="1" <?php echo ($coolingOffFlag == "1" ? 'checked="checked"' : '') ?>><label for="cooling_off_flag">クーリングオフ</label>
            </li>
          </ul>
        </td>
      </tr>
      <tr>
        <th>適用日付</th>
        <td class="form-group">
          @php
              $displayFullDate = (in_array($requestType, ['卒塾', '退塾']) && $coolingOffFlag != "1") ? 'display: none' : '';
              $displayYearMonth = (in_array($requestType, ['', '休塾']) || (in_array($requestType, ['退塾']) && $coolingOffFlag == "1")) ? 'display: none' : '';
 //             $displayFullDate = in_array($requestType, ['卒塾', '退塾']) ? 'display: none' : '';
 //             $displayYearMonth = in_array($requestType, ['', '休塾']) ? 'display: none' : '';
              $targetDate = old('target_date', $studentRequest->target_date);
              $targetFullDate = '';
              $targetYear = '';
              $targetMonth = '';

              if (in_array($requestType, ['', '休塾']) || (in_array($requestType, ['退塾']) && $coolingOffFlag == "1")) {
                  $targetFullDate = is_array($targetDate) ? $targetDate['target_full_date'] : $targetDate;
                  $targetYear = date('Y');
                  $targetMonth = date('m');
              } else {
                  $targetYear = is_array($targetDate)
                      ? $targetDate['target_year']
                      : ($targetDate
                          ? date('Y', strtotime($targetDate))
                          : date('Y'));
                  $targetMonth = is_array($targetDate)
                      ? $targetDate['target_month']
                      : ($targetDate
                          ? date('m', strtotime($targetDate))
                          : date('m'));
              }
          @endphp
          <div id="target_full_date_container" style="{{ $displayFullDate }}">
            <input type="text" name="target_date[target_full_date]" class="datepicker-here" autocomplete="off" data-language="jp" data-auto-close="true" value="{{ $targetFullDate }}"/>
          </div>
          <div id="target_year_month_container" style="{{ $displayYearMonth }}">
            <select name="target_date[target_year]">
              <option class="default" value="">年</option>
              @for($year = intval(date('Y')) - 1; $year <= intval(date('Y')) + 3; $year++)
                <option value="{{ $year }}" {{ ($year == $targetYear) ? 'selected="selected"' : '' }}>{{ $year }}</option>
              @endfor
            </select>
            年
            <select name="target_date[target_month]">
              <option class="default" value="">月</option>
              @for ($month = 1; $month <= 12; $month++)
                <option value="{{ str_pad($month, 2, '0', STR_PAD_LEFT) }}" {{ ($month == $targetMonth) ? 'selected="selected"' : '' }}>{{ str_pad($month, 2, '0', STR_PAD_LEFT) }}</option>
              @endfor
            </select>
            月末
          </div>
        </td>
      </tr>
      <tr>
        <th>休塾最終日付</th>
        <td class="form-group">
          @php
              $displayKyujyuku = in_array($requestType, ['卒塾', '退塾']) ? 'display: none' : '';
          @endphp
          <div id="kyujyuku_end_date_container" style="{{ $displayKyujyuku }}">
            <input type="text" id="kyujyuku_end_date" name="kyujyuku_end_date" class="datepicker-here" autocomplete="off" data-language="jp" data-auto-close="true" value="{{ old('kyujyuku_end_date', $studentRequest->kyujyuku_end_date) }}"/>
          </div>
        </td>
      </tr>
      <tr>
        <th>理由</th>
        <td class="form-group">
          <textarea id="reason" name="reason" autocomplete="off" placeholder="理由" maxlength="300">{{ old('reason', $studentRequest->reason) }}</textarea>
          {{-- <input type="text" id="reason" name="reason" autocomplete="off" placeholder="理由" value="{{ old('reason', $studentRequest->reason) }}"/> --}}
        </td>
      </tr>
      <tr>
        <th>登録先生</th>
        <td class="form-group">
          {{ $studentRequest->teacher_id === null ? '' : (isset($studentRequest->teacher->name) === true ? $studentRequest->teacher->name : '') }}
        </td>
      </tr>
      <tr>
        <th>承認先生</th>
        <td class="form-group">
          {{ $studentRequest->syounin_teacher_id === null ? '未承認' : $studentRequest->syounin_teacher->name }}
        </td>
      </tr>
    </table>
  </div>
  <div class="datatable btn_list">
    {{-- キャンセル（一覧に遷移）--}}
    <button type="button" id="cancel" class="btn-info btn-line btn-lg btn-lg-slim">キャンセル</button>
    {{-- 申請（登録・更新） --}}
    @php
      $add_class = 'is_disabled';
      if(in_array($permission, ['校長', 'マネージャー', '本部スタッフ']) === false) {
        if($studentRequest->id === null) {
          $add_class = '';
        }
        if($studentRequest->id !== null && $studentRequest->syounin_teacher_id === null &&
            ($studentRequest->teacher_id == $teacher->id || in_array($permission, ['管理者']))) {
          $add_class = '';
        }
      }
    @endphp
    <button type="button" id="save" class="btn-lg {{ $add_class }}">申請</button>
    @if($studentRequest->id !== null)
      {{-- 申請取消（削除） --}}
      @php
        $add_class = 'is_disabled';
        if($studentRequest->teacher_id == $teacher->id || in_array($permission, ['管理者'])) {
          if($studentRequest->id !== null && $studentRequest->syounin_teacher_id === null) {
            $add_class = '';
          }
        }
      @endphp
      <button type="button" id="torikeshi" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">申請取消</button>
      {{-- 承認 --}}
      @if(in_array($permission, ['校長', 'マネージャー', '管理者']))
        @if($studentRequest->teacher_id != $teacher->id)
        <button type="button" id="syounin" class="btn-lg {{ ($studentRequest->syounin_teacher_id !== null || $studentRequest->id === null) ? 'is_disabled' : '' }}">承認</button>
        @endif
      @endif
      {{-- 印刷 --}}
      <button type="button" id="print" class="btn-lg btn-lg-slim {{ ($studentRequest->syounin_teacher_id === null || $studentRequest->id === null || $studentRequest->syounin_torikeshi_shinsei_flag === 1) ? 'is_disabled' : '' }}">印刷</button>
      {{-- 承認取消申請 --}}
      @php
        $add_class = 'is_disabled';
        if($studentRequest->teacher_id == $teacher->id || in_array($permission, ['管理者'])) {
          if($studentRequest->syounin_teacher_id !== null && $studentRequest->syounin_torikeshi_shinsei_flag !== 1) {
            $add_class = '';
          }
        }
      @endphp
      <button type="button" id="syounin_torikeshi_shinsei" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">承認取消申請</button>
      {{-- 承認取消 --}}
      @php
        $add_class = 'is_disabled';
        if(in_array($permission, ['校長', 'マネージャー', '管理者'])) {
          if($studentRequest->teacher_id != $teacher->id) {
            if($studentRequest->syounin_teacher_id !== null && $studentRequest->syounin_torikeshi_shinsei_flag == "1") {
              $add_class = '';
            }
          }
      @endphp
      <button type="button" id="syounin_torikeshi" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">承認取消</button>
      @php
        }
      @endphp
    @endif
    <input type="hidden" id="id" name="id" value="{{ $studentRequest->id }}">
  </div>
</div>
</form>
@endsection
@section('script')
  <script>
    $(function(){
      $('#student_id').select2({ language: "ja"});

      $('#save').on('click', function() {
        $('#form').attr('action', '{{ url("/master/student_request/save") }}');
        $('#form').submit();
      });

      $('#torikeshi').on('click', function() {
        swal({
          text: "申請を取消します。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/master/student_request_delete") }}');
            $('#form').submit();
          }
        });
      });

      $('#syounin_torikeshi_shinsei').on('click', function() {
        swal({
          text: "承認取消申請を行います。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/master/student_request_torikeshi_shinsei") }}');
            $('#form').submit();
          }
        });
      });

      $('#print').on('click', function() {
        window.open('{{ url('/master/student_request/print'). '/'. $studentRequest->id }}', '_blank');
      });

      $('#cancel').on('click', function() {
        location.href = '{{ url("/master/student_request") }}';
      });

      $('body').on('change', '#request_type, #cooling_off_flag', function(e) {
        let requestType = $('#request_type').val();
        let coolingOffFlag = $('#cooling_off_flag').prop('checked');

        if (requestType === '' || requestType === '休塾') {
          $('#kyujyuku_end_date_container').show();
          $('#target_full_date_container').show();
          $('#target_year_month_container').hide();
        } else if(coolingOffFlag == true) {
          $('#kyujyuku_end_date_container').hide();
          $('#target_full_date_container').show();
          $('#target_year_month_container').hide();
        } else {
          $('#kyujyuku_end_date_container').hide();
          $('#target_full_date_container').hide();
          $('#target_year_month_container').show();
        }

        if(requestType == '退塾') {
          $('.cooling_off_flag_area').show();
        } else {
          $('#cooling_off_flag').prop('checked', false);
          $('.cooling_off_flag_area').hide();
        }
      });
/*
      $('body').on('change', '#request_type', function(e) {
        let requestType = $(this).val();

        if (requestType === '' || requestType === '休塾') {
          $('#kyujyuku_end_date_container').show();
          $('#target_full_date_container').show();
          $('#target_year_month_container').hide();
        } else {
          $('#kyujyuku_end_date_container').hide();
          $('#target_full_date_container').hide();
          $('#target_year_month_container').show();
        }
      });
*/

      @if (session()->has('alert.success'))
        @if(session('alert.success') == "承認済み。" && isset($studentRequest->id) === true)
          window.open('{{ url('/master/student_request/print'). '/'. $studentRequest->id }}', '_blank');
          location.href = '{{ url("/master/student_request") }}';
        @endif
      @endif

    })
  </script>
  @if(in_array($permission, ['校長', 'マネージャー', '管理者']))
  <script>
    $(function(){
      $('#syounin').on('click', function() {
        $('#form').attr('action', '{{ url("/master/student_request/syounin") }}');
        $('#form').submit();
      });

      $('#syounin_torikeshi').on('click', function() {
        swal({
          text: "承認取消を行います。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/master/student_request_torikeshi") }}');
            $('#form').submit();
          }
        });
      });

    })
  </script>
  @endif
@endsection
