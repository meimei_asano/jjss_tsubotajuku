@extends('layouts.default')
@section('content')
<form id="form" method="post" action="">
  {{ csrf_field() }}
  <section class="scroll_table-wrap wht_bloc">
    <h2 class="heading-1">範囲設定</h2>

    <div class="scroll_table mgn-btm16">
      <table class="table-unit_list datatable table-horizon form-group">
        <colgroup>
          <col style="width:30%;">
          <col style="">
          <col style="width:15%;">
          <col style="width:15%;">
          <col style="width:30%;">
        </colgroup>
        <thead>
          <tr>
            <td colspan="5">
              <p class="heading-2">{{ $text->kamoku->name }}　：　{{ $text->name }}</p>
            </td>
          </tr>
          <tr>
            <th></th>
            <th>学習順</th>
            <th>名称</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        <tbody>
        @if(count($text_units) > 0)
          @foreach($text_units as $unit)
            <tr>
              <td></td>
              <td>
                {{ $unit->unit_num }}
              </td>
              <td>
                <input type="text" name="name[]" value="{{ $unit->name }}" class="txt-rgt" style="min-width:20px;width:100%;">
              </td>
              <td class="txt-ctr">
                <input type="hidden" name="text_unit_id[]" value="{{ $unit->id }}" >

                <button type="button" class="add_row btn-line" >＋</button>
                <button type="button" class="del_row btn-line" >−</button>
              </td>
              <td></td>
            </tr>
          @endforeach
        @endif
          <tr>
            <td></td>
            <td></td>
            <td>
              <input type="text" name="name[]" value="" class="txt-rgt" style="min-width:20px;width:100%;">
            </td>
            <td class="txt-ctr">
              <input type="hidden" name="text_unit_id[]" value="" >

              <button type="button" class="add_row btn-line" >＋</button>
              <button type="button" class="del_row btn-line" >−</button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <div class="datatable btn_list txt-ctr">
      <button type="button" id="cancel" class="btn-info btn-line">戻る</button>
      <button type="button" id="save" class="btn-lg">保存</button>
      <input type="hidden" id="text_id" name="text_id" value="{{ $text_id }}">
    </div>
  </section>
</form>
@endsection

@section('script')
  <script src="{{ asset('js/jquery-ui.min.js') }}" defer></script>
  <script>
    $(function(){

      $('.table-unit_list tbody').sortable();

      $(document).on("click", ".add_row", function(){
        var t = $(this).parent().parent();
        var o = $(this).parent().parent().clone(true);
        o.find('select, input[type="text"], input[type="hidden"]').val('');
        o.insertAfter(t);
      });

      $(document).on("click", ".del_row", function(){
        if($(".table-unit_list tbody").children().length == 1) {
          $(".add_row").trigger("click");
        }
        $(this).parent().parent().remove();
      });

      $('#save').on('click', function() {
        $('#form').attr('action', '{{ url('/master/text_unit') }}');
        $('#form').submit();
      });

      $('#cancel').on('click', function() {
        location.href = '{{ url('/master/text') }}';
      })
    })
  </script>
@endsection
