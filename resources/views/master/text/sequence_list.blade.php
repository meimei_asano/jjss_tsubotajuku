@extends('layouts.default')

@section('content')
<style>
  .mukou_line { background-color: #ebebeb; }
</style>
<section class="scroll_table-wrap wht_bloc">

  <h2 class="heading-1">教材：学習順序の設定</h2>

  <div class="flex flex-j-between flex-a-ctr mgn-btm16">

  <div class="form-group flex">

    <select id="kamoku_id" name="kamoku_id" >
      <option value="">科目</option>
      {!! \JJSS::options($kamokus, 'id', 'name', ($kamoku_id) ?? '') !!}
    </select>

    　<button id="btn-search">検索<i class='bx bx-search'></i></button>
    <div class="loading" style="display: none;"><i class='bx bx-loader-alt' ></i></div>
  </div>

  </div>

  <form id="form2" action="{{ url('master/text_sequence') }}" method="post">
    <div class="scroll_table mgn-btm16">
      {{ csrf_field() }}
      <table class="table-horizon table-striped datatable table-text_units flt-ctr" >
        <colgroup>
          <col style="width:15%;">
          <col style="width:10%;">
          <col style="">
          <col style="width: 5%;">
          <col style="width: 5%;">
          <col style="width:20%;">
        </colgroup>
        <thead>
          <tr>
            <th></th>
            <th>学習順</th>
            <th>教材・テキスト名</th>
            <th>逆テスト</th>
            <th>学習順設定</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
        @if(!$texts)
          <tr>
            <td colspan="6">
              登録されていません
            </td>
          </tr>
        @else
          @foreach($texts as $text)
          <tr>
            <td></td>
            <td class="txt-rgt">{{ $text->sequence or '' }}</td>
            <td class="">{{ $text->name or '' }}</td>
            <td class="txt-ctr">{{ ($text->has_reverse_test == "1" ? "有" : "") }}</td>
            <td class="form-group">
              <select name="mukou_flag[]" class="mukou_flag">
                <option value="">有効</option>
                <option value="1" {{ strlen($text->sequence)==0 ? 'selected="true"' : '' }}>無効</option>
              </select>
            </td>
            <td>
              <input type="hidden" name="text_id[]" value="{{ $text->id or null }}" >
              <input type="hidden" name="kamoku_id[]" value="{{ $text->kamoku_id or null }}" >
            </td>
          </tr>
          @endforeach
        @endif
        </tbody>
      </table>
    </div>
    <div class="btn_list txt-ctr">
      <button type="button" class="cancel btn-info btn-line">戻る</button>
      <button type="button" class="touroku_sequence btn-lg">登録する</button>
      <input type="hidden" name="kamoku_id" value="{{ $kamoku_id or null}}" >
    </div>
  </form>
</section>
@endsection


@section('script')
  <script src="{{ asset('js/jquery-ui.min.js') }}" defer></script>
<script>
  $(function(){

    $('.table-text_units tbody').sortable();

    $('#btn-search').on('click', function() {
      window.location.href = "{{ url('/master/text_sequence/') }}/" + $('#kamoku_id').val();
    });

    $(document).on("change", ".mukou_flag", function() {
      if($(this).val() == "1") {
        $(this).parent().parent().find('td').addClass('mukou_line');
      } else {
        $(this).parent().parent().find('td').removeClass('mukou_line');
      }
    });

    $(".mukou_flag").each(function() {
      $(this).trigger("change");
    });

    $('.touroku_sequence').on('click', function() {

      //check1
      var kamoku_id = [];
      $('input[name^="kamoku_id"]').each(function() {
        kamoku_id.push($(this).val());
      });
      var kamoku_ids = kamoku_id.filter(function (x, i, self) {
        return self.indexOf(x) === i;
      });
      if(kamoku_ids.length > 1) {
        swal("", "同一科目で登録してください。", "");
        return;
      }

      //check2
      var f = 0;//0:ok, 1:ng
      $('input[name^="sequence"]').each(function() {
        if($(this).val() == "") {
          f = 1;
          return;
        }
      });
      if(f == 1) {
        swal("", "全ての学習順を入力してください。", "");
        return;
      }

      //submit
      $('#form2').submit();

    });

    $('.cancel').on('click', function() {
      location.href = '{{ url('/master/text') }}';
    })

  });
</script>
@endsection

