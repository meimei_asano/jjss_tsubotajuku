@extends('layouts.default')
@section('content')
<style>
  .set_orderby {cursor:pointer;}
</style>
<section class="scroll_table-wrap wht_bloc">

  <h2 class="heading-1">教材一覧</h2>

  <div class="flex flex-j-between flex-a-ctr mgn-btm16">

    <form id="form" action="{{  url('master/text') }}" class="mgn-btm16" >
      <div class="form-group flex">
        <input type="text" id="search_name" name="search_name" placeholder="教材名" class="input-sm" value="{{ ($search['name']) ?? '' }}">
        &nbsp;
        <select id="search_kamoku_id" name="search_kamoku_id" >
          <option value="">科目</option>
          {!! \JJSS::options($kamokus, 'id', 'name', ($search['kamoku_id']) ?? '') !!}
        </select>
        &nbsp;
        <button id="btn-search">検索<i class='bx bx-search'></i></button>
        <div class="loading" style="display: none;"><i class='bx bx-loader-alt' ></i></div>

        <input type="hidden" name="orderby_column_name" value="{{ ($orderby['column_name']) ?? '' }}">
        <input type="hidden" name="orderby_type" value="{{ ($orderby['type']) ?? '' }}">
      </div>
    </form>

    <div>
      <a href="{{ url('/master/text') . '/new' }}"><button type="button"><i class="bx bx-plus"></i>追加</button></a>
      <!--
      <a href="{{ url('/master/text_sequence') }}"><button type="button">学習順序の設定</button></a>
      -->
    </div>

    {{--
    <div class="btn_list">
      <button id="create" onclick="location.href='{{ url('master/text'). '/new' }}';"><i class='bx bxs-user-plus'></i>追加</button>
    </div>
--}}
  </div>
  <div class="scroll_table mgn-btm16">
    <table class="table-horizon table-striped datatable">
      <thead>
        <tr>
          {{--
          <th class="th_check">
            <input type="checkbox" class="check" id="check_all" value="all">
            <label for="check_all"></label>
          </th>
          --}}
          <th class="set_orderby" dt-column="kyouka_code">教科</th>
          <th class="set_orderby" dt-column="kamoku_name">科目</th>
          <th class="set_orderby" dt-column="name">教材・テキスト名</th>
          <th>価格</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      @if(count($texts) == 0)
        <tr>
          <td colspan="4">
            登録されていません
          </td>
        </tr>
      @else
        @foreach ($texts as $text)
        <tr style="">
          <td class="">{{ $text->kamoku->kyouka_code or null }}</td>
          <td class="">{{ $text->kamoku->name or null }}</td>
          <td class="">{{ $text->name or null }}</td>
          <td class="">{{ $text->price or null }}</td>
          <td class="txt-rgt">
            <button class="btn-sm" onclick="location.href='{{ url("/master/text_unit/" . $text->id) }}';">範囲設定</button>
            <button class="btn-sm" onclick="location.href='{{ url("/master/text/" . $text->id) }}';">変更</button>
          </td>
        </tr>
        @endforeach
      @endif
      </tbody>
    </table>
  </div>
  <div class="flex flex-j-between">
    <div class="pager txt-ctr">
      {{ $texts->appends(Request::only(['search_name', 'search_kamoku_id', 'orderby_column_name', 'orderby_type']))->links() }}
    </div>
  </div>
</section>
@endsection

@section('script')
<script>
$(function(){
  $('#btn-search').on('click', function() {
    $('#form').submit();
  });

  $('.set_orderby').on('click', function() {
    var current_orderby = $('input[name="orderby_type"]').val();
    var current_column = $('input[name="orderby_column_name"]').val();
    var new_column = $(this).attr('dt-column');

    if (current_column == new_column && current_orderby == "asc") {
      $('input[name="orderby_type"]').val('desc');
    } else {
      $('input[name="orderby_column_name"]').val(new_column);
      $('input[name="orderby_type"]').val('asc');
    }

    $('#form').submit();
  });
});
</script>
@endsection
