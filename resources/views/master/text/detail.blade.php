@extends('layouts.default')
@section('content')
<form id="form" method="post" action="">
  {{ csrf_field() }}
  <section class="scroll_table-wrap wht_bloc">
  <h2 class="heading-1">教材情報</h2>
  <div class="">
    <table class="datatable table-vertical">
      <tr>
        <th>教科</th>
        <td class="form-group">
          <select name="kamoku_id">
            <option value="">---</option>
            {!! \JJSS::options($kamokus, 'id', 'name', ($text->kamoku_id) ?? '') !!}
          </select>
        </td>
      </tr>
      <tr>
        <th>教材名</th>
        <td class="form-group">
          <input type="text" name="name" class="input-lg" value="{{ old('name', $text->name) }}">
        </td>
      </tr>
      <tr>
        <th>売価（税抜）</th>
        <td class="form-group">
          <input type="number" name="price" class="txt-rgt" value="{{ old('price', (isset($text->price) === true ? $text->price : '')) }}">&nbsp;円
        </td>
      </tr>
      <tr>
        <th>逆テスト</th>
        <td class="form-group">
          <ul class="flex">
            <li>
             <input type="radio" name="has_reverse_test" id="has_reverse_test_0" value="0" {{ (isset($text->has_reverse_test) === true ? ($text->has_reverse_test != "1" ? 'checked="checked"' : '') : '') }}><label for="has_reverse_test_0">無</label>
            </li>
            <li>
              <input type="radio" name="has_reverse_test" id="has_reverse_test_1" value="1" {{ (isset($text->has_reverse_test) === true ? ($text->has_reverse_test == "1" ? 'checked="checked"' : '') : '') }}><label for="has_reverse_test_1">有</label>
            </li>
          </ul>
        </td>
      </tr>
      <tr>
        <th>全回数</th>
        <td class="form-group">
          <ul class="flex">
            <li>
              <input type="radio" name="units_by_student" id="units_by_student_0" value="0" {{ (isset($text->units_by_student) === true ? ($text->units_by_student != "1" ? 'checked="checked"' : '') : '') }}><label for="units_by_student_0">固定</label>
            </li>
            <li>
              <input type="radio" name="units_by_student" id="units_by_student_1" value="1" {{ (isset($text->units_by_student) === true ? ($text->units_by_student == "1" ? 'checked="checked"' : '') : '') }}><label for="units_by_student_1">生徒毎に決定</label>
            </li>
          </ul>
        </td>
      </tr>
      <tr>
        <th>枝分かれテキスト</th>
        <td class="form-group">
          <select name="branch_next_text_id">
            <option value="">---</option>
            {!! \JJSS::options($texts, 'id', 'name', ($text->branch_next_text_id) ?? '') !!}
          </select>
        </td>
      </tr>
    </table>
  </div>
  <div class="datatable btn_list txt-ctr">
    <button type="button" id="cancel" class="btn-info btn-line flt-lft">戻る</button>
  @if($text->id)
    <button type="button" id="delete" class="btn-warning">削除</button>
  @endif
    <button type="button" id="save" class="btn-lg">保存</button>
    <input type="hidden" id="text_id" name="text_id" value="{{ $text->id or '' }}">
  </div>
</div>
</form>
@endsection

@section('script')
  <script>
    $(function(){

      $('#save').on('click', function() {
        $('#form').attr('action', '{{ url('/master/text') }}');
        $('#form').submit();
      });

      $('#delete').on('click', function() {
        $('#form').attr('action', '{{ url('/master/text_delete') }}');
        $('#form').submit();
      });

      $('#cancel').on('click', function() {
        location.href = '{{ url('/master/text') }}';
      });

    })
  </script>
@endsection
