@extends('layouts.default')

@section('content')

  <form id="form" method="post" action="">
  {{ csrf_field() }}
  <section class="scroll_table-wrap wht_bloc">

    <h2 class="heading-1">講習期間情報</h2>

    <div class="">
      <table class="datatable table-vertical">
        <tr>
          <th>講習期間名</th>
          <td class="form-group">
            <input type="text" id="term_name" name="term_name" class="" value="{{ old('term_name', $term->term_name) }}">
          </td>
        </tr>
        <tr>
          <th>表示名</th>
          <td class="form-group">
            <input type="text" id="display_name" name="display_name" class="" value="{{ old('display_name', $term->display_name) }}">
          </td>
        </tr>
        <tr>
          <th>種別</th>
          <td class="form-group">
            @php
              $term_type_str = old('term_type', (isset($term->term_type) === true ? $term->term_type : ''));
            @endphp
            <ul class="flex">
              <li>
                <input type="radio" name="term_type" id="term_type1" value="通常" {{ $term_type_str == '通常' ? 'checked=checked' : '' }} ><label for="term_type1">通常</label>
              </li>
              <li>
                <input type="radio" name="term_type" id="term_type2" value="特別講習" {{ $term_type_str == '特別講習' ? 'checked=checked' : '' }} ><label for="term_type2">特別講習</label>
              </li>
            </ul>
          </td>
        </tr>
        <tr>
          <th>期間開始日</th>
          <td class="form-group">
            <input type='text' id="start_date" name="start_date" class="datepicker-here" data-language="jp" value="{{ old('start_date', $term->start_date) }}" >
          </td>
        </tr>
        <tr>
          <th>期間終了日</th>
          <td class="form-group">
            <input type='text' id="end_date" name="end_date" class="datepicker-here" data-language="jp" value="{{ old('end_date', $term->end_date) }}" >
          </td>
        </tr>
    </table>
  </div>
  <div class="datatable btn_list">
    <button type="button" id="cancel" class="btn-info btn-line btn-lg">キャンセル</button>
    <button type="button" id="save" class="btn-lg">保存</button>
    <input type="hidden" id="term_id" name="term_id" value="{{ $term->id }}">
  </div>
</div>
</form>
@endsection

@section('script')
  <script>
    $(function(){
      $('#save').on('click', function() {
        $('#form').attr('action', '{{ url('/master/term') }}');
        $('#form').submit();
      });

      $('#cancel').on('click', function() {
        location.href = '{{ url('/master/term') }}';
      })
    })
  </script>
@endsection
