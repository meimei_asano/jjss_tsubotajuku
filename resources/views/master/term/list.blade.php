@extends('layouts.default')
@section('content')
<style>
  .set_orderby {cursor:pointer;}
</style>
<section class="scroll_table-wrap wht_bloc">

  <h2 class="heading-1">講習期間一覧</h2>

  <div class="flex flex-j-between flex-a-ctr mgn-btm16">
    <form id="form" action="{{ url('master/term') }}" class="mgn-btm16" >
      <div class="form-group flex">
        <input type="text" id="search_name" name="search_name" placeholder="講習期間名" class="input-sm" value="{{ ($search['term_name']) ?? '' }}">
        &nbsp;
        <ul class="flex">
          <li>
            <input type="checkbox" name="search_old" id="search_old" {{ isset($search['search_old']) ? 'checked' : '' }}> <label for="search_old">過去の講習期間を表示する</label>
          </li>
        </ul>
        &nbsp;
        <button id="btn-search">検索<i class='bx bx-search'></i></button>
        <div class="loading" style="display: none;"><i class='bx bx-loader-alt' ></i></div>

        <input type="hidden" name="orderby_column_name" value="{{ ($orderby['column_name']) ?? '' }}">
        <input type="hidden" name="orderby_type" value="{{ ($orderby['type']) ?? '' }}">
      </div>
    </form>
    <div class="btn_list">
      <button id="create" onclick="location.href='{{ url('master/term'). '/new' }}';">追加</button>
    </div>
  </div>

  <div class="scroll_table mgn-btm16">
    <table class="table-horizon table-striped datatable">
      <thead>
        <tr>
          <th>講習期間名</th>
          <th>表示名</th>
          <th>種別</th>
          <th class="set_orderby" dt-column="start_date">期間</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      @if(count($terms) == 0)
        <tr>
          <td colspan="6">
            登録されていません
          </td>
        </tr>
      @else
        @foreach ($terms as $term)
        <tr style="">
          <td class="">{{ $term->term_name }}</td>
          <td class="">{{ $term->display_name }}</td>
          <td class="">{{ $term->term_type }}</td>
          <td class="">{{ $term->start_date }} ー {{ $term->end_date }}</td>
          <td class="txt-rgt">
            <button class="btn-sm" onclick="location.href='{{ url('master/term'). '/'. $term->id }}';">変更</button>
          </td>
        </tr>
        @endforeach
      @endif
      </tbody>
    </table>
  </div>

  <div class="pager txt-ctr">
    {{ $terms->appends(Request::only(['search_name', 'search_old', 'orderby_column_name', 'orderby_type']))->links() }}
  </div>
  
</section>
@endsection

@section('script')
<script>
$(function(){
  $('#btn-search').on('click', function() {
    $('#form').submit();
  });

  $('.set_orderby').on('click', function() {
    var current_orderby = $('input[name="orderby_type"]').val();
    var current_column = $('input[name="orderby_column_name"]').val();
    var new_column = $(this).attr('dt-column');

    if (current_column == new_column && current_orderby == "asc") {
      $('input[name="orderby_type"]').val('desc');
    } else {
      $('input[name="orderby_column_name"]').val(new_column);
      $('input[name="orderby_type"]').val('asc');
    }

    $('#form').submit();
  });
});
</script>
@endsection