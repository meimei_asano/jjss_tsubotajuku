@extends('layouts.default')
@section('content')
<form id="form" method="post" action="">
  {{ csrf_field() }}
  <section class="scroll_table-wrap wht_bloc">
  <h2 class="heading-1">コース変更情報</h2>
  <div class="">
    <table class="datatable table-vertical">
      <tr>
        <th>生徒</th>
        <td class="form-group">
          @if($studentCourseChange->id)
          {{ $studentCourseChange->student->name }}
          <input type="hidden" name="student_id" value="{{ $studentCourseChange->student_id }}">
          @else
          <select id="student_id" name="student_id">
            <option class="default" value="">選択してください</option>
            @if(count($students) > 0)
              @foreach($students as $student)
                <option value="{{ $student->id }}" {{ $student->id == old('student_id', $studentCourseChange->student_id) ? 'selected="selected"' : '' }} >{{ $student->name }}</option>
              @endforeach
            @endif
          </select>
          @endif
        </td>
      </tr>
      <tr>
        <th>申込日</th>
        <td class="form-group">
          <input type="text" id="request_date" name="request_date" class="datepicker-here" autocomplete="off" data-language="jp" data-auto-close="true" value="{{ old('request_date', $studentCourseChange->request_date) }}"/>
        </td>
      </tr>
      <tr>
        <th>変更予定日</th>
        <td class="form-group">
          @php
              $yoteiDate = old('change_yotei_date', $studentCourseChange->change_yotei_date);
              $yoteiYear = '';
              $yoteiMonth = '';

              $yoteiYear = is_array($yoteiDate)
                  ? $yoteiDate['year']
                  : ($yoteiDate
                      ? date('Y', strtotime($yoteiDate))
                      : date('Y'));
              $yoteiMonth = is_array($yoteiDate)
                  ? $yoteiDate['month']
                  : ($yoteiDate
                      ? date('m', strtotime($yoteiDate))
                      : date('m'));
          @endphp
          <select name="change_yotei_date[year]">
            <option class="default" value="">年</option>
            @for($year = intval(date('Y')) - 1; $year <= intval(date('Y')) + 3; $year++)
              <option value="{{ $year }}" {{ ($year == $yoteiYear) ? 'selected="selected"' : '' }}>{{ $year }}</option>
            @endfor
          </select>
          年
          <select name="change_yotei_date[month]">
            <option class="default" value="">月</option>
            @for ($month = 1; $month <= 12; $month++)
              <option value="{{ str_pad($month, 2, '0', STR_PAD_LEFT) }}" {{ ($month == $yoteiMonth) ? 'selected="selected"' : '' }}>{{ str_pad($month, 2, '0', STR_PAD_LEFT) }}</option>
            @endfor
          </select>
          月
        </td>
      </tr>
      <tr>
        <th>現在のコース</th>
        <td class="form-group">
          <div class="current_course_name">{{ $studentCourseChange->current_course ? $studentCourseChange->current_course->name : '（自動取得）' }}</div>
          <input type="hidden" name="current_course_id" value="{{ $studentCourseChange->current_course_id }}">
        </td>
      </tr>
      <tr>
        <th>コース</th>
        <td class="form-group">
          @php
            $courseId = old('course_id', $studentCourseChange->course_id);
          @endphp
          <select id="course_id" name="course_id">
            <option class="default" value="">選択してください</option>
            @if(count($courses) > 0)
              @foreach($courses as $course)
                <option value="{{ $course->id }}" {{ $course->id == $courseId ? 'selected="selected"' : '' }} >{{ $course->name }}</option>
              @endforeach
            @endif
          </select>
        </td>
      </tr>
      <tr>
        <th>通塾予定</th>
        <td class="form-group">
          <div class="mgn-btm8" style="margin-left: 2.4em;">
            ※手入力が可能です
          </div>
          <div class="mgn-btm8">
            <b>月</b>
            <input type='text' id="mon_start_time" name="mon_start_time" class="timepicker input-mn" value="{{ old('mon_start_time', (isset($studentCourseChange->mon_start_time) === true ? $studentCourseChange->mon_start_time : '')) }}" > 〜
            <input type='text' id="mon_end_time" name="mon_end_time" class="timepicker input-mn" value="{{ old('mon_end_time', (isset($studentCourseChange->mon_end_time) === true ? $studentCourseChange->mon_end_time : '')) }}" >
          </div>
          <div class="mgn-btm8">
            <b>火</b>
            <input type='text' id="tue_start_time" name="tue_start_time" class="timepicker input-mn" value="{{ old('tue_start_time', (isset($studentCourseChange->tue_start_time) === true ? $studentCourseChange->tue_start_time : '')) }}" > 〜
            <input type='text' id="tue_end_time" name="tue_end_time" class="timepicker input-mn" value="{{ old('tue_end_time', (isset($studentCourseChange->tue_end_time) === true ? $studentCourseChange->tue_end_time : '')) }}" >
          </div>
          <div class="mgn-btm8">
            <b>水</b>
            <input type='text' id="wed_start_time" name="wed_start_time" class="timepicker input-mn" value="{{ old('wed_start_time', (isset($studentCourseChange->wed_start_time) === true ? $studentCourseChange->wed_start_time : '')) }}" > 〜
            <input type='text' id="wed_end_time" name="wed_end_time" class="timepicker input-mn" value="{{ old('wed_end_time', (isset($studentCourseChange->wed_end_time) === true ? $studentCourseChange->wed_end_time : '')) }}" >
          </div>
          <div class="mgn-btm8">
            <b>木</b>
            <input type='text' id="thu_start_time" name="thu_start_time" class="timepicker input-mn" value="{{ old('thu_start_time', (isset($studentCourseChange->thu_start_time) === true ? $studentCourseChange->thu_start_time : '')) }}" > 〜
            <input type='text' id="thu_end_time" name="thu_end_time" class="timepicker input-mn" value="{{ old('thu_end_time', (isset($studentCourseChange->thu_end_time) === true ? $studentCourseChange->thu_end_time : '')) }}" >
          </div>
          <div class="mgn-btm8">
            <b>金</b>
            <input type='text' id="fri_start_time" name="fri_start_time" class="timepicker input-mn" value="{{ old('fri_start_time', (isset($studentCourseChange->fri_start_time) === true ? $studentCourseChange->fri_start_time : '')) }}" > 〜
            <input type='text' id="fri_end_time" name="fri_end_time" class="timepicker input-mn" value="{{ old('fri_end_time', (isset($studentCourseChange->fri_end_time) === true ? $studentCourseChange->fri_end_time : '')) }}" >
          </div>
        </td>
      </tr>
      <tr>
        <th>登録先生</th>
        <td class="form-group">
          {{ $studentCourseChange->teacher_id === null ? '' : (isset($studentCourseChange->teacher->name)===true ? $studentCourseChange->teacher->name : '') }}
        </td>
      </tr>
      <tr>
        <th>承認先生</th>
        <td class="form-group">
          {{ $studentCourseChange->syounin_teacher_id === null ? '未承認' : $studentCourseChange->syounin_teacher->name }}
        </td>
      </tr>
    </table>
  </div>
  <div class="datatable btn_list">
    {{-- キャンセル（一覧に遷移）--}}
    <button type="button" id="cancel" class="btn-info btn-line btn-lg btn-lg-slim">キャンセル</button>
    {{-- 申請（登録・更新） --}}
    @php
      $add_class = 'is_disabled';
      if(in_array($permission, ['校長', 'マネージャー', '本部スタッフ']) === false) {
        if($studentCourseChange->id === null) {
          $add_class = '';
        }
        if($studentCourseChange->id !== null && $studentCourseChange->syounin_teacher_id === null
            && ($studentCourseChange->teacher_id == $teacher->id || in_array($permission, ['管理者']))) {
          $add_class = '';
        }
      }
    @endphp
    <button type="button" id="save" class="btn-lg {{ $add_class }}">申請</button>
    @if($studentCourseChange->id !== null)
      {{-- 申請取消（削除） --}}
      @php
        $add_class = 'is_disabled';
        if(in_array($permission, ['校長', 'マネージャー','本部スタッフ']) === false) {
          if($studentCourseChange->teacher_id == $teacher->id || in_array($permission, ['管理者'])) {
            if($studentCourseChange->id !== null && $studentCourseChange->syounin_teacher_id === null) {
              $add_class = '';
            }
          }
        }
      @endphp
      <button type="button" id="torikeshi" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">申請取消</button>
      {{-- 承認 --}}
      @if(in_array($permission, ['校長', 'マネージャー', '管理者']))
        @if($studentCourseChange->teacher_id != $teacher->id)
        <button type="button" id="syounin" class="btn-lg {{ ($studentCourseChange->syounin_teacher_id !== null || $studentCourseChange->id === null) ? 'is_disabled' : '' }}">承認</button>
        @endif
      @endif
      {{-- 印刷 --}}
      <button type="button" id="print" class="btn-lg btn-lg-slim {{ ($studentCourseChange->syounin_teacher_id === null || $studentCourseChange->id === null || $studentCourseChange->syounin_torikeshi_shinsei_flag === 1) ? 'is_disabled' : '' }}">印刷</button>
      {{-- 承認取消申請 --}}
      @php
        $add_class = 'is_disabled';
        if($studentCourseChange->teacher_id == $teacher->id || in_array($permission, ['管理者'])) {
          if($studentCourseChange->syounin_teacher_id !== null && $studentCourseChange->syounin_torikeshi_shinsei_flag !== 1) {
            $add_class = '';
          }
        }
      @endphp
      <button type="button" id="syounin_torikeshi_shinsei" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">承認取消申請</button>
      {{-- 承認取消 --}}
      @php
        $add_class = 'is_disabled';
        if(in_array($permission, ['校長', 'マネージャー', '管理者'])) {
          if($studentCourseChange->teacher_id != $teacher->id) {
            if($studentCourseChange->syounin_teacher_id !== null && $studentCourseChange->syounin_torikeshi_shinsei_flag == "1") {
              $add_class = '';
            }
          }
      @endphp
      <button type="button" id="cancel_approval" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">承認取消</button>
      @php
        }
      @endphp
    @endif

    <input type="hidden" id="id" name="id" value="{{ $studentCourseChange->id }}">
    <input type="hidden" name="entry_type" value="" >
  </div>
</div>
</form>
@endsection

@section('script')
  <script src="{{ asset('js/timepicker.min.js') }}" defer></script>
  <script>
    $(function(){
      $('#student_id').select2({ language: "ja"});
      $('#course_id').select2({ language: "ja"});

      $('.timepicker').timepicker({
          'timeFormat': 'H:i',
          'minTime': '21:40',
          'maxTime': '21:40',
          'scrollDefault': '16:00',
          'noneOption': [
              { 'label': '16:00', 'value': '16:00' },
              { 'label': '16:30', 'value': '16:30' },
              { 'label': '17:00', 'value': '17:00' },
              { 'label': '17:30', 'value': '17:30' },
              { 'label': '18:00', 'value': '18:00' },
              { 'label': '18:30', 'value': '18:30' },
              { 'label': '19:00', 'value': '19:00' },
              { 'label': '19:30', 'value': '19:30' },
              { 'label': '20:00', 'value': '20:00' },
              { 'label': '20:30', 'value': '20:30' },
              { 'label': '21:00', 'value': '21:00' },
              { 'label': '21:30', 'value': '21:30' },
          ],
      });

      function getCourse() {
        var student_id = $('[name="student_id"]').val();
        var change_yotei_date = $('select[name="change_yotei_date[year]"]').val() + '-' + $('select[name="change_yotei_date[month]"]').val() + '-01';

        if(student_id == "" || $('select[name="change_yotei_date[year]"]').val() == "" || $('select[name="change_yotei_date[month]"]').val() == "") {
          return;
        }

        $.ajax({
          url: "/api/master/student_change_course/get_course",
          type: "GET",
          data: {
            student_id: student_id,
            change_yotei_date: change_yotei_date
          },
          dataType: 'json',
          beforeSend: function() {
            $('.loading').fadeIn();
          },
        })
        .done((data, statusText, xhr) => {
          $('.current_course_name').html(data.course_name);
          $('input[name="current_course_id"]').val(data.course_id);
        })
        .fail((data) => {
          var options = {
            text: "処理中にエラーが発生しました。",
            icon: "error",
            timer: 2000,
            buttons: false,
          };
          swal(options);
          //self.show();
          $('.loading').fadeOut();
        })
        .always((data) => {
          $('.loading').fadeOut();
        });
      }

      $('#student_id, select[name="change_yotei_date[year]"], select[name="change_yotei_date[month]"]').on('change', function() {
        getCourse();
      });

      $(document).on('change', '#course_id', function() {
        var o = $(this);
        console.log(o.val());
        if(o.val() == "17") {
          o.parent().parent().next().hide();
          o.parent().parent().next().find('input[type="text"]').val("");
        } else {
          o.parent().parent().next().show();
        }
      });
      $('#course_id').trigger('change');

      $('#save').on('click', function() {
        $('#form').attr('action', '{{ url("/master/student_change_course/save") }}');
        $('#form').submit();
      });

      $('#torikeshi').on('click', function() {
        swal({
          text: "申請を取消します。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/master/student_change_course_delete") }}');
            $('#form').submit();
          }
        });
      });

      $('#print').on('click', function() {
        window.open('{{ url('/master/student_change_course/print'). '/'. $studentCourseChange->id }}', '_blank');
      });

      $('#syounin_torikeshi_shinsei').on('click', function() {
        swal({
          text: "承認取消申請を行います。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/master/student_change_course_torikeshi_shinsei") }}');
            $('#form').submit();
          }
        });
      });

      $('#cancel').on('click', function() {
        location.href = '{{ url("/master/student_change_course") }}';
      });

      @if (session()->has('alert.success'))
        @if(session('alert.success') == "承認済み。" && isset($studentCourseChange->id) === true)
          window.open('{{ url('/master/student_change_course/print'). '/'. $studentCourseChange->id }}', '_blank');
          location.href = '{{ url("/master/student_change_course") }}';
        @endif
      @endif

    })
  </script>
  @if(in_array($permission, ['校長', 'マネージャー', '管理者']))
  <script>
    $(function(){
      $('#syounin').on('click', function() {
        $('#form').attr('action', '{{ url("/master/student_change_course/syounin") }}');
        $('#form').submit();
      });

      $('#cancel_approval').on('click', function() {
        swal({
          text: "承認を取消します。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/master/student_change_course/cancel_approval") }}');
            $('#form').submit();
          }
        });

      });

    })
  </script>
  @endif
@endsection
