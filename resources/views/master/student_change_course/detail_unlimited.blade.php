@extends('layouts.default')
@section('content')
<form id="form" method="post" action="">
  {{ csrf_field() }}
  <section class="scroll_table-wrap wht_bloc">
  <h2 class="heading-1">コース変更情報</h2>
  <div class="">
    <table class="datatable table-vertical">
      <tr>
        <th>生徒</th>
        <td class="form-group">
          @if($studentCourseChange->id)
          {{ $studentCourseChange->student->name }}
          <input type="hidden" name="student_id" value="{{ $studentCourseChange->student_id }}">
          @else
          <select id="student_id" name="student_id">
            <option class="default" value="">選択してください</option>
            @if(count($students) > 0)
              @foreach($students as $student)
                <option value="{{ $student->id }}" {{ $student->id == old('student_id', $studentCourseChange->student_id) ? 'selected="selected"' : '' }} >{{ $student->name }}</option>
              @endforeach
            @endif
          </select>
          @endif
        </td>
      </tr>
      <tr>
        <th>申込日</th>
        <td class="form-group">
          <input type="text" id="request_date" name="request_date" class="datepicker-here" autocomplete="off" data-language="jp" data-auto-close="true" value="{{ old('request_date', $studentCourseChange->request_date) }}"/>
        </td>
      </tr>
      <tr>
        <th>変更予定日</th>
        <td class="form-group">
          <input type="text" id="change_yotei_date" name="change_yotei_date" class="datepicker-here" autocomplete="off" data-language="jp" data-auto-close="true" value="{{ old('change_yotei_date', $studentCourseChange->change_yotei_date) }}"/>
        </td>
      </tr>
      <tr>
        <th>現在のコース</th>
        <td class="form-group">
          <div class="current_course_name">{{ $studentCourseChange->current_course ? $studentCourseChange->current_course->name : '（自動取得）' }}</div>
          <input type="hidden" name="current_course_id" value="{{ $studentCourseChange->current_course_id }}">
        </td>
      </tr>
      <tr>
        <th>終了年月</th>
        <td class="form-group">
          @php
              $endDate = old('end_date', $studentCourseChange->end_date);
              $endYear = '';
              $endMonth = '';

              $endYear = is_array($endDate)
                  ? $endDate['year']
                  : ($endDate
                      ? date('Y', strtotime($endDate))
                      : date('Y'));
              $endMonth = is_array($endDate)
                  ? $endDate['month']
                  : ($endDate
                      ? date('m', strtotime($endDate))
                      : date('m'));
          @endphp
          <select name="end_date[year]">
            <option class="default" value="">年</option>
            @for($year = intval(date('Y')) - 1; $year <= intval(date('Y')) + 3; $year++)
              <option value="{{ $year }}" {{ ($year == $endYear) ? 'selected="selected"' : '' }}>{{ $year }}</option>
            @endfor
          </select>
          年
          <select name="end_date[month]">
            <option class="default" value="">月</option>
            @for ($month = 1; $month <= 12; $month++)
              <option value="{{ str_pad($month, 2, '0', STR_PAD_LEFT) }}" {{ ($month == $endMonth) ? 'selected="selected"' : '' }}>{{ str_pad($month, 2, '0', STR_PAD_LEFT) }}</option>
            @endfor
          </select>
          月
        </td>
      </tr>
      <tr>
        <th>コース</th>
        <td>（無制限）</td>
      </tr>
      <tr>
        <th>登録先生</th>
        <td class="form-group">
          {{ $studentCourseChange->teacher_id === null ? '' : $studentCourseChange->teacher->name }}
        </td>
      </tr>
      <tr>
        <th>承認先生</th>
        <td class="form-group">
          {{ $studentCourseChange->syounin_teacher_id === null ? '未承認' : $studentCourseChange->syounin_teacher->name }}
        </td>
      </tr>
    </table>
  </div>
  <div class="datatable btn_list">
    {{-- キャンセル（一覧に遷移）--}}
    <button type="button" id="cancel" class="btn-info btn-line btn-lg btn-lg-slim">キャンセル</button>
    {{-- 申請（登録・更新） --}}
    @php
      $add_class = 'is_disabled';
      if(in_array($permission, ['校長', 'マネージャー', '本部スタッフ']) === false) {
        if($studentCourseChange->id === null) {
          $add_class = '';
        }
        if($studentCourseChange->id !== null && $studentCourseChange->syounin_teacher_id === null
            && ($studentCourseChange->teacher_id == $teacher->id || in_array($permission, ['管理者']))) {
          $add_class = '';
        }
      }
    @endphp
    <button type="button" id="save" class="btn-lg {{ $add_class }}">申請</button>

    @if($studentCourseChange->id !== null)
      {{-- 申請取消（削除） --}}
      @php
        $add_class = 'is_disabled';
        if($studentCourseChange->teacher_id == $teacher->id || in_array($permission, ['管理者'])) {
          if($studentCourseChange->id !== null && $studentCourseChange->syounin_teacher_id === null) {
            $add_class = '';
          }
        }
      @endphp
      <button type="button" id="torikeshi" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">申請取消</button>
      {{-- 承認 --}}
      @if(in_array($permission, ['校長','マネージャー','管理者']))
        @if($studentCourseChange->teacher_id != $teacher->id)
        <button type="button" id="syounin" class="btn-lg {{ ($studentCourseChange->syounin_teacher_id !== null || $studentCourseChange->id === null) ? 'is_disabled' : '' }}">承認</button>
        @endif
      @endif
      {{-- 印刷 --}}
      <button type="button" id="print" class="btn-lg btn-lg-slim {{ ($studentCourseChange->syounin_teacher_id === null || $studentCourseChange->id === null || $studentCourseChange->syounin_torikeshi_shinsei_flag === 1) ? 'is_disabled' : '' }}">印刷</button>
      {{-- 承認取消申請 --}}
      @php
        $add_class = 'is_disabled';
        if($studentCourseChange->teacher_id == $teacher->id || in_array($permission, ['管理者'])) {
          if($studentCourseChange->syounin_teacher_id !== null && $studentCourseChange->syounin_torikeshi_shinsei_flag !== 1) {
            $add_class = '';
          }
        }
      @endphp
      <button type="button" id="syounin_torikeshi_shinsei" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">承認取消申請</button>
      {{-- 承認取消 --}}
      @php
        $add_class = 'is_disabled';
        if(in_array($permission, ['校長', 'マネージャー', '管理者'])) {
          if($studentCourseChange->teacher_id != $teacher->id) {
            if($studentCourseChange->syounin_teacher_id !== null && $studentCourseChange->syounin_torikeshi_shinsei_flag == "1") {
              $add_class = '';
            }
          }
      @endphp
      <button type="button" id="cancel_approval" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">承認取消</button>
      @php
        }
      @endphp
    @endif
    <input type="hidden" id="id" name="id" value="{{ $studentCourseChange->id }}">
    <input type="hidden" name="entry_type" value="unlimited" >
  </div>
</div>
</form>
@endsection

@section('script')
  <script src="{{ asset('js/timepicker.min.js') }}" defer></script>
  <script>
    $(function(){
      $('#student_id').select2({ language: "ja"});

      function getCourse() {
        var student_id = $('[name="student_id"]').val();
        var change_yotei_date = $('#change_yotei_date').val();

        if(student_id == "" || change_yotei_date == "") {
          return;
        }

        $.ajax({
          url: "/api/master/student_change_course/get_course",
          type: "GET",
          data: {
            student_id: student_id,
            change_yotei_date: change_yotei_date
          },
          dataType: 'json',
          beforeSend: function() {
            $('.loading').fadeIn();
          },
        })
        .done((data, statusText, xhr) => {
          $('.current_course_name').html(data.course_name);
          $('input[name="current_course_id"]').val(data.course_id);
        })
        .fail((data) => {
          var options = {
            text: "処理中にエラーが発生しました。",
            icon: "error",
            timer: 2000,
            buttons: false,
          };
          swal(options);
          //self.show();
          $('.loading').fadeOut();
        })
        .always((data) => {
          $('.loading').fadeOut();
        });
      }

      $('#student_id').on('change', function() {
        getCourse();
      });

      $('#change_yotei_date').datepicker({
        language: 'jp',
        autoClose: true,
        dateFormat: 'yyyy-mm-dd',
        onSelect: function(dateText) {
          getCourse();
        }
      });
      var change_yotei_date_dp = $('#change_yotei_date').datepicker().data('datepicker');
      @if (old('change_yotei_date', $studentCourseChange->change_yotei_date))
      change_yotei_date_dp.selectDate(new Date('{{ old('change_yotei_date', $studentCourseChange->change_yotei_date) }}'));
      @endif

      $('#save').on('click', function() {
        $('#form').attr('action', '{{ url("/master/student_change_course_unlimited/save") }}');
        $('#form').submit();
      });

      $('#torikeshi').on('click', function() {
        swal({
          text: "申請を取消します。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/master/student_change_course_delete") }}');
            $('#form').submit();
          }
        });
      });

      $('#print').on('click', function() {
        window.open('{{ url('/master/student_change_course/print'). '/'. $studentCourseChange->id }}', '_blank');
      });

      $('#syounin_torikeshi_shinsei').on('click', function() {
        swal({
          text: "承認取消申請を行います。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/master/student_change_course_torikeshi_shinsei") }}');
            $('#form').submit();
          }
        });
      });

      $('#cancel').on('click', function() {
        location.href = '{{ url("/master/student_change_course") }}';
      })

      @if (session()->has('alert.success'))
        @if(session('alert.success') == "承認済み。" && isset($studentCourseChange->id) === true)
          window.open('{{ url('/master/student_change_course/print'). '/'. $studentCourseChange->id }}', '_blank');
          location.href = '{{ url("/master/student_change_course") }}';
        @endif
      @endif

      //$('#change_yotei_date').val('{{ old('change_yotei_date', $studentCourseChange->change_yotei_date) }}');


    })
  </script>
  @if(in_array($permission, ['校長', 'マネージャー', '管理者']))
  <script>
    $(function(){
      $('#syounin').on('click', function() {
        $('#form').attr('action', '{{ url("/master/student_change_course_unlimited/syounin") }}');
        $('#form').submit();
      });

      $('#cancel_approval').on('click', function() {
        swal({
          text: "承認を取消します。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/master/student_change_course_unlimited/cancel_approval") }}');
            $('#form').submit();
          }
        });
      });
    })
  </script>
  @endif
@endsection
