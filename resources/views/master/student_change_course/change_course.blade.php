<style>
body {
    font-size: 11pt;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
    table-layout: fixed;
    text-align: center;
    width: 100%;
}
th, td {
    padding: 0;
}
.main {
    width: 800px;
}
.bordered {
    border: solid 1px black;
}
.bordered-bottom {
    border-bottom: solid 1px black;
}
.bg-color-silver {
    background-color: silver;
}
.text-centered {
    text-align: center;
}
.clr {
    clear: both;
    margin: 0;
    padding: 0;
}
.row-indent {
    float: left;
    width: 3.5%;
}
.row-indent-right {
    float: right;
    width: 3.5%;
}
.row-1 .col-1 {
    background-color: silver;
    border: solid 1px black;
    float: left;
    font-size: 16pt;
    margin: 0;
    padding: 3.5px 0;
    text-align: center;
    width: 20%;
}
.row-1 .col-2 {
    float: right;
    text-align: right;
    width: 20%;
}
.row-2 {
    font-size: 21.5pt;
    font-weight: bold;
    text-align: center;
    text-decoration: underline;
}
.row-3 .col-1,
.row-9 .col-1,
.row-10 .col-1,
{
    float: left;
}
.group-row .col-1 {
    float: left;
    width: 60%;
}
.group-row .col-1 div div.student-name,
.group-row .col-1 div div.under-student-name
{
    float: left;
    font-size: 13pt;
}
.group-row .col-2 {
    float: right;
    width: 35%;
}
.group-row .col-2 div.school-name {
    font-size: 15pt;
}
.row-11 .col-1 {
    float: left;
    width: 17%;
}
.row-11 .col-2 {
    float: left;
    width: 14%;
}
.row-11 .col-3 {
    float: left;
    width: 64%;
}
.row-11 .unlimited {
    float: left;
    width: 78%;
}
.row-12 .col-1,
.row-18 .col-1,
{
    float: left;
    width: 12%;
}
.row-12 .col-2,
.row-18 .col-2,
{
    float: left;
    width: 80%;
}
.row-13 .col-1,
.row-19 .col-1,
{
    float: left;
    width: 80%;
}
.row-14 .col-1 {
    float: left;
    width: 17%;
}
.row-14 .col-2 {
    float: left;
    width: 14%;
}
.row-14 .col-3 {
    float: left;
    width: 64%;
}
.row-14 .unlimited {
    float: left;
    width: 78%;
}
.row-15 .col-1,
.row-16 .col-1,
.row-17 .col-1,
{
    float: left;
    width: 16%;
}
.row-15 .col-2,
.row-15 .col-3,
.row-15 .col-4,
.row-15 .col-5,
.row-15 .col-6,
.row-16 .col-2,
.row-16 .col-3,
.row-16 .col-4,
.row-16 .col-5,
.row-16 .col-6,
.row-17 .col-2,
.row-17 .col-3,
.row-17 .col-4,
.row-17 .col-5,
.row-17 .col-6,
{
    float: left;
    width: 13.5%;
}
.row-last .col-1 {
    float: right;
    width: 22%;
}
</style>
<div class="main">
    <div class="row-1">
        <div class="col-1">{{ $copyFor === 'home' ? '自宅保管用' : '校舎保管用' }}</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-2">{{ \JJSS::printDate($studentCourseChange->request_date) }}</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-2">コ ー ス 変 更 受 付</div>
    <br>
    <div class="row-3">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">〒{{ $studentCourseChange->student->zip_code }}</div>
        <div class="clr"></div>
    </div>
    <div class="group-row">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">
            <div>{{ $studentCourseChange->student->pref . ' ' . $studentCourseChange->student->address1 }}</div>
            <div>{{ $studentCourseChange->student->address2 }}</div>
            <div>
                <div style="float: left; width: 5%">&nbsp;</div>
                <div class="student-name">{{ $studentCourseChange->student->name }}　様</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 5%">&nbsp;</div>
                <div class="under-student-name">保護者様</div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-2">
            <div class="school-name">坪田塾　{{ $studentCourseChange->student_school[0]->name }}</div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>{{ $studentCourseChange->student_school[0]->pref . ' ' . $studentCourseChange->student_school[0]->address }}</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>{{ $studentCourseChange->student_school[0]->building }}</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div></div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div></div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-9">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">平素は格別のご高配に賜り、誠にありがとうございます。</div>
        <div class="clr"></div>
    </div>
    <div class="row-10">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">下記の通りコースの変更を受付いたしましたので、ご連絡させて頂きます。</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-11">
        @php
            $currentCourseType = \JJSS::getCourseType($studentCourseChange->current_course_id);
            $courseType = \JJSS::getCourseType($studentCourseChange->course_id);
        @endphp
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">◆現在のコース：</div>
        @if($currentCourseType === 'course')
        <div class="col-2 bordered-bottom text-centered">{{ $studentCourseChange->current_course->name }}</div>
        <div class="col-3">コース（{{ \JJSS::printTimesByCourse($studentCourseChange->current_course->name) }}）</div>
        @else
        <div class="unlimited">無制限コース</div>
        @endif
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-12">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">&nbsp;</div>
        <div class="col-2">（月謝金額：{{ number_format($studentCourseChange->current_price) }}円）</div>
        <div class="clr"></div>
    </div>
    <br>
    <br>
    <div class="row-13">
        @php
            $duration = $courseType === 'course'
                ? \JJSS::printDate($studentCourseChange->change_yotei_date) . 'より'
                : \JJSS::printDate($studentCourseChange->change_yotei_date) . '〜' . \JJSS::printDate($studentCourseChange->end_date);
        @endphp
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">{{ $duration }}</div>
        <div class="clr"></div>
    </div>
    <div class="row-14">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">◆新しいコース：</div>
        @if($courseType === 'course')
        <div class="col-2 bordered-bottom text-centered">{{ $studentCourseChange->course->name }}</div>
        <div class="col-3">コース（{{ \JJSS::printTimesByCourse($studentCourseChange->course->name) }}）</div>
        @else
        <div class="unlimited">無制限コース</div>
        @endif
        <div class="clr"></div>
    </div>
    <br>
    @if($courseType === 'course')
    <div class="row-15">
        <div class="row-indent">&nbsp;</div>
        <div class="row-indent">&nbsp;</div>
        <div class="col-1 bordered">&nbsp;</div>
        <div class="col-2 bordered text-centered">月</div>
        <div class="col-3 bordered text-centered">火</div>
        <div class="col-4 bordered text-centered">水</div>
        <div class="col-5 bordered text-centered">木</div>
        <div class="col-6 bordered text-centered">金</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    <div class="row-16">
        <div class="row-indent">&nbsp;</div>
        <div class="row-indent">&nbsp;</div>
        <div class="col-1 bordered text-centered">開始時間</div>
        <div class="col-2 bordered text-centered">{{ \JJSS::displayTime($studentCourseChange->mon_start_time) ?: '&nbsp;' }}</div>
        <div class="col-3 bordered text-centered">{{ \JJSS::displayTime($studentCourseChange->tue_start_time) ?: '&nbsp;' }}</div>
        <div class="col-4 bordered text-centered">{{ \JJSS::displayTime($studentCourseChange->wed_start_time) ?: '&nbsp;' }}</div>
        <div class="col-5 bordered text-centered">{{ \JJSS::displayTime($studentCourseChange->thu_start_time) ?: '&nbsp;' }}</div>
        <div class="col-6 bordered text-centered">{{ \JJSS::displayTime($studentCourseChange->fri_start_time) ?: '&nbsp;' }}</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    <div class="row-17">
        <div class="row-indent">&nbsp;</div>
        <div class="row-indent">&nbsp;</div>
        <div class="col-1 bordered text-centered">終了時間</div>
        <div class="col-2 bordered text-centered">{{ \JJSS::displayTime($studentCourseChange->mon_end_time) ?: '&nbsp;' }}</div>
        <div class="col-3 bordered text-centered">{{ \JJSS::displayTime($studentCourseChange->tue_end_time) ?: '&nbsp;' }}</div>
        <div class="col-4 bordered text-centered">{{ \JJSS::displayTime($studentCourseChange->wed_end_time) ?: '&nbsp;' }}</div>
        <div class="col-5 bordered text-centered">{{ \JJSS::displayTime($studentCourseChange->thu_end_time) ?: '&nbsp;' }}</div>
        <div class="col-6 bordered text-centered">{{ \JJSS::displayTime($studentCourseChange->fri_end_time) ?: '&nbsp;' }}</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="clr"></div>
    </div>
    <br>
    @endif
    <div class="row-18">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">&nbsp;</div>
        <div class="col-2">（月謝金額：{{ number_format($studentCourseChange->price) }}円）</div>
        <div class="clr"></div>
    </div>
    <br>
    <br>
    <div class="row-19">
        <div class="row-indent">&nbsp;</div>
        @if($courseType === 'course')
        <div class="col-1">従来のコースとの差額は、次回請求時にご精算させて頂きます。</div>
        @else
        <div class="col-1">従来のコースとの差額は、次回請求時にご精算させて頂きます。<br>無制限コース終了日までの月謝及び設備費を別途ご請求させて頂きます。</div>
        @endif
        <div class="clr"></div>
    </div>
    @if ($copyFor === 'home')
    <br>
    <div class="row-last">
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-1">
            <table>
                <tr>
                    <th class="bordered">担当</th>
                    <th class="bordered">校長</th>
                </tr>
                <tr>
                    <td class="bordered" style="height: 60px;">&nbsp;</td>
                    <td class="bordered">&nbsp;</td>
                </tr>
            </table>
        </div>
        <div class="clr"></div>
    </div>
    @endif
</div>
