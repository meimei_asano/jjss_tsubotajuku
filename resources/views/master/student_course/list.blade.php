@extends('layouts.default')

@section('content')
<section class="scroll_table-wrap wht_bloc">

  <h2 class="heading-1">生徒情報</h2>

  @include('master.student.detail_menu')

  <table class="datatable table-vertical">
    <tr>
      <th>生徒コード</th>
      <td>
        {{ $student->student_code or null }}
      </td>
    </tr>
    <tr>
      <th>生徒名</th>
      <td>
        {{ $student->name or null }}
        <input type="hidden" name="student_id" value="{{ old('student_id', $student->id) }}">
      </td>
    </tr>
    <tr>
      <th>コース</th>
      <td>
        <div class="flex">
          <div style="width: 50%;">
            期間
          </div>
          <div style="width: 30%;">
            コース
          </div>
          <div>　</div>
        </div>
        {{-- コース追加 --}}
        <form name="form" action="{{ asset('/master/student_course') }}" method="post">
          {{ csrf_field() }}
          <div class="flex form-group mgn-btm24">
            <div style="width: 50%;">
              <input type='text' name="start_date" class="datepicker-here" data-language="jp" value="{{-- old('start_date') --}}" style="width:25%;" >　〜　
              <input type='text' name="end_date" class="datepicker-here" data-language="jp" value="{{-- old('end_date') --}}" style="width:25%;" >
            </div>
            <div style="width: 30%;">
              <select name="course_id">
                <option value="">---</option>
                {!! \JJSS::options($courses, 'id', 'name') !!}
              </select>
            </div>
            <div>
              @if(in_array($permission, ['管理者']) === true)
                <button type="submit" class="save_course" dt-course_id="">登録</button>
              @endif
              <input type="hidden" name="student_id" value="{{ $student->id }}" >
              <input type="hidden" name="attend_base_id" value="" >
            </div>
          </div>
        </form>
        {{-- コース変更・参照 --}}
      @if(count($student->attend_bases))
          @foreach($student->attend_bases as $attend_base)
            {{-- 契約終了日が空欄 or 契約終了日登録があり、その日付は今月以降 --}}
            @if(strlen($attend_base->end_date) == 0 || (strlen($attend_base->end_date) > 0 && Carbon\Carbon::parse($attend_base->end_date)->firstOfMonth() >= Carbon\Carbon::today()->firstOfMonth()))
              <form name="form{{ $attend_base->id }}" action="{{ asset('/master/student_course') }}" method="post">
                {{ csrf_field() }}
                <div class="flex form-group mgn-btm8">
                  <div style="width: 50%;">
                    <input type='text' name="start_date" class="datepicker-here" data-language="jp" value="{{ $attend_base->start_date }}" style="width:25%;">　〜　
                    <input type='text' name="end_date" class="datepicker-here" data-language="jp" value="{{ $attend_base->end_date }}" style="width:25%;">
                  </div>
                  <div style="width: 30%;">
                    <select name="course_id">
                      <option value="">---</option>
                      {!! \JJSS::options($courses, 'id', 'name', $attend_base->course_id) !!}
                    </select>
                  </div>
                  <div>
                    @if(in_array($permission, ['管理者']) === true)
                      <button type="submit" >変更</button>
                    @endif
                    <input type="hidden" name="student_id" value="{{ $student->id }}" >
                    <input type="hidden" name="attend_base_id" value="{{ $attend_base->id }}" >
                  </div>
                </div>
              </form>
            @else
              <div class="flex form-group mgn-btm8">
                <div style="width: 50%;">
                  <p style="font-size:120%;">{{ $attend_base->start_date }}　　　　　　　〜　{{ strlen($attend_base->end_date) > 0 ? $attend_base->end_date : '' }}</p>
                </div>
                <div style="width: 30%;">
                  <p style="font-size:120%;">{{ $attend_base->course->name }}</p>
                </div>
                <div>　</div>
              </div>
            @endif
          </div>
        @endforeach
        </div>
      @endif
      </td>
    </tr>
  </table>
</section>
@endsection

@section('script')
  <script>
    $(function(){

      $('#save').on('click', function() {
        $('#form').attr('action', '{{ url('/master/student') }}');
        $('#form').submit();
      });

      $('#cancel').on('click', function() {
        location.href = '{{ url('/master/student') }}';
      });

    })
  </script>
@endsection
