
@extends('layouts.default')

<link rel="stylesheet" href="{{ asset('css/master.student_attend.css') }}">

@section('content')
<section class="scroll_table-wrap wht_bloc">

    <h2 class="heading-1">生徒情報</h2>

    @include('master.student.detail_menu')

  @if($terms)

    @foreach($terms as $term)

      @if(strlen($term_id) == 0)
        @php
          $term_id = $term->id;
        @endphp
      @endif

      @if($term->start_date)
        <section class="txt-rgt">
          <a href="{{ url('/master/student_attend_special/' . $student->id . '/' . $term->id) }}"><button type="button"
        @if($term_id == $term->id)
           class="btn-sm mgn-btm8"
        @else
           class="btn-sm mgn-btm8 btn-thin"
        @endif
           >
            {{ $term->term_name }}　　（{{ Carbon\Carbon::parse($term->start_date)->format('Y年m月d日') }}　〜　{{ isset($term->end_date) === true ? Carbon\Carbon::parse($term->end_date)->format('Y年m月d日') : '' }}）
          </button></a>
        </section>
      @endif

    @endforeach

  <form id="form" method="post" action="">
    {{ csrf_field() }}
    @foreach($terms as $term)
      @if($term_id == $term->id)
    <table class="datatable table-vertical">
      <tr>
          <th>生徒コード</th>
          <td>{{ $student->student_code }}</td>
      </tr>
      <tr>
        <th>生徒名</th>
        <td>{{ $student->name }}</td>
      </tr>
      <tr>
        <th>特別講習名</th>
        <td>
          {{ $term->display_name }}
        </td>
      </tr>
      <tr>
        <th>期間</th>
        <td class="form-group">
          {{ (isset($term->start_date) === true ? $term->start_date : '') }} -
          {{ (isset($term->end_date) === true ? $term->end_date : '') }}
        </td>
      </tr>
      <tr>
        <th>通塾予定</th>
        <td class="form-group">
            <div class="flex sp_jikan">
                <div>&nbsp;</div>
                <div>※手入力が可能です</div>
                <ul class="flex">
                    <li>Ⅰ</li>
                    <li>Ⅱ</li>
                    <li>Ⅲ</li>
                    <li>Ⅳ</li>
                </ul>
            </div>
        @php
          $target_date = Carbon\Carbon::parse($term->start_date);
          $end_date = Carbon\Carbon::parse($term->end_date);
          $i = 0;
        @endphp
        @while($end_date->gte($target_date))
          @php
            $attend_date = isset($attend_dates[$target_date->format('Y-m-d')]) === true ? $attend_dates[$target_date->format('Y-m-d')] : array();
          @endphp
          <div class="flex sp_jikan">
            <div style="width:100px;"><b>{{ $target_date->format('m月d日') }}（{{ isset($weeks[$target_date->dayOfWeek]) === true ? $weeks[$target_date->dayOfWeek] : '　' }}）</b></div>
            <div>
              <input type='text' name="start_time[]" id="start_time_{{ $target_date->format('Ymd') }}" class="timepicker input-mn" value="{{ old('start_time.' . $i, (isset($attend_date['start_time']) === true ? $attend_date['start_time'] : '')) }}" > 〜
              <input type='text' name="end_time[]" id="end_time_{{ $target_date->format('Ymd') }}" class="timepicker input-mn" value="{{ old('end_time.' . $i, (isset($attend_date['end_time']) === true ? $attend_date['end_time'] : '')) }}" >
              <input type="hidden" name="attend_date[]" value="{{ old('attend_date.' . $i, $target_date->format('Y-m-d')) }}" >
              <input type="hidden" name="attend_special_id[]" value="{{ old('attend_special_id.' . $i, (isset($attend_date['id']) === true ? $attend_date['id'] : '')) }}" >
            </div>
            <ul class="flex">
              <li>
                <input type="checkbox" id="t1{{ $target_date->format('Ymd') }}" class="set_time" value="1"><label for="t1{{ $target_date->format('Ymd') }}"></label>
              </li>
              <li>
                <input type="checkbox" id="t2{{ $target_date->format('Ymd') }}" class="set_time" value="2"><label for="t2{{ $target_date->format('Ymd') }}"></label>
              </li>
              <li>
                <input type="checkbox" id="t3{{ $target_date->format('Ymd') }}" class="set_time" value="3"><label for="t3{{ $target_date->format('Ymd') }}"></label>
              </li>
              <li>
                <input type="checkbox" id="t4{{ $target_date->format('Ymd') }}" class="set_time" value="4"><label for="t4{{ $target_date->format('Ymd') }}"></label>
              </li>
            </ul>
          </div>
          @php
            $target_date->addDay();
            $i++;
          @endphp
        @endwhile
        </td>
      </tr>
    </table>
    <div class="datatable btn_list txt-ctr">
      @if(in_array(\JJSS::getPermission(), array('管理者','校長','講師')))
        <button type="button" id="save" class="btn-lg">保存</button>
      @endif
      <input type="hidden" id="student_id" name="student_id" value="{{ $student->id }}">
      <input type="hidden" id="term_id" name="term_id" value="{{ old('term_id', (isset($term->id) === true ? $term->id : '')) }}">
    </div>
      @endif
    @endforeach
  </form>
  @else
    <p>特別講習は登録がありません。</p>
  @endif
</section>
@endsection

@section('script')
  <script src="{{ asset('js/timepicker.min.js') }}" defer></script>
  <script>
    $(function(){

      $('.timepicker').timepicker({
          'timeFormat': 'H:i',
          'minTime': '22:00',
          'maxTime': '22:00',
          'scrollDefault': '13:00',
          'noneOption': [
              { 'label': '13:00 Ⅰ〜', 'value': '13:00' },
              { 'label': '14:00', 'value': '14:00' },
              { 'label': '15:00 〜Ⅰ', 'value': '15:00' },
              { 'label': '15:10 Ⅱ〜', 'value': '15:10' },
              { 'label': '16:10', 'value': '16:10' },
              { 'label': '17:10 〜Ⅱ', 'value': '17:10' },
              { 'label': '17:20 Ⅲ〜', 'value': '17:20' },
              { 'label': '18:20', 'value': '18:20' },
              { 'label': '19:20 〜Ⅲ', 'value': '19:20' },
              { 'label': '19:30 Ⅳ〜', 'value': '19:30' },
              { 'label': '20:30', 'value': '20:30' },
              { 'label': '21:30 〜Ⅳ', 'value': '21:30' },
          ]
      });

      $('#save').on('click', function() {
          $('#form').attr('action', '{{ url('/master/student_attend_special') }}');
          $('#form').submit();
      });

      var special_s_times = ['13:00', '15:10', '17:20', '19:30'];
      var special_e_times = ['15:00', '17:10', '19:20', '21:30'];

      $('.set_time').on('click', function() {
          var o = $(this);

          var s_time = '';
          var e_time = '';
          o.parent().parent().find('.set_time').each(function() {
              if($(this).prop('checked')) {
                  var n = $(this).val() - 0;
                  if(s_time == '') {
                      s_time = special_s_times[(n - 1)];
                  }
                  e_time = special_e_times[(n - 1)];
              }
          });

          o.parent().parent().parent().find('input[name^="start_time"]').val(s_time);
          o.parent().parent().parent().find('input[name^="end_time"]').val(e_time);
      });
    })
  </script>
@endsection
