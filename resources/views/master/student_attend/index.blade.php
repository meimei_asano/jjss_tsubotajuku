@extends('layouts.default')

@section('content')


<section class="scroll_table-wrap wht_bloc">

    <h2 class="heading-1">生徒情報</h2>

    @include('master.student.detail_menu')

@foreach($student->attend_bases as $student_attend_base)

    @if(strlen($attend_base_id) == 0)
      @php
        $attend_base_id = $student_attend_base->id;
      @endphp
    @endif

    @if($student_attend_base->start_date)
      <section class="txt-rgt">
        <a href="{{ asset('/master/student_attend/' . $student->id . '/' . $student_attend_base->id) }}"><button type="button" style="width:30%;"
      @if($attend_base_id == $student_attend_base->id)
         class="btn-sm txt-lft mgn-btm8"
      @else
         class="btn-sm txt-lft mgn-btm8 btn-thin"
      @endif
         >
          {{ Carbon\Carbon::parse($student_attend_base->start_date)->format('Y年m月d日') }}　〜　{{ isset($student_attend_base->end_date) === true ? Carbon\Carbon::parse($student_attend_base->end_date)->format('Y年m月d日') : '' }}
        </button></a>
      </section>
    @endif

  @endforeach

<form id="form" method="post" action="">
  {{ csrf_field() }}
  @foreach($student->attend_bases as $student_attend_base)
    @if($attend_base_id == $student_attend_base->id)
  <div class="">
    <table class="datatable table-vertical">
      <tr>
          <th>生徒コード</th>
          <td>{{ $student->student_code }}</td>
      </tr>
      <tr>
          <th>生徒名</th>
          <td>{{ $student->name }}</td>
      </tr>
      <tr>
        <th>コース</th>
        <td>
          {{ $student_attend_base->course->name or null }}
          <input type="hidden" id="student_id" name="student_id" class="" value="{{ old('student_id', $student->id) }}">
        </td>
      </tr>
      <tr>
        <th>契約期間</th>
        <td class="form-group">
            {{ old('start_date', (isset($student_attend_base->start_date) === true ? $student_attend_base->start_date : '')) }}　-　{{ old('end_date', (isset($student_attend_base->end_date) === true ? $student_attend_base->end_date : '')) }}
          {{-- <input type='text' id="start_date" name="start_date" class="datepicker-here" data-language="jp" value="{{ old('start_date', (isset($student_attend_base->start_date) === true ? $student_attend_base->start_date : '')) }}" > - --}}
          {{-- <input type='text' id="end_date" name="end_date" class="datepicker-here" data-language="jp" value="{{ old('end_date', (isset($student_attend_base->end_date) === true ? $student_attend_base->end_date : '')) }}" > --}}
        </td>
      </tr>
      <tr>
        <th>通塾予定</th>
        <td class="form-group">
          <div class="mgn-btm8" style="margin-left: 2.4em;">
            ※手入力が可能です
          </div>
          <div class="mgn-btm8">
            <b>月</b>　
            <input type='text' id="mon_start_time" name="mon_start_time" class="timepicker input-mn" value="{{ old('mon_start_time', (isset($student_attend_base->mon_start_time) === true ? $student_attend_base->mon_start_time : '')) }}" > 〜
            <input type='text' id="mon_end_time" name="mon_end_time" class="timepicker input-mn" value="{{ old('mon_end_time', (isset($student_attend_base->mon_end_time) === true ? $student_attend_base->mon_end_time : '')) }}" >
          </div>
          <div class="mgn-btm8">
            <b>火</b>　
            <input type='text' id="tue_start_time" name="tue_start_time" class="timepicker input-mn" value="{{ old('tue_start_time', (isset($student_attend_base->tue_start_time) === true ? $student_attend_base->tue_start_time : '')) }}" > 〜
            <input type='text' id="tue_end_time" name="tue_end_time" class="timepicker input-mn" value="{{ old('tue_end_time', (isset($student_attend_base->tue_end_time) === true ? $student_attend_base->tue_end_time : '')) }}" >
          </div>
          <div class="mgn-btm8">
            <b>水</b>　
            <input type='text' id="wed_start_time" name="wed_start_time" class="timepicker input-mn" value="{{ old('wed_start_time', (isset($student_attend_base->wed_start_time) === true ? $student_attend_base->wed_start_time : '')) }}" > 〜
            <input type='text' id="wed_end_time" name="wed_end_time" class="timepicker input-mn" value="{{ old('wed_end_time', (isset($student_attend_base->wed_end_time) === true ? $student_attend_base->wed_end_time : '')) }}" >
          </div>
          <div class="mgn-btm8">
            <b>木</b>　
            <input type='text' id="thu_start_time" name="thu_start_time" class="timepicker input-mn" value="{{ old('thu_start_time', (isset($student_attend_base->thu_start_time) === true ? $student_attend_base->thu_start_time : '')) }}" > 〜
            <input type='text' id="thu_end_time" name="thu_end_time" class="timepicker input-mn" value="{{ old('thu_end_time', (isset($student_attend_base->thu_end_time) === true ? $student_attend_base->thu_end_time : '')) }}" >
          </div>
          <div class="mgn-btm8">
            <b>金</b>　
            <input type='text' id="fri_start_time" name="fri_start_time" class="timepicker input-mn" value="{{ old('fri_start_time', (isset($student_attend_base->fri_start_time) === true ? $student_attend_base->fri_start_time : '')) }}" > 〜
            <input type='text' id="fri_end_time" name="fri_end_time" class="timepicker input-mn" value="{{ old('fri_end_time', (isset($student_attend_base->fri_end_time) === true ? $student_attend_base->fri_end_time : '')) }}" >
          </div>
        </td>
      </tr>
    </table>
  </div>
  <div class="datatable btn_list txt-ctr">
    {{-- <button type="button" id="cancel" class="btn-info btn-line btn-lg">キャンセル</button> --}}
    @if(in_array(\JJSS::getPermission(), array('管理者','校長')))
        <button type="button" id="save" class="btn-lg">保存</button>
    @endif
    <input type="hidden" id="student_id" name="student_id" value="{{ $student->id }}">
    <input type="hidden" id="attend_base_id" name="attend_base_id" value="{{ old('attend_base_id', (isset($student_attend_base->id) === true ? $student_attend_base->id : '')) }}">
  </div>
    @endif
  @endforeach
</form>
</section>
@endsection

@section('script')
  <script src="{{ asset('js/timepicker.min.js') }}" defer></script>
  <script>
    $(function(){

      $('.timepicker').timepicker({
          'timeFormat': 'H:i',
          'minTime': '21:40',
          'maxTime': '21:40',
          'scrollDefault': '16:00',
          'noneOption': [
              { 'label': '16:00', 'value': '16:00' },
              { 'label': '16:30', 'value': '16:30' },
              { 'label': '17:00', 'value': '17:00' },
              { 'label': '17:30', 'value': '17:30' },
              { 'label': '18:00', 'value': '18:00' },
              { 'label': '18:30', 'value': '18:30' },
              { 'label': '19:00', 'value': '19:00' },
              { 'label': '19:30', 'value': '19:30' },
              { 'label': '20:00', 'value': '20:00' },
              { 'label': '20:30', 'value': '20:30' },
              { 'label': '21:00', 'value': '21:00' },
              { 'label': '21:30', 'value': '21:30' },
          ],
      });

      $('#save').on('click', function() {
          $('#form').attr('action', '{{ url('/master/student_attend') }}');
          $('#form').submit();
      });

      $('#cancel').on('click', function() {
        location.href = '{{ url('/master/student') }}';
      })
    })
  </script>
@endsection
