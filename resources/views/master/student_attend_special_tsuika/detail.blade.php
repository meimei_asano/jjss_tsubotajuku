@extends('layouts.default')
@section('content')
<style>
.set_container {
  position: relative;
  display: inline-block;
}
.remove_text_request_field {
  position: absolute;
  color: white;
  font-weight: bolder;
  background-color: red;
  border: solid 1px white;
  border-radius: 50%;
  padding: 0 8px;
  cursor: pointer;
  right: -20px;
  top: 15px;
}
.remove_text_request_field:hover {
  color: red;
  font-weight: bolder;
  background-color: white;
  border: solid 1px red;
}
</style>
<form id="form" method="post" action="">
  {{ csrf_field() }}
  <section class="scroll_table-wrap wht_bloc">
  <h2 class="heading-1">特別講習申込</h2>
  <div class="">
    <table class="datatable table-vertical">
      <tr>
        <th>生徒</th>
        <td class="form-group">
          <select id="student_id" name="student_id">
            <option class="default" value="">選択してください</option>
            @if(count($students) > 0)
              @foreach($students as $student)
                <option value="{{ $student->id }}" {{ $student->id == old('student_id', $studentAttendSpecialTsuika->student_id) ? 'selected="selected"' : '' }} >{{ $student->name }}</option>
              @endforeach
            @endif
          </select>
        </td>
      </tr>
      <tr>
        <th>申込日</th>
        <td class="form-group">
          <input type="text" id="request_date" name="request_date" class="datepicker-here" autocomplete="off" data-language="jp" data-auto-close="true" value="{{ old('request_date', $studentAttendSpecialTsuika->request_date) }}"/>
        </td>
      </tr>
      <tr>
        <th>特別講習期間</th>
        <td class="form-group">
          @php
            $requestId = old('term_id', $studentAttendSpecialTsuika->term_id);
          @endphp
          <select id="term_id" name="term_id">
            <option class="default" value="">特別講習期間</option>
            @foreach($terms as $term)
              <option value="{{ $term->id }}" {{ ($term->id == $requestId) ? 'selected="selected"' : '' }} >{{ $term->term_name }}</option>
            @endforeach
          </select>
        </td>
      </tr>
      <tr>
        <th>総合計申し込み時間数</th>
        <td class="form-group">
          @php
            $totalHours = old('total_hours', $studentAttendSpecialTsuika->total_hours);
          @endphp
          <input type="text" data-format="number" id="total_hours" name="total_hours" autocomplete="off" placeholder="総合計申し込み時間数" value="{{ $totalHours }}"/> 時間
        </td>
      </tr>
      <tr>
        <th>期間中通常授業時間数</th>
        <td class="form-group">
          @php
            $baseHours = old('base_hours', $studentAttendSpecialTsuika->base_hours) ?: 0;
          @endphp
          <input type="text" data-format="number" id="base_hours" name="base_hours" autocomplete="off" readonly placeholder="期間中通常授業時間数" value="{{ $baseHours }}"/> 時間
        </td>
      </tr>
      <tr>
        <th>追加時間数</th>
        <td class="form-group">
          @php
            $addedHours = ($totalHours && $baseHours) ? intval($totalHours) - intval($baseHours) : 0;
          @endphp
          <span id="added_hours">{{ $addedHours }}</span> 時間
        </td>
      </tr>
      <tr>
        <th>単価</th>
        <td class="form-group">
          @php
            $unitPrice = \JJSS::calculateUnitPrice($studentAttendSpecialTsuika->student_id);
          @endphp
          <span id="unit_price">{{ number_format($unitPrice) }}</span> 円
        </td>
      </tr>
      <tr>
        <th>合計金額</th>
        <td class="form-group">
          @php
            $subtotal = $addedHours * $unitPrice;
          @endphp
          <span id="subtotal">{{ number_format($subtotal) }}</span> 円
        </td>
      </tr>
      <tr>
        <th>登録先生</th>
        <td class="form-group">
          {{ $studentAttendSpecialTsuika->teacher_id === null ? '' : $studentAttendSpecialTsuika->teacher->name }}
        </td>
      </tr>
      <tr>
        <th>承認先生</th>
        <td class="form-group">
          {{ $studentAttendSpecialTsuika->syounin_teacher_id === null ? '未承認' : $studentAttendSpecialTsuika->syounin_teacher->name }}
        </td>
      </tr>
    </table>
  </div>
  <div class="datatable btn_list">
    {{-- キャンセル（一覧に遷移）--}}
    <button type="button" id="cancel" class="btn-info btn-line btn-lg btn-lg-slim">キャンセル</button>
    {{-- 申請（登録・更新） --}}
    @php
      $add_class = 'is_disabled';
      if(in_array($permission, ['校長', 'マネージャー', '本部スタッフ']) === false) {
        if($studentAttendSpecialTsuika->id === null) {
          $add_class = '';
        }
        if($studentAttendSpecialTsuika->id !== null && $studentAttendSpecialTsuika->syounin_teacher_id === null
            && ($studentAttendSpecialTsuika->teacher_id == $teacher->id || in_array($permission, ['管理者']))) {
          $add_class = '';
        }
      }
    @endphp
    <button type="button" id="save" class="btn-lg {{ $add_class }}">申請</button>

    @if($studentAttendSpecialTsuika->id !== null)
      {{-- 申請取消（削除） --}}
      @php
        $add_class = 'is_disabled';
        if($studentAttendSpecialTsuika->teacher_id == $teacher->id || in_array($permission, ['管理者'])) {
          if($studentAttendSpecialTsuika->id !== null && $studentAttendSpecialTsuika->syounin_teacher_id === null) {
            $add_class = '';
          }
        }
      @endphp
      <button type="button" id="torikeshi" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">申請取消</button>
      {{-- 承認 --}}
      @if(in_array($permission, ['校長', 'マネージャー', '管理者']))
        @if($studentAttendSpecialTsuika->teacher_id != $teacher->id)
        <button type="button" id="syounin" class="btn-lg {{ ($studentAttendSpecialTsuika->syounin_teacher_id !== null || $studentAttendSpecialTsuika->id === null) ? 'is_disabled' : '' }}">承認</button>
        @endif
      @endif
      {{-- 印刷 --}}
      <button type="button" id="print" class="btn-lg btn-lg-slim {{ ($studentAttendSpecialTsuika->syounin_teacher_id === null || $studentAttendSpecialTsuika->id === null || $studentAttendSpecialTsuika->syounin_torikeshi_shinsei_flag === 1) ? 'is_disabled' : '' }}">印刷</button>
      {{-- 承認取消申請 --}}
      @php
        $add_class = 'is_disabled';
        if($studentAttendSpecialTsuika->teacher_id == $teacher->id || in_array($permission, ['管理者'])) {
          if($studentAttendSpecialTsuika->syounin_teacher_id !== null && $studentAttendSpecialTsuika->syounin_torikeshi_shinsei_flag !== 1) {
            $add_class = '';
          }
        }
      @endphp
      <button type="button" id="syounin_torikeshi_shinsei" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">承認取消申請</button>
      {{-- 承認取消 --}}
      @php
        $add_class = 'is_disabled';
        if(in_array($permission, ['校長', 'マネージャー', '管理者'])) {
          if($studentAttendSpecialTsuika->teacher_id != $teacher->id) {
            if($studentAttendSpecialTsuika->syounin_teacher_id !== null && $studentAttendSpecialTsuika->syounin_torikeshi_shinsei_flag == "1") {
              $add_class = '';
            }
          }
        }
      @endphp
      <button type="button" id="syounin_torikeshi" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">承認取消</button>
    @endif

    <input type="hidden" id="id" name="id" value="{{ $studentAttendSpecialTsuika->id }}">
  </div>
</div>
</form>
@endsection
@section('script')
  <script>
    $(function(){
      $('#student_id').select2({ language: "ja"});
      $('#save').on('click', function() {
        $('#form').attr('action', '{{ url("/master/student_attend_special_tsuika/save") }}');
        $('#form').submit();
      });

      $('#torikeshi').on('click', function() {
        swal({
          text: "申請を取消します。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/master/student_attend_special_tsuika_delete") }}');
            $('#form').submit();
          }
        });
      });

      $('#syounin_torikeshi_shinsei').on('click', function() {
        swal({
          text: "承認取消申請を行います。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/master/student_attend_special_tsuika_torikeshi_shinsei") }}');
            $('#form').submit();
          }
        });
      });

      $('#print').on('click', function() {
        window.open('{{ url('/master/student_attend_special_tsuika/print'). '/'. $studentAttendSpecialTsuika->id }}', '_blank');
      });

      $('#cancel').on('click', function() {
        location.href = '{{ url("/master/student_attend_special_tsuika") }}';
      });

      $(document).on('change', '#total_hours, #base_hours', function(e) {
        let totalHours = ($('#total_hours').attr('data-value') === void 0) ? $('#total_hours').val() : $('#total_hours').attr('data-value');
        let baseHours = ($('#base_hours').attr('data-value') === void 0) ? $('#base_hours').val() : $('#base_hours').attr('data-value');

        if (totalHours !== '' && baseHours !== '') {
          let addedHours = parseInt(totalHours) - parseInt(baseHours);
          let unitPrice = parseInt({{ $unitPrice }});
          let subtotal = addedHours * unitPrice;
          subtotal = subtotal.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
          $('#added_hours').html(addedHours);
          $('#subtotal').html(subtotal);
        }
      });

      $(document).on('change', '#term_id, #student_id', function (e){
        let term_id = $('#term_id').val();
        let student_id = $('#student_id').val();
        if (term_id == '' || student_id == '') {
          $('#base_hours').val('0');

          return false;
        }
        $.ajax({
          url: "/api/master/student_attend_special_tsuika/get_base_hours",
          type: "GET",
          data: {
            term_id: term_id,
            student_id: student_id,
          },
          dataType: 'json',
          beforeSend: function() {
            $('.loading').fadeIn();
          },
        })
        .done((data, statusText, xhr) => {
          $('#base_hours').val(data.base_hours);
        })
        .fail((data) => {
          var options = {
            text: "処理中にエラーが発生しました。",
            icon: "error",
            timer: 2000,
            buttons: false,
          };
          swal(options);
          self.show();
          $('.loading').fadeOut();
        })
        .always((data) => {
          $('.loading').fadeOut();
        });
      });

      @if (session()->has('alert.success'))
        @if(session('alert.success') == "承認済み。" && isset($studentAttendSpecialTsuika->id) === true)
          window.open('{{ url('/master/student_attend_special_tsuika/print'). '/'. $studentAttendSpecialTsuika->id }}', '_blank');
          location.href = '{{ url("/master/student_attend_special_tsuika") }}';
        @endif
      @endif

    })
  </script>
  @if(in_array($permission, ['校長', '管理者']))
  <script>
    $(function(){
      $('#syounin').on('click', function() {
        $('#form').attr('action', '{{ url("/master/student_attend_special_tsuika/syounin") }}');
        $('#form').submit();
      });

      $('#syounin_torikeshi').on('click', function() {
        swal({
          text: "承認取消を行います。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/master/student_attend_special_tsuika_torikeshi") }}');
            $('#form').submit();
          }
        });
      });

    })
  </script>
  @endif
@endsection
