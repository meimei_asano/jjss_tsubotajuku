@extends('layouts.default')
@section('content')
<style>
  .set_orderby {cursor:pointer;}
</style>

<section class="scroll_table-wrap wht_bloc">
  <h2 class="heading-1">特別講習申込</h2>
  <div class="flex flex-j-between flex-a-ctr mgn-btm16">
    <form id="search_form" action="{{ url('master/student_attend_special_tsuika') }}" method="get">
      <div class="form-group flex">
        <select id="school_id" name="school_id" >
          <option value="">校舎</option>
          {!! \JJSS::options($schools, 'id', 'name', ($search['school_id']) ?? '') !!}
        </select>
        &nbsp;
        <input type="text" id="student_code" name="student_code" placeholder="生徒コード" class="input-sm" value="{{ $search['student_code'] ?? '' }}"/>
        &nbsp;
        <input type="text" id="student_name" name="student_name" placeholder="生徒名" class="input-sm" value="{{ $search['student_name'] ?? '' }}"/>
        &nbsp;
        <select id="teacher_id" name="teacher_id" >
          <option value="">登録先生</option>
          {!! \JJSS::options($teachers, 'id', 'name', ($search['teacher_id']) ?? '') !!}
        </select>
        &nbsp;
        <ul>
          <li>
            <input type="checkbox" id="syounin_teacher_id" name="syounin_teacher_id" value="1" {{ (isset($search['syounin_teacher_id']) && $search['syounin_teacher_id']) ? 'checked' : '' }}>
            <label for="syounin_teacher_id">承認済</label>
          </li>
        </ul>
        &nbsp;
        <button id="btn-search">検索<i class='bx bx-search'></i></button>

        <input type="hidden" name="orderby_column_name" value="{{ ($orderby['column_name']) ?? '' }}">
        <input type="hidden" name="orderby_type" value="{{ ($orderby['type']) ?? '' }}">
      </div>
    </form>
    <div class="btn_list">
      <button id="create" onclick="location.href='{{ url('/master/student_attend_special_tsuika'). '/new' }}';"><i class='bx'></i>新規</button>
    </div>
  </div>
  <div class="scroll_table mgn-btm4">
    <table class="table-horizon table-striped datatable">
      <thead>
        <tr>
          <th class="th_check">
            <input type="checkbox" class="check" id="check_all" value="all">
            <label for="check_all"></label>
          </th>
          <th class="set_orderby" dt-column="students.school_id">校舎</th>
          <th class="set_orderby" dt-column="students.student_code">生徒コード</th>
          <th class="set_orderby" dt-column="students.kana">生徒名</th>
          <th class="set_orderby" dt-column="request_date">申込日</th>
          <th class="set_orderby" dt-column="terms.term_name">特別講習期間名</th>
          <th class="set_orderby" dt-column="teachers.kana">登録先生</th>
          <th>承認済</th>
          <th>承認取消</th>
          <th style="width:300px;"></th>
        </tr>
      </thead>
      <tbody>
      @if(count($studentAttendSpecialTsuikas) === 0)
        <tr>
          <td colspan="9">
            レコードが見つかりません。
          </td>
        </tr>
      @else
        @foreach ($studentAttendSpecialTsuikas as $studentAttendSpecialTsuika)
          <tr>
            <td class="td_check">
              @if(($studentAttendSpecialTsuika->teacher_id == $teacher->id || in_array($permission, ['管理者'])) && $studentAttendSpecialTsuika->syounin_teacher_id === null)
                <input type="checkbox" class="check" data-student_attend_special_tsuika_id="{{ $studentAttendSpecialTsuika->id }}">
                <label></label>
              @endif
            </td>
            <td>{{ $studentAttendSpecialTsuika->student_school[0]->name }}</td>
            <td>{{ $studentAttendSpecialTsuika->student->student_code }}</td>
            <td>{{ $studentAttendSpecialTsuika->student->name }}</td>
            <td>{{ \JJSS::printDate($studentAttendSpecialTsuika->request_date) }}</td>
            <td>{{ $studentAttendSpecialTsuika->term->term_name }}</td>
            <td>{{ $studentAttendSpecialTsuika->teacher->name }}</td>
            <td>{{ ($studentAttendSpecialTsuika->syounin_teacher_id !== null) ? '済' : '未' }}</td>
            <td>{{ ($studentAttendSpecialTsuika->syounin_torikeshi_shinsei_flag == 1 ? '申請中' : '') }}</td>
            <td>
              <button class="btn-sm" onclick="location.href='{{ url('/master/student_attend_special_tsuika'). '/'. $studentAttendSpecialTsuika->id }}';">編集</button>
              <button class="btn-sm" onclick="window.open('{{ url('/master/student_attend_special_tsuika/print'). '/'. $studentAttendSpecialTsuika->id }}', '_blank')"><i class='bx bxs-printer'></i>印刷</button>
            </td>
          </tr>
        @endforeach
      @endif
      </tbody>
    </table>
  </div>
  <div class="flex flex-j-between">
    <div class="btn_list">
      <button type="button" id="ikkatsu_torikeshi" style="width: 120px;">一括申請取消</button>
    </div>
    <div class="pager txt-ctr">
      {{ $studentAttendSpecialTsuikas->appends(array('school_id' => (isset($search['school_id'])===true ? $search['school_id'] : ''),
                                 'student_code' => (isset($search['student_code'])===true ? $search['student_code'] : ''),
                                 'student_name' => (isset($search['student_name'])===true ? $search['student_name'] : ''),
                                 'teacher_id' => (isset($search['teacher_id'])===true ? $search['teacher_id'] : ''),
                                 'syounin_teacher_id' => (isset($search['syounin_teacher_id'])===true ? $search['syounin_teacher_id'] : ''),

                                 'orderby_column_name' => (isset($orderby['column_name'])===true ? $orderby['column_name'] : ''),
                                 'orderby_type' => (isset($orderby['type'])===true ? $orderby['type'] : '')
                                 ))->links() }}
    </div>
  </div>

  <form id="form" method="post" action="{{ url('master/student_attend_special_tsuika_ikkatsu_delete') }}" >
    {{ csrf_field() }}
    <input type="hidden" id="ids" name="ids">
  </form>

</section>
@endsection
@section('script')
<script>
$(function() {

  $('#teacher_id').select2({ language: "ja"});

  $('#btn-search').on('click', function(){
    $('#search_form').submit();
  });

  $('.set_orderby').on('click', function() {
    var current_orderby = $('input[name="orderby_type"]').val();
    var current_column = $('input[name="orderby_column_name"]').val();
    var new_column = $(this).attr('dt-column');

    if (current_column == new_column && current_orderby == "asc") {
      $('input[name="orderby_type"]').val('desc');
    } else {
      $('input[name="orderby_column_name"]').val(new_column);
      $('input[name="orderby_type"]').val('asc');
    }

    $('#search_form').submit();
  });

  $('#ikkatsu_torikeshi').on('click', function() {
    var selected_id = [];
    $('.check').each(function() {
      if($(this).prop('checked')) {
        if($(this).attr('data-student_attend_special_tsuika_id') !== undefined) {
          selected_id.push($(this).attr('data-student_attend_special_tsuika_id'));
        }
      }
    });

    if(selected_id.length == 0) {
      var options = {
        text: "対象の特別講習申込情報にチェックしてください。",
        icon: "info",
        timer: 2000,
        buttons: false,
      }
      swal(options);
      return;
    }

    swal({
      text: "一括申請取消を行います。よろしいですか？",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#ids').val(selected_id.join(','));
        $('#form').submit();
      }
    });
  });

});
</script>
@endsection
