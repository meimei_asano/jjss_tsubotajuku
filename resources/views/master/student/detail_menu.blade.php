<style>
    .tab_area {
        font-size:0;
        margin:0 10px;
        border-bottom: 1px solid #d3d7d7;
    }
    .tab_area li {
        width:150px;
        margin:0 5px;
        display:inline-block;
        padding:12px 0;
        color: #525d5d;
        background:#FDF4E7;
        text-align:center;
        font-size:13px;
        cursor:pointer;
        transition:ease 0.2s opacity;
        border-top: 1px solid #d3d7d7;
        border-right: 1px solid #d3d7d7;
        border-left: 1px solid #d3d7d7;
    }
    .tab_area li:hover {
        opacity:0.5;
    }
    .tab_area li.active {
        position: relative;
        top: 1px;
        background-color: white;
    }
    .tab_area li.is_disabled {
        background-color: #ddd;
    }
</style>

@php
    $permission = \JJSS::getPermission();
@endphp

<ul class="tab_area" >
    <a href="{{ asset('/master/student/' . $student->id) }}"><li class="{{ request()->is('*master/student/*') ? 'active' : '' }}">基本</li></a>
    <a href="{{ url('/master/student_pif/' . (isset($student->id) === true ? $student->id : '')) }}"><li class="{{ request()->is('*master/student_pif/*') ? 'active' : '' }} {{ strlen($student->id) > 0 ? '' : 'is_disabled' }}">PIF</li></a>
    <a href="{{ url('/master/student_seikyu/' . (isset($student->id) === true ? $student->id : '')) }}"><li class="{{ request()->is('*master/student_seikyu/*') ? 'active' : '' }} {{ strlen($student->id) > 0 ? '' : 'is_disabled' }}">請求関連</li></a>
    @if(in_array($permission, array('管理者','校長')))
        <a href="{{ url('/master/student_course/' . (isset($student->id) === true ? $student->id : '')) }}"><li class="{{ request()->is('*master/student_course/*') ? 'active' : '' }} {{ strlen($student->id) > 0 ? '' : 'is_disabled' }}">コース登録・変更</li></a>
    @endif
    @if(in_array($permission, array('管理者','校長','マネージャー')))
        <a href="{{ url('/master/student_attend/' . (isset($student->id) === true ? $student->id : '')) }}"><li class="{{ request()->is('*master/student_attend/*') ? 'active' : '' }} {{ strlen($student->id) > 0 ? '' : 'is_disabled' }}">コース・通塾予定</li></a>
    @endif
    @if(in_array($permission, array('管理者','校長','講師','マネージャー')))
        <a href="{{ url('/master/student_attend_special/' . (isset($student->id) === true ? $student->id : '')) }}"><li class="{{ request()->is('*master/student_attend_special/*') ? 'active' : '' }} {{ strlen($student->id) > 0 ? '' : 'is_disabled' }}">特別期間通塾予定</li></a>
    @endif
</ul>

<div class="mgn-btm40"></div>
