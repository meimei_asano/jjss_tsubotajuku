@extends('layouts.default')
@section('content')
<style>
  .set_orderby {cursor:pointer;}
</style>
<section class="scroll_table-wrap wht_bloc">

  <h2 class="heading-1">生徒一覧</h2>

  <div class="flex flex-j-between flex-a-ctr mgn-btm16">
    <form id="form" action="{{  url('master/student') }}" class="mgn-btm16" >
      <div class="form-group flex">
        <input type="text" id="search_name" name="search_name" placeholder="生徒名" class="input-sm" value="{{ ($search['name']) ?? '' }}">
        &nbsp;
        <select id="search_school_id" name="search_school_id" >
          <option value="">所属校舎</option>
          {!! \JJSS::options($schools, 'id', 'name', ($search['school_id']) ?? '') !!}
        </select>
        &nbsp;
        <button id="btn-search">検索<i class='bx bx-search'></i></button>
        <div class="loading" style="display: none;"><i class='bx bx-loader-alt' ></i></div>

        <input type="hidden" name="orderby_column_name" value="{{ ($orderby['column_name']) ?? '' }}">
        <input type="hidden" name="orderby_type" value="{{ ($orderby['type']) ?? '' }}">
      </div>
    </form>
    <div class="btn_list">
      @if(in_array(\JJSS::getPermission(), array('管理者')))
        <button id="btn-csv_export" ><i class='bx bx-export'></i>CSVダウンロード</button>
      @endif
      @if(in_array(\JJSS::getPermission(), array('管理者')))
      <button id="create" onclick="location.href='{{ url('master/student'). '/new' }}';"><i class='bx bxs-user-plus'></i>追加</button>
      @endif
    </div>
  </div>

  <div class="scroll_table mgn-btm16">
    <table class="table-horizon table-striped datatable">
      <thead>
        <tr>
          <th class="set_orderby" dt-column="student_code">生徒コード</th>
          <th class="set_orderby" dt-column="name">生徒名</th>
          <th class="set_orderby" dt-column="kana">カナ</th>
          <th class="set_orderby" dt-column="school_id">所属校舎</th>
          <th class="set_orderby" dt-column="grade_code">学年</th>
          <th>9タイプ</th>
          <th>コース</th>
          <th>担当講師</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      @if(count($students) == 0)
        <tr>
          <td colspan="8">
            登録されていません
          </td>
        </tr>
      @else
        @foreach ($students as $student)
        <tr style="">
          <td class="">{{ $student->student_code }}</td>
          <td class="">{{ $student->name }}</td>
          <td class="">{{ $student->kana }}</td>
          <td class="">{{ $student->school->name }}</td>
          <td class="">{{ config('jjss.grade')[$student->grade_code] ?? '' }}</td>
          <td class="">{{ ($student->ninetype) ? \JJSS::displayNineType($student->ninetype->ninetype_codes) : '' }}</td>
          <td class="">
            @if(count($student->courses_today) > 0)
              {{ $student->courses_today[0]->name }}
            @elseif(count($student->attend_bases) > 0)
              {{ $student->attend_bases[0]->course->name }}
            @endif
          </td>
          <td class="">{{ $student->teacher->name or null }}</td>
          <td class="txt-rgt">
            <button class="btn-sm" onclick="location.href='{{ url('master/student'). '/'. $student->id }}';">変更</button>
          </td>
        </tr>
        @endforeach
      @endif
      </tbody>
    </table>
  </div>
  <div class="flex flex-j-between">
    <div class="btn_list">
    </div>
    <div class="pager txt-ctr">
      {{ $students->appends(array('search_name' => (isset($search['name'])===true ? $search['name'] : ''),
                                  'search_school_id' => (isset($search['school_id'])===true ? $search['school_id'] : ''),
                                  'orderby_column_name' => (isset($orderby['column_name'])===true ? $orderby['column_name'] : ''),
                                  'orderby_type' => (isset($orderby['type'])===true ? $orderby['type'] : '')
                                  ))->links() }}
    </div>
  </div>
</section>
@endsection

@section('script')
<script>
$(function(){
  $('#search_school_id').select2({ language: "ja"});

  $('#btn-search').on('click', function() {
    $('#form').submit();
  });

  $('.set_orderby').on('click', function() {
    var current_orderby = $('input[name="orderby_type"]').val();
    var current_column = $('input[name="orderby_column_name"]').val();
    var new_column = $(this).attr('dt-column');

    if (current_column == new_column && current_orderby == "asc") {
      $('input[name="orderby_type"]').val('desc');
    } else {
      $('input[name="orderby_column_name"]').val(new_column);
      $('input[name="orderby_type"]').val('asc');
    }

    $('#form').submit();
  });

  $('#btn-csv_export').on('click', function() {

    location.href = '<?php echo e(url('/master/student_csv_export')); ?>'
                      + '?search_name=' + $('#search_name').val()
                      + '&search_school_id=' + $('#search_school_id').val()
                      + '&orderby_column_name=' + $('input[name="orderby_column_name"]').val()
                      + '&orderby_type=' + $('input[name="orderby_type"]').val();
  });

});
</script>
@endsection
