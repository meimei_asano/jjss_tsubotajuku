@extends('layouts.default')
@section('content')
<form id="form" method="post" action="">
  {{ csrf_field() }}
  <section class="scroll_table-wrap wht_bloc">

  <h2 class="heading-1">生徒情報</h2>

  <div class="">

    @include('master.student.detail_menu')

    <table class="datatable table-vertical">
      <tr>
        <th>生徒コード</th>
        <td>{{ $student->student_code or null}}</td>
      </tr>
      <tr>
        <th>生徒名</th>
        <td>{{ $student->name or null }}</td>
      </tr>
      <tr>
        <th>コース</th>
        <td class="form-group flex">
          @if(count($student->attend_bases))
            <div style="width: 45%;">
              @foreach($student->attend_bases as $attend_base)
                @if(!($attend_base->end_date) || (Carbon\Carbon::today()->lte(Carbon\Carbon::parse($attend_base->end_date))) || (Carbon\Carbon::today()->lte(Carbon\Carbon::parse($attend_base->start_date))))
                  <p>{{ $attend_base->course->name or null }}　　（{{ $attend_base->start_date }} 〜 {{ strlen($attend_base->end_date) > 0 ? $attend_base->end_date : '' }}）</p>
                @endif
              @endforeach
            </div>
          @endif
        </td>
      </tr>
      {{-- table:student_shibous ↓↓ --}}
      <tr>
        <th>志望校</th>
        <td class="form-group">
          <div class="mgn-btm8">学校：<input type="text" id="shibou_school" name="shibou_school" value="{{ old('shibou_school', isset($student_shibou->shibou_school) === true ? $student_shibou->shibou_school : '') }}" class="input-lg" ></div>
          <div>学部：<input type="text" id="shibou_gakubu" name="shibou_gakubu" value="{{ old('shibou_gakubu', isset($student_shibou->shibou_gakubu) === true ? $student_shibou->shibou_gakubu : '') }}" class="input-lg" ></div>
        </td>
      </tr>
      <tr>
        <th>使用科目</th>
        <td class="form-group">
          <input type="text" id="shibou_kamokus" name="shibou_kamokus" value="{{  old('shibou_kamokus', isset($student_shibou->kamokus) === true ? $student_shibou->kamokus : '') }}" class="input-lg">
        </td>
      </tr>
      {{-- table:student_shibous ↑↑ --}}
      <tr>
        <th>学習科目</th>
        <td class="form-group">
          @php
              $student_kamokus = old('student_kamoku');
              if($student_kamokus === null) {
                  $student_kamokus = [];
                  if($student->kamokus) {
                      foreach($student->kamokus as $_kamokus) {
                          $student_kamokus[] = $_kamokus->kamoku_id;
                      }
                  }
              }
          @endphp
          @if(count($kamokus) > 0)
            @foreach($kamokus as $kamoku)
              {{ in_array($kamoku->id, $student_kamokus) === true ? $kamoku->name : '' }}&nbsp;
            @endforeach
          @endif
        </td>
      </tr>
      <tr>
        <th>９タイプ</th>
        <td class="form-group">
          @php
            $student_ninetypes = old('ninetype_codes', (isset($student->ninetype->ninetype_codes) === true ? explode(',', $student->ninetype->ninetype_codes) : array()));
          @endphp
          @foreach($ninetypes as $type_code => $type_name)
              {{ in_array($type_code, $student_ninetypes) === true ? $type_code . ':' . $type_name . '　　' : '' }}
          @endforeach
        </td>
      </tr>
      <tr>
        <th>部活</th>
        <td class="form-group">
          <input type="text" id="bukatsu" name="bukatsu" value="{{  old('bukatsu', isset($student_pif->bukatsu) === true ? $student_pif->bukatsu : '') }}" class="input-lg" >
        </td>
      </tr>
      <tr>
        <th>学校関係特記</th>
        <td class="form-group">
          <input type="text" id="gakkou_tokki" name="gakkou_tokki" value="{{  old('gakkou_tokki', isset($student_pif->gakkou_tokki) === true ? $student_pif->gakkou_tokki : '') }}" class="input-lg" >
        </td>
      </tr>
      <tr>
        <th>夢</th>
        <td class="form-group">
          <input type="text" id="yume" name="yume" value="{{  old('yume', isset($student_pif->yume) === true ? $student_pif->yume : '') }}" class="input-lg" >
        </td>
      </tr>
      <tr>
        <th>趣味</th>
        <td class="form-group">
          <input type="text" id="syumi" name="syumi" value="{{  old('syumi', isset($student_pif->syumi) === true ? $student_pif->syumi : '') }}" class="input-lg" >
        </td>
      </tr>
      <tr>
        <th>家族構成</th>
        <td class="form-group">
          <input type="text" id="kazoku_kousei" name="kazoku_kousei" value="{{  old('kazoku_kousei', isset($student_pif->kazoku_kousei) === true ? $student_pif->kazoku_kousei : '') }}" class="input-lg" >
        </td>
      </tr>
      <tr>
        <th>家庭環境</th>
        <td class="form-group">
          <input type="text" id="katei_jijyou" name="katei_jijyou" value="{{  old('katei_jijyou', isset($student_pif->katei_jijyou) === true ? $student_pif->katei_jijyou : '') }}" class="input-lg" >
        </td>
      </tr>
      <tr>
        <th>赤点・進級</th>
        <td class="form-group">
          <input type="text" id="akaten_shinkyu" name="akaten_shinkyu" value="{{  old('akaten_shinkyu', isset($student_pif->akaten_shinkyu) === true ? $student_pif->akaten_shinkyu : '') }}" class="input-lg" >
        </td>
      </tr>
      <tr>
        <th>定期(日付)</th>
        <td class="form-group">
          <input type="text" id="teiki_test" name="teiki_test" value="{{  old('teiki_test', isset($student_pif->teiki_test) === true ? $student_pif->teiki_test : '') }}" class="input-lg" >
        </td>
      </tr>
      <tr>
        <th>予定</th>
        <td>
          @if(count($student->attend_bases) > 0)
            @foreach($student->attend_bases as $attend_base)
              {{-- start_date < 今日 < end_date(or 未登録) なら表示 --}}
              @if(Carbon\Carbon::parse($attend_base->start_date)->lt(Carbon\Carbon::today()) && (strlen($attend_base->end_date) == 0 || Carbon\Carbon::today()->lt(Carbon\Carbon::parse($attend_base->end_date))))
                <div class="flex">
                  <div class="txt-ctr" style="width:20%;">
                    <p>月</p><p>{{ \JJSS::displayTime($attend_base->mon_start_time) }} - {{ \JJSS::displayTime($attend_base->mon_end_time) }}</p>
                  </div>
                  <div class="txt-ctr" style="width:20%;">
                    <p>火</p><p>{{ \JJSS::displayTime($attend_base->tue_start_time) }} - {{ \JJSS::displayTime($attend_base->tue_end_time) }}</p>
                  </div>
                  <div class="txt-ctr" style="width:20%;">
                    <p>水</p><p>{{ \JJSS::displayTime($attend_base->wed_start_time) }} - {{ \JJSS::displayTime($attend_base->wed_end_time) }}</p>
                  </div>
                  <div class="txt-ctr" style="width:20%;">
                    <p>木</p><p>{{ \JJSS::displayTime($attend_base->thu_start_time) }} - {{ \JJSS::displayTime($attend_base->thu_end_time) }}</p>
                  </div>
                  <div class="txt-ctr" style="width:20%;">
                    <p>金</p><p>{{ \JJSS::displayTime($attend_base->fri_start_time) }} - {{ \JJSS::displayTime($attend_base->fri_end_time) }}</p>
                  </div>
                </div>
              @endif
            @endforeach
          @endif
        </td>
      </tr>
      <tr>
        <th>帰宅時間配慮</th>
        <td class="form-group">
          <input type="text" id="hairyo_jikou" name="hairyo_jikou" value="{{  old('hairyo_jikou', isset($student_pif->hairyo_jikou) === true ? $student_pif->hairyo_jikou : '') }}" class="input-lg" >
        </td>
      </tr>
      <tr>
        <th>言ってはいけない(Taboo Word)</th>
        <td class="form-group">
          <textarea id="iwanai" name="iwanai">{{  old('iwanai', isset($student_pif->iwanai) === true ? $student_pif->iwanai : '') }}</textarea>
        </td>
      </tr>

    </table>
  </div>
  <div class="datatable btn_list txt-ctr">
    @if(in_array(\JJSS::getPermission(), array('管理者', '校長', '講師')))
    <button type="button" id="save" class="btn-lg">保存</button>
    @endif
    <input type="hidden" id="student_id" name="student_id" value="{{ $student->id }}">
  </div>
</div>
</form>
@endsection

@section('script')
  <script>
    $(function(){

      $('#save').on('click', function() {
        $('#form').attr('action', '{{ url('/master/student_pif') }}');
        $('#form').submit();
      });

    })
  </script>
@endsection
