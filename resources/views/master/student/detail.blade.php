@extends('layouts.default')
@section('content')
<form id="form" method="post" action="">
  {{ csrf_field() }}
  <section class="scroll_table-wrap wht_bloc">

  <h2 class="heading-1">生徒情報</h2>

  <div class="">

    @include('master.student.detail_menu')

    <table class="datatable table-vertical">
      <tr>
        <th>生徒コード</th>
        <td class="form-group">
          <input type="text" id="student_code" name="student_code" class="input-lg" placeholder="000-00-000" value="{{ old('student_code', $student->student_code) }}">
        </td>
      </tr>
      <tr>
        <th>名前</th>
        <td class="form-group">
          <input type="text" id="name" name="name" class="input-lg" value="{{ old('name', $student->name) }}">
        </td>
      </tr>
      <tr>
        <th>カナ</th>
        <td class="form-group">
          <input type="text" id="kana" name="kana" class="input-lg" value="{{ old('kana', $student->kana) }}">
        </td>
      </tr>
      <tr>
        <th>所属校舎</th>
        <td class="form-group">
          <select name="school_id">
            <option value="">---</option>
            @if(count($schools) > 0)
              @foreach($schools as $school)
                <option value="{{ $school->id }}" {{ $school->id == old('school_id', $student->school_id) ? 'selected="selected"' : '' }} >{{ $school->name }}</option>
              @endforeach
            @endif
          </select>
        </td>
      </tr>
      <tr>
        <th>担当講師</th>
        <td class="form-group">
          <select name="teacher_id">
            <option value="">---</option>
            @if(count($teachers) > 0)
              @foreach($teachers as $teacher)
                <option value="{{ $teacher->id }}" {{ $teacher->id == old('teacher_id', $student->teacher_id) ? 'selected="selected"' : '' }} >{{ $teacher->name }}</option>
              @endforeach
            @endif
          </select>
        </td>
      </tr>
      <tr>
        <th>学校</th>
        <td class="form-group">
          <input type="text" id="gakkou_name" name="gakkou_name" class="input-lg" value="{{ old('gakkou_name', $student->gakkou_name) }}">
        </td>
      </tr>
      <tr>
        <th>学年</th>
        <td class="form-group">
          <select name="grade_code">
            <option value="">---</option>
            @foreach(config('jjss.grade') as $key => $grade)
              <option value="{{ $key }}" {{ $key == old('grade_code', $student->grade_code) ? 'selected="selected"' : '' }} >{{ $grade }}</option>
            @endforeach
          </select>
        </td>
      </tr>
      <tr>
        <th>コース</th>
        <td class="form-group flex">
          @if(count($student->attend_bases))
            <div style="width: 45%;">
              @foreach($student->attend_bases as $attend_base)
                @if(!($attend_base->end_date) || (Carbon\Carbon::today()->lte(Carbon\Carbon::parse($attend_base->end_date))) || (Carbon\Carbon::today()->lte(Carbon\Carbon::parse($attend_base->start_date))))
                  <p>{{ $attend_base->course->name or null }}　　（{{ $attend_base->start_date }} 〜 {{ strlen($attend_base->end_date) > 0 ? $attend_base->end_date : '' }}）</p>
                @endif
              @endforeach
            </div>
          @endif
          @if(in_array(\JJSS::getPermission(), array('管理者')))
            <div class="{{ strlen($student->id) > 0 ? '' : 'is_disabled' }}">
              <button type="button" class="change_course">コース変更</button>
            </div>
          @endif
        </td>
      </tr>
      <tr>
        <th>学習科目</th>
        <td class="form-group">
          <ul class="flex flex-col4">
            @php
                $student_kamokus = old('student_kamoku');
                if($student_kamokus === null) {
                    $student_kamokus = [];
                    if($student->kamokus) {
                        foreach($student->kamokus as $_kamokus) {
                            $student_kamokus[] = $_kamokus->kamoku_id;
                        }
                    }
                }
            @endphp
            @if(count($kamokus) > 0)
              @foreach($kamokus as $kamoku)
                <li>
                  <input type="checkbox" name="student_kamoku[]" id="student_kamoku{{ $kamoku->id }}"
                         value="{{ $kamoku->id }}" {{ in_array($kamoku->id, $student_kamokus) === true ? 'checked="checked"' : '' }} >
                  <label for="student_kamoku{{ $kamoku->id }}">{{ $kamoku->name }}</label>
                </li>
              @endforeach
            @endif
          </ul>
        </td>
      </tr>
      <tr>
        <th>９タイプ</th>
        <td class="form-group">
          @php
            $student_ninetypes = old('ninetype_codes', (isset($student->ninetype->ninetype_codes) === true ? explode(',', $student->ninetype->ninetype_codes) : array()));
          @endphp
          <ul class="flex flex-col4">
          @foreach($ninetypes as $type_code => $type_name)
            <li>
              <input type="checkbox" name="ninetype_codes[]" value="{{ $type_code }}" id="ninetype_codes{{ $type_code }}" {{ in_array($type_code, $student_ninetypes) === true ? 'checked="checked"' : '' }}><label for="ninetype_codes{{ $type_code }}">{{ $type_name }}</label>
            </li>
          @endforeach
          </ul>
{{--
          <hr>
          <ul class="flex flex-col4">
            @foreach($ninetypes as $type_code => $type_name)
              <li>
                {{ $type_name }}
                <input type="text" name="score_type{{ $type_code }}" id="score_type{{ $type_code }}"
                       value="{{ old('score_type' . $type_code, isset($student->student_ninetype->{'score_type' . $type_code}) === true ? $student->student_ninetype->{'score_type' . $type_code} : '') }}" class="flt-rgt txt-rgt" style="min-width:75px;width:75px;">
              </li>
            @endforeach
          </ul>
--}}
        </td>
      </tr>
      <tr>
        <th>入塾年月日</th>
        <td class="form-group">
          <input type='text' id="nyujyuku_date" name="nyujyuku_date" class="datepicker-here" data-language="jp" value="{{ old('nyujyuku_date', $student->nyujyuku_date) }}" />
        </td>
      </tr>
      <tr>
        <th>退塾年月日</th>
        <td class="form-group">
          <input type='text' id="taijyuku_date" name="taijyuku_date" class="datepicker-here" data-language="jp" value="{{ old('taijyuku_date', $student->taijyuku_date) }}" />
        </td>
      </tr>
      <tr>
        <th>卒塾年月日</th>
        <td class="form-group">
          <input type='text' id="sotsujyuku_date" name="sotsujyuku_date" class="datepicker-here" data-language="jp" value="{{ old('sotsujyuku_date', $student->sotsujyuku_date) }}" />
        </td>
      </tr>
      <tr>
        <th>生年月日</th>
        <td class="form-group">
          <input type='text' id="birth_date" name="birth_date" class="datepicker-here" data-language="jp" value="{{ old('birth_date', $student->birth_date) }}" />
        </td>
      </tr>
      <tr>
        <th>郵便番号</th>
        <td class="form-group">
          <input type="text" id="zip_code" name="zip_code" class="" value="{{ old('zip_code', $student->zip_code) }}">
        </td>
      </tr>
      <tr>
        <th>都道府県</th>
        <td class="form-group">
          <select id="pref" name="pref">
            <option value="">選択してください</option>
            @foreach(config('pref') as $pref)
              <option value="{{ $pref }}" {{ $pref == old('pref', $student->pref) ? 'selected="selected"' : '' }} >{{ $pref }}</option>
            @endforeach
          </select>
        </td>
      </tr>
      <tr>
        <th>住所1</th>
        <td class="form-group">
          <input type="text" id="address1" name="address1" class="input-lg" value="{{ old('address1', $student->address1) }}">
        </td>
      </tr>
      <tr>
        <th>住所2</th>
        <td class="form-group">
          <input type="text" id="address2" name="address2" class="input-lg" value="{{ old('address2', $student->address2) }}">
        </td>
      </tr>
      <tr>
        <th>自宅電話番号</th>
        <td class="form-group">
          <input type="text" id="home_tel" name="home_tel" class="" value="{{ old('home_tel', $student->home_tel) }}">
        </td>
      </tr>
      <tr>
        <th>本人電話番号</th>
        <td class="form-group">
          <input type="text" id="student_tel" name="student_tel" class="" value="{{ old('student_tel', $student->student_tel) }}">
        </td>
      </tr>
      <tr>
        <th>保護者氏名</th>
        <td class="form-group">
          <input type="text" id="hogosya_name" name="hogosya_name" class="input-lg" value="{{ old('hogosya_name', $student->hogosya_name) }}">
        </td>
      </tr>
      <tr>
        <th>保護者カナ</th>
        <td class="form-group">
          <input type="text" id="hogosya_kana" name="hogosya_kana" class="input-lg" value="{{ old('hogosya_kana', $student->hogosya_kana) }}">
        </td>
      </tr>
      <tr>
        <th>保護者携帯</th>
        <td class="form-group">
          <input type="text" id="hogosya_tel" name="hogosya_tel" class="" value="{{ old('hogosya_tel', $student->hogosya_tel) }}">
        </td>
      </tr>
      <tr>
        <th>保護者メールアドレス</th>
        <td class="form-group">
          <input type="text" id="hogosya_email" name="hogosya_email" class="input-lg" value="{{ old('hogosya_email', $student->hogosya_email) }}">
        </td>
      </tr>

    </table>
  </div>
  <div class="datatable btn_list">
    @if(in_array(\JJSS::getPermission(), array('管理者', '校長')))
    <button type="button" id="cancel" class="btn-info btn-line btn-lg">キャンセル</button>
    <button type="button" id="save" class="btn-lg">保存</button>
    @endif
    <input type="hidden" id="student_id" name="student_id" value="{{ $student->id }}">
  </div>
</div>
</form>
@endsection

@section('script')
  <script>
    $(function(){

      $('#save').on('click', function() {
        $('#form').attr('action', '{{ url('/master/student') }}');
        $('#form').submit();
      });

      $('#cancel').on('click', function() {
        location.href = '{{ url('/master/student') }}';
      });

      $('.change_course').on('click', function() {
        location.href = '{{ url('/master/student_course/' . (isset($student->id) === true ? $student->id : '')) }}';
      });
    })
  </script>
@endsection
