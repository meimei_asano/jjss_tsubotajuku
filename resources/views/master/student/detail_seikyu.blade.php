@extends('layouts.default')
@section('content')
<form id="form" method="post" action="">
  {{ csrf_field() }}
  <section class="scroll_table-wrap wht_bloc">

  <h2 class="heading-1">生徒情報</h2>

  <div class="">

    @include('master.student.detail_menu')

    <table class="datatable table-vertical">
      <tr>
        <th>生徒コード</th>
        <td>{{ $student->student_code or null}}</td>
      </tr>
      <tr>
        <th>生徒名</th>
        <td>{{ $student->name or null }}</td>
      </tr>
      <tr>
        <th>引落申込み用紙受取日</th>
        <td class="form-group">
          <input type='text' id="hikiotoshi_uketori_date" name="hikiotoshi_uketori_date" class="datepicker-here" data-language="jp" value="{{ old('hikiotoshi_uketori_date', $student->hikiotoshi_uketori_date) }}" />
        </td>
      </tr>
      {{-- 引落情報 ↓↓↓ --}}
      <tr>
        <th>引落開始日</th>
        <td class="form-group">
          <input type='text' id="hikiotoshi_start_date" name="hikiotoshi_start_date" class="datepicker-here" data-language="jp" value="{{ old('hikiotoshi_start_date', $student->hikiotoshi_start_date) }}" />
        </td>
      </tr>
      <tr>
        <th>銀行</th>
        <td class="form-group">
          コード：<input type="text" id="bank_code" name="bank_code" value="{{ old('bank_code', $student->bank_code) }}" length="4" maxlength="4" style="min-width:100px;width:100px;">
          名前：<input type="text" id="bank_name" name="bank_name" class="input-lg" value="{{ old('bank_name', $student->bank_name) }}" >
        </td>
      </tr>
      <tr>
        <th>支店</th>
        <td class="form-group">
          コード：<input type="text" id="branch_code" name="branch_code" value="{{ old('branch_code', $student->branch_code) }}" length="3" maxlength="3" style="min-width:100px;width:100px;">
          名前：<input type="text" id="branch_name" name="branch_name" class="input-lg" value="{{ old('branch_name', $student->branch_name) }}" >
        </td>
      </tr>
      <tr>
        <th>預金種目</th>
        <td class="form-group">
          <select name="deposit_type" id="deposit_type">
            <option value="" >----</option>
            @php
              $deposit_type = old('deposit_type', $student->deposit_type);
            @endphp
            @if(count(config('jjss.deposit_types')) > 0)
              @foreach(config('jjss.deposit_types') as $deposit_key => $deposit_value)
                <option value="{{ $deposit_key }}" <?php echo ($deposit_type == $deposit_key ? 'selected="selected"' : ''); ?>>{{ $deposit_value }}</option>
              @endforeach
            @endif
          </select>
        </td>
      </tr>
      <tr>
        <th>通帳番号</th>
        <td class="form-group">
          <input type="text" id="account_number" name="account_number" value="{{ old('account_number', $student->account_number) }}" >
        </td>
      </tr>
      <tr>
        <th>名義人名</th>
        <td class="form-group">
          <input type="text" id="account_holder" name="account_holder" class="input-lg" value="{{ old('account_holder', $student->account_holder) }}" >
        </td>
      </tr>
      <tr>
        <th>顧客番号</th>
        <td class="form-group">
          <input type="text" id="customer_number" name="customer_number" value="{{ old('customer_number', $student->customer_number) }}" >
        </td>
      </tr>
      <tr>
        <th>委託者番号</th>
        <td class="form-group">
          <input type="text" id="itaku_number" name="itaku_number" value="{{ old('itaku_number', $student->itaku_number) }}" >
        </td>
      </tr>
      {{-- 引落情報 ↑↑↑ --}}
    </table>
  </div>
  <div class="datatable btn_list txt-ctr">
    @if(in_array(\JJSS::getPermission(), array('管理者', '本部スタッフ')))
    <button type="button" id="save" class="btn-lg">保存</button>
    @endif
    <input type="hidden" id="student_id" name="student_id" value="{{ $student->id }}">
  </div>
</div>
</form>
@endsection

@section('script')
  <script>
    $(function(){

      $('#save').on('click', function() {
        $('#form').attr('action', '{{ url('/master/student_seikyu') }}');
        $('#form').submit();
      });

    })
  </script>
@endsection
