<style>
body {
    font-size: 11pt;
}
table {
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
    table-layout: fixed;
    text-align: center;
}
th, td {
    padding: 0 5px;
}
.main {
    width: 800px;
}
.bordered {
    border: solid 1px black;
}
.height-mn {
    height: 2em;
}
.text-center {
    text-align: center;
}
.text-right {
    text-align: right;
}
.bg-color-silver {
    background-color: silver;
}
.clr {
    clear: both;
    margin: 0;
    padding: 0;
}
.row-1 .col-1 {
    float: left;
    width: 20%;
    border: solid 1px black;
    padding: 3.5px 0;
    margin: 0;
    text-align: center;
    background-color: silver;
    font-size: 16pt;
}
.row-1 .col-2 {
    float: right;
    width: 20%;
    text-align: right;
}
.row-2 {
    font-size: 21.5pt;
    text-decoration: underline;
    font-weight: bold;
    text-align: center;
}
.row-indent {
    float: left;
    width: 3.5%;
}
.row-indent-right {
    float: right;
    width: 3.5%;
}
.row-3 .col-1,
.row-9 .col-1,
.row-10 .col-1,
.row-12 .col-1,
{
    float: left;
}
.row-11 .col-1 {
    float: left;
    width: 93%;
}
.group-row .col-1 {
    float: left;
    width: 60%;
}
.group-row .col-1 div div.student-name,
.group-row .col-1 div div.under-student-name
{
    float: left;
    font-size: 13pt;
}
.group-row .col-2 {
    float: right;
    width: 35%;
}
.group-row .col-2 div.school-name {
    font-size: 15pt;
}
.row-last .col-1 {
    float: right;
    width: 22%;
}
</style>
<div class="main">
    <div class="row-1">
        <div class="col-1">{{ $format === 'home' ? '自宅保管用' : '校舎保管用' }}</div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-2">{{ \JJSS::printDate($studentSoldText->request_date) }}</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-2">使用教材受渡し表</div>
    <br>
    <div class="row-3">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">〒{{ $studentSoldText->student->zip_code }}</div>
        <div class="clr"></div>
    </div>
    <div class="group-row">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">
            <div>{{ $studentSoldText->student->pref . ' ' . $studentSoldText->student->address1 }}</div>
            <div>{{ $studentSoldText->student->address2 }}</div>
            <div>
                <div style="float: left; width: 5%">&nbsp;</div>
                <div class="student-name">{{ $studentSoldText->student->name }}　様</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 5%">&nbsp;</div>
                <div class="under-student-name">保護者様</div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-2">
            <div class="school-name">坪田塾　{{ $studentSoldText->student_school[0]->name }}</div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>{{ $studentSoldText->student_school[0]->pref . ' ' . $studentSoldText->student_school[0]->address }}</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div>{{ $studentSoldText->student_school[0]->building }}</div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div></div>
                <div class="clr"></div>
            </div>
            <div>
                <div style="float: left; width: 10%">&nbsp;</div>
                <div></div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-9">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">平素は格別のご高配に賜り、誠にありがとうございます。</div>
        <div class="clr"></div>
    </div>
    <div class="row-10">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">下記の教材を当塾にて購入いたしましたので、ご連絡させて頂きます。</div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-11">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">
            <div style="float: left; width: 5%">&nbsp;</div>
            <div style="float: left;">
                @php
                    $textRequests = json_decode($studentSoldText->text_request, JSON_UNESCAPED_UNICODE);
                @endphp
                <table>
                    <tr>
                        <td colspan="2" class="bordered text-center height-mn" style="width: 78%">教材名</td>
                        <td class="bordered text-center height-mn" style="width: 22%">金額</td>
                    </tr>
                    @foreach($textRequests as $textRequest)
                    <tr>
                        <td colspan="2" class="bordered height-mn" style="text-align: left;">{{ $textRequest['text'] }}</td>
                        <td class="bordered text-right height-mn">{{ number_format($textRequest['price']) }}円</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td style="width: 63%;">&nbsp;</td>
                        <td class="bordered height-mn bg-color-silver" style="width: 15%">小計</td>
                        <td class="bordered text-right height-mn" style="width: 22%">{{ number_format($studentSoldText->subtotal) }}円</td>
                    </tr>
                    <tr>
                        <td style="width: 63%;">&nbsp;</td>
                        <td class="bordered height-mn bg-color-silver" style="width: 15%">消費税</td>
                        <td class="bordered text-right height-mn" style="width: 22%">{{ number_format($studentSoldText->tax) }}円</td>
                    </tr>
                    <tr>
                        <td style="width: 63%;">&nbsp;</td>
                        <td class="bordered height-mn bg-color-silver" style="width: 15%">合計</td>
                        <td class="bordered text-right height-mn" style="width: 22%">{{ number_format($studentSoldText->total) }}円</td>
                    </tr>
                </table>
            </div>
            <div class="clr"></div>
        </div>
        <div class="row-indent-right"></div>
        <div class="clr"></div>
    </div>
    <br>
    <div class="row-12">
        <div class="row-indent">&nbsp;</div>
        <div class="col-1">代金は次回請求時に合わせてご請求させて頂きます。</div>
        <div class="clr"></div>
    </div>
    @if ($format === 'home')
    <br>
    <br>
    <div class="row-last">
        <div class="row-indent-right">&nbsp;</div>
        <div class="col-1">
            <table>
                <tr>
                    <th class="bordered">担当</th>
                    <th class="bordered">校長</th>
                </tr>
                <tr>
                    <td class="bordered" style="height: 60px;">&nbsp;</td>
                    <td class="bordered">&nbsp;</td>
                </tr>
            </table>
        </div>
        <div class="clr"></div>
    </div>
    @endif
</div>
