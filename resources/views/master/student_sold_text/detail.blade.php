@extends('layouts.default')
@section('content')
<style>
.set_container {
  position: relative;
  display: inline-block;
}
.remove_text_request_field {
  position: absolute;
  color: white;
  font-weight: bolder;
  background-color: red;
  border: solid 1px white;
  border-radius: 50%;
  padding: 0 8px;
  cursor: pointer;
  right: -20px;
  top: 15px;
}
.remove_text_request_field:hover {
  color: red;
  font-weight: bolder;
  background-color: white;
  border: solid 1px red;
}
.text_select {
  min-width: 22px !important;
  width: 22px !important;
}
</style>
<form id="form" method="post" action="">
  {{ csrf_field() }}
  <section class="scroll_table-wrap wht_bloc">
  <h2 class="heading-1">教材受け渡し情報</h2>

  @if($studentSoldText->syounin_torikeshi_shinsei_flag == "1")
    <div class="alert alert-danger flex flex-a-ctr">
      承認取消申請中です。
    </div>
  @endif

  <div class="">
    <table class="datatable table-vertical">
      <tr>
        <th>生徒</th>
        <td class="form-group">
          <select id="student_id" name="student_id">
            <option class="default" value="">選択してください</option>
            @if(count($students) > 0)
              @foreach($students as $student)
                <option value="{{ $student->id }}" {{ $student->id == old('student_id', $studentSoldText->student_id) ? 'selected="selected"' : '' }} >{{ $student->name }}</option>
              @endforeach
            @endif
          </select>
        </td>
      </tr>
      <tr>
        <th>受け渡し日</th>
        <td class="form-group">
          <input type="text" id="request_date" name="request_date" class="datepicker-here" autocomplete="off" data-language="jp" data-auto-close="true" value="{{ old('request_date', $studentSoldText->request_date) }}"/>
        </td>
      </tr>
      <tr>
        <th>受け渡し教材情報</th>
        <td class="form-group">
          <div id="text_request_group">
            @php
              $textRequest = old('text_request', json_decode($studentSoldText->text_request, JSON_UNESCAPED_UNICODE));
            @endphp
            @if($textRequest && is_array($textRequest) && count($textRequest) > 0)
              @foreach($textRequest as $key => $item)
                @if($key !== 0)
                  <br class="set_number_{{ $key }}">
                @endif
                <div class="set_container" data-set-number="{{ $key }}">
                  @if($key !== 0)
                    <hr>
                    <span class="remove_text_request_field">X</span>
                  @endif
                  <div class="mgn-btm8">
                    <b>教材名</b>
                    <input type="text" name="text_request[{{ $key }}][text]" placeholder="教材名" autocomplete="off" value="{{ $item['text'] }}"/>
                    <select class="text_select">
                      <option value="">---</option>
                      @php
                        $texts_optiton_html = '';
                        foreach($texts as $text) {
                            $texts_optiton_html .= '<option value="' . $text['name'] .  '" data_price="' . (int)$text['price'] . '">' . $text['name'] . '</option>';
                        }
                        echo $texts_optiton_html;
                      @endphp
                    </select>
                  </div>
                  <div class="mgn-btm8">
                    <b>金額</b>
                    <input type="text" data-format="number" name="text_request[{{ $key }}][price]" class="price" placeholder="金額" autocomplete="off" value="{{ $item['price'] }}"/> （半角数字のみ）
                  </div>
                </div>
              @endforeach
            @else
              <div class="set_container" data-set-number="0">
                <div class="mgn-btm8">
                  <b>教材名</b>
                  <input type="text" name="text_request[0][text]" placeholder="教材名" autocomplete="off" value=""/>
                  <select class="text_select">
                    <option value="">---</option>
                    @php
                          $texts_optiton_html = '';
                          foreach($texts as $text) {
                              $texts_optiton_html .= '<option value="' . $text['name'] .  '" data_price="' . (int)$text['price'] . '">' . $text['name'] . '</option>';
                          }
                          echo $texts_optiton_html;
                      @endphp
                  </select>
                </div>
                <div class="mgn-btm8">
                  <b>金額</b>
                  <input type="text" data-format="number" name="text_request[0][price]" class="price" placeholder="金額" autocomplete="off" value=""/> （半角数字のみ）
                </div>
              </div>
            @endif
          </div>
          <button type="button" id="add_text_request_fields">追加</button>
        </td>
      </tr>
      <tr>
        <th>合計</th>
        <td id="total" class="form-group">0</td>
      </tr>
      <tr>
        <th>登録先生</th>
        <td class="form-group">
          {{ $studentSoldText->teacher_id === null ? '' : (isset($studentSoldText->teacher->name)===true ? $studentSoldText->teacher->name : '') }}
        </td>
      </tr>
      <tr>
        <th>承認先生</th>
        <td class="form-group">
          {{ $studentSoldText->syounin_teacher_id === null ? '未承認' : $studentSoldText->syounin_teacher->name }}
        </td>
      </tr>
    </table>
  </div>
  <div class="datatable btn_list">
    {{-- キャンセル（一覧に遷移）--}}
    <button type="button" id="cancel" class="btn-info btn-line btn-lg btn-lg-slim">キャンセル</button>
    {{-- 申請（登録・更新） --}}
    @php
      $add_class = 'is_disabled';
      if(in_array($permission, ['校長', 'マネージャー', '本部スタッフ']) === false) {
        if($studentSoldText->id === null) {
          $add_class = '';
        }
        if($studentSoldText->id !== null && $studentSoldText->syounin_teacher_id === null
            && ($studentSoldText->teacher_id == $teacher->id || in_array($permission, ['管理者']))) {
          $add_class = '';
        }
      }
    @endphp
    <button type="button" id="save" class="btn-lg {{ $add_class }}">申請</button>
    @if($studentSoldText->id !== null)
      {{-- 申請取消（削除） --}}
      @php
        $add_class = 'is_disabled';
        if($studentSoldText->teacher_id == $teacher->id || in_array($permission, ['管理者'])) {
          if($studentSoldText->id !== null && $studentSoldText->syounin_teacher_id === null) {
            $add_class = '';
          }
        }
      @endphp
      <button type="button" id="torikeshi" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">申請取消</button>
      {{-- 承認 --}}
      @if(in_array($permission, ['校長', 'マネージャー', '管理者']))
        @if($studentSoldText->teacher_id != $teacher->id)
        <button type="button" id="syounin" class="btn-lg {{ ($studentSoldText->syounin_teacher_id !== null || $studentSoldText->id === null) ? 'is_disabled' : '' }}">承認</button>
        @endif
      @endif
      {{-- 印刷 --}}
      <button type="button" id="print" class="btn-lg btn-lg-slim {{ ($studentSoldText->syounin_teacher_id === null || $studentSoldText->id === null || $studentSoldText->syounin_torikeshi_shinsei_flag === 1) ? 'is_disabled' : '' }}">印刷</button>
      {{-- 承認取消申請 --}}
      @php
        $add_class = 'is_disabled';
        if((in_array($permission, ['講師']) && $studentSoldText->teacher_id == $teacher->id) || in_array($permission, ['管理者'])) {
          if($studentSoldText->syounin_teacher_id !== null && $studentSoldText->syounin_torikeshi_shinsei_flag !== 1) {
            $add_class = '';
          }
        }
      @endphp
      <button type="button" id="syounin_torikeshi_shinsei" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">承認取消申請</button>
      {{-- 承認取消 --}}
      @php
        $add_class = 'is_disabled';
        if(in_array($permission, ['校長', 'マネージャー', '管理者'])) {
          if($studentSoldText->teacher_id != $teacher->id) {
            if($studentSoldText->syounin_teacher_id !== null && $studentSoldText->syounin_torikeshi_shinsei_flag == "1") {
              $add_class = '';
            }
          }
      @endphp
      <button type="button" id="syounin_torikeshi" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">承認取消</button>
      @php
        }
      @endphp
    @endif
    <input type="hidden" id="id" name="id" value="{{ $studentSoldText->id }}">
  </div>
</div>
</form>
@endsection

@section('script')
  <script>
    $(function(){
      var fx = {
        'computeTotal': function() {
          let computation = new Promise(function(resolve, reject) {
            let prices = $('.price');
            let taxRate = {{ config('jjss.tax_rate') }};
            let total = 0;

            if (prices.length) {
              prices.each(function(key, item){
                let price = ($(item).attr('data-value') === void 0) ? $(item).val() : $(item).attr('data-value');

                if (price && isNaN(price) === false) {
                  price = parseFloat(price);
                  total += price;
                }

                if (prices.length === (key + 1)) {
                  let taxValue = Math.round(parseFloat(total * taxRate));
                  total = parseFloat(total + taxValue);
                  resolve(total);
                }
              });
            } else {
              resolve(total);
            }
          });

          computation.then(function(total){
            total = total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            $('#total').html(total);
          });
        },
      }

      $('#student_id').select2({ language: "ja"});

      $('#save').on('click', function() {
        $('#form').attr('action', '{{ url("/master/student_sold_text/save") }}');
        $('#form').submit();
      });

      $('#torikeshi').on('click', function() {
        swal({
          text: "申請を取消します。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/master/student_sold_text_delete") }}');
            $('#form').submit();
          }
        });
      });

      $('#syounin_torikeshi_shinsei').on('click', function() {
        swal({
          text: "承認取消申請を行います。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/master/student_sold_text_torikeshi_shinsei") }}');
            $('#form').submit();
          }
        });
      });

      $('#syounin_torikeshi').on('click', function() {
        swal({
          text: "承認取消を行います。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/master/student_sold_text_torikeshi") }}');
            $('#form').submit();
          }
        });
      });

      $('#cancel').on('click', function() {
        location.href = '{{ url("/master/student_sold_text") }}';
      });

      $('body').on('click', '#add_text_request_fields', function(e) {
        let lastChildNumber = parseInt($('#text_request_group').children().last().attr('data-set-number'));
        let nextNumber = (lastChildNumber + 1);
        let row = '<br class="set_number_' + nextNumber + '">' +
          '<div class="set_container" data-set-number="' + nextNumber + '">' +
          '<hr>' +
          '<span class="remove_text_request_field">X</span>' +
          '<div class="mgn-btm8">' +
            '<b>教材名</b>' +
            '<input type="text" name="text_request[' + nextNumber + '][text]" placeholder="教材名" autocomplete="off" value=""/> ' +
            '<select class="text_select">' +
            '@php
              $texts_optiton_html = '';
              foreach($texts as $text) {
                  $texts_optiton_html .= '<option value="' . $text['name'] .  '" data_price="' . (int)$text['price'] . '">' . $text['name'] . '</option>';
              }
              echo $texts_optiton_html;
            @endphp
            ' +
            '</select>' +
          '</div>' +
          '<div class="mgn-btm8">' +
            '<b>金額</b>' +
            '<input type="text" data-format="number" name="text_request[' + nextNumber + '][price]" class="price" placeholder="金額" autocomplete="off" value=""/> （半角数字のみ）' +
          '</div>' +
          '</div>';
        $('#text_request_group').append(row);
        $('[data-format="number"]').numberformat();
      });

      $('body').on('click', '.remove_text_request_field', function(e) {
        let parentDiv = $(this).closest('div');
        let setNumber = parentDiv.attr('data-set-number');
        $('br.set_number_' + setNumber).remove();
        parentDiv.remove();
        fx.computeTotal();
      });

      $('body').on('change', '.price', function(e) {
        fx.computeTotal();
      });

      fx.computeTotal();

      $(document).on('change', '.text_select', function() {
        $(this).prev().val($(this).val());
        $(this).parent().parent().find('.price').val($("option:selected", this).attr('data_price')).change();
        fx.computeTotal();
      });

      $('#print').on('click', function() {
        window.open('{{ url('/master/student_sold_text/print'). '/'. $studentSoldText->id }}', '_blank');
        return;
      });

      @if (session()->has('alert.success'))
        @if(session('alert.success') == "承認済み。" && isset($studentSoldText->id) === true)
          window.open('{{ url('/master/student_sold_text/print'). '/'. $studentSoldText->id }}', '_blank')
          location.href = '{{ url("/master/student_sold_text") }}';
        @endif
      @endif

    })

  </script>
  @if(in_array($permission, ['校長', 'マネージャー', '管理者']))
  <script>
    $(function(){
      $('#syounin').on('click', function() {
        $('#form').attr('action', '{{ url("/master/student_sold_text/syounin") }}');
        $('#form').submit();
      });
    })
  </script>
  @endif
@endsection
