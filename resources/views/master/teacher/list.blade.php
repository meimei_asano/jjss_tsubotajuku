@extends('layouts.default')
@section('content')
<style>
  .set_orderby {cursor:pointer;}
</style>
<section class="scroll_table-wrap wht_bloc">

  <h2 class="heading-1">先生一覧</h2>

  <div class="flex flex-j-between flex-a-ctr mgn-btm16">
    <form id="form" action="{{  url('master/teacher') }}" class="mgn-btm16" >
      <div class="form-group flex">
        <input type="text" id="search_name" name="search_name" placeholder="先生名で検索" class="input-sm" value="{{ ($search['name']) ?? '' }}">
        &nbsp;
        <select id="search_school_id" name="search_school_id" >
          <option value="">担当校舎</option>
          {!! \JJSS::options($schools, 'id', 'name', ($search['tantou_school_id']) ?? '') !!}
        </select>
        &nbsp;
        <button id="btn-search">検索<i class='bx bx-search'></i></button>
        <div class="loading" style="display: none;"><i class='bx bx-loader-alt' ></i></div>

        <input type="hidden" name="orderby_column_name" value="{{ ($orderby['column_name']) ?? '' }}">
        <input type="hidden" name="orderby_type" value="{{ ($orderby['type']) ?? '' }}">
      </div>
    </form>
    <div class="btn_list">
      <button id="create" onclick="location.href='{{ url('master/teacher'). '/new' }}';"><i class='bx bxs-user-plus'></i>追加</button>
    </div>
  </div>

  <div class="scroll_table mgn-btm16">
    <table class="table-horizon table-striped datatable">
      <thead>
        <tr>
          {{--
          <th class="th_check">
            <input type="checkbox" class="check" id="check_all" value="all">
            <label for="check_all"></label>
          </th>
          --}}
          <th class="set_orderby" dt-column="name">先生名</th>
          <th class="set_orderby" dt-column="kana">カナ</th>
          <th class="set_orderby" dt-column="nyusya_date">入社日</th>
          <th class="set_orderby" dt-column="tantou_school_id">担当校舎</th>
          {{--<th>指導可能科目</th>--}}
          <th class="set_orderby" dt-column="koyou_keitai">雇用形態</th>
          <th class="set_orderby" dt-column="permission">権限</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      @if(count($teachers) == 0)
        <tr>
          <td colspan="6">
            登録されていません
          </td>
        </tr>
      @else
        @foreach ($teachers as $teacher)
        <tr style="">
          <td class="">{{ $teacher->name }}</td>
          <td class="">{{ $teacher->kana }}</td>
          <td class="">{{ $teacher->nyusya_date }}</td>
          <td class="">{{ $teacher->school->name or null }}</td>
          <td class="">{{ $teacher->koyou_keitai }}</td>
          <td class="">{{ $teacher->permission }}</td>
          <td class="txt-rgt">
            <button class="btn-sm" onclick="location.href='{{ url('master/teacher'). '/'. $teacher->id }}';">変更</button>
          </td>
        </tr>
        @endforeach
      @endif
      </tbody>
    </table>
  </div>

  <div class="pager txt-ctr">
    {{ $teachers->appends(Request::only(['search_name', 'search_school_id', 'orderby_column_name', 'orderby_type']))->links() }}
  </div>
</section>
@endsection
@section('script')
<script>
$(function(){
  $('#search_school_id').select2({ language: "ja"});

  $('#btn-search').on('click', function() {
    $('#form').submit();
  });

  $('.set_orderby').on('click', function() {
    var current_orderby = $('input[name="orderby_type"]').val();
    var current_column = $('input[name="orderby_column_name"]').val();
    var new_column = $(this).attr('dt-column');

    if (current_column == new_column && current_orderby == "asc") {
      $('input[name="orderby_type"]').val('desc');
    } else {
      $('input[name="orderby_column_name"]').val(new_column);
      $('input[name="orderby_type"]').val('asc');
    }

    $('#form').submit();
  });
});
</script>
@endsection
