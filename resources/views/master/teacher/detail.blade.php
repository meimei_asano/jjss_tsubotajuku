@extends('layouts.default')
@section('content')
<form id="form" method="post" action="">
  {{ csrf_field() }}
  <section class="scroll_table-wrap wht_bloc">
  <h2 class="heading-1">先生情報</h2>
  <div class="">
    <table class="datatable table-vertical">
      <tr>
        <th>名前</th>
        <td class="form-group">
          <input type="text" id="name" name="name" class="" value="{{ old('name', $teacher->name) }}">
        </td>
      </tr>
      <tr>
        <th>カナ</th>
        <td class="form-group">
          <input type="text" id="kana" name="kana" class="" value="{{ old('kana', $teacher->kana) }}">
        </td>
      </tr>
      <tr>
        <th>E-mail</th>
        <td class="form-group">
          <input type="text" id="email" name="email" class="input-lg" value="{{ old('email', (isset($teacher->user->email) === true ? $teacher->user->email : '')) }}">
        </td>
      </tr>
      <tr>
        <th>パスワード</th>
        <td class="form-group">
          <input type="password" id="password" name="password" placeholder="（変更する場合のみ）" value="" {{ (strlen($teacher->id) == 0 || $user->teacher->permission == '管理者') ? '' : 'disabled="disabled"' }}>
          <input type="password" id="password_confirmation" name="password_confirmation" placeholder="（再入力）" value="" {{ (strlen($teacher->id) == 0 || $user->teacher->permission == '管理者') ? '' : 'disabled="disabled"' }}>
        </td>
      </tr>
      <tr>
        <th>入社日</th>
        <td class="form-group">
          <input type='text' id="nyusya_date" name="nyusya_date" class="datepicker-here" data-language="jp" data-auto-close="true" autocomplete="off" value="{{ old('nyusya_date', $teacher->nyusya_date) }}" />
        </td>
      </tr>
      <tr>
        <th>担当校舎</th>
        <td class="form-group">
          <select name="tantou_school_id">
            <option value="">---</option>
  @if(count($schools) > 0)
    @foreach($schools as $school)
            <option value="{{ $school->id }}" {{ $school->id == old('tantou_school_id', $teacher->tantou_school_id) ? 'selected="selected"' : '' }} >{{ $school->name }}</option>
    @endforeach
  @endif
          </select>
        </td>
      </tr>
      <tr>
        <th>指導可能科目</th>
        <td class="form-group">
          <ul class="flex flex-col4">
  @php
    $teacher_kamokus = old('teacher_kamoku', $teacher->teacher_kamokus->toArray());
  @endphp
  @if(count($kamokus) > 0)
    @foreach($kamokus as $kamoku)
            <li>
              <input type="checkbox" name="teacher_kamoku[]" id="teacher_kamoku{{ $kamoku->id }}"
                     value="{{ $kamoku->id }}" {{ in_array($kamoku->id, $teacher_kamokus) === true ? 'checked="checked"' : '' }} >
              <label for="teacher_kamoku{{ $kamoku->id }}">{{ $kamoku->name }}</label>
            </li>
    @endforeach
  @endif
          </ul>
        </td>
      </tr>
      <tr>
        <th>雇用形態</th>
        <td class="form-group">
          <select id="koyou_keitai" name="koyou_keitai">
            <option value="">選択してください</option>
            @foreach (config('jjss.koyou_keitai') as $kk)
              <option value="{{ $kk }}"{{ (old('koyou_keitai', $teacher->koyou_keitai) == $kk) ? ' selected' : '' }}>{{ $kk }}</option>
            @endforeach
          </select>
        </td>
      </tr>
      <tr>
        <th>権限</th>
        <td class="form-group">
          <select id="permission" name="permission">
            <option value="">選択してください</option>
            @foreach (config('jjss.permission') as $p)
              <option value="{{ $p }}"{{ (old('permission', $teacher->permission) == $p) ? ' selected' : '' }}>{{ $p }}</option>
            @endforeach
          </select>
        </td>
      </tr>
    </table>
  </div>
  <div class="datatable btn_list">
    <button type="button" id="cancel" class="btn-info btn-line btn-lg">キャンセル</button>
    <button type="button" id="save" class="btn-lg">保存</button>
    <input type="hidden" id="teacher_id" name="teacher_id" value="{{ $teacher->id }}">
  </div>
</div>
</form>
@endsection

@section('script')
  <script>
    $(function(){
      $('#save').on('click', function() {
        $('#form').attr('action', '{{ url('/master/teacher') }}');
        $('#form').submit();
      });

      $('#cancel').on('click', function() {
        location.href = '{{ url('/master/teacher') }}';
      })
    })
  </script>
@endsection
