@section('addrow')
<div id="background-addrow" style="display: none;">
    <div id="dialog-addrow">
        <div class="flex flex-j-between flex-a-ctr">
            <div></div>
            <div>
                <span><i id="addrow_close" class="bx bx-x"></i></span>
            </div>
        </div>
        <div id="addrow_panel">
            <div>
                <label id="addrow_text"><input type="radio" name="addrow_radio" value="text">テキスト追加</label>
           </div>
            <div>
                <label id="addrow_need"><input type="radio" name="addrow_radio" value="need">臨時追加</label>
            </div>
            <div>
                <label id="addrow_kakomon"><input type="radio" name="addrow_radio" value="kakomon">過去問追加</label>
            </div>
            <div id="addrow_text_div" style="display: none;">
                <div class="form-group">
                    科目：
                    <select id="addrow_text_kamoku" class="select-sm">
                        <option class="default" value="">選択してください</option>
                        @foreach ($kamokus as $kamoku)
                            <option value="{{ $kamoku->id }}">{{ $kamoku->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    テキスト：
                    <select id="addrow_text_text_id" class="select-sm">
                        <option class="default" value="">選択してください</option>
                    </select>
                </div>
                <div class="form-group">
                    本・逆・再：
                    <select id="addrow_text_reverse_test" class="select-sm" style="width:100px;min-width:100px !important;">
                        <option class="default" value="">選択してください</option>
                        <option value="0">本</option>
                        <option value="1">逆</option>
                        <option value="R_0">再(本)</option>
                        <option value="R_1">再(逆)</option>
                    </select>
                </div>
                <div class="form-group">
                    学習回：
                    <select id="addrow_text_text_unit_id" class="select-sm">
                        <option class="default" value="">選択してください</option>
                    </select>
                </div>
                <div class="form-group" id="addrow_div_retry_number">
                    再：
                    <input type="text" id="addrow_retry_number" style="width:100px;min-width:100px;height:30px;" placeholder="半角数値"> 回目
                </div>
            </div>
            <div id="addrow_need_div" style="display: none;">
                <div class="form-group">
                    追加テキスト名：
                    <input id="addrow_need_additional_text_name" type="text">
                </div>
                <div class="form-group">
                    回数：
                    <input id="addrow_need_additional_text_unit" type="text" placeholder="半角数値">
                </div>
            </div>
            <div id="addrow_kakomon_div" style="display: none;">
                <div class="form-group">
                    過去問名：
                    <input id="addrow_kakomon_additional_text_name" type="text">
                </div>
                <div class="form-group">
                    回数：
                    <input id="addrow_kakomon_additional_text_unit" type="text">
                </div>
            </div>
            <div id="addrow_submit_div" data-student_note_id="" style="display: none;">
                <button>登録</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script_addrow')
<script>
$(function() {
    $(document).on('click', '.note_addrow', function(){
        $('#background-addrow').fadeIn();
        $('#addrow_submit_div').data('student_note_id', $(this).data('student_note_id'));
    });
    $(document).on('click', '#addrow_close', function (){
        close_addrow_any_div();
        $('#background-addrow').hide();
        $('input[name=addrow_radio]').prop('checked', false);
    });
    $(document).on('click', '#addrow_text', function (){
        close_addrow_any_div();
        $('#addrow_text_div').show();
        $('#addrow_submit_div').show();
    });
    $(document).on('change', '#addrow_text_kamoku', function (){
        var kamoku_id = $(this).val();
        if (kamoku_id == "") {
            $('#addrow_text_reverse_test').val('0');
            $('#addrow_text_text_id').children('option:not(.default)').remove();
            $('#addrow_text_text_unit_id').children('option:not(.default)').remove();
            return false;
        }
        $.ajax({
            url: "/api/classroom/note/texts",
            type: "GET",
            data: {
                kamoku_id: kamoku_id,
            },
            dataType: 'json',
            beforeSend: function() {
                $('#addrow_text_text_id').children('option:not(.default)').remove();
                $('.loading').fadeIn();
            },
        })
        .done( (data, statusText, xhr) => {
            var texts = data.texts;
            for (var i in texts) {
                $('#addrow_text_text_id').append('<option value="' + texts[i]['id'] + '">' + texts[i]['name'] + '</option>');
            }
        })
        .fail( (data) => {
            var options = {
                text: "処理中にエラーが発生しました。",
                icon: "error",
                timer: 2000,
                buttons: false,
            };
            swal(options);
            self.show();
        })
        .always( (data) => {
            $('.loading').fadeOut();
        });
    });
    $(document).on('change', '#addrow_text_text_id', function (){
        var text_id = $(this).val();
        if (text_id == "") {
            $('#addrow_text_reverse_test').val('0');
            $('#addrow_text_text_unit_id').children('option:not(.default)').remove();
            return false;
        }
        $.ajax({
            url: "/api/classroom/note/text_units",
            type: "GET",
            data: {
                text_id: text_id,
            },
            dataType: 'json',
            beforeSend: function() {
                $('#addrow_text_reverse_test').val('0');
                $('#addrow_div_retry_number').hide();
                $('#addrow_retry_number').val('');
                $('#addrow_text_text_unit_id').children('option:not(.default)').remove();
                $('.loading').fadeIn();
            },
        })
        .done( (data, statusText, xhr) => {
            if (! data.units_by_student) {
                var text_units = data.text_units;
                $('#addrow_text_text_unit_id').append('<option value="0">初回指導</option>');
                for (var i in text_units) {
                    $('#addrow_text_text_unit_id').append('<option value="' + text_units[i]['id'] + '">' + text_units[i]['name'] + '</option>');
                }
            } else {
                if (data.is_english_words_tokkun) {
                    $('#addrow_text_text_unit_id').append('<option value="0">カウント</option>');
                } else {
                    $('#addrow_text_text_unit_id').append('<option value="0">初回指導</option>');
                }
                for (var i = 1; i <= 199; i++) {
                    $('#addrow_text_text_unit_id').append('<option value="' + i + '">' + i + '</option>');
                }
            }
        })
        .fail( (data) => {
            var options = {
                text: "処理中にエラーが発生しました。",
                icon: "error",
                timer: 2000,
                buttons: false,
            };
            swal(options);
            self.show();
        })
        .always( (data) => {
            $('.loading').fadeOut();
        });
    });
    $(document).on('change', '#addrow_text_reverse_test', function() {
        var reverse_test = $(this).val();
        if (reverse_test.startsWith('R')) {
            $('#addrow_div_retry_number').show();
        } else {
            $('#addrow_div_retry_number').hide();
        }
    });
    $(document).on('change', '#addrow_text_text_unit_id', function (){

    });
    $(document).on('click', '#addrow_need', function (){
        close_addrow_any_div();
        $('#addrow_need_div').show();
        $('#addrow_submit_div').show();
    });
    $(document).on('click', '#addrow_kakomon', function (){
        close_addrow_any_div();
        $('#addrow_kakomon_div').show();
        $('#addrow_submit_div').show();
    });

    $(document).on('click', '#addrow_submit_div', function (){
        var addrow_text_type = $('input[name=addrow_radio]:checked').val();
        switch (addrow_text_type) {
            case 'text':
                var text_id = $('#addrow_text_text_id').val();
                var additional_text_name = '';
                var reverse_test = $('#addrow_text_reverse_test').val();
                var text_unit_id = $('#addrow_text_text_unit_id').val();
                var display_text_unit = $('#addrow_text_text_unit_id option:selected').text();
                if (reverse_test.trim() == '' || text_unit_id.trim() == '') var has_error = true;

                // 「再」の場合
                if (reverse_test.startsWith('R')) {
                    var retry_number = $('#addrow_retry_number').val();
                    if (!retry_number.match(/^[0-9]+$/)) {
                        var has_error = true;
                    }
                    retry_number = (retry_number == '1') ? '' : retry_number;

                    reverse_test = reverse_test.slice(-1);  // 末尾の文字を取得
                    display_text_unit = display_text_unit + '再' + retry_number;  // X再、X再2 のようにセット
                }
                break;
            case 'need':
                var text_id = 0;
                var additional_text_name = $('#addrow_need_additional_text_name').val().trim();
                var reverse_test = 0;
                var text_unit_id = $('#addrow_need_additional_text_unit').val().trim();
                if(text_unit_id != '') {
                    if (!text_unit_id.match(/^[0-9]+$/)) {
                        var has_error = true;
                    }
                } else {
                    text_unit_id = 0;
                }
                var display_text_unit = $('#addrow_need_additional_text_unit').val().trim();

                if (additional_text_name.trim() == '') var has_error = true;
                break;
            case 'kakomon':
                var text_id = 0;
                var additional_text_name = $('#addrow_kakomon_additional_text_name').val().trim();
                var reverse_test = 0;
                var text_unit_id = 0;
                var display_text_unit = $('#addrow_kakomon_additional_text_unit').val().trim();
                if (additional_text_name.trim() == '' || display_text_unit.trim() == '') var has_error = true;
                break;
        }
        if (has_error) {
            var options = {
                text: "正しく入力してください。",
                icon: "error",
                timer: 2000,
                buttons: false,
            };
            swal(options);
            return false;
        }

        var student_note_id = $(this).data('student_note_id');
        $('#form').attr('action', '{{ url("classroom/note/addrow") }}' + '/' + student_note_id);
        $('#form').append( $('<input>').attr({type : 'hidden', name : 'text_id', value : text_id}));
        $('#form').append( $('<input>').attr({type : 'hidden', name : 'additional_text_name', value : additional_text_name}));
        $('#form').append( $('<input>').attr({type : 'hidden', name : 'reverse_test', value : reverse_test}));
        $('#form').append( $('<input>').attr({type : 'hidden', name : 'text_unit_id', value : text_unit_id}));
        $('#form').append( $('<input>').attr({type : 'hidden', name : 'display_text_unit', value : display_text_unit}));
        $('#form').submit();

        $('.loading').fadeIn();
    });

    function close_addrow_any_div() {
        $('#addrow_text_div').hide();
        $('#addrow_need_div').hide();
        $('#addrow_kakomon_div').hide();
        $('#addrow_submit_div').hide();
        $('#addrow_text_kamoku').val('');
        $('#addrow_text_text_id').children('option:not(.default)').remove();
        $('#addrow_text_reverse_test').val('0');
        $('#addrow_div_retry_number').hide();
        $('#addrow_text_text_unit_id').children('option:not(.default)').remove();
    }

    $('.delete_row').on('contextmenu', function() {

        var o = $(this);

        swal({
            text: "この行を削除します。よろしいですか？",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    var action_url = "{{ url("classroom/note/delete_row/") }}" + "/" + o.attr('row_id');
                    $('#form').attr('action', action_url);
                    $('#form').submit();
                }
            });
    });

    $('.remarks').on('blur', function() {

        var o = $(this);
        var note_id = $(this).attr('note_id');

        if(note_id == "") {
            return;
        }
        $.ajax({
            url: "/api/classroom/note/instruction/update_remarks",
            type: "PUT",
            data: {
                note_id : note_id,
                remarks : o.val()
            },
            dataType: 'json',
            beforeSend: function() {
                $('.loading').fadeIn();
            },
        })
            .done((data, statusText, xhr) => {
                //入力内容がそのまま登録された
            })
            .fail( (data) => {
                var options = {
                    text: "処理中にエラーが発生しました。",
                    icon: "error",
                    timer: 2000,
                    buttons: false,
                };
                swal(options);
            })
            .always( (data) => {
                $('.loading').fadeOut();
            });
    });
});
</script>
@endsection
