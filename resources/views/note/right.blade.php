<div>
    <table class="table-note table-note-right datatable">
        <thead>
            <tr>
                <td class="lg">{{ \JJSS::displayDate($student_note->note_date) }}</td>
                <th colspan="2">氏名</th>
                <td colspan="3" class="name">{{ $student_note->student->name }}</td>
                <th class="sm">学年</th>
                <td class="mn">{{ config('jjss.grade')[ $student_note->student->grade_code ] ?? '' }}</td>
            </tr>
        </thead>
        <tbody>
            {{-- 1 --}}
            <tr>
                <th colspan="8" style="text-align: left;">
                    １&nbsp;：&nbsp;今日の自分を評価してください。
                </th>
            </tr>
            <tr>
                <td colspan="8" style="text-align: left; line-height: 14pt;">
                    ・限界まで頑張った!!!!!（5点）
                    ・超頑張った!!!!（4点)
                    ・自分なりに頑張った!!!（3点）
                    <br>
                    ・前回よりは頑張った!!（2点）&nbsp;
                    ・次は頑張ろう!（1点）
                </td>
            </tr>
            {{-- 2 --}}
            <tr>
                <th colspan="8" style="text-align: left;">
                    ２&nbsp;：&nbsp;今日の自分が頑張ったことを具体的に教えて下さい。
                </th>
            </tr>
            <tr>
                <td colspan="8" style="height:5em;">
                </td>
            </tr>
            {{-- 3 --}}
            <tr>
                <th colspan="8" style="text-align: left;">
                    ３&nbsp;：&nbsp;次回の自分はどうありたいですか？
                </th>
            </tr>
            <tr>
                <td colspan="8" style="text-align: left; line-height: 14pt;">
                    ・大きな声で挨拶する！　　　
                    ・予習は全部終わらせる！　　　　
                    ・指導で先生を驚かす！
                    <br>
                    ・時間を有効に使う！　　　　
                    ・その他（　　　　　　　　　　　　　　　　　　　　　　）
                </td>
            </tr>
            {{-- 4 --}}
            <tr>
                <th colspan="8" style="text-align: left;">
                    ４&nbsp;：&nbsp;今日の指導で一番良かった先生は誰ですか？また、その先生の指導は何点でしたか？
                </th>
            </tr>
            <tr>
                <td colspan="8" style="text-align: left;">
                    　　　　　　　　［　　　　　　　　　　　　］先生　　　　　　　　点
                </td>
            </tr>
            {{-- 5 --}}
            <tr>
                <th colspan="8" style="text-align: left;">
                    ５&nbsp;：&nbsp;その先生の指導で良かった所はどこですか？
                </th>
            </tr>
            <tr>
                <td colspan="8" style="text-align: left; line-height: 14pt;">
                    ・挨拶が素敵だった　　　
                    ・自分がわかってないところと、わかってるところが明確になった
                    <br>
                    ・指導時間が適切だった　
                    ・ちょっと先生に憧れた
                    <br>
                    ・指導がとても熱かった！
                    ・なるほどと思った
                    <br>
                    ・その他（　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　）
                </td>
            </tr>
            {{-- 6 --}}
            <tr>
                <th colspan="8" style="text-align: left;">
                    ６&nbsp;：&nbsp;（先生の点数が100点満点でない場合）マイナスだった点はどこですか？
                </th>
            </tr>
            <tr>
                <td colspan="8" style="height:4em;">
                </td>
            </tr>
            {{-- 7 --}}
            <tr>
                <th colspan="8" style="text-align: left;">
                    ７&nbsp;：&nbsp;最後に、自由な一言をどうぞ！
                </th>
            </tr>
            <tr>
                <td colspan="8" style="height:5em;">
                </td>
            </tr>
        </tbody>
    </table>
</div>
