<link href="{{ url('css/note.css') }}" rel="stylesheet">

<div>
    @if (isset($note_name))
    <h2 class="heading-1" style="margin-bottom: 0;">{{ $note_name }}子別ノート</h2>
    @endif
    <table class="table-note datatable">
        <thead>
        <tr>
            <td class="lg">{{ \JJSS::displayDate($student_note->note_date) }}</td>
            <th colspan="2">氏名</th>
            <td colspan="4" class="name" style="position:relative;">
                {{ $student_note->student->name }}
                @if($is_using === true)
                    <i class="bx bx-message add_memo" data-kamoku_id="" style="font-size:65%;"></i>
                @endif
            </td>
            <th class="sm">学年</th>
            <td class="mn">{{ config('jjss.grade')[ $student_note->student->grade_code ] ?? '' }}</td>
        </tr>
        <tr>
            <td rowspan="2">{{ \JJSS::displayWeek($student_note->note_date) }}</td>
            <th rowspan="2" class="mn">時間</th>
            <th class="sm">予定</th>
            <td colspan="4">
                {{ \JJSS::displayTime($student_note->plan_attend_start_time) }}
                〜
                {{ \JJSS::displayTime($student_note->plan_attend_end_time) }}
            </td>
            <td rowspan="2" colspan="2" style="font-size:150%;">
                {{ ($student_note->student->ninetype) ? \JJSS::displayNineType($student_note->student->ninetype->ninetype_codes, 'code_only') : '' }}
            </td>
        </tr>
        <tr>
            <th class="sm">実際</th>
            <td colspan="4">
                {{ \JJSS::displayTime($student_note->actual_attend_start_time) }}
                〜
                {{ \JJSS::displayTime($student_note->actual_attend_end_time) }}
            </td>
        </tr>
        </thead>
        <tbody>
        <tr class="header">
            <th colspan="3">教材名</th>
            <th class="mn">回数</th>
            {!! isset($is_print)===true ? '<th class="mn">点数</th>' : '' !!}
            <th class="mn">合否</th>
            <th class="vlg" colspan="2">指導時間</th>
            <th colspan="{{ isset($is_print)===true ? "1" : "2" }}">講師名</th>
        </tr>
        @php
            $row_count = count($student_note->student_note_rows);
            if($row_count < 12) {
                $row_all = $row_count + (12 - $row_count%12);
            } else {
                $row_all = $row_count + 1;
            }
            $idx = ($row_index) ?? 0;
            if(isset($is_print) === true) {
                $row_all = $idx + 12;
            }
        @endphp
        @for ($i = $idx; $i < $row_all; $i++)
        {{-- @for ($i = $idx; $i < ($idx + 12); $i++) --}}
            @if (isset($student_note->student_note_rows[$i]))
                @php
                    $row = $student_note->student_note_rows[$i];
                @endphp
                <tr>
                    <td colspan="3" class="text{{ ($is_old && ! ($is_last ?? false)) ? '' : ' delete_row' }}" row_id="{{ $row->id }}" style="position:relative;">
                        {{ ($row->text_id) ? $row->text->name : $row->additional_text_name }}
                        {{ ($row->reverse_test) ? '（逆）' : '' }}
                        @if($is_using === true)
                            <i class="bx bx-message add_memo" data-kamoku_id="{{ $row->text->kamoku_id or null }}"></i>
                        @endif
                    </td>
                    <td>
                        {{ $row->display_text_unit }}
                    </td>
                    {!! isset($is_print)===true ? '<td></td>' : '' !!}
                    <td class="result">
                        {!! (strlen($row->teacher_id) == 0) ? '' : ['<span class="red">否</span>', '合', 'S'][$row->result_flag] !!}
                    </td>
                    <td class="inst_time" colspan="2">
                        @if ($is_old)
                            <span class="start_time">{{ \JJSS::displayTime($row->actual_start_time) ?: '　　　' }}</span>
                            〜
                            <span class="end_time">{{ \JJSS::displayTime($row->actual_end_time) ?: '　　　' }}</span>
                        @else
                            @if (! $student_note->checked_flag)
                                &nbsp;
                            @elseif (! $row->actual_start_time && \Carbon\Carbon::parse($target_date)->isToday())
                                @if(strlen($student_note->actual_attend_start_time) == 0){{-- 入室時刻が未登録 --}}
                                    <button class="timer_start is_disabled" data-row_id="{{ $row->id }}" disabled="disabled"><i class="bx bx-timer"></i>指導開始</button>
                                @else
                                    <button class="timer_start" data-row_id="{{ $row->id }}" ><i class="bx bx-timer"></i>指導開始</button>
                                @endif
                            @elseif (! $row->actual_end_time && \Carbon\Carbon::parse($target_date)->isToday())
                                <span class="start_time">{{ \JJSS::displayTime($row->actual_start_time) }}</span>
                                〜
                                <span class="end_time"><button class="timer_end" data-row_id="{{ $row->id }}"><i class="bx bx-timer"></i>終了</button></span>
                            @else
                                <span class="start_time">{{ \JJSS::displayTime($row->actual_start_time) ?: '　　　' }}</span>
                                〜
                                <span class="end_time">{{ \JJSS::displayTime($row->actual_end_time) ?: '　　　' }}</span>
                            @endif
                        @endif
                        <input type="hidden" class="note_row_id" value="{{ $row->id }}">
                    </td>
                    <td colspan="{{ isset($is_print)===true ? "1" : "2" }}" class="teacher">{{ ($row->teacher->name) ?? '' }}</td>
                </tr>
            @else
                <tr>
                    <td colspan="3" class="text">
                        @if ((($is_show_create_button) ?? false) && (! $is_old))
                            <button id="create" class="note_create"><i class='bx bx-edit'></i>子別ノートを生成</button>
                            @php ($is_show_create_button = false)
                        @endif
                        @if ((($is_row_add_button) ?? false) && (! $is_old || ($is_last ?? false)))
                            <button class="note_addrow" data-student_note_id="{{ $student_note->id }}"><i class='bx bx-plus-circle'></i>テキストを追加</button>
                            @php ($is_row_add_button = false)
                        @endif
                    </td>
                    <td></td>
                    {!! isset($is_print)===true ? '<td></td>' : '' !!}
                    <td></td>
                    <td class="inst_time" colspan="2"></td>
                    <td colspan="{{ isset($is_print)===true ? "1" : "2" }}"></td>
                </tr>
            @endif
        @endfor
        <tr>
            @if(isset($is_print) === true)
                <td colspan="9" class="remarks" style="text-align:left; vertical-align: top;">
                    <p><b>特記事項</b></p>
                    {{ $student_note->remarks }}
                </td>
            @else
                <td colspan="9" class="remarks">
                    <textarea class="remarks" note_id="{{ $student_note->id }}" placeholder="備考" {{ ($is_old) ? 'disabled' : ''  }}>{{ $student_note->remarks }}</textarea>
                </td>
            @endif
        </tr>
        </tbody>
    </table>
</div>
