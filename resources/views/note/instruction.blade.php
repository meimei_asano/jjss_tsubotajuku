<script>
$(function() {
    $(document).on('click', '.timer_start', function(){
        var self = $(this);
        instruction('start', self, null);
    });

    $(document).on('click', '.timer_end', function(){
        var result_flag;
        swal("合否を選択してください", {
            dangerMode: true,
            buttons: {
                cancel: "キャンセル",
                pass: {
                    text: "合",
                },
                fail: {
                    text: '否',
                },
                instruction: {
                    text: '指導のみ',
                }
            },
        })
        .then((value) => {
            switch (value) {
                case "pass":
                    result_flag = 1;
                    break;
                case "fail":
                    result_flag = 0;
                    break;
                case "instruction":
                    result_flag = 2;
                    break;
                default:
                    var options = {
                        text: "キャンセルしました。",
                        icon: "info",
                        timer: 1000,
                        buttons: false,
                    };
                    swal(options);
                    return false;
            }
            var self = $(this);
            instruction('end', self, result_flag);
        });
    });

    function instruction(start_end, self, result_flag) {
        var student_note_row_id = self.data('row_id');
        $.ajax({
            url: "/api/classroom/note/instruction/" + start_end + "/" + student_note_row_id,
            type: "PUT",
            data: {
                result_flag: result_flag,
                teacher_id: {{ \Auth::user()->teacher->id }},
            },
            dataType: 'json',
            beforeSend: function() {
                $('.loading').fadeIn();
                if(start_end !== "re_end") {
                    self.fadeOut();
                }
            },
        })
        .done( (data, statusText, xhr) => {
            if (start_end == 'start') {
                var col = '<span class="start_time">' + data.start_time + '</span> 〜 '
                    + '<span class="end_time"><button class="timer_end" data-row_id="' + student_note_row_id + '"}><i class="bx bx-timer"></i>終了</button></span>';
                self.after(col);
                self.remove();
            } else if (start_end == 'end') {
                var col = '<span class="end_time">' + data.end_time + '</span>';
                // 合否
                self.closest('tr').find('td.result').html(data.result);
                // 先生
                self.closest('tr').find('td.teacher').text(data.teacher);
                self.after(col);
                self.remove();
            } else if(start_end == 're_end') {
                // 合否
                self.closest('tr').find('td.result').html(data.result);
                // 先生
                self.closest('tr').find('td.teacher').text(data.teacher);
            }
        })
        .fail( (data) => {
            var options = {
                text: "処理中にエラーが発生しました。",
                icon: "error",
                timer: 2000,
                buttons: false,
            };
            swal(options);
            self.show();
        })
        .always( (data) => {
            $('.loading').fadeOut();
        });
    }

    $(document).on('contextmenu', '.result', function() {
        var o = $(this);
        var note_row_id = o.parent().find('.note_row_id').val();
        o.attr('data-row_id', note_row_id);

        var result_flag;
        swal("修正する結果を選択してください", {
            dangerMode: true,
            buttons: {
                cancel: "キャンセル",
                pass: {
                    text: "合",
                },
                fail: {
                    text: '否',
                },
                instruction: {
                    text: '指導のみ',
                }
            },
        })
        .then((value) => {
            switch (value) {
                case "pass":
                    result_flag = 1;
                    break;
                case "fail":
                    result_flag = 0;
                    break;
                case "instruction":
                    result_flag = 2;
                    break;
                default:
                    var options = {
                        text: "キャンセルしました。",
                        icon: "info",
                        timer: 1000,
                        buttons: false,
                    };
                    swal(options);
                    return false;
            }
            instruction('re_end', o, result_flag);
        });

    });

    $(document).on('contextmenu', '.start_time, .end_time', function() {
        var o = $(this);
        var up_koumoku = '';
        if(o.hasClass('start_time')) {
            up_koumoku = 'actual_start_time';
        } else if(o.hasClass('end_time')) {
            up_koumoku = 'actual_end_time';
        }

        if(o.find('button').length != 0) {
            return;
        }

        var c = document.createElement("input");
        c.type = "text";
        c.style.cssText = 'width : 100px;' + 'height: 40px;' + 'font-size: 200%;';
        c.setAttribute('class', 'value_set_time');

        swal({
            title : '時間変更',
            text : '時間(00:00の形式)を入力してOKボタンを押下ください。',
            content: c,
        })
        .then((value) => {
            if($('.value_set_time').val() == "") {
                return;
            }
            instruction_change(up_koumoku, o, $('.value_set_time').val());
        });

    });

    function instruction_change(koumoku, self, time_value)
    {
        var note_row_id = self.parent().find('.note_row_id').val();
        $.ajax({
            url: "/api/classroom/note/instruction/update_row_something/" + note_row_id,
            type: "PUT",
            data: {
                'koumoku' : koumoku,
                'up_value' : time_value,
            },
            dataType: 'json',
            beforeSend: function() {
                $('.loading').fadeIn();
                self.fadeOut();
            },
        })
        .done( (data, statusText, xhr) => {
            self.html(data[koumoku]);
            self.show();
        })
        .fail( (data) => {
            var options = {
                text: "処理中にエラーが発生しました。",
                icon: "error",
                timer: 2000,
                buttons: false,
            };
            swal(options);
        })
        .always( (data) => {
            $('.loading').fadeOut();
        });
    }

});
</script>
