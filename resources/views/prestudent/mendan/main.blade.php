<style>
table {
    border-collapse: collapse;
    border-spacing: 0;
    table-layout: fixed;
    font-size: 12pt;
}
th, td {
    padding: 0;
}
table.main {
    width: 800px;
}
table.main table {
    width: 100%;
}
.bordered {
    border: 1px solid black;
}
.bordered-2 {
    border: 2px solid black;
}
.bordered-3 {
    border: 3px solid black;
}
.bordered-4 {
    border: 4px solid black;
}
.layer-2-header {
    border-top: 0;
    border-bottom: 0;
    background-color: #d8d8d8;
}
.height-mn {
    height: 1.6em;
    padding: 0 5px;
}
.text-center {
    text-align: center;
}
.text-right {
    text-align: right;
}
.font-size-4pt {
    font-size: 4pt;
}
.font-size-7pt {
    font-size: 7pt;
}
.font-size-8pt {
    font-size: 8.2pt;
}
.font-size-9pt {
    font-size: 9pt;
}
.font-size-10pt {
    font-size: 10pt;
}
.font-size-11pt {
    font-size: 11pt;
}
.font-size-12pt {
    font-size: 12pt;
}
.font-size-13pt {
    font-size: 13pt;
}
.font-size-14pt {
    font-size: 14pt;
}
.no-border-top {
    border-top: 0;
}
.no-border-bottom {
    border-bottom: 0;
}
.no-border-right {
    border-right: 0;
}
.no-border-left {
    border-left: 0;
}
.dotted-border-top {
    border-top-style: dotted;
}
.dotted-border-bottom {
    border-bottom-style: dotted;
}
.bcolor-d8 {
    background-color: #d8d8d8;
}
</style>
<table class="main">
    <tr><!-- LAYER 1 -->
        <td>
            <table>
                <tr>
                    <td colspan="6" class="bordered text-center">坪田塾 {{ $mendanKiroku->school->name }}</td>
                    <td colspan="23">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="5">&nbsp;</td>
                    <td colspan="20" class="text-center">
                        @if($type === 'unlimited')
                            面談記録用紙兼請求書（無制限コース）
                        @else
                            面談記録用紙兼請求書（通常コース）
                        @endif
                    </td>
                    <td colspan="4" rowspan="2" class="text-center">{{ $intendedFor }}</td>
                </tr>
                <tr>
                    <td colspan="25" class="font-size-4pt">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="15">&nbsp;</td>
                    <td colspan="6" class="bordered text-center" style="width: 165.5px">面談日</td>
                    <td colspan="4" class="bordered text-center" style="width: 110px">面談者</td>
                    <td colspan="4" class="bordered text-center" style="width: 110px">校長</td><!-- 82.75px -->
                </tr>
                <tr>
                    <td colspan="15">&nbsp;</td>
                    <td colspan="6" class="bordered text-center">{{ \JJSS::printDate($mendanKiroku->mendan_date) }}</td>
                    <td colspan="4" class="bordered text-center">{{ $mendanKiroku->mendan_teacher->name }}</td>
                    <td colspan="4" class="bordered text-center">{{ $mendanKiroku->manager_teacher->name }}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><!-- SPACE -->
        <td class="font-size-7pt">&nbsp;</td>
    </tr>
    <tr><!-- LAYER 2 -->
        <td class="bordered-4">
            <table>
                <tr>
                    <td colspan="14" class="bordered layer-2-header no-border-left text-center" style="width: 386px">
                        生徒欄
                    </td>
                    <td style="width: 28px">&nbsp;</td>
                    <td colspan="14" class="bordered layer-2-header no-border-right text-center" style="width: 386px">
                        保護者欄
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="bordered dotted-border-bottom no-border-left text-center font-size-7pt">
                        フリガナ
                    </td>
                    <td colspan="11" class="bordered dotted-border-bottom font-size-7pt">&nbsp;&nbsp;{{ $mendanKiroku->kana }}</td>
                    <td>&nbsp;</td>
                    <td colspan="3" class="bordered dotted-border-bottom text-center font-size-7pt">
                        フリガナ
                    </td>
                    <td colspan="7" class="bordered dotted-border-bottom font-size-7pt">&nbsp;&nbsp;{{ $mendanKiroku->hogosya_kana }}</td>
                    <td colspan="2" rowspan="2" class="bordered text-center">
                        続柄
                    </td>
                    <td colspan="2" rowspan="2" class="bordered no-border-right text-center">{{ $mendanKiroku->hogosya_tsudukigara }}</td>
                </tr>
                <tr>
                    <td colspan="3" class="bordered no-border-top no-border-left text-center">
                        氏名
                    </td>
                    <td colspan="11" class="bordered no-border-top">&nbsp;{{ $mendanKiroku->name }}</td>
                    <td>&nbsp;</td>
                    <td colspan="3" class="bordered no-border-top text-center">
                        氏名
                    </td>
                    <td colspan="7" class="bordered no-border-top">&nbsp;{{ $mendanKiroku->hogosya_name }}</td>
                    <!-- rowspan 2 above -->
                </tr>
                <tr>
                    <td colspan="3" class="bordered text-center no-border-left">
                        生年月日
                    </td>
                    <td colspan="7" class="bordered">&nbsp;{{ \JJSS::printDate($mendanKiroku->birth_date) }}</td>
                    <td colspan="2" class="bordered text-center">
                        性別
                    </td>
                    <td colspan="2" class="bordered text-center">{{ $mendanKiroku->gender }}</td>
                    <td>&nbsp;</td>
                    <td colspan="3" class="bordered text-center">
                        自宅Tel
                    </td>
                    <td colspan="11" class="bordered no-border-right">&nbsp;{{ $mendanKiroku->hogosya_home_tel }}</td>
                </tr>
                <tr>
                    <td colspan="3" class="bordered text-center no-border-left">
                        学校名
                    </td>
                    <td colspan="7" class="bordered">&nbsp;{{ $mendanKiroku->gakkou_name }}</td>
                    <td colspan="2" class="bordered text-center">
                        学年
                    </td>
                    <td colspan="2" class="bordered text-center">{{ config('jjss.grade')[$mendanKiroku->grade_code] }}</td>
                    <td>&nbsp;</td>
                    <td colspan="3" class="bordered text-center">
                        自宅Fax
                    </td>
                    <td colspan="11" class="bordered no-border-right">&nbsp;{{ $mendanKiroku->hogosya_fax }}</td>
                </tr>
                <tr>
                    <td colspan="3" rowspan="2" class="bordered text-center dotted-border-top no-border-left">
                        住所
                    </td>
                    <td colspan="11" class="bordered dotted-border-top font-size-7pt no-border-bottom">&nbsp;〒{{ $mendanKiroku->zip_code }}</td>
                    <td>&nbsp;</td>
                    <td colspan="3" class="bordered text-center">
                        携帯Tel
                    </td>
                    <td colspan="11" class="bordered no-border-right">&nbsp;{{ $mendanKiroku->hogosya_tel }}</td>
                </tr>
                <tr>
                    <!-- rowspan 2 above -->
                    <td colspan="11" class="bordered no-border-top">&nbsp;{{ $mendanKiroku->pref }} {{ $mendanKiroku->address1 }} {{ $mendanKiroku->address2 }}</td>
                    <td>&nbsp;</td>
                    <td colspan="3" class="bordered text-center">
                        メール
                    </td>
                    <td colspan="11" class="bordered no-border-right">&nbsp;{{ $mendanKiroku->hogosya_email }}</td>
                </tr>
                <tr>
                    <td colspan="3" class="bordered text-center no-border-left">
                        携帯Tel
                    </td>
                    <td colspan="11" class="bordered">&nbsp;{{ $mendanKiroku->tel }}</td>
                    <td>&nbsp;</td>
                    <td colspan="14">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" class="bordered no-border-bottom text-center no-border-left">
                        メール
                    </td>
                    <td colspan="11" class="bordered no-border-bottom">&nbsp;{{ $mendanKiroku->email }}</td>
                    <td>&nbsp;</td>
                    <td colspan="14">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><!-- SPACE -->
        <td>&nbsp;</td>
    </tr>
    <tr><!-- LAYER 3 -->
        <td>
            <table>
                <tr>
                    <td colspan="14" style="width: 386px; vertical-align: top;">
                        @php
                            $orientationDateTime = \JJSS::displayDateTime($mendanKiroku->orientation_datetime);
                        @endphp
                        <table>
                            <tr><td colspan="14">◆面談後スケジュール</td></tr>
                            <tr>
                                <td colspan="6" class="bordered text-center" style="width: 165.5px">初回OT実施日時</td>
                                <td colspan="8" class="bordered text-center">&nbsp;{{ $orientationDateTime ? $orientationDateTime->format('Y年n月j日 H:i') : '' }}</td>
                            </tr>
                            <tr><td colspan="14" class="font-size-14pt">&nbsp;</td></tr>
                            <tr>
                                <td colspan="6" class="bordered text-center">初回授業日</td>
                                <td colspan="8" class="bordered text-center">&nbsp;{{ \JJSS::printDate($mendanKiroku->first_shidou_date) }}</td>
                            </tr>
                            <tr>
                                <td colspan="6" class="bordered text-center">学習科目</td>
                                <td colspan="8" class="bordered text-center">{{ \JJSS::displayKamokusByName($mendanKiroku->kamoku_ids) }}</td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 28px">&nbsp;</td>
                    <td colspan="14" style="width: 386px; vertical-align: top;">
                        <!-- Scheduled Course OR Unlimited -->
                        @if($type === 'unlimited')
                            @include('prestudent.mendan.unlimited')
                        @else
                            @include('prestudent.mendan.course')
                        @endif
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><!-- SPACE -->
        <td>&nbsp;</td>
    </tr>
    @php
        $seikyus = json_decode($mendanKiroku->seikyu_data);
        $totals = json_decode($mendanKiroku->seikyu_total);
    @endphp
    <tr><!-- LAYER 4 -->
        <td>
            <table>
                <tr>
                    <td colspan="8" rowspan="2" class="bordered-4 text-center font-size-14pt bcolor-d8" style="width: 221px">
                        初回請求金額合計
                    </td>
                    <td colspan="5" rowspan="2" class="bordered-4 text-center font-size-14pt" style="width: 138px">
                        {{ number_format($totals->total) }}円
                    </td>
                    <td colspan="16" style="width: 441px">&nbsp;</td>
                </tr>
                <tr>
                    <!-- rowspan 2 above -->
                    <td colspan="16">&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><!-- SPACE -->
        <td class="font-size-4pt">&nbsp;</td>
    </tr>
    <tr><!-- LAYER 5 -->
        <td class="font-size-11pt">
            ［振込口座］ {{ $mendanKiroku->school->furikomisaki }}
        </td>
    </tr>
    <tr><!-- SPACE -->
        <td>&nbsp;</td>
    </tr>
    <tr><!-- LAYER 6 -->
        <td>
            <table>
                <tr>
                    <td colspan="25" class="bordered text-center height-mn">内訳</td>
                    <td colspan="4" class="bordered text-center height-mn" style="width: 120px;">金額</td>
                </tr>
                @foreach($seikyus as $seikyu)
                <tr>
                    <td colspan="25" class="bordered height-mn">{{ $seikyu->title ?? '' }}</td>
                    <td colspan="4" class="bordered text-right height-mn">{{ ctype_digit($seikyu->price ?? '') ? number_format($seikyu->price).'円' : '' }}</td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="21">&nbsp;</td>
                    <td colspan="4" class="bordered text-center height-mn bcolor-d8">小計</td>
                    <td colspan="4" class="bordered text-right height-mn">{{ number_format($totals->subtotal) }}円</td>
                </tr>
                <tr>
                    <td colspan="21">&nbsp;</td>
                    <td colspan="4" class="bordered text-center height-mn bcolor-d8">消費税</td>
                    <td colspan="4" class="bordered text-right height-mn">{{ number_format($totals->tax) }}円</td>
                </tr>
                <tr>
                    <td colspan="21">&nbsp;</td>
                    <td colspan="4" class="bordered text-center height-mn bcolor-d8">合計</td>
                    <td colspan="4" class="bordered text-right height-mn">{{ number_format($totals->total) }}円</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr><!-- LAYER 7 -->
        <td class="font-size-11pt">◆初回納入金振込期限：初回授業日（{{ \JJSS::printDateShort($mendanKiroku->first_shidou_date) }}まで）</td>
    </tr>
    <tr>
        <td class="font-size-8pt">※初回授業日までに初回納入金をお振込み頂けない場合は、初回授業の受講を延期して頂く場合がございますので、ご注意頂けます様お願い申し上げます。</td>
    </tr>
    @if($intendedFor !== '校舎保管用')
        <tr><!-- SPACE -->
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="font-size-11pt">上記の内容及び契約書の内容について了解した上で入会を申し込みます。</td>
        </tr>
        <tr><!-- SPACE -->
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td colspan="13" style="width: 358px;">&nbsp;</td>
                        <td colspan="6" class="font-size-11pt">　　　　　　年　　　月　　　日</td>
                        <td colspan="10">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="29" style="height:32px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="13" style="width: 358px;">&nbsp;</td>
                        <td colspan="6" class="font-size-11pt">氏名</td>
                        <td colspan="10">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
    @endif
</table>
