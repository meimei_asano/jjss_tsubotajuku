<table>
    <tr><td colspan="14">◆通塾コース({{ \JJSS::printTimesByCourse($mendanKiroku->course->name) }}) ［{{ $mendanKiroku->course->name }}］</td></tr>
    <tr>
        <td colspan="3" class="bordered text-center">月</td>
        <td colspan="11" class="bordered text-center">{{ \JJSS::displayTime($mendanKiroku->mon_start_time) }} 〜 {{ \JJSS::displayTime($mendanKiroku->mon_end_time) }}</td>
    </tr>
    <tr>
        <td colspan="3" class="bordered text-center">火</td>
        <td colspan="11" class="bordered text-center">{{ \JJSS::displayTime($mendanKiroku->tue_start_time) }} 〜 {{ \JJSS::displayTime($mendanKiroku->tue_end_time) }}</td>
    </tr>
    <tr>
        <td colspan="3" class="bordered text-center">水</td>
        <td colspan="11" class="bordered text-center">{{ \JJSS::displayTime($mendanKiroku->wed_start_time) }} 〜 {{ \JJSS::displayTime($mendanKiroku->wed_end_time) }}</td>
    </tr>
    <tr>
        <td colspan="3" class="bordered text-center">木</td>
        <td colspan="11" class="bordered text-center">{{ \JJSS::displayTime($mendanKiroku->thu_start_time) }} 〜 {{ \JJSS::displayTime($mendanKiroku->thu_end_time) }}</td>
    </tr>
    <tr>
        <td colspan="3" class="bordered text-center">金</td>
        <td colspan="11" class="bordered text-center">{{ \JJSS::displayTime($mendanKiroku->fri_start_time) }} 〜 {{ \JJSS::displayTime($mendanKiroku->fri_end_time) }}</td>
    </tr>
</table>
