@php
    $unlimitedStartDate = \JJSS::printDate($mendanKiroku->first_shidou_date);
    $unlimitedEndDate = \JJSS::printDate($mendanKiroku->unlimited_end_date);
    $unlimitedDiscount = $mendanKiroku->unlimited_discount_flag == 1 ? 'あり' : 'なし';
@endphp
<table>
    <tr><td colspan="14">◆無制限コース情報</td></tr>
    <tr>
        <td colspan="3" class="bordered text-center">開始日</td>
        <td colspan="11" class="bordered text-center">{{ $unlimitedStartDate }}</td>
    </tr>
    <tr>
        <td colspan="3" class="bordered text-center">終了日</td>
        <td colspan="11" class="bordered text-center">{{ $unlimitedEndDate }}</td>
    </tr>
    <tr>
        <td colspan="3" class="bordered text-center">割引</td>
        <td colspan="11" class="bordered text-center">{{ $unlimitedDiscount }}</td>
    </tr>
</table>
