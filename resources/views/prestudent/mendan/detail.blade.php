@extends('layouts.default')
@section('content')
<form id="form" method="post" action="{{ url('/prestudent/mendan') }}">
  {{ csrf_field() }}
  <section class="scroll_table-wrap wht_bloc">
  <h2 class="heading-1">判定面談</h2>

  @if($mendanKiroku->process_finished_flag == -1)
    <div class="alert alert-danger flex flex-a-ctr">
      この判定面談は一時保存状態です。
    </div>
  @endif

  <div class="">
    <table class="datatable table-vertical">
      <tr>
        <th class="required">入塾校舎</th>
        <td class="form-group">
          <select id="school_id" name="school_id">
            <option value="">---</option>
            @if(count($schools) > 0)
              @foreach($schools as $school)
                <option value="{{ $school->id }}" {{ $school->id == old('school_id', $mendanKiroku->school_id) ? 'selected="selected"' : '' }} >{{ $school->name }}</option>
              @endforeach
            @endif
          </select>
        </td>
      </tr>
      <tr>
        <th class="required">面談日</th>
        <td class="form-group">
          <input type='text' id="mendan_date" name="mendan_date" autocomplete="off"  class="datepicker-here" data-language="jp" data-auto-close="true" value="{{ old('mendan_date', $mendanKiroku->mendan_date) }}" placeholder="面談日">
        </td>
      </tr>
      <tr>
        <th class="required">面談者</th>
        <td class="form-group">
          {{ $mendan_teacher_name }}
        </td>
      </tr>
      <tr>
        <th class="required">校長</th>
        <td class="form-group">
          <select id="manager_teacher_id" name="manager_teacher_id" style="background-image:none;" disabled>
            <option value="">---</option>
            @if(count($koucyous) > 0)
              @foreach($koucyous as $koucyou)
                <option class="manager_teacher_id_option" value="{{ $koucyou->id }}" {{ $koucyou->id == old('manager_teacher_id', $mendanKiroku->manager_teacher_id) ? 'selected="selected"' : '' }} data-tantou_school_id="{{ $koucyou->tantou_school_id }}">{{ $koucyou->name }}</option>
              @endforeach
            @endif
          </select>
        </td>
      </tr>
      <tr>
        <th class="required">生徒氏名</th>
        <td class="form-group">
          <input type="text" id="name" name="name" autocomplete="off" class="input-lg" value="{{ old('name', $mendanKiroku->name) }}" placeholder="生徒氏名">
        </td>
      </tr>
      <tr>
        <th class="required">生徒カナ</th>
        <td class="form-group">
          <input type="text" id="kana" name="kana" autocomplete="off" class="input-lg" value="{{ old('kana', $mendanKiroku->kana) }}" placeholder="生徒カナ">
        </td>
      </tr>
      <tr>
        <th class="required">生年月日</th>
        <td class="form-group">
          <input type='hidden' id="birth_date" name="birth_date">
          @php
            $birth_date = old('birth_date', $mendanKiroku->birth_date);
            $year = $month = $day = null;
            if ($birth_date) {
              $date = explode('-', $birth_date);
              if (count($date) == 3 && ! ($date[0] == 0 || $date[1] == 0 || $date[2] == 0) && checkdate($date[1], $date[2], $date[0])) {
                $bd = new \Carbon\Carbon($birth_date);
                $year = $bd->format('Y');
                $month = $bd->format('n');
                $day = $bd->format('j');
              }
            }
          @endphp
          <select id="birth_date_y" name="birth_date_y" style="min-width:100px;width:100px;">
            <option value="0">----</option>
            @for($y = 1970; $y <= date('Y'); $y++)
              <option value="{{ $y }}" {{ ($year == $y) ? 'selected' : '' }}>{{ $y }}</option>
            @endfor
          </select> 年
          <select id="birth_date_m" name="birth_date_m" style="min-width:70px;width:70px;">
            <option value="0">--</option>
            @for($m = 1; $m <= 12; $m++)
              <option value="{{ $m }}" {{ ($month == $m) ? 'selected' : '' }}>{{ $m }}</option>
            @endfor
          </select> 月
          <select id="birth_date_d" name="birth_date_d"  style="min-width:70px;width:70px;">
            <option value="0">--</option>
            @for($d = 1; $d <= 31; $d++)
              <option value="{{ $d }}" {{ ($day == $d) ? 'selected' : '' }}>{{ $d }}</option>
            @endfor
          </select> 日
        </td>
      </tr>
      <tr>
        <th class="required">性別</th>
        <td class="form-group">
          <select name="gender">
            <option value="">---</option>
            @foreach(config('jjss.gender') as $key => $gender)
              <option value="{{ $key }}" {{ $key == old('gender', $mendanKiroku->gender) ? 'selected="selected"' : '' }} >{{ $gender }}</option>
            @endforeach
          </select>
        </td>
      </tr>
      <tr>
        <th class="required">学校名</th>
        <td class="form-group">
          <input type="text" id="gakkou_name" name="gakkou_name" autocomplete="off" class="input-lg" value="{{ old('gakkou_name', $mendanKiroku->gakkou_name) }}" placeholder="学校名">
        </td>
      </tr>
      <tr>
        <th class="required">学年</th>
        <td class="form-group">
          <select id="grade_code" name="grade_code">
            <option value="">---</option>
            @foreach(config('jjss.grade') as $key => $grade)
              <option value="{{ $key }}" {{ $key == old('grade_code', $mendanKiroku->grade_code) ? 'selected="selected"' : '' }} >{{ $grade }}</option>
            @endforeach
          </select>
        </td>
      </tr>
      <tr>
        <th class="required">郵便番号</th>
        <td class="form-group">
          <input type="text" id="zip_code" name="zip_code" autocomplete="off" class="input-lg" value="{{ old('zip_code', $mendanKiroku->zip_code) }}" placeholder="郵便番号">
        </td>
      </tr>
      <tr>
        <th class="required">都道府県</th>
        <td class="form-group">
          <select id="pref" name="pref">
            <option value="">選択してください</option>
            @foreach(config('pref') as $pref)
              <option value="{{ $pref }}" {{ $pref == old('pref', $mendanKiroku->pref) ? 'selected="selected"' : '' }} >{{ $pref }}</option>
            @endforeach
          </select>
        </td>
      </tr>
      <tr>
        <th class="required">市区町村・番地</th>
        <td class="form-group">
          <input type="text" id="address1" name="address1" autocomplete="off" class="input-lg" value="{{ old('address1', $mendanKiroku->address1) }}" placeholder="住所1">
        </td>
      </tr>
      <tr>
        <th>マンション名・部屋番号</th>
        <td class="form-group">
          <input type="text" id="address2" name="address2" autocomplete="off" class="input-lg" value="{{ old('address2', $mendanKiroku->address2) }}" placeholder="住所2">
        </td>
      </tr>
      <tr>
        <th>生徒携帯TEL</th>
        <td class="form-group">
          <input type="text" id="tel" name="tel" autocomplete="off" class="input-lg" value="{{ old('tel', $mendanKiroku->tel) }}" placeholder="携帯TEL">
        </td>
      </tr>
      <tr>
        <th>生徒携帯メール</th>
        <td class="form-group">
          <input type="text" id="email" name="email" autocomplete="off" class="input-lg" value="{{ old('email', $mendanKiroku->email) }}" placeholder="メール">
        </td>
      </tr>
      <tr>
        <th class="required">保護者氏名</th>
        <td class="form-group">
          <input type="text" id="hogosya_name" name="hogosya_name" autocomplete="off" class="input-lg" value="{{ old('hogosya_name', $mendanKiroku->hogosya_name) }}" placeholder="保護者氏名">
        </td>
      </tr>
      <tr>
        <th class="required">保護者カナ</th>
        <td class="form-group">
          <input type="text" id="hogosya_kana" name="hogosya_kana" autocomplete="off" class="input-lg" value="{{ old('hogosya_kana', $mendanKiroku->hogosya_kana) }}" placeholder="保護者カナ">
        </td>
      </tr>
      <tr>
        <th class="required">続柄</th>
        <td class="form-group">
          <input type="text" id="hogosya_tsudukigara" name="hogosya_tsudukigara" autocomplete="off" class="input-lg" value="{{ old('hogosya_tsudukigara', $mendanKiroku->hogosya_tsudukigara) }}" placeholder="続柄">
        </td>
      </tr>
      <tr>
        <th>自宅TEL</th>
        <td class="form-group">
          <input type="text" id="hogosya_home_tel" name="hogosya_home_tel" autocomplete="off" class="input-lg" value="{{ old('hogosya_home_tel', $mendanKiroku->hogosya_home_tel) }}" placeholder="自宅TEL">
        </td>
      </tr>
      <tr>
        <th>自宅FAX</th>
        <td class="form-group">
          <input type="text" id="hogosya_fax" name="hogosya_fax" autocomplete="off" class="input-lg" value="{{ old('hogosya_fax', $mendanKiroku->hogosya_fax) }}" placeholder="自宅FAX">
        </td>
      </tr>
      <tr>
        <th class="required">保護者携帯TEL</th>
        <td class="form-group">
          <input type="text" id="hogosya_tel" name="hogosya_tel" autocomplete="off" class="input-lg" value="{{ old('hogosya_tel', $mendanKiroku->hogosya_tel) }}" placeholder="保護者携帯TEL">
        </td>
      </tr>
      <tr>
        <th class="required">保護者メール</th>
        <td class="form-group">
          <input type="text" id="hogosya_email" name="hogosya_email" autocomplete="off" class="input-lg" value="{{ old('hogosya_email', $mendanKiroku->hogosya_email) }}" placeholder="保護者メール">
        </td>
      </tr>
      <tr>
        <th class="required">初回OT実施日時</th>
        <td class="form-group">
          @php
              $orientationDate = old('orientation_date');
              $orientationTime = old('orientation_time');
              if($orientationDate == null && $orientationTime == null) {
                  $orientationDate = '';
                  $orientationTime = '';
                  if($mendanKiroku->orientation_datetime) {
                      $orientationDateTime = \JJSS::displayDateTime($mendanKiroku->orientation_datetime);

                      if ($orientationDateTime) {
                          $orientationDate = $orientationDateTime->format('Y-m-d');
                          $orientationTime = $orientationDateTime->format('H:i');
                      }
                  }
              }
          @endphp
          <input type='text' id="orientation_date" name="orientation_date" autocomplete="off" class="datepicker-here" data-language="jp" data-auto-close="true" value="{{ $orientationDate }}" />
          &nbsp;
          <input type='text' id="orientation_time" name="orientation_time" autocomplete="off" class="timepicker input-mn" value="{{ $orientationTime }}" > ※手入力が可能です
        </td>
      </tr>
      <tr>
        <th class="required">初回授業日</th>
        <td class="form-group">
          <input type='text' id="first_shidou_date" name="first_shidou_date" autocomplete="off" class="datepicker-here" data-language="jp" data-auto-close="true" value="{{ old('first_shidou_date', $mendanKiroku->first_shidou_date) }}" placeholder="初回授業日">
        </td>
      </tr>
      <tr>
        <th>学習科目</th>
        <td class="form-group">
          <ul class="flex flex-col4">
            @php
                $mendanKiroku_kamokus = old('kamoku_ids');
                if($mendanKiroku_kamokus === null) {
                    $mendanKiroku_kamokus = [];
                    if($mendanKiroku->kamoku_ids) {
                        $mendanKiroku_kamokus = explode(',', $mendanKiroku->kamoku_ids);
                    }
                }
            @endphp
            @if(count($kamokus) > 0)
              @foreach($kamokus as $kamoku)
                <li>
                  <input type="checkbox" name="kamoku_ids[]" id="kamoku_ids{{ $kamoku->id }}"
                         value="{{ $kamoku->id }}" {{ in_array($kamoku->id, $mendanKiroku_kamokus) === true ? 'checked="checked"' : '' }} >
                  <label for="kamoku_ids{{ $kamoku->id }}">{{ $kamoku->name }}</label>
                </li>
              @endforeach
            @endif
          </ul>
        </td>
      </tr>
      @php
          $courseId = old('course_id', $mendanKiroku->course_id);
          $courseType = ($courseId) ? \JJSS::getCourseType($courseId) : '';
      @endphp
      <tr>
        <th class="required">通塾コース</th>
        <td class="form-group flex">
          <select id="course_id" name="course_id" class="course_change">
            <option value="">---</option>
            @if(count($courses) > 0)
              @foreach($courses as $course)
                <option data-is-unlimited="{{ $course->is_unlimited }}" data-is-none_course="{{ $course->id == \JJSS::getNoneCourse() }}" value="{{ $course->id }}" {{ $course->id == $courseId ? 'selected="selected"' : '' }} >{{ $course->name }}</option>
              @endforeach
            @endif
          </select>
        </td>
      </tr>
      <tr class="course_schedule_row" style="{{ ($courseType === '' || $courseType === 'unlimited' || $mendanKiroku->course_id == \JJSS::getNoneCourse()) ? 'display: none;' : '' }}">
        <th class="required">通塾予定</th>
        <td class="form-group">
          <div class="mgn-btm8" style="margin-left: 2.4em;">
            ※手入力が可能です
          </div>
          <div class="mgn-btm8">
            <b>月</b>　
            <input type='text' id="mon_start_time" name="mon_start_time" autocomplete="off" class="timepicker input-mn" value="{{ old('mon_start_time', (isset($mendanKiroku->mon_start_time) === true ? $mendanKiroku->mon_start_time : '')) }}" > 〜
            <input type='text' id="mon_end_time" name="mon_end_time" autocomplete="off" class="timepicker input-mn" value="{{ old('mon_end_time', (isset($mendanKiroku->mon_end_time) === true ? $mendanKiroku->mon_end_time : '')) }}" >
          </div>
          <div class="mgn-btm8">
            <b>火</b>　
            <input type='text' id="tue_start_time" name="tue_start_time" autocomplete="off" class="timepicker input-mn" value="{{ old('tue_start_time', (isset($mendanKiroku->tue_start_time) === true ? $mendanKiroku->tue_start_time : '')) }}" > 〜
            <input type='text' id="tue_end_time" name="tue_end_time" autocomplete="off" class="timepicker input-mn" value="{{ old('tue_end_time', (isset($mendanKiroku->tue_end_time) === true ? $mendanKiroku->tue_end_time : '')) }}" >
          </div>
          <div class="mgn-btm8">
            <b>水</b>　
            <input type='text' id="wed_start_time" name="wed_start_time" autocomplete="off" class="timepicker input-mn" value="{{ old('wed_start_time', (isset($mendanKiroku->wed_start_time) === true ? $mendanKiroku->wed_start_time : '')) }}" > 〜
            <input type='text' id="wed_end_time" name="wed_end_time" autocomplete="off" class="timepicker input-mn" value="{{ old('wed_end_time', (isset($mendanKiroku->wed_end_time) === true ? $mendanKiroku->wed_end_time : '')) }}" >
          </div>
          <div class="mgn-btm8">
            <b>木</b>　
            <input type='text' id="thu_start_time" name="thu_start_time" autocomplete="off" class="timepicker input-mn" value="{{ old('thu_start_time', (isset($mendanKiroku->thu_start_time) === true ? $mendanKiroku->thu_start_time : '')) }}" > 〜
            <input type='text' id="thu_end_time" name="thu_end_time" autocomplete="off" class="timepicker input-mn" value="{{ old('thu_end_time', (isset($mendanKiroku->thu_end_time) === true ? $mendanKiroku->thu_end_time : '')) }}" >
          </div>
          <div class="mgn-btm8">
            <b>金</b>　
            <input type='text' id="fri_start_time" name="fri_start_time" autocomplete="off" class="timepicker input-mn" value="{{ old('fri_start_time', (isset($mendanKiroku->fri_start_time) === true ? $mendanKiroku->fri_start_time : '')) }}" > 〜
            <input type='text' id="fri_end_time" name="fri_end_time" autocomplete="off" class="timepicker input-mn" value="{{ old('fri_end_time', (isset($mendanKiroku->fri_end_time) === true ? $mendanKiroku->fri_end_time : '')) }}" >
          </div>
        </td>
      </tr>
      <tr class="unlimited_row" style="{{ ($courseType === '' || $courseType === 'course') ? 'display: none;' : '' }}">
        <th class="required">無制限終了日</th>
        <td class="form-group">
          <input type='text' id="unlimited_end_date" name="unlimited_end_date" autocomplete="off" class="datepicker-here" data-language="jp" data-auto-close="true" value="{{ old('unlimited_end_date', $mendanKiroku->unlimited_end_date) }}" placeholder="無制限終了日">
        </td>
      </tr>
      <tr class="unlimited_row" style="{{ ($courseType === '' || $courseType === 'course') ? 'display: none;' : '' }}">
        <th>無制限割引有無</th>
        <td class="form-group">
          <ul class="flex flex-col4">
            <li>
              <input type="checkbox" name="unlimited_discount_flag" id="unlimited_discount_flag" value="1" {{ (old('unlimited_discount_flag', $mendanKiroku->unlimited_discount_flag) == '1') ? 'checked="checked"' : '' }} >
              <label for="unlimited_discount_flag"></label>
            </li>
          </ul>
        </td>
      </tr>
      <tr>
        <th class="required">請求内訳</th>
        <td class="form-group">
          <input id="seikyu_data" type="hidden" name="seikyu_data" value="{{ old('seikyu_data', $mendanKiroku->seikyu_data) }}">
          <table id="seikyu_data_table">
            <tr>
              <th>内訳</th>
              <th>金額</th>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>①入会金<input type="hidden" class="seikyu_breakdown" name="seikyu_breakdown[]" value="①入会金"></td>
              <td><input type="text" data-format="number" class="seikyu_amount" name="seikyu_amount[]" autocomplete="off" placeholder="金額"></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>②学力テスト＋コンサルフィー<input type="hidden" class="seikyu_breakdown" name="seikyu_breakdown[]" value="②学力テスト＋コンサルフィー"></td>
              <td><input type="text" data-format="number" class="seikyu_amount" name="seikyu_amount[]" autocomplete="off" placeholder="金額"></td>
              <td>&nbsp;</td>
            </tr>
          </table>
          <span id="gessya_recalc_message" class="red"></span>
        </td>
      </tr>
      <tr>
        <th>小計</th>
        <td class="form-group">
          <input type="text" data-format="number" id="subtotal" name="subtotal" value="{{ old('subtotal', $seikyu_total->subtotal ?? '') }}" readonly/>
        </td>
      </tr>
      <tr>
        <th>消費税</th>
        <td class="form-group">
          <input type="text" data-format="number" id="tax" name="tax" value="{{ old('tax', $seikyu_total->tax ?? '') }}" readonly/>
        </td>
      </tr>
      <tr>
        <th>合計</th>
        <td class="form-group">
          <input type="text" data-format="number" id="total" name="total" value="{{ old('total', $seikyu_total->total ?? '') }}" readonly/>
        </td>
      </tr>
    </table>
  </div>
  <div class="datatable btn_list">
    {{-- キャンセル（一覧に遷移）--}}
    <button type="button" id="cancel" class="btn-info btn-line btn-lg btn-lg-slim">キャンセル</button>
    {{-- 一時保存 --}}
    @php
      $add_class = 'is_disabled';
      if($mendanKiroku->id === null) {
        $add_class = '';
      }
      if($mendanKiroku->id !== null && $mendanKiroku->syounin_teacher_id === null
          && ($mendanKiroku->teacher_id == \Auth::user()->teacher->id || in_array($permission, ['管理者']))) {
        $add_class = '';
      }
    @endphp
    <button type="button" id="temp" class="btn-line btn-lg btn-lg-slim {{ $add_class }}">一時保存</button>
    {{-- 申請（登録・更新） --}}
    @php
      $add_class = 'is_disabled';
      if($mendanKiroku->id === null) {
        $add_class = '';
      }
      if($mendanKiroku->id !== null && $mendanKiroku->syounin_teacher_id === null
          && ($mendanKiroku->teacher_id == \Auth::user()->teacher->id || in_array($permission, ['管理者']))) {
        $add_class = '';
      }
    @endphp
    <button type="button" id="save" class="btn-lg {{ $add_class }}">申請</button>
    {{-- 申請取消（削除） --}}
    @php
      $add_class = 'is_disabled';
      if($mendanKiroku->teacher_id == \Auth::user()->teacher->id || in_array($permission, ['管理者'])) {
        if($mendanKiroku->id !== null && $mendanKiroku->syounin_teacher_id === null) {
          $add_class = '';
        }
      }
    @endphp
    <button type="button" id="torikeshi" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">申請取消</button>
    {{-- 承認 --}}
    @if(in_array($permission, ['校長', 'マネージャー', '管理者']))
      @php
        $add_class = 'is_disabled';
        if((in_array($permission, ['マネージャー', '管理者']) && $mendanKiroku->id !== null && $mendanKiroku->process_finished_flag != -1 && $mendanKiroku->syounin_teacher_id === null && $mendanKiroku->mendan_teacher_id != \Auth::user()->teacher->id) ||
           ($mendanKiroku->process_finished_flag != -1 && $mendanKiroku->syounin_teacher_id === null && $mendanKiroku->id !== null && $mendanKiroku->school_id == \Auth::user()->teacher->tantou_school_id) && $mendanKiroku->mendan_teacher_id != \Auth::user()->teacher->id) {
          $add_class = '';
        }
      @endphp
      <button type="button" id="syounin" class="btn-lg {{ $add_class }}">承認</button>
    @endif
    {{-- 印刷 --}}
    <button type="button" id="print" class="btn-lg btn-lg-slim">印刷</button>
    {{-- 承認取消申請 --}}
    @php
      $add_class = 'is_disabled';
      if($mendanKiroku->teacher_id == \Auth::user()->teacher->id || in_array($permission, ['管理者'])) {
        if($mendanKiroku->syounin_teacher_id !== null && $mendanKiroku->syounin_torikeshi_shinsei_flag !== 1) {
          $add_class = '';
        }
      }
    @endphp
    <button type="button" id="syounin_torikeshi_shinsei" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">承認取消申請</button>
    {{-- 承認取消 --}}
    @php
      $add_class = 'is_disabled';
      if(in_array($permission, ['校長', 'マネージャー', '管理者'])) {
        if((in_array($permission, ['マネージャー', '管理者']) && $mendanKiroku->syounin_torikeshi_shinsei_flag == "1") ||
        ($mendanKiroku->syounin_teacher_id !== null && $mendanKiroku->syounin_torikeshi_shinsei_flag == "1" && $mendanKiroku->school_id == \Auth::user()->teacher->tantou_school_id)) {
          $add_class = '';
        }
    @endphp
    <button type="button" id="cancel_approval" class="btn-warning btn-lg btn-lg-slim {{ $add_class }}">承認取消</button>
    @php
      }
    @endphp

    <input type="hidden" id="id" name="id" value="{{ $mendanKiroku->id }}">
  </div>

  </section>
</form>
@endsection

@section('script')
  <script src="{{ asset('js/timepicker.min.js') }}" defer></script>
  <script>
    $(function(){
      var fx = {
        'addSeikyuFields': function(data = null) {
          var seikyuBreakdown = '',
                  seikyuAmount = '';
          if (data) {
            seikyuBreakdown = data.title;
            seikyuAmount = data.price;
          }
          var tableRow = '<tr class="gessya_row">' +
                  '<td>' + seikyuBreakdown + '<input type="hidden" class="seikyu_breakdown" name="seikyu_breakdown[]" value="' + seikyuBreakdown + '"></td>';
          if (seikyuAmount == '' || seikyuAmount == 0) {
              tableRow += '<td><input type="hidden" class="seikyu_amount" name="seikyu_amount[]" value=""></td>';
          } else {
              tableRow += '<td><input type="text" data-format="number" class="seikyu_amount" name="seikyu_amount[]" placeholder="金額" value="' + seikyuAmount + '"></td>';
          }
          tableRow += '</tr>';
          return tableRow;
        },
      }

      $('.timepicker').timepicker({
          'timeFormat': 'H:i',
          'minTime': '21:40',
          'maxTime': '21:40',
          'scrollDefault': '16:00',
          'noneOption': [
              { 'label': '16:00', 'value': '16:00' },
              { 'label': '16:30', 'value': '16:30' },
              { 'label': '17:00', 'value': '17:00' },
              { 'label': '17:30', 'value': '17:30' },
              { 'label': '18:00', 'value': '18:00' },
              { 'label': '18:30', 'value': '18:30' },
              { 'label': '19:00', 'value': '19:00' },
              { 'label': '19:30', 'value': '19:30' },
              { 'label': '20:00', 'value': '20:00' },
              { 'label': '20:30', 'value': '20:30' },
              { 'label': '21:00', 'value': '21:00' },
              { 'label': '21:30', 'value': '21:30' },
          ],
      });

      function set_birth_date()
      {
        var y = $('#birth_date_y').val();
        var m = $('#birth_date_m').val();
        var d = $('#birth_date_d').val();
        var target_date = y + '-' + m + '-' + d;
        $('#birth_date').val(target_date);
      }

      $('body').on('click', '#temp,#save', function(e) {
        e.preventDefault();
        var type = $(this).attr('id');  // temp or save
        if (type == 'save') {
          calc_gessya_or_error( type );
        } else {
          doSave(type);
        }
      });

      function doSave(type) {
        $('#manager_teacher_id').prop('disabled', false);  // 校長データを送信可能に変更
        set_birth_date();
        var isUnlimited = $('.course_change').find('option:selected').attr('data-is-unlimited');
        $('#form').attr('action', '{{ url("/prestudent/mendan") }}/' + type);
        $('#form').submit();
      }

      $('#torikeshi').on('click', function() {
        swal({
          text: "申請を取消します。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/prestudent/mendan_delete") }}');
            $('#form').submit();
          }
        });
      });

      $('#print').on('click', function(e) {
        @if( $mendanKiroku->syounin_teacher_id === null  // 未承認 かつ
          && (
               in_array($permission, ['管理者'])  // 管理者 か
            || $mendanKiroku->id === null  // 新規の場合 か
            || $mendanKiroku->teacher_id == \Auth::user()->teacher->id  // 登録者が一致する場合
          ))
        // 申請処理->印刷
        e.preventDefault();
        var type = 'print';
        calc_gessya_or_error( type );
        @else
        window.open('{{ url('/prestudent/mendan/print'). '/'. $mendanKiroku->id }}', '_blank');
        @endif
      });
      @if (session()->get('print', false) == true)
      window.open('{{ url('/prestudent/mendan/print'). '/'. $mendanKiroku->id }}', '_blank');
      @endif

      $('#syounin_torikeshi_shinsei').on('click', function() {
        swal({
          text: "承認取消申請を行います。よろしいですか？",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            $('#form').attr('action', '{{ url("/prestudent/mendan_torikeshi_shinsei") }}');
            $('#form').submit();
          }
        });
      });

      $('body').on('click', '#cancel', function(e) {
          e.preventDefault();
          location.href = '{{ url('/prestudent/mendan') }}';
      });

      @if(in_array($permission, ['校長', 'マネージャー', '管理者']))
      $('#syounin').on('click', function() {
          set_birth_date();
          $('#form').attr('action', '{{ url("/prestudent/mendan/syounin") }}');
          $('#form').submit();
      });
      $('#cancel_approval').on('click', function() {
          set_birth_date();
          $('#form').attr('action', '{{ url("/prestudent/mendan/cancel_approval") }}');
          $('#form').submit();
      });
      @endif

      $('body').on('change', '#school_id', function(e) {
          var school_id = $(this).find('option:selected').val();
          var elem = $('.manager_teacher_id_option[data-tantou_school_id="' + school_id + '"]');
          var manager_teacher_id = elem.val();
          $('#manager_teacher_id').val(manager_teacher_id);
      });

      $('body').on('change', '.course_change', function(e) {
          var isNoneCourse = $(this).find('option:selected').attr('data-is-none_course');
          var isUnlimited = $(this).find('option:selected').attr('data-is-unlimited');

          if (isNoneCourse == true) {
            $('.course_schedule_row').hide();
            $('.unlimited_row').hide();
          } else if (isUnlimited === '1') {
              $('.course_schedule_row').hide();
              $('.unlimited_row').show();
          } else if (isUnlimited === '0') {
              $('.unlimited_row').hide();
              $('.course_schedule_row').show();
          } else {
              $('.course_schedule_row').hide();
              $('.unlimited_row').hide();
          }
      });

      if ($('#seikyu_data').val().trim()) {
          var seikyuData = [];
          try {
              seikyuData = JSON.parse($('#seikyu_data').val().trim());
          } catch (e) {
              seikyuData = [];
          }

          if (seikyuData.length) {
              var seikyuBreakdown = $('.seikyu_breakdown'),
                  seikyuAmount = $('.seikyu_amount'),
                  sdLength = seikyuData.length,
                  i;

              for (i = 0; i < sdLength; i++) {
                  if ($(seikyuAmount[i]).length) {
                      $(seikyuAmount[i]).val(seikyuData[i].price);
                  } else {
                      $('#seikyu_data_table').append(fx.addSeikyuFields(seikyuData[i]));
                      $('[data-format="number"]').numberformat();
                  }
              }
          }
          calc_total();
      }

      function calc_total() {
          subtotal = 0;
          $('.seikyu_amount').each(function(){
              val = ($(this).attr('data-value') === void 0) ? $(this).val() : $(this).attr('data-value');
              subtotal += parseInt(val - 0);
          });
          tax = parseInt(subtotal * 0.1);
          total = parseInt(subtotal * 1.1);
          $('#subtotal').val(subtotal).change();
          $('#tax').val(tax).change();
          $('#total').val(total).change();
      }
      $('.seikyu_amount').on('focusout', function () {
          calc_total();
      });

      @php
        // 月割計算するかどうか
        $need_calc = ($mendanKiroku->mendan_teacher_id == \Auth::user()->teacher->id) ||
                     ($mendanKiroku->process_finished_flag === NULL || $mendanKiroku->process_finished_flag == -1)
                   ? 'true' : 'false';
      @endphp

      $(document).on('change',
                     '#school_id,#grade_code,#course_id,' +
                     '#mon_start_time,#tue_start_time,#wed_start_time,#thu_start_time,#fri_start_time,' +
                     '#mon_end_time,#tue_end_time,#wed_end_time,#thu_end_time,#fri_end_time,' +
                     '#unlimited_discount_flag',
                     function () {
        if (check_for_calc_gessya_price() && {{ $need_calc }}) {
          set_need_recalc();
          calc_gessya_price();
        }
      });

      var first_shidou_date_dp = $('#first_shidou_date').datepicker().data('datepicker');
      @if (old('first_shidou_date', $mendanKiroku->first_shidou_date))
       first_shidou_date_dp.selectDate(new Date('{{ old('first_shidou_date', $mendanKiroku->first_shidou_date) }}'));
      @endif
      first_shidou_date_dp.update('onHide', function(inst, animationCompleted){
        if (animationCompleted == false) return;
        if (check_for_calc_gessya_price() && {{ $need_calc }}) {
          set_need_recalc();
          calc_gessya_price();
        }
      });
      var unlimited_end_date_dp = $('#unlimited_end_date').datepicker().data('datepicker');
      @if (old('unlimited_end_date', $mendanKiroku->unlimited_end_date))
        unlimited_end_date_dp.selectDate(new Date('{{ old('unlimited_end_date', $mendanKiroku->unlimited_end_date) }}'));
      @endif
      unlimited_end_date_dp.update('onHide', function(inst, animationCompleted){
        if (animationCompleted == false) return;
        if (check_for_calc_gessya_price() && {{ $need_calc }}) {
          set_need_recalc();
          calc_gessya_price();
        }
      });

      function set_need_recalc() {
        $('#gessya_recalc_message').text('一時保存・申請を行う前に再計算を行ってください。');
        $('#temp,#save').addClass('is_disabled');
      }

      function reset_need_recalc() {
        $('#gessya_recalc_message').text('');
        $('#temp,#save').removeClass('is_disabled');
      }

      function calc_gessya_or_error(type) {
        if (check_for_calc_gessya_price()) {
          calc_gessya_price(type);
        } else {
          var options = {
            text: "日割金額の算出でエラーが発生しました。\nデータが不足しているか、正しくないデータが指定されています。\n\n入塾校舎・学年・初回授業日・通塾コース・(通常コースの場合は)通塾予定を選択、または入力してください。\n無制限コースの場合は、無制限終了日に初回授業日以後の日付を指定してください。",
            icon: "warning",
            buttons: 'OK',
          };
          swal(options);
        }
      }

      function check_for_calc_gessya_price() {
        if (
                $('#school_id').val() !== '' &&
                $('#grade_code').val() !== '' &&
                $('#first_shidou_date').val() !== '' &&
                $('#course_id').val() !== '' &&
                (
                    $('#course_id').val() === '16' || $('#course_id').val() === '17' ||
                    ( $('#course_id').val() !== '16' && $('#course_id').val() !== '17' &&
                        (
                            // 通常コース時、毎週の入退出時間の入力をチェック
                            ($('#mon_start_time').val() !== '' && $('#mon_end_time').val() !== '') ||
                            ($('#tue_start_time').val() !== '' && $('#tue_end_time').val() !== '') ||
                            ($('#wed_start_time').val() !== '' && $('#wed_end_time').val() !== '') ||
                            ($('#thu_start_time').val() !== '' && $('#thu_end_time').val() !== '') ||
                            ($('#fri_start_time').val() !== '' && $('#fri_end_time').val() !== '')
                        )
                    )
                ) &&
                (
                    $('#course_id').val() !== '16' ||
                    (
                        // 無制限コース時、無制限終了日は初回授業日以後かをチェック
                        $('#course_id').val() === '16' &&
                        $('#unlimited_end_date').val() >= $('#first_shidou_date').val()
                    )
                )
        ) {
          // これだけのデータがセットされていれば再計算処理はエラーにならない
          return true;
        } else {

          return false;
        }
      }

      @if (isset($errors) && count($errors) > 0)
      // エラー時に再計算する
      if (check_for_calc_gessya_price() && {{ $need_calc }}) {
        calc_gessya_price();
      }
      @endif

      function calc_gessya_price(type) {
        $.ajax({
          url: "/api/prestudent/mendan/get_gessya_price",
          type: "GET",
          data: {
            course_id: $('#course_id').val(),
            grade_code: $('#grade_code').val(),
            school_id: $('#school_id').val(),
            first_shidou_date: $('#first_shidou_date').val(),
            unlimited_discount_flag: $('#unlimited_discount_flag:checked').val(),
            unlimited_end_date: $('#unlimited_end_date').val(),
            mon_start_time: $('#mon_start_time').val(), mon_end_time: $('#mon_end_time').val(),
            tue_start_time: $('#tue_start_time').val(), tue_end_time: $('#tue_end_time').val(),
            wed_start_time: $('#wed_start_time').val(), wed_end_time: $('#wed_end_time').val(),
            thu_start_time: $('#thu_start_time').val(), thu_end_time: $('#thu_end_time').val(),
            fri_start_time: $('#fri_start_time').val(), fri_end_time: $('#fri_end_time').val()
          },
          dataType: 'json',
          beforeSend: function() {
            $('.loading').fadeIn();
          },
        })
        .done((data, statusText, xhr) => {
          $('.gessya_row').remove();
          for (d of data) {
            $('#seikyu_data_table').append(fx.addSeikyuFields(d));
            $('[data-format="number"]').numberformat();
          }
          calc_total();
          reset_need_recalc();
          if (type) {
            doSave(type);  // 保存処理
          } else {
            $('.loading').fadeOut();
          }
        })
        .fail((jqXHR) => {
          var text = "処理中にエラーが発生しました。";
          if (jqXHR.responseJSON.error) text = jqXHR.responseJSON.error;
          var options = {
            text: text,
            icon: "error",
            timer: 5000,
            buttons: false,
          };
          swal(options);
          //self.show();
          $('.loading').fadeOut();
        })
        .always((data) => {
        });
      }
    });
  </script>
@endsection
