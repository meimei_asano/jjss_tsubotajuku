@extends('layouts.default')
@section('content')
<section class="scroll_table-wrap wht_bloc">

  <h2 class="heading-1">判定面談リスト</h2>

  <div class="flex flex-j-between flex-a-ctr mgn-btm16">
    <form id="search_form" action="{{  url('prestudent/mendan') }}" class="mgn-btm16">
      <div class="form-group flex">
        <input type="text" id="search_name" name="search_name" placeholder="生徒氏名" class="input-sm" value="{{ $search['search_name'] ?? '' }}">
        &nbsp;
        <select id="school_id" name="school_id" >
          <option value="">入塾校舎</option>
          {!! \JJSS::options($schools, 'id', 'name', ($search['school_id']) ?? '') !!}
        </select>
        &nbsp;
        <select id="teacher_id" name="teacher_id" >
          <option value="">面談者</option>
          {!! \JJSS::options($teachers, 'id', 'name', ($search['teacher_id']) ?? '') !!}
        </select>
        <input type="text" id="mendan_date" name="mendan_date" placeholder="面談日" class="datepicker-here input-sm" data-language="jp" autocomplete="off" />
        &nbsp;
        <ul>
          <li>
            <input type="checkbox" id="process_finished_flag" name="process_finished_flag" value="1" {{ (isset($search['process_finished_flag']) && $search['process_finished_flag']) ? 'checked' : '' }}>
            <label for="process_finished_flag">承認済</label>
          </li>
        </ul>
        &nbsp;
        <button id="btn-search">検索<i class='bx bx-search'></i></button>
      </div>
      <input type="hidden" name="orderby_column_name" value="{{ ($orderby['column_name']) ?? '' }}">
      <input type="hidden" name="orderby_type" value="{{ ($orderby['type']) ?? '' }}">
    </form>
    <div class="btn_list">
      <button id="create" onclick="location.href='{{ url('prestudent/mendan'). '/new' }}';"><i class='bx bxs-user-plus'></i>新規</button>
    </div>
  </div>

  <div class="scroll_table mgn-btm16">
    <table class="table-horizon table-striped datatable">
      <thead>
        <tr>
          <th class="th_check">
            <input type="checkbox" class="check" id="check_all" value="all">
            <label for="check_all"></label>
          </th>
          <th class="set_orderby" dt-column="school_id">入塾校舎</th>
          <th class="set_orderby" dt-column="mendan_date">面談日</th>
          <th class="set_orderby" dt-column="mendan_teachers.kana">面談者</th>
          <th class="set_orderby" dt-column="kana">生徒氏名</th>
          <th class="set_orderby" dt-column="grade_code">学年</th>
          <th class="set_orderby" dt-column="syounin_teacher_id">承認済</th>
          <th>承認取消</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      @if(count($mendanKirokus) == 0)
        <tr>
          <td colspan="9">
            登録されていません
          </td>
        </tr>
      @else
        @foreach ($mendanKirokus as $mendanKiroku)
        <tr style="">
          <td class="td_check">
            @if(($mendanKiroku->teacher_id == $teacher->id || in_array($permission, ['管理者'])) && $mendanKiroku->syounin_teacher_id === null)
              <input type="checkbox" class="check" data-mendan_kiroku_id="{{ $mendanKiroku->id }}">
              <label></label>
            @endif
          </td>
          <td class="">{{ ($mendanKiroku->school) ? $mendanKiroku->school->name : '' }}</td>
          <td class="">{{ $mendanKiroku->mendan_date }}</td>
          <td class="">{{ ($mendanKiroku->mendan_teacher) ? $mendanKiroku->mendan_teacher->name : '' }}</td>
          <td class="">{{ $mendanKiroku->name }}</td>
          <td class="">{{ ($mendanKiroku->grade_code) ? config('jjss.grade')[$mendanKiroku->grade_code] : '' }}</td>
          <td class="">{{ ($mendanKiroku->process_finished_flag == -1) ? '一時保存' : (($mendanKiroku->process_finished_flag == 1) ? '済' : '') }}</td>
          <td class="">{{ ($mendanKiroku->syounin_torikeshi_shinsei_flag == 1 ? '申請中' : '') }}</td>
          <td class="">
            <button class="btn-sm" onclick="location.href='{{ url('prestudent/mendan'). '/'. $mendanKiroku->id }}';">変更</button>
            @if ($mendanKiroku->process_finished_flag != -1)
            <button class="btn-sm" onclick="window.open('{{ url('prestudent/mendan/print'). '/'. $mendanKiroku->id }}', '_blank');"><i class='bx bxs-printer'></i>印刷</button>
            @endif
          </td>
        </tr>
        @endforeach
      @endif
      </tbody>
    </table>
  </div>
  <div class="flex flex-j-between">
    <div class="btn_list">
      <button type="button" id="ikkatsu_torikeshi" style="width: 120px;">一括申請取消</button>
    </div>
    <div class="pager txt-ctr">
      {{ $mendanKirokus->appends(array('search_name' => (isset($search['name'])===true ? $search['name'] : ''),
                                  'school_id' => (isset($search['school_id'])===true ? $search['school_id'] : ''),
                                  'teacher_id' => (isset($search['teacher_id'])===true ? $search['teacher_id'] : ''),
                                  'mendan_date' => (isset($search['mendan_date'])===true ? $search['mendan_date'] : ''),
                                  'process_finished_flag' => (isset($search['process_finished_flag'])===true ? $search['process_finished_flag'] : ''),

                                  'orderby_column_name' => (isset($orderby['column_name'])===true ? $orderby['column_name'] : ''),
                                  'orderby_type' => (isset($orderby['type'])===true ? $orderby['type'] : '')
                                  ))->links() }}
    </div>
  </div>

  <form id="form" method="post" action="{{ url('prestudent/mendan_ikkatsu_delete') }}" >
    {{ csrf_field() }}
    <input type="hidden" id="ids" name="ids">
  </form>
</section>
@endsection

@section('script')
<script>
$(function(){
  var mendanDate = $('#mendan_date').datepicker({language: 'jp', autoClose: true}).data('datepicker');
  var searchMendanDate = '{{ $search["mendan_date"] ?? null }}';
  if (searchMendanDate != '') {
    var createDate = new Date(searchMendanDate);
    mendanDate.selectDate(createDate);
  }

  $('#school_id').select2({ language: "ja"});
  $('#teacher_id').select2({ language: "ja"});

  $('#btn-search').on('click', function() {
    $('#search_form').submit();
  });


  $('.set_orderby').on('click', function() {
    var current_orderby = $('input[name="orderby_type"]').val();
    var current_column = $('input[name="orderby_column_name"]').val();
    var new_column = $(this).attr('dt-column');

    if (current_column == new_column && current_orderby == "asc") {
      $('input[name="orderby_type"]').val('desc');
    } else {
      $('input[name="orderby_column_name"]').val(new_column);
      $('input[name="orderby_type"]').val('asc');
    }

    $('#search_form').submit();
  });

  $('#ikkatsu_torikeshi').on('click', function() {
    var selected_id = [];
    $('.check').each(function() {
      if($(this).prop('checked')) {
        if($(this).attr('data-mendan_kiroku_id') !== undefined) {
          selected_id.push($(this).attr('data-mendan_kiroku_id'));
        }
      }
    });

    if(selected_id.length == 0) {
      var options = {
        text: "対象の判定面談情報にチェックしてください。",
        icon: "info",
        timer: 2000,
        buttons: false,
      }
      swal(options);
      return;
    }

    swal({
      text: "一括申請取消を行います。よろしいですか？",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('#ids').val(selected_id.join(','));
        $('#form').submit();
      }
    });
  });

});
</script>
@endsection
