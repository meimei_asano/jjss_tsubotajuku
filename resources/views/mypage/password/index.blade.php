{{-- 個人設定 --}}
@extends('layouts.default')

@section('content')
<form id="form" action="/mypage/password/register" method="post" enctype="multipart/form-data">
	{{ csrf_field() }}
	<h3 class="heading-2 txt-ctr">個人設定：パスワード変更</h3>
	<div class="flex flex-a-ctr flex-j-ctr">

		<section class="wht_bloc">
			<table class="table-vertical mgn-btm16" style="border:none;">
				<tbody>
					<tr>
						<th>名前</th>
						<td>{{ $user->name }}</td>
					</tr>
					<tr>
						<th>現在のパスワード</th>
						<td class="form-group">
							<input type="password" name="old_password" value="">
						</td>
					</tr>
					<tr>
						<th>新しいパスワード</th>
						<td class="form-group">
							<input type="password" name="password" value="">
						</td>
					</tr>
					<tr>
						<th>新しいパスワード(再入力)</th>
						<td class="form-group">
							<input type="password" name="password_confirmation" value="">
						</td>
					</tr>
				</tbody>
			</table>

			<div class="btn_list btn-popup btn-popup-confirm txt-ctr">
				<button type="button">パスワードを変更する</button>
			</div>
		</section>
	</div>
</form>
@endsection

@section('script')
<script>
	$(function(){
		$(document).on('click', '.btn-popup-confirm', function(){

			// 確認アラート
			var options = {
				text: "本当にパスワードを変更しますか？",
				icon: "info",
				buttons: {
					cancel: "キャンセル",
					ok: "OK"
				}
			};
			swal(options)
				.then(function(val) {
					if (val) {
						// OKの時の処理
						$('#form').submit();
					}
				});
		});
	});
</script>
@endsection
