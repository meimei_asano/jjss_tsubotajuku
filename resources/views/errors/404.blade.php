@extends('layouts.default')
@section('content')
  <section class="wht_bloc txt-ctr">
    <h2 class="heading-1"><i class='bx bx-error mgn-btm8'></i>ページが見つかりませんでした。</h2>
    <p class="mgn-btm40">アクセスしようとしたページは、削除されたかURLが変更されています。</p>
    <a href="{{ url('') }}" class="btn btn-lg txt-ctr">トップへ戻る</a>
    </div>
  </section>
@endsection
