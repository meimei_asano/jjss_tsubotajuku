<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/classroom/note/texts', 'Api\Classroom\Note\GetInfoController@texts');
Route::get('/classroom/note/text_units', 'Api\Classroom\Note\GetInfoController@text_units');

Route::put('/classroom/note/instruction/start/{student_note_row_id}', 'Api\Classroom\Note\InstructionController@start');
Route::put('/classroom/note/instruction/end/{student_note_row_id}', 'Api\Classroom\Note\InstructionController@end');
Route::put('/classroom/note/instruction/re_end/{student_note_row_id}', 'Api\Classroom\Note\InstructionController@end');
Route::put('/classroom/note/instruction/update_row_something/{student_note_row_id}', 'Api\Classroom\Note\InstructionController@update_row_something');

Route::put('/classroom/note/instruction/update_remarks', 'Api\Classroom\Note\InstructionController@update_remarks');

Route::put('/classroom/coming/instruction/start/{student_note_id}', 'Api\Classroom\Coming\InstructionController@start');
Route::put('/classroom/coming/instruction/end/{student_note_id}', 'Api\Classroom\Coming\InstructionController@end');
Route::put('/classroom/coming/instruction/update_something/{student_note_id}', 'Api\Classroom\Coming\InstructionController@update_something');

Route::put('/classroom/pif/save', 'Api\Classroom\Pif\SaveController');
Route::put('/classroom/pif/save_memo', 'Api\Classroom\Pif\SaveMemoController');
Route::put('/classroom/pif/list_memo', 'Api\Classroom\Pif\ListMemoController');
Route::put('/classroom/pif/delete_memo', 'Api\Classroom\Pif\DeleteMemoController');

Route::put('/plan/monthly/get_texts', 'Api\Plan\Monthly\GetInfoController@getTextList');
Route::put('/plan/monthly/get_student_monthlies', 'Api\Plan\Monthly\GetInfoController@getStudentMonthlies');
Route::put('/plan/monthly/get_from_note', 'Api\Plan\Monthly\GetInfoController@getFromNote');

Route::get('/master/student_attend_change_tsuika/get_unit_price', 'Api\Master\StudentAttendChangeTsuika\GetUnitPriceController');
Route::get('/master/student_attend_change_tsuika/get_date_type', 'Api\Master\StudentAttendChangeTsuika\GetDateTypeController');

Route::get('/master/student_attend_special_tsuika/get_base_hours', 'Api\Master\StudentAttendSpecialTsuika\GetBaseHoursController');

Route::get('/prestudent/mendan/get_gessya_price', 'Api\Prestudent\Mendan\GetGessyaPriceController');

Route::get('/master/student_change_course/get_course', 'Api\Master\StudentChangeCourse\GetCourseController');