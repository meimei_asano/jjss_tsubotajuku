<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// .envのホスト名を踏まえたURL生成を行う
if (isset($_SERVER['HTTP_X_FORWARDED_HOST'])) {
    URL::forceRootUrl(config('app.url'));
}

Route::get('/', function () {
    return redirect('/home');
});

Route::get('/welcome', function () {
    return view('welcome');
});

// スタイルガイド
Route::get('/style', function () {
    return view('style');
});
Route::get('/style_old', function () {
    return view('style_old');
});

//アンケート部分
Route::get('/right', function () {
    return view('/note/right');
});


Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');


// [TODO] ユーザー権限に応じてリダイレクト先調整
Route::get('/home', function () {
    return redirect('/classroom/coming');
});


// [ユーザー全て]
Route::group(['middleware' => ['auth', 'can:all']], function () {
    // 入退室生徒リスト
    Route::get('/classroom/coming/{school_id?}/{date?}', 'Classroom\Coming\ListController');
    Route::post('/classroom/coming/attend_change/{school_id?}/{date?}', 'Classroom\Coming\AttendChangeController');

    // 追加コマ・振替
    Route::get('/classroom/changing', 'Classroom\Changing\ListController');

    // 子別ノート
    Route::get('/classroom/note/{student_id}/{date?}', 'Classroom\Note\ListController');
    Route::get('/classroom/note/check/{student_id}/{date?}', 'Classroom\Note\CheckController');
    Route::post('/classroom/note/check/{student_note_id}', 'Classroom\Note\CheckController@check');
    Route::post('/classroom/note/recreate/{student_note_id}', 'Classroom\Note\CheckController@regenerate');
    Route::get('/classroom/note/create/{student_id}/{date}', 'Classroom\Note\CreateController@list');
    Route::post('/classroom/note/create/{student_id}/{date}', 'Classroom\Note\CreateController@generate');
    Route::post('/classroom/note/addrow/{student_note_id}', 'Classroom\Note\CreateController@addrow');
    Route::post('/classroom/note/delete_row/{student_note_row_id}', 'Classroom\Note\CreateController@delete_row');
    Route::get('/classroom/note/print/{student_id}/{date}', 'Classroom\Note\PrintController');
    Route::post('/classroom/note/print', 'Classroom\Note\PrintController@multi');
    // 承認待ち子別ノート一覧
    Route::get('/classroom/checking/{school_id?}/{date?}', 'Classroom\Checking\ListController');
    // 承認処理
    Route::post('/classroom/checking/check', 'Classroom\Checking\CheckController');

    // 生徒：一覧
    Route::get('/master/student', 'Master\Student\ListController');
    // 生徒：編集画面表示
    Route::get('/master/student/{student_id?}', 'Master\Student\DetailController');

    // 生徒 > PIF ：編集画面表示(表示のみ => マネージャー,本部スタッフ   編集可能 => 講師,校長,管理者)
    Route::get('/master/student_pif/{student_id}', 'Master\Student\PifDetailController');
    // 生徒 > PIF ：登録・更新
    Route::post('/master/student_pif', 'Master\Student\PifSaveController');

    // 生徒 > 請求関連項目 ：編集画面表示(表示のみ => マネージャー,校長,講師   編集可能 => 管理者,本部スタッフ)
    Route::get('/master/student_seikyu/{student_id}', 'Master\Student\SeikyuDetailController');
    // 生徒 > 請求関連項目 ：登録・更新
    Route::post('/master/student_seikyu', 'Master\Student\SeikyuSaveController');

    // 生徒：コースと通塾予定
    Route::get('/master/student_attend/{student_id}/{attend_base_id?}', 'Master\StudentAttend\IndexController');
    // 生徒：コースと通塾予定の登録・更新
    Route::post('/master/student_attend', 'Master\StudentAttend\SaveController');
    // 生徒：コース変更
    Route::get('/master/student_change_course', 'Master\StudentChangeCourse\ListController');
    // 生徒：コース変更
    Route::get('/master/student_change_course/{change_course_id}', 'Master\StudentChangeCourse\DetailController');
    // 生徒：コース変更
    Route::post('/master/student_change_course/{type}', 'Master\StudentChangeCourse\SaveController');
    // 生徒：コース変更
    Route::get('/master/student_change_course/print/{id}', 'Master\StudentChangeCourse\PrintController');
    // 生徒：コース変更
    Route::get('/master/student_change_course_unlimited/{change_course_id}', 'Master\StudentChangeCourse\DetailUnlimitedController');
    // 生徒：コース変更
    Route::post('/master/student_change_course_unlimited/{type}', 'Master\StudentChangeCourse\SaveUnlimitedController');
    // 生徒：追加コマ：削除
    Route::post('/master/student_change_course_delete', 'Master\StudentChangeCourse\DeleteController');
    // 生徒：追加コマ：一括削除
    Route::post('/master/student_change_course_ikkatsu_delete', 'Master\StudentChangeCourse\DeletesController');
    // 生徒：追加コマ：承認取消申請
    Route::post('/master/student_change_course_torikeshi_shinsei', 'Master\StudentChangeCourse\TorikeshiShinseiController');

    // 生徒：特別期間通塾予定
    Route::get('/master/student_attend_special/{student_id}/{attend_special_id?}', 'Master\StudentAttend\SpecialIndexController');
    // 生徒：コースと通塾予定の登録・更新
    Route::post('/master/student_attend_special', 'Master\StudentAttend\SpecialSaveController');
    // 生徒：追加コマ
    Route::get('/master/student_attend_change_tsuika', 'Master\StudentAttendChangeTsuika\ListController');
    // 生徒：追加コマ
    Route::get('/master/student_attend_change_tsuika/{change_course_id}', 'Master\StudentAttendChangeTsuika\DetailController');
    // 生徒：追加コマ
    Route::post('/master/student_attend_change_tsuika/{type}', 'Master\StudentAttendChangeTsuika\SaveController');
    Route::get('/master/student_attend_change_tsuika/print/{id}', 'Master\StudentAttendChangeTsuika\PrintController');
    // 生徒：追加コマ：削除
    Route::post('/master/student_attend_change_tsuika_delete', 'Master\StudentAttendChangeTsuika\DeleteController');
    // 生徒：追加コマ：一括削除
    Route::post('/master/student_attend_change_tsuika_ikkatsu_delete', 'Master\StudentAttendChangeTsuika\DeletesController');
    // 生徒：追加コマ：承認取消申請
    Route::post('/master/student_attend_change_tsuika_torikeshi_shinsei', 'Master\StudentAttendChangeTsuika\TorikeshiShinseiController');
    // 生徒：追加コマ：承認取消
    Route::post('/master/student_attend_change_tsuika_torikeshi', 'Master\StudentAttendChangeTsuika\TorikeshiController');


    // 生徒：教材受け渡し
    Route::get('/master/student_sold_text', 'Master\StudentSoldText\ListController');
    // 生徒：教材受け渡し
    Route::get('/master/student_sold_text/{id}', 'Master\StudentSoldText\DetailController');
    // 生徒：教材受け渡し
    Route::post('/master/student_sold_text/{type}', 'Master\StudentSoldText\SaveController');
    // 生徒：教材受け渡し：削除
    Route::post('/master/student_sold_text_delete', 'Master\StudentSoldText\DeleteController');
    // 生徒：教材受け渡し：一括削除
    Route::post('/master/student_sold_text_ikkatsu_delete', 'Master\StudentSoldText\DeletesController');
    // 生徒：教材受け渡し：承認取消申請
    Route::post('/master/student_sold_text_torikeshi_shinsei', 'Master\StudentSoldText\TorikeshiShinseiController');
    // 生徒：教材受け渡し：承認取消
    Route::post('/master/student_sold_text_torikeshi', 'Master\StudentSoldText\TorikeshiController');


    // 生徒：月間計画：一覧
    Route::get('/plan/monthly', 'Plan\Monthly\ListController');
    // 生徒：月間計画：詳細・編集画面
    Route::get('/plan/monthly/{id}', 'Plan\Monthly\DetailController');
    // 生徒：月間計画：新規登録画面1
    Route::get('/plan/monthly_add', 'Plan\Monthly\AddController');
    // 生徒：月間計画：新規登録画面2
    Route::post('/plan/monthly_create_plan', 'Plan\Monthly\CreatePlanController');
    // 生徒：月間計画：登録
    Route::post('/plan/monthly', 'Plan\Monthly\SaveController');
    // 生徒：月間計画：結果
    Route::get('/plan/monthly_result', 'Plan\Monthly\DetailResultController');
    // 生徒：月間計画：詳細 PDF出力
    Route::post('/plan/monthly_print_yotei', 'Plan\Monthly\PrintController');
    // 生徒：月間計画：詳細 PDF出力
    Route::post('/plan/monthly_print_result', 'Plan\Monthly\PrintResultController');


    // 生徒：教材受け渡し：印刷
    Route::get('/master/student_sold_text/print/{id}', 'Master\StudentSoldText\PrintController');

    // 生徒：特別講習
    Route::get('/master/student_attend_special_tsuika', 'Master\StudentAttendSpecialTsuika\ListController');
    // 生徒：特別講習
    Route::get('/master/student_attend_special_tsuika/print/{id}', 'Master\StudentAttendSpecialTsuika\PrintController');
    // 生徒：特別講習
    Route::get('/master/student_attend_special_tsuika/{id}', 'Master\StudentAttendSpecialTsuika\DetailController');
    // 生徒：特別講習
    Route::post('/master/student_attend_special_tsuika/{type}', 'Master\StudentAttendSpecialTsuika\SaveController');
///////////////////
    // 生徒：特別講習：削除
    Route::post('/master/student_attend_special_tsuika_delete', 'Master\StudentAttendSpecialTsuika\DeleteController');
    // 生徒：特別講習：一括削除
    Route::post('/master/student_attend_special_tsuika_ikkatsu_delete', 'Master\StudentAttendSpecialTsuika\DeletesController');
    // 生徒：特別講習：承認取消申請
    Route::post('/master/student_attend_special_tsuika_torikeshi_shinsei', 'Master\StudentAttendSpecialTsuika\TorikeshiShinseiController');
    // 生徒：特別講習：承認取消
    Route::post('/master/student_attend_special_tsuika_torikeshi', 'Master\StudentAttendSpecialTsuika\TorikeshiController');
///////////////////

    // 生徒：休塾、卒塾、退塾願
    Route::get('/master/student_request', 'Master\StudentRequest\ListController');
    // 生徒：休塾、卒塾、退塾願
    Route::get('/master/student_request/{id}', 'Master\StudentRequest\DetailController');
    // 生徒：休塾、卒塾、退塾願
    Route::post('/master/student_request/{type}', 'Master\StudentRequest\SaveController');
    // 生徒：休塾、卒塾、退塾願
    Route::get('/master/student_request/print/{id}', 'Master\StudentRequest\PrintController');
    // 生徒：休塾、卒塾、退塾願：削除
    Route::post('/master/student_request_delete', 'Master\StudentRequest\DeleteController');
    // 生徒：休塾、卒塾、退塾願：一括削除
    Route::post('/master/student_request_ikkatsu_delete', 'Master\StudentRequest\DeletesController');
    // 生徒：休塾、卒塾、退塾願：承認取消申請
    Route::post('/master/student_request_torikeshi_shinsei', 'Master\StudentRequest\TorikeshiShinseiController');
    // 生徒：休塾、卒塾、退塾願：承認取消
    Route::post('/master/student_request_torikeshi', 'Master\StudentRequest\TorikeshiController');

    // 生徒：判定面談
    Route::get('/prestudent/mendan', 'PreStudent\Mendan\ListController');
    // 生徒：判定面談
    Route::get('/prestudent/mendan/{id?}', 'PreStudent\Mendan\DetailController');
    // 生徒：判定面談
    Route::post('/prestudent/mendan/{type}', 'PreStudent\Mendan\SaveController');
    // 生徒：判定面談
    Route::get('/prestudent/mendan/print/{id}', 'PreStudent\Mendan\PrintController');
    // 生徒：判定面談：削除
    Route::post('/prestudent/mendan_delete', 'PreStudent\Mendan\DeleteController');
    // 生徒：判定面談：一括削除
    Route::post('/prestudent/mendan_ikkatsu_delete', 'PreStudent\Mendan\DeletesController');
    // 生徒：判定面談：承認取消申請
    Route::post('/prestudent/mendan_torikeshi_shinsei', 'PreStudent\Mendan\TorikeshiShinseiController');

    // for change course
    Route::get('/other/display/{type}', 'Other\DisplayController');

    //ログインユーザー：ログインパスワード変更
    Route::get('/mypage/password', 'Mypage\Password\IndexController');
    Route::post('/mypage/password/register', 'Mypage\Password\RegisterController');
});

// [校長以上]
Route::group(['middleware' => ['auth', 'can:head']], function () {
    // 生徒：登録・更新
    Route::post('/master/student', 'Master\Student\SaveController');

    // 生徒：契約コース変更
    Route::get('/master/student_course/{student_id}', 'Master\StudentCourse\ListController');
    // 生徒：契約コース登録・更新
    Route::post('/master/student_course', 'Master\StudentCourse\SaveController');

    //引落情報
    // ★サンプルのうちはget で、本開発では post を使う
    Route::get('/hq/hikiotoshi/print', 'HQ\Hikiotoshi\PrintController');
});

// [本部スタッフ以上]
Route::group(['middleware' => ['auth', 'can:hq']], function () {
    // 売上請求管理

    // 請求：一覧
    Route::get('/hq/invoice', 'HQ\Invoice\ListController');
    // 請求：PDF出力
    Route::get('/hq/invoice/print/{invoice_id}', 'HQ\Invoice\PrintController');
    Route::post('/hq/invoice/print', 'HQ\Invoice\PrintController@multi');
    // 請求：csv取込
    Route::post('/hq/invoice/import', 'HQ\Invoice\ImportController');

    // システム利用ログ
});

// [管理者のみ]
Route::group(['middleware' => ['auth', 'can:admin']], function () {
    // 生徒：CSV出力
    Route::get('/master/student_csv_export', 'Master\Student\CsvExportController');

    // 校舎：一覧
    Route::get('/master/school', 'Master\School\ListController');
    // 校舎：編集画面表示
    Route::get('/master/school/{school_id}', 'Master\School\DetailController');
    // 校舎：登録・更新
    Route::post('/master/school', 'Master\School\SaveController');

    // 先生：一覧
    Route::get('/master/teacher', 'Master\Teacher\ListController');
    // 先生：編集画面表示
    Route::get('/master/teacher/{teacher_id?}', 'Master\Teacher\DetailController');
    // 先生：登録・更新
    Route::post('/master/teacher', 'Master\Teacher\SaveController');

    // 教材：一覧
    Route::get('/master/text', 'Master\Text\ListController');
    // 教材：編集画面表示
    Route::get('/master/text/{text_id?}', 'Master\Text\DetailController');
    // 教材：登録
    Route::post('/master/text', 'Master\Text\SaveController');
    // 教材：削除
    Route::post('/master/text_delete', 'Master\Text\DeleteController');

    // 教材：学習順序表示
    Route::get('/master/text_sequence/{kamoku_id?}', 'Master\Text\SequenceIndexController');
    // 教材：学習順序登録
    Route::post('/master/text_sequence', 'Master\Text\SequenceSaveController');

    // 教材：範囲設定表示
    Route::get('/master/text_unit/{text_id}', 'Master\Text\UnitListController');
    // 教材：範囲設定の登録
    Route::post('/master/text_unit', 'Master\Text\UnitSaveController');

    // 講習期間：一覧
    Route::get('/master/term', 'Master\Term\ListController');
    // 講習期間：編集画面表示
    Route::get('/master/term/{term_id?}', 'Master\Term\DetailController');
    // 講習期間：登録・更新
    Route::post('/master/term', 'Master\Term\SaveController');
});
