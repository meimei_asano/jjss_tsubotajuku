<?php

namespace App\Notifications;

use App\Mail\CustomMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class CustomResetPassword extends Notification
{
    use Queueable;

    /**
     * The token generated.
     *
     * @var string
     */
    public $_token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->_token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \App\Mail\CustomMail
     */
    public function toMail($notifiable)
    {
        $url = url('/password/reset/' . $this->_token);

        return (new CustomMail)
            ->to($notifiable->email)
            ->subject(__('Reset Password'))
            ->text('_emails.password_reset', ['url' => $url]);
    }
}
