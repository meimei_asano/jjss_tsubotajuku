<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];

    //リレーション
    public function school()
    {
        return $this->belongsTo('App\Models\School');
    }

    public function student()
    {
        return $this->belongsTo('App\Models\Student');
    }

    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher', 'checked_teacher_id');
    }

    public function invoice_details()
    {
        return $this->hasMany('App\Models\InvoiceDetail');
    }
}
