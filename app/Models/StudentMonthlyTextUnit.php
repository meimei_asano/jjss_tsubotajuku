<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class StudentMonthlyTextUnit extends Model
{
//    use SoftDeletes;

    protected $guarded = ['id'];

//    protected $dates = ['deleted_at'];

    //リレーション
    public function student_monthly()
    {
        return $this->belongsTo('App\Models\StudentMonthly');
    }

    public function student_monthly_text()
    {
        return $this->belongsTo('App\Models\StudentMonthlyText');
    }

    public function kamoku()
    {
        return $this->belongsTo('App\Models\Kamoku');
    }

    public function text()
    {
        return $this->belongsTo('App\Models\Text');
    }
}
