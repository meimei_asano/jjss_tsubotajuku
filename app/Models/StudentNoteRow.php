<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentNoteRow extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];

    //リレーション
    public function student_note()
    {
        return $this->belongsTo('App\Models\StudentNote');
    }

    public function text()
    {
        return $this->belongsTo('App\Models\Text');
    }

    public function text_unit()
    {
        return $this->belongsTo('App\Models\TextUnit');
    }

    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher');
    }
}
