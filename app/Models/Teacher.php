<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $guarded = ['id', 'user_id'];

    //リレーション
    public function school()
    {
        return $this->belongsTo('App\Models\School', 'tantou_school_id');
    }

    public function students()
    {
        return $this->hasMany('App\Models\Student');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function teacher_kamokus()
    {
        return $this->hasMany('App\Models\TeacherKamoku');
    }

    public function student_pif_memos()
    {
        return $this->hasMany('App\Models\StudentPifMemo');
    }
}



