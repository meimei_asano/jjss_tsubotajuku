<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class School extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];

    //リレーション
    public function students()
    {
        return $this->hasMany('App\Models\Student');
    }

    public function teachers()
    {
        return $this->hasMany('App\Models\Teacher', 'tantou_school_id');
    }

    public function annual_schedules()
    {
        return $this->hasMany('App\Models\AnnualSchedule');
    }
}
