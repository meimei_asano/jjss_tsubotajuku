<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentNote extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];

    //リレーション
    public function student()
    {
        return $this->belongsTo('App\Models\Student');
    }

    public function student_note_rows()
    {
        return $this->hasMany('App\Models\StudentNoteRow');
    }
}
