<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kamoku extends Model
{
    protected $guarded = ['id'];

    //リレーション
    public function teacher_kamokus()
    {
        return $this->hasMany('App\Models\Teacher_kamoku');
    }

    public function texts()
    {
        return $this->hasMany('App\Models\Text');
    }

    public function student_pif_memos()
    {
        return $this->hasMany('App\Models\StudentPifMemo');
    }

}
