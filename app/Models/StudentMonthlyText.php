<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentMonthlyText extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];

    //リレーション
    public function student_monthly()
    {
        return $this->belongsTo('App\Models\StudentMonthly');
    }

    public function student_monthly_text_units()
    {
        return $this->hasMany('App\Models\StudentMonthlyTextUnit')->orderBy('text_unit_num', 'asc');
    }

    public function kamoku()
    {
        return $this->belongsTo('App\Models\Kamoku');
    }

    public function text()
    {
        return $this->belongsTo('App\Models\Text');
    }

}
