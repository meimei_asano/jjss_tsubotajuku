<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];

    //リレーション
    public function school()
    {
        return $this->belongsTo('App\Models\School');
    }

    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher');
    }

    public function shibou()
    {
        // 本来hasMany
        return $this->hasOne('App\Models\StudentShibou');
    }

    public function kamokus()
    {
        return $this->hasMany('App\Models\StudentKamoku');
    }

    public function ninetype()
    {
        return $this->hasOne('App\Models\StudentNinetype');
    }

    public function attend_bases()
    {
        return $this->hasMany('App\Models\StudentAttendBase')->orderBy('start_date', 'desc');
    }

    public function attend_specials()
    {
        return $this->hasMany('App\Models\StudentAttendSpecial');
    }

    public function attend_changes()
    {
        return $this->hasMany('App\Models\StudentAttendChange');
    }

    public function text_units_by_students()
    {
        return $this->hasMany('App\Models\TextUnitsByStudent');
    }

    public function courses_today()
    {
        return $this->hasManyThrough('App\Models\Course', 'App\Models\StudentAttendBase', 'student_id', 'id', 'id', 'course_id')
            ->where('student_attend_bases.start_date', '<=', date('Y-m-d'))
            ->where(function($q){
                $q->whereNull('student_attend_bases.end_date')
                    ->orWhere('student_attend_bases.end_date', '>=', date('Y-m-d'));
            });
    }

    public function courses_current()
    {
        return $this->hasMany('App\Models\StudentAttendBase')
            ->where('student_attend_bases.start_date', '<=', date('Y-m-d'))
            ->where(function($q){
                $q->whereNull('student_attend_bases.end_date')
                    ->orWhere('student_attend_bases.end_date', '>=', date('Y-m-d'));
            });
    }

    public function student_pif()
    {
        return $this->hasOne('App\Models\StudentPif');
    }

    public function student_pif_memos()
    {
        return $this->hasMany('App\Models\StudentPifMemo')->orderBy('id', 'desc');
    }
}



