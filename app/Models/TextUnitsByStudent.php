<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TextUnitsByStudent extends Model
{
    protected $guarded = ['id'];

    //リレーション
    public function text()
    {
        return $this->belongsTo('App\Models\Text');
    }

    public function student()
    {
        return $this->belongsTo('App\Models\Student');
    }
}
