<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentMonthly extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];

    //リレーション
    public function student()
    {
        return $this->belongsTo('App\Models\Student');
    }

    public function term()
    {
        return $this->belongsTo('App\Models\Term');
    }

    public function student_monthly_texts()
    {
        return $this->hasMany('App\Models\StudentMonthlyText')->orderBy('kamoku_sequence', 'asc');
    }
}
