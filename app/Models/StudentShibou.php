<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentShibou extends Model
{
    protected $guarded = ['id'];

    //リレーション
    public function student()
    {
        return $this->belongsTo('App\Models\Student');
    }

}



