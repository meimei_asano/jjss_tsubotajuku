<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentKyuujyuku extends Model
{
    use SoftDeletes;

    /**
     * Guarded columns
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Date columns
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Association to students table
     *
     * @return $this
     */
    public function student()
    {
        return $this->belongsTo('App\Models\Student');
    }

}