<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];

    //リレーション
    public function price()
    {
        return $this->hasMany('App\Models\Price');
    }

    public function student_attend_base()
    {
        return $this->hasOne('App\Models\StudentAttendBase');
    }
}
