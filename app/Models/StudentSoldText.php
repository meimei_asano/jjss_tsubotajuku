<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentSoldText extends Model
{
    use SoftDeletes;

    /**
     * Guarded columns
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Date columns
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Association to students table
     *
     * @return $this
     */
    public function student()
    {
        return $this->belongsTo('App\Models\Student', 'student_id');
    }

    /**
     * Association to schools table through students table
     *
     * @return $this
     */
    public function student_school()
    {
        //inversed
        return $this->hasManyThrough(
            'App\Models\School',
            'App\Models\Student',
            'id',
            'id',
            'student_id',
            'school_id'
        );
    }

    /**
     * Association to teachers table
     *
     * @return $this
     */
    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher', 'teacher_id');
    }

    /**
     * Association to teachers table
     *
     * @return $this
     */
    public function syounin_teacher()
    {
        return $this->belongsTo('App\Models\Teacher', 'syounin_teacher_id');
    }
}
