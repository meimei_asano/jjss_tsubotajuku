<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Text extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];

    //リレーション
    public function kamoku()
    {
        return $this->belongsTo('App\Models\Kamoku');
    }

    public function text_units()
    {
        return $this->hasMany('App\Models\TextUnit');
    }

    public function text_units_by_students()
    {
        return $this->hasMany('App\Models\TextUnitsByStudent');
    }
}
