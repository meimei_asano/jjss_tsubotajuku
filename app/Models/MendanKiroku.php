<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MendanKiroku extends Model
{
    use SoftDeletes;

    /**
     * Guarded columns
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Date columns
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Association to schools table
     *
     * @return $this
     */
    public function school()
    {
        return $this->belongsTo('App\Models\School');
    }

    /**
     * Association to teachers table
     *
     * @return $this
     */
    public function mendan_teacher()
    {
        return $this->belongsTo('App\Models\Teacher', 'mendan_teacher_id');
    }

    /**
     * Association to teachers table
     *
     * @return $this
     */
    public function manager_teacher()
    {
        return $this->belongsTo('App\Models\Teacher', 'manager_teacher_id');
    }

    /**
     * Association to courses table
     *
     * @return $this
     */
    public function course()
    {
        return $this->belongsTo('App\Models\Course');
    }
}
