<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentKamoku extends Model
{
    protected $guarded = ['id'];

    //リレーション
    public function student()
    {
        return $this->belongsTo('App\Models\Student');
    }

    public function kamoku()
    {
        return $this->belongsTo('App\Models\Kamoku');
    }
}



