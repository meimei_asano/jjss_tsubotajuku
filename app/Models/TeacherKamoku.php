<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeacherKamoku extends Model
{
    protected $guarded = ['id'];

    //リレーション
    public function kamoku()
    {
        return $this->belongsTo('App\Models\kamoku');
    }

    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher');
    }

}



