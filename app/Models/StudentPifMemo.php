<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StudentPifMemo extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    protected $dates = ['deleted_at'];

    //リレーション
    public function student()
    {
        return $this->belongsTo('App\Models\Student');
    }

    public function teacher()
    {
        return $this->belongsTo('App\Models\Teacher');
    }

    public function kamoku()
    {
        return $this->belongsTo('App\Models\Kamoku');
    }
}



