<?php

namespace App\Helpers;

class CustomMpdf
{
    /**
     * Generate PDF form(s)
     *
     * @param  mixed $contents
     * @param  array $config
     * @param  string $title
     * @return void
     */
    public function generate($contents, array $config = [], string $title = '自学自習システム')
    {
        // set default config
        $default = [
            'fontdata' => [
                'ipa' => [
                    'R' => 'ipag.ttf'
                ]
            ],
        ];

        // combine default and custom config
        $config = array_merge($default, $config);

        $mpdf = new \Mpdf\Mpdf($config);
        $mpdf->setTitle($title);

        // check if contents is in array format
        if (is_array($contents)) {
            foreach ($contents as $key => $page) {
                // check if not first page
                if ($key !== 0) {
                    $mpdf->AddPage();
                }
                $mpdf->WriteHTML($page);
            }
        } else {
            $mpdf->WriteHTML($contents);
        }

        $mpdf->Output();
    }

}
