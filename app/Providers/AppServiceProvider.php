<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use Hash;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Validator拡張
        Validator::extend('old_password', function($attribute, $value, $parameters, $validator) {
            return Hash::check($value, current($parameters));
        });
        Validator::extend('multi_date_format', function($attribute, $value, $formats) {
            foreach($formats as $format) {
                $parsed = date_parse_from_format($format, $value);
                if ($parsed['error_count'] === 0 && $parsed['warning_count'] === 0) {
                    return true;
                }
            }
            return false;
        });
        Validator::extend('greater_than', function($attribute, $value, $parameters, $validator) {
            $smallerValue = array_get($validator->getData(), $parameters[0]);
            return intval($value) > intval($smallerValue);
        });
        Validator::replacer('greater_than', function ($message, $attribute, $rule, $parameters) {
            $biggerAttribute = '';
            $smallerAttribute = '';
            switch ($attribute) {
                case 'total_hours':
                    $biggerAttribute = '総合計申し込み時間数';
                    break;
            }
            switch ($parameters[0]) {
                case 'base_hours':
                    $smallerAttribute = '期間中通常授業時間数';
                    break;
            }

            return $biggerAttribute . 'は' . $smallerAttribute . 'より大きくなければなりません。';
        });
        Validator::extend('time_range', function($attribute, $value, $ranges) {
            $min = Carbon::parse(($ranges[0]) ?? '16:00');  // 通常期間の指導開始時間
            $max = Carbon::parse(($ranges[1]) ?? '21:40');  // 通常期間の指導終了時間
            $v = Carbon::parse($value);
            if ($v < $min || $v > $max) {
                return false;
            }
            return true;
        });
        Validator::replacer('time_range', function($message, $attribute, $rule, $ranges) {
            $attr_name = \Lang::get('validation.attributes.'. $attribute);
            $min = Carbon::parse(($ranges[0]) ?? '16:00');  // 通常期間の指導開始時間
            $max = Carbon::parse(($ranges[1]) ?? '21:40');  // 通常期間の指導終了時間
            return $attr_name. 'は'. $min->format('H:i'). '〜'. $max->format('H:i'). 'の範囲で入力してください。';
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
