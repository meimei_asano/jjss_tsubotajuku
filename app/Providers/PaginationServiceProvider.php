<?php

namespace App\Providers;

//use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\PaginationServiceProvider as ServiceProvider;
use Illuminate\Pagination\Paginator;


class PaginationServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        Paginator::viewFactoryResolver(function () {
            return $this->app['view'];
        });

        Paginator::currentPathResolver(function () {
            return url($this->app['request']->getPathInfo());
        });

        Paginator::currentPageResolver(function ($pageName = 'page') {
            $page = $this->app['request']->input($pageName);

            if (filter_var($page, FILTER_VALIDATE_INT) !== false && (int) $page >= 1) {
                return (int) $page;
            }

            return 1;
        });
    }
}
