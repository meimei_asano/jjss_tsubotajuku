<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // 認可タイプを定義する
        // [ユーザー全て]
        Gate::define('all', function($user) {
            return ($user->teacher->permission == '講師')
                || ($user->teacher->permission == '校長')
                || ($user->teacher->permission == 'マネージャー')
                || ($user->teacher->permission == '本部スタッフ')
                || ($user->teacher->permission == '管理者');
        });
        // [校長以上]
        Gate::define('head', function($user) {
            return ($user->teacher->permission == '校長')
                || ($user->teacher->permission == 'マネージャー')
                || ($user->teacher->permission == '本部スタッフ')
                || ($user->teacher->permission == '管理者');
        });
        // [本部スタッフ以上]
        Gate::define('hq', function($user) {
            return ($user->teacher->permission == '本部スタッフ')
                || ($user->teacher->permission == '管理者');
        });
        // [管理者のみ]
        Gate::define('admin', function($user) {
            return ($user->teacher->permission == '管理者');
        });
    }
}
