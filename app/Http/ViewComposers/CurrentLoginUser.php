<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Teacher;

class CurrentLoginUser
{
    protected $login_user;

    public function __construct()
    {
        $this->login_user = \Auth::user();
    }

   public function compose(View $view)
   {
       $name = '';
       $permission = '';
       if ($this->login_user) {
           $teacher = Teacher::where('user_id', $this->login_user->id)->first();
           $name = $teacher->name;
           $permission = $teacher->permission;
       }

       $view->with([
           'login_name' => $name,
           'login_permission' => $permission,
       ]);
   }
}
