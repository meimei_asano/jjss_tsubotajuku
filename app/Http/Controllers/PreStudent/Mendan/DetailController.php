<?php

namespace App\Http\Controllers\PreStudent\Mendan;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\Kamoku;
use App\Models\MendanKiroku;
use App\Models\School;
use App\Models\Teacher;

class DetailController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  mixed $id
     * @return view
     */
    public function __invoke($id = 'new')
    {
        // check if $id is integer or numeric
        if (is_numeric($id)) {
            $mendanKiroku = MendanKiroku::find($id);
            if (!$mendanKiroku) {
                return redirect('/prestudent/mendan');
            }
        } elseif ($id === 'new') { // check if $id is new
            $mendanKiroku = new MendanKiroku;
            $mendanKiroku->seikyu_data = '[{"title":"①入会金","price":"30000"},{"title":"②学力テスト＋コンサルフィー","price":"30000"},{"title":"③設備費（月額）","price":"2000"}]';
        } else {
            return redirect('/prestudent/mendan');
        }

        $courses = Course::all();
        $kamokus = Kamoku::all();
        $schools = School::all();
        if ($mendanKiroku->mendan_teacher_id) {
            $mendan_teacher_name = Teacher::find($mendanKiroku->mendan_teacher_id)->name;
        } else {
            $mendan_teacher_name = \Auth::user()->name;
        }
        $koucyous = Teacher::where('id', '!=', 0)->where('permission', '校長')->get()->sortBy('user_id');
        $teacher = \Auth::user()->teacher;
        $permission = $teacher->permission;
        $seikyu_total = json_decode($mendanKiroku->seikyu_total);

        return view('prestudent.mendan.detail', compact('id', 'mendanKiroku', 'seikyu_total', 'schools', 'mendan_teacher_name', 'koucyous', 'kamokus', 'courses', 'permission'));
    }
}
