<?php

namespace App\Http\Controllers\Prestudent\Mendan;

use App\Http\Controllers\Controller;
use App\Validation\MendanKirokuDeletesValidation as Validation;
use Illuminate\Http\Request;
use App\Models\MendanKiroku;
use DB;

class DeletesController extends Controller
{
    public function __invoke(Request $request)
    {
        //$request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            //ここでエラーになるのはIDが存在しない場合のみなので新規登録画面へリダイレクトする
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $ids = $request->input('ids');
            $id_list = explode(',', $ids);
            foreach($id_list as $k => $id) {
                $mendan_kiroku = MendanKiroku::find($id);
                $mendan_kiroku->delete();
            }

            DB::commit();

            $request->session()->flash('alert.success', '申請取消しました。');
            return redirect('/prestudent/mendan');

        } catch (\PDOException $e){
            DB::rollBack();
            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }
}
