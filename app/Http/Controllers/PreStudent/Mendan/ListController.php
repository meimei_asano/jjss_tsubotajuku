<?php

namespace App\Http\Controllers\PreStudent\Mendan;

use App\Http\Controllers\Controller;
use App\Models\MendanKiroku;
use App\Models\School;
use App\Models\Teacher;
use Illuminate\Http\Request;

class ListController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  Illuminate\Http\Request $request
     * @return view
     */
    public function __invoke(Request $request)
    {
        $search = [];
        $orderby = [];
        $searchName = $request->input('search_name') ?: null;
        $searchTeacher = $request->input('teacher_id') ?: null;
        $searchMendanDate = $request->input('mendan_date') ?: null;
        $schoolId = $request->input('school_id') ?: null;
        $processFinishedFlag  = $request->input('process_finished_flag') ?: null;
        $teacher = \Auth::user()->teacher;
        $permission = $teacher->permission;

        // restrict user school option or query if 講師 or 校長
        if (in_array($permission, ['講師', '校長'])) {
            $schoolId = $teacher->tantou_school_id;
            $schools = School::where('id', $schoolId)->get()->toArray();
        } else {
            $schools = School::all()->toArray();
        }

        $query = MendanKiroku::select('*', 'mendan_kirokus.id as id', 'mendan_kirokus.name as name', 'mendan_teachers.name as mendan_teacher_name')->with([
                'school',
                'mendan_teacher',
            ]);
        $query->leftJoin('teachers AS mendan_teachers', 'mendan_kirokus.mendan_teacher_id', '=', 'mendan_teachers.id');

        // search by name
        if ($searchName) {
            $search['search_name'] = $searchName;
            $query->where('mendan_kirokus.name', 'like', "%{$searchName}%");
        }

        // search by teacher
        if ($searchTeacher) {
            $search['teacher_id'] = $searchTeacher;
            $query->where('mendan_teacher_id', '=', $searchTeacher);
        }

        // search by mendan date
        if ($searchMendanDate) {
            $mendanDate = \JJSS::getTargetDate($searchMendanDate);
            $search['mendan_date'] = $mendanDate;
            $query->where('mendan_date', '=', $mendanDate);
        }

        // search by school id
        if ($schoolId) {
            $search['school_id'] = $schoolId;
            $query->where('school_id', '=', $schoolId);
        }

        // search by process_finished_flag
        if ($processFinishedFlag) {
            $search['process_finished_flag'] = $processFinishedFlag;
            $query->where('process_finished_flag', 1);
        } else {
            $query->whereIn('process_finished_flag', [-1, 0]);
        }

        //sort
        if ($request->input('orderby_column_name')) {
            $orderby['column_name'] = $request->input('orderby_column_name');
            $orderby['type'] = $request->input('orderby_type');
            $query->orderBy($orderby['column_name'], $orderby['type']);
        }

        $mendanKirokus = $query->paginate(config('paginate.items.mendan'));
        //$mendanKirokus = $query->get();

        $teachers = Teacher::where('id', '!=', 0)->get()->toArray();
        $permission = \Auth::user()->teacher->permission;

        return view('prestudent.mendan.list',
                    compact(
                        'mendanKirokus',
                        'schools',
                        'teachers',
                        'teacher',
                        'permission',
                        'search',
                        'orderby'
                    )
        );
    }
}
