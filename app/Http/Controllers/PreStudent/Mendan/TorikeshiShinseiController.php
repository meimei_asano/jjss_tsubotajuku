<?php

namespace App\Http\Controllers\PreStudent\Mendan;

use App\Http\Controllers\Controller;
use App\Validation\MendanKirokuTorikeshiShinseiValidation as Validation;
use Illuminate\Http\Request;
use App\Models\MendanKiroku;
use DB;

class TorikeshiShinseiController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $id = $request->input('id');

            if ($id) {
                $mendanKiroku = MendanKiroku::find($id);
                $mendanKiroku->syounin_torikeshi_shinsei_flag = 1;//承認取消申請！
                $mendanKiroku->save();
           }

            DB::commit();

            $request->session()->flash('alert.success', '申請しました。');
            return redirect('/prestudent/mendan');
        } catch (\PDOException $e){
            DB::rollBack();
            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }
}
