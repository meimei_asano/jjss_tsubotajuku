<?php

namespace App\Http\Controllers\PreStudent\Mendan;

use App\Http\Controllers\Controller;
use App\Models\MendanKiroku;
use App\Models\Student;
use App\Models\StudentAttendBase;
use App\Models\StudentKamoku;
use App\Validation\MendanKirokuValidation as Validation;
use DB;
use Illuminate\Http\Request;

class SaveController extends Controller
{
    public function __invoke(Request $request, string $type)
    {
        if (in_array($type, ['syounin', 'cancel_approval'])) {
            $teacher = \Auth::user()->teacher;
            $permission = $teacher->permission;

            if (!in_array($permission, ['校長', 'マネージャー', '管理者'])) {
                $request->flash();

                return \Redirect::back()->withErrors(['このリクエストを実行する権限がありません。']);
            }
        } elseif ($type !== 'temp' && $type !== 'save' && $type !== 'print') {
            return \Redirect::back()->withErrors(['このリクエストを実行する権限がありません。']);
        }

        $result = [];
        $flashMessage = '';
        $requestData = $request->all();

        unset($requestData['birth_date_y']);
        unset($requestData['birth_date_m']);
        unset($requestData['birth_date_d']);

        $requestData['kamoku_ids'] = $requestData['kamoku_ids'] ?? [];
        $requestData['unlimited_discount_flag'] = $requestData['unlimited_discount_flag'] ?? null;
        $courseType = \JJSS::getCourseType(intval($requestData['course_id']));

        if ($courseType) {
            array_walk($requestData , function (&$item, $key) use ($courseType) {
                if ($courseType === 'unlimited' && (strpos($key, 'start_time') || strpos($key, 'end_time'))) {
                    $item = null;
                } elseif ($courseType === 'course' && in_array($key, ['unlimited_start_date', 'unlimited_end_date', 'unlimited_discount_flag'])) {
                    $item = null;
                }
            });
        } else {
            array_walk($requestData , function(&$item, $key){
                if (strpos($key, 'start_time') || strpos($key, 'end_time') || in_array($key, ['unlimited_start_date', 'unlimited_end_date', 'unlimited_discount_flag'])) {
                    $item = null;
                }
            });
        }

        if ($type !== 'temp') {
            $hasErrors = Validation::validate($requestData);

            if ($hasErrors) {
                $request->flash();
                return \Redirect::back()->withErrors($hasErrors);
            }
        }

        if ($type === 'temp') {
            $result = $this->saveData($requestData, true);
            $flashMessage = '一時保存しました。';
        } elseif ($type === 'save' || $type === 'print') {
            $result = $this->saveData($requestData);
            $flashMessage = '登録しました。';
        } elseif ($type === 'syounin') {
            $result = $this->syounin($requestData);
            $flashMessage = '承認済み。';
        } elseif ($type === 'cancel_approval') {
            $result = $this->cancelApproval($requestData);
            $flashMessage = '承認は取り消されました。';
        }

        if ($result['error'] === 0) {
            $mendanKiroku = $result['mendanKiroku'];
            $request->session()->flash('alert.success', $flashMessage);
            if ($type === 'print') $request->session()->flash('print', true);

            return redirect('/prestudent/mendan/' . $mendanKiroku->id);
        } else {
            $request->flash();

            return \Redirect::back()->withErrors($result['messages']);
        }
    }

    /**
     * Save information
     *
     * @param  array $requestData
     * @param  boolean $temporary
     * @return array $result
     */
    private function saveData(array $requestData, $temporary = false)
    {
        if ($temporary) {  // 一時保存対応
            $requestData['school_id'] = $requestData['school_id'] ?: 0;
            $requestData['mendan_date'] = $requestData['mendan_date'] ?: null;
            $requestData['manager_teacher_id'] = $requestData['manager_teacher_id'] ?: 0;
            $date = explode('-', $requestData['birth_date']);
            if (count($date) != 3 || ($date[0] == 0 || $date[1] == 0 || $date[2] == 0) || !checkdate($date[1], $date[2], $date[0])) {
                $requestData['birth_date'] = null;
            }
            $requestData['course_id'] = $requestData['course_id'] ?: 0;
        }

        if ($requestData['course_id'] == \JJSS::getNoneCourse()) {
            // コースなし対応
            $requestData['mon_start_time'] = NULL; $requestData['mon_end_time'] = NULL;
            $requestData['tue_start_time'] = NULL; $requestData['tue_end_time'] = NULL;
            $requestData['wed_start_time'] = NULL; $requestData['wed_end_time'] = NULL;
            $requestData['thu_start_time'] = NULL; $requestData['thu_end_time'] = NULL;
            $requestData['fri_start_time'] = NULL; $requestData['fri_end_time'] = NULL;
        }

        $result = ['error' => 0];
        DB::beginTransaction();
        try {
            array_walk($requestData , function(&$item, $key){
                if (strpos($key, 'start_time') || strpos($key, 'end_time')) {
                    $item = ($item) ? $item . ':00' : null;
                }
                if (in_array($key, ['first_shidou_date', 'unlimited_start_date', 'unlimited_end_date']) && !$item) {
                    $item = null;
                }
                if ($key === 'kamoku_ids') {
                    $item = implode(',', $item);
                }
            });

            //$seikyuBreakdown = array_filter($requestData['seikyu_breakdown']);
            $seikyuBreakdown = $requestData['seikyu_breakdown'];
            unset($requestData['seikyu_breakdown']);
            //$seikyuAmount = array_filter($requestData['seikyu_amount'], function($v, $k){ return ($v !== ''); }, ARRAY_FILTER_USE_BOTH);
            $seikyuAmount = $requestData['seikyu_amount'];
            unset($requestData['seikyu_amount']);
            $seikyuCount = count($seikyuBreakdown);
            $seikyuData = [];

            for ($i=0; $i < $seikyuCount; $i++) {
                $seikyuData[] = [
                    'title' => $seikyuBreakdown[$i],
                    'price' => $seikyuAmount[$i],
                ];
            }
            $requestData['seikyu_data'] = json_encode($seikyuData, JSON_UNESCAPED_UNICODE);

            $seikyuTotal = [];
            $seikyuTotal['subtotal'] = $requestData['subtotal'];
            $seikyuTotal['tax'] = $requestData['tax'];
            $seikyuTotal['total'] = $requestData['total'];
            unset($requestData['subtotal']);
            unset($requestData['tax']);
            unset($requestData['total']);
            $requestData['seikyu_total'] = json_encode($seikyuTotal, JSON_UNESCAPED_UNICODE);

            $requestData['orientation_datetime'] = ($requestData['orientation_date'] && $requestData['orientation_time'])
                ? $requestData['orientation_date'] . ' ' . $requestData['orientation_time'] . ':00'
                : null;
            unset($requestData['orientation_date']);
            unset($requestData['orientation_time']);

            $id = $requestData['id'];
            unset($requestData['id']);

            // 一時保存、申請
            $process_finished_flag = ($temporary) ? -1 : 0;  // -1:一時保存、0:申請、1:承認

            if ($id) {
                $mendanKiroku = MendanKiroku::find($id);
                $mendanKiroku->process_finished_flag = $process_finished_flag;
                $mendanKiroku->update($requestData);
            } else {
                $mendanKiroku = new MendanKiroku;
                $teacher = \Auth::user()->teacher;
                $requestData['mendan_teacher_id'] = $teacher->id;
                $requestData['teacher_id'] = $teacher->id;
                $requestData['process_finished_flag'] = $process_finished_flag;
                $mendanKiroku->fill($requestData)->save();
            }

            DB::commit();
            $result['mendanKiroku'] = $mendanKiroku;
        } catch (\PDOException $e){
            DB::rollBack();
            $result['error'] = 1;
            $result['messages'] = [$e->getMessage()];
        }

        return $result;
    }

    /**
     * Update approval
     *
     * @param  array $requestData
     * @return array $result
     */
    private function syounin(array $requestData)
    {
        $result = ['error' => 0];
        $id = $requestData['id'];
        $mendanKiroku = MendanKiroku::find($id);

        if ($mendanKiroku->syounin_teacher_id !== null) {
            $result['error'] = 1;
            $result['messages'] = ['すでに承認されています。'];
        } else {
            DB::beginTransaction();
            try {
                // students 生徒マスタへの登録
                $student_code = \JJSS::getNewStudentCode($mendanKiroku->school_id, $mendanKiroku->first_shidou_date);
                $student = new Student;
                $studentData = [
                    'student_code' => $student_code,
                    'name' => $mendanKiroku->name,
                    'kana' => $mendanKiroku->kana,
                    'school_id' => $mendanKiroku->school_id,
                    'teacher_id' => null,
                    'gakkou_name' => $mendanKiroku->gakkou_name,
                    'grade_code' => $mendanKiroku->grade_code,
                    'nyujyuku_date' => $mendanKiroku->first_shidou_date,
                    'birth_date' => $mendanKiroku->birth_date,
                    'zip_code' => $mendanKiroku->zip_code,
                    'pref' => $mendanKiroku->pref,
                    'address1' => $mendanKiroku->address1,
                    'address2' => $mendanKiroku->address2,
                    'home_tel' => $mendanKiroku->hogosya_home_tel,
                    'student_tel' => $mendanKiroku->tel,
                    'hogosya_name' => $mendanKiroku->hogosya_name,
                    'hogosya_kana' => $mendanKiroku->hogosya_kana,
                    'hogosya_tel' => $mendanKiroku->hogosya_tel,
                    'hogosya_email' => $mendanKiroku->hogosya_email,
                ];
                $student->fill($studentData);
                $student->save();

                // student_attend_bases
                $isUnlimited = ($mendanKiroku->course_id == \JJSS::getUnlimitedCourse());
                $studentAttendBase = new StudentAttendBase;
                $studentAttendBaseData = [
                    'student_id' => $student->id,
                    'start_date' => $mendanKiroku->first_shidou_date,
                    'end_date' => $mendanKiroku->unlimited_end_date ?? null,  // 無制限コースの場合のみセットされる
                    'course_id' => $mendanKiroku->course_id,
                    'mon_start_time' => ($isUnlimited) ? '16:00:00' : $mendanKiroku->mon_start_time,
                    'mon_end_time' => ($isUnlimited) ? '21:40:00' : $mendanKiroku->mon_end_time,
                    'tue_start_time' => ($isUnlimited) ? '16:00:00' : $mendanKiroku->tue_start_time,
                    'tue_end_time' => ($isUnlimited) ? '21:40:00' : $mendanKiroku->tue_end_time,
                    'wed_start_time' => ($isUnlimited) ? '16:00:00' : $mendanKiroku->wed_start_time,
                    'wed_end_time' => ($isUnlimited) ? '21:40:00' : $mendanKiroku->wed_end_time,
                    'thu_start_time' => ($isUnlimited) ? '16:00:00' : $mendanKiroku->thu_start_time,
                    'thu_end_time' => ($isUnlimited) ? '21:40:00' : $mendanKiroku->thu_end_time,
                    'fri_start_time' => ($isUnlimited) ? '16:00:00' : $mendanKiroku->fri_start_time,
                    'fri_end_time' => ($isUnlimited) ? '21:40:00' : $mendanKiroku->fri_end_time,
                ];
                $studentAttendBase->fill($studentAttendBaseData);
                $studentAttendBase->save();

                // student_kamokus
                foreach (explode(',', $mendanKiroku->kamoku_ids) as $kamoku_id) {
                    $studentKamoku = new StudentKamoku;
                    $studentKamokuData = [
                        'student_id' => $student->id,
                        'kamoku_id' => $kamoku_id,
                    ];
                    $studentKamoku->fill($studentKamokuData);
                    $studentKamoku->save();
                }

                //save current logged in user id in syounin_teacher_id
                $teacher = \Auth::user()->teacher;
                $mendanKiroku->update([
                    'process_finished_flag' => 1,
                    'syounin_teacher_id' => $teacher->id,
                    'created_student_id' => $student->id
                ]);

                DB::commit();
                $result['mendanKiroku'] = $mendanKiroku;
            } catch (Exception $e) {
                DB::rollBack();
                $result['error'] = 1;
                $result['messages'] = [$e->getMessage()];
            }
        }

        return $result;
    }

    /**
     * Cancel approval
     *
     * @param  array $requestData
     * @return array $result
     */
    private function cancelApproval(array $requestData)
    {
        $result = ['error' => 0];
        $id = $requestData['id'];
        $mendanKiroku = MendanKiroku::find($id);

        // TODO: 承認取消可能な条件は？消去対象のリレーションテーブルの範囲は？

        if ($mendanKiroku->syounin_teacher_id === null) {
            $result['error'] = 1;
            $result['messages'] = ['まだ承認されていません。'];
        } elseif ($mendanKiroku->created_student_id === null) {
            $result['error'] = 1;
            $result['messages'] = ['生成された生徒データが特定できません。'];
        } else {
            DB::beginTransaction();
            try {
                // soft delete $student
                $student = Student::find($mendanKiroku->created_student_id);
                $student_id = $student->id;
                $student->student_code = NULL;
                $student->save();
                $student->delete();
                StudentAttendBase::where('student_id', $student_id)->delete();
                StudentKamoku::where('student_id', $student_id)->delete();

                // revert syounin_teacher_id back to null
                $mendanKiroku->update([
                    'process_finished_flag' => 0,
                    'syounin_teacher_id' => null,
                    'created_student_id' => null,
                    'syounin_torikeshi_shinsei_flag' => 0,
                ]);
                DB::commit();
                $result['mendanKiroku'] = $mendanKiroku;
            } catch (Exception $e) {
                DB::rollBack();
                $result['error'] = 1;
                $result['messages'] = [$e->getMessage()];
            }
        }

        return $result;
    }
}
