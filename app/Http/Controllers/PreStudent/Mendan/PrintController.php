<?php

namespace App\Http\Controllers\PreStudent\Mendan;

use App\Facades\CustomMpdf;
use App\Http\Controllers\Controller;
use App\Models\MendanKiroku;

class PrintController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  integer  $id
     * @return void
     */
    public function __invoke(int $id)
    {
        $mendanKiroku = MendanKiroku::with([
                'school',
                'mendan_teacher',
                'manager_teacher',
                'course',
            ])->find($id);

        // check if $mendanKiroku is existing
        if (!$mendanKiroku) {
            return redirect('/prestudent/mendan');
        }

        // check type of PDF form
        $type = $mendanKiroku->course->is_unlimited === 1 ? 'unlimited' : 'course';

        $pdfPages = [
            view('prestudent.mendan.main', [
                'type' => $type,
                'intendedFor' => '本社保管用',
                'mendanKiroku' => $mendanKiroku,
            ]),
            view('prestudent.mendan.main', [
                'type' => $type,
                'intendedFor' => '校舎保管用',
                'mendanKiroku' => $mendanKiroku,
            ]),
            view('prestudent.mendan.main', [
                'type' => $type,
                'intendedFor' => '保護者用',
                'mendanKiroku' => $mendanKiroku,
            ]),
            view('prestudent.mendan.contract'),
        ];

        // set mpdf configuration
        $config = [
            'format' => 'A4',
            'margin_left' => 10, //余白(左)
            'margin_right' => 10, //余白(右)
            'margin_top' => 10, //余白(上)
            'margin_bottom' => 10, //余白(下)
            'margin_header' => 5, //余白(ヘッダー)
            'margin_footer' => 5, //余白(フッター)
        ];

        // generate PDF form
        CustomMpdf::generate($pdfPages, $config);
    }
}
