<?php

namespace App\Http\Controllers\Classroom\Coming;

use Illuminate\Http\Request;
use App\Models\StudentAttendChange;

class AttendChangeController
{
    public function __invoke(Request $request, int $school_id, string $date)
    {
        $change = new StudentAttendChange;
        $change->student_id = $request->input('student_id');
        $change->change_type = $request->input('change_type');
        if ($change->change_type == 'furikae') {
            $change->before_date = $request->input('before_date');
        }
        $change->attend_date = ($request->input('attend_date') ? $request->input('attend_date') : null);
        $change->start_time = ($request->input('start_time') ? $request->input('start_time') : null);
        $change->end_time = ($request->input('end_time') ? $request->input('end_time') : null);

        $change->save();

        return redirect("/classroom/coming/{$school_id}/{$date}");
    }
}
