<?php

namespace App\Http\Controllers\Classroom\Coming;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\School;
use App\Models\StudentAttendBase;
use App\Models\StudentAttendSpecial;
use App\Models\StudentAttendChange;
use App\Models\StudentKyuujyuku;
use App\Models\StudentNote;
use App\Models\Student;
use Illuminate\Http\Request;

class ListController extends Controller
{
    public function __invoke(Request $request, int $school_id = null, string $date = null)
    {
        $teacher = \Auth::user()->teacher;
        $permission = $teacher->permission;
        $date = $date ?: null;
        $search_tantou = '0';

        if(in_array($permission, ['マネージャー', '管理者', '本部スタッフ']) === true) {
            $school_id = $school_id ?: null;
        } else {
            $school_id = $school_id ?: $teacher->tantou_school_id;
        }

        if($school_id == 9999) {
            $school_id = null;
        }


        if (in_array($permission, ['講師', '校長'])) { // 管理者 - admin, 講師 - teacher, 校長 - head master, 本部スタッフ - head staff
            $school_id = $teacher->tantou_school_id;
            if (($request->input('search_tantou') === null && $permission == '講師') || $request->input('search_tantou') == '1') {
                $search_tantou = '1';
            }
        }

        $target_date = \JJSS::getTargetDate($date);
        if (!$target_date) {
            return redirect('/classroom/coming');
        }

        // 通常,特別講習,休日
        $date_type = \JJSS::getTypeOfDate($school_id, $target_date);
        // 休日の場合はエラー表示
        if ($date_type == \JJSS::DATE_TYPE['holiday']) {
            return view('classroom.coming.list',
                [
                    'search_tantou' => $search_tantou,
                    'school_id' => $school_id,
                    'target_date' => $target_date->format('Y-m-d'),
                    'comings' => [],
                    'schools' => School::all(),
                    'students' => [],
                    'date_type' => $date_type,
                    'permission' => $permission,
                ]
            )->withErrors('この日は休日です。');
        }

        $comings = $this->getComings($school_id, $target_date, $date_type);

        // filter students handling by the teacher or headmaster
        if ($search_tantou === '1') {
            $comings = $comings->filter(function ($value, $key) use ($teacher) {
                return $value['teacher_id'] === $teacher->id;
            });
        }

        $students = Student::select();

        if ($school_id) {
            $students->where('school_id', $school_id);
        }

        //生徒名でのソート
        $orderby = [];
        if ($request->input('orderby_column_name')) {

            $comings_list = array_keys($comings->toArray());
            $coming_students_query = Student::select('id');
            $coming_students_query->whereIn('id', $comings_list);

            $orderby['column_name'] = $request->input('orderby_column_name');
            $orderby['type'] = $request->input('orderby_type');
            $coming_students_query->orderBy($orderby['column_name'], $orderby['type']);
            $coming_students = $coming_students_query->get();

            $_comings = [];
            $_comings_list = $comings->toArray();

            if(count($coming_students) > 0) {
                foreach($coming_students as $coming_student) {
                    $_comings[$coming_student->id] = $_comings_list[$coming_student->id];
                }
            }
            $comings_list = $_comings;
        } else {
            $comings_list = $comings->toArray();
        }

        return view('classroom.coming.list',
            [
                'search_tantou' => $search_tantou,
                'school_id' => $school_id,
                'target_date' => $target_date->format('Y-m-d'),
                'comings' => $comings_list,
//                'comings' => $comings->toArray(),
                'schools' => School::all(),
                'students' => $students->get(),
                'date_type' => $date_type,
                'permission' => $permission,
                'orderby' => $orderby,
            ]
        );
    }

    private function getComings($school_id, Carbon $target_date, $date_type)
    {
        if ($date_type == \JJSS::DATE_TYPE['basic']) {
            $comings = $this->getComingsOnBasic($school_id, $target_date);
        } else {  // if ($date_type == \JJSS::DATE_TYPE['special']) {
            $comings = $this->getComingsOnSpecial($school_id, $target_date);
        }

        // 追加コマ・振替のチェック
        $changes = StudentAttendChange::where('attend_date', $target_date)
            ->join('students', function($join) use ($school_id){
                $join->on('students.id', '=', 'student_attend_changes.student_id');
                if ($school_id) {
                    $join->where('students.school_id', '=', $school_id);
                }
            })
            ->get()->toArray();
        foreach($changes as $change) {
            $comings[$change['student_id']] = [
                'student' => [
                    'id' => $change['student_id'],
                    'name' => $change['name'],
                ],
                'note_id' => '',
                'checked_flag' => '',
                'plan_attend_start_time' => $change['start_time'],
                'plan_attend_end_time' => $change['end_time'],
                'actual_attend_start_time' => '',
                'actual_attend_end_time' => '',
                'teacher_id' => $change['teacher_id'],
                'change_date' => ($change['change_type'] === 'tsuika') ? '追加コマ' : date('n/j', strtotime($change['before_date'])) . 'からの振替',
            ];
        }

        // 子別ノート存在チェック,student_id取得 ... 上書きする
        $student_notes = StudentNote::select(['*', 'student_notes.id AS note_id'])
            ->where('note_date', $target_date)
            ->join('students', function($join) use ($school_id){
                $join->on('students.id', '=', 'student_notes.student_id');
                if ($school_id) {
                    $join->where('students.school_id', '=', $school_id);
                }
            })
            ->get()->toArray();
        foreach($student_notes as $note) {
            // 優先的に上書きする
            $comings[$note['student_id']] = [
                'student' => [
                    'id' => $note['student_id'],
                    'name' => $note['name'],
                ],
                'note_id' => $note['note_id'],
                'checked_flag' => $note['checked_flag'],
                'plan_attend_start_time' => $note['plan_attend_start_time'],
                'plan_attend_end_time' => $note['plan_attend_end_time'],
                'actual_attend_start_time' => $note['actual_attend_start_time'],
                'actual_attend_end_time' => $note['actual_attend_end_time'],
                'teacher_id' => $note['teacher_id'],
                'change_date' => $comings[$note['student_id']]['change_date'] ?? '',
            ];
        }

        // 振替前の日付になる場合は除外
        $change_befores = StudentAttendChange::where('before_date', $target_date)
            ->get()->toArray();
        foreach($change_befores as $change_before) {
            unset($comings[$change_before['student_id']]);
        }

        // 開始時間順、終了時間順
        uasort($comings, function ($a, $b) {
            if ($a['plan_attend_start_time'] < $b['plan_attend_start_time']) {
                return -1;
            } elseif ($a['plan_attend_start_time'] == $b['plan_attend_start_time']) {
                if ($a['plan_attend_end_time'] < $b['plan_attend_end_time']) {
                    return -1;
                } elseif ($a['plan_attend_end_time'] == $b['plan_attend_end_time']) {
                    if ($a['student']['id'] < $b['student']['id']) {
                        return -1;
                    }
                }
            }
            return 1;
        });

        return collect($comings);
    }

    private function getComingsOnBasic($school_id, Carbon $target_date)
    {
        // 曜日に基づくカラムを指定できるようにする
        $week = \JJSS::week($target_date);
        $week_start_time_column = $week. '_start_time';
        $week_end_time_column = $week. '_end_time';

        $attends = StudentAttendBase::where('start_date', '<=', $target_date)
            ->where(function($q) use ($target_date) {
                $q->whereNull('end_date')
                    ->orWhere('end_date', '>=', $target_date);
            })
            ->join('students', function($join) use ($school_id){
                $join->on('students.id', '=', 'student_attend_bases.student_id');
                if ($school_id) {
                    $join->where('students.school_id', '=', $school_id);
                }
            })
            ->whereNotNull($week_start_time_column)
            ->get()->toArray();

        //指定の日(target_date)が休塾期間中、退塾日後、卒塾日後の生徒を取得
        $leave_students = \JJSS::getLeaves($school_id, $target_date);

        $comings = [];
        foreach($attends as $attend) {
            //休塾中・卒塾日後・退塾日後の生徒は含めない
            if(in_array($attend['student_id'], $leave_students)) {
                continue;
            }
            $comings[$attend['student_id']] = [
                'student' => [
                    'id' => $attend['student_id'],
                    'name' => $attend['name'],
                ],
                'note_id' => '',
                'checked_flag' => '',
                'plan_attend_start_time' => $attend[$week_start_time_column],
                'plan_attend_end_time' => $attend[$week_end_time_column],
                'actual_attend_start_time' => '',
                'actual_attend_end_time' => '',
                'teacher_id' => $attend['teacher_id'],
                'change_date' => '',
            ];
        }

        return $comings;
    }

    private function getComingsOnSpecial($school_id, Carbon $target_date)
    {
        $attends = StudentAttendSpecial::where('attend_date', $target_date)
            ->join('students', function($join) use ($school_id){
                $join->on('students.id', '=', 'student_attend_specials.student_id');
                if ($school_id) {
                    $join->where('students.school_id', '=', $school_id);
                }
            })
            ->get()->toArray();

        //指定の日(target_date)が休塾期間中、退塾日後、卒塾日後の生徒を取得
        $leave_students = \JJSS::getLeaves($school_id, $target_date);

        $comings = [];
        foreach($attends as $attend) {
            //休塾中・卒塾日後・退塾日後の生徒は含めない
            if(in_array($attend['student_id'], $leave_students)) {
                continue;
            }
            $comings[$attend['student_id']] = [
                'student' => [
                    'id' => $attend['student_id'],
                    'name' => $attend['name'],
                ],
                'note_id' => '',
                'checked_flag' => '',
                'plan_attend_start_time' => $attend['start_time'],
                'plan_attend_end_time' => $attend['end_time'],
                'actual_attend_start_time' => '',
                'actual_attend_end_time' => '',
                'teacher_id' => $attend['teacher_id'],
                'change_date' => '',
            ];
        }

        return $comings;
    }


}
