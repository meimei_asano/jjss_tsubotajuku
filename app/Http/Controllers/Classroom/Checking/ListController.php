<?php

namespace App\Http\Controllers\Classroom\Checking;

use App\Http\Controllers\Controller;
use App\Models\StudentNote;
use App\Models\School;
use Illuminate\Http\Request;

class ListController extends Controller
{
    public function __invoke(Request $request, int $school_id = null, string $date = null)
    {
        $teacher = \Auth::user()->teacher;
        $permission = $teacher->permission;
        $date = $date ?: null;
        $search_tantou = '0';
        $search_printed_flag = '0';

        if(in_array($permission, ['マネージャー', '管理者', '本部スタッフ']) === true) {
            $school_id = $school_id ?: null;
        } else {
            $school_id = $school_id ?: $teacher->tantou_school_id;
        }

        if($school_id == 9999) {
            $school_id = null;
        }
        //$school_id = $school_id ?: $teacher->tantou_school_id;

        $orderby = [];

        if (in_array($permission, ['講師', '校長'])) { // 管理者 - admin, 講師 - teacher, 校長 - head master, 本部スタッフ - head staff
            $school_id = $teacher->tantou_school_id;
            if ($request->input('search_tantou') === null || $request->input('search_tantou') == '1') {
                $search_tantou = '1';
            }
        }

        if($request->input('search_printed_flag') !== null) {
            $search_printed_flag = $request->input('search_printed_flag');
        }

        $target_date = \JJSS::getTargetDate($date);
        if (!$target_date) {
            return redirect('/classroom/checking');
        }

        // 通常,特別講習,休日
        $date_type = \JJSS::getTypeOfDate($school_id, $target_date);
        // 休日の場合はエラー表示
        if ($date_type == \JJSS::DATE_TYPE['holiday']) {
            return view('classroom.checking.list',
                [
                    'search_tantou' => $search_tantou,
                    'school_id' => $school_id,
                    'schools' => School::all(),
                    'search_printed_flag' => $search_printed_flag,
                    'student_notes' => [],
                    'target_date' => $target_date->format('Y-m-d'),
                    'permission' => $permission,
                ]
            )->withErrors('この日は休日です。');
        }

        $s_query = StudentNote::select('*', 'student_notes.id AS id');
        //$s_query->leftJoin('students', 'student_notes.students_id', '=', 'students.id');

        $s_query->where('note_date', $target_date);
        $s_query->where('checked_flag', true);
        $s_query->whereIn('printed_flag', (strlen($search_printed_flag) > 0 ? array($search_printed_flag) : array('0','1')));
        if($school_id !== null) {
            $s_query->join('students', function ($join) use ($school_id) {
                $join->on('students.id', '=', 'student_notes.student_id')
                    ->where('students.school_id', '=', $school_id);
            });
        } else {
            $s_query->join('students', function ($join) use ($school_id) {
                $join->on('students.id', '=', 'student_notes.student_id');
            });

        }

        //sort
        if ($request->input('orderby_column_name')) {
            $orderby['column_name'] = $request->input('orderby_column_name');
            $orderby['type'] = $request->input('orderby_type');
            $s_query->orderBy($orderby['column_name'], $orderby['type']);
        } else {
            $s_query->orderBy('plan_attend_start_time');
            $s_query->orderBy('plan_attend_end_time');
        }

        $student_notes = $s_query->get();


/*
        $student_notes = StudentNote::select('*', 'student_notes.id AS id')
            ->where('note_date', $target_date)
            ->where('checked_flag', true)
            ->whereIn('printed_flag', (strlen($search_printed_flag) > 0 ? array($search_printed_flag) : array('0','1')))
            ->join('students', function($join) use ($school_id){
                $join->on('students.id', '=', 'student_notes.student_id')
                     ->where('students.school_id', '=', $school_id);
            })
            ->orderBy('plan_attend_start_time')
            ->orderBy('plan_attend_end_time')
            ->get();
*/

        // filter students handling by the teacher or headmaster
        if ($search_tantou === '1') {
            $student_notes = $student_notes->filter(function ($value, $key) use ($teacher) {
                return $value['teacher_id'] === $teacher->id;
            });
        }

        return view('classroom.checking.list',
            [
                'search_tantou' => $search_tantou,
                'school_id' => $school_id,
                'schools' => School::all(),
                'search_printed_flag' => $search_printed_flag,
                'student_notes' => $student_notes,
                'target_date' => $target_date->format('Y-m-d'),
                'permission' => $permission,
                'orderby' => $orderby,
            ]
        );
    }
}
