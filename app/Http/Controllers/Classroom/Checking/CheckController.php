<?php

namespace App\Http\Controllers\Classroom\Checking;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\StudentNote;

use DB;

class CheckController extends Controller
{
    public function __invoke(Request $request)
    {
        // TODO: 担当校舎の先生かを確認
        $teacher_id = \Auth::user()->teacher->id;

        $student_note_ids = explode(',', $request->input('student_note_ids'));

        DB::beginTransaction();
        try {
            if(count($student_note_ids) > 0) {
                foreach($student_note_ids as $student_note_id) {
                    $student_note = StudentNote::find($student_note_id);
                    $student_note->checked_flag = true;
                    $student_note->save();
                }
            }
            DB::commit();

            return response()->json(array('f' => 'ok', 'student_note_ids' => $student_note_ids));

        } catch (\PDOException $e){
            DB::rollBack();

            return response()->json($e->getMessage());
            //return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

}
