<?php

namespace App\Http\Controllers\Classroom\Changing;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\School;
use App\Models\StudentAttendChange;
use Illuminate\Http\Request;
use DB;

class ListController extends Controller
{
    public function __invoke(Request $request)
    {
        $search = [];
        $orderby = [];
        $teacher = \Auth::user()->teacher;
        $permission = $teacher->permission;

        $teacher_id = $teacher->id;
        $school_id = $request->input('search_school_id') ?? null;

        if (in_array($permission, ['講師', '校長'])) { // 管理者 - admin, 講師 - teacher, 校長 - head master, 本部スタッフ - head staff
            $school_id = $teacher->tantou_school_id;
            if ($request->input('search_tantou') == '1') {
                $search['tantou'] = '1';
            } elseif ($request->input('search_tantou') == null) {
                if ($permission == '講師') {
                    $search['tantou'] = '1';
                } else {  // 校長
                    $search['tantou'] = '0';
                }
            } else {
                $search['tantou'] = '0';
            }
        }

        $query = StudentAttendChange::with(['student'])->select();
        $query->leftJoin('students', 'student_attend_changes.student_id', '=', 'students.id');
        $query->leftJoin('teachers', 'students.teacher_id', '=', 'teachers.id');
        //校舎名のカナは項目が無いのでソートはIDで行うことにしとく
        //$query->leftJoin('schools', 'students.school_id', '=', 'schools.id');

        //検索：生徒名
        if($request->input('search_name')) {
            $search_name = $request->input('search_name');
            $query->whereHas('student', function($q) use ($search_name){
                $q->where('name', 'like', '%' . $search_name . '%');
            });

            $search['name'] = $request->input('search_name');
        }
        //検索：自分の担当生徒
        if(isset($search['tantou']) === true) {
            if($search['tantou'] == "1") {
                $query->whereHas('student', function($q) use ($teacher_id){
                    $q->where('teacher_id', $teacher_id);
                });
            }
        }

        //検索：校舎
        // (講師、校長)
        if (in_array($permission, ['校長', '講師'])) {
            //所属校舎のみ表示可能
            $query->whereHas('student', function($q) use ($school_id) {
                $q->where('school_id', '=', $school_id);
            });

       } else if (in_array($permission, ['管理者', '本部スタッフ'])) {
            //校舎を指定して検索が可能
            if (strlen($school_id) > 0) {
                $query->whereHas('student', function($q) use ($school_id) {
                    $q->where('school_id', '=', $school_id);
                });
            }
        }
        //検索：昨日以前の情報を表示
        $base_attend_date = Carbon::today()->format('Y-m-d');
        if($request->input('search_before_today')) {
            $query->where(function($q) use($base_attend_date){
                $q->WhereNull('attend_date')->orWhere('attend_date', '<', $base_attend_date);
            });
            $search['before_today'] = $request->input('search_before_today');
        } else {
            //昨日以前の情報を表示する条件ではない場合、今日以降の情報を表示する
            $query->where(function($q) use($base_attend_date){
                $q->WhereNull('attend_date')->orWhere('attend_date', '>=', $base_attend_date);
            });
        }

        //sort
        if ($request->input('orderby_column_name')) {
            $orderby['column_name'] = $request->input('orderby_column_name');
            $orderby['type'] = $request->input('orderby_type');
            $query->orderBy($orderby['column_name'], $orderby['type']);
        }

        $list = $query->paginate( config('paginate.items.note_changing') );

        return view('classroom.changing.list',
            [
                'search' => $search,
                'orderby' => $orderby,
                'list' => $list,
                'schools' => School::all(),
                'search_school_id' => $school_id,
                'permission' => $permission,
            ]
        );
    }

}
