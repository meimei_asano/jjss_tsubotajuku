<?php

namespace App\Http\Controllers\Classroom\Note;

use App\Facades\CustomMpdf;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kamoku;
use App\Models\StudentNote;
use Carbon\Carbon;

class PrintController extends Controller
{
    public function __invoke(int $student_id, string $date = null)
    {
        $target_date = \JJSS::getTargetDate($date);

        $student_note = \AutomateStudentNote::getTodayStudentNote($student_id, $target_date);

        // 子別ノートが生成されていない
        if (!$student_note) {
            // 子別ノート生成画面へ移動
            return redirect("classroom/note/create/{$student_id}/{$target_date->format('Y-m-d')}");
        }
        // 子別ノートが承認されていない
        if (!$student_note->checked_flag) {
            // 子別ノート承認画面へ移動
            return redirect("classroom/note/check/{$student_id}/{$target_date->format('Y-m-d')}");
        }

        $school_id = $student_note->student->school_id;

        $html = view('classroom.note.print',
            [
                'school_id' => $school_id,
                'student_id' => $student_id,
                'target_date' => $target_date->format('Y-m-d'),
                'student_note' => $student_note,
                'kamokus' => Kamoku::all(),
            ]
        );

        $config = [
            'format' => 'B5-L',
            'margin_left' => 8, //余白(左)
            'margin_right' => 8, //余白(右)
            'margin_top' => 8, //余白(上)
            'margin_bottom' => 8, //余白(下)
            'margin_header' => 5, //余白(ヘッダー)
            'margin_footer' => 5, //余白(フッター)
        ];

        CustomMpdf::generate($html, $config);

        //printed_flag = 1 (印刷済) に更新する
        $student_note->printed_flag = '1';
        $student_note->save();

    }


    public function multi(Request $request)
    {
        $student_note_ids = explode(',', $request->input('student_note_ids'));
        $student_notes = StudentNote::whereIn('id', $student_note_ids)->get();

        if (! $student_notes) {
            $school_id = $request->input('school_id');
            $target_date = $request->input('target_date');
            //エラーだったよ！を表示する別画面の方が良い気がする
            return redirect('classroom/checking/' . $school_id . '/' . $target_date);
        }

        $kamokus = Kamoku::all();

        $htmls = [];
        foreach ($student_notes as $student_note) {
            $htmls[] = view('classroom.note.print',
                [
                    'school_id' => $student_note->student->school_id,
                    'student_id' => $student_note->student->id,
                    'target_date' => Carbon::parse($student_note->note_date)->format('Y-m-d'),
                    'student_note' => $student_note,
                    'kamokus' => $kamokus,
                ]
            );
        }
        $html = implode('<pagebreak>', $htmls);

        $config = [
            'format' => 'B5-L',
            'margin_left' => 8, //余白(左)
            'margin_right' => 8, //余白(右)
            'margin_top' => 8, //余白(上)
            'margin_bottom' => 8, //余白(下)
            'margin_header' => 5, //余白(ヘッダー)
            'margin_footer' => 5, //余白(フッター)
        ];

        CustomMpdf::generate($html, $config, '子別ノート');

        // 印刷済に更新
        foreach ($student_notes as $student_note) {
            $student_note->printed_flag = true;
            $student_note->save();
        }
    }

}
