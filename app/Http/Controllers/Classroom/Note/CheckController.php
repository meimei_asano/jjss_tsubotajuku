<?php

namespace App\Http\Controllers\Classroom\Note;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentNote;
use App\Models\Kamoku;
use DB;

class CheckController extends Controller
{
    public function __invoke(int $student_id, string $date = null)
    {
        $target_date = \JJSS::getTargetDate($date);

        $current_student_note = StudentNote
            ::where('student_id', $student_id)
            ->where('note_date', $target_date)
            ->first();

        // 子別ノートが生成されていない
        if (! $current_student_note) {
            // 子別ノート生成画面へ移動
            return redirect("classroom/note/create/{$student_id}/{$target_date->format('Y-m-d')}");
        }

        $school_id = $current_student_note->student->school_id;

        // 休日か？
        $date_type = \JJSS::getTypeOfDate($school_id, $target_date);
        // 休日の場合はエラー表示
        if ($date_type == \JJSS::DATE_TYPE['holiday']) {
            return redirect('classroom/coming');
        }

        // 承認済みか？
        if ($current_student_note->checked_flag) {
            return redirect("classroom/note/{$student_id}/{$target_date->format('Y-m-d')}");
        }

        // 前回の子別ノート
        $last_student_note = \AutomateStudentNote::getLastStudentNote($student_id, $target_date);
        if (! $last_student_note) {
            // これまで一つも子別ノートが作られていなければ空で表示
            $last_student_note = new StudentNote;
            $student = Student::find($student_id);
            $last_student_note->student = $student;
        }

        $student = Student::find($student_id);

        return view('classroom.note.check',
            [
                'school_id' => $school_id,
                'target_date' => $target_date->format('Y-m-d'),
                'last_student_note' => $last_student_note,
                'current_student_note' => $current_student_note,
                'kamokus' => Kamoku::all(),
                'student' => $student,//PIF用
                'teacher_id' => \Auth::user()->teacher->id,//PIF用
            ]
        );
    }

    public function regenerate(int $student_note_id)
    {
        $student_note = StudentNote::find($student_note_id);
        $student_id = $student_note->student_id;
        $target_date = \JJSS::getTargetDate($student_note->target_date);

        $result = \AutomateStudentNote::recreateStudentNote($student_id, $target_date);
        if ($result) {
            // 子別ノートに遷移
            return redirect("/classroom/note/{$student_id}/{$target_date->format('Y-m-d')}");
        } else {
            return redirect("/classroom/note/check/{$student_id}/{$target_date->format('Y-m-d')}")->withErrors(['子別ノート再生成中にエラーが発生しました。']);
        }
    }

    public function check(int $student_note_id)
    {
        // TODO: 担当校舎の先生かを確認
        $teacher_id = \Auth::user()->teacher->id;

        DB::beginTransaction();
        try {
            $student_note = StudentNote::find($student_note_id);
            $student_note->checked_flag = true;
            $student_note->save();
            DB::commit();

            return redirect("/classroom/note/{$student_note->student_id}/{$student_note->target_date}");

        } catch (\PDOException $e){
            DB::rollBack();

            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

}
