<?php

namespace App\Http\Controllers\Classroom\Note;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\StudentNote;
use App\Models\Student;
use App\Models\StudentNoteRow;
use App\Models\Kamoku;
use DB;

class CreateController extends Controller
{
    public function list(int $student_id, string $date)
    {
        $target_date = \JJSS::getTargetDate($date);

        $student = Student::find($student_id);
        $school_id = $student->school_id;

        // 休日か？
        $date_type = \JJSS::getTypeOfDate($school_id, $target_date);
        // 休日の場合はエラー表示
        if ($date_type == \JJSS::DATE_TYPE['holiday']) {
            return redirect('classroom/coming');
        }

        // 振替済みか
        $has_change_before_date = \AutomateStudentNote::hasChangeBeforeDate($student_id, $target_date);
        if ($has_change_before_date) {
            return redirect('classroom/coming');
        }

        // 今日の子別ノートがあるか
        $today_student_note = \AutomateStudentNote::getTodayStudentNote($student_id, $target_date);
        if ($today_student_note) {
            return redirect("classroom/note/check/{$student_id}/{$target_date->format('Y-m-d')}");
        }

        $last_student_note = \AutomateStudentNote::getLastStudentNote($student_id, $target_date);
        if ($last_student_note) {
            // 指定日以降の個別ノートでないかのチェック
            if ($last_student_note->note_date >= $target_date) {
                return redirect("classroom/coming/{$school_id}");
            }
        } else {
            // これまで一つも子別ノートが作られていなければ空で表示
            $last_student_note = new StudentNote;
            $last_student_note->student = $student;
        }

        // 今回の子別ノートを空で表示
        $plan_attend = \AutomateStudentNote::getPlanAttendTime($student_id, $target_date);
        $current_student_note = new StudentNote;
        $current_student_note->note_date = $target_date;
        $current_student_note->plan_attend_start_time = $plan_attend['start_time'];
        $current_student_note->plan_attend_end_time = $plan_attend['end_time'];
        $current_student_note->student = $student;

        $student = Student::find($student_id);

        return view('classroom.note.create',
            [
                'school_id' => $school_id,
                'student_id' => $student_id,
                'target_date' => $target_date->format('Y-m-d'),
                'last_student_note' => $last_student_note,
                'current_student_note' => $current_student_note,

                'kamokus' => Kamoku::all(),
                'student' => $student,//PIF用
                'teacher_id' => \Auth::user()->teacher->id,//PIF用
            ]
        );
    }

    public function generate(int $student_id, string $date)
    {
        $target_date = \JJSS::getTargetDate($date);

        $result = \AutomateStudentNote::createStudentNote($student_id, $target_date);
        if ($result) {
            // 子別ノートに遷移
            return redirect("/classroom/note/{$student_id}/{$target_date->format('Y-m-d')}");
        } else {
            return redirect("/classroom/note/create/{$student_id}/{$target_date->format('Y-m-d')}")->withErrors(['子別ノート生成中にエラーが発生しました。']);
        }
    }

    public function addrow(Request $request, int $student_note_id)
    {
        $student_note = StudentNote::find($student_note_id);
        if (! $student_note) {
            return redirect('/classroom/coming');
        }

        $note_row = new StudentNoteRow;
        $note_row->student_note_id = $student_note_id;
        $note_row->text_id = $request->input('text_id');
        $note_row->additional_text_name = $request->input('additional_text_name');
        $note_row->reverse_test = $request->input('reverse_test');
        $note_row->text_unit_id = $request->input('text_unit_id');
        $note_row->display_text_unit = $request->input('display_text_unit');

        $note_row->save();

        return redirect( url()->previous('/classroom/coming') );
    }


    public function delete_row(Request $request, int $student_note_row_id)
    {
        $note_row = StudentNoteRow::find($student_note_row_id);
        $note_row->delete();

        return redirect( url()->previous('/classroom/coming') );
    }
}
