<?php

namespace App\Http\Controllers\Classroom\Note;

use App\Http\Controllers\Controller;
use App\Models\Kamoku;
use App\Models\Student;

class ListController extends Controller
{
    public function __invoke(int $student_id, string $date = null)
    {
        $target_date = \JJSS::getTargetDate($date);

        $student_note = \AutomateStudentNote::getTodayStudentNote($student_id, $target_date);

        // 子別ノートが生成されていない
        if (! $student_note) {
            // 子別ノート生成画面へ移動
            return redirect("classroom/note/create/{$student_id}/{$target_date->format('Y-m-d')}");
        }
        // 子別ノートが承認されていない
        if (! $student_note->checked_flag) {
            // 子別ノート承認画面へ移動
            return redirect("classroom/note/check/{$student_id}/{$target_date->format('Y-m-d')}");
        }

        $student = Student::find($student_id);

        $school_id = $student_note->student->school_id;
        return view('classroom.note.list',
            [
                'school_id' => $school_id,
                'student_id' => $student_id,
                'target_date' => $target_date->format('Y-m-d'),
                'student_note' => $student_note,
                'kamokus' => Kamoku::all(),
                'student' => $student,
                'teacher_id' => \Auth::user()->teacher->id,//PIF用
            ]
        );
    }
}
