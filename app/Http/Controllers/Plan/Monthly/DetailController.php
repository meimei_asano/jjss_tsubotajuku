<?php

namespace App\Http\Controllers\Plan\Monthly;

use App\Http\Controllers\Controller;

use App\Models\Student;
use App\Models\StudentMonthly;
use App\Models\StudentMonthlyText;
use App\Models\StudentMonthlyTextUnit;
use App\Models\Kamoku;


class DetailController extends Controller
{
    public function __invoke($id)
    {
        $student_monthly = StudentMonthly::find($id);

        $unit_info = [];
        if(count($student_monthly->student_monthly_texts) > 0) {
            foreach($student_monthly->student_monthly_texts as $monthly_text) {

                $m_unit_list = [];
                if(count($monthly_text->student_monthly_text_units) > 0) {
                    foreach($monthly_text->student_monthly_text_units as $text_unit) {
                        $m_unit_list[$text_unit->text_unit_num] = [
                                                        'text_unit_name' => $text_unit->text_unit_name,
                                                        'text_unit_num' => $text_unit->text_unit_num,
                                                        'learning_flag' => $text_unit->learning_flag,
                                                        'display_flag' => 0,
                                                        ];
                    }
                }

                $unit_list = [];
                if($monthly_text->text->units_by_student != "1" && isset($monthly_text->text->text_units) === true) {
                    foreach($monthly_text->text->text_units as $text_unit) {
                        if(isset($m_unit_list[$text_unit->unit_num]) === true) {
                            if($text_unit->unit_num == "1") {
                                if(isset($m_unit_list[0]) === true) {
                                    $unit_list[] = $m_unit_list[0];
                                }
                            }
                            $unit_list[] = $m_unit_list[$text_unit->unit_num];
                        } else {
                            if($text_unit->unit_num == "1") {
                                $unit_list[] = [
                                    'text_unit_name' => ($monthly_text->kamoku_id == "7" ? "カウント" : "初回"),
                                    'text_unit_num' => 0,
                                    'learning_flag' => 1,
                                    'display_flag' => 1,
                                ];
                            }
                            $unit_list[] = [
                                    'text_unit_name' => $text_unit->name,
                                    'text_unit_num' => $text_unit->unit_num,
                                    'learning_flag' => 1,
                                    'display_flag' => 1,
                                ];
                        }
                    }
                } else if($monthly_text->text->units_by_student == "1" && (int)$monthly_text->text_all_kaisuu > 0) {
                    for($i = 0; $i <= (int)$monthly_text->text_all_kaisuu; $i++) {
                        if(isset($m_unit_list[$i]) === true) {
                            $unit_list[] = $m_unit_list[$i];
                        } else {
                            $unit_name = $i;
                            if($i == 0) {
                                if($monthly_text->kamoku_id == "7") {
                                    $unit_name = "カウント";
                                } else {
                                    $unit_name = "初回";
                                }
                            }

                            $unit_list[] = [
                                'text_unit_name' => $unit_name,
                                'text_unit_num' => $i,
                                'learning_flag' => 1,
                                'display_flag' => 1,
                            ];
                        }
                    }
                }

                $unit_info[$monthly_text->id] = $unit_list;
            }
        }

        $visit_dates = \JJSS::getVisitDates($student_monthly->student->id, $student_monthly->term->id);

        return view('plan.monthly.detail', [
                'student_monthly_id' => $student_monthly->id,
                'student' => $student_monthly->student,
                'term' => $student_monthly->term,
                'kamokus' => Kamoku::all(),
                'student_monthly' => $student_monthly,
                'unit_info' => $unit_info,
                'visit_dates' => $visit_dates,
                'visit_week_days' => \JJSS::getVisitWeekDays($student_monthly->student->id, $student_monthly->term->id),

            ]
        );
    }
}
