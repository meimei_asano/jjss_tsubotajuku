<?php

namespace App\Http\Controllers\Plan\Monthly;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentMonthly;
use App\Models\StudentMonthlyText;
use App\Models\StudentMonthlyTextUnit;
use App\Models\School;
use App\Models\Term;

class ListController extends Controller
{
    public function __invoke(Request $request)
    {
        $search = [];
        $orderby = [];

        $query = StudentMonthly::select()
                    ->with([
                        'student',
                        'term',
                    ]);

        // search
        if ($request->input('search_student_id')) {
            $search['search_student_id'] = $request->input('search_student_id');
            $query->where('student_id', '=', $search['search_student_id']);
        }

        if ($request->input('search_term_id')) {
            $search['search_term_id'] = $request->input('search_term_id');
            $query->where('term_id', '=', $search['search_term_id']);
        }

        //校長、講師は担当校舎の情報のみを取り扱う
        if(in_array(\JJSS::getPermission(), array('校長', '講師'))) {
            $tantou_school_id = \Auth::user()->teacher->tantou_school_id;
            $search['school_id'] = $tantou_school_id;
            $query->whereHas('student', function($q) use ($tantou_school_id){
                $q->where('school_id', '=', $tantou_school_id);
            });

        } else {
            if ($request->input('search_school_id')) {
                $search['school_id'] = $request->input('search_school_id');
                $search_school_id = $search['school_id'];
                $query->whereHas('student', function($q) use ($search_school_id){
                    $q->where('school_id', '=', $search_school_id);
                });
            }
        }

        //sort
        if ($request->input('orderby_column_name')) {
            $orderby['column_name'] = $request->input('orderby_column_name');
            $orderby['type'] = $request->input('orderby_type');
            if(in_array($orderby['column_name'], array("school_id", "student_code")) === true) {
                $query->select("student_monthlies.*", \DB::raw('(SELECT ' . $orderby['column_name'] . ' FROM students WHERE student_monthlies.student_id = students.id limit 1 ) as ' . $orderby['column_name']));
            } else if($orderby['column_name'] == "student_name") {
                $query->select("student_monthlies.*", \DB::raw('(SELECT kana FROM students WHERE student_monthlies.student_id = students.id limit 1 ) as ' . $orderby['column_name']));
            }

            $query->orderBy($orderby['column_name'], $orderby['type']);

        }

        $student_monthlies = $query->paginate( config('paginate.items.plan_monthly') );

        //校長、講師は担当校舎の情報のみを取り扱う
        if(in_array(\JJSS::getPermission(), array('校長', '講師'))) {
            $schools = School::where('id', '=', $tantou_school_id)->get();
            $students = Student::where('school_id', '=', $tantou_school_id)->get();
        } else {
            $schools = School::all();
            $students = Student::all();
        }

        return view('plan.monthly.list', [
            'search' => $search,
            'orderby' => $orderby,
            'student_monthlies' => $student_monthlies,
            'students' => $students,
            'schools' => $schools,
            'terms' => Term::orderBy('start_date', 'desc')->get(),
        ]);
    }
}
