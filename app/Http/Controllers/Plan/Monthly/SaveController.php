<?php

namespace App\Http\Controllers\Plan\Monthly;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Validation\PlanMonthlyValidation as Validation;
use App\Models\StudentMonthly;
use App\Models\StudentMonthlyText;
use App\Models\StudentMonthlyTextUnit;
use App\Models\User;
use DB;

class SaveController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();
        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            //student_monthlies に登録
            $student_monthly_id = $request->input('student_monthly_id');
            $student_monthly = StudentMonthly::find($student_monthly_id);
            if(!$student_monthly) {
                $student_monthly = new StudentMonthly;
            }
            $student_monthly->student_id = $request->input('student_id');
            $student_monthly->term_id = $request->input('term_id');
            //$student_monthly-planned_hours = //通塾予定時間 (time型)
            $student_monthly->furikaeri_teacher = '';
            $student_monthly->furikaeri_student = '';
            $student_monthly->mokuhyou_teacher = '';
            $student_monthly->save();

            $student_monthly_id = $student_monthly->id;

            //student_monthly_texts に登録（ぐるぐる）
            $student_monthly_text_id = $request->input('student_monthly_text_id');
            $kamoku_id = $request->input('kamoku_id');
            $text_id = $request->input('text_id');
            $reverse_test = $request->input('reverse_test');
            $text_name = $request->input('text_name');
            $text_all_kaisuu = $request->input('text_all_kaisuu');
            $keikaku_suu = $request->input('keikaku_suu');
            $maikai_kaisuu = $request->input('maikai_kaisuu');
            foreach(config('jjss.weeks') as $wk => $week) {
                if (in_array($wk, array(0, 6))) {
                    continue;
                }
                $maikai_kaisuu_name = 'maikai_kaisuu_' . $wk;
                $$maikai_kaisuu_name = $request->input('maikai_kaisuu_' . $wk);
            }

            $touroku_ids = [];
            $c = count($kamoku_id);
            for($i = 0; $i < $c; $i++) {

                if(strlen($kamoku_id[$i]) == 0) {
                    continue;
                }

                $monthly_text = new StudentMonthlyText;
                if(isset($student_monthly_text_id[$i])) {
                    if (strlen($student_monthly_text_id[$i]) > 0) {
                        $monthly_text = StudentMonthlyText::find($student_monthly_text_id[$i]);
                    }
                }
                $monthly_text->student_monthly_id = $student_monthly_id;
                $monthly_text->kamoku_id = $kamoku_id[$i];
                $monthly_text->kamoku_sequence = $i;
                $monthly_text->text_id = $text_id[$i];
                $monthly_text->reverse_test = (strlen($reverse_test[$i]) > 0 ? $reverse_test[$i] : '0');
                $monthly_text->text_name = $text_name[$i];
                $monthly_text->text_all_kaisuu = (strlen($text_all_kaisuu[$i]) > 0 ? $text_all_kaisuu[$i] : '0');
                $monthly_text->keikaku_suu = $keikaku_suu[$i];
                $monthly_text->maikai_kaisuu = (strlen($maikai_kaisuu[$i]) > 0 ? $maikai_kaisuu[$i] : null);
                foreach(config('jjss.weeks') as $wk => $week) {
                    if (in_array($wk, array(0, 6))) {
                        continue;
                    }
                    $maikai_kaisuu_name = 'maikai_kaisuu_' . $wk;
                    $maikai_kaisuu_ar = $$maikai_kaisuu_name;
                    $monthly_text->{'maikai_kaisuu_' . $wk} = (strlen($maikai_kaisuu_ar[$i]) > 0 ? $maikai_kaisuu_ar[$i] : null);
                }

                $monthly_text->save();

                $touroku_ids[] = $monthly_text->id;

                //student_monthly_text_units (delete => insert)
                $monthly_text_unit = StudentMonthlyTextUnit::where('student_monthly_id', '=', $student_monthly_id)
                                                            ->where('student_monthly_text_id', '=', $monthly_text->id)
                                                            ->delete();

                $unit_num_list = $request->input(($monthly_text->text_id ?? '') . ($reverse_test[$i] ?? '') . "unit_num");
                $unit_name_list = $request->input(($monthly_text->text_id ?? '') . ($reverse_test[$i] ?? '') . "unit_name");
                $unit_learning_flag = $request->input(($monthly_text->text_id ?? '') . ($reverse_test[$i] ?? '') . "unit_num_learning");
                $unit_display_flag = $request->input(($monthly_text->text_id ?? '') . ($reverse_test[$i] ?? '') . "unit_display_flag");
                if(is_array($unit_num_list) === true) {
                    $unit_count = count($unit_num_list);
                    if ($unit_count > 0) {
                        $insert_array = [];
                        for ($ui = 0; $ui < $unit_count; $ui++) {
                            if($unit_display_flag[$ui] == "1") {
                                continue;
                            }
                            $insert_array[] = [
                                'learning_flag' => $unit_learning_flag[$ui],
                                'student_monthly_id' => $student_monthly_id,
                                'student_monthly_text_id' => $monthly_text->id,
                                'kamoku_id' => $monthly_text->kamoku_id,
                                'text_id' => $monthly_text->text_id,
                                'text_unit_num' => $unit_num_list[$ui],
                                'text_unit_name' => $unit_name_list[$ui],
                            ];
                        }
                        StudentMonthlyTextUnit::insert($insert_array);
                    } else {
                        //学習回が無いのなら不要
                        $monthly_text->delete();
                    }
                } else {
                    //学習回が無いのなら不要
                    $monthly_text->delete();
                }
            }

            StudentMonthlyText::where('student_monthly_id', '=', $student_monthly_id)
                                ->whereNotIn('id', $touroku_ids)->delete();

            DB::commit();

            $request->session()->flash('alert.success', '登録しました。');
            return redirect('/plan/monthly/' . $student_monthly_id);

        } catch (\PDOException $e){
            DB::rollBack();
            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

}
