<?php

namespace App\Http\Controllers\Plan\Monthly;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Models\Student;
use App\Models\StudentMonthly;
use App\Models\StudentNote;


class DetailResultController extends Controller
{
    public function __invoke(Request $request)
    {
        $monthly_id = $request->input("monthly_id");

        //月間計画
        $monthly_plan = StudentMonthly::find($monthly_id);
        //来塾予定日
        $visit_dates = \JJSS::getVisitDates($monthly_plan->student_id, $monthly_plan->term_id);
        //来塾実績
        $visited_dates = \JJSS::getVisitedDates($monthly_plan->student_id, $monthly_plan->term_id);
        //対象期間の結果
        $kekka = StudentNote::where("student_id", "=", $monthly_plan->student_id)
                            ->where("note_date", ">=", $monthly_plan->term->start_date)
                            ->where("note_date", "<=", $monthly_plan->term->end_date)
                            ->where("checked_flag", "=", "1")//承認済
                            ->get();

        $kekka_datas = [];
        $sub_kekka_info = [];

        if($kekka) {
            if(count($kekka) > 0) {
                foreach($kekka as $note) {
                    if($note->student_note_rows) {
                        if(count($note->student_note_rows) > 0) {
                            foreach($note->student_note_rows as $note_row) {
                                if(strlen($note_row->result_flag) > 0) {
                                    $unit_num = isset($note_row->text_unit)===true ? $note_row->text_unit->unit_num : $note_row->display_text_unit;
                                    if($note_row->text) {
                                        if ($note_row->text->units_by_student == "1") {
                                            $unit_num = $note_row->text_unit_id;
                                        }
                                    }
                                    $kekka_datas[$note_row->text_id . $note_row->reverse_test][$unit_num][] = ['date' => $note->note_date, 'kekka' => $note_row->result_flag];

                                    //計画がなくて結果のみだった場合に利用する配列
                                    $sub_kekka_info[$note_row->text_id . $note_row->reverse_test] = [
                                                                                'text_name' => (strlen($note_row->additional_text_name) > 0 ? $note_row->additional_text_name : (isset($note_row->text)===true ? $note_row->text->name : '')),
                                                                                'unit_name' . $unit_num => (strlen($note_row->display_text_unit) > 0 ? $note_row->display_text_unit : '　'),
                                                                                ];
                                }
                            }
                        }
                    }
                }
            }
        }
        //生徒情報
        $student = Student::find($monthly_plan->student_id);

        return view('plan.monthly.detail_result', [
                'monthly_id' => $monthly_id,
                'monthly_plan' => $monthly_plan,
                'kekka' => $kekka_datas,
                'sub_kekka_info' => $sub_kekka_info,
                'student' => $student,
                'visit_dates' => $visit_dates,
                'visited_dates' => $visited_dates,
            ]
        );
    }
}
