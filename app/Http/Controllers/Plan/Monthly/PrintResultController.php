<?php

namespace App\Http\Controllers\Plan\Monthly;

use App\Http\Controllers\Controller;
use App\Models\StudentNote;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Facades\CustomMpdf;

use App\Models\StudentMonthly;
use App\Models\Term;

class PrintResultController extends Controller
{
    public function __invoke(Request $request)
    {
        set_time_limit(0);

        //データを取得
        $student_monthly_ids = explode(',', $request->input('student_monthly_ids'));

        $htmls = [];
        foreach($student_monthly_ids as $student_monthly_id) {

            //月間計画情報取得
            $student_monthly = StudentMonthly::find($student_monthly_id);
            //期間
            $term = Term::find($student_monthly->term_id);
            //期間中の来塾予定日
            $visit_dates = \JJSS::getVisitDates($student_monthly->student_id, $student_monthly->term_id);

            //期間中の全日数の配列を作成
            $term_dates = [];
            $term_date = Carbon::parse($term->start_date);
            while($term_date->lte(Carbon::parse($term->end_date))) {
                $term_dates[] = $term_date->format('Y-m-d');
                $term_date = $term_date->addDay();
            }
            //1週間の来塾曜日
            $visit_week_days = \JJSS::getVisitWeekDays($student_monthly->student->id, $student_monthly->term->id);

            //来塾実績
            $visited_dates = \JJSS::getVisitedDates($student_monthly->student_id, $student_monthly->term_id);
            //対象期間の結果
            $kekka = StudentNote::where("student_id", "=", $student_monthly->student_id)
                ->where("note_date", ">=", $student_monthly->term->start_date)
                ->where("note_date", "<=", $student_monthly->term->end_date)
                ->where("checked_flag", "=", "1")//承認済
                ->get();

            $kekka_datas = [];
            $sub_kekka_info = [];

            if($kekka) {
                if(count($kekka) > 0) {
                    foreach($kekka as $note) {
                        if($note->student_note_rows) {
                            if(count($note->student_note_rows) > 0) {
                                foreach($note->student_note_rows as $note_row) {
                                    if(strlen($note_row->result_flag) > 0) {
                                        $unit_num = isset($note_row->text_unit)===true ? $note_row->text_unit->unit_num : $note_row->display_text_unit;
                                        if($note_row->text) {
                                            if ($note_row->text->units_by_student == "1") {
                                                $unit_num = $note_row->text_unit_id;
                                            }
                                        }
                                        $kekka_datas[$note_row->text_id . $note_row->reverse_test][$unit_num][] = ['date' => $note->note_date, 'kekka' => $note_row->result_flag];

                                        //計画がなくて結果のみだった場合に利用する配列
                                        $sub_kekka_info[$note_row->text_id . $note_row->reverse_test] = [
                                            'text_name' => (strlen($note_row->additional_text_name) > 0 ? $note_row->additional_text_name : (isset($note_row->text)===true ? $note_row->text->name : '')),
                                            'unit_name' . $unit_num => (strlen($note_row->display_text_unit) > 0 ? $note_row->display_text_unit : '　'),
                                        ];
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $htmls[] = $this->getPDFBody(
                                $student_monthly,
                                $term_dates,
                                $visit_dates,//来塾予定日
                                $visit_week_days,//来塾予定曜日
                                $visited_dates,//来塾日
                                $kekka_datas,//計画のある実績
                                $sub_kekka_info//未計画実績
            );
        }

        $html = implode('<pagebreak>', $htmls);

        //PDF出力
        CustomMpdf::generate($html, $this->getPDFConfig(), '学習実績');

    }


    public function getPDFBody($student_monthly, $term_dates, $visit_dates, $visit_week_days, $visited_dates, $kekka_datas, $sub_kekka_info)
    {
        //来塾予定（カレンダー）
        $calendar = view('plan.monthly.print_calendar',
            [
                'student_monthly' => $student_monthly,
                'term_dates' => $term_dates,
                'visit_dates' => $visit_dates,
                'visited_dates' => $visited_dates,
            ]);

        //学習実績
        $main = view('plan.monthly.print_result',
            [
               'student_monthly' => $student_monthly,
               'visit_week_days' => $visit_week_days,
               'visited_dates' => $visited_dates,
               'kekka' => $kekka_datas,
               'sub_kekka_info' => $sub_kekka_info,
            ]);

        return $calendar . $main;
//        return $calendar . '<pagebreak>' . $main;

    }


    public function getPDFConfig()
    {
        $config = [
            'format' => 'A4',
            'margin_left' => 10, //余白(左)
            'margin_right' => 10, //余白(右)
            'margin_top' => 10, //余白(上)
            'margin_bottom' => 10, //余白(下)
            'margin_header' => 5, //余白(ヘッダー)
            'margin_footer' => 5, //余白(フッター)
        ];

        return $config;
    }
}
