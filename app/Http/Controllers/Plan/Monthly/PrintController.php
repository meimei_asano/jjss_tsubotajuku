<?php

namespace App\Http\Controllers\Plan\Monthly;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Facades\CustomMpdf;

use App\Models\StudentMonthly;
use App\Models\Term;

class PrintController extends Controller
{
    public function __invoke(Request $request)
    {
        set_time_limit(0);

        //データを取得
        $student_monthly_ids = explode(',', $request->input('student_monthly_ids'));

        $htmls = [];
        foreach($student_monthly_ids as $student_monthly_id) {

            //月間計画情報取得
            $student_monthly = StudentMonthly::find($student_monthly_id);
            //期間
            $term = Term::find($student_monthly->term_id);
            //期間中の来塾予定日
            $visit_dates = \JJSS::getVisitDates($student_monthly->student_id, $student_monthly->term_id);

            //期間中の全日数の配列を作成
            $term_dates = [];
            $term_date = Carbon::parse($term->start_date);
            while($term_date->lte(Carbon::parse($term->end_date))) {
                $term_dates[] = $term_date->format('Y-m-d');
                $term_date = $term_date->addDay();
            }

            $visit_week_days = \JJSS::getVisitWeekDays($student_monthly->student->id, $student_monthly->term->id);

            //来塾実績
            $visited_dates = \JJSS::getVisitedDates($student_monthly->student_id, $student_monthly->term_id);

            $htmls[] = $this->getPDFBody($student_monthly, $term_dates, $visit_dates, $visit_week_days, $visited_dates);
        }

        $html = implode('<pagebreak>', $htmls);

        //PDF出力
        CustomMpdf::generate($html, $this->getPDFConfig(), '月間計画');

    }

    public function getPDFBody($student_monthly, $term_dates, $visit_dates, $visit_week_days, $visited_dates)
    {
        //来塾予定（カレンダー）
        $calendar = view('plan.monthly.print_calendar',
            [
                'student_monthly' => $student_monthly,
                'term_dates' => $term_dates,
                'visit_dates' => $visit_dates,
                'visited_dates' => $visited_dates,
            ]);

        $main = view('plan.monthly.print_schedule',
            [
               'student_monthly' => $student_monthly,
               'visit_week_days' => $visit_week_days,
            ]);

        return $calendar . $main;
        //return $calendar . '<pagebreak>' . $main;

    }

    public function getPDFConfig()
    {
        $config = [
            'format' => 'A4',
            'margin_left' => 10, //余白(左)
            'margin_right' => 10, //余白(右)
            'margin_top' => 10, //余白(上)
            'margin_bottom' => 10, //余白(下)
            'margin_header' => 5, //余白(ヘッダー)
            'margin_footer' => 5, //余白(フッター)
        ];

        return $config;
    }
}
