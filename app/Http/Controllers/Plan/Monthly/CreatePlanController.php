<?php

namespace App\Http\Controllers\Plan\Monthly;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Validation\PlanMonthlyCreatePlanValidation as Validation;
use Carbon\Carbon;

use App\Models\Term;
use App\Models\Student;
use App\Models\StudentAttendSpecial;
use App\Models\Kamoku;
use App\Models\AnnualSchedule;
use App\Models\Text;

class CreatePlanController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        //TODO:飛んできた内容をバリデート
        //  開始テキストの開始回数 → 終了テキストの回数 の場合は、その順番が正しいかどうか（逆じゃないか）
        // ↓
        $errors = \App\Validation\PlanMonthlyCreatePlanValidation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        $student_id = $request->input('student_id');
        $term_id = $request->input('term_id');

        $student = Student::with([
                        'school',
                        'shibou',
                        'kamokus',
                        'ninetype',
                        'teacher',
                        'courses_current',
                    ])->find($student_id);
        if($student === null) {
            //生徒情報がなければ処理しない
//TODO:ここのエラーの処理はこれで大丈夫かどうか
            return \Redirect::back()->withErrors(array('エラーが発生しました。'));;
        }

        $term = Term::find($term_id);
        //期間中の来塾予定日
        $visit_dates = \JJSS::getVisitDates($student_id, $term_id);

        //期間中の全日数の配列を作成
        $term_dates = [];
        $term_date = Carbon::parse($term->start_date);
        while($term_date->lte(Carbon::parse($term->end_date))) {
            $term_dates[] = $term_date->format('Y-m-d');
            $term_date = $term_date->addDay();
        }

        //
        // 学習教科書を算出 start
        //
        $kamoku_id = $request->input('kamoku_id');

        //はじめのテキスト情報
        $start_text_id = $request->input('start_text_id');
        $start_text_all_kaisuu = $request->input('start_text_all_kaisuu');
        $start_text_kaisuu = $request->input('start_text_kaisuu');
        $reverse_test = $request->input("reverse_test");
        $hon_flag = $request->input('hon_flag');//0:次へ繋げる、1:現在の教科書でおしまい

        //毎回○回
        $maikai_kaisuu = $request->input('maikai_kaisuu');

        $maikai_kaisuu_1 = $request->input('maikai_kaisuu_1');//月
        $maikai_kaisuu_2 = $request->input('maikai_kaisuu_2');//火
        $maikai_kaisuu_3 = $request->input('maikai_kaisuu_3');//水
        $maikai_kaisuu_4 = $request->input('maikai_kaisuu_4');//木
        $maikai_kaisuu_5 = $request->input('maikai_kaisuu_5');//金

        //おわりのテキスト情報
        $end_text_id = $request->input('end_text_id');
        $end_text_all_kaisuu = $request->input('end_text_all_kaisuu');
        $end_text_kaisuu = $request->input('end_text_kaisuu');

        $student_monthly_texts = [];

        $c = count($kamoku_id);

        for($i = 0; $i < $c; $i++) {//科目毎にぐるぐる

            //本流
            //学習する教科書・回数をセット
            $start_text = TEXT::where('id', '=', $start_text_id[$i])->first();
            $texts = null;
            if($start_text) {
                if (strlen($start_text->sequence) > 0) {
                    $texts = TEXT::where('kamoku_id', '=', $kamoku_id[$i])
                        ->whereNotNull('sequence')
                        ->where('sequence', '>=', $start_text->sequence)
                        ->orderBy('sequence', 'asc')
                        ->get();
                }
            }
            if(!$texts) {
                continue;
            }

            //進め方($susume_type 0:毎回○回、1:終了テキストと回数を指定)
            $start_info = [
                        'text_id' => $start_text_id[$i],
                        'reverse_test' => $reverse_test[$i],
                        'all_kaisuu' => $start_text_all_kaisuu[$i],
                        'start_kaisuu' => $start_text_kaisuu[$i],
                        'hon_flag' => $hon_flag[$i]
                    ];
            //終了テキスト指定
            if(strlen($end_text_id[$i]) > 0 && strlen($end_text_all_kaisuu[$i]) > 0 && strlen($end_text_kaisuu[$i]) > 0) {
                $end_info = [
                        'text_id' => $end_text_id[$i],
                        'all_kaisuu' => $end_text_all_kaisuu[$i],
                        'end_kaisuu' => $end_text_kaisuu[$i]
                    ];
                $r = $this->calculate_learn_text_units1($texts, $start_info, $end_info);
                $learn_texts = $r['learn_texts'];
                $all_learning_kaisuu = $r['all_learning_kaisuu'];
            //毎回の回数指定
            } else {
                $week_kaisuu = [];
                foreach(config('jjss.weeks_alpha') as $week_k => $week_v) {
                    $nm = "maikai_kaisuu_" . $week_k;
                    $w = $$nm;
                    if($w[$i]) {
                        if(strlen($w[$i]) > 0) {
                            $week_kaisuu[$week_k] = $w[$i];
                        }
                    }
                }
                $r = $this->calculate_learn_text_units2($texts, $start_info, $maikai_kaisuu[$i], $week_kaisuu, $visit_dates, $term_dates);
                $learn_texts = $r['learn_texts'];
                $all_learning_kaisuu = $r['all_learning_kaisuu'];
            }

            //テキスト
            if(count($texts) > 0) {
                foreach($texts as $text) {

                    if(isset($learn_texts[$text->id]) === false) {
                        continue;
                    }

                    $_text = [];
                    $_text['kamoku_id'] = $text->kamoku_id;
                    $_text['kamoku_name'] = $text->kamoku->name;
                    $_text['text_id'] = $text->id;
                    $_text['reverse_test'] = $reverse_test[$i];
                    $_text['text_name'] = $text->name;
                    $_text['units_by_student'] = $text->units_by_student;
                    $_text['text_all_kaisuu'] = $learn_texts[$text->id]['all_kaisuu'];
                    $_text['keikaku_suu'] = $learn_texts[$text->id]['keikaku_suu'];

                    $_maikai_info = [];
                    $_maikai_info['maikai_kaisuu'] = $maikai_kaisuu[$i];
                    foreach(config('jjss.weeks_alpha') as $week_k => $week_v) {
                        $nm = "maikai_kaisuu_" . $week_k;
                        $w = $$nm;
                        $_maikai_info[$nm] = '';
                        if($w[$i]) {
                            if(strlen($w[$i]) > 0) {
                                $_maikai_info[$nm] = $w[$i];
                            }
                        }
                    }
                    $_text = array_merge($_text, $_maikai_info);

                    $_text['text_units'] = $learn_texts[$text->id]['text_units'];

                    $all_learning_kaisuu = $all_learning_kaisuu - $_text['keikaku_suu'];

                    $student_monthly_texts[] = $_text;

                    if($reverse_test[$i] == "1" || (int)$_text['keikaku_suu'] == 0) {
                        //このテキスト自体が逆テストならここまで
                        continue;
                    }

                    //kamoku_id=7 かつ 毎回回数指定 の場合は枝テキストや逆テキスト(通塾日ベース)のために残り回数を算出する
                    $nokori_learning_kaisuu = $all_learning_kaisuu;
                    if($kamoku_id[$i] == "7" && isset($week_kaisuu) === true) {

                        $re_term_dates = array_reverse($term_dates);//期間の日付を逆に並べ替えたもの
                        $re_start_date = '';
                        foreach($re_term_dates as $re_term_date) {
                            if(count($week_kaisuu) > 0) {
                                $week_no = Carbon::parse($re_term_date)->dayOfWeek;
                                if(isset($week_kaisuu[$week_no])) {
                                    $nokori_learning_kaisuu = $nokori_learning_kaisuu - (int)$week_kaisuu[$week_no];
                                }
                            } else {
                                $nokori_learning_kaisuu = $nokori_learning_kaisuu - (int)$maikai_kaisuu[$i];
                            }

                            if($nokori_learning_kaisuu > 0) {//この日学習する回数を差し引いて残りが1以上なら次ループの日も学習する
                                $re_start_date = $re_term_date;
                            }
                            if($nokori_learning_kaisuu <= 0) {
                                break;
                            }
                        }

                        $nokori_learning_kaisuu = 0;
                        if(strlen($re_start_date) > 0) {
                            foreach($visit_dates as $visit_date) {
                                if(Carbon::parse($re_start_date) <= Carbon::parse($visit_date)) {
                                    if (count($week_kaisuu) > 0) {
                                        $week_no = Carbon::parse($visit_date)->dayOfWeek;
                                        if(isset($week_kaisuu[$week_no]) === true) {
                                            $nokori_learning_kaisuu = $nokori_learning_kaisuu + (int)$week_kaisuu[$week_no];
                                        }
                                    } else {
                                        $nokori_learning_kaisuu = $nokori_learning_kaisuu + (int)$maikai_kaisuu[$i];
                                    }
                                }
                            }
                        }
                    }

                    //分流(branch_next_text_id)
                    $branch_text_plan = $this->sub_text_plan($text->id, 'branch', $nokori_learning_kaisuu, $_maikai_info, $visit_dates, $term_dates);
                    if(count($branch_text_plan) > 0) {
                        $student_monthly_texts = array_merge($student_monthly_texts, $branch_text_plan);
                    }

                    //逆テスト(has_reverse_test = "1")
                    $gyaku_text_plan = $this->sub_text_plan($text->id, 'reverse', $nokori_learning_kaisuu, $_maikai_info, $visit_dates, $term_dates);
                    if(count($gyaku_text_plan) > 0) {
                        $student_monthly_texts = array_merge($student_monthly_texts, $gyaku_text_plan);
                    }
                }
            }
        }

        $term = Term::where('id', '=', $term_id)->first();
        $kamokus = Kamoku::all();
        $user = \Auth::user();

        return view('plan.monthly.detail', [
                                            'student' => $student,
                                            'term' => $term,
                                            'kamokus' => $kamokus,
                                            //'user' => $user,
                                            'student_monthly_texts' => $student_monthly_texts,
                                            'visit_dates' => $visit_dates,
                                            'visit_week_days' => \JJSS::getVisitWeekDays($student->id, $term_id),
                                            ]
                );
    }


    //おわりのテキスト指定ありの場合のtext_unit求める
    public function calculate_learn_text_units1($texts, $start_info = [], $end_info = [])
    {
        $learn_texts = [];
        $all_learning_kaisuu = 0;//この期間で学習する回数の合計
        if(count($start_info) == 0 || count($end_info) == 0) {
            return ['learn_texts' => $learn_texts, 'all_learning_kaisuu' => $all_learning_kaisuu];
        }

        if(count($texts) > 0) {
            $end_flag = 0; //0:まだ、 1:おわり
            foreach($texts as $text) {
                if($end_flag == 1) {
                    break;
                }

                $text_units = [];
                $text_all_kaisuu = '';
                $keikaku_suu = 0;

                //全回数固定の場合
                if($text->units_by_student == "0" && isset($text->text_units) === true) {
                    foreach($text->text_units as $text_unit) {

                        $_data = [];
                        $_data['unit_num'] = $text_unit->unit_num;
                        $_data['unit_name'] = $text_unit->name;
                        $_data['display_flag'] = 0;//0:表示, 1:非表示
                        if($text->id == $start_info['text_id']) {
                            if($text_unit->unit_num < (int)$start_info['start_kaisuu']) {
                                $_data['display_flag'] = 1;//開始テキストの開始回未満は非表示にする
                            }
                        }
                        if($text->id == $end_info['text_id']) {
                            if($text_unit->unit_num == $end_info['end_kaisuu']) {
                                $end_flag = 1;
                            }

                            if($text_unit->unit_num > $end_info['end_kaisuu']) {
                                $_data['display_flag'] = 1;//終了テキストの終了回より大きい回数は非表示にする
                            }
                        }

                        if($text_unit->unit_num == 1) {
                            $text_units[] = [
                                        'unit_num' => 0,
                                        'unit_name' => ($text->kamoku_id == "7" ? "カウント" : "初回"),
                                        'display_flag' => $_data['display_flag']
                                    ];
                        }

                        $text_units[] = $_data;

                        if($_data['display_flag'] == 0) {
                            $all_learning_kaisuu++;
                            $keikaku_suu++;
                        }
                    }
                    $text_all_kaisuu = count($text->text_units);
                }
                //全回数指定の場合
                if($text->units_by_student == "1") {

                    if ($text->id == $start_info['text_id']) {
                        $text_all_kaisuu = $start_info['all_kaisuu'];
                    } else if ($text->id == $end_info['text_id']) {
                        $text_all_kaisuu = $end_info['all_kaisuu'];
                    }

                    if((int)$text_all_kaisuu > 0) {
                        for ($i = 1; $i <= (int)$text_all_kaisuu; $i++) {

                            $_data = [];
                            $_data['unit_num'] = $i;
                            $_data['unit_name'] = $i;
                            $_data['display_flag'] = 0;//0:表示, 1:非表示
                            if($text->id == $start_info['text_id']) {
                                if($i < (int)$start_info['start_kaisuu']) {
                                    $_data['display_flag'] = 1;//開始テキストの開始回未満は非表示にする
                                }
                            }
                            if($text->id == $end_info['text_id']) {
                                if($i == $end_info['end_kaisuu']) {
                                    $end_flag = 1;
                                }
                                if($i > $end_info['end_kaisuu']) {
                                    $_data['display_flag'] = 1;//終了テキストの終了回より大きい回数は非表示にする
                                }
                            }

                            if($i == 1) {
                                $text_units[] = [
                                    'unit_num' => 0,
                                    'unit_name' => ($text->kamoku_id == "7" ? "カウント" : "初回"),
                                    'display_flag' => $_data['display_flag']
                                ];
                            }

                            $text_units[] = $_data;

                            if($_data['display_flag'] == 0) {
                                $all_learning_kaisuu++;
                                $keikaku_suu++;
                            }
                        }
                    }
                }

                $learn_texts[$text->id] = ['all_kaisuu' => $text_all_kaisuu, 'text_units' => $text_units, 'keikaku_suu' => $keikaku_suu];
                if($end_flag == "1") {
//                if(strlen($text_all_kaisuu) == 0) {
                    break;
                }
                //逆テストの場合は次へは続かないので１教材分の情報のみでよい
                if($start_info['reverse_test'] == "1" && count($learn_texts) == 1) {
                    break;
                }
            }
        }
        return ['learn_texts' => $learn_texts, 'all_learning_kaisuu' => $all_learning_kaisuu];
    }


    //毎回何回の指定ありの場合のtext_unit求める
    public function calculate_learn_text_units2($texts, $start_info = [], $maikai_kaisuu = 0, $week_kaisuu = [], $visit_dates = [], $term_dates = [])
    {
        $learn_texts = [];
        $all_learning_kaisuu = 0;//$all_kaisuu
        if(count($start_info) == 0 || ((int)$maikai_kaisuu == 0 && count($week_kaisuu) == 0)) {
            return ['learn_texts' => $learn_texts, 'all_learning_kaisuu' => $all_learning_kaisuu];
        }

        //全学習回数を求める（ここでは来塾日ベースで求める。kamoku_id=7(単語)の場合はforeach1周目で日にちベースに再算出する）
        if(count($week_kaisuu) > 0) {
            $wc = count($visit_dates);
            for($wi = 0; $wi < $wc; $wi++) {
                $week_no = Carbon::parse($visit_dates[$wi])->dayOfWeek;
                if(in_array($week_no, array_keys($week_kaisuu)) === true) {
                    $all_learning_kaisuu = $all_learning_kaisuu + (int)$week_kaisuu[$week_no];
                }
            }
        } else if($maikai_kaisuu > 0) {
            $all_learning_kaisuu = $maikai_kaisuu * count($visit_dates);
        }

        if(count($texts) > 0) {
            $_all_kaisuu = '';
            $end_flag = 0;//0:続く、1:終了
            foreach($texts as $text) {

                //1周目のみ処理
                if(strlen($_all_kaisuu) == 0) {
                    //英単語特訓(kamoku_id=7)だけは毎日学習するのが基本
                    if ($text->kamoku_id == "7") {
                        $all_learning_kaisuu = 0;
                        if (count($week_kaisuu) > 0) {
                            $tc = count($term_dates);
                            for ($ti = 0; $ti < $tc; $ti++) {
                                $week_no = Carbon::parse($term_dates[$ti])->dayOfWeek;
                                if (in_array($week_no, array_keys($week_kaisuu)) === true) {
                                    $all_learning_kaisuu = $all_learning_kaisuu + (int)$week_kaisuu[$week_no];
                                }
                            }
                        } else if ($maikai_kaisuu > 0) {
                            $all_learning_kaisuu = $maikai_kaisuu * count($term_dates);
                        }
                    }
                    $_all_kaisuu = $all_learning_kaisuu;
                }

                if($_all_kaisuu <= 0 || $end_flag == 1) {
                    break;
                }

                $text_units = [];
                $text_all_kaisuu = '';//指定がない 場合は空欄
                $keikaku_suu = 0;

                //全回数固定の場合
                if($text->units_by_student == "0" && isset($text->text_units) === true) {
                    foreach($text->text_units as $text_unit) {

                        $_data = [];
                        $_data['unit_num'] = $text_unit->unit_num;
                        $_data['unit_name'] = $text_unit->name;
                        $_data['display_flag'] = 0;//0:表示, 1:非表示
                        if($text->id == $start_info['text_id']) {
                            if($text_unit->unit_num < (int)$start_info['start_kaisuu']) {
                                $_data['display_flag'] = 1;//開始テキストの開始回未満は非表示にする
                            }
                        }
                        if($end_flag == 1) {
                            $_data['display_flag'] = 1;//開始テキストの開始回未満は非表示にする
                        }

                        if($text_unit->unit_num == 1) {
                            $text_units[] = [
                                'unit_num' => 0,
                                'unit_name' => ($text->kamoku_id == "7" ? "カウント" : "初回"),
                                'display_flag' => $_data['display_flag']
                            ];
                        }

                        $text_units[] = $_data;

                        if($_data['display_flag'] == 0) {
                            $_all_kaisuu--;
                            $keikaku_suu++;
                        }
                        if($_all_kaisuu <= 0) {
                            $end_flag = 1;
                        }
                    }
                    $text_all_kaisuu = count($text->text_units);//初回(0回目)は省いた全回数
                }
                //全回数指定の場合
                if($text->units_by_student == "1") {

                    if ($text->id == $start_info['text_id']) {
                        $text_all_kaisuu = $start_info['all_kaisuu'];
                    }

                    if((int)$text_all_kaisuu > 0) {
                        for ($i = 1; $i <= (int)$text_all_kaisuu; $i++) {

                            $_data = [];
                            $_data['unit_num'] = $i;
                            $_data['unit_name'] = $i;
                            $_data['display_flag'] = 0;//0:表示, 1:非表示
                            if($text->id == $start_info['text_id']) {
                                if($i < (int)$start_info['start_kaisuu']) {
                                    $_data['display_flag'] = 1;//開始テキストの開始回未満は非表示にする
                                }
                            }
                            if($end_flag == 1) {
                                $_data['display_flag'] = 1;//開始テキストの開始回未満は非表示にする
                            }

                            if($i == 1) {
                                $text_units[] = [
                                    'unit_num' => 0,
                                    'unit_name' => ($text->kamoku_id == "7" ? "カウント" : "初回"),
                                    'display_flag' => $_data['display_flag']
                                ];
                            }

                            $text_units[] = $_data;

                            if($_data['display_flag'] == 0) {
                                $_all_kaisuu--;
                                $keikaku_suu++;
                            }
                            if($_all_kaisuu <= 0) {
                                $end_flag = 1;
                            }
                        }
                    }
                }

                $learn_texts[$text->id] = ['all_kaisuu' => $text_all_kaisuu, 'text_units' => $text_units, 'keikaku_suu' => $keikaku_suu];
                if(strlen($text_all_kaisuu) == 0) {
                    break;
                }
                if($start_info['reverse_test'] == "1" && count($learn_texts) == 1) {
                    break;
                }
                if($start_info['hon_flag'] == "1" && count($learn_texts) == 1) {
                    break;
                }
                if($text->kamoku_id == "8" && count($learn_texts) == 1) {
                    break;
                }

            }
        }
        return ['learn_texts' => $learn_texts, 'all_learning_kaisuu' => $all_learning_kaisuu];
    }



    // >> $text_id : テキストID
    //    $type : 'reverce'(default) = 逆テストあるかどうか, 'branch' = 枝わかれテキスト
    //    $susumu_all_kaisuu : 残り何回あるか
    //    $visit_dates : array 通塾予定日
    //    $term_dates : array 期間の全部の日付
    //
    // << array (学習するテキストの情報)
    public function sub_text_plan($text_id = '', $type = 'reverse', $susumu_all_kaisuu = 0, $_maikai_info = array(),
                                  $visit_dates = array(), $term_dates = array())
    {
        $r = array();
        if(strlen($text_id) == 0 || $susumu_all_kaisuu <= 0 || count($visit_dates) == 0 || count($term_dates) == 0) {
            return $r;
        }

        $text = Text::find($text_id);

        if($type == 'reverse') {
            if($text->has_reverse_test == "1") {
                $learn_text = $text;
            }
        }
        if($type == 'branch') {
            $branch_text = null;
            if($text->branch_next_text_id) {
                if(strlen($text->branch_next_text_id) > 0) {
                    $branch_text = Text::find($text->branch_next_text_id);
                }
            }
            if(!$branch_text) {
                return $r;
            }
            $learn_text = $branch_text;
        }

        if(isset($learn_text) === false) {
            return $r;
        }

        $_r = [];
        $_r['kamoku_id'] = $learn_text->kamoku_id;
        $_r['kamoku_name'] = $learn_text->kamoku->name;
        $_r['text_id'] = $learn_text->id;
        $_r['reverse_test'] = ($type == "reverse" ? "1" : "0");
        $_r['text_name'] = $learn_text->name;
        $_r['units_by_student'] = $learn_text->units_by_student;

        $_r = array_merge($_r, $_maikai_info);

        if($learn_text->units_by_student == "0") {//学習回数固定
            if ($learn_text->text_units) {
                $keikaku_suu = 0;
                foreach ($learn_text->text_units as $text_unit) {
                    $_data = [];
                    $_data['unit_num'] = $text_unit->unit_num;
                    $_data['unit_name'] = $text_unit->name;
                    $_data['display_flag'] = 0;//0:表示、1:非表示
                    $susumu_all_kaisuu = $susumu_all_kaisuu - 1;
                    if($susumu_all_kaisuu < 0) {
                        $_data['display_flag'] = 1;
                    }

                    $_r['text_units'][] = $_data;

                    if($_data['display_flag'] == 0) {
                        $keikaku_suu++;
                    }
                }
                $_r['text_all_kaisuu'] = count($learn_text->text_units);
                $_r['keikaku_suu'] = $keikaku_suu;
            }
        }

        if(isset($_r['text_units']) === false) {//学習回数変動 => 手動で決めてもらう
            $_r['text_all_kaisuu'] = '';
            $_r['text_units'] = [];
        }

        $r[] = $_r;

        if($type == 'branch' && $susumu_all_kaisuu > 0 && $learn_text->has_reverse_test == "1") {
            $_r['text_name'] = $learn_text->name;

            if($learn_text->units_by_student == "0") {//学習回数固定
               $_r['text_units'] = [];
               $_r['keikaku_suu'] = '';
               if ($learn_text->text_units) {
                    $keikaku_suu = 0;
                    foreach ($learn_text->text_units as $text_unit) {
                        $_data = [];
                        $_data['unit_num'] = $text_unit->unit_num;
                        $_data['unit_name'] = $text_unit->name;
                        $_data['display_flag'] = 0;//0:表示、1:非表示
                        $susumu_all_kaisuu = $susumu_all_kaisuu - 1;
                        if($susumu_all_kaisuu < 0) {
                            $_data['display_flag'] = 1;
                        }

                        $_r['text_units'][] = $_data;

                        if($_data['display_flag'] == 0) {
                            $keikaku_suu++;
                        }
                    }
                    $_r['text_all_kaisuu'] = count($learn_text->text_units);
                    $_r['keikaku_suu'] = $keikaku_suu;

                    $_r = array_merge($_r, $_maikai_info);

               }
            }
            $r[] = $_r;
        }

        return $r;
    }
}
