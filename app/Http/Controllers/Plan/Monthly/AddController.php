<?php

namespace App\Http\Controllers\Plan\Monthly;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Student;
use App\Models\Term;
use App\Models\Kamoku;

class AddController extends Controller
{
    public function __invoke(Request $request)
    {
        //校長、講師は担当校舎の情報のみを取り扱う
        if(in_array(\JJSS::getPermission(), array('校長', '講師'))) {
            $tantou_school_id = \Auth::user()->teacher->tantou_school_id;
            $students = Student::where('school_id', '=', $tantou_school_id)->get();
        } else {
            $students = Student::all();
        }

        $terms = Term::all();
        $kamokus = Kamoku::all();

        $user = \Auth::user();

        return view('plan.monthly.add', [
                                            'students' => $students,
                                            'terms' => $terms,
                                            'kamokus' => $kamokus,
                                            'user' => $user
                                            ]
                );
    }
}
