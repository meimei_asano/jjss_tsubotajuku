<?php

namespace App\Http\Controllers\HQ\Invoice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Facades\CustomMpdf;
use Illuminate\Validation\Rules\In;

class PrintController extends Controller
{
    public function __invoke(int $invoice_id)
    {
        $invoice = Invoice::find($invoice_id);

        if (! $invoice) {
            return redirect('hq/invoice');
        }

        $html = $this->getPDFBody($invoice);

        CustomMpdf::generate($html, $this->getPDFConfig(), '請求書');

        // 発行済にステータス変更
        $invoice->printed_flag = true;
        $invoice->save();
    }

    public function multi(Request $request)
    {
        $invoice_ids = explode(',', $request->input('invoice_ids'));
        $invoices = Invoice::whereIn('id', $invoice_ids)->get();

        if (! $invoices) {
            return redirect('hq/invoice');
        }

        $htmls = [];
        foreach ($invoices as $invoice) {
            $htmls[] = $this->getPDFBody($invoice);
        }
        $html = implode('<pagebreak>', $htmls);

        CustomMpdf::generate($html, $this->getPDFConfig(), '請求書');

        // 発行済にステータス変更
        foreach ($invoices as $invoice) {
            $invoice->printed_flag = true;
            $invoice->save();
        }
    }

    public function getPDFBody($invoice)
    {
        $main = view('hq.invoice.print',
            [
                'invoice' => $invoice,
                'is_hikae' => false,
            ]
        );

        $sub = view('hq.invoice.print',
            [
                'invoice' => $invoice,
                'is_hikae' => true,
            ]
        );

        return $main. '<pagebreak>'. $sub;
    }

    public function getPDFConfig()
    {
        $config = [
            'format' => 'A4',
            'margin_left' => 10, //余白(左)
            'margin_right' => 10, //余白(右)
            'margin_top' => 10, //余白(上)
            'margin_bottom' => 10, //余白(下)
            'margin_header' => 5, //余白(ヘッダー)
            'margin_footer' => 5, //余白(フッター)
        ];

        return $config;
    }
}
