<?php

namespace App\Http\Controllers\HQ\Invoice;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use Illuminate\Http\Request;
use Validator;

class ImportController extends Controller
{
    private $format = [
        [
            'key' => '校舎コード',
            'column' => 'school_code',
            'validation' => 'required|regex:/^\d{3}$/',
        ],
        [
            'key' => '生徒コード',
            'column' => 'student_code',
            'validation' => 'required|regex:/^\d{3}-\d{2}-\d{3}$/',
        ],
        [
            'key' => '請求日',
            'column' => 'invoice_date',
            'validation' => 'required|multi_date_format:"Y-m-d","Y/m/d"',
        ],
        [
            'key' => '支払期日',
            'column' => 'payment_date',
            'validation' => 'required|multi_date_format:"Y-m-d","Y/m/d"',
        ],
        [
            'key' => '内訳行番号',
            'column' => 'row_num',
            'validation' => 'required|integer|between:0,10',
        ],
        [
            'key' => '内訳',
            'column' => 'item',
            'validation' => 'required|string',
        ],
        [
            'key' => '税抜',
            'column' => 'price',
            'validation' => 'required|integer|between:0,10000000000',
        ],
        [
            'key' => '税率',
            'column' => 'tax_rate',
            'validation' => 'numeric',
        ],
        [
            'key' => '消費税額',
            'column' => 'tax',
            'validation' => 'integer|between:0,10000000000',
        ],
        [
            'key' => '税込',
            'column' => 'included',
            'validation' => 'integer|between:0,10000000000',
        ],
        [
            'key' => '合計',
            'column' => 'total',
            'validation' => 'integer|between:0,10000000000',
        ],
    ];

    public function __invoke(Request $request)
    {
        $file = $request->file->store('csv');
        $path = storage_path('app'). '/'. $file;

        // csvファイル読み込み
        $csvStr = file_get_contents($path);
        $csvEncoding = $this->getEncoding($csvStr, $encodes = array('SJIS-win','UTF-8'));
        file_put_contents($path, mb_convert_encoding($csvStr, 'UTF-8', $csvEncoding));
        $csv = new \SplFileObject($path);
        $csv->setFlags(\SplFileObject::READ_CSV | \SplFileObject::READ_AHEAD | \SplFileObject::SKIP_EMPTY);

        // ヘッダが合致するか確認する
        $header = $csv->current();
        if (! $this->headerValidation($header) ) {
            $message  = '取り込もうとしているデータの1行目のヘッダ部分が合致しません。CSVに正しくデータが入っているか確認してください。';
            @unlink($path);
            return redirect('hq/invoice')->withErrors([$message]);
        }

        // 取込対象を特定
        $data = [];
        $fail = [];
        $currentRow = 0;
        foreach($csv as $row) {
            $currentRow++;
            // ヘッダ行スキップ
            if ($currentRow == 1) continue;
            // データvalidation
            $row = $this->setKeyColumn($row);
            if (($result = $this->dataValidation($row)) !== true) {
                $fail[$currentRow] = $currentRow. '行目: '. "$result";
            }
            $data[$currentRow] = $row;
        }

        // 取込処理
        $result = $this->import($data, $fail);
        if ($result['fail']) {
            @unlink($path);
            return redirect('hq/invoice')->withErrors($result['fail']);
        }

        @unlink($path);
        return redirect('hq/invoice')->with('alert.success', $result['saves'].'件の請求データ取込が完了しました。');
    }

    protected function headerValidation(array $header)
    {
        $keys = array_column($this->format, 'key');
        return ($header == $keys);
    }

    /**
     * 配列データのキーをformatのcolumnで振り直す
     */
    protected function setKeyColumn(array $data)
    {
        $columns = array_column($this->format, 'column');
        $return = [];
        foreach ($data as $k => $v) {
            $return[ $columns[$k] ] = $v;
        }
        return $return;
    }

    protected function dataValidation(array $row)
    {
        $validation = array_column($this->format, 'validation');
        $validation = $this->setKeyColumn($validation);

        $v = Validator::make($row, $validation);
        if ($v->fails()) {
            $message = '';
            foreach ($v->errors()->all() as $m) {
                $message .= $m.' ';
            }
            $message = rtrim($message);
            return $message;
        }
        return true;
    }

    protected function dataValidationWhenFirstRow(array $row)
    {
        $validation = [
            'total' => 'required',
        ];

        $v = Validator::make($row, $validation);
        if ($v->fails()) {
            $message = '';
            foreach ($v->errors()->all() as $m) {
                $message .= $m.' ';
            }
            $message = rtrim($message);
            return $message;
        }
        return true;
    }

    protected function import(array $data, array $fail)
    {
        $saves = 0;

        \DB::beginTransaction();
        try {
            $check_row_num = 1;
            foreach ($data as $rowNo => $row) {
                if ($row['row_num'] == 1) {
                    $failed = false;

                    // 1行目の場合は 請求小計,消費税率,消費税額,請求合計 必須
                    if (($result = $this->dataValidationWhenFirstRow($row)) !== true) {
                        if (isset($fail[$rowNo])) {
                            $fail[$rowNo] .= " ". $result;
                        } else {
                            $fail[$rowNo] = $rowNo . '行目: ' . $result;
                        }
                        $failed = true;
                    }

                    $school_id = \JJSS::getSchoolIDbyCode($row['school_code']);
                    if (! $school_id) {
                        $f = "指定された校舎コードに一致する校舎が存在しませんでした(校舎コード：{$row['school_code']})。";
                        if (isset($fail[$rowNo])) {
                            $fail[$rowNo] .= " ". $f;
                        } else {
                            $fail[$rowNo] = $rowNo . '行目: ' . $f;
                        }
                        $failed = true;
                    }
                    $student_id = \JJSS::getStudentIDbyCode($row['student_code']);
                    if (! $student_id) {
                        $f = "指定された生徒コードに一致する生徒データが存在しませんでした(生徒コード：{$row['student_code']})。";
                        if (isset($fail[$rowNo])) {
                            $fail[$rowNo] .= " ". $f;
                        } else {
                            $fail[$rowNo] = $rowNo . '行目: ' . $f;
                        }
                        $failed = true;
                    }

                    if (! $failed) {
                        $invoice = $this->saveInvoice($row, $school_id, $student_id);
                        $saves++;
                    }
                    $check_row_num = 1;
                }

                if ($check_row_num++ != $row['row_num']) {
                    // row_numが連番でない場合
                    $f = '内訳行番号が連番になっていません。';
                    if (isset($fail[$rowNo])) {
                        $fail[$rowNo] .= " ". $f;
                    } else {
                        $fail[$rowNo] = $rowNo . '行目: ' . $f;
                    }
                    $failed = true;
                }

                if (! $failed) {
                    $invoice_detail = $this->saveInvoiceDetail($invoice->id, $row);
                }
            }

            if (count($fail) == 0) {
                \DB::commit();
            } else {
                \DB::rollback();
            }
        } catch (\Exception $e) {
            $fail['db'] = 'データベース登録中にエラーが発生しました。'. $e->getMessage();
            \DB::rollback();
        }

        return [
            'saves' => $saves,
            'fail' => $fail,
        ];
    }

    protected function saveInvoice($row, $school_id, $student_id) {
        $invoice = new Invoice;
        $invoice->school_id = $school_id;
        $invoice->student_id = $student_id;
        $invoice->invoice_date = $row['invoice_date'];
        $invoice->payment_date = $row['payment_date'];
        $invoice->hikiotoshi_yotei_date = NULL;
        $invoice->hikiotoshi_flag = 0;
        $invoice->subtotal = NULL;
        $invoice->tax_rate = NULL;
        $invoice->tax = NULL;
        $invoice->total = $row['total'];
        $invoice->checked_teacher_id = 0;
        $invoice->printed_flag = 0;
        $invoice->confirmed_flag = 0;

        $invoice->save();
        return $invoice;
    }

    protected function saveInvoiceDetail($invoice_id, $row) {
        $invoice_detail = new InvoiceDetail;
        $invoice_detail->invoice_id = $invoice_id;
        $invoice_detail->row_num = $row['row_num'];
        $invoice_detail->item = $row['item'];
        $invoice_detail->price = $row['price'];
        $invoice_detail->tax_rate = $row['tax_rate'];
        $invoice_detail->tax = $row['tax'];
        $invoice_detail->included = $row['included'];

        $invoice_detail->save();
        return $invoice_detail;
    }

    private function getEncoding($str, $encodes = array('UTF-8','SJIS-win'))
    {
        $encoding = false;
        foreach($encodes as $checkEncoding) {
            // エンコード変換前後の文字列が合致するかでエンコードを判定する
            if (mb_convert_encoding($str, $checkEncoding, $checkEncoding) == $str) {
                $encoding = $checkEncoding;
                break;
            }
        }
        return $encoding;
    }
}
