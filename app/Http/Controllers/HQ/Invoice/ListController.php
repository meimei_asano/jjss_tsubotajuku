<?php

namespace App\Http\Controllers\HQ\Invoice;

use App\Http\Controllers\Controller;
use App\Models\Invoice;
use App\Models\School;
use Illuminate\Http\Request;

class ListController extends Controller
{
    public function __invoke(Request $request)
    {
        $search = [];
        $orderby = [];
        $studentName = $request->input('student_name') ?: '';
        $schoolId = $request->input('school_id') ?: '';
        $invoiceStart = $request->input('invoice_date_start') ?: '';
        $invoiceEnd = $request->input('invoice_date_end') ?: '';
        $paymentStart = $request->input('payment_date_start') ?: '';
        $paymentEnd = $request->input('payment_date_end') ?: '';
        $checkedTeacherId = $request->input('checked_teacher_id') ?: 0;
        $printedFlag = $request->input('printed_flag') ?: 0;
        $confirmedFlag = $request->input('confirmed_flag') ?: 0;

        if ($invoiceStart) {
            $search['invoice_date_start'] = $invoiceStart;
            $invoiceStart = (\JJSS::getTargetDate($invoiceStart) === false) ? '' : $invoiceStart;
        }

        if ($invoiceEnd) {
            $search['invoice_date_end'] = $invoiceEnd;
            $invoiceEnd = (\JJSS::getTargetDate($invoiceEnd) === false) ? '' : $invoiceEnd;
        }

        if ($paymentStart) {
            $search['payment_date_start'] = $paymentStart;
            $paymentStart = (\JJSS::getTargetDate($paymentStart) === false) ? '' : $paymentStart;
        }

        if ($paymentEnd) {
            $search['payment_date_end'] = $paymentEnd;
            $paymentEnd = (\JJSS::getTargetDate($paymentEnd) === false) ? '' : $paymentEnd;
        }

        $query = Invoice::select('*', 'invoices.id AS id')
        //$query = Invoice::select()//, 'invoice.id AS id')
            ->with([
                'school',
                'student',
            ]);
        $query->leftJoin('students', 'invoices.student_id', '=', 'students.id');

        //search
        if ($studentName) {
            $search['student_name'] = $studentName;
            $query->whereHas('student', function ($q) use ($studentName) {
                $q->where('students.name', 'like', '%' . $studentName . '%');
            });
        }

        if ($schoolId) {
            $search['school_id'] = $schoolId;
            $query->where('invoices.school_id', $schoolId);
        }

        if ($invoiceStart) {
            $query->where('invoices.invoice_date', '>=', $invoiceStart);
        }

        if ($invoiceEnd) {
            $query->where('invoices.invoice_date', '<=', $invoiceEnd);
        }

        if ($paymentStart) {
            $query->where('invoices.payment_date', '>=', $paymentStart);
        }

        if ($paymentEnd) {
            $query->where('payment_date', '<=', $paymentEnd);
        }

        if ($checkedTeacherId) {
            $search['checked_teacher_id'] = $checkedTeacherId;
            $query->where('invoices.checked_teacher_id', '<>', 0);
        } else {
            $query->where('invoices.checked_teacher_id', $checkedTeacherId);
        }

        if ($printedFlag) {
            $search['printed_flag'] = $printedFlag;
            $query->where('invoices.printed_flag', $printedFlag);
        } else {
            $query->where('invoices.printed_flag', 0);
        }

        if ($confirmedFlag) {
            $search['confirmed_flag'] = $confirmedFlag;
            $query->where('invoices.confirmed_flag', $confirmedFlag);
        } else {
            $query->where('invoices.confirmed_flag', 0);
        }

        // sort
        if ($request->input('orderby_column_name')) {
            $orderby['column_name'] = $request->input('orderby_column_name');
            $orderby['type'] = $request->input('orderby_type');
            $query->orderBy($orderby['column_name'], $orderby['type']);
        }
        $invoices = $query->paginate( config('paginate.items.invoice') );

        if ($invoices->count() === 0 && $invoices->total() !== 0) {
            $lastPage = $invoices->lastPage();
            $queryData = $request->query();
            $queryData['page'] = $lastPage;
            $redirectPath = http_build_query($queryData);

            return redirect('/hq/invoice?' . $redirectPath);
        }

        return view('hq.invoice.list',
            [
                'search' => $search,
                'orderby' => $orderby,
                'invoices' => $invoices,
                'schools' => School::all()->toArray(),
            ]
        );
    }
}
