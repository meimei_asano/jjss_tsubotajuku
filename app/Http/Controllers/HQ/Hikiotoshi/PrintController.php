<?php

namespace App\Http\Controllers\HQ\Hikiotoshi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Facades\CustomMpdf;
use Illuminate\Validation\Rules\In;
use App\Models\Invoice;

class PrintController extends Controller
{
    public function __invoke(Request $request)
    {
        //$ids = explode(',', $request->input('ids'));
        //$datas = Invoice::whereIn('id', $ids)->get();

        //if (! $datas) {
        //    return redirect('hq/hikiotoshi');
        //}

        $datas = [0 => 0];

        $htmls = [];
        foreach ($datas as $data) {
            $htmls[] = $this->getPDFBody($data);
        }
        $html = implode('<pagebreak>', $htmls);

        CustomMpdf::generate($html, $this->getPDFConfig(), '引落額変更通知');

        //// 発行済にステータス変更
        //foreach ($datas as $data) {
        //    $data->printed_flag = true;
        //    $data->save();
        //}
    }

    public function getPDFBody($invoice)
    {
        $main = view('hq.hikiotoshi.print',
            [
                'invoice' => $invoice,
                'is_hikae' => false,
            ]
        );

        $sub = view('hq.hikiotoshi.print',
            [
                'invoice' => $invoice,
                'is_hikae' => true,
            ]
        );

        return $main. '<pagebreak>'. $sub;
    }

    public function getPDFConfig()
    {
        $config = [
            'format' => 'A4',
            'margin_left' => 10, //余白(左)
            'margin_right' => 10, //余白(右)
            'margin_top' => 10, //余白(上)
            'margin_bottom' => 10, //余白(下)
            'margin_header' => 5, //余白(ヘッダー)
            'margin_footer' => 5, //余白(フッター)
        ];

        return $config;
    }
}
