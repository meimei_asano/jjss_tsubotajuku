<?php

namespace App\Http\Controllers\Master\StudentSoldText;

use App\Http\Controllers\Controller;
use App\Models\StudentSoldText;
use App\Validation\StudentSoldTextValidation as Validation;
use DB;
use Illuminate\Http\Request;

class SaveController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  Illuminate\Http\Request $request
     * @return redirection with message(s)
     */
    public function __invoke(Request $request, string $type)
    {
        if ($type === 'syounin') {
            $teacher = \Auth::user()->teacher;
            $permission = $teacher->permission;

            if (!in_array($permission, ['校長', 'マネージャー', '管理者'])) {
                $request->flash();

                return \Redirect::back()->withErrors(['このリクエストを実行する権限がありません。']);
            }
        } elseif ($type !== 'save') {
            return \Redirect::back()->withErrors(['このリクエストを実行する権限がありません。']);
        }

        $result = [];
        $flashMessage = '登録しました。';
        $requestData = $request->all();
        $hasErrors = Validation::validate($requestData, $type);

        if ($hasErrors) {
            $request->flash();

            return \Redirect::back()->withErrors($hasErrors);
        }

        if ($type === 'save') {
            $result = $this->saveData($requestData);
        } elseif ($type === 'syounin') {
            $result = $this->syounin($requestData);
            $flashMessage = '承認済み。';
        }

        if ($result['error'] === 0) {
            $studentSoldText = $result['studentSoldText'];
            $request->session()->flash('alert.success', $flashMessage);

            return redirect('/master/student_sold_text/' . $studentSoldText->id);
        } else {
            $request->flash();

            return \Redirect::back()->withErrors($result['messages']);
        }
    }

    /**
     * Save information
     *
     * @param  array $requestData
     * @return array $result
     */
    private function saveData(array $requestData)
    {
        $result = ['error' => 0];
        DB::beginTransaction();
        try {
            $id = $requestData['id'];
            unset($requestData['id']);
            unset($requestData['syounin_teacher_id']);
            unset($requestData['teacher_id']);
            unset($requestData['subtotal']);
            unset($requestData['tax_rate']);
            unset($requestData['tax']);
            unset($requestData['total']);
            $subtotal = 0;
            $taxRate = config('jjss.tax_rate');
            $taxTotal = 0;
            $total = 0;

            foreach ($requestData['text_request'] as &$fields) {
                $price = intval($fields['price']);
                $tax = intval(round($price * $taxRate));
                $included = $price + $tax;
                $fields['tax_rate'] = number_format($taxRate, 2);
                $fields['tax'] = strval($tax);
                $fields['included'] = strval($included);
                $subtotal += $price;
                $taxTotal += $tax;
                $total += $included;
            }

            $requestData['subtotal'] = $subtotal;
            $requestData['tax_rate'] = $taxRate;
            $requestData['tax'] = $taxTotal;
            $requestData['total'] = $total;
            $requestData['text_request'] = json_encode($requestData['text_request'], JSON_UNESCAPED_UNICODE);

            if ($id) {
                $studentSoldText = StudentSoldText::find($id);
                $studentSoldText->update($requestData);
            } else {
                $studentSoldText = new StudentSoldText;
                $teacher = \Auth::user()->teacher;
                $requestData['teacher_id'] = $teacher->id;
                $studentSoldText->fill($requestData)->save();
            }

            DB::commit();
            $result['studentSoldText'] = $studentSoldText;
        } catch (\PDOException $e){
            DB::rollBack();
            $result['error'] = 1;
            $result['messages'] = [$e->getMessage()];
        }

        return $result;
    }

    /**
     * Update approval
     *
     * @param  array $requestData
     * @return array $result
     */
    private function syounin(array $requestData)
    {
        $result = ['error' => 0];
        $id = $requestData['id'];
        $studentSoldText = StudentSoldText::find($id);

        if ($studentSoldText->syounin_teacher_id !== null) {
            $result['error'] = 1;
            $result['messages'] = ['すでに承認されています。'];
        } else {
            DB::beginTransaction();
            try {
                $teacher = \Auth::user()->teacher;
                $studentSoldText->update(['syounin_teacher_id' => $teacher->id]);
                DB::commit();
                $result['studentSoldText'] = $studentSoldText;
            } catch (Exception $e) {
                DB::rollBack();
                $result['error'] = 1;
                $result['messages'] = [$e->getMessage()];
            }
        }

        return $result;
    }

}
