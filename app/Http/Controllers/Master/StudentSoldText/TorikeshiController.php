<?php

namespace App\Http\Controllers\Master\StudentSoldText;

use App\Http\Controllers\Controller;
use App\Models\StudentSoldText;
use App\Validation\StudentSoldTextTorikeshiValidation as Validation;
use DB;
use Illuminate\Http\Request;

class TorikeshiController extends Controller
{
    public function __invoke(Request $request)
    {

        $request->flash();

        $teacher = \Auth::user()->teacher;
        $permission = $teacher->permission;
        if (!in_array($permission, ['校長', 'マネージャー', '管理者'])) {
            return \Redirect::back()->withErrors(['このリクエストを実行する権限がありません。']);
        }

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $id = $request->input('id');

            if ($id) {
                $student_sold_text = StudentSoldText::find($id);
                $student_sold_text->syounin_teacher_id = null;
                $student_sold_text->syounin_torikeshi_shinsei_flag = 0;//承認取消申請！
                $student_sold_text->save();
            }

            DB::commit();

            $request->session()->flash('alert.success', '承認を取消しました。');
            return redirect('/master/student_sold_text');
            //return redirect('/master/student_sold_text/' . $student_sold_text->id);

        } catch (\PDOException $e){
            DB::rollBack();
            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }
}
