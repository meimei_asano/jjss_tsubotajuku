<?php

namespace App\Http\Controllers\Master\StudentSoldText;

use App\Http\Controllers\Controller;
use App\Validation\StudentSoldTextTorikeshiShinseiValidation as Validation;
use Illuminate\Http\Request;
use App\Models\StudentSoldText;
use DB;

class TorikeshiShinseiController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $id = $request->input('id');

            if ($id) {
                $student_sold_text = StudentSoldText::find($id);
                $student_sold_text->syounin_torikeshi_shinsei_flag = 1;//承認取消申請！
                $student_sold_text->save();
           }

            DB::commit();

            $request->session()->flash('alert.success', '申請しました。');
            return redirect('/master/student_sold_text');
            //return redirect('/master/student_sold_text/' . $student_sold_text->id);

        } catch (\PDOException $e){
            DB::rollBack();
            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }
}
