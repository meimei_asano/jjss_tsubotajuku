<?php

namespace App\Http\Controllers\Master\StudentSoldText;

use App\Http\Controllers\Controller;
use App\Validation\StudentSoldTextDeleteValidation as Validation;
use Illuminate\Http\Request;
use App\Models\StudentSoldText;
use DB;

class DeleteController extends Controller
{
    public function __invoke(Request $request)
    {
        //$request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            //ここでエラーになるのはIDが存在しない場合のみなので新規登録画面へリダイレクトする
            //return \Redirect::back()->withErrors($errors);
            return redirect('/master/student_sold_text/new')->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $id = $request->input('id');

            if ($id) {
                $student_sold_text = StudentSoldText::find($id);
                $student_sold_text->delete();
           }

            DB::commit();

            $request->session()->flash('alert.success', '申請取消しました。');
            return redirect('/master/student_sold_text');
            //return redirect('/master/student_sold_text/new');

        } catch (\PDOException $e){
            DB::rollBack();
            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }
}
