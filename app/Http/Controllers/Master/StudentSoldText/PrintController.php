<?php

namespace App\Http\Controllers\Master\StudentSoldText;

use App\Facades\CustomMpdf;
use App\Http\Controllers\Controller;
use App\Models\StudentSoldText;

class PrintController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  integer $id
     * @return view
     */
    public function __invoke(int $id)
    {
        $contents = [];
        $studentSoldText = StudentSoldText::with([
            'student',
            'student_school',
        ])->find($id);

        $contents[] = view('master.student_sold_text.teaching_materials_delivery',
            [
                'format' => 'school',
                'studentSoldText' => $studentSoldText,
            ]
        );

        $contents[] = view('master.student_sold_text.teaching_materials_delivery',
            [
                'format' => 'home',
                'studentSoldText' => $studentSoldText,
            ]
        );

        $config = [
            'format' => 'A4',
            'margin_left' => 10, //余白(左)
            'margin_right' => 10, //余白(右)
            'margin_top' => 10, //余白(上)
            'margin_bottom' => 10, //余白(下)
            'margin_header' => 5, //余白(ヘッダー)
            'margin_footer' => 5, //余白(フッター)
        ];

        CustomMpdf::generate($contents, $config);
    }
}