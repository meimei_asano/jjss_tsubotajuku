<?php

namespace App\Http\Controllers\Master\StudentSoldText;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentSoldText;
use App\Models\Teacher;
use App\Models\Text;

class DetailController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  mixed $id
     * @return view
     */
    public function __invoke($id = 'new')
    {
        if ($id !== 'new') {
            $studentSoldText = StudentSoldText::with([
                'teacher',
                'syounin_teacher'
            ])->find($id);
        } else {
            $studentSoldText = new StudentSoldText;
        }

        $teachers = Teacher::all();
        $teacher = \Auth::user()->teacher;
        $permission = $teacher->permission;
        $texts = Text::all()->toArray();

        if (in_array($permission, ['講師', '校長'])) {
            $students = Student::where('school_id', $teacher->tantou_school_id)->get();
        } else {
            $students = Student::all();
        }

        return view('master.student_sold_text.detail', [
            'id' => $id,
            'students' => $students,
            'studentSoldText' => $studentSoldText,
            'teachers' => $teachers,
            'teacher' => $teacher,
            'permission' => $permission,
            'texts' => $texts
        ]);
    }
}
