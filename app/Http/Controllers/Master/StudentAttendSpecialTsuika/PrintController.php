<?php

namespace App\Http\Controllers\Master\StudentAttendSpecialTsuika;

use App\Facades\CustomMpdf;
use App\Http\Controllers\Controller;
use App\Models\StudentAttendSpecialTsuika;

class PrintController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  integer $id
     * @return view
     */
    public function __invoke(int $id)
    {
        $studentAttendSpecialTsuika = StudentAttendSpecialTsuika::with([
            'student',
            'student_school',
            'term',
        ])->find($id);

        if ($studentAttendSpecialTsuika === null) {

            return redirect('/master/student_attend_special_tsuika');
        } 

        $contents = [
            view('master.student_attend_special_tsuika.student_attend_special',
                [
                    'copyFor' => 'school',
                    'studentAttendSpecialTsuika' => $studentAttendSpecialTsuika,
                ]
            ),
            view('master.student_attend_special_tsuika.student_attend_special',
                [
                    'copyFor' => 'home',
                    'studentAttendSpecialTsuika' => $studentAttendSpecialTsuika,
                ]
            )
        ];

        $config = [
            'format' => 'A4',
            'margin_left' => 10, //余白(左)
            'margin_right' => 10, //余白(右)
            'margin_top' => 10, //余白(上)
            'margin_bottom' => 10, //余白(下)
            'margin_header' => 5, //余白(ヘッダー)
            'margin_footer' => 5, //余白(フッター)
        ];

        CustomMpdf::generate($contents, $config);
    }
}