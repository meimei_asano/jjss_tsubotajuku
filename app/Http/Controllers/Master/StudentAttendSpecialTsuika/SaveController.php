<?php

namespace App\Http\Controllers\Master\StudentAttendSpecialTsuika;

use App\Http\Controllers\Controller;
use App\Models\StudentAttendSpecialTsuika;
use App\Validation\StudentAttendSpecialTsuikaValidation as Validation;
use DB;
use Illuminate\Http\Request;

class SaveController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  Illuminate\Http\Request $request
     * @return redirection with message(s)
     */
    public function __invoke(Request $request, string $type)
    {
        if ($type === 'syounin') {
            $teacher = \Auth::user()->teacher;
            $permission = $teacher->permission;

            if (!in_array($permission, ['校長', '管理者'])) {
                $request->flash();

                return \Redirect::back()->withErrors(['このリクエストを実行する権限がありません。']);
            }
        } elseif ($type !== 'save') {
            return \Redirect::back()->withErrors(['このリクエストを実行する権限がありません。']);
        }

        $result = [];
        $flashMessage = '登録しました。';
        $requestData = $request->all();

        /*if (in_array($requestData['request_type'], ['卒塾', '退塾'])) {
            if (empty($requestData['target_date']['target_year']) || empty($requestData['target_date']['target_month'])) {
                $requestData['target_date'] = '';
            } else {
                $requestData['kyujyuku_end_date'] = null;
                $targetDate = $requestData['target_date']['target_year'] . '-' . $requestData['target_date']['target_month'];
                $targetDate = date('Y-m-t', strtotime($targetDate . '-01'));
                $requestData['target_date'] = $targetDate;
            }
        } else {
            $requestData['target_date'] = $requestData['target_date']['target_full_date'];
        }*/

        $hasErrors = Validation::validate($requestData, $type);

        if ($hasErrors) {
            $request->flash();

            return \Redirect::back()->withErrors($hasErrors);
        }

        if ($type === 'save') {
            $result = $this->saveData($requestData);
        } elseif ($type === 'syounin') {
            $result = $this->syounin($requestData);
            $flashMessage = '承認済み。';
        }

        if ($result['error'] === 0) {
            $studentAttendSpecialTsuika = $result['studentAttendSpecialTsuika'];
            $request->session()->flash('alert.success', $flashMessage);

            return redirect('/master/student_attend_special_tsuika/' . $studentAttendSpecialTsuika->id);
        } else {
            $request->flash();

            return \Redirect::back()->withErrors($result['messages']);
        }
    }

    /**
     * Save information
     *
     * @param  array $requestData
     * @return array $result
     */
    private function saveData(array $requestData)
    {
        $result = ['error' => 0];
        DB::beginTransaction();
        try {
            $id = $requestData['id'];
            unset($requestData['id']);
            unset($requestData['syounin_teacher_id']);
            unset($requestData['teacher_id']);
            $addedHours = intval($requestData['total_hours']) - intval($requestData['base_hours']);
            $unitPrice = \JJSS::calculateUnitPrice($requestData['student_id']);
            $requestData['unit_price'] = $unitPrice;
            $requestData['subtotal'] = $addedHours * $unitPrice;

            if ($id) {
                $studentAttendSpecialTsuika = StudentAttendSpecialTsuika::find($id);
                $studentAttendSpecialTsuika->update($requestData);
            } else {
                $studentAttendSpecialTsuika = new StudentAttendSpecialTsuika;
                $teacher = \Auth::user()->teacher;
                $requestData['teacher_id'] = $teacher->id;
                $studentAttendSpecialTsuika->fill($requestData)->save();
            }

            DB::commit();
            $result['studentAttendSpecialTsuika'] = $studentAttendSpecialTsuika;
        } catch (\PDOException $e){
            DB::rollBack();
            $result['error'] = 1;
            $result['messages'] = [$e->getMessage()];
        }

        return $result;
    }

    /**
     * Update approval
     *
     * @param  array $requestData
     * @return array $result
     */
    private function syounin(array $requestData)
    {
        $result = ['error' => 0];
        $id = $requestData['id'];
        $studentAttendSpecialTsuika = StudentAttendSpecialTsuika::find($id);

        if ($studentAttendSpecialTsuika->syounin_teacher_id !== null) {
            $result['error'] = 1;
            $result['messages'] = ['すでに承認されています。'];
        } else {
            DB::beginTransaction();
            try {
                $teacher = \Auth::user()->teacher;
                $studentAttendSpecialTsuika->update(['syounin_teacher_id' => $teacher->id]);
                DB::commit();
                $result['studentAttendSpecialTsuika'] = $studentAttendSpecialTsuika;
            } catch (Exception $e) {
                DB::rollBack();
                $result['error'] = 1;
                $result['messages'] = [$e->getMessage()];
            }
        }

        return $result;
    }

}