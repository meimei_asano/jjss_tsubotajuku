<?php

namespace App\Http\Controllers\Master\StudentAttendSpecialTsuika;

use App\Http\Controllers\Controller;
use App\Models\StudentAttendSpecialTsuika;
use App\Validation\StudentAttendSpecialTsuikaTorikeshiValidation as Validation;
use DB;
use Illuminate\Http\Request;

class TorikeshiController extends Controller
{
    public function __invoke(Request $request)
    {

        $request->flash();

        $teacher = \Auth::user()->teacher;
        $permission = $teacher->permission;
        if (!in_array($permission, ['校長', '管理者'])) {
            return \Redirect::back()->withErrors(['このリクエストを実行する権限がありません。']);
        }

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $id = $request->input('id');

            if ($id) {
                $m = [];

                $student_attend_special_tsuika = StudentAttendSpecialTsuika::find($id);


                $student_attend_special_tsuika->syounin_teacher_id = null;
                $student_attend_special_tsuika->syounin_torikeshi_shinsei_flag = 0;//承認取消申請！
                $student_attend_special_tsuika->save();
            }

            DB::commit();

            $request->session()->flash('alert.success', '承認を取消しました。');
            return redirect('/master/student_attend_special_tsuika');
            //return redirect('/master/student_attend_special_tsuika/' . $student_attend_special_tsuika->id);

        } catch (\PDOException $e){
            DB::rollBack();
            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }
}
