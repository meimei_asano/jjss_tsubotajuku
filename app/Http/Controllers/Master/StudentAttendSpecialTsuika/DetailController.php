<?php

namespace App\Http\Controllers\Master\StudentAttendSpecialTsuika;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentAttendSpecialTsuika;
use App\Models\Teacher;
use App\Models\Term;

class DetailController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  mixed $id
     * @return view
     */
    public function __invoke($id = 'new')
    {
        if ($id !== 'new') {
            $studentAttendSpecialTsuika = StudentAttendSpecialTsuika::with([
                'teacher',
                'syounin_teacher'
            ])->find($id);

            if ($studentAttendSpecialTsuika === null) {
                return redirect('/master/student_attend_special_tsuika');
            }
        } else {
            $studentAttendSpecialTsuika = new StudentAttendSpecialTsuika;
        }

        $teacher = \Auth::user()->teacher;
        $permission = $teacher->permission;
        $students = Student::all();
        $teachers = Teacher::all();
        $terms = Term::all();

        return view('master.student_attend_special_tsuika.detail', [
            'id' => $id,
            'permission' => $permission,
            'studentAttendSpecialTsuika' => $studentAttendSpecialTsuika,
            'students' => $students,
            'teachers' => $teachers,
            'teacher' => $teacher,
            'terms' => $terms,
        ]);
    }
}