<?php

namespace App\Http\Controllers\Master\StudentAttendSpecialTsuika;

use App\Http\Controllers\Controller;
use App\Validation\StudentAttendSpecialTsuikaTorikeshiShinseiValidation as Validation;
use Illuminate\Http\Request;
use App\Models\StudentAttendSpecialTsuika;
use DB;

class TorikeshiShinseiController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $id = $request->input('id');

            if ($id) {
                $student_attend_special_tsuika = StudentAttendSpecialTsuika::find($id);
                $student_attend_special_tsuika->syounin_torikeshi_shinsei_flag = 1;//承認取消申請！
                $student_attend_special_tsuika->save();
           }

            DB::commit();

            $request->session()->flash('alert.success', '申請しました。');
            return redirect('/master/student_attend_special_tsuika');
            //return redirect('/master/student_attend_special_tsuika/' . $student_attend_special_tsuika->id);

        } catch (\PDOException $e){
            DB::rollBack();
            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }
}
