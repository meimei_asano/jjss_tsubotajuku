<?php

namespace App\Http\Controllers\Master\School;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Validation\SchoolValidation as Validation;
use App\Models\School;
use DB;

class SaveController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $school_id = $request->input('school_id');
            if ($school_id) {
                $school = School::find($school_id);
            } else {
                $school = new School;
            }

            $school->name = $request->input('name');
            $school->school_code = $request->input('school_code');
            $school->zip_code = $request->input('zip_code');
            $school->pref = $request->input('pref');
            $school->address = $request->input('address');
            $school->building = $request->input('building');
            $school->tel = $request->input('fax');
            $school->fax = $request->input('fax');
            $school->price_table = $request->input('price_table');
            $school->seats = $request->input('seats');
            $school->furikomisaki = $request->input('furikomisaki');
            $school->save();
            DB::commit();

            $request->session()->flash('alert.success', '登録しました。');
            return redirect('/master/school/' . $school->id);

        } catch (\PDOException $e){
            DB::rollBack();

            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

}
