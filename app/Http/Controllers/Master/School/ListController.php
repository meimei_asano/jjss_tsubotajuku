<?php

namespace App\Http\Controllers\Master\School;

use App\Http\Controllers\Controller;
use App\Models\School;
use Illuminate\Http\Request;

class ListController extends Controller
{
    public function __invoke(Request $request)
    {
    	$search = [];
        $orderby = [];

        $query = School::select();

        // search
        if ($request->input('search_name')) {
            $search['name'] = $request->input('search_name');
            $query->where('name', 'like', "%{$search['name']}%");
        }

        //sort
        if ($request->input('orderby_column_name')) {
            $orderby['column_name'] = $request->input('orderby_column_name');
            $orderby['type'] = $request->input('orderby_type');
            $query->orderByRaw('length(' . $orderby['column_name'] . ') ' . $orderby['type']);
            $query->orderBy($orderby['column_name'], $orderby['type']);
        }
        $schools = $query->paginate( config('paginate.items.school') );

        return view('master.school.list', [
            'search' => $search,
            'orderby' => $orderby,
            'schools' => $schools,
        ]);
    }
}
