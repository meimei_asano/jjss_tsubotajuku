<?php

namespace App\Http\Controllers\Master\School;

use App\Http\Controllers\Controller;
use App\Models\School;

class DetailController extends Controller
{
    public function __invoke($school_id = 'new')
    {
        $school = new School();
        if ($school_id != 'new') {
            $school = School::find($school_id);
        }

        return view('master.school.detail', ['id' => $school_id, 'school' => $school]);
    }
}
