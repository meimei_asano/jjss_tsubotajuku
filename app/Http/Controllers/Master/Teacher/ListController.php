<?php

namespace App\Http\Controllers\Master\Teacher;

use App\Http\Controllers\Controller;
use App\Models\School;
use App\Models\Teacher;
use Illuminate\Http\Request;

class ListController extends Controller
{
    public function __invoke(Request $request)
    {
        $search = [];
        $orderby = [];

        $query = Teacher::select();

        // search
        if ($request->input('search_name')) {
            $search['name'] = $request->input('search_name');
            $query->where('name', 'like', "%{$search['name']}%");
        }
        if ($request->input('search_school_id')) {
            $search['tantou_school_id'] = $request->input('search_school_id');
            $query->where('tantou_school_id', '=', $search['tantou_school_id']);
        }

        //sort
        if ($request->input('orderby_column_name')) {
            $orderby['column_name'] = $request->input('orderby_column_name');
            $orderby['type'] = $request->input('orderby_type');
            if (in_array($orderby['column_name'], ['name', 'kana'])) {
                $query->orderByRaw('length(' . $orderby['column_name'] . ') ' . $orderby['type']);
                $query->orderBy($orderby['column_name'], $orderby['type']);
            } else {
                $query->orderBy($orderby['column_name'], $orderby['type']);
            }
        }
        $teachers = $query->paginate( config('paginate.items.teacher') );

        if ($teachers->count() === 0 && $teachers->total() !== 0) {
            $lastPage = $teachers->lastPage();
            $queryData = $request->query();
            $queryData['page'] = $lastPage;
            $redirectPath = http_build_query($queryData);

            return redirect('/master/teacher?' . $redirectPath);
        }

        return view('master.teacher.list', [
            'search' => $search,
            'orderby' => $orderby,
            'teachers' => $teachers,
            'schools' => School::all()->toArray(),
        ]);
    }
}
