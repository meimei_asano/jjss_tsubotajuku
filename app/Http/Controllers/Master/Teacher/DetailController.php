<?php

namespace App\Http\Controllers\Master\Teacher;

use App\Http\Controllers\Controller;

use App\Models\Teacher;
use App\Models\School;
use App\Models\Kamoku;

class DetailController extends Controller
{
    public function __invoke($teacher_id = 'new')
    {
        $teacher = new Teacher;
        if ($teacher_id != 'new') {
            $teacher = Teacher::find($teacher_id);
        }

        $schools = School::all();

        $kamokus = Kamoku::all();

        $user = \Auth::user();

        return view('master.teacher.detail', ['id' => $teacher_id, 'teacher' => $teacher, 'schools' => $schools, 'kamokus' => $kamokus, 'user' => $user]);
    }
}
