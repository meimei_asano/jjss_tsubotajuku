<?php

namespace App\Http\Controllers\Master\Teacher;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Validation\TeacherValidation as Validation;
use App\Models\Teacher;
use App\Models\TeacherKamoku;
use App\Models\User;
use DB;

class SaveController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $teacher_id = $request->input('teacher_id');
            if ($teacher_id) {
                $teacher = Teacher::find($teacher_id);
                $user = User::find($teacher->user_id);
                TeacherKamoku::where('teacher_id', '=', $teacher->id)->delete();
            } else {
                $user = new User;
                $teacher = new Teacher;
                $user->password = bcrypt($request->input('password'));
            }

            $user->name = $request->input('name');
            $user->email = $request->input('email');
            if($request->input('password')) {
                $user->password = bcrypt($request->input('password'));
            }
            $user->save();

            $teacher->user_id = $user->id;
            $teacher->name = $request->input('name');
            $teacher->kana = $request->input('kana');
            $teacher->nyusya_date = $request->input('nyusya_date');
            $teacher->tantou_school_id = $request->input('tantou_school_id');
            $teacher->koyou_keitai = $request->input('koyou_keitai');
            $teacher->permission = $request->input('permission');
            $teacher->save();

            if($request->input('teacher_kamoku')) {
                $_kamokus = $request->input('teacher_kamoku');
                $insert_teacher_kamokus = [];
                foreach($_kamokus as $teacher_kamoku) {
                    $insert_teacher_kamokus[] = [
                        'teacher_id' => $teacher->id,
                        'kamoku_id' => $teacher_kamoku
                    ];
                }
                DB::table('teacher_kamokus')->insert($insert_teacher_kamokus);
            }
            DB::commit();

            $request->session()->flash('alert.success', '登録しました。');
            return redirect('/master/teacher/' . $teacher->id);

        } catch (\PDOException $e){
            DB::rollBack();

            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

}
