<?php

namespace App\Http\Controllers\Master\StudentCourse;

use App\Http\Controllers\Controller;

use App\Models\Student;
use App\Models\Course;

class ListController extends Controller
{
    public function __invoke($student_id)
    {
        $student = Student::find($student_id);

        $courses = Course::all()->toArray();

        $teacher = \Auth::user()->teacher;
        $permission = $teacher->permission;

        return view('master.student_course.list', ['student' => $student, 'courses' => $courses, 'permission' => $permission]);
    }
}
