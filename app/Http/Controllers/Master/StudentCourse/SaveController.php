<?php

namespace App\Http\Controllers\Master\StudentCourse;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Validation\StudentCourseValidation as Validation;
use App\Models\StudentAttendBase;

use DB;

class SaveController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $attend_base_id = $request->input('attend_base_id');
            if ($attend_base_id) {
                $attend_base = StudentAttendBase::find($attend_base_id);
            } else {
                $attend_base = new StudentAttendBase;
            }

            $attend_base->start_date = $request->input('start_date');
            $attend_base->end_date = strlen($request->input('end_date')) > 0 ? $request->input('end_date') : null;
            $attend_base->course_id = $request->input('course_id');
            $attend_base->student_id = $request->input('student_id');

            $attend_base->save();

            DB::commit();

            $request->session()->flash('alert.success', '登録しました。');

            return redirect('/master/student_course/' . $attend_base->student_id);

        } catch (\PDOException $e){
            DB::rollBack();

            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

}
