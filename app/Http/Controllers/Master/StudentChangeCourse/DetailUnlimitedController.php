<?php

namespace App\Http\Controllers\Master\StudentChangeCourse;

use App\Http\Controllers\Controller;
use App\Models\School;
use App\Models\Student;
use App\Models\StudentCourseChange;
use App\Models\Teacher;

class DetailUnlimitedController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  mixed $id
     * @return view
     */
    public function __invoke($id = 'new')
    {
        if ($id !== 'new') {
            $studentCourseChange = StudentCourseChange::with([
                'current_course',
                'student',
                'syounin_teacher',
                'teacher'
            ])->find($id);
        } else {
            $studentCourseChange = new StudentCourseChange;
        }

        $teacher = \Auth::user()->teacher;
        $permission = $teacher->permission;

        $schools = School::all();
        if (in_array($permission, ['講師', '校長'])) {
            $students = Student::where('school_id', $teacher->tantou_school_id)->get();
        } else {
            $students = Student::all();
        }
        $teachers = Teacher::all();
        $permission = $teacher->permission;

        return view('master.student_change_course.detail_unlimited', [
            'id' => $id,
            'schools' => $schools,
            'students' => $students,
            'studentCourseChange' => $studentCourseChange,
            'teachers' => $teachers,
            'teacher' => $teacher,
            'permission' => $permission,
        ]);
    }
}
