<?php

namespace App\Http\Controllers\Master\StudentChangeCourse;

use App\Http\Controllers\Controller;
use App\Validation\StudentChangeCourseDeleteValidation as Validation;
use Illuminate\Http\Request;
use App\Models\StudentCourseChange;
use DB;

class DeleteController extends Controller
{
    public function __invoke(Request $request)
    {
        //$request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            //ここでエラーになるのはIDが存在しない場合のみなので新規登録画面へリダイレクトする
            //return \Redirect::back()->withErrors($errors);
            if($request->input('entry_type') == "unlimited") {
                return redirect('/master/student_change_course_unlimited/new')->withErrors($errors);
            } else {
                return redirect('/master/student_change_course/new')->withErrors($errors);
            }
        }

        DB::beginTransaction();
        try {
            $id = $request->input('id');

            if ($id) {
                $student_course_change = StudentCourseChange::find($id);
                $student_course_change->delete();
           }

            DB::commit();

            $request->session()->flash('alert.success', '申請取消しました。');
            return redirect('/master/student_change_course');
            //if($request->input('entry_type') == "unlimited") {
            //    return redirect('/master/student_change_course_unlimited/new');
            //} else {
            //    return redirect('/master/student_change_course/new');
            //}

        } catch (\PDOException $e){
            DB::rollBack();
            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }
}
