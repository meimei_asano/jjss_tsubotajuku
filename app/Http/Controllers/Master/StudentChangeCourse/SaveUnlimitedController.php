<?php

namespace App\Http\Controllers\Master\StudentChangeCourse;

use App\Http\Controllers\Controller;
use App\Models\StudentAttendBase;
use App\Models\StudentCourseChange;
use App\Validation\StudentCourseChangeUnlimitedValidation as Validation;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class SaveUnlimitedController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  Illuminate\Http\Request $request
     * @return redirection with message(s)
     */
    public function __invoke(Request $request, string $type)
    {
        if (in_array($type, ['syounin', 'cancel_approval'])) {
            $teacher = \Auth::user()->teacher;
            $permission = $teacher->permission;

            if (!in_array($permission, ['校長', 'マネージャー', '管理者'])) {
                $request->flash();

                return \Redirect::back()->withErrors(['このリクエストを実行する権限がありません。']);
            }
        } elseif ($type !== 'save') {
            return \Redirect::back()->withErrors(['このリクエストを実行する権限がありません。']);
        }

        $result = [];
        $flashMessage = '';
        $requestData = $request->all();

        if (empty($requestData['end_date']['year']) || empty($requestData['end_date']['month'])) {
            $requestData['end_date'] = '';
        } else {
            $endDateYearMonth = $requestData['end_date']['year'] . '-' . $requestData['end_date']['month'];
            $endDate = $endDateYearMonth . '-' . date('t', strtotime($endDateYearMonth . '-01'));
            $requestData['end_date'] = $endDate;
        }

        if ($requestData['student_id'] && strlen($requestData['end_date']) > 0) {
        //if (!$requestData['id'] && !$requestData['current_course_id'] && $requestData['student_id']) {
            //契約変更前のコース
            // 下記 1.〜3. のどれかに該当する student_attend_bases
            // 1. 開始日 < 変更日 かつ 終了日 = 未登録
            // 2. 開始日 < 変更日 かつ 終了日 = 変更日 - 1日 かつ 該当契約より後の日付でのレコードが無い
            // 3. 開始日 < 変更日 かつ 変更日 < 終了日 かつ 該当契約より後の日付でのレコードが無い
            $to_day = date('Y-m-d', strtotime($requestData['change_yotei_date']));
            $studentAttendBase = $this->find_before_course($requestData['student_id'], $to_day);
/*
            //契約変更前のコース
            // start_date <= [変更予定日 - 1 日]
            // [変更予定日 - 1 日] <= end_date || end_date == NULL
            $to_day = date('Y-m-d', strtotime('-1 day', strtotime($requestData['change_yotei_date'])));
            //$to_day = Carbon::today();
            $studentAttendBase = StudentAttendBase::where('student_id', $requestData['student_id'])
                ->where('start_date', '<=', $to_day)
                ->where(function($studentAttendBase) use ($to_day){
                    $studentAttendBase->orWhere('end_date', '<=', $to_day)
                        ->orWhereNull('end_date');
                })->orderBy('start_date', 'desc')
                ->first();
*/
            if($studentAttendBase !== null) {
                $requestData['current_course_id'] = strval($studentAttendBase->course_id);
                $requestData['current_course_start_date'] = $studentAttendBase->start_date;
            }
        }

        $requestData['course_id'] = \JJSS::getUnlimitedCourse(); // temporary

        $checks = $this->check($requestData);
        if(count($checks) > 0) {
            $request->flash();
            return \Redirect::back()->withErrors($checks);
        }

        $hasErrors = Validation::validate($requestData, $type);
        if ($hasErrors) {
            $request->flash();
            return \Redirect::back()->withErrors($hasErrors);
        }

        if ($type === 'save') {
            $result = $this->saveData($requestData);
            $flashMessage = '登録しました。';
        } elseif ($type === 'syounin') {
            $result = $this->syounin($requestData);
            $flashMessage = '承認済み。';
        } elseif ($type === 'cancel_approval') {
            $result = $this->cancelApproval($requestData);
            $flashMessage = '承認は取り消されました。';
        }

        if ($result['error'] === 0) {
            $studentCourseChange = $result['studentCourseChange'];
            $request->session()->flash('alert.success', $flashMessage);
            if($type === 'cancel_approval') {
                return redirect('/master/student_change_course');
            } else {
                return redirect('/master/student_change_course_unlimited/' . $studentCourseChange->id);
            }
        } else {
            $request->flash();

            return \Redirect::back()->withErrors($result['messages']);
        }
    }

    //生徒IDと契約変更予定日から、その前の契約情報を取得する
    //
    //契約変更前のコース
    // 下記 1.〜3. のどれかに該当する student_attend_bases
    // 1. 開始日 < 変更日 かつ 終了日 = 未登録
    // 2. 開始日 < 変更日 かつ 終了日 = 変更日 - 1日 かつ 該当契約より後の日付でのレコードが無い
    // 3. 開始日 < 変更日 かつ 変更日 < 終了日 かつ 該当契約より後の日付でのレコードが無い
    //
    private function find_before_course($student_id = '', $change_yotei_date = '')
    {
        $studentAttendBase = null;
        if(strlen($student_id) == 0 || strlen($change_yotei_date) == 0) {
            return $studentAttendBase;
        }

        $change_yotei_date = Carbon::parse($change_yotei_date);

        // 1. 開始日 < 変更日 かつ 終了日 = 未登録
        $studentAttendBase = StudentAttendBase::where('student_id', $student_id)
            ->where('start_date', '<=', $change_yotei_date)
            ->whereNull('end_date')
            ->orderBy('start_date', 'desc')
            ->first();
        if($studentAttendBase !== null) {
            return $studentAttendBase;
        }

        // 2. 開始日 < 変更日 かつ 終了日 = 変更日 - 1日
        // 3. 開始日 < 変更日 かつ 変更日 < 終了日 かつ 該当契約より後の日付でのレコードが無い
        $studentAttendBase = StudentAttendBase::where('student_id', $student_id)
            ->where('start_date', '<=', $change_yotei_date)
            ->where('end_date', '>=', $change_yotei_date->subDay())
            //->where(function($studentAttendBase) use ($change_yotei_date){
            //    $studentAttendBase->orWhere('end_date', '=', $change_yotei_date->subDay())
            //        ->orWhere('end_date', '>', $change_yotei_date);
            //})->orderBy('start_date', 'desc')
            ->orderBy('start_date', 'desc')
            ->first();
        if($studentAttendBase !== null) {
            //該当した契約より後の日付でレコードが無い
            $checkStudentAttendBase = StudentAttendBase::where('student_id', $student_id)
                ->where('start_date', '>', $studentAttendBase->start_date)
                ->whereNull('deleted_at')
                ->get();
            if(count($checkStudentAttendBase) > 0) {
                $studentAttendBase = null;
            }
        }
        return $studentAttendBase;
    }

    private function check(array $requestData)
    {
        $errors = [];

        $query = StudentCourseChange::where('student_id', '=', $requestData['student_id']);
        if(strlen($requestData['id']) > 0) {
            $query->whereNotIn('id', [$requestData['id']]);
        }
        $query->whereNull('syounin_teacher_id');
        $student_course_change = $query->get();
        if(count($student_course_change) > 0) {
            $errors[] = "承認前のコース変更情報が既に登録されています。";
        }

        //変更前のコースがあるかどうか
        $to_day = date('Y-m-d', strtotime($requestData['change_yotei_date']));
        $studentAttendBase = $this->find_before_course($requestData['student_id'], $to_day);
        if($studentAttendBase === null) {
            $errors[] = "変更前のコースが存在しないため、コース変更を行うことができません。";
        }

        return $errors;
    }


    /**
     * Save information
     *
     * @param  array $requestData
     * @return array $result
     */
    private function saveData(array $requestData)
    {
        $result = ['error' => 0];
        DB::beginTransaction();
        try {
            $id = $requestData['id'];
            unset($requestData['id']);
            unset($requestData['entry_type']);

            $student_id = $requestData['student_id'];
            $current_course_id = $requestData['current_course_id'];
            if ($id) {
                unset($requestData['student_id']);
                //unset($requestData['current_course_id']);
            }

            array_walk($requestData, function(&$item, $key) use (&$requestData) {
                if (strpos($key, 'start_time')
                    || strpos($key, 'end_time')
                    || in_array($key, ['syounin_teacher_id', 'teacher_id', 'hanei_flag'])) {
                    unset($requestData[$key]);
                }
            });

            // 月謝をセット
            $requestData['current_price'] = \JJSS::getMonthlyPriceByCurrentCourse($student_id, $current_course_id, $requestData['current_course_start_date']);
            unset($requestData['current_course_start_date']);
            $requestData['price'] = \JJSS::getMonthlyPriceByNewCourse($student_id, $requestData['course_id'], $requestData['change_yotei_date']);

            if ($id) {
                $studentCourseChange = StudentCourseChange::find($id);
                $studentCourseChange->update($requestData);
            } else {
                $requestData['mon_start_time'] = '16:00:00';
                $requestData['tue_start_time'] = '16:00:00';
                $requestData['wed_start_time'] = '16:00:00';
                $requestData['thu_start_time'] = '16:00:00';
                $requestData['fri_start_time'] = '16:00:00';
                $requestData['mon_end_time'] = '21:40:00';
                $requestData['tue_end_time'] = '21:40:00';
                $requestData['wed_end_time'] = '21:40:00';
                $requestData['thu_end_time'] = '21:40:00';
                $requestData['fri_end_time'] = '21:40:00';
                $studentCourseChange = new StudentCourseChange;
                $teacher = \Auth::user()->teacher;
                $requestData['teacher_id'] = $teacher->id;
                $studentCourseChange->fill($requestData)->save();
            }

            DB::commit();
            $result['studentCourseChange'] = $studentCourseChange;
        } catch (\PDOException $e){
            DB::rollBack();
            $result['error'] = 1;
            $result['messages'] = [$e->getMessage()];
        }

        return $result;
    }

    /**
     * Update approval
     *
     * @param  array $requestData
     * @return array $result
     */
    private function syounin(array $requestData)
    {
        $result = ['error' => 0];
        $id = $requestData['id'];
        $studentCourseChange = StudentCourseChange::find($id);

        unset($requestData['entry_type']);

        if ($studentCourseChange->syounin_teacher_id !== null) {
            $result['error'] = 1;
            $result['messages'] = ['すでに承認されています。'];
        } else {
            DB::beginTransaction();
            try {
                //update $studentAttendBase->end_date = $studentCourseChange->change_yotei_date - 1 day
                $beforeDate = date('Y-m-d', strtotime('-1 day', strtotime($studentCourseChange->change_yotei_date)));
                $studentAttendBase = $this->find_before_course($studentCourseChange->student_id, $studentCourseChange->change_yotei_date);
/*
                $studentAttendBase = StudentAttendBase::where('student_id', $studentCourseChange->student_id)
                    ->where('start_date', '<=', $beforeDate)
                    ->where(function($studentAttendBase) use ($beforeDate) {
                        $studentAttendBase->orWhere('end_date', '=', $beforeDate)
                            ->orWhereNull('end_date');
                    })->orderBy('start_date', 'desc')
                    ->first();
*/
                if($studentAttendBase === null) {
                    return [
                        'error' => 1,
                        'messages' => '変更前のコースが存在しないため、コース変更を行うことができません。',
                    ];
                }

                // 変更前コースの終了日があり、かつ変更予定日よりも後の日付の場合、
                // 変更前コースの契約終了日をunlimited_updated_end_dateにセット
                $unlimited_updated_end_date = null;
                if(strlen($studentAttendBase->end_date) > 0) {
                    $unlimited_updated_end_date = $studentAttendBase->end_date;
                }

                //if(strlen($studentAttendBase->end_date) == 0) {
                    $studentAttendBase->update(['end_date' => $beforeDate]);
                //}

                //add new entry to StudentAttendBase with data from StudentCourseChange
                $newStudentAttendBase = new StudentAttendBase;
                $newData = [
                    'student_id' => $studentCourseChange->student_id,
                    'start_date' => $studentCourseChange->change_yotei_date,
                    'end_date' => $studentCourseChange->end_date,
                    'course_id' => $studentCourseChange->course_id,
                    'mon_start_time' => $studentCourseChange->mon_start_time,
                    'mon_end_time' => $studentCourseChange->mon_end_time,
                    'tue_start_time' => $studentCourseChange->tue_start_time,
                    'tue_end_time' => $studentCourseChange->tue_end_time,
                    'wed_start_time' => $studentCourseChange->wed_start_time,
                    'wed_end_time' => $studentCourseChange->wed_end_time,
                    'thu_start_time' => $studentCourseChange->thu_start_time,
                    'thu_end_time' => $studentCourseChange->thu_end_time,
                    'fri_start_time' => $studentCourseChange->fri_start_time,
                    'fri_end_time' => $studentCourseChange->fri_end_time,
                ];
                $newStudentAttendBase->fill($newData);
                $newStudentAttendBase->save();

                //save current logged in user id in syounin_teacher_id
                $teacher = \Auth::user()->teacher;
                $studentCourseChange->update([
                                'syounin_teacher_id' => $teacher->id,
                                'hanei_flag' => '1',
                                'student_attend_base_id' => $newStudentAttendBase->id,
                                'current_student_attend_base_id' => $studentAttendBase->id,
                                'unlimited_updated_end_date' => $unlimited_updated_end_date,
                ]);
                DB::commit();
                $result['studentCourseChange'] = $studentCourseChange;
            } catch (Exception $e) {
                DB::rollBack();
                $result['error'] = 1;
                $result['messages'] = [$e->getMessage()];
            }
        }

        return $result;
    }

    /**
     * Cancel approval
     *
     * @param  array $requestData
     * @return array $result
     */
    private function cancelApproval(array $requestData)
    {
        $result = ['error' => 0];
        $id = $requestData['id'];
        $studentCourseChange = StudentCourseChange::find($id);

        unset($requestData['entry_type']);

        if ($studentCourseChange->syounin_teacher_id === null) {
            $result['error'] = 1;
            $result['messages'] = ['まだ承認されていません。'];
        } else {
            DB::beginTransaction();
            try {
                // soft delete $studentAttendBase
                if(strlen($studentCourseChange->student_attend_base_id) > 0) {
                    $studentAttendBase = StudentAttendBase::find($studentCourseChange->student_attend_base_id);
                    if($studentAttendBase !== null) {
                        $studentAttendBase->delete();
                    }
                }

                if(strlen($studentCourseChange->current_student_attend_base_id) > 0) {
                    $previousStudentAttendBase = StudentAttendBase::find($studentCourseChange->current_student_attend_base_id);
                    if($previousStudentAttendBase !== null) {
                        if(strlen($studentCourseChange->unlimited_updated_end_date) > 0) {
                            $previousStudentAttendBase->end_date = $studentCourseChange->unlimited_updated_end_date;
                        } else {
                            $previousStudentAttendBase->end_date = null;
                        }
                        $previousStudentAttendBase->save();
                    }
                }

                // revert syounin_teacher_id back to null
                $studentCourseChange->update([
                            'syounin_teacher_id' => null,
                            'syounin_torikeshi_shinsei_flag' => 0,
                            'hanei_flag' => 0,
                            'student_attend_base_id' => null,
                            'current_student_attend_base_id' => null,
                            'unlimited_updated_end_date' => null,
                        ]);
                DB::commit();
                $result['studentCourseChange'] = $studentCourseChange;
            } catch (Exception $e) {
                DB::rollBack();
                $result['error'] = 1;
                $result['messages'] = [$e->getMessage()];
            }
        }

        return $result;
    }
}
