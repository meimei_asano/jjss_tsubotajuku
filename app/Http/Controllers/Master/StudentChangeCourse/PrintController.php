<?php

namespace App\Http\Controllers\Master\StudentChangeCourse;

use App\Facades\CustomMpdf;
use App\Models\StudentCourseChange;
use App\Http\Controllers\Controller;

class PrintController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  integer $id
     * @return void
     */
    public function __invoke(int $id)
    {
        $studentCourseChange = StudentCourseChange::with([
                'course',
                'current_course',
                'teacher',
                'student',
                'student_school',
            ])->find($id);

        if ($studentCourseChange === null) {

            return redirect('/master/student_change_course');
        } 

        $contents = [
            view('master.student_change_course.change_course',
                [
                    'copyFor' => 'school',
                    'studentCourseChange' => $studentCourseChange,
                ]
            ),
            view('master.student_change_course.change_course',
                [
                    'copyFor' => 'home',
                    'studentCourseChange' => $studentCourseChange,
                ]
            ),
        ];

        $config = [
            'format' => 'A4',
            'margin_left' => 10, //余白(左)
            'margin_right' => 10, //余白(右)
            'margin_top' => 10, //余白(上)
            'margin_bottom' => 10, //余白(下)
            'margin_header' => 5, //余白(ヘッダー)
            'margin_footer' => 5, //余白(フッター)
        ];

        CustomMpdf::generate($contents, $config);
    }
}
