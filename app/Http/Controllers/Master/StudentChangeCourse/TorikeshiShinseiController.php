<?php

namespace App\Http\Controllers\Master\StudentChangeCourse;

use App\Http\Controllers\Controller;
use App\Validation\StudentChangeCourseTorikeshiShinseiValidation as Validation;
use Illuminate\Http\Request;
use App\Models\StudentCourseChange;
use DB;

class TorikeshiShinseiController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $id = $request->input('id');

            if ($id) {
                $student_course_change = StudentCourseChange::find($id);
                $student_course_change->syounin_torikeshi_shinsei_flag = 1;//承認取消申請！
                $student_course_change->save();
           }

            DB::commit();

            $request->session()->flash('alert.success', '申請しました。');
            return redirect('/master/student_change_course');
            //if($request->input('entry_type') == "unlimited") {
            //    return redirect('/master/student_change_course_unlimited/' . $student_course_change->id);
            //} else {
            //    return redirect('/master/student_change_course/' . $student_course_change->id);
            //}
        } catch (\PDOException $e){
            DB::rollBack();
            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }
}
