<?php

namespace App\Http\Controllers\Master\Term;

use App\Http\Controllers\Controller;
use App\Models\Term;
use Illuminate\Http\Request;

class ListController extends Controller
{
    public function __invoke(Request $request)
    {
        $search = [];
        $orderby = [];

        $query = Term::select();

        if (!$request->input('search_old')) {
            $query->where('end_date', '>=', date('Y-m-d'));
        } else {
            $search['search_old'] = $request->input('search_old');
        }

        // search
        if ($request->input('search_name')) {
            $search['term_name'] = $request->input('search_name');
            $query->where('term_name', 'like', "%{$search['term_name']}%");
        }

        //sort
        if ($request->input('orderby_column_name')) {
            $orderby['column_name'] = $request->input('orderby_column_name');
            $orderby['type'] = $request->input('orderby_type');
            $query->orderBy($orderby['column_name'], $orderby['type']);
        } else {
            $query->orderBy('start_date', 'ASC');
        }
        $terms = $query->paginate( config('paginate.items.term') );

        return view('master.term.list', [
            'search' => $search,
            'orderby' => $orderby,
            'terms' => $terms,
        ]);
    }
}
