<?php

namespace App\Http\Controllers\Master\Term;

use App\Http\Controllers\Controller;

use App\Models\Term;

class DetailController extends Controller
{
    public function __invoke($term_id = 'new')
    {
        $term = new Term;
        if ($term_id != 'new') {
            $term = Term::find($term_id);
        }

        return view('master.term.detail', ['id' => $term_id, 'term' => $term]);
    }
}
