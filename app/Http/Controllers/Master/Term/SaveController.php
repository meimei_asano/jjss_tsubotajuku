<?php

namespace App\Http\Controllers\Master\Term;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Validation\TermValidation as Validation;
use App\Models\Term;
use DB;

class SaveController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $term_id = $request->input('term_id');
            if ($term_id) {
                $term = Term::find($term_id);
            } else {
                $term = new Term;
            }

            $term->term_name = $request->input('term_name');
            $term->display_name = $request->input('display_name');
            $term->term_type = $request->input('term_type');
            $term->start_date = $request->input('start_date');
            $term->end_date = $request->input('end_date');
            $term->save();

            DB::commit();

            $request->session()->flash('alert.success', '登録しました。');
            return redirect('/master/term/' . $term->id);

        } catch (\PDOException $e){
            DB::rollBack();

            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

}
