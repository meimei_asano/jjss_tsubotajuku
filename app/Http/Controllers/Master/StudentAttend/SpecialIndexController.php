<?php

namespace App\Http\Controllers\Master\StudentAttend;

use App\Http\Controllers\Controller;

use App\Models\Term;
use App\Models\Student;
use App\Models\StudentAttendSpecial;

class SpecialIndexController extends Controller
{
    public function __invoke($student_id, $term_id = '')
    {
        $student = new Student;
        if ($student_id != 'new') {
            $student = Student::find($student_id);
        }

        $terms = Term::where('term_type', '=' , '特別講習')->orderBy('start_date', 'desc')->get();

        //term_id 指定があればその期間。なければ一番start_datemの遅い期間を取得
        if(strlen($term_id) == 0) {
            $target_term = Term::where('term_type', '=' , '特別講習')->orderBy('start_date', 'desc')->first();
            if($target_term !== null) {
               $term_id = $target_term->id;
            }
        } else {
            $target_term = Term::find($term_id);
        }
        //対象期間の生徒の通塾予定データを取得する
        $attend_dates = [];
        if($target_term) {
            $attend_specials = StudentAttendSpecial::where('student_id', '=', $student_id)
                                                    ->where('attend_date', '>=', $target_term->start_date)
                                                    ->where('attend_date', '<=', $target_term->end_date)
                                                    ->orderBy('attend_date', 'asc')
                                                    ->get();
            if(count($attend_specials) > 0) {
                foreach($attend_specials as $attend_special) {
                    $attend_dates[$attend_special->attend_date] = array(
                                                                    'id' => $attend_special->id,
                                                                    'start_time' => $attend_special->start_time,
                                                                    'end_time' => $attend_special->end_time
                                                                  );
                }
            }
        }

        $weeks = config('jjss.weeks');

        return view('master.student_attend.special_index', [
                                                                'student' => $student,
                                                                'terms' => $terms,
                                                                'term_id' => $term_id,
                                                                'attend_dates' => $attend_dates,
                                                                'weeks' => $weeks
                                                                ]
                );
    }
}
