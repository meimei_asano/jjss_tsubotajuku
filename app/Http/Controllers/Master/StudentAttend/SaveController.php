<?php

namespace App\Http\Controllers\Master\StudentAttend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Validation\StudentAttendBaseValidation as Validation;
use App\Models\Student;
use App\Models\StudentAttendBase;

use DB;

class SaveController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $student_id = $request->input('student_id');
            $attend_base_id = $request->input('attend_base_id');

            if ($attend_base_id) {
                $attend_base = StudentAttendBase::find($attend_base_id);
            } else {
                //レコードありきでここの処理に到達するのでここは通らない
                $attend_base = new StudentAttendBase;
            }

            //$attend_base->student_id = $student_id;
            //$attend_base->start_date = $request->input('start_date');
            //$attend_base->end_date = strlen($request->input('end_date')) > 0 ? $request->input('end_date') : null;
            //$attend_base->course_id = (int)$request->input('course_id');
            $attend_base->mon_start_time = strlen($request->input('mon_start_time')) > 0 ? $request->input('mon_start_time') . ':00' : null;
            $attend_base->mon_end_time = strlen($request->input('mon_end_time')) > 0 ? $request->input('mon_end_time') . ':00' : null;
            $attend_base->tue_start_time = strlen($request->input('tue_start_time')) > 0 ? $request->input('tue_start_time') . ':00' : null;
            $attend_base->tue_end_time = strlen($request->input('tue_end_time')) > 0 ? $request->input('tue_end_time') . ':00' : null;
            $attend_base->wed_start_time = strlen($request->input('wed_start_time')) > 0 ? $request->input('wed_start_time') . ':00' : null;
            $attend_base->wed_end_time = strlen($request->input('wed_end_time')) > 0 ? $request->input('wed_end_time') . ':00' : null;
            $attend_base->thu_start_time = strlen($request->input('thu_start_time')) > 0 ? $request->input('thu_start_time') . ':00' : null;
            $attend_base->thu_end_time = strlen($request->input('thu_end_time')) > 0 ? $request->input('thu_end_time') . ':00' : null;
            $attend_base->fri_start_time = strlen($request->input('fri_start_time')) > 0 ? $request->input('fri_start_time') . ':00' : null;
            $attend_base->fri_end_time = strlen($request->input('fri_end_time')) > 0 ? $request->input('fri_end_time') . ':00' : null;

            $attend_base->save();

            DB::commit();

            $request->session()->flash('alert.success', '登録しました。');
            return redirect('/master/student_attend/' . $student_id . '/' . $attend_base->id);

        } catch (\PDOException $e){
            DB::rollBack();

            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

}
