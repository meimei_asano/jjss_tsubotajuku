<?php

namespace App\Http\Controllers\Master\StudentAttend;

use App\Http\Controllers\Controller;

use App\Models\Student;

class IndexController extends Controller
{
    public function __invoke($student_id, $attend_base_id = '')
    {
        $student = new Student;
        if ($student_id != 'new') {
            $student = Student::find($student_id);
        }

        if(!$student->attend_bases) {
            $student->attend_bases = new stdClass;
        }
        return view('master.student_attend.index', ['student' => $student, 'attend_base_id' => $attend_base_id]);
    }
}
