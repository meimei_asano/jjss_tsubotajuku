<?php

namespace App\Http\Controllers\Master\StudentAttend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Validation\StudentAttendSpecialValidation as Validation;

use App\Models\StudentAttendSpecial;
use App\Models\Term;
use DB;

class SpecialSaveController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $student_id = $request->input('student_id');
            $term_id = $request->input('term_id');

            $term_data = Term::find($term_id);

            //対象の日付のstudent_attend_specialデータを削除
            $del_attend = StudentAttendSpecial::where('student_id', '=', $student_id)
                                                ->where('attend_date', '>=', $term_data->start_date)
                                                ->where('attend_date', '<=', $term_data->end_date)
                                                ->delete();
            //新たに登録
            $attend_dates = $request->input('attend_date');
            $start_times = $request->input('start_time');
            $end_times = $request->input('end_time');
            $c = count($attend_dates);
            $insert_datas = [];
            for($i = 0; $i < $c; $i++) {
                if (strlen($start_times[$i]) > 0 || strlen($end_times[$i]) > 0) {
                    $insert_datas[] = ['student_id' => $student_id,
                        'attend_date' => $attend_dates[$i],
                        'start_time' => (strlen($start_times[$i]) > 0 ? $start_times[$i] : null),
                        'end_time' => (strlen($end_times[$i]) > 0 ? $end_times[$i] : null),
                    ];
                }
            }
            StudentAttendSpecial::insert($insert_datas);

            DB::commit();

            $request->session()->flash('alert.success', '登録しました。');
            return redirect('/master/student_attend_special/' . $student_id . '/' . $term_id);

        } catch (\PDOException $e){
            DB::rollBack();

            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

}
