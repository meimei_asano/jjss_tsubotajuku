<?php

namespace App\Http\Controllers\Master\Student;

use App\Http\Controllers\Controller;

use App\Models\Student;

class SeikyuDetailController extends Controller
{
    public function __invoke($student_id)
    {
        $student = Student::find($student_id);

        return view('master.student.detail_seikyu', [
                                'id' => $student_id,
                                'student' => $student,
                            ]);
    }
}
