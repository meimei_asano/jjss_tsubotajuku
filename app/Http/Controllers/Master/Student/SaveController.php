<?php

namespace App\Http\Controllers\Master\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Validation\StudentValidation as Validation;
use App\Models\Student;
use App\Models\StudentKamoku;
use App\Models\StudentNinetype;
use App\Models\StudentShibou;

use DB;

class SaveController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $student_id = $request->input('student_id');
            if ($student_id) {
                $student = Student::find($student_id);
                StudentKamoku::where('student_id', '=', $student->id)->delete();
            } else {
                $student = new Student;
            }

            $student->student_code = $request->input('student_code');
            $student->name = $request->input('name');
            $student->kana = $request->input('kana');
            $student->school_id = $request->input('school_id');
            $student->teacher_id = strlen($request->input('teacher_id')) > 0 ? $request->input('teacher_id') : null;
            $student->gakkou_name = $request->input('gakkou_name');
            $student->grade_code = $request->input('grade_code');
            $student->nyujyuku_date = strlen($request->input('nyujyuku_date')) > 0 ? $request->input('nyujyuku_date') : null;
            $student->taijyuku_date = strlen($request->input('taijyuku_date')) > 0 ? $request->input('taijyuku_date') : null;
            $student->sotsujyuku_date = strlen($request->input('sotsujyuku_date')) > 0 ? $request->input('sotsujyuku_date') : null;
            //$student->hikiotoshi_uketori_date = strlen($request->input('hikiotoshi_uketori_date')) > 0 ? $request->input('hikiotoshi_uketori_date') : null;
            $student->birth_date = strlen($request->input('birth_date')) > 0 ? $request->input('birth_date') : null;
            $student->zip_code = $request->input('zip_code');
            $student->pref = $request->input('pref');
            $student->address1 = $request->input('address1');
            $student->address2 = $request->input('address2');
            $student->home_tel = $request->input('home_tel');
            $student->student_tel = $request->input('student_tel');
            $student->hogosya_name = $request->input('hogosya_name');
            $student->hogosya_kana = $request->input('hogosya_kana');
            $student->hogosya_tel = $request->input('hogosya_tel');
            $student->hogosya_email = $request->input('hogosya_email');
            //$student->hogosya_enquete_file = $request->input('hogosya_enquete_file');
            //$student->student_enquete_file = $request->input('student_enquete_file');
            $student->hogosya_enquete_file = '';
            $student->student_enquete_file = '';

            $student->save();

            //学習科目
            if($request->input('student_kamoku')) {
                $_kamokus = $request->input('student_kamoku');
                $insert_student_kamokus = [];
                foreach($_kamokus as $student_kamoku) {
                    $insert_student_kamokus[] = [
                        'student_id' => $student->id,
                        'kamoku_id' => $student_kamoku
                    ];
                }
                DB::table('student_kamokus')->insert($insert_student_kamokus);
            }

            //9タイプ診断結果
            if($student_id) {
                $student_ninetype = StudentNinetype::where('student_id', '=', $student_id)->first();
            }
            if(isset($student_ninetype) === false) {
                $student_ninetype = new StudentNinetype;
                $student_ninetype->student_id = $student->id;
            }
            $ninetype_codes = $request->input('ninetype_codes');
            if(is_array($ninetype_codes) === false) {
                $ninetype_codes = array();
            }
            $ninetype_codes = array_filter($ninetype_codes, function($val) {
                                    return !(is_null($val) || $val === "");
                              });
            $student_ninetype->ninetype_codes = implode(',', $ninetype_codes);
            /* score_type1 〜 9 は不要
            $ninetypes = config('jjss.ninetypes');
            foreach($ninetypes as $type_code => $type_name) {
                $student_ninetype->{'score_type' . $type_code} = (int)$request->input('score_type' . $type_code);
            }
            */
            $student_ninetype->save();

            //志望校
            /*
            $student_shibou = StudentShibou::where('student_id', '=', $student->id)
                                            ->where('grade_code', '=', $student->grade_code)
                                            ->first();
            if(!$student_shibou) {
                $student_shibou = new StudentShibou;
                $student_shibou->student_id = $student->id;
                $student_shibou->grade_code = $student->grade_code;
            }
            $student_shibou->shibou_school = $request->input('shibou_school');
            $student_shibou->shibou_gakubu = $request->input('shibou_gakubu');
            $student_shibou->save();
            */

            DB::commit();

            $request->session()->flash('alert.success', '登録しました。');
            return redirect('/master/student/' . $student->id);

        } catch (\PDOException $e){
            DB::rollBack();

            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

}
