<?php

namespace App\Http\Controllers\Master\Student;

use Symfony\Component\HttpFoundation\StreamedResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Student;

class CsvExportController extends Controller
{
    public function __invoke(Request $request)
    {
        $search = [];
        $orderby = [];

        $query = Student::select()
            ->with([
                'school',
                'shibou',
                'kamokus',
                'ninetype',
                'teacher',
                'courses_today',
            ]);

        // search
        if ($request->input('search_name')) {
            $search['name'] = $request->input('search_name');
            $query->where('name', 'like', "%{$search['name']}%");
        }

        if ($request->input('search_school_id')) {
            $search['school_id'] = $request->input('search_school_id');
            $query->where('school_id', '=', $search['school_id']);
        }

        //sort
        if ($request->input('orderby_column_name')) {
            $orderby['column_name'] = $request->input('orderby_column_name');
            $orderby['type'] = $request->input('orderby_type');
            if ($orderby['column_name'] == 'grade_code') {
                $query->orderByRaw("cast({$orderby['column_name']} as signed) {$orderby['type']}");
            } else {
                $query->orderByRaw('length(' . $orderby['column_name'] . ') ' . $orderby['type']);
                $query->orderBy($orderby['column_name'], $orderby['type']);
            }
        } else {
            $query->orderByRaw('length(`kana`) ASC');
            $query->orderBy('kana', 'ASC');
        }

        $students = $query->get();

        $export_columns = config('jjss.student_export_columns');

        //export
        $response = new StreamedResponse (
            function() use ($students, $export_columns){

                $stream = fopen('php://output', 'w');

                //　文字化け回避
                stream_filter_prepend($stream,'convert.iconv.utf-8/cp932//TRANSLIT');

                // タイトルを追加
                fputcsv($stream, array_flip($export_columns));

                $grade_list = config('jjss.grade');

                foreach ($students as $student) {
                    $outputs = [];
                    foreach ($export_columns as $column_name) {

                        $matches = explode('->', $column_name);
                        if (count($matches) == 2) {//「->」× 1
                            $outputs[] = (isset($student->{$matches[0]}->{$matches[1]}) === true ? $student->{$matches[0]}->{$matches[1]} : '');
                            continue;
                        }

                        if ($column_name == "grade.grade_code") {
                            if (isset($grade_list[$student->grade_code]) === true) {
                                $outputs[] = $grade_list[$student->grade_code];
                            } else {
                                $outputs[] = "";
                            }
                            continue;
                        }

                        if($column_name == "ninetypes") {
                            $outputs[] = (($student->ninetype) ? \JJSS::displayNineType($student->ninetype->ninetype_codes) : '');
                            continue;
                        }

                        if($column_name == "kamokus") {
                            $kamoku_ids = [];
                            if(count($student->kamokus) > 0) {
                                foreach($student->kamokus as $kamoku) {
                                    $kamoku_ids[] = $kamoku->kamoku_id;
                                }
                            }
                            $outputs[] = (count($kamoku_ids) > 0 ? \JJSS::displayKamokusByName(implode(',',$kamoku_ids)) : '');
                            continue;
                        }

                        if ($column_name == "courses_today.course.name") {
                            $str = '';
                            if($student->courses_current) {
                                if(isset($student->courses_current[0]) === true) {
                                    if(strlen($student->courses_current[0]->course->name) > 0) {
                                        $str = $student->courses_current[0]->course->name;
                                    }
                                }
                            }
                            $outputs[] = $str;
                            continue;
                        }

                        if (preg_match("/^courses_today.(.*)_start_time$/", $column_name, $matches)) {
                            $str = '';
                            if ($student->courses_current) {
                                if (isset($student->courses_current[0]) === true) {
                                    if (strlen($student->courses_current[0]->{$matches[1] . "_start_time"}) > 0) {
                                        $str = substr($student->courses_current[0]->{$matches[1] . "_start_time"}, 0, 5) . '〜' . substr($student->courses_current[0]->{$matches[1] . "_end_time"}, 0, 5);
                                    }
                                }
                            }
                            $outputs[] = $str;
                            continue;
                        }

                        if (isset($student->{$column_name}) === true) {
                            $outputs[] = $student->{$column_name};
                        } else {
                            $outputs[] = '';
                        }
                    }
                    fputcsv($stream, $outputs);
                }
                fclose($stream);
            }
        );
        $response->headers->set('Content-Type', 'application/octet-stream');
        $response->headers->set('Content-Disposition', 'attachment; filename="student_list.csv"');

        return $response;
    }
}
