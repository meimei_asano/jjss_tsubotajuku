<?php

namespace App\Http\Controllers\Master\Student;

use App\Http\Controllers\Controller;

use App\Models\Student;
use App\Models\School;
use App\Models\Teacher;
use App\Models\Kamoku;
use App\Models\StudentShibou;
use App\Models\StudentPif;


class PifDetailController extends Controller
{
    public function __invoke($student_id)
    {
        $student = Student::find($student_id);
        $student_shibou = StudentShibou::where('student_id','=',$student_id)
                                            ->orderBy('grade_code', 'DESC')
                                            ->first();
        $student_pif = StudentPif::where('student_id', '=', $student_id)->first();

        $kamokus = Kamoku::all();
        $ninetypes = config('jjss.ninetypes');

        return view('master.student.detail_pif', [
                                'id' => $student_id,
                                'student' => $student,
                                'student_shibou' => $student_shibou,
                                'student_pif' => $student_pif,
                                'kamokus' => $kamokus,
                                'ninetypes' => $ninetypes]
                    );
    }
}
