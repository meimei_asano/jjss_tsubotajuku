<?php

namespace App\Http\Controllers\Master\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Validation\StudentSeikyuValidation as Validation;
use App\Models\Student;

use DB;

class SeikyuSaveController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $student_id = $request->input('student_id');
            $student = Student::find($student_id);

            $student->hikiotoshi_uketori_date = strlen($request->input('hikiotoshi_uketori_date')) > 0 ? $request->input('hikiotoshi_uketori_date') : null;

            $student->bank_code = $request->input('bank_code');
            $student->bank_name = $request->input('bank_name');
            $student->branch_code = $request->input('branch_code');
            $student->branch_name = $request->input('branch_name');
            $student->deposit_type = strlen($request->input('deposit_type')) > 0 ? $request->input('deposit_type') : null;
            $student->account_number = $request->input('account_number');
            $student->account_holder = $request->input('account_holder');
            $student->customer_number = $request->input('customer_number');
            $student->itaku_number = $request->input('itaku_number');
            $student->hikiotoshi_start_date = strlen($request->input('hikiotoshi_start_date')) > 0 ? $request->input('hikiotoshi_start_date') : null;

            $student->save();

            DB::commit();

            $request->session()->flash('alert.success', '登録しました。');
            return redirect('/master/student_seikyu/' . $student->id);

        } catch (\PDOException $e){
            DB::rollBack();

            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

}
