<?php

namespace App\Http\Controllers\Master\Student;

use App\Http\Controllers\Controller;

use App\Models\Student;
use App\Models\School;
use App\Models\Teacher;
use App\Models\Kamoku;
use App\Models\StudentShibou;

class DetailController extends Controller
{
    public function __invoke($student_id = 'new')
    {
        $student = new Student;
        $student_shibou = new StudentShibou;
        if ($student_id != 'new') {
            $student = Student::find($student_id);
            $student_shibou = StudentShibou::where('student_id','=',$student_id)
                                                ->orderBy('grade_code', 'DESC')
                                                ->get();
        }

        $schools = School::all();

        $teachers = Teacher::all()->sortBy('tantou_school_id')->sortBy('user_id');

        $kamokus = Kamoku::all();

        $ninetypes = config('jjss.ninetypes');

        return view('master.student.detail', ['id' => $student_id, 'student' => $student, 'student_shibou' => $student_shibou, 'schools' => $schools, 'teachers' => $teachers, 'kamokus' => $kamokus, 'ninetypes' => $ninetypes]);
    }
}
