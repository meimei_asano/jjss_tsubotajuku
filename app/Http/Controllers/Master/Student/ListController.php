<?php

namespace App\Http\Controllers\Master\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\School;

class ListController extends Controller
{
    public function __invoke(Request $request)
    {


        $search = [];
        $orderby = [];

        $query = Student::select()
            ->with([
                'school',
                'shibou',
                'kamokus',
                'ninetype',
                'teacher',
                'courses_today',
            ]);

        // search
        if ($request->input('search_name')) {
            $search['name'] = $request->input('search_name');
            $query->where('name', 'like', "%{$search['name']}%");
        }

        //校長、講師は担当校舎の情報のみを取り扱う
        if(in_array(\JJSS::getPermission(), array('校長', '講師'))) {
            $tantou_school_id = \Auth::user()->teacher->tantou_school_id;
            $search['school_id'] = $tantou_school_id;
            $query->where('school_id', '=', $search['school_id']);
        } else {
            if ($request->input('search_school_id')) {
                $search['school_id'] = $request->input('search_school_id');
                $query->where('school_id', '=', $search['school_id']);
            }
        }

        //sort
        if ($request->input('orderby_column_name')) {
            $orderby['column_name'] = $request->input('orderby_column_name');
            $orderby['type'] = $request->input('orderby_type');
            if ($orderby['column_name'] == 'grade_code') {
                $query->orderByRaw("cast({$orderby['column_name']} as signed) {$orderby['type']}");
            } else {
                //$query->orderByRaw('length(' . $orderby['column_name'] . ') ' . $orderby['type']);
                $query->orderBy($orderby['column_name'], $orderby['type']);
            }
        } else {
            //$query->orderByRaw('length(`kana`) ASC');
            $query->orderBy('kana', 'ASC');
        }
        $students = $query->paginate(config('paginate.items.student'));

        if ($students->count() === 0 && $students->total() !== 0) {
            $lastPage = $students->lastPage();
            $queryData = $request->query();
            $queryData['page'] = $lastPage;
            $redirectPath = http_build_query($queryData);

            return redirect('/master/student?' . $redirectPath);
        }

        //校長、講師は担当校舎の情報のみを取り扱う
        if(in_array(\JJSS::getPermission(), array('校長', '講師'))) {
            $schools = School::where('id', '=', $tantou_school_id)->get()->toArray();
        } else {
            $schools = School::all()->toArray();
        }

        return view('master.student.list', [
            'search' => $search,
            'orderby' => $orderby,
            'students' => $students,
            'schools' => $schools,
        ]);
    }
}
