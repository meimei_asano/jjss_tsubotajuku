<?php

namespace App\Http\Controllers\Master\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Validation\StudentPifValidation as Validation;
use App\Models\Student;
use App\Models\StudentShibou;
use App\Models\StudentPif;

use DB;

class PifSaveController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $student_id = $request->input('student_id');
            $student = Student::find($student_id);

            //志望校
            $student_shibou = StudentShibou::where('student_id', '=', $student->id)
                                            ->where('grade_code', '=', $student->grade_code)
                                            ->first();
            if(!$student_shibou) {
                $student_shibou = new StudentShibou;
                $student_shibou->student_id = $student->id;
                $student_shibou->grade_code = $student->grade_code;
            }
            $student_shibou->shibou_school = $request->input('shibou_school');
            $student_shibou->shibou_gakubu = $request->input('shibou_gakubu');
            $student_shibou->kamokus = $request->input('shibou_kamokus');
            $student_shibou->save();

            //PIF情報
            $student_pif = StudentPif::where('student_id', '=', $student->id)->first();

            if(!$student_pif) {
                $student_pif = new StudentPif;
                $student_pif->student_id = $student->id;
            }

            $student_pif->kazoku_kousei = $request->input('kazoku_kousei');
            $student_pif->katei_jijyou = $request->input('katei_jijyou');
            $student_pif->yume = $request->input('yume');
            $student_pif->syumi = $request->input('syumi');
            $student_pif->bukatsu = $request->input('bukatsu');
            $student_pif->gakkou_tokki = $request->input('gakkou_tokki');
            $student_pif->iwanai = $request->input('iwanai');
            $student_pif->akaten_shinkyu = $request->input('akaten_shinkyu');
            $student_pif->teiki_test = $request->input('teiki_test');
            $student_pif->hairyo_jikou = $request->input('hairyo_jikou');

            $student_pif->save();

            DB::commit();

            $request->session()->flash('alert.success', '登録しました。');
            return redirect('/master/student_pif/' . $student->id);

        } catch (\PDOException $e){
            DB::rollBack();

            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

}
