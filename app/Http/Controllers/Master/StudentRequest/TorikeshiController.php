<?php

namespace App\Http\Controllers\Master\StudentRequest;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentKyuujyuku;
use App\Models\StudentRequest;
use App\Validation\StudentRequestTorikeshiValidation as Validation;
use DB;
use Illuminate\Http\Request;

class TorikeshiController extends Controller
{
    public function __invoke(Request $request)
    {

        $request->flash();

        $teacher = \Auth::user()->teacher;
        $permission = $teacher->permission;
        if (!in_array($permission, ['校長', 'マネージャー', '管理者'])) {
            return \Redirect::back()->withErrors(['このリクエストを実行する権限がありません。']);
        }

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $id = $request->input('id');

            if ($id) {
                $m = [];

                $student_request = StudentRequest::find($id);

                if($student_request->request_type == "退塾") {
                    $student = Student::find($student_request->student_id);
                    if($student_request->target_date != $student->taijyuku_date) {
                        $m[] = "生徒マスタに登録されている退塾日と日付が異なっていました。";
                    }
                    $student->fill(['taijyuku_date' => null]);
                    $student->save();

                } elseif($student_request->request_type == "卒塾") {
                    $student = Student::find($student_request->student_id);
                    if($student_request->target_date != $student->sotsujyuku_date) {
                        $m[] = "生徒マスタに登録されている卒塾日と日付が異なっていました。";
                    }
                    $student->fill(['sotsujyuku_date' => null]);
                    $student->save();

                } elseif($student_request->request_type == "休塾") {
                    $student_kyuujyuku = StudentKyuujyuku::where('student_request_id', '=', $student_request->id);
                    $student_kyuujyuku->delete();
                }

                $student_request->syounin_teacher_id = null;
                $student_request->syounin_torikeshi_shinsei_flag = 0;//承認取消申請！
                $student_request->save();
            }

            DB::commit();

            $request->session()->flash('alert.success', '承認を取消しました。' . "<br>" . implode("<br>", $m));
            return redirect('/master/student_request');
            //return redirect('/master/student_request/' . $student_request->id);

        } catch (\PDOException $e){
            DB::rollBack();
            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }
}
