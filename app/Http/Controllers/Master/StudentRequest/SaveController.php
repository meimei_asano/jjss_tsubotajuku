<?php

namespace App\Http\Controllers\Master\StudentRequest;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentRequest;
use App\Models\StudentKyuujyuku;
use App\Validation\StudentRequestValidation as Validation;
use DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

class SaveController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  Illuminate\Http\Request $request
     * @return redirection with message(s)
     */
    public function __invoke(Request $request, string $type)
    {
        if ($type === 'syounin') {
            $teacher = \Auth::user()->teacher;
            $permission = $teacher->permission;

            if (!in_array($permission, ['校長', 'マネージャー', '管理者'])) {
                $request->flash();

                return \Redirect::back()->withErrors(['このリクエストを実行する権限がありません。']);
            }
        } elseif ($type !== 'save') {
            return \Redirect::back()->withErrors(['このリクエストを実行する権限がありません。']);
        }

        $result = [];
        $flashMessage = '登録しました。';
        $requestData = $request->all();
        if(isset($requestData['cooling_off_flag']) === false) {
            $requestData['cooling_off_flag'] = 0;
        }

        if (in_array($requestData['request_type'], ['卒塾', '退塾'])) {
            if ($requestData['request_type'] == "退塾" && $requestData['cooling_off_flag'] == "1") {
                $requestData['kyujyuku_end_date'] = null;
                $requestData['target_date'] = $requestData['target_date']['target_full_date'];
            } elseif (empty($requestData['target_date']['target_year']) || empty($requestData['target_date']['target_month'])) {
                $requestData['target_date'] = '';
            } else {
                $requestData['kyujyuku_end_date'] = null;
                $targetDate = $requestData['target_date']['target_year'] . '-' . $requestData['target_date']['target_month'];
                $targetDate = date('Y-m-t', strtotime($targetDate . '-01'));
                $requestData['target_date'] = $targetDate;
            }
        } else {
            $requestData['target_date'] = $requestData['target_date']['target_full_date'];
        }

        $hasErrors = Validation::validate($requestData, $type);
        if ($hasErrors) {
            $request->flash();
            return \Redirect::back()->withErrors($hasErrors);
        }

        $checks = $this->check($requestData);
        if (count($checks) > 0) {
            $request->flash();
            return \Redirect::back()->withErrors($checks);
        }

        if ($type === 'save') {
            $result = $this->saveData($requestData);
        } elseif ($type === 'syounin') {
            $result = $this->syounin($requestData);
            $flashMessage = '承認済み。';
        }

        if ($result['error'] === 0) {
            $studentRequest = $result['studentRequest'];
            $request->session()->flash('alert.success', $flashMessage);

            return redirect('/master/student_request/' . $studentRequest->id);
        } else {
            $request->flash();

            return \Redirect::back()->withErrors($result['messages']);
        }
    }


    private function check(array $requestData)
    {
       $errors = [];

       //休塾登録
       if($requestData['request_type'] == "休塾") {
           //卒塾日 or 退塾日 登録がある（生徒マスタ、申請データの両方確認）
           $student = Student::find($requestData['student_id']);
           //休塾終了日 < 卒塾 or 退塾日であるかどうか
           if(strlen($student->taijyuku_date) > 0) {
                if(Carbon::parse($requestData['kyujyuku_end_date']) > $student->taijyuku_date) {
                    $errors[] = "休塾日は生徒マスタに登録されている退塾日よりも前の期間を指定してください。";
                }
           }
           if(strlen($student->sotsujyuku_date) > 0) {
               if(Carbon::parse($requestData['kyujyuku_end_date']) > $student->sotsujyuku_date) {
                   $errors[] = "休塾日は生徒マスタに登録されている卒塾日よりも前の期間を指定してください。";
               }
           }
           $student_requests = StudentRequest::where('student_id', '=', $requestData['student_id'])
                                            ->whereNull('syounin_teacher_id')
                                            ->whereIn('request_type', ['退塾', '卒塾'])
                                            ->get();
           if(count($student_requests) > 0) {
               $_e = 0;
               foreach($student_requests as $student_request) {
                    if(Carbon::parse($requestData['kyujyuku_end_date']) > $student_request->target_date) {
                        $_e++;
                    }
               }
               if($_e > 0) {
                   $errors[] = "休塾日よりも前の日付で卒塾または退塾願が登録されています。";
               }
           }
           //他の休塾期間登録はないか
           //休塾開始日〜終了日 が重なっていないか
           //（具体的には
           //      開始日 <= 他休塾期間開始日 <= 終了日
           //      開始日 <= 他休塾期間終了日 <= 終了日　の状態があるかどうか。あったらNG）
           $t_date = $requestData['target_date'];
           $k_date = $requestData['kyujyuku_end_date'];
           $query = StudentRequest::where('student_id', '=', $requestData['student_id'])
                                    ->where(function($query) use ($t_date, $k_date) {
                                        $query->orWhere(function($query) use ($t_date, $k_date) {
                                                            $query->where('target_date', '>=', $t_date)
                                                                    ->where('target_date', '<=', $k_date);
                                        })->orWhere(function($query) use ($t_date, $k_date) {
                                                            $query->where('kyujyuku_end_date', '>=', $t_date)
                                                                    ->where('kyujyuku_end_date', '<=', $k_date);
                                        });
                                    });
           if(strlen($requestData['id']) > 0) {
               $query->whereNotIn('id', [$requestData['id']]);
           }
           $student_requests = $query->get();
           if(count($student_requests) > 0) {
               $errors[] = "重複する休塾登録があります。";
           }

       } elseif(in_array($requestData['request_type'], ["退塾", "卒塾"]) === true) {
            //すでに卒塾 or 退塾 の登録がない
            //生徒マスタの方
            $student = Student::find($requestData['student_id']);
            if(!$student) {
                $errors[] = "生徒マスタに生徒情報がありません。";
            } else {
                if(strlen($student->taijyuku_date) > 0 || strlen($student->sotsujyuku_date) > 0) {
                    $errors[] = "既に生徒マスタに卒塾日または退塾日の登録があります。";
                }
            }

            //申請データの方（承認済みのものは生徒マスタのチェックでNGになるので対象外）
           $query = StudentRequest::where('student_id', '=', $requestData['student_id'])
                                                ->whereIn('request_type', ["退塾", "卒塾"])
                                                ->whereNull('syounin_teacher_id');
           if(strlen($requestData['id']) > 0) {
               $query->whereNotIn('id', [$requestData['id']]);
           }
           $student_requests = $query->get();
           if(count($student_requests) > 0) {
               $errors[] = "既に卒塾または退塾願が登録されています。";
           }

           //登録する日付 よりも後に休塾登録がない。登録する日付 が休塾期間ではない。
           //   具体的には 登録日 <= 休塾終了日 → NG
           $student_requests = StudentRequest::where('student_id', '=', $requestData['student_id'])
                                                ->where('request_type', '=', "休塾")
                                                ->where('kyujyuku_end_date', '>=', $requestData['target_date'])
                                                ->get();
           if(count($student_requests) > 0) {
               $errors[] = "指定の日付は休塾期間中または、指定の日付よりも後の休塾願が登録されています。";
           }
        }
        return $errors;
    }


    /**
     * Save information
     *
     * @param  array $requestData
     * @return array $result
     */
    private function saveData(array $requestData)
    {
        $result = ['error' => 0];
        DB::beginTransaction();
        try {
            $id = $requestData['id'];
            unset($requestData['id']);
            unset($requestData['syounin_teacher_id']);
            unset($requestData['teacher_id']);

            if ($id) {
                $studentRequest = StudentRequest::find($id);
                $studentRequest->update($requestData);
            } else {
                $studentRequest = new StudentRequest;
                $teacher = \Auth::user()->teacher;
                $requestData['teacher_id'] = $teacher->id;
                $studentRequest->fill($requestData)->save();
            }

            DB::commit();
            $result['studentRequest'] = $studentRequest;
        } catch (\PDOException $e){
            DB::rollBack();
            $result['error'] = 1;
            $result['messages'] = [$e->getMessage()];
        }

        return $result;
    }

    /**
     * Update approval
     *
     * @param  array $requestData
     * @return array $result
     */
    private function syounin(array $requestData)
    {
        $result = ['error' => 0];
        $id = $requestData['id'];
        $studentRequest = StudentRequest::find($id);

        if ($studentRequest->syounin_teacher_id !== null) {
            $result['error'] = 1;
            $result['messages'] = ['すでに承認されています。'];
        } else {
            DB::beginTransaction();
            try {
                if($studentRequest->request_type == "退塾") {
                    $student = Student::find($studentRequest->student_id);
                    $student->fill(['taijyuku_date' => $studentRequest->target_date]);
                    $student->save();
                } elseif($studentRequest->request_type == "卒塾") {
                    $student = Student::find($studentRequest->student_id);
                    $student->fill(['sotsujyuku_date' => $studentRequest->target_date]);
                    $student->save();
                } elseif($studentRequest->request_type == "休塾") {
                    $student_kyuujyuku = new StudentKyuujyuku;
                    $student_kyuujyuku->fill([
                                    'student_request_id' => $studentRequest->id,
                                    'student_id' => $studentRequest->student_id,
                                    'start_date' => $studentRequest->target_date,
                                    'end_date' => $studentRequest->kyujyuku_end_date
                            ]);
                    $student_kyuujyuku->save();
                }

                $teacher = \Auth::user()->teacher;
                $studentRequest->update(['syounin_teacher_id' => $teacher->id]);
                DB::commit();
                $result['studentRequest'] = $studentRequest;
            } catch (Exception $e) {
                DB::rollBack();
                $result['error'] = 1;
                $result['messages'] = [$e->getMessage()];
            }
        }

        return $result;
    }

}
