<?php

namespace App\Http\Controllers\Master\StudentRequest;

use App\Http\Controllers\Controller;
use App\Validation\StudentRequestDeleteValidation as Validation;
use Illuminate\Http\Request;
use App\Models\StudentRequest;
use DB;

class DeleteController extends Controller
{
    public function __invoke(Request $request)
    {
        //$request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            //ここでエラーになるのはIDが存在しない場合のみなので新規登録画面へリダイレクトする
            //return \Redirect::back()->withErrors($errors);
            return redirect('/master/student_request/new')->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $id = $request->input('id');

            if ($id) {
                $student_request = StudentRequest::find($id);
                $student_request->delete();
           }

            DB::commit();

            $request->session()->flash('alert.success', '申請取消しました。');
            return redirect('/master/student_request');
//            return redirect('/master/student_request/new');

        } catch (\PDOException $e){
            DB::rollBack();
            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }
}
