<?php

namespace App\Http\Controllers\Master\StudentRequest;

use App\Facades\CustomMpdf;
use App\Http\Controllers\Controller;
use App\Models\StudentRequest;

class PrintController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  integer $id
     * @return view
     */
    public function __invoke(int $id)
    {
        $contents = [];
        $studentRequest = StudentRequest::with([
            'student',
            'student_school',
        ])->find($id);

        switch ($studentRequest->request_type) {
            case '休塾':
                $contents[] = view('master.student_request.request',
                    [
                        'format' => 'kyujuku',
                        'copyFor' => 'school',
                        'studentRequest' => $studentRequest,
                    ]
                );
                $contents[] = view('master.student_request.request',
                    [
                        'format' => 'kyujuku',
                        'copyFor' => 'home',
                        'studentRequest' => $studentRequest,
                    ]
                );
                break;
            case '卒塾':
                $contents[] = view('master.student_request.request',
                    [
                        'format' => 'sotsujuku',
                        'copyFor' => 'school',
                        'studentRequest' => $studentRequest,
                    ]
                );
                $contents[] = view('master.student_request.request',
                    [
                        'format' => 'sotsujuku',
                        'copyFor' => 'home',
                        'studentRequest' => $studentRequest,
                    ]
                );
                break;
            case '退塾':
                $contents[] = view('master.student_request.request',
                    [
                        'format' => 'taijuku',
                        'copyFor' => 'school',
                        'studentRequest' => $studentRequest,
                    ]
                );
                $contents[] = view('master.student_request.request',
                    [
                        'format' => 'taijuku',
                        'copyFor' => 'home',
                        'studentRequest' => $studentRequest,
                    ]
                );
                break;
        }

        $config = [
            'format' => 'A4',
            'margin_left' => 10, //余白(左)
            'margin_right' => 10, //余白(右)
            'margin_top' => 10, //余白(上)
            'margin_bottom' => 10, //余白(下)
            'margin_header' => 5, //余白(ヘッダー)
            'margin_footer' => 5, //余白(フッター)
        ];

        CustomMpdf::generate($contents, $config);
    }
}
