<?php

namespace App\Http\Controllers\Master\StudentRequest;

use App\Http\Controllers\Controller;
use App\Validation\StudentRequestTorikeshiShinseiValidation as Validation;
use Illuminate\Http\Request;
use App\Models\StudentRequest;
use DB;

class TorikeshiShinseiController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $id = $request->input('id');

            if ($id) {
                $student_request = StudentRequest::find($id);
                $student_request->syounin_torikeshi_shinsei_flag = 1;//承認取消申請！
                $student_request->save();
           }

            DB::commit();

            $request->session()->flash('alert.success', '申請しました。');
            return redirect('/master/student_request');
            //return redirect('/master/student_request/' . $student_request->id);

        } catch (\PDOException $e){
            DB::rollBack();
            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }
}
