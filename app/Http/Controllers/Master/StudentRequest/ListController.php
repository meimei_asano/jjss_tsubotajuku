<?php

namespace App\Http\Controllers\Master\StudentRequest;

use App\Http\Controllers\Controller;
use App\Models\School;
use App\Models\StudentRequest;
use App\Models\Teacher;
use Illuminate\Http\Request;

class ListController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  Illuminate\Http\Request $request
     * @return view
     */
    public function __invoke(Request $request)
    {
        $search = [];
        $orderby = [];
        $schoolId = $request->input('school_id') ?: null;
        $studentCode = $request->input('student_code') ?: null;
        $studentName = $request->input('student_name') ?: null;
        $teacherId = $request->input('teacher_id');
        $syouninTeacherId  = $request->input('syounin_teacher_id') ?: null;
        $targetDate = $request->input('target_date') ?: null;
        $teacher = \Auth::user()->teacher;
        $permission = $teacher->permission;

        // restrict user school option or query if 講師 or 校長
        if (in_array($permission, ['講師', '校長'])) {
            $schoolId = $teacher->tantou_school_id;
            $schools = School::where('id', $schoolId)->get();
        } else {
            $schools = School::all();
        }
        $query = StudentRequest::select('*', 'student_requests.id as id', 'student_requests.teacher_id as teacher_id')->with([
        //$query = StudentRequest::with([
                'student',
                'student_school',
                'teacher',
            ]);
        $query->leftJoin('students', 'student_requests.student_id', '=', 'students.id');
        $query->leftJoin('teachers', 'student_requests.teacher_id', '=', 'teachers.id');

        // search by school_id
        if ($schoolId) {
            $search['school_id'] = $schoolId;
            $query->whereHas('student', function ($q) use ($schoolId) {
                $q->where('students.school_id', $schoolId);
            });
            $query->whereHas('teacher', function ($q) use ($schoolId) {
                $q->where('teachers.tantou_school_id', $schoolId);
            });
        }

        // search by student_code
        if ($studentCode) {
            $search['student_code'] = $studentCode;
            $query->whereHas('student', function ($q) use ($studentCode) {
                $q->where('students.student_code', 'like', '%' . $studentCode . '%');
            });
        }

        // search by student_name
        if ($studentName) {
            $search['student_name'] = $studentName;
            $query->whereHas('student', function ($q) use ($studentName) {
                $q->where('students.name', 'like', '%' . $studentName . '%');
            });
        }

        // search by teacher_id
        if (!in_array($teacherId, ['', null])) {
            $search['teacher_id'] = $teacherId;
            $query->where('teachers.id', $teacherId);
        }

        // search by syounin_teacher_id
        if ($syouninTeacherId) {
            $search['syounin_teacher_id'] = $syouninTeacherId;
            $query->whereNotNull('syounin_teacher_id');
        } else {
            $query->whereNull('syounin_teacher_id');
        }

        if ($targetDate) {
            $search['target_date'] = $targetDate;
            $query->where('target_date', 'like', $targetDate . '%');
        }
        //sort
        if ($request->input('orderby_column_name')) {
            $orderby['column_name'] = $request->input('orderby_column_name');
            $orderby['type'] = $request->input('orderby_type');
            $query->orderBy($orderby['column_name'], $orderby['type']);
        }

        $studentRequests = $query->paginate(config('paginate.items.student_request'));
        //$studentRequests = $query->get();

        return view('master.student_request.list', [
            'schools' => $schools->toArray(),
            'teachers' => Teacher::whereIn('permission', ['講師', '校長', '管理者'])->get()->toArray(),
            'teacher' => $teacher,
            'permission' => $permission,
            'studentRequests' => $studentRequests,
            'search' => $search,
            'orderby' => $orderby,
        ]);
    }
}