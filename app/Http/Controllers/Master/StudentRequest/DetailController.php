<?php

namespace App\Http\Controllers\Master\StudentRequest;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentRequest;
use App\Models\Teacher;

class DetailController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  mixed $id
     * @return view
     */
    public function __invoke($id = 'new')
    {
        if ($id !== 'new') {
            $studentRequest = StudentRequest::with([
                'teacher',
                'syounin_teacher'
            ])->find($id);
        } else {
            $studentRequest = new StudentRequest;
        }

        $teacher = \Auth::user()->teacher;
        $permission = $teacher->permission;

        if (in_array($permission, ['講師', '校長'])) {
            $students = Student::where('school_id', $teacher->tantou_school_id)->get();
        } else {
            $students = Student::all();
        }
        $teachers = Teacher::all();

        return view('master.student_request.detail', [
            'id' => $id,
            'students' => $students,
            'studentRequest' => $studentRequest,
            'teachers' => $teachers,
            'teacher' => $teacher,
            'permission' => $permission,
        ]);
    }
}