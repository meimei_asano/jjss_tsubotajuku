<?php

namespace App\Http\Controllers\Master\Text;

use App\Http\Controllers\Controller;
use App\Models\Text;
use App\Models\Kamoku;
use Illuminate\Http\Request;

class ListController extends Controller
{
    public function __invoke(Request $request)
    {
        $search = [];
        $orderby = [];

        $query = Text::select([
            'texts.*',
            'kamokus.kyouka_code',
            'kamokus.name as kamoku_name'
        ])
        ->join('kamokus', 'texts.kamoku_id', '=', 'kamokus.id'); 

        if ($request->input('search_name')) {
            $search['name'] = $request->input('search_name');
            $query->where('texts.name', 'like', '%'. $request->input('search_name'). '%');
        }
        if ($request->input('search_kamoku_id')) {
            $search['kamoku_id'] = $request->input('search_kamoku_id');
            $query->where('kamoku_id', '=', $request->input('search_kamoku_id'));
        }

        //sort
        if ($request->input('orderby_column_name')) {
            $orderby['column_name'] = $request->input('orderby_column_name');
            $orderby['type'] = $request->input('orderby_type');
            $column_name = 'texts.' . $orderby['column_name'];

            if ($orderby['column_name'] === 'kyouka_code') {
                $column_name = 'kamokus.kyouka_code';
            } else if ($orderby['column_name'] === 'kamoku_name') {
                $column_name = 'kamoku_name';
            }

            $query->orderByRaw('length(' . $column_name . ') ' . $orderby['type']);
            $query->orderBy($column_name, $orderby['type']);
        }

        $texts = $query->paginate( config('paginate.items.text') );

        $kamokus = Kamoku::orderBy('kyouka_code', 'asc')->orderBy('id', 'asc')->get()->toArray();

        return view('master.text.list', [
            'search' => $search,
            'orderby' => $orderby,
            'texts' => $texts,
            'kamokus' => $kamokus
        ]);
    }
}



