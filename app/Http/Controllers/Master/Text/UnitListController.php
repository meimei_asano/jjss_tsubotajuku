<?php

namespace App\Http\Controllers\Master\Text;

use App\Http\Controllers\Controller;
use App\Models\Text;
use App\Models\TextUnit;
use App\Models\Kamoku;
use Illuminate\Http\Request;

class UnitListController extends Controller
{
    public function __invoke($text_id = '', Request $request)
    {
        $search = [];
        $orderby = [];

        $query = TextUnit::select();
        $query->where("text_id", "=", $text_id);
        $query->orderBy("unit_num", "asc");
        $text_units = $query->get();

        return view('master.text.unit_list', [
            'text_id' => $text_id,
            'text' => Text::find($text_id),
            'text_units' => $text_units,
        ]);
    }
}



