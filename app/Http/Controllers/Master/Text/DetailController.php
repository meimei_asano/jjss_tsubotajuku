<?php

namespace App\Http\Controllers\Master\Text;

use App\Http\Controllers\Controller;

use App\Models\Text;
use App\Models\Kamoku;

class DetailController extends Controller
{
    public function __invoke($text_id = 'new')
    {
        $text = new Text;
        if ($text_id != 'new') {
            $text = Text::find($text_id);
        }

        $kamokus = Kamoku::all()->toArray();
        $texts = Text::whereNull('sequence')->orderBy('kamoku_id', 'asc')->orderBy('sequence', 'asc')->get()->toArray();

        return view('master.text.detail', ['id' => $text_id, 'text' => $text, 'kamokus' => $kamokus, 'texts' => $texts]);
    }
}
