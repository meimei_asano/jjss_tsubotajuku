<?php

namespace App\Http\Controllers\Master\Text;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Validation\TextDeleteValidation as Validation;
use App\Models\Text;
use App\Models\TextUnit;
use DB;

class DeleteController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $text_id = $request->input('text_id');

            if ($text_id) {
                $text = Text::find($text_id);
                $text->delete();

                TextUnit::where("text_id", "=", $text_id)->delete();
            }

            DB::commit();

            $request->session()->flash('alert.success', '削除しました。');
            return redirect('/master/text');

        } catch (\PDOException $e){
            DB::rollBack();

            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

}
