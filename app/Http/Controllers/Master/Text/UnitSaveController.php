<?php

namespace App\Http\Controllers\Master\Text;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Validation\TextUnitValidation as Validation;
use App\Models\TextUnit;
use DB;

class UnitSaveController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {

            //1レコードずつ処理する
            $text_id = $request->input('text_id');
            $text_unit_ids = $request->input('text_unit_id');
            $names = $request->input('name');
            //処理済みidの配列
            $save_ids = [];

            $c = count($text_unit_ids);
            if($c > 0) {
                for($i = 0; $i < $c; $i++) {

                    $text_unit = TextUnit::find($text_unit_ids[$i]);
                    if(!$text_unit) {
                        if(strlen($names[$i]) == 0) {
                            continue;
                        }
                        $text_unit = new TextUnit;
                    }

                    $text_unit->text_id = $text_id;
                    $text_unit->unit_num = ($i + 1);
                    $text_unit->name = (strlen($names[$i]) > 0 ? $names[$i] : ($i + 1));
                    $text_unit->test_type = '';
                    $text_unit->save();

                    $save_ids[] = $text_unit->id;
                }
            }

            $text_unit = TextUnit::where("text_id", "=", $text_id)->whereNotIn("id", $save_ids);
            $text_unit->delete();

            DB::commit();

            $request->session()->flash('alert.success', '登録しました。');
            return redirect('/master/text_unit/' . $text_id);

        } catch (\PDOException $e){
            DB::rollBack();

            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

}
