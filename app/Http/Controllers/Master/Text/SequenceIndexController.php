<?php

namespace App\Http\Controllers\Master\Text;

use App\Http\Controllers\Controller;
use App\Models\Text;
use App\Models\Kamoku;
use Illuminate\Http\Request;

class SequenceIndexController extends Controller
{
    public function __invoke($kamoku_id = '', Request $request)
    {
        $query = Text::select();

        if (strlen($kamoku_id) > 0) {
            $query->where('kamoku_id', '=', $kamoku_id);
            $query->orderByRaw("sequence IS NULL ASC");
            $query->orderBy('sequence', 'asc');
            //$query->orderBy('kamoku_id', 'asc');
            $texts = $query->get();
        } else {
            $texts = null;
        }

        $kamokus = Kamoku::orderBy('kyouka_code', 'asc')->orderBy('id', 'asc')->get()->toArray();

        return view('master.text.sequence_list', [
                                                'texts' => $texts,
                                                'kamokus' => $kamokus,
                                                'kamoku_id' => $kamoku_id
                                            ]
                );
    }
}



