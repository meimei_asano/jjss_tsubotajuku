<?php

namespace App\Http\Controllers\Master\Text;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Text;
use DB;

class SequenceSaveController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        DB::beginTransaction();
        try {
            $yuukou_lines = [];
            $c = count($request->text_id);

            for($i = 0; $i < $c; $i++) {

                $text = Text::find($request->text_id[$i]);
                if(is_null($text)) {
                    //存在しないはずはないけど、存在していなければスキップする
                    continue;
                }

                if($request->mukou_flag[$i] == "1") {
                    //無効の行の登録
                    $text->next_text_id = null;
                    $text->sequence = null;
                    $text->save();
                } else {
                    $yuukou_lines[] = $request->text_id[$i];
                }
            }

            $c = count($yuukou_lines);
            for($i = 0; $i < $c ; $i++) {

                $text = Text::find($yuukou_lines[$i]);
                $text->next_text_id = (isset($yuukou_lines[($i + 1)])===true ? $yuukou_lines[($i + 1)] : null);
                $text->sequence = ($i + 1);
                $text->save();

            }

            DB::commit();

            $request->session()->flash('alert.success', '登録しました。');
            return redirect('/master/text_sequence/' . $request->input('kamoku_id'));

        } catch (\PDOException $e){
            DB::rollBack();

            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

}
