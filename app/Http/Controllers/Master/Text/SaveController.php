<?php

namespace App\Http\Controllers\Master\Text;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Validation\TextValidation as Validation;
use App\Models\Text;
use DB;

class SaveController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $text_id = $request->input('text_id');
            if ($text_id) {
                $text = Text::find($text_id);
            } else {
                $text = new Text;
            }

            $text->kamoku_id = $request->input('kamoku_id');
            $text->name = $request->input('name');
            $text->price = (strlen($request->input('price')) > 0 ? $request->input('price') : null);
            $text->has_reverse_test = $request->input('has_reverse_test');
            $text->units_by_student = $request->input('units_by_student');
            $text->branch_next_text_id = (strlen($request->input('branch_next_text_id')) > 0 ? $request->input('branch_next_text_id') : null);
            $text->save();

            DB::commit();

            $request->session()->flash('alert.success', '登録しました。');
            return redirect('/master/text/' . $text->id);

        } catch (\PDOException $e){
            DB::rollBack();

            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }

}
