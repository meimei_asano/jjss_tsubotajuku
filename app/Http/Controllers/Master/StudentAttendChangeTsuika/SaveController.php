<?php

namespace App\Http\Controllers\Master\StudentAttendChangeTsuika;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentAttendChange;
use App\Models\StudentAttendChangeTsuika;
use App\Validation\StudentAttendChangeTsuikaValidation as Validation;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class SaveController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  Illuminate\Http\Request $request
     * @return redirection with message(s)
     */
    public function __invoke(Request $request, string $type)
    {
        if ($type === 'syounin') {
            $teacher = \Auth::user()->teacher;
            $permission = $teacher->permission;

            if (!in_array($permission, ['校長', 'マネージャー', '管理者'])) {
                $request->flash();

                return \Redirect::back()->withErrors(['このリクエストを実行する権限がありません。']);
            }
        } elseif ($type !== 'save') {
            return \Redirect::back()->withErrors(['このリクエストを実行する権限がありません。']);
        }

        $result = [];
        $flashMessage = '登録しました。';
        $requestData = $request->all();
        $hasErrors = Validation::validate($requestData, $type);

        if ($hasErrors) {
            $request->flash();

            return \Redirect::back()->withErrors($hasErrors);
        }

        if ($type === 'save') {
            $result = $this->saveData($requestData);
        } elseif ($type === 'syounin') {
            $result = $this->syounin($requestData);
            $flashMessage = '承認済み。';
        }

        if ($result['error'] === 0) {
            $studentAttendChangeTsuika = $result['studentAttendChangeTsuika'];
            $request->session()->flash('alert.success', $flashMessage);

            return redirect('/master/student_attend_change_tsuika/' . $studentAttendChangeTsuika->id);
        } else {
            $request->flash();

            return \Redirect::back()->withErrors($result['messages']);
        }
    }

    /**
     * Save information
     *
     * @param  array $requestData
     * @return array $result
     */
    private function saveData(array $requestData)
    {
        $result = ['error' => 0];
        DB::beginTransaction();
        try {
            array_walk($requestData, function(&$item, $key) use (&$requestData) {
                if (in_array($key, ['start_time', 'end_time'])) {
                    $item = ($item) ? $item . ':00' : null;
                }
                if (in_array($key, ['syounin_teacher_id', 'teacher_id', 'hanei_flag'])) {
                    unset($requestData[$key]);
                }
            });

            $add_date_base = '';
            foreach($requestData['tsuika_request'] as $rk => $rv) {
                $add_date_base = $rv['add_date'];
                break;
            }

            $requestData['tsuika_request'] = json_encode($requestData['tsuika_request'], JSON_UNESCAPED_UNICODE);
            $id = $requestData['id'];
            unset($requestData['id']);
            unset($requestData['grade_code']);

            if ($id) {
                $studentAttendChangeTsuika = StudentAttendChangeTsuika::find($id);
                $studentAttendChangeTsuika->update($requestData);
            } else {
                $student = Student::find($requestData['student_id']);
                $unit_price = \JJSS::getUnitPrice($student->grade_code, true, false, $add_date_base);
                $requestData['unit_price'] = $unit_price;
                $studentAttendChangeTsuika = new StudentAttendChangeTsuika;
                $teacher = \Auth::user()->teacher;
                $requestData['teacher_id'] = $teacher->id;
                $studentAttendChangeTsuika->fill($requestData)->save();
            }

            DB::commit();
            $result['studentAttendChangeTsuika'] = $studentAttendChangeTsuika;
        } catch (\PDOException $e){
            DB::rollBack();
            $result['error'] = 1;
            $result['messages'] = [$e->getMessage()];
        }

        return $result;
    }

    /**
     * Update approval
     *
     * @param  array $requestData
     * @return array $result
     */
    private function syounin(array $requestData)
    {
        $result = ['error' => 0];
        $id = $requestData['id'];
        $studentAttendChangeTsuika = StudentAttendChangeTsuika::find($id);

        if ($studentAttendChangeTsuika->syounin_teacher_id !== null) {
            $result['error'] = 1;
            $result['messages'] = ['すでに承認されています。'];
        } else {
            DB::beginTransaction();
            try {
                // Add new data from StudentAttendChangeTsuika to StudentAttendChange
                $newData = [
                    'student_id' => $studentAttendChangeTsuika->student_id,
                    'change_type' => 'tsuika',
                ];
                $timestamp = Carbon::now();
                $tsuikaRequest = json_decode($studentAttendChangeTsuika->tsuika_request, JSON_UNESCAPED_UNICODE);
                $multipleEntryData = [];
                foreach ($tsuikaRequest as $item) {
                    $newData['attend_date'] = $item['add_date'];
                    $newData['start_time'] = $item['start_time'];
                    $newData['end_time'] = $item['end_time'];
                    $newData['student_attend_change_tsuika_id'] = $studentAttendChangeTsuika->id;
                    $newData['created_at'] = $timestamp;
                    $newData['updated_at'] = $timestamp;
                    $multipleEntryData[] = $newData;
                }
                StudentAttendChange::insert($multipleEntryData);
                $id = DB::getPdo()->lastInsertId();

                // Save current logged in user id in syounin_teacher_id
                $teacher = \Auth::user()->teacher;
                $studentAttendChangeTsuika->update([
                                            'syounin_teacher_id' => $teacher->id,
                                            'hanei_flag' => '1',
                                            //'student_attend_change_id' => $id,
                                    ]);
                DB::commit();
                $result['studentAttendChangeTsuika'] = $studentAttendChangeTsuika;
            } catch (Exception $e) {
                DB::rollBack();
                $result['error'] = 1;
                $result['messages'] = [$e->getMessage()];
            }
        }

        return $result;
    }

}
