<?php

namespace App\Http\Controllers\Master\StudentAttendChangeTsuika;

use App\Facades\CustomMpdf;
use App\Models\StudentAttendChangeTsuika;
use App\Http\Controllers\Controller;

class PrintController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  integer $id
     * @return void
     */
    public function __invoke(int $id)
    {
        $studentAttendChangeTsuika = StudentAttendChangeTsuika::with([
            'student',
            'student_school',
            'teacher',
        ])->find($id);

        if ($studentAttendChangeTsuika === null) {

            return redirect('/master/student_attend_change_tsuika');
        } 

        $contents = [
            view('master.student_attend_change_tsuika.add_time',
                [
                    'copyFor' => 'school',
                    'studentAttendChangeTsuika' => $studentAttendChangeTsuika,
                ]
            ),
            view('master.student_attend_change_tsuika.add_time',
                [
                    'copyFor' => 'home',
                    'studentAttendChangeTsuika' => $studentAttendChangeTsuika,
                ]
            )
        ];

        $config = [
            'format' => 'A4',
            'margin_left' => 10, //余白(左)
            'margin_right' => 10, //余白(右)
            'margin_top' => 10, //余白(上)
            'margin_bottom' => 10, //余白(下)
            'margin_header' => 5, //余白(ヘッダー)
            'margin_footer' => 5, //余白(フッター)
        ];

        CustomMpdf::generate($contents, $config);
    }
}
