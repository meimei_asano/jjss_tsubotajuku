<?php

namespace App\Http\Controllers\Master\StudentAttendChangeTsuika;

use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\StudentAttendChangeTsuika;
use App\Models\Teacher;

class DetailController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  mixed $id
     * @return view
     */
    public function __invoke($id = 'new')
    {
        if ($id !== 'new') {
            $studentAttendChangeTsuika = StudentAttendChangeTsuika::with([
                'student',
                'syounin_teacher',
                'teacher'
            ])->find($id);
        } else {
            $studentAttendChangeTsuika = new StudentAttendChangeTsuika;
        }

        $teacher = \Auth::user()->teacher;
        $permission = $teacher->permission;

        if (in_array($permission, ['講師', '校長'])) {
            $students = Student::where('school_id', $teacher->tantou_school_id)->get();
        } else {
            $students = Student::all();
        }
        $teachers = Teacher::all();
        $permission = $teacher->permission;

        return view('master.student_attend_change_tsuika.detail', [
            'id' => $id,
            'students' => $students,
            'studentAttendChangeTsuika' => $studentAttendChangeTsuika,
            'teachers' => $teachers,
            'teacher' => $teacher,
            'permission' => $permission,
        ]);
    }
}
