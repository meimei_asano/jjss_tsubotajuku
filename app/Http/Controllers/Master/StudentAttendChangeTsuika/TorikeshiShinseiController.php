<?php

namespace App\Http\Controllers\Master\StudentAttendChangeTsuika;

use App\Http\Controllers\Controller;
use App\Validation\StudentAttendChangeTsuikaTorikeshiShinseiValidation as Validation;
use Illuminate\Http\Request;
use App\Models\StudentAttendChangeTsuika;
use DB;

class TorikeshiShinseiController extends Controller
{
    public function __invoke(Request $request)
    {
        $request->flash();

        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $id = $request->input('id');

            if ($id) {
                $student_attend_change_tsuika = StudentAttendChangeTsuika::find($id);
                $student_attend_change_tsuika->syounin_torikeshi_shinsei_flag = 1;//承認取消申請！
                $student_attend_change_tsuika->save();
           }

            DB::commit();

            $request->session()->flash('alert.success', '申請しました。');
            return redirect('/master/student_attend_change_tsuika');
            //return redirect('/master/student_attend_change_tsuika/' . $student_attend_change_tsuika->id);

        } catch (\PDOException $e){
            DB::rollBack();
            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }
}
