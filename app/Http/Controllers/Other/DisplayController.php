<?php

namespace App\Http\Controllers\Other;

use App\Facades\CustomMpdf;
use App\Http\Controllers\Controller;

class DisplayController extends Controller
{
    /**
     * Initiate invoke function
     *
     * @param  string $type
     * @return view
     */
    public function __invoke(string $type)
    {
        $html = [];

        switch ($type) {
            case 'first':
                $html[] = view('other.display.first');
                break;
            case 'second':
                $html[] = view('other.display.second');
                break;
            case 'change-course':
                $html[] = view('other.change_course', ['copyFor' => 'school']);
                $html[] = view('other.change_course', ['copyFor' => 'home']);
                break;
            case 'add-time':
                $html[] = view('other.add_time');
                break;
            case 'teaching-materials-delivery-home':
                $data = ['format' => 'home'];
                $html[] = view('other.teaching_materials_delivery', $data);
                break;
            case 'teaching-materials-delivery-school':
                $data = ['format' => 'school'];
                $html[] = view('other.teaching_materials_delivery', $data);
                break;
            case 'request_kyujuku':
                $html[] = view('other.request', ['format' => 'kyujuku', 'copyFor' => 'school']);
                $html[] = view('other.request', ['format' => 'kyujuku', 'copyFor' => 'home']);
                break;
            case 'request_sotsujuku':
                $html[] = view('other.request', ['format' => 'sotsujuku', 'copyFor' => 'school']);
                $html[] = view('other.request', ['format' => 'sotsujuku', 'copyFor' => 'home']);
                break;
            case 'request_taijuku':
                $html[] = view('other.request', ['format' => 'taijuku', 'copyFor' => 'school']);
                $html[] = view('other.request', ['format' => 'taijuku', 'copyFor' => 'home']);
                break;
            case 'student_attend_special':
                $html[] = view('other.student_attend_special', ['copyFor' => 'school']);
                $html[] = view('other.student_attend_special', ['copyFor' => 'home']);
                break;
            case 'hikiotoshi_henkou':
                $html[] = view('other.hikiotoshi_henkou');
                break;
            default:
                die('Invalid!');
                break;
        }

        $config = [
            'format' => 'A4',
            'margin_left' => 10, //余白(左)
            'margin_right' => 10, //余白(右)
            'margin_top' => 10, //余白(上)
            'margin_bottom' => 10, //余白(下)
            'margin_header' => 5, //余白(ヘッダー)
            'margin_footer' => 5, //余白(フッター)
        ];

        CustomMpdf::generate($html, $config);
    }
}
