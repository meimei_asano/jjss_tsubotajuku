<?php

namespace App\Http\Controllers\Mypage\Password;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;

class IndexController extends Controller
{
    public function __invoke()
    {
        $user_id = \Auth::id();
        $user = User::find($user_id);

        return view('mypage.password.index', ['user' => $user]);
    }
}
