<?php

namespace App\Http\Controllers\Mypage\Password;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Validation\MypagePasswordValidation as Validation;

use App\Models\User;
use DB;

class RegisterController extends Controller
{
    public function __invoke(Request $request)
    {
        $errors = Validation::validate($request);
        if ($errors) {
            return \Redirect::back()->withErrors($errors);
        }

        DB::beginTransaction();
        try {
            $user_id = \Auth::id();
            $user = User::find($user_id);

            $user->password = bcrypt($request->input('password'));;
            $user->save();

            DB::commit();

            $request->session()->flash('alert.success', 'パスワードを変更しました。');
            return redirect('/mypage/password');

        } catch (\PDOException $e){
            DB::rollBack();

            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }
}
