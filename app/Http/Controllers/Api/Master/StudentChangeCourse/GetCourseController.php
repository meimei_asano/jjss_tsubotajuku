<?php

namespace App\Http\Controllers\Api\Master\StudentChangeCourse;

use App\Models\StudentAttendBase;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;

class GetCourseController
{

    public function __invoke(Request $request)
    {
        $student_id = $request->input('student_id');
        $change_yotei_date = $request->input('change_yotei_date');

        $change_yotei_date = Carbon::parse($change_yotei_date);

        // 1. 開始日 < 変更日 かつ 終了日 = 未登録
        $studentAttendBase = StudentAttendBase::where('student_id', $student_id)
            ->where('start_date', '<=', $change_yotei_date)
            ->whereNull('end_date')
            ->orderBy('start_date', 'desc')
            ->first();

        if($studentAttendBase === null) {
            // 2. 開始日 < 変更日 かつ 終了日 = 変更日 - 1日
            // 3. 開始日 < 変更日 かつ 変更日 < 終了日 かつ 該当契約より後の日付でのレコードが無い
            $studentAttendBase = StudentAttendBase::where('student_id', $student_id)
                ->where('start_date', '<=', $change_yotei_date)
                ->where('end_date', '>=', $change_yotei_date->subDay())
       //         ->where(function ($studentAttendBase) use ($change_yotei_date) {
       //             $studentAttendBase->orWhere('end_date', '>=', $change_yotei_date->subDay())
       //                 ->orWhere('end_date', '>', $change_yotei_date->addDay());
       //         })->orderBy('start_date', 'desc')
                ->orderBy('start_date', 'desc')
                ->first();

            if ($studentAttendBase !== null) {
                //該当した契約より後の日付でレコードが無い
                $checkStudentAttendBase = StudentAttendBase::where('student_id', $student_id)
                    ->where('start_date', '>', $studentAttendBase->start_date)
                    ->whereNull('deleted_at')
                    ->get();
                if (count($checkStudentAttendBase) > 0) {
                    $studentAttendBase = null;
                }
            }
        }

        $course_id = '';
        $course_name = '（なし）';
        if($studentAttendBase !== null) {
            $course_id = $studentAttendBase->course_id;
            $course_name = $studentAttendBase->course->name;
        }

        $data = [
                'course_id' => $course_id,
                'course_name' => $course_name,
            ];
        $status_code = Response::HTTP_OK;

        return response($data, $status_code);
    }


}
