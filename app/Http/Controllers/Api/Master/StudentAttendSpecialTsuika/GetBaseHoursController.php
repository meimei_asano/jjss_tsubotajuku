<?php

namespace App\Http\Controllers\Api\Master\StudentAttendSpecialTsuika;

use App\Models\AnnualSchedule;
use App\Models\StudentAttendBase;
use App\Models\StudentAttendSpecialTsuika;
use App\Models\Term;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GetBaseHoursController
{
    /**
     * Initiate invoke function
     *
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $term_id = $request->input('term_id');
        $student_id = $request->input('student_id');
        $term = Term::find($term_id, ['start_date', 'end_date']);
        $target_dates = $this->getTargetDates($term->toArray());
        $base_hours = $this->getBaseHours($student_id, $target_dates);
        $data = ['base_hours' => $base_hours];
        $status_code = Response::HTTP_OK;

        return response($data, $status_code);
    }

    /**
     * Get target dates
     *
     * @param  array $range
     * @return array $target_dates
     */
    private function getTargetDates(array $range)
    {
        $holidays = AnnualSchedule::where('schedule_date', '>=', $range['start_date'])
            ->where('schedule_date', '<=', $range['end_date'])
            ->where('date_type', 'holiday')
            ->pluck('schedule_date')
            ->all();

        $period = new DatePeriod(
             new DateTime($range['start_date']),
             new DateInterval('P1D'),
             new DateTime($range['end_date'] . ' 23:59:59')
        );

        $target_dates = collect($period)
            ->map(function ($item) {
                return $item->format('Y-m-d');
            })
            ->reject(function ($date) use ($holidays) {
                $day = \JJSS::week($date);
                $bool = in_array($date, $holidays) || in_array($day, ['sat', 'sun']);

                return $bool;
            })
            ->all();

        return $target_dates;
    }

    /**
     * Get base hours
     *
     * @param  integer $student_id
     * @param  array $student_id
     * @return integer $base_hours
     */
    private function getBaseHours(int $student_id, array $target_dates)
    {
        $query = StudentAttendBase::with(['course']);
        $count = 0;

        foreach ($target_dates as $date) {
            $whereClause = 'where';

            if ($count > 0) {
                $whereClause = 'orWhere';
            }

            $query->$whereClause(function($q1) use ($student_id, $date) {
                $q1->where('student_id', $student_id);
                $q1->where('start_date', '<=', $date)
                    ->where(function($q2) use ($date) {
                        $q2->whereNull('end_date')
                            ->orWhere('end_date', '>=', $date);
                    });
                });

            if ($count === 0) {
                $count++;
            }
        }

        $bases = $query->get();
        $base_hours = 0;
        $bases->map(function ($item, $key) use (&$base_hours, $target_dates) {
            foreach ($target_dates as $date) {
                $day = \JJSS::week($date);

                if ($item['start_date'] <= $date
                    && ($item['end_date'] >= $date || $item['end_date'] == null)
                    && $item[$day . '_start_time'] !== null)
                {
                    $hours = \JJSS::getTimesByCourse($item['course']['name']);
                    $base_hours += $hours;
                }
            }

            return $item;
        });

        return $base_hours;
    }
}
