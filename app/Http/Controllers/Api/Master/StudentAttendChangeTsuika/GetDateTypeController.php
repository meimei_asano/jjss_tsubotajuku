<?php

namespace App\Http\Controllers\Api\Master\StudentAttendChangeTsuika;

use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;

class GetDateTypeController
{
    public function __invoke(Request $request)
    {
        $student_id = $request->input('student_id');
        $target_date = $request->input('target_date');

        $student = Student::find($student_id);
        $date_type = \JJSS::getTypeOfDate($student->school_id, Carbon::parse($target_date));

        $data = [
                'date_type' => $date_type
        ];
        $status_code = Response::HTTP_OK;


        return response($data, $status_code);
    }
}
