<?php

namespace App\Http\Controllers\Api\Master\StudentAttendChangeTsuika;

use App\Models\Student;
use App\Models\StudentAttendChangeTsuika;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GetUnitPriceController
{
    /**
     * Initiate invoke function
     *
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $student_id = $request->input('student_id');
        $student = Student::find($student_id);
        $unit_price = \JJSS::getUnitPrice($student->grade_code, true, false, $request->input('add_date'));
        $grade_code_name = config('jjss.grade')[$student->grade_code] ?? '';
        $data = [
                'unit_price' => $unit_price,
                'grade_code' => $student->grade_code,
                'grade_code_name' => $grade_code_name,
        ];
        $status_code = Response::HTTP_OK;

        return response($data, $status_code);
    }
}
