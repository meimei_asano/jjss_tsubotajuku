<?php

namespace App\Http\Controllers\Api\Plan\Monthly;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\StudentMonthly;
use App\Models\StudentNote;
use App\Models\Text;
use App\Models\TextUnit;
use App\Models\Term;
use Mpdf\Tag\Select;

use DB;


class GetInfoController extends Controller
{
    public function getTextList(Request $request)
    {
        $query = Text::select()
                    ->with([
                        'text_units',
                    ]);

        //where
        if($request->input('kamoku_id')) {
           $query->where('kamoku_id', "=", $request->input('kamoku_id'));
        }
        if($request->input('text_id')) {
            $query->where('id', "=", $request->input('text_id'));
        }

        //sort
        $query->orderByRaw("sequence IS NULL ASC");
        $query->orderBy("sequence", "asc");

        $texts = $query->get();

        $data = $texts->toArray();
        $statusCode = Response::HTTP_OK;

        return response($data, $statusCode);
    }

    public function getStudentMonthlies(Request $request)
    {
        $query = StudentMonthly::select();

        if($request->input('student_id')) {
            $query->where("student_id", "=", $request->input('student_id'));
        }
        if($request->input('term_id')) {
            $query->where("term_id", "=", $request->input('term_id'));
        }

        $data = $query->get()->toArray();
        $statusCode = Response::HTTP_OK;

        return response($data, $statusCode);
    }

    public function getFromNote(Request $request)
    {
        $student_id = $request->input('student_id');
        $term_id = $request->input('term_id');

        //最新の子別ノートを取得
        $note = StudentNote::where("student_id", "=", $student_id)
                            ->where("note_date", "<=", Carbon::now()->format("Y-m-d"))
                            ->where("checked_flag", "=", "1")
                            ->orderBy("note_date", "desc")
                            ->first();

        //1つ前の月間計画を取得
        $new_term = Term::find($term_id);

        $before_term_info = DB::table('student_monthlies')
                    ->leftJoin('terms', 'student_monthlies.term_id', '=', 'terms.id')
                    ->where('student_monthlies.student_id', '=', $student_id)
                    ->where('terms.start_date', '<', $new_term->start_date)
                    ->whereNull('student_monthlies.deleted_at')
                    ->orderBy('terms.start_date', 'desc')
                    ->select('student_monthlies.*')
                    ->first();

        $before_texts = array();
        if($before_term_info) {
            $before_plan_info = StudentMonthly::find($before_term_info->id);
            if(count($before_plan_info->student_monthly_texts) > 0) {
                foreach($before_plan_info->student_monthly_texts as $before_text) {
                    if($before_text->text->units_by_student != "0") {
                        $before_texts[$before_text->text_id . $before_text->reverse_test . $before_text->text_name]
                            = ['text_all_kaisuu' => $before_text->text_all_kaisuu];
                    }
                }
            }
        }

        $list = [];
        $sub_list = [];//本流にするリスト
        if($note) {
            if($note->student_note_rows) {
                if(count($note->student_note_rows)) {
                    foreach($note->student_note_rows as $note_row) {

                        //対象はテキストが存在する情報のみ
                        if($note_row->text_id == 0) {
                            continue;
                        }

                        $key = $note_row->text_id
                                 . ($note_row->reverse_test == "1" ? $note_row->reverse_test : '');

                        if(isset($list[$key]) === true) {
                            if($note_row->text->kamoku_id != "7") {
                                $list[$key]['maikai_kaisuu'] = $list[$key]['maikai_kaisuu'] + 1;
                            }
                            $kaisuu = isset($note_row->text_units) === true ? $note_row->text_units->unit_num : $note_row->display_text_unit;
                            if($note_row->result_flag == "1") {
                                //結果が「合格」 => 計画は現在の回＋1回 からセット
                                if(ctype_digit($kaisuu)) {
                                    $kaisuu = $kaisuu + 1;
                                } else {
                                    //n再n
                                    if(preg_match("/(.*)再/", $kaisuu, $matches)) {
                                        if(ctype_digit($matches[1])) {
                                            $kaisuu = $matches[1] + 1;
                                        }
                                    }
                                }
                            } else {
                                if(preg_match("/(.*)再/", $kaisuu, $matches)) {
                                    if(ctype_digit($matches[1])) {
                                        $kaisuu = $matches[1];
                                    }
                                }
                            }
                            if($list[$key]['kaisuu'] < $kaisuu && $list[$key]['result_flag'] == "1") {
                                $list[$key]['kaisuu'] = $kaisuu;
                                $_list['result_flag'] = $note_row->result_flag;
                            }
                            continue;
                        } else {
                            $_list = [];
                            $_list['kamoku_id'] = $note_row->text->kamoku_id;
                            $_list['text_id'] = $note_row->text_id;
                            $_list['reverse_test'] = $note_row->reverse_test;
                            //回数：このレコードで「合格」なら次の回数を指定
                            $_list['kaisuu'] = (isset($note_row->text_units) === true ? $note_row->text_units->unit_num : $note_row->display_text_unit);
                            if($note_row->result_flag == "1") {
                                //結果が「合格」 => 計画は現在の回＋1回 からセット
                                if(ctype_digit($_list['kaisuu'])) {
                                    $_list['kaisuu'] = $_list['kaisuu'] + 1;
                                } else {
                                    //n再n
                                    if(preg_match("/(.*)再/", $_list['kaisuu'], $matches)) {
                                        if(ctype_digit($matches[1])) {
                                            $_list['kaisuu'] = $matches[1] + 1;
                                        }
                                    }
                                }
                            } else {
                                //n再n
                                if(preg_match("/(.*)再/", $_list['kaisuu'], $matches)) {
                                    if(ctype_digit($matches[1])) {
                                        $_list['kaisuu'] = $matches[1];
                                    }
                                }
                            }
                            $_list['result_flag'] = $note_row->result_flag;

                            $all_kaisuu = (isset($note_row->text->text_units) === true ? count($note_row->text->text_units) : '');
                            if($note_row->text->units_by_student == "1") {
                                if(isset($before_texts[$note_row->text_id . $note_row->reverse_test . (strlen($note_row->additional_text_name) > 0 ? $note_row->additional_text_name : $note_row->text->name)]) === true) {
                                    $all_kaisuu = $before_texts[$note_row->text_id . $note_row->reverse_test . (strlen($note_row->additional_text_name) > 0 ? $note_row->additional_text_name : $note_row->text->name)]['text_all_kaisuu'];
                                }
                            }
                            if(strlen($all_kaisuu) == 0 && $note_row->text->units_by_student == "1") {
                                $all_kaisuu = "own";
                            }
                            $_list['all_kaisuu'] = $all_kaisuu;

                            $_list['maikai_kaisuu'] = 1;

                            if(strlen($note_row->text->sequence) > 0 && $note_row->reverse_test != "1") {
                                $sub_list[$note_row->text->kamoku_id][$note_row->text->sequence] = $key;
                            }
                        }

                        $list[$key] = $_list;
                    }
                }
            }
        }

        $data = [];
        if(count($list) > 0) {
            foreach($list as $k => $l) {

                //本流かどうか
                $hon_flag = 1;//0:true, 1:false
                if(isset($sub_list[$l['kamoku_id']])) {
                    $_s = $sub_list[$l['kamoku_id']];
                    $_sequence = max(array_keys($_s));
                    if($k == $_s[$_sequence]) {
                        $hon_flag = 0;
                    }
                }
                $l['hon_flag'] = $hon_flag;

                $max_unit = TextUnit::where("text_id", "=", $l['text_id'])->max("unit_num");
                if($max_unit !== null) {
                    if(ctype_digit($l['kaisuu'])) {  // 回数が数字でない場合にエラーとならないよう調整
                        if($l['kaisuu'] > $max_unit && $hon_flag == 1) {//開始回がテキストの終了回より大きくなるなら今が終了回と判定し引き継がない
                            continue;
                        }
                    }
                }

                if (preg_match("/(初回指導|初回|カウント)/", $l['kaisuu'])) {
                //if ($l['kaisuu'] == '初回指導' || $l['kaisuu'] == '初回' || $l['kaisuu'] == 'カウント') {
                    // 初回指導, カウント は0回目扱い→次は1回目
                    $l['kaisuu'] = 1;
                }

                //教科書リストセット
                $_texts = Text::where("kamoku_id", "=", $l['kamoku_id'])->orderBy("sequence", "asc")->get();
                $texts = [];
                $before_text_id = '';
                if($_texts) {
                    if (count($_texts) > 0) {
                        foreach ($_texts as $text) {
                            $all_kaisuu = (isset($text->text_units) === true ? count($text->text_units) : '');
                            $texts[] = ['text_id' => $text->id, 'text_name' => $text->name, 'all_kaisuu' => ($text->units_by_student == "1" ? 'own' : $all_kaisuu)];

                            if($max_unit !== null) {
                                if ($l['kaisuu'] > $max_unit && $l['text_id'] == $before_text_id) {

                                    $l['kaisuu'] = 0;
                                    $l['text_id'] = $text->id;
                                    $l['all_kaisuu'] = ($text->units_by_student == "1" ? 'own' : $all_kaisuu);

                                }
                            }
                            $before_text_id = $text->id;
                        }
                    }
                }

                $l['texts'] = $texts;
                $data[] = $l;
            }
        }

        $statusCode = Response::HTTP_OK;

        $r = [
            'data' => $data,
            'schedule_weekdays' => \JJSS::getVisitWeekDays($student_id, $term_id),
        ];

        return response($r, $statusCode);
    }
}
