<?php

namespace App\Http\Controllers\Api\Classroom\Pif;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\StudentPifMemo;
use App\Models\Teacher;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;


class SaveMemoController extends Controller
{
    public function __invoke(Request $request)
    {
        $student_id = $request->input('student_id');
        $pif_memo_id = $request->input('pif_memo_id');

        $teacher_id = $request->input('pif_memo_teacher_id');

        $pif_memo = StudentPifMemo::find($pif_memo_id);
        if(!$pif_memo) {
            $pif_memo = new StudentPifMemo;
            $pif_memo->student_id = $student_id;
            $pif_memo->teacher_id = $teacher_id;
        }

        $pif_memo->memo_date = Carbon::today()->format('Y-m-d');
        $pif_memo->kamoku_id = ($request->input('pif_memo_kamoku_id')) ? $request->input('pif_memo_kamoku_id') : null;
        $pif_memo->teacher_id = $teacher_id;
        $pif_memo->naiyou = $request->input('pif_memo_naiyou');

        $pif_memo->save();

        $data = $pif_memo->toArray();
        $statusCode = Response::HTTP_OK;

        return response($data, $statusCode);
    }

}
