<?php

namespace App\Http\Controllers\Api\Classroom\Pif;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\StudentPifMemo;

class ListMemoController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = StudentPifMemo::select()
                    ->with([
                        'teacher',
                        'kamoku',
                    ]);

        //IDがあればIDのみで検索する
        if($request->input('pif_memo_id')) {

            $query->where('id', "=", $request->input('pif_memo_id'));

            $pif_memos = $query->get();

            $data = $pif_memos->toArray();
            $statusCode = Response::HTTP_OK;

            return response($data, $statusCode);
        }

        $query->where('student_id', '=', $request->input('student_id'));

        if ($request->input('pif_memo_kamoku_id')) {
            if(strlen($request->input('pif_memo_kamoku_id')) > 0) {
                $query->where('kamoku_id', '=', $request->input('pif_memo_kamoku_id'));
            } else {
                $query->whereNull('kamoku_id');//生徒に対して
            }
        } else {
            $query->whereNull('kamoku_id');//生徒に対して
        }

        //sort
        $query->orderBy("memo_date", "desc");
        $query->orderBy("id", "desc");

        $query->limit(10);

        $pif_memos = $query->get();

        $data = $pif_memos->toArray();
        $statusCode = Response::HTTP_OK;

        return response($data, $statusCode);
    }
}
