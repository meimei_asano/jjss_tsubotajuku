<?php

namespace App\Http\Controllers\Api\Classroom\Pif;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\StudentPifMemo;

use DB;

class DeleteMemoController extends Controller
{
    public function __invoke(Request $request)
    {
        //$request->flash();

        DB::beginTransaction();
        try {
            $pif_memo = StudentPifMemo::find($request->input('pif_memo_id'));
            $pif_memo->delete();

            DB::commit();

            $data = $pif_memo->toArray();
            $statusCode = Response::HTTP_OK;

            return response($data, $statusCode);
        } catch (\PDOException $e){
            DB::rollBack();

            return \Redirect::back()->withErrors([$e->getMessage()]);
        }
    }
}
