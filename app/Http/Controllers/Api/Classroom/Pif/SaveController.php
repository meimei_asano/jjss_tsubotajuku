<?php

namespace App\Http\Controllers\Api\Classroom\Pif;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\StudentPif;
use Carbon\Carbon;

class SaveController extends Controller
{
    public function __invoke(Request $request)
    {
        $_datas = $request->input('data');
        parse_str($_datas, $datas);

        $student_id = $datas['student_id'];
        $pif_id = $datas['pif_id'];

        $pif = StudentPif::find($pif_id);
        if(!$pif) {
            $pif = new StudentPif;
            $pif->student_id = $student_id;
        }

        $pif->kazoku_kousei = $datas['kazoku_kousei'];
        $pif->katei_jijyou = $datas['katei_jijyou'];
        $pif->yume = $datas['yume'];
        $pif->syumi = $datas['syumi'];
        $pif->bukatsu = $datas['bukatsu'];
        $pif->gakkou_tokki = $datas['gakkou_tokki'];
        $pif->iwanai = $datas['iwanai'];
        $pif->akaten_shinkyu = $datas['akaten_shinkyu'];
        $pif->teiki_test = $datas['teiki_test'];
        $pif->hairyo_jikou = $datas['hairyo_jikou'];

        $pif->save();

        $data = $pif->toArray();
        $statusCode = Response::HTTP_OK;

        return response($data, $statusCode);
    }

}
