<?php

namespace App\Http\Controllers\Api\Classroom\Note;

use App\Models\TextUnit;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Text;

class GetInfoController
{
    public function texts(Request $request)
    {
        $kamoku_id = $request->input('kamoku_id');
        $texts = Text
            ::where('kamoku_id', $kamoku_id)
            ->orderBy('id', 'ASC')
            ->get();

        $data = [
            'texts' => $texts,
        ];
        $statusCode = Response::HTTP_OK;

        return response($data, $statusCode);
    }

    public function text_units(Request $request)
    {
        $text_id = $request->input('text_id');
        $text = Text::find($text_id);
        $text_units = TextUnit
            ::where('text_id', $text_id)
            ->orderBy('unit_num', 'ASC')
            ->get();

        $data = [
            'is_english_words_tokkun' => ($text->kamoku_id == 7),  // '英単語特訓'の科目か？ TODO: マジックナンバー修正
            'units_by_student' => $text->units_by_student,
            'text_units' => $text_units,
        ];
        $statusCode = Response::HTTP_OK;

        return response($data, $statusCode);
    }
}
