<?php

namespace App\Http\Controllers\Api\Classroom\Note;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\StudentNote;
use App\Models\StudentNoteRow;
use App\Models\Teacher;
use Carbon\Carbon;

class InstructionController extends Controller
{
    public function start(int $student_note_row_id)
    {
        // TODO: error trap
        $note_row = StudentNoteRow::find($student_note_row_id);
        $note_row->actual_start_time = Carbon::now()->format('H:i:s');
        $note_row->save();

        $data = [
            'start_time' => \JJSS::displayTime($note_row->actual_start_time),
            'end_time' => '',
        ];
        $statusCode = Response::HTTP_OK;

        return response($data, $statusCode);
    }

    public function end(int $student_note_row_id, Request $request)
    {
        // TODO: error trap
        $result_flag = $request->input('result_flag');
        $teacher_id = $request->input('teacher_id');

        $note_row = StudentNoteRow::find($student_note_row_id);
        if(!preg_match("/re_end/", $request->path())) {
            $note_row->actual_end_time = Carbon::now()->format('H:i:s');
        }
        $note_row->result_flag = $result_flag;
        $note_row->teacher_id = $teacher_id;
        $note_row->save();

        $teacher = Teacher::find($teacher_id);
        $data = [
            'start_time' => \JJSS::displayTime($note_row->actual_start_time),
            'end_time' => \JJSS::displayTime($note_row->actual_end_time),
            'result' => ['<span class="red">否</span>', '合', 'S'][$result_flag],
            'teacher' => $teacher->name,
        ];

        $statusCode = Response::HTTP_OK;
        return response($data, $statusCode);
    }

    public function update_remarks(Request $request)
    {
        $student_note_id = $request->input('note_id');
        $remarks = $request->input('remarks');//備考

        $note = StudentNote::find($student_note_id);
        $note->remarks = $remarks;
        $note->save();

        $data = [];

        $statusCode = Response::HTTP_OK;
        return response($data, $statusCode);
    }

    public function update_row_something(int $student_note_row_id, Request $request)
    {
        // TODO: error trap
        $koumoku = $request->input('koumoku');
        $up_value = $request->input('up_value');

        $note_row = StudentNoteRow::find($student_note_row_id);
        $note_row->{$koumoku} = $up_value;
        $note_row->save();

        $data = [
            'actual_start_time' => \JJSS::displayTime($note_row->actual_start_time),
            'actual_end_time' => \JJSS::displayTime($note_row->actual_end_time),
        ];

        $statusCode = Response::HTTP_OK;
        return response($data, $statusCode);
    }
}
