<?php

namespace App\Http\Controllers\Api\Classroom\Coming;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\StudentNote;
use Carbon\Carbon;

class InstructionController extends Controller
{
    public function start(int $student_note_id)
    {
        // TODO: error trap
        $note = StudentNote::find($student_note_id);
        $note->actual_attend_start_time = Carbon::now()->format('H:i:s');
        $note->save();

        $data = [
            'start_time' => \JJSS::displayTime($note->actual_attend_start_time),
            'end_time' => '',
        ];
        $statusCode = Response::HTTP_OK;

        return response($data, $statusCode);
    }

    public function end(int $student_note_id, Request $request)
    {
        // TODO: error trap
        $note = StudentNote::find($student_note_id);
        $note->actual_attend_end_time = Carbon::now()->format('H:i:s');
        $note->save();

        $data = [
            'start_time' => \JJSS::displayTime($note->actual_attend_start_time),
            'end_time' => \JJSS::displayTime($note->actual_attend_end_time),
        ];

        $statusCode = Response::HTTP_OK;
        return response($data, $statusCode);
    }

    public function update_something(int $student_note_id, Request $request)
    {
        $koumoku = $request->input('koumoku');
        $up_value = $request->input('up_value');

        // TODO: error trap
        $note = StudentNote::find($student_note_id);
        $note->{$koumoku} = $up_value;
        $note->save();

        $data = [
            'plan_attend_start_time' => \JJSS::displayTime($note->plan_attend_start_time),
            'plan_attend_end_time' => \JJSS::displayTime($note->plan_attend_end_time),
            'actual_attend_start_time' => \JJSS::displayTime($note->actual_attend_start_time),
            'actual_attend_end_time' => \JJSS::displayTime($note->actual_attend_end_time),
            'remarks' => $note->remarks,
        ];
        $statusCode = Response::HTTP_OK;
        return response($data, $statusCode);
    }

}
