<?php

namespace App\Http\Controllers\Api\Prestudent\Mendan;

use App\Models\School;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;

class GetGessyaPriceController
{
    /**
     * Initiate invoke function
     *
     * @param  Illuminate\Http\Request $request
     * @return Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // 月割金額、補足情報を取得する
        $data = $this->getGessyaData($request);

        if (isset($data['error'])) {
            $status_code = Response::HTTP_BAD_REQUEST;
            return response($data, $status_code);
        }

        $status_code = Response::HTTP_OK;
        return response($data, $status_code);
    }

    private function getGessyaData(Request $request)
    {
        $course_id = $request->input('course_id');

        $data = ($course_id != \JJSS::getUnlimitedCourse()) ?
            // 通常コース
            $this->getStandartGessyaData($request)
        :
            // 無制限コース
            $this->getUnlimitedGessyaData($request)
        ;

        return $data;
    }

    /**
     * 月謝を取得（無制限なら月割金額）
     */
    private function getMonthlyPrice(Request $request)
    {
        $course_id = $request->input('course_id');
        $grade_code = $request->input('grade_code');
        $school_id = $request->input('school_id');
        $school = School::find($school_id);
        $price_table = ($school) ? $school->price_table : '';
        $unlimited_discount_flag = $request->input('unlimited_discount_flag') ?? 0;

        $monthly_price = \JJSS::getMonthlyPrice($course_id, $grade_code, $price_table, $unlimited_discount_flag);
        return $monthly_price;
    }

    private function getMonthlyPriceByGradeCode(Request $request, $grade_code)
    {
        $course_id = $request->input('course_id');
        $school_id = $request->input('school_id');
        $school = School::find($school_id);
        $price_table = ($school) ? $school->price_table : '';
        $unlimited_discount_flag = $request->input('unlimited_discount_flag') ?? 0;

        $monthly_price = \JJSS::getMonthlyPrice($course_id, $grade_code, $price_table, $unlimited_discount_flag);
        return $monthly_price;
    }

    private function getComingWeekdays(Request $request)
    {
        $course_id = $request->input('course_id');

        if ($course_id != \JJSS::getUnlimitedCourse()) {
            // 通常コース
            return [
                // TODO: 時刻として正しい入力かのチェック  date_format:"H:i
                'mon' => $request->input('mon_start_time') && $request->input('mon_end_time'),
                'tue' => $request->input('tue_start_time') && $request->input('tue_end_time'),
                'wed' => $request->input('wed_start_time') && $request->input('wed_end_time'),
                'thu' => $request->input('thu_start_time') && $request->input('thu_end_time'),
                'fri' => $request->input('fri_start_time') && $request->input('fri_end_time'),
                'sat' => false, 'sun' => false,
            ];
        } else {
            // 無制限コース 営業日ベースで見るため、土日もチェック対象
            return [
                'mon' => true, 'tue' => true, 'wed' => true, 'thu' => true, 'fri' => true,
                'sat' => true, 'sun' => true,
            ];
        }
    }

    /**
     * 対象年月の営業日のリストを取る
     * 特別講習期間の土日なども考慮する
     */
    private function getOpenDaysOnTargetMonth($target_month, $school_id)
    {
        $repeat_date = Carbon::parse($target_month);
        $w = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
        $days = [];
        while (true) {
            // 初日から最終日までの毎日をまずはセット
            $days[$repeat_date->toDateString()] = $w[$repeat_date->dayOfWeek];

            if ($repeat_date->isLastOfMonth()) break;
            $repeat_date->addDay();
        }

        $open_days = [];
        foreach ($days as $date => $weekday) {
            // 通常,特別講習,休日
            $date_type = \JJSS::getTypeOfDate($school_id, Carbon::parse($date));  // TODO: 期間設定がない場合に全て休日扱いになり、後の処理でDivision By Zeroエラーになる
            if ($date_type == \JJSS::DATE_TYPE['holiday']) continue;
            // 休日以外は営業日
            $open_days[$date] = $weekday;
        }

        return $open_days;

        /**
         * 返されるデータのサンプル
        return $sample = [
            '2019-12-02' => 'mon', '2019-12-03' => 'tue', '2019-12-04' => 'wed', '2019-12-05' => 'thu', '2019-12-06' => 'fri',
            '2019-12-09' => 'mon', '2019-12-10' => 'tue', '2019-12-11' => 'wed', '2019-12-12' => 'thu', '2019-12-13' => 'fri',
            '2019-12-16' => 'mon', '2019-12-17' => 'tue', '2019-12-18' => 'wed', '2019-12-19' => 'thu', '2019-12-20' => 'fri',
            '2019-12-23' => 'mon', '2019-12-24' => 'tue', '2019-12-25' => 'wed', '2019-12-26' => 'thu', '2019-12-27' => 'fri',
            '2019-12-28' => 'sat',  // 冬期講習につき土曜も授業あり
            '2019-12-29' => 'sun',  // 年間スケジュールに登録あるため日曜でも授業あり
            // '2019-12-30' => 'mon',  // 年間スケジュールにて休日登録
            // '2019-12-31' => 'tue',  // 年間スケジュールにて休日登録
        ];
        */
    }

    /**
     * 生徒の通塾曜日から対象月の通塾可能日数を算出する
     */
    private function getMaxTsujyukuDaysOnTargetMonth(Request $request)
    {
        // 対象月を取得
        $first_shidou_date = Carbon::parse($request->input('first_shidou_date'));
        $target_month = $first_shidou_date->year. '-'. $first_shidou_date->month. '-01';

        // 登塾曜日
        $coming_weekdays = $this->getComingWeekdays($request);

        // 営業日のリストを取得
        $school_id = $request->input('school_id');
        $open_days = $this->getOpenDaysOnTargetMonth($target_month, $school_id);

        $max_tsujuku_days = [];
        foreach($open_days as $date => $weekday) {
            if ($coming_weekdays[$weekday]) {
                $max_tsujuku_days[$date] = $weekday;
            }
        }

        return $max_tsujuku_days;
    }

    /**
     * 設備費のデータを生成した後、
     * 通常コースでの通塾残日数/当月対象曜日からみたその月の通学可能日数を取得、
     * 日割り金額計算を行い、
     * 返すデータを生成
     */
    private function getStandartGessyaData(Request $request)
    {
        $data = [];

        // 設備費
        $data[] = [
            'title' => '③設備費（月額）',
            'price' => number_format( \JJSS::getMonthlySetsubiPrice() ),
        ];

        // 月謝金額を取得する
        //  コース、学年、金額テーブル(首都圏校舎か、そうでないか)、
        //  また無制限コースなら割引有無の引数でpricesテーブルから取得
        $monthly_price = $this->getMonthlyPrice($request);
        $str_monthly_price = number_format($monthly_price);

        // 対象月を取得
        $first_shidou_date = Carbon::parse($request->input('first_shidou_date'));
        $month = $first_shidou_date->month;

        // 対象月内の通学可能日数
        $max_tsujuku_days = $this->getMaxTsujyukuDaysOnTargetMonth($request);
        $monthly_days = count($max_tsujuku_days);

        if ($monthly_days == 0) {
            if ($request['course_id'] != \JJSS::getNoneCourse()) {
                return ['error' => '初回授業日が含まれる講習期間が登録されていないため、日割金額を計算できません。'];
            } else {
                // 「コースなし」の場合は、月謝データは空として返す（設備費もなし）
                return [];
            }
        }

        // 通塾残日数を取得
        $first_shidou_date = $request->input('first_shidou_date');
        $tsujuku_days = [];
        foreach($max_tsujuku_days as $date => $weekday) {
            if ($first_shidou_date <= $date) {
                $tsujuku_days[$date] = $weekday;
            }
        }
        $student_days = count($tsujuku_days);

        // 日割の講座料を算出する
        $price = round($monthly_price * $student_days / $monthly_days);

        // 通常コース
        $data[] = [
            'title' => "④{$month}月分　講座料（日割り）",
            'price' => $price,
        ];
        $data[] = [
            'title' => "　（月額授業料　{$str_monthly_price}円 × {$student_days}日／{$monthly_days}日）",
            'price' => '',
        ];
        return $data;
    }

    /**
     * 無制限コースでの通塾残日数/当月の最大通塾日数を取得、
     * 期間分設備費のデータを生成する
     */
    private function getUnlimitedGessyaData(Request $request)
    {
        // 月謝金額を取得する
        //  コース、学年、金額テーブル(首都圏校舎か、そうでないか)、
        //  また無制限コースなら割引有無の引数でpricesテーブルから取得
        $monthly_price = $this->getMonthlyPrice($request);
        $str_monthly_price = number_format($monthly_price);

        // 対象月を取得
        $first_shidou_date = Carbon::parse($request->input('first_shidou_date'));

        // 対象月内の通学可能日数
        $max_tsujuku_days = $this->getMaxTsujyukuDaysOnTargetMonth($request);
        $monthly_days = count($max_tsujuku_days);

        if ($monthly_days == 0) {
            return ['error' => '初回授業日が含まれる講習期間が登録されていないため、日割金額を計算できません。'];
        }

        // 通塾残日数を取得
        $tsujuku_days = [];
        foreach($max_tsujuku_days as $date => $weekday) {
            if ($first_shidou_date->format('Y-m-d') <= $date) {
                $tsujuku_days[$date] = $weekday;
            }
        }
        $student_days = count($tsujuku_days);

        // 日割の講座料を算出する
        $price = round($monthly_price * $student_days / $monthly_days);

        // 先に日割り分をデータ登録
        $data = [
            [
                'title' => "　初月日割り（月額授業料　{$str_monthly_price}円 × {$student_days}日／{$monthly_days}日）",
                'price' => '',
            ]
        ];


        // 無制限コースのため、指定年月までの金額を算出する
        $first_month = Carbon::parse($request->input('first_shidou_date'))->day(1);
        $last_month = Carbon::parse($request->input('unlimited_end_date'))->day(1);

        $diff_years = $last_month->year - $first_month->year;
        $diff_months = ($diff_years * 12) + ($last_month->month - $first_month->month);

        $d = '';
        $first = true;
        $months = 0;
        $target_month = clone ($first_month);
        for ($i = 0; $i < $diff_months; $i++) {
            $target_month->addMonth(1);
            $months++;

            // もし4月なら学年の繰り上げ
            if ($target_month->month == 4) {
                $grade_code = $request->input('grade_code');
                $request['grade_code'] = ($grade_code == 12 || $grade_code == 'G')
                    ? 'G'
                    : ($grade_code + 1);
                $monthly_price = $this->getMonthlyPrice($request);
                $str_monthly_price = number_format($monthly_price);
            }

            if ($first) {
                // 期間のはじめ
                $d = "　". $target_month->format('Y年n月');
                $first = false;
            }
            if ( $target_month->month == 3 || $target_month == $last_month ) {
                // 3月で繰り上がりまたは無制限終了月
                $d .= "〜". $target_month->format('Y年n月'). "分（月額授業料　{$str_monthly_price}円×{$months}月）";
                $data[] = [
                    'title' => $d,
                    'price' => '',
                ];
                $price += $monthly_price * $months;

                // 次の期間のために調整
                $months = 0;

                $first = true;
            }
        }

        // 設備費算出用
        $total_months = $diff_months + 1;

        // 無制限コース
        array_unshift($data,
                [
                    'title' => "③設備費（{$total_months}ヶ月分）",
                    'price' => \JJSS::getMonthlySetsubiPrice() * $total_months,
                ],
                [
                    'title' => "④無制限コース講座料",
                    'price' => $price,
                ]
            );
        return $data;
    }

    private function countMonths($first_month, $last_month)
    {
        $count = 0;
        $month = clone $first_month;
        while (true) {
            if ($month > $last_month) break;
            $month->addMonth();

            $count++;
        }
        return $count;
    }
}
