<?php

namespace App\Rules;

use App\Models\AnnualSchedule;
use App\Models\Term;

use Illuminate\Contracts\Validation\Rule;
use Carbon\Carbon;

class CustomTsuikaRequestDateAndTimeBase implements Rule
{
    protected $request_data = null;
    protected $error_no = 0;
    protected $value = null;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($requestData)
    {
        $this->request_data = $requestData;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (empty($value)) {
            return true;
        }

        $this->value = $value;
        $field = explode('.', $attribute);
        $setNumber = $field[1];
        $date = $this->request_data['tsuika_request'][$setNumber]['add_date'];
        $day = strtolower(date('D', strtotime($date)));
        $start = $this->request_data['tsuika_request'][$setNumber]['start_time'] . ':00';
        $end = $this->request_data['tsuika_request'][$setNumber]['end_time'] . ':00';

        if(preg_match('/^([1-9][0-9]{3})(-|\/)([1-9]{1}|1[0-2]{1}|0[1-9]{1})-|\/([1-9]{1}|0[1-9]{1}|[1-2]{1}[0-9]{1}|3[0-1]{1})$/', $date) !== 1) {
            //日付の形式でないので判定できない。これは validation:date でエラーになるはずなのでここではそっとreturn
            return true;
        }

        $target_date = Carbon::parse($date);

        //日付が通常期間なのか特別期間なのか
        //開始日 ＜ 日付 ＜ 終了日 のレコードを探す
        $term = Term::where('start_date', '<=', $target_date)
            ->where('end_date', '>=', $target_date)
            ->first();//1件しかない
        $kikan_type = '';
        $start_time = '';
        $end_time = '';
        if($term !== null) {
            $kikan_type = $term->term_type;
        } else {
            $this->error_no = 4;
            return false;
        }

        //休塾日かどうかチェック

        //テーブルに登録がある日でない？→あるなら休塾日
        $k_day = AnnualSchedule::where('schedule_date', '=', $target_date)
                                ->where('date_type', '=', 'holiday')
                                ->get();
        if(count($k_day) > 0) {
            $this->error_no = 3;
            return false;
        }

        //土日？ => 土日の場合、テーブルに営業日の登録がある日？ => ない => 休塾日
        if($kikan_type == "通常") {
            $week_holidays = [0, 6];//土曜・日曜が休み
        } elseif($kikan_type == "特別講習") {
            $week_holidays = [0];//日曜が休み
        }
        if(in_array($target_date->dayOfWeek, $week_holidays) === true) {
            $k_day = AnnualSchedule::where('schedule_date', '=', $target_date)
                ->where('date_type', '=', 'workday')
                ->get();
            if(count($k_day) == 0) {
                $this->error_no = 3;
                return false;
            }
        }

        //時間帯指定のチェック

        if($kikan_type == "通常") {
            $start_time = '16:00:00';
            $end_time = '21:40:00';
        } elseif($kikan_type == "特別講習") {
            $start_time = '13:00:00';
            $end_time = '22:00:00';
        }

        // そこから営業時間どっちか判定可能
        if ($start && $end) {
            //形式が合ってれば進む（形式エラーは別途バリデートしている）
            if(preg_match("/^([0-1][0-9]|2[0-3]):[0-5][0-9]:00$/", $start) && preg_match("/^([0-1][0-9]|2[0-3]):[0-5][0-9]:00$/", $end)){
                $target_start = Carbon::parse($start);
                $target_end  = Carbon::parse($end);
            } else {
                return true;
            }

            if($target_start < Carbon::parse($start_time) || Carbon::parse($end_time) < $target_start
                || $target_end < Carbon::parse($start_time) || Carbon::parse($end_time) < $target_end) {
                $this->error_no = 5;
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.custom.tsuika_request.' . $this->error_no, ['value' => $this->value]);
    }
}
