<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class CustomTsuikaRequestDate implements Rule
{
    protected $request_data = null;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($requestData)
    {
        $this->request_data = $requestData;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (empty($value)) {
            return true;
        }

        $tsuikaRequest = collect($this->request_data['tsuika_request']);
        $checkUnique = $tsuikaRequest->unique();

        // check if all input data are unique
        if ($tsuikaRequest->count() !== $checkUnique->count()) {
           return false;
        }

        $rawTsuikaRequest = $this->request_data['tsuika_request'];
        $nendo_list = [];

        foreach ($rawTsuikaRequest as $rawValue) {
            $_date = Carbon::parse($rawValue['add_date']);
            $_year = $_date->subMonths(3)->year;
            $nendo_list[] = $_year;//年度
        }

        $nendo_list = array_unique($nendo_list);

        //年度をまたぐ日付登録があるかないかをチェック
        if(count($nendo_list) > 1) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.custom.tsuika_request.date_nendo');
    }
}
