<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CustomSchoolSchedule implements Rule
{
    protected $request_data = null;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($requestData)
    {
        $this->request_data = $requestData;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (empty($value)) {

            return true;
        }

        $field = substr($attribute, 0, 3);
        $start = $this->request_data[$field . '_start_time'];
        $end = $this->request_data[$field . '_end_time'];

        if ($start && $end) {
            $startTime = strtotime($start);
            $endTime = strtotime($end);

            if ($startTime >= $endTime) {

                return false;
            }

            return true;
        } else {

            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.custom.school_schedule');
    }
}
