<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CustomChangeYoteiDate implements Rule
{
    protected $request_data = null;
    protected $error_no = 0;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($requestData)
    {
        $this->request_data = $requestData;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $requestData = $this->request_data;
        $requestDate = strtotime($requestData['request_date']);
        $requestYear = date('Y', $requestDate);
        $requestMonth = date('m', $requestDate);
        $requestDay = date('d', $requestDate);
        $requestNextMonth = str_pad(intval($requestMonth) + 1, 2, '0', STR_PAD_LEFT);
        $yoteiDate = strtotime($value);
        $yoteiYear = date('Y', $yoteiDate);
        $yoteiMonth = date('m', $yoteiDate);
        $yoteiDay = date('d', $yoteiDate);

        if ($yoteiMonth <= $requestMonth && $yoteiYear <= $requestYear) {
            $this->error_no = 1;

            return false;
        } elseif ($requestYear === $yoteiYear && $requestNextMonth === $yoteiMonth && intval($requestDay) >= 16) {
            $this->error_no = 2;

            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $errorNo = '.error_one';

        if ($this->error_no === 2) {
            $errorNo = '.error_two';
        }

        return trans('validation.custom.change_yotei_date' . $errorNo);
    }
}
