<?php

namespace App\Rules;

use App\Models\AnnualSchedule;
use Illuminate\Contracts\Validation\Rule;
use Carbon\Carbon;

class CustomCheckHoliday implements Rule
{
    protected $request_data = null;
    protected $value = null;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($requestData)
    {
        $this->request_data = $requestData;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (empty($value)) {
            return true;
        }

        $date = $value;
        $target_date = Carbon::parse($date);

        //テーブルに登録がある日でない？→あるなら休塾日
        $k_day = AnnualSchedule::where('schedule_date', '=', $target_date)
            ->where('date_type', '=', 'holiday')
            ->get();
        if(count($k_day) > 0) {
            return false;
        }

        //土日？ => 土日の場合、テーブルに営業日の登録がある日？ => ない => 休塾日
        if(in_array($target_date->dayOfWeek, [0,6]) === true) {
            $k_day = AnnualSchedule::where('schedule_date', '=', $target_date)
                ->where('date_type', '=', 'workday')
                ->get();
            if(count($k_day) == 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.custom.holiday');
    }
}
