<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CustomTsuikaRequestDuplicate implements Rule
{
    protected $request_data = null;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($requestData)
    {
        $this->request_data = $requestData;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (empty($value)) {

            return true;
        }

        $tsuikaRequest = collect($this->request_data['tsuika_request']);
        $checkUnique = $tsuikaRequest->unique();

        // check if all input data are unique
        if ($tsuikaRequest->count() !== $checkUnique->count()) {

            return false;
        }

        $rawTsuikaRequest = $this->request_data['tsuika_request'];
        $compiledDatesAndTimes = [];

        foreach ($rawTsuikaRequest as $rawValue) {
            // check if nextrow date is in compiled dates
            if (array_key_exists($rawValue['add_date'], $compiledDatesAndTimes)) {
                $rowDate = $rawValue['add_date'];
                $rowStart = $rawValue['start_time'];
                $rowEnd = $rawValue['end_time'];
                $compiledDateTimes = $compiledDatesAndTimes[$rowDate];

                foreach ($compiledDateTimes as $compiledTime) {
                    //check of nextrow and compiled are equal
                    if ($compiledTime['start_time'] === $rowStart && $compiledTime['end_time'] === $rowEnd) {

                        return false;
                    }

                    $compiledStartTime = strtotime($compiledTime['start_time']);
                    $compiledEndTime = strtotime($compiledTime['end_time']);
                    $rowStartTime = strtotime($rowStart);
                    $rowEndTime = strtotime($rowEnd);

                    // check if compiled are in between nextrow start_time and end_time
                    if ($rowStartTime <= $compiledStartTime && $compiledEndTime <= $rowEndTime) {

                        return false;
                    }

                    // check if nextrow are in between compiled start_time and end_time
                    if ($compiledStartTime <= $rowStartTime && $rowEndTime <= $compiledEndTime) {

                        return false;
                    }

                    // check if nextrow start_time is in between compiled start_time and end_time
                    if ($compiledStartTime < $rowStartTime && $rowStartTime < $compiledEndTime) {

                        return false;
                    }

                    // check if nextrow end_time is in between compiled start_time and end_time
                    if ($compiledStartTime < $rowEndTime && $rowEndTime < $compiledEndTime) {

                        return false;
                    }
                }

                $compiledDatesAndTimes[$rowDate][] = [
                    'start_time' => $rowStart,
                    'end_time' => $rowEnd,
                ];
            } else {
                $compiledDatesAndTimes[$rawValue['add_date']][] = [
                    'start_time' => $rawValue['start_time'],
                    'end_time' => $rawValue['end_time'],
                ];
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.custom.tsuika_request.duplicate');
    }
}
