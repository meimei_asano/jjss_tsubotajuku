<?php

namespace App\Rules;

use App\Models\StudentAttendBase;
use Illuminate\Contracts\Validation\Rule;

class CustomTsuikaRequestDateAndTime implements Rule
{
    protected $request_data = null;
    protected $error_no = 0;
    protected $value = null;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($requestData)
    {
        $this->request_data = $requestData;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (empty($value)) {

            return true;
        }

        $this->value = $value;
        $field = explode('.', $attribute);
        $setNumber = $field[1];
        $date = $this->request_data['tsuika_request'][$setNumber]['add_date'];
        $day = strtolower(date('D', strtotime($date)));
        $start = $this->request_data['tsuika_request'][$setNumber]['start_time'] . ':00';
        $end = $this->request_data['tsuika_request'][$setNumber]['end_time'] . ':00';

        if ($start && $end) {
            $startTime = strtotime($start);
            $endTime = strtotime($end);
            $computationIsNotWholeNumber = !is_int((($endTime - $startTime) / 60 / 60));

            // check if start_time is equal to OR ahead of end_time OR if computation of end_time minus start_time is not a whole number
            if ($startTime >= $endTime || $computationIsNotWholeNumber) {
                $this->error_no = 1;

                return false;
            }

            if ($this->request_data['student_id']) {
                $studentAttendBase = StudentAttendBase::where('student_id', $this->request_data['student_id'])
                    ->where('start_date', '<=', $date)
                    ->where(function($studentAttendBase) use ($date){
                        $studentAttendBase->orWhere('end_date', '>=', $date)
                            ->orWhereNull('end_date');
                    })
                    ->orderBy('start_date', 'desc')
                    ->first();
                if($studentAttendBase === null) {
                    $this->error_no = 6;
                    return false;
                }
                if($studentAttendBase->course_id == "16") {//無制限コースの生徒は追加コマ不要
                    $this->error_no = 7;
                    return false;
                }
                $scheduleStartTime = $day . '_start_time';
                $scheduleEndTime = $day . '_end_time';
                $scheduleStartTime = $studentAttendBase->$scheduleStartTime;
                $scheduleEndTime = $studentAttendBase->$scheduleEndTime;

                // check if input start_time and end_time are the same as in student_attend_base
                if ($start === $scheduleStartTime && $end === $scheduleEndTime) {
                    $this->error_no = 2;

                    return false;
                }

                $scheduleStartTime = strtotime($scheduleStartTime);
                $scheduleEndTime = strtotime($scheduleEndTime);

                // check if compiled are in between nextrow start_time and end_time
                if ($startTime <= $scheduleStartTime && $scheduleEndTime <= $endTime) {
                    $this->error_no = 2;

                    return false;
                }

                // check if nextrow are in between compiled start_time and end_time
                if ($scheduleStartTime <= $startTime && $endTime <= $scheduleEndTime) {
                    $this->error_no = 2;

                    return false;
                }

                // check if nextrow start_time is in between compiled start_time and end_time
                if ($scheduleStartTime < $startTime && $startTime < $scheduleEndTime) {
                    $this->error_no = 2;

                    return false;
                }

                // check if nextrow end_time is in between compiled start_time and end_time
                if ($scheduleStartTime < $endTime && $endTime < $scheduleEndTime) {
                    $this->error_no = 2;

                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.custom.tsuika_request.' . $this->error_no, ['value' => $this->value]);
    }
}
