<?php

namespace App\Rules;

use App\Models\StudentAttendChangeTsuika;
use Illuminate\Contracts\Validation\Rule;

class CustomRequestDate implements Rule
{
    protected $id = null;
    protected $table_name = null;
    protected $error_no = 0;

    /**
     * Create a new rule instance.
     *
     * @param  string  $tableName
     * @param  int  $id
     * @return void
     */
    public function __construct(string $tableName, int $id)
    {
        $this->table_name = $tableName;
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->table_name === 'student_attend_change_tsuikas') {
            $studentAttendChangeTsuika = StudentAttendChangeTsuika::find($this->id);
            $originalRequestDate = $studentAttendChangeTsuika->request_date;

            // if date is not equal to original request_date
            if ($value !== $originalRequestDate) {
                $this->error_no = 1;

                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.custom.request_date.' . $this->error_no);
    }
}
