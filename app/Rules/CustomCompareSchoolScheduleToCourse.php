<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CustomCompareSchoolScheduleToCourse implements Rule
{
    protected $request_data = null;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($requestData)
    {
        $this->request_data = $requestData;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $requestData = $this->request_data;

        // 無制限コースは無条件にtrue
        if ($requestData['course_id'] == \JJSS::getUnlimitedCourse()) {
            return true;
        }

        $courseTotalHours = \JJSS::getTotalHoursOfCourse($value);
        $monStart = $requestData['mon_start_time'];
        $tueStart = $requestData['tue_start_time'];
        $wedStart = $requestData['wed_start_time'];
        $thuStart = $requestData['thu_start_time'];
        $friStart = $requestData['fri_start_time'];
        $monEnd = $requestData['mon_end_time'];
        $tueEnd = $requestData['tue_end_time'];
        $wedEnd = $requestData['wed_end_time'];
        $thuEnd = $requestData['thu_end_time'];
        $friEnd = $requestData['fri_end_time'];
        $combinedTotalTime = 0;

        if ($monStart && $monEnd) {
            $computation = $this->computeTimeDifference($monStart, $monEnd);

            if ($computation) {
                $combinedTotalTime += $computation;
            }
        }

        if ($tueStart && $tueEnd) {
            $computation = $this->computeTimeDifference($tueStart, $tueEnd);

            if ($computation) {
                $combinedTotalTime += $computation;
            }
        }

        if ($wedStart && $wedEnd) {
            $computation = $this->computeTimeDifference($wedStart, $wedEnd);

            if ($computation) {
                $combinedTotalTime += $computation;
            }
        }

        if ($thuStart && $thuEnd) {
            $computation = $this->computeTimeDifference($thuStart, $thuEnd);

            if ($computation) {
                $combinedTotalTime += $computation;
            }
        }

        if ($friStart && $friEnd) {
            $computation = $this->computeTimeDifference($friStart, $friEnd);

            if ($computation) {
                $combinedTotalTime += $computation;
            }
        }

        return $courseTotalHours == $combinedTotalTime;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.custom.compare_school_schedule_to_course');
    }

    /**
     * Compute time difference of start time and end time
     *
     * @param  string  $start
     * @param  string  $end
     * @return bool | integer
     */
    protected function computeTimeDifference($start, $end)
    {
        $start = strtotime($start);
        $end = strtotime($end);

        if ($end > $start) {
            $timeDiff = $end - $start;

            return ($timeDiff / 60) / 60;
        }

        return false;
    }
}
