<?php

namespace App\Validation;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;

class StudentAttendChangeTsuikaDeleteValidation
{
    public static function validate(Request $request)
    {
        $id = $request->input('id');

        $rules = self::getRules();

        $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails()) {
            return false;
        } else {
            return $validator->errors();
        }
    }

    private static function getRules()
    {
        return self::getCommonRules();
    }

    private static function getCommonRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                Rule::exists('student_attend_change_tsuikas')->where(function ($query) {
                    $query->whereNull('deleted_at');
                }),
            ]
        ];
    }
}
