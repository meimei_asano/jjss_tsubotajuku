<?php

namespace App\Validation;

use Illuminate\Http\Request;
use Validator;

class StudentSeikyuValidation
{
    public static function validate(Request $request)
    {
        $student_id = $request->input('student_id');
        $rules = ($student_id) ? self::getModifyRules($student_id) : self::getCreateRules();

        $validator = Validator::make($request->all(), $rules);
        if (! $validator->fails()) {
            return false;
        } else {
            return $validator->errors();
        }
    }

    private static function getCreateRules()
    {
        return array_merge(
            self::getCommonRules(),
            [
            ]
        );
    }

    private static function getModifyRules($student_id)
    {
        $rules = self::getCommonRules();
        return $rules;
    }

    private static function getCommonRules()
    {
        return [
            'hikiotoshi_uketori_date' => 'nullable|date',
        ];
    }
}
