<?php

namespace App\Validation;

use Validator;

class StudentRequestValidation
{
    public static function validate(array $requestData, string $type)
    {
        $id = $requestData['id'];
        $rules = ($id)
            ? ($type === 'syounin' ? ['id' => 'required|integer|exists:student_requests,id'] : self::getModifyRules($id))
            : self::getCreateRules();

        $validator = Validator::make($requestData, $rules);

        if (!$validator->fails()) {
            return false;
        } else {
            return $validator->errors();
        }
    }

    private static function getCreateRules()
    {
        return self::getCommonRules();
    }

    private static function getModifyRules($id)
    {
        $rules = self::getCommonRules();
        $rules['id'] = 'required|integer|exists:student_requests,id';
        return $rules;
    }

    private static function getCommonRules()
    {
        return [
            'student_id' => 'required|integer|exists:students,id',
            'request_date' => 'required|date',
            'request_type' => 'required|in:' . implode(',', config('jjss.request_type')),
            'target_date' => 'required|date',
            'kyujyuku_end_date' => 'nullable|required_if:request_type,休塾|date',
        ];
    }
}
