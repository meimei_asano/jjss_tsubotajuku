<?php

namespace App\Validation;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;

class StudentRequestDeletesValidation
{
    public static function validate(Request $request)
    {
        $ids = $request->input('ids');
        $requestData = ['id' => explode(',', $ids)];

        $rules = self::getRules();
        $validator = Validator::make($requestData, $rules);
        if (!$validator->fails()) {
            return false;
        } else {
            return $validator->errors();
        }
    }

    private static function getRules()
    {
        return self::getCommonRules();
    }

    private static function getCommonRules()
    {
        return [
/*
            'id.*' => [
                'required',
                'integer',
                Rule::exists('student_requests')->where(function ($query) {
                    $query->whereNull('deleted_at');
                }),
            ]
*/
        ];
    }
}
