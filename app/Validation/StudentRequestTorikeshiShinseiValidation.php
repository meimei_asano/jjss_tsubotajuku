<?php

namespace App\Validation;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Validation\Rule;

class StudentRequestTorikeshiShinseiValidation
{
    public static function validate(Request $request)
    {
        $rules = self::getRules();
        $validator = Validator::make($request->all(), $rules);
        if (!$validator->fails()) {
            return false;
        } else {
            return $validator->errors();
        }
    }

    private static function getRules()
    {
        return self::getCommonRules();
    }

    private static function getCommonRules()
    {
        return [
            'id' => [
                'required',
                'integer',
                Rule::exists('student_requests')->where(function ($query) {
                    $query->whereNull('deleted_at');
                }),
            ]
        ];
    }
}
