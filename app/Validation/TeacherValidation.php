<?php

namespace App\Validation;

use Illuminate\Http\Request;
use App\Models\Teacher;

use Validator;

class TeacherValidation
{
    public static function validate(Request $request)
    {
        $teacher_id = $request->input('teacher_id');
        $rules = ($teacher_id) ? self::getModifyRules($teacher_id) : self::getCreateRules();
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $validator->errors();
        }

        return false;
    }

    private static function getCreateRules()
    {
        $rules = self::getCommonRules();
        $rules['password'] = 'required|string|min:6|confirmed';
        $rules['email'] = 'required|email|unique:users,email';

        return $rules;
    }

    private static function getModifyRules($teacher_id)
    {
        $teacher = Teacher::find($teacher_id);

        $rules = self::getCommonRules();
        $rules['password'] = 'nullable|string|min:6|confirmed';
        //$rules['email'] = $rules['email'] . ',' . $teacher_id . ',id';
        $rules['email'] = 'required|email|unique:users,email,' . $teacher->user_id . ',id';

        return $rules;
    }

    private static function getCommonRules()
    {
        return [
            'name' => 'required|string|max:50',
            'kana' => 'string|max:50',
            //'email' => 'required|email|unique:users,email',
            'nyusya_date' => 'required|date',//入社日
            'tantou_school_id' => 'required|integer|exists:schools,id',//校舎
            'koyou_keitai' => 'required|string|max:50|in:正社員,アルバイト',
            'permission' => 'required|string|max:50|in:校長,講師,本部スタッフ,マネージャー,管理者',
        ];
    }
}
