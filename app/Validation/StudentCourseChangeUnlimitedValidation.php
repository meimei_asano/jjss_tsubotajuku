<?php

namespace App\Validation;

use App\Rules\CustomChangeYoteiDate;
use App\Rules\CustomSchoolSchedule;
use App\Rules\CustomCompareSchoolScheduleToCourse;
use Validator;

class StudentCourseChangeUnlimitedValidation
{
    public static function validate(array $requestData, string $type)
    {
        $id = $requestData['id'];
        $rules = ($id)
            ? (in_array($type, ['syounin', 'cancel_approval']) ? ['id' => 'required|integer|exists:student_course_changes,id'] : self::getModifyRules($id, $requestData))
            : self::getCreateRules($requestData);

        $validator = Validator::make($requestData, $rules);

        if (!$validator->fails()) {
            return false;
        } else {
            return $validator->errors();
        }
    }

    private static function getCreateRules($requestData)
    {
        return self::getCommonRules($requestData);
    }

    private static function getModifyRules($id, $requestData)
    {
        $rules = self::getCommonRules($requestData);
        $rules['id'] = 'required|integer|exists:student_course_changes,id';
        return $rules;
    }

    private static function getCommonRules($requestData)
    {
        return [
            'student_id' => 'required|integer|exists:students,id',
            'request_date' => 'required|date',
            'change_yotei_date' => 'required|date',
            'end_date' => 'nullable|date',
            'current_course_id' => 'integer|exists:courses,id',
            //'current_course_id' => 'required_unless:student_id,|integer|exists:courses,id',
            'course_id' => 'required|integer|exists:courses,id|different:current_course_id',
        ];
    }
}
