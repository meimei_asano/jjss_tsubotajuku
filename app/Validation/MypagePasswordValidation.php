<?php

namespace App\Validation;

use Illuminate\Http\Request;
use Validator;

class MypagePasswordValidation
{
    public static function validate(Request $request)
    {
        $user_id = $request->input('user_id');
        $rules = self::getModifyRules($user_id);

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $validator->errors();
        }

        return false;
    }

    private static function getModifyRules($user_id)
    {
        $rules = self::getCommonRules();
        return $rules;
    }

    private static function getCommonRules()
    {
        $password = \Auth::user()->password;
        return [
            'old_password' => 'required|string|old_password:'. $password,
            'password' => 'required|string|min:6|confirmed',
            ];
    }
}
