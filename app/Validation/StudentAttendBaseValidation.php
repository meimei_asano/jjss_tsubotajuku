<?php

namespace App\Validation;

use Illuminate\Http\Request;
use Validator;

class StudentAttendBaseValidation
{
    public static function validate(Request $request)
    {
        $attend_base_id = $request->input('attend_base_id');
        $rules = ($attend_base_id) ? self::getModifyRules() : self::getCreateRules();

        $validator = Validator::make($request->all(), $rules);
        if (! $validator->fails()) {
            return false;
        } else {
            return $validator->errors();
        }
    }

    private static function getCreateRules()
    {
        return array_merge(
            self::getCommonRules(),
            [
            ]
        );
    }

    private static function getModifyRules()
    {
        return array_merge(
            self::getCommonRules(),
            [
            ]
        );
    }

    private static function getCommonRules()
    {
        return [
            //'student_id' => 'required|integer|exists:students,id',
            //'start_date' => 'required|date',
            //'end_date' => 'nullable|date',
            //'course_id' => 'required|exists:courses,id',
            'mon_start_time' => 'nullable|',
            'mon_end_time' => 'nullable|',
            'tue_start_time' => 'nullable|',
            'tue_end_time' => 'nullable|',
            'wed_start_time' => 'nullable|',
            'wed_end_time' => 'nullable|',
            'thu_start_time' => 'nullable|',
            'thu_end_time' => 'nullable|',
            'fri_start_time' => 'nullable|',
            'fri_end_time' => 'nullable|',
        ];
    }
}
