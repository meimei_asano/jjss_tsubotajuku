<?php

namespace App\Validation;

use Illuminate\Http\Request;
use Validator;

class StudentAttendSpecialValidation
{
    public static function validate(Request $request)
    {
        $rules = self::getCreateRules();

        $validator = Validator::make($request->all(), $rules);
        if (! $validator->fails()) {
            return false;
        } else {
            return $validator->errors();
        }
    }

    private static function getCreateRules()
    {
        return array_merge(
            self::getCommonRules(),
            [
            ]
        );
    }

    private static function getCommonRules()
    {
        return [
            'student_id' => 'required|integer|exists:students,id',
            'term_id' => 'required|integer|exists:terms,id',
            'start_time' => 'required|array',
            'start_time.*' => 'nullable',
            'end_time' => 'required|array',
            'end_time.*' => 'nullable',
            'attend_date' => 'required|array',
            'attend_date.*' => 'required|date',
            'attend_special_id' => 'required|array',
            'attend_special_id.*' => 'nullable|integer|exists:student_attend_specials,id',
        ];
    }
}
