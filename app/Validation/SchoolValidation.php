<?php

namespace App\Validation;

use Illuminate\Http\Request;
use Validator;

class SchoolValidation
{
    public static function validate(Request $request)
    {
        $school_id = $request->input('school_id');
        $rules = ($school_id) ? self::getModifyRules($school_id) : self::getCreateRules();

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $validator->errors();
        }

        return false;
    }

    private static function getCreateRules()
    {
        $rules = self::getCommonRules();
        $rules['name'] = $rules['name'] . '|unique:schools,name';
        return $rules;
    }

    private static function getModifyRules($school_id)
    {
        $rules = self::getCommonRules();
        $rules['name'] = $rules['name'] . '|unique:schools,name,' . $school_id . ',id';
        return $rules;
    }

    private static function getCommonRules()
    {
        return [
            'name' => 'required|string|max:50',
            'school_code' => 'required|regex:/^[0-9]{3}$/',
            'zip_code' => 'string|regex:/^[0-9]{7}$/',
            'pref' => 'required|string|in:' . implode(',', config('pref')),
            'address' => 'string',
            'building' => 'string',
            'tel' => 'string',
            'fax' => 'string',
            'price_table' => 'required|string|max:1|regex:/[A-Z]/',
            'seats' => 'required|integer',
        ];
    }
}
