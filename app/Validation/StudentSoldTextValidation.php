<?php

namespace App\Validation;

use Validator;
use App\Rules\CustomCheckHoliday;


class StudentSoldTextValidation
{
    public static function validate(array $requestData, string $type)
    {
        $id = $requestData['id'];
        $rules = ($id)
            ? ($type === 'syounin' ? ['id' => 'required|integer|exists:student_sold_texts,id'] : self::getModifyRules($id, $requestData))
            : self::getCreateRules($requestData);

        $validator = Validator::make($requestData, $rules);

        if (!$validator->fails()) {
            return false;
        } else {
            return $validator->errors();
        }
    }

    private static function getCreateRules($requestData)
    {
        return self::getCommonRules($requestData);
    }

    private static function getModifyRules($id, $requestData)
    {
        $rules = self::getCommonRules($requestData);
        $rules['id'] = 'required|integer|exists:student_sold_texts,id';
        return $rules;
    }

    private static function getCommonRules($requestData)
    {
        return [
            'student_id' => 'required|integer|exists:students,id',
            'request_date' => [
                'bail',
                'required',
                'date',
                new CustomCheckHoliday($requestData),
            ],
            'text_request' => 'required',
            'text_request.*.text' => 'required|string',
            'text_request.*.price' => 'required|integer',
        ];
    }
}
