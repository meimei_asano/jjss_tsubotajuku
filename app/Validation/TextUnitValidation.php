<?php

namespace App\Validation;

use Illuminate\Http\Request;
use Validator;

class TextUnitValidation
{
    public static function validate(Request $request)
    {
        $text_id = $request->input('text_id');
        $rules = ($text_id) ? self::getModifyRules() : self::getCreateRules();

        $validator = Validator::make($request->all(), $rules);
        if (! $validator->fails()) {
            return false;
        } else {
            return $validator->errors();
        }
    }

    private static function getCreateRules()
    {
        return array_merge(
            self::getCommonRules(),
            [
            ]
        );
    }

    private static function getModifyRules()
    {
        return array_merge(
            self::getCommonRules(),
            [
            ]
        );
    }

    private static function getCommonRules()
    {
        return [
                ];
    }
}
