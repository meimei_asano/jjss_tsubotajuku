<?php

namespace App\Validation;

use Illuminate\Http\Request;
use Validator;

use App\Models\Text;


class TextDeleteValidation
{
    public static function validate(Request $request)
    {
        $text_id = $request->input('text_id');
        $rules = self::getRules();

        $validator = Validator::make($request->all(), $rules);
        if (! $validator->fails()) {
            return false;
        } else {
            return $validator->errors();
        }
    }

    private static function getRules()
    {
        return [
            'text_id' => [
                'required',
                function ($attribute, $value, $fail) {

                    $text = Text::find($value);
                    if($text) {
                        if(strlen($text->sequence) > 0) {
                           return $fail('学習順に設定されています。まずは学習順の設定から削除してください。');
                        }
                    }
                },
                function ($attribute, $value, $fail) {
                    $text = Text::where("branch_next_text_id", "=", $value)->get();
                    if(count($text) > 0) {
                        return $fail("枝分かれテキストに指定されています。まずは指定をクリアしてください。");
                    }
                },
            ]
        ];
    }

}

