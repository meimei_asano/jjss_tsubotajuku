<?php

namespace App\Validation;

use Validator;

class StudentAttendSpecialTsuikaValidation
{
    public static function validate(array $requestData, string $type)
    {
        $id = $requestData['id'];
        $rules = ($id)
            ? ($type === 'syounin' ? ['id' => 'required|integer|exists:student_attend_special_tsuikas,id'] : self::getModifyRules($id))
            : self::getCreateRules();

        $validator = Validator::make($requestData, $rules);

        if (!$validator->fails()) {
            return false;
        } else {
            return $validator->errors();
        }
    }

    private static function getCreateRules()
    {
        return self::getCommonRules();
    }

    private static function getModifyRules($id)
    {
        $rules = self::getCommonRules();
        $rules['id'] = 'required|integer|exists:student_attend_special_tsuikas,id';
        return $rules;
    }

    private static function getCommonRules()
    {
        return [
            'student_id' => 'required|integer|exists:students,id',
            'request_date' => 'required|date',
            'term_id' => 'required|integer|exists:terms,id',
            'total_hours' => 'required|integer|greater_than:base_hours',
            'base_hours' => 'required|integer',
        ];
    }
}