<?php

namespace App\Validation;

use Illuminate\Http\Request;
use Validator;

class TermValidation
{
    public static function validate(Request $request)
    {
        $term_id = $request->input('term_id');
        $rules = ($term_id) ? self::getModifyRules() : self::getCreateRules();

        $validator = Validator::make($request->all(), $rules);
        if (! $validator->fails()) {
            return false;
        } else {
            return $validator->errors();
        }
    }

    private static function getCreateRules()
    {
        return array_merge(
            self::getCommonRules(),
            [
            ]
        );
    }

    private static function getModifyRules()
    {
        return array_merge(
            self::getCommonRules(),
            [
            ]
        );
    }

    private static function getCommonRules()
    {
        return [
            'term_name' => 'required|string|max:50',
            'display_name' => 'required|string|max:50',
            'term_type' => 'required|string|max:50|in:通常,特別講習',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date',
        ];
    }
}
