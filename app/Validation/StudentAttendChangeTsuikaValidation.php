<?php

namespace App\Validation;

use App\Rules\CustomRequestDate;
use App\Rules\CustomTsuikaRequestDateAndTime;
use App\Rules\CustomTsuikaRequestDateAndTimeBase;
use App\Rules\CustomTsuikaRequestDuplicate;
use App\Rules\CustomTsuikaRequestDate;
use Validator;
use Carbon\Carbon;

class StudentAttendChangeTsuikaValidation
{
    public static function validate(array $requestData, string $type)
    {
        $id = $requestData['id'];
        $rules = ($id)
            ? ($type === 'syounin' ? ['id' => 'required|integer|exists:student_attend_change_tsuikas,id'] : self::getModifyRules($id, $requestData))
            : self::getCreateRules($requestData);

        $validator = Validator::make($requestData, $rules);

        if (!$validator->fails()) {
            return false;
        } else {
            return $validator->errors();
        }
    }

    private static function getCreateRules($requestData)
    {
        $rules = self::getCommonRules($requestData);
        $rules['request_date'][] = 'after_or_equal:' . Carbon::today()->subDay(7)->format('Y-m-d');  // 7日前までバックデート可
        return $rules;
    }

    private static function getModifyRules($id, $requestData)
    {
        $rules = self::getCommonRules($requestData);
        $rules['id'] = 'required|integer|exists:student_attend_change_tsuikas,id';
        $rules['request_date'][] = new CustomRequestDate('student_attend_change_tsuikas', $id);
        return $rules;
    }

    private static function getCommonRules($requestData)
    {
        return [
            'student_id' => [
                'required',
                'integer',
                'exists:students,id',
            ],
            'request_date' => [
                'required',
                'date',
            ],
            'tsuika_request' => [
                'required',
                new CustomTsuikaRequestDuplicate($requestData),
                new CustomTsuikaRequestDate($requestData),
            ],
            'tsuika_request.*.start_time' => 'bail|required|date_format:"H:i"',
            'tsuika_request.*.end_time' => 'bail|required|date_format:"H:i"',
            'tsuika_request.*.add_date' => [
                'bail',
                'required',
                'date',
                'after:request_date',
                new CustomTsuikaRequestDateAndTimeBase($requestData),
                new CustomTsuikaRequestDateAndTime($requestData),
            ],
            'tsuika_request.*.hours' => 'required',
            'tsuika_request.*.kyouka' => 'nullable|string',
            'tsuika_hours' => 'required',
            'unit_price' => 'required|numeric',
        ];
    }
}
