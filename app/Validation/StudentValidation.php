<?php

namespace App\Validation;

use Illuminate\Http\Request;
use Validator;

class StudentValidation
{
    public static function validate(Request $request)
    {
        $student_id = $request->input('student_id');
        $rules = ($student_id) ? self::getModifyRules($student_id) : self::getCreateRules();

        $validator = Validator::make($request->all(), $rules);
        if (! $validator->fails()) {
            return false;
        } else {
            return $validator->errors();
        }
    }

    private static function getCreateRules()
    {
        return array_merge(
            self::getCommonRules(),
            [
            ]
        );
    }

    private static function getModifyRules($student_id)
    {
        $rules = self::getCommonRules();
        $rules['student_code'] = $rules['student_code'] . ',' . $student_id . ',id';
        return $rules;
    }

    private static function getCommonRules()
    {
        return [
            'student_code' => 'required|regex:/^\d{3}-\d{2}-\d{3}$/|unique:students,student_code',
            'name' => 'required|string|max:50',
            'kana' => 'string|max:50',
            'school_id' => 'required|integer|exists:schools,id',
            'teacher_id' => 'nullable|integer|exists:teachers,id',
            'gakkou_name' => 'max:50',
            'grade_code' => 'required|in:' . implode(',', array_keys(config('jjss.grade'))),
            'nyujyuku_date' => 'required|date',//入塾日
            'taijyuku_date' => 'nullable|date',//退塾日
            'sotsujyuku_date' => 'nullable|date',//卒塾日
            //'hikiotoshi_uketori_date' => 'nullable|date',//引き落し受取日
            'birth_date' => 'nullable|date',//生年月日
            'zip_code' => 'required|alpha_dash',//郵便番号
            'pref' => 'required',//都道府県
            'address1' => 'required|max:255',//市区町村・番地
            'address2' => 'nullable|max:255',//建物・マンション名
            'home_tel' => 'regex:/^[0-9-¥(¥)]+$/|max:50',//自宅電話番号
            'student_tel' => 'regex:/^[0-9-¥(¥)]+$/|max:50',//本人電話番号
            'hogosya_tel' => 'regex:/^[0-9-¥(¥)]+$/|max:50',//保護者電話番号
            'hogosya_email' => 'nullable|email',
          //  'hogosya_enquete_file' => '',
          //  'student_enquete_file' => '',
        ];
    }
}
