<?php

namespace App\Validation;

use App\Rules\CustomCompareSchoolScheduleToCourse;
use Carbon\Carbon;
use Validator;

class MendanKirokuValidation
{
    public static function validate(array $requestData)
    {
        $id = $requestData['id'];
        $rules = ($id) ? self::getModifyRules($id, $requestData) : self::getCreateRules($requestData);
        $validator = Validator::make($requestData, $rules,
            $messages = ['required_if' => '通塾コースが無制限の時、無制限終了日は必須です。']
        );

        if (!$validator->fails()) {
            return false;
        } else {
            return $validator->errors();
        }
    }

    private static function getCreateRules($requestData)
    {
        return self::getCommonRules($requestData);
    }

    private static function getModifyRules($id, $requestData)
    {
        $rules = self::getCommonRules($requestData);
        $rules['id'] = 'required|integer|exists:mendan_kirokus,id';
        $rules['email'] = $rules['email'] . ',' . $id . ',id';
        $rules['hogosya_email'] = $rules['hogosya_email'] . ',' . $id . ',id';
        return $rules;
    }

    private static function getCommonRules($requestData)
    {
        return [
            'school_id' => 'required|integer|exists:schools,id',
            'mendan_date' => 'required|date',
            'mendan_teacher_id' => 'integer|exists:teachers,id',
            'manager_teacher_id' => 'integer|exists:teachers,id',
            'name' => 'required|string|max:50',
            'kana' => 'required|string|max:50',
            'birth_date' => 'required|date',
            'gender' => 'required|in:' . implode(',', array_keys(config('jjss.gender'))),
            'gakkou_name' => 'required|string|max:50',
            'grade_code' => 'required|in:' . implode(',', array_keys(config('jjss.grade'))),
            'zip_code' => 'required|alpha_dash',
            'pref' => 'required|string|max:50',
            'address1' => 'required|string|max:50',
            'address2' => 'string|max:50',
            'tel' => 'string|max:50',
            'email' => 'email|max:255|unique:mendan_kirokus,email',
            'hogosya_name' => 'required|string|max:50',
            'hogosya_kana' => 'required|string|max:50',
            'hogosya_tsudukigara' => 'required|string|max:50',
            'hogosya_home_tel' => 'string|max:50',
            'hogosya_fax' => 'string|max:50',
            'hogosya_tel' => 'required|string|max:50',
            'hogosya_email' => 'required|email|max:255',
            'orientation_date' => [
                'required',
                'required_unless:orientation_time,',
                'date',
                'before_or_equal:first_shidou_date',
                function ($attribute, $value, $fail) use ($requestData) {
                    // 初回OT日が休塾日である
                    $orientation_date = Carbon::parse($requestData['orientation_date']);
                    $date_type = \JJSS::getTypeOfDate($requestData['school_id'], $orientation_date);
                    if ($date_type == \JJSS::DATE_TYPE['holiday']) {
                        $fail('初回OT日には休塾日を指定することはできません。');
                    }
                },
            ],
            'orientation_time' => [
                'bail',  // どこかでエラーになったら以降のバリデーションを中止
                'required',
                'required_unless:orientation_date,',
                'date_format:"H:i"',
                'time_range:"13:00","21:40"',
            ],
            'first_shidou_date' => [
                'bail',
                'required',
                'date',
                function ($attribute, $value, $fail) use ($requestData) {
                    // 初回授業日が休塾日である
                    $first_shidou_date = Carbon::parse($requestData['first_shidou_date']);
                    $date_type = \JJSS::getTypeOfDate($requestData['school_id'], $first_shidou_date);
                    if ($date_type == \JJSS::DATE_TYPE['holiday']) {
                        $fail('初回授業日には休塾日を指定することはできません。');
                    }
                },
                function ($attribute, $value, $fail) use ($requestData) {
                    // 初回授業日と通塾曜日が合っていない
                    $first_shidou_date = Carbon::parse($requestData['first_shidou_date']);
                    $week = \JJSS::week($first_shidou_date);
                    if ($week == 'sat' || $week == 'sun') {
                        $fail('初回授業日には土日を指定することはできません。');
                    } else {
                        if ($requestData['course_id'] != \JJSS::getUnlimitedCourse() && $requestData['course_id'] != 17 && $requestData[$week. '_start_time'] == '') {
                            $fail('初回授業日には通塾予定でない曜日を指定することはできません。');
                        }
                    }
                }
            ],
            'kamoku_ids' => 'required',
            'kamoku_ids.*' => 'exists:kamokus,id',
            'course_id' => [
                'required',
                'integer',
                'exists:courses,id',
                new CustomCompareSchoolScheduleToCourse($requestData),
            ],
            'mon_start_time' => 'bail|nullable|date_format:"H:i"|time_range:"16:00","21:40"',
            'mon_end_time' => [
                'bail',
                'nullable',
                'date_format:"H:i"',
                'time_range:"16:00","21:40"',  // "16:00","21:40" なら省略可能(デフォルト値)
                function ($attribute, $value, $fail) use ($requestData) {
                    if ($requestData['mon_start_time'] >= $requestData['mon_end_time']) {
                        $fail('月曜終了時間には月曜開始時間以降の時刻を指定してください。');
                    }
                }
            ],
            'tue_start_time' => 'bail|nullable|date_format:"H:i"|time_range:"16:00","21:40"',
            'tue_end_time' => [
                'bail',
                'nullable',
                'date_format:"H:i"',
                'time_range:"16:00","21:40"',
                function ($attribute, $value, $fail) use ($requestData) {
                    if ($requestData['tue_start_time'] >= $requestData['tue_end_time']) {
                        $fail('火曜終了時間には火曜開始時間以降の時刻を指定してください。');
                    }
                }
            ],
            'wed_start_time' => 'bail|nullable|date_format:"H:i"|time_range:"16:00","21:40"',
            'wed_end_time' => [
                'bail',
                'nullable',
                'date_format:"H:i"',
                'time_range:"16:00","21:40"',
                function ($attribute, $value, $fail) use ($requestData) {
                    if ($requestData['wed_start_time'] >= $requestData['wed_end_time']) {
                        $fail('水曜終了時間には水曜開始時間以降の時刻を指定してください。');
                    }
                }
            ],
            'thu_start_time' => 'bail|nullable|date_format:"H:i"|time_range:"16:00","21:40"',
            'thu_end_time' => [
                'bail',
                'nullable',
                'date_format:"H:i"',
                'time_range:"16:00","21:40"',
                function ($attribute, $value, $fail) use ($requestData) {
                    if ($requestData['thu_start_time'] >= $requestData['thu_end_time']) {
                        $fail('木曜終了時間には木曜開始時間以降の時刻を指定してください。');
                    }
                }
            ],
            'fri_start_time' => 'bail|nullable|date_format:"H:i"|time_range:"16:00","21:40"',
            'fri_end_time' => [
                'bail',
                'nullable',
                'date_format:"H:i"',
                'time_range:"16:00","21:40"',
                function ($attribute, $value, $fail) use ($requestData) {
                    if ($requestData['fri_start_time'] >= $requestData['fri_end_time']) {
                        $fail('金曜終了時間には金曜開始時間以降の時刻を指定してください。');
                    }
                }
            ],
            'unlimited_end_date' => [
                'required_if:course_id,'. \JJSS::getUnlimitedCourse(),
                function ($attribute, $value, $fail) use ($requestData) {
                    if ($requestData['course_id'] != \JJSS::getUnlimitedCourse()) return;
                    if (strtotime($requestData['unlimited_end_date']) === false) {
                        $fail('無制限終了日には正しい形式の日付を指定してください。');
                    }
                    if ($requestData['unlimited_end_date'] < $requestData['first_shidou_date']) {
                        $fail('無制限終了日には初回授業日かそれ以降の日付を指定してください。');
                    }
                },
            ],
            'unlimited_discount_flag' => 'nullable|integer',
            'seikyu_data' => [
                'required',
                'string',
                function ($attribute, $value, $fail) use ($requestData) {
                    //$sb = array_filter($requestData['seikyu_breakdown']);
                    $sb = $requestData['seikyu_breakdown'];
                    $sbCount = count($sb);
                    //$sa = array_filter($requestData['seikyu_amount'], function($v, $k){ return ($v !== ''); }, ARRAY_FILTER_USE_BOTH);
                    $sa = $requestData['seikyu_amount'];
                    $saCount = count($sa);

                    if ($saCount === 0) {
                        $fail('請求内訳は必須です。');
                    } elseif ($sbCount !== $saCount) {
                        $fail('請求フィールドの内訳と金額はペアにする必要があります。');
                    }
                },
            ],
            'seikyu_breakdown' => 'nullable|array',
            'seikyu_amount' => 'nullable|array',
            'seikyu_amount.*' => 'nullable|numeric',
        ];
    }
}
