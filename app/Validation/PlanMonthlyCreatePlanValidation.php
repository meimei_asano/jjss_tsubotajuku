<?php

namespace App\Validation;

use Illuminate\Http\Request;
use Validator;

class PlanMonthlyCreatePlanValidation
{
    public static function validate(Request $request)
    {
        $student_id = $request->input('student_id');
        $rules = self::getRules();

        $validator = Validator::make($request->all(), $rules);
        if (! $validator->fails()) {
            return false;
        } else {
            return $validator->errors();
        }
    }

    private static function getRules()
    {
        return [
        //    'hikiotoshi_uketori_date' => 'nullable|date',
        ];
    }

}
