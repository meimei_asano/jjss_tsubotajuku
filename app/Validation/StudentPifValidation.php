<?php

namespace App\Validation;

use Illuminate\Http\Request;
use Validator;

class StudentPifValidation
{
    public static function validate(Request $request)
    {
        $student_id = $request->input('student_id');
        $rules = ($student_id) ? self::getModifyRules($student_id) : self::getCreateRules();

        $validator = Validator::make($request->all(), $rules);
        if (! $validator->fails()) {
            return false;
        } else {
            return $validator->errors();
        }
    }

    private static function getCreateRules()
    {
        return array_merge(
            self::getCommonRules(),
            [
            ]
        );
    }

    private static function getModifyRules($student_id)
    {
        $rules = self::getCommonRules();
        return $rules;
    }

    private static function getCommonRules()
    {
        return [
            //student_shibou
            'shibou_school' => 'string|max:50',
            'shibou_gakubu' => 'string|max:50',
            //'shibou_kamokus' => '',//文字数制限は特になし
            //student_pif
            /*
            'bukatsu' => '',
            'gakkou_tokki' => '',
            'yume' => '',
            'syumi' => '',
            'kazoku_kousei' => '',
            'katei_jijyou' => '',
            'akaten_shinkyu' => '',
            'teiki_test' => '',
            'hairyo_jikou' => '',
            'iwanai' => '',
            */
        ];
    }
}
