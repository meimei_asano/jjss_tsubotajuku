<?php

namespace App\Library;

use App\Models\AnnualSchedule;
use App\Models\Course;
use App\Models\Kamoku;
use App\Models\Price;
use App\Models\School;
use App\Models\Student;
use App\Models\Term;
use App\Models\StudentAttendBase;
use App\Models\StudentAttendSpecial;
use App\Models\StudentAttendChange;
use App\Models\StudentNote;
use App\Models\StudentKyuujyuku;
use Carbon\Carbon;

class JJSS
{
    const DATE_TYPE =[
        'holiday' => '休日',
        'basic' => '通常',
        'special' => '特別講習',
    ];

    // 対象日付の種別を取得する
    // 'basic', 'special', 'holiday'
    public static function getTypeOfDate($school_id, Carbon $target_date)
    {
        // 年間スケジュールで休み: holiday
        // 年間スケジュールで稼働(workday): この場合は以下判定へ
        // 通常期間...
        //   workday: basic
        //   土日: holiday
        //   平日: basic
        // 特別講習期間...
        //   workday: special
        //   日曜: holiday
        //   平日: special

        // 休日指定されているかどうか
        $query = AnnualSchedule::where('schedule_date', $target_date)
            ->whereIn('date_type', ['holiday', 'workday']);
        if ($school_id) {
            $query->where(function($q) use ($school_id){
                $q->whereNull('school_id')
                    ->orWhere('school_id', 0)
                    ->orWhere('school_id', $school_id);
            });
        }
        $date_type = $query->value('date_type');

        if ($date_type == 'holiday') {
            return self::DATE_TYPE['holiday'];
        }

        // 通常期間、特別講習期間をチェック
        $target_term_type = Term::where('start_date', '<=', $target_date)
                           ->where('end_date', '>=', $target_date)
                           ->value('term_type');

        if ($target_term_type == '通常') {
            // 稼働日の場合
            if ($date_type == 'workday') {
                return self::DATE_TYPE['basic'];
            }
            // 通常期間の同日は休み
            $is_weekend = $target_date->isWeekend();
            if ($is_weekend) {
                return self::DATE_TYPE['holiday'];
            } else {
                return self::DATE_TYPE['basic'];
            }
        } elseif ($target_term_type == '特別講習') {
            // 稼働日の場合
            if ($date_type == 'workday') {
                return self::DATE_TYPE['special'];
            }
            $is_sunday = $target_date->isSunday();
            if ($is_sunday) {
                return self::DATE_TYPE['holiday'];
            } else {
                return self::DATE_TYPE['special'];
            }
        }

        // 期間が正しく設定されていないので防御的な処理とする
        return self::DATE_TYPE['holiday'];
    }

    // 日付文字列が与えられた場合はCarbon形式に変換して返し、与えられない場合はシステム日付をCarbon形式で返す
    public static function getTargetDate(string $date = null)
    {
        if (! $date) {
            $target_date = Carbon::today();
        } else {
            $temp = \DateTime::createFromFormat('Y-m-d', $date);  // 渡された形式が正しいかをチェック
            if (! $temp) {
                $target_date = false;
            } else {
                $target_date = Carbon::parse($temp->format('Y-m-d'));
            }
        }

        return $target_date;
    }

    public static function displayNineType(string $ninetype_code, $display_type = '')
    {
        $nums = ['', '①', '②', '③', '④', '⑤', '⑥', '⑦', '⑧', '⑨',];
        $display = '';

        if (! $ninetype_code) return '';

        $ninetype_codes = explode(',', $ninetype_code);
        foreach ($ninetype_codes as $code) {
            if(strlen($code) == 0 || isset($nums[$code]) === false) {
                continue;
            }
            if($display_type == "code_only") {
                $display .= $nums[$code];
            } else {
                $display .= $nums[$code] . config('jjss.ninetypes')[$code];
            }
        }
        return $display;
    }

    public static function displayNineTypePoint(string $ninetype_code)
    {
        $nums = ['', '①', '②', '③', '④', '⑤', '⑥', '⑦', '⑧', '⑨',];
        $display = [];

        if (! $ninetype_code) return '';

        $ninetype_codes = explode(',', $ninetype_code);
        foreach ($ninetype_codes as $code) {
            $display[] = $nums[$code]  . config('jjss.ninetypes')[$code] . '／' . config('jjss.ninetype_points')[$code];
        }
        return implode("<br>", $display);
    }

    public static function displayTime(?string $time)
    {
        return substr($time, 0, 5);
    }

    public static function displayDate(?string $date)
    {
        if (! $date) return '';

        $carbon_date = Carbon::parse($date);
        return $carbon_date->format('Y/n/j');
    }

    public static function displayDateShort(?string $date)
    {
        if (! $date) return '';

        $carbon_date = Carbon::parse($date);
        return $carbon_date->format('n/j');
    }

    public static function printDate(?string $date, ?bool $week = false)
    {
        if (! $date) return '';

        $carbon_date = Carbon::parse($date);
        if ($week) {
            return $carbon_date->format('Y年n月j日').'('.self::displayWeek($date).')';
        } else {
            return $carbon_date->format('Y年n月j日');
        }
    }

    public static function printDateShort(?string $date, ?bool $week = false)
    {
        if (! $date) return '';

        $carbon_date = Carbon::parse($date);
        if ($week) {
            return $carbon_date->format('n月j日').'('.self::displayWeek($date).')';
        } else {
            return $carbon_date->format('n月j日');
        }
    }

    public static function printYearMonthOnly(?string $date)
    {
        if (! $date) return '';
        $carbon_date = Carbon::parse($date);

        return $carbon_date->format('Y年n月');
    }

    public static function printTimesByCourse(string $course_name)
    {
        $times = [
                                  'A3' => '週3回 2時間', 'A4' => '週4回 2時間', 'A5' => '週5回 2時間',
            'B2' => '週2回 3時間', 'B3' => '週3回 3時間', 'B4' => '週4回 3時間', 'B5' => '週5回 3時間',
            'C2' => '週2回 4時間', 'C3' => '週3回 4時間', 'C4' => '週4回 4時間', 'C5' => '週5回 4時間',
            'D2' => '週2回 5時間', 'D3' => '週3回 5時間', 'D4' => '週4回 5時間', 'D5' => '週5回 5時間',
        ];

        $time = '';

        if (isset($times[$course_name])) {
            $time = $times[$course_name];
        }

        return $time;
    }

    /**
     * Get course hours value
     *
     * @param  string $course_name
     * @return integer $hours
     */
    public static function getTimesByCourse(string $course_name)
    {
        $course_hours = [
            'A' => 2,
            'B' => 3,
            'C' => 4,
            'D' => 5,
        ];

        $course = substr($course_name, 0, 1);
        $hours = $course_hours[$course];

        return $hours;
    }

    /**
     * Get datetime parse to Carbon
     *
     * @param  string $datetime
     * @return Carbon OR null $result_datetime
     */
    public static function displayDateTime(string $datetime = null)
    {
        $temp_datetime = \DateTime::createFromFormat('Y-m-d H:i:s', $datetime);
        $result_datetime = null;
        if ($temp_datetime) {
            $result_datetime = Carbon::parse($temp_datetime->format('Y-m-d H:i:s'));
        }

        return $result_datetime;
    }

    // @return '日', '月', '火', '水', '木', '金', '土'
    public static function displayWeek(?string $date)
    {
        if (! $date) return '';

        $week = ['日', '月', '火', '水', '木', '金', '土'];
        $carbon_date = Carbon::parse($date);
        return $week[ $carbon_date->format('w') ];
    }

    // @return 'mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'
    public static function week(string $date)
    {
        $carbon_date = Carbon::parse($date);
        return strtolower( $carbon_date->format('D') );
    }

    public static function options(array $select_list, $value_key, $label_key, $selected_value = '')
    {
        $html_str = '';

        if(count($select_list) > 0) {
            foreach ($select_list as $option) {
                $select_str = '';
                if($selected_value !== '' && $option[$value_key] == $selected_value) {
                    $select_str = 'selected="selected"';
                }
                $html_str .= '<option value="' . $option[$value_key] . '" ' . $select_str . ' >';
                $html_str .= $option[$label_key];
                $html_str .= '</option>';
            }
        }

        return $html_str;
    }

    public static function displayAttendChangeType(string $change_type)
    {
        $display = '';

        if (strlen($change_type) == 0) {
            return $display;
        }

        $display .= config('jjss.attend_change_types')[$change_type];

        return $display;
    }

    public static function getSchoolIDbyName(string $school_name) {
        $school = School::where('name', $school_name)->first();
        return ($school) ? $school->id : NULL;
    }

    public static function getSchoolIDbyCode(string $school_code)
    {
        $school = School::where('school_code', $school_code)->first();
        return ($school) ? $school->id : NULL;
    }

    public static function getStudentIDbyCode(string $student_code)
    {
        $student = Student::where('student_code', $student_code)->first();
        return ($student) ? $student->id : NULL;
    }

    public static function getNewStudentCode($school_id, $first_shidou_date)
    {
        $school = School::find($school_id);
        $school_code = $school->school_code;

        $first_shidou_year = Carbon::parse($first_shidou_date)->format('y');
        $first_shidou_month = Carbon::parse($first_shidou_date)->month;
        $nendo_year = ($first_shidou_month >= 4) ? $first_shidou_year : $first_shidou_year - 1;

        $student_code_prefix = $school_code. '-'. $nendo_year. '-';
        $last_student_code = Student::where('student_code', 'LIKE', $student_code_prefix. '%')
                                    ->max('student_code');
        $last_student_code_suffix = substr($last_student_code, -3);

        $new_student_code_suffix = str_pad(((int) $last_student_code_suffix + 1), 3, '0', STR_PAD_LEFT);
        $new_student_code = $student_code_prefix. $new_student_code_suffix;

        return $new_student_code;
    }

    public static function getPermission()
    {
        $user = \Auth::user();
        return $user->teacher->permission;
    }

    /**
     * Get kamokus ids and convert into names
     *
     * @param  string $kamokus_ids
     * @return string $kamokus_names
     */
    public static function displayKamokusByName(string $kamokus_ids)
    {
        if ($kamokus_ids) {
            $kamokus = Kamoku::whereIn('id', explode(',', $kamokus_ids))
                ->get()
                ->pluck('name')
                ->toArray();
            $kamokus_names = implode(', ', $kamokus);

            return $kamokus_names;
        } else {
            return '';
        }
    }

    /**
     * Get course type
     *
     * @param  integer $course_id
     * @return string $course_type
     */
    public static function getCourseType(int $course_id)
    {
        $course_type = '';
        $course = Course::find($course_id);

        if ($course) {
            $course_type = $course->is_unlimited == 1 ? 'unlimited' : 'course';
        }

        return $course_type;
    }

    /**
     * Get total hours of course
     *
     * @param  integer $course_id
     * @return integer $total_hours
     */
    public static function getTotalHoursOfCourse($course_id)
    {
        if (!is_numeric($course_id)) {

            return $course_id;
        }

        $course_hours = [
            'A' => 2,
            'B' => 3,
            'C' => 4,
            'D' => 5,
        ];

        $course = Course::find($course_id)->toArray();
        $course_name = collect($course)->get('name');
        if($course_id == 17) {
            //コース無し
            $total_hours = 0;
        } else {
            //コースいろいろ
            $course_name_class = substr($course_name, 0, 1);
            $course_name_class_section = abs((int)filter_var($course_name, FILTER_SANITIZE_NUMBER_INT));
            $course_name_class_hour = $course_hours[$course_name_class];
            $total_hours = $course_name_class_section * $course_name_class_hour;
        }
        return $total_hours;
    }

    /**
     * Calculate unit price "calc_unit_price"
     *
     * @param  integer $student_id
     * @return numeric $total
     */
    public static function calculateUnitPrice(int $student_id = null)
    {
        $total = 1000;

        return $total;
    }

    /**
     * Get unit price
     *
     * @param  mixed $grade_code
     * @param  bool $is_enrolled
     * @param  bool $number_format
     * @return numeric $value
     */
    public static function getUnitPrice($grade_code, bool $is_enrolled, bool $number_format = true, $date = '')
    {
        $value = 0;

        //今日が3/31以前 && 契約開始日が 4/1 以降 → grade_code は 今＋1学年
        $today = Carbon::today();
        if(strlen($date) == 0){
            $date = $today;
        } else {
            $date = Carbon::parse($date);
        }
        if($today < $date) {
            //年度で見る
            $sub_year = ($date->subMonths(3)->year) - ($today->subMonths(3)->year);
            $grade_code = ($grade_code == "G" ? 13 : $grade_code);
            $grade_code = $grade_code + $sub_year;
            if($grade_code > 12) {
                $grade_code = 'G';
            } else if($grade_code < 7) {
                $grade_code = 7;
            }
        }

        $unitPrices = [
            'enrolled' => [
                '7' => '1900',
                '8' => '2000',
                '9' => '2200',
                '10' => '1900',
                '11' => '2000',
                '12' => '2200',
                'G' => '2200',
            ],
            'notEnrolled' => [
                '7' => '2100',
                '8' => '2200',
                '9' => '2400',
                '10' => '2100',
                '11' => '2200',
                '12' => '2400',
                'G' => '2400',
            ],
        ];

        if ($is_enrolled) {
            if (array_key_exists($grade_code, $unitPrices['enrolled'])) {
                if ($number_format) {
                    $value = number_format($unitPrices['enrolled'][$grade_code]);
                } else {
                    $value = $unitPrices['enrolled'][$grade_code];
                }
            }
        } elseif (!$is_enrolled) {
            if (array_key_exists($grade_code, $unitPrices['notEnrolled'])) {
                if ($number_format) {
                    $value = number_format($unitPrices['notEnrolled'][$grade_code]);
                } else {
                    $value = $unitPrices['notEnrolled'][$grade_code];
                }
            }
        }

        return $value;
    }

    // 設備費を取得
    public static function getMonthlySetsubiPrice()
    {
        return 2000;
    }

    // 月額金額を取得
    public static function getMonthlyPrice($course_id, $grade_code, $price_table, $unlimited_discount = 1)
    {
        if($course_id == 17) {
            return 0;
        }
        $price = Price::where('course_id', $course_id)
                      ->where('grade_code', $grade_code)
                      ->where('price_table', $price_table)
                      ->where('unlimited_discount', ($course_id == self::getUnlimitedCourse()) ? $unlimited_discount : 0)  // 無制限でない場合は0とする
                      ->firstOrFail();

        return $price->price;
    }

    public static function getMonthlyPriceByCurrentCourse($student_id, $current_course_id, $start_date)
    {
        $student = Student::findOrFail($student_id);
        $studentAttendBase = StudentAttendBase::where('student_id', $student_id)
            ->orderBy('id', 'desc')
            ->first();
        $price_table = null;
        if ($studentAttendBase) {
            // 東京校で旧月謝の設定かどうかを確認
            $price_table = $studentAttendBase->price_table_override;
        }
        if (! $price_table) {
            $school = School::findOrFail($student->school_id);
            $price_table = $school->price_table;
        }

        //学年
        $grade_code = $student->grade_code;
        //今日が3/31以前 && 契約開始日が 4/1 以降 → grade_code は 今＋1学年
        $today = Carbon::today();
        $start_date = Carbon::parse($start_date);
        if($today < $start_date) {
            //年度で見る
            $sub_year = ($start_date->subMonths(3)->year) - ($today->subMonths(3)->year);
            $grade_code = ($grade_code == "G" ? 13 : $grade_code);
            $grade_code = $grade_code + $sub_year;
            if($grade_code > 12) {
                $grade_code = 'G';
            } else if($grade_code < 7) {
                $grade_code = 7;
            }
        }

        return self::getMonthlyPrice($current_course_id, $grade_code, $price_table);
    }

    public static function getMonthlyPriceByNewCourse($student_id, $course_id, $henkou_yotei_date)
    {
        $student = Student::findOrFail($student_id);
        $school = School::findOrFail($student->school_id);

        //学年
        $grade_code = $student->grade_code;
        //今日が3/31以前 && 契約開始日が 4/1 以降 → grade_code は 今＋1学年
        $today = Carbon::today();
        $henkou_yotei_date = Carbon::parse($henkou_yotei_date);
        if($today < $henkou_yotei_date) {
            //年度で見る
            $sub_year = ($henkou_yotei_date->subMonths(3)->year) - ($today->subMonths(3)->year);
            $grade_code = ($grade_code == "G" ? 13 : $grade_code);
            $grade_code = $grade_code + $sub_year;
            if($grade_code > 12) {
                $grade_code = 'G';
            } else if($grade_code < 7) {
                $grade_code = 7;
            }
        }

        return self::getMonthlyPrice($course_id, $grade_code, $school->price_table);
    }

    //対象生徒の期間内の通塾予定日の配列を返す
    public static function getVisitDates($student_id = '', $term_id = '')
    {
        $visit_dates = [];

        if(strlen($student_id) == 0 || strlen($term_id) == 0) {
            return $visit_dates;
        }

        $term = Term::find($term_id);

        //特別講習の期間なら来塾日は決まっている
        if($term->term_type == "特別講習") {
            //student_attend_specials から算出
            $attend_specials = StudentAttendSpecial::where("student_id", "=", $student_id)
                                            ->where("attend_date", ">=", $term->start_date)
                                            ->where("attend_date", "<=", $term->end_date)
                                            ->orderBy("attend_date", "asc")
                                            ->get();
            if(count($attend_specials) > 0) {
                foreach($attend_specials as $attend_special) {
                    $visit_dates[] = $attend_special->attend_date;
                }
            }
        }

        if($term->term_type == "通常") {
            //曜日 - 休塾日 から算出 (曜日：student_attend_bases, 休塾日：土日 ＋ annual_schedules data_type=holiday )

            //開始日から終了日までの塾の予定日を配列で取得
            $yasumi_dates = [];//土日以外の休みの日
            $work_dates = [];//通常休みだけど開塾している日

            $annual_dates = AnnualSchedule::where('schedule_date', '>=', $term->start_date)
                ->where('schedule_date', '<=', $term->end_date)
                ->whereIn('date_type', array('holiday', 'workday'))
                ->orderBy('schedule_date', 'asc')
                ->get();
            if (count($annual_dates) > 0) {
                foreach ($annual_dates as $annual_date) {
                    if($annual_date->date_type == "holiday") {
                        $yasumi_dates[] = $annual_date->schedule_date;
                    } else if($annual_date->date_type == "workday") {
                        $work_dates[] = $annual_date->schedule_date;
                    }
                }
            }

            $term_start_date = $term->start_date;
            $term_end_date = $term->end_date;
            $student_keiyakus = StudentAttendBase::where("student_id", "=", $student_id)
                                        ->where(function($q) use ($term_start_date, $term_end_date){
                                            $q->where(function($_q) use ($term_start_date, $term_end_date) {
                                                $_q->where("start_date", "<=", $term_end_date)
                                                    ->whereNull("end_date");
                                            })
                                            ->orWhere(function($_q) use ($term_start_date, $term_end_date) {
                                                $_q->where("end_date", ">=", $term_start_date)
                                                    ->where("start_date", "<=", $term_end_date);
                                            });
                                        })
                                        ->orderBy("start_date", "asc")
                                        ->get();
            if(count($student_keiyakus) > 0) {
                foreach($student_keiyakus as $student_keiyaku) {

                    //生徒の通塾曜日
                    $student_youbis = [];
                    $weeks = config('jjss.weeks_alpha');
                    foreach ($weeks as $wk => $wv) {
                        if (strlen($student_keiyaku->{$wv . "_start_time"}) > 0) {
                            $student_youbis[] = $wk;
                        }
                    }

                    // 開始日から終了日までをぐるぐるまわしながら曜日で日付を取得していく(お休みの日の配列と重複したら取得せずスキップ）
                    $term_start_date = Carbon::parse($term->start_date);
                    $term_end_date = Carbon::parse($term->end_date);
                    $keiyaku_start_date = Carbon::parse($student_keiyaku->start_date);
                    $keiyaku_end_date = '';
                    if(strlen($student_keiyaku->end_date) > 0) {
                        $keiyaku_end_date = Carbon::parse($student_keiyaku->end_date);
                    }


                    $_date = ($keiyaku_start_date < $term_start_date) ? $term_start_date : $keiyaku_start_date;
                    $target_end_date = (strlen($keiyaku_end_date) == 0) ? $term_end_date : (($keiyaku_end_date < $term_end_date) ? $keiyaku_end_date : $term_end_date);
                    while ($_date <= $target_end_date) {
                        if (in_array($_date->format('Y-m-d'), $yasumi_dates) === false) {
                            //通塾曜日かどうか
                            if (in_array($_date->dayOfWeek, $student_youbis)) {
                                $visit_dates[] = $_date->format('Y-m-d');
                            }
                        }
                        $_date = $_date->addDay();
                    }
                }
            }
        }
        //追加コマ・振替
        $tsuika_furikae_dates = StudentAttendChange::where('student_id', '=', $student_id)
                                                    ->where("attend_date", ">=", $term->start_date)
                                                    ->where("attend_date", "<=", $term->end_date)
                                                    ->orderBy("attend_date", "asc")
                                                    ->get();
        if(count($tsuika_furikae_dates) > 0) {
            foreach($tsuika_furikae_dates as $tsuika_furikae_date) {
//TODO::振替前の日はどうする？（時間単位で振替すると、振替前の日も来塾日になるので削除しないほうがよさそう）
                $visit_dates[] = $tsuika_furikae_date->attend_date;
            }
        }

        //重複削除
        $visit_dates = array_unique($visit_dates);
        //並べ替え
        sort($visit_dates);

        //退塾日・卒塾日・休塾日対応
        $taijyuku_date = '';
        $sotsujyuku_date = '';
        $student = Student::find($student_id);
        if($student) {
            if(strlen($student->taijyuku_date) > 0) {
                $taijyuku_date = $student->taijyuku_date;
            }
            if(strlen($student->sotsujyuku_date) > 0) {
                $sotsujyuku_date = $student->sotsujyuku_date;
            }
        }

        $kyuujyuku_dates = StudentKyuujyuku::where('student_id', '=', $student_id)->get();


        $_visit_dates = $visit_dates;
        $visit_dates = [];
        if(count($_visit_dates) > 0) {
            foreach($_visit_dates as $_visit_date) {
                //退塾日以降ならスキップ
                if(strlen($taijyuku_date) > 0) {
                    if($_visit_date >= $taijyuku_date) {
                        continue;
                    }
                }
                //卒塾日以降ならスキップ
                if(strlen($sotsujyuku_date) > 0) {
                    if($_visit_date >= $sotsujyuku_date) {
                        continue;
                    }
                }
                //休塾期間ならスキップ
                if(count($kyuujyuku_dates) > 0) {
                    foreach($kyuujyuku_dates as $kyuujyuku_date) {
                        if($_visit_date >= $kyuujyuku_date->start_date && $_visit_date <= $kyuujyuku_date->end_date){
                            continue 2;
                        }
                    }
                }

                $visit_dates[] = $_visit_date;
            }
        }

        return $visit_dates;
    }

    //対象生徒の期間内の通塾予定曜日の配列を返す
    public static function getVisitWeekDays($student_id = '', $term_id = '')
    {
        $visit_dates = [];

        if(strlen($student_id) == 0 || strlen($term_id) == 0) {
            return $visit_dates;
        }

        $term = Term::find($term_id);

        //特別講習の期間なら来塾日は決まっている
        if($term->term_type == "特別講習") {
            //student_attend_specials から算出
            $attend_specials = StudentAttendSpecial::where("student_id", "=", $student_id)
                ->where("attend_date", ">=", $term->start_date)
                ->where("attend_date", "<=", $term->end_date)
                ->orderBy("attend_date", "asc")
                ->get();
            if(count($attend_specials) > 0) {
                foreach($attend_specials as $attend_special) {
                    $visit_dates[] = Carbon::parse($attend_special->attend_date)->dayOfWeek;
                }
            }
        }

        if($term->term_type == "通常") {
            //曜日 - 休塾日 から算出 (曜日：student_attend_bases, 休塾日：土日 ＋ annual_schedules data_type=holiday )

            $term_start_date = $term->start_date;
            $term_end_date = $term->end_date;
            $student_keiyakus = StudentAttendBase::where("student_id", "=", $student_id)
                ->where(function($q) use ($term_start_date, $term_end_date){
                    $q->where(function($_q) use ($term_start_date, $term_end_date) {
                        $_q->where("start_date", "<=", $term_end_date)
                            ->whereNull("end_date");
                    })
                    ->orWhere(function($_q) use ($term_start_date, $term_end_date) {
                        $_q->where("end_date", ">=", $term_start_date)
                            ->where("start_date", "<=", $term_end_date);
                    });
                })
                ->orderBy("start_date", "asc")
                ->get();

            if(count($student_keiyakus) > 0) {
                foreach($student_keiyakus as $student_keiyaku) {

                    //生徒の通塾曜日
                    $weeks = config('jjss.weeks_alpha');
                    foreach ($weeks as $wk => $wv) {
                        if (strlen($student_keiyaku->{$wv . "_start_time"}) > 0) {
                            $visit_dates[] = $wk;
                        }
                    }
                }
            }
        }

        return array_unique($visit_dates);
    }


    //対象生徒の期間内の通塾日の配列を返す
    public static function getVisitedDates($student_id = '', $term_id = '')
    {
        $visited_dates = [];

        if(strlen($student_id) == 0 || strlen($term_id) == 0) {
            return $visited_dates;
        }

        $term = Term::find($term_id);

        $student_notes = StudentNote::where("student_id", "=", $student_id)
                                    ->where("note_date", ">=", $term->start_date)
                                    ->where("note_date", "<=", $term->end_date)
                                    ->where("checked_flag", "=", "1")
                                    ->whereNotNull("actual_attend_start_time")
                                    ->orderBy("note_date", "asc")
                                    ->get();
        if(count($student_notes) > 0) {
            foreach($student_notes as $student_note) {
                $visited_dates[$student_note->note_date] = $student_note->note_date;
            }
        }

        return $visited_dates;
    }

    /**
     * Get unlimited course id
     *
     * @return string $course_id
     */
    public static function getUnlimitedCourse()
    {
        $course_id = 16;

        return strval($course_id);
    }

    public static function getNoneCourse()
    {
        $course_id = 17;

        return strval($course_id);
    }

    //指定の日(target_date)が休塾期間中、退塾日後、卒塾日後の生徒を取得
    public static function getLeaves($school_id = '', $target_date = '')
    {
        $list = [];//student_idの配列
        if (strlen($school_id) == 0 || strlen($target_date) == 0) {
            return $list;
        }

        //卒塾・退塾
        $query = Student::where('school_id', '=', $school_id)
            ->where(function ($query) use ($target_date) {
                $query->orWhere('taijyuku_date', '<', $target_date)
                    ->orWhere('sotsujyuku_date', '<', $target_date);
            });
        $leave_students = $query->get();
        if ($leave_students !== null) {
            if (count($leave_students) > 0) {
                foreach($leave_students as $leave_student) {
                    $list[] = $leave_student->id;
                }
            }
        }

        //休塾
        $query = StudentKyuujyuku::where('start_date', '<=', $target_date)
            ->Where('end_date', '>=', $target_date)
            ->join('students', function($join) use ($school_id){
                $join->on('students.id', '=', 'student_kyuujyukus.student_id');
                if ($school_id) {
                    $join->where('students.school_id', '=', $school_id);
                }
            });
        $leaving_students = $query->get();
        if ($leaving_students !== null) {
            if (count($leaving_students) > 0) {
                foreach($leaving_students as $leaving_student) {
                    $list[] = $leaving_student->student_id;
                }
            }
        }

        return array_unique($list);
    }

}
