<?php

namespace App\Library;

use App\Models\School;
use App\Models\Student;
use App\Models\StudentAttendBase;
use App\Models\StudentAttendSpecial;
use App\Models\StudentAttendChange;
use App\Models\StudentMonthly;
use App\Models\StudentMonthlyTextUnit;
use App\Models\StudentNote;
use App\Models\StudentNoteRow;
use App\Models\Term;
use App\Models\Text;
use App\Models\TextUnit;
use Carbon\Carbon;
use DB;

class AutomateStudentNote
{
    const ENGLISH_WORD_KAMOKU_ID = 7;  // 英単語特訓="7"
    const ENGLISH_WORD_CT_KAMOKU_ID = 8;  // 英単語CT="8"

    /**
     * 全校舎全生徒の子別ノートを生成する
     * （現在使用しないようにしている）
     */
    public static function createStudentNoteTodayAll(Carbon $target_date)
    {
        $student_ids = self::getComingsAllStudentIDs($target_date);

        $count = 0;
        foreach ($student_ids as $student_id) {
            $student_note = self::getTodayStudentNote($student_id, $target_date);
            if ($student_note) continue;

            $result = self::createStudentNote($student_id, $target_date);
            $count++;
        }
        return $count;
    }

    protected static function getComingsAllStudentIDs(Carbon $target_date)
    {
        $student_ids = [];

        $school_ids = School::get()->pluck('id')->toArray();
        foreach ($school_ids as $school_id) {
            // 後日ソースコードを整理のこと
            // 通常,特別講習,休日
            $date_type = \JJSS::getTypeOfDate($school_id, $target_date);
            // 休日の場合はエラー表示
            if ($date_type == \JJSS::DATE_TYPE['holiday']) {
                return [];
            }

            $comings = self::getComings($school_id, $target_date, $date_type);

            //指定の日(target_date)が休塾期間中、退塾日後、卒塾日後の生徒を取得
            $leave_students = \JJSS::getLeaves($school_id, $target_date);

            foreach ($comings as $student_id => $coming) {
                //休塾中・卒塾日後・退塾日後の生徒は含めない
                if(in_array($student_id, $leave_students)) {
                    continue;
                }
                $student_ids[] = $student_id;
            }
        }

        return $student_ids;
    }

    private static function getComings(int $school_id, Carbon $target_date, $date_type)
    {
        if ($date_type == \JJSS::DATE_TYPE['basic']) {
            $comings = self::getComingsOnBasic($school_id, $target_date);
        } else {  // if ($date_type == \JJSS::DATE_TYPE['special']) {
            $comings = self::getComingsOnSpecial($school_id, $target_date);
        }

        // 追加コマ・振替のチェック
        $changes = StudentAttendChange::where('attend_date', $target_date)
            ->join('students', function ($join) use ($school_id) {
                $join->on('students.id', '=', 'student_attend_changes.student_id')
                    ->where('students.school_id', '=', $school_id);
            })
            ->get()->toArray();
        foreach ($changes as $change) {
            $comings[$change['student_id']] = [
                'student' => [
                    'id' => $change['student_id'],
                    'name' => $change['name'],
                ],
                'note_id' => '',
                'plan_attend_start_time' => $change['start_time'],
                'plan_attend_end_time' => $change['end_time'],
                'actual_attend_start_time' => '',
                'actual_attend_end_time' => '',
            ];
        }

        // 子別ノート存在チェック,student_id取得 ... 上書きする
        $student_notes = StudentNote::select(['*', 'student_notes.id AS note_id'])
            ->where('note_date', $target_date)
            ->join('students', function ($join) use ($school_id) {
                $join->on('students.id', '=', 'student_notes.student_id')
                    ->where('students.school_id', '=', $school_id);
            })
            ->get()->toArray();
        foreach ($student_notes as $note) {
            // 優先的に上書きする
            $comings[$note['student_id']] = [
                'student' => [
                    'id' => $note['student_id'],
                    'name' => $note['name'],
                ],
                'note_id' => $note['note_id'],
                'plan_attend_start_time' => $note['plan_attend_start_time'],
                'plan_attend_end_time' => $note['plan_attend_end_time'],
                'actual_attend_start_time' => $note['actual_attend_start_time'],
                'actual_attend_end_time' => $note['actual_attend_end_time'],
            ];
        }

        // 振替前の日付になる場合は除外
        $change_befores = StudentAttendChange::where('before_date', $target_date)
            ->get()->toArray();
        foreach ($change_befores as $change_before) {
            unset($comings[$change_before['student_id']]);
        }

        return $comings;
    }

    private static function getComingsOnBasic(int $school_id, Carbon $target_date)
    {
        // 曜日に基づくカラムを指定できるようにする
        $week = \JJSS::week($target_date);
        $week_start_time_column = $week . '_start_time';
        $week_end_time_column = $week . '_end_time';

        $attends = StudentAttendBase::where('start_date', '<=', $target_date)
            ->where(function ($q) use ($target_date) {
                $q->whereNull('end_date')
                    ->orWhere('end_date', '>=', $target_date);
            })
            ->join('students', function ($join) use ($school_id) {
                $join->on('students.id', '=', 'student_attend_bases.student_id')
                    ->where('students.school_id', '=', $school_id);
            })
            ->whereNotNull($week_start_time_column)
            ->get()->toArray();

        //指定の日(target_date)が休塾期間中、退塾日後、卒塾日後の生徒を取得
        $leave_students = \JJSS::getLeaves($school_id, $target_date);

        $comings = [];
        foreach ($attends as $attend) {
            //休塾中・卒塾日後・退塾日後の生徒は含めない
            if(in_array($attend['student_id'], $leave_students)) {
                continue;
            }
            $comings[$attend['student_id']] = [
                'student' => [
                    'id' => $attend['student_id'],
                    'name' => $attend['name'],
                ],
                'note_id' => '',
                'plan_attend_start_time' => $attend[$week_start_time_column],
                'plan_attend_end_time' => $attend[$week_end_time_column],
                'actual_attend_start_time' => '',
                'actual_attend_end_time' => '',
            ];
        }

        return $comings;
    }

    private static function getComingsOnSpecial(int $school_id, Carbon $target_date)
    {
        $attends = StudentAttendSpecial::where('attend_date', $target_date)
            ->join('students', function ($join) use ($school_id) {
                $join->on('students.id', '=', 'student_attend_specials.student_id')
                    ->where('students.school_id', '=', $school_id);
            })
            ->get()->toArray();

        //指定の日(target_date)が休塾期間中、退塾日後、卒塾日後の生徒を取得
        $leave_students = \JJSS::getLeaves($school_id, $target_date);

        $comings = [];
        foreach ($attends as $attend) {
            //休塾中・卒塾日後・退塾日後の生徒は含めない
            if(in_array($attend['student_id'], $leave_students)) {
                continue;
            }
            $comings[$attend['student_id']] = [
                'student' => [
                    'id' => $attend['student_id'],
                    'name' => $attend['name'],
                ],
                'note_id' => '',
                'plan_attend_start_time' => $attend['start_time'],
                'plan_attend_end_time' => $attend['end_time'],
                'actual_attend_start_time' => '',
                'actual_attend_end_time' => '',
            ];
        }

        return $comings;
    }

    public static function createStudentNote(int $student_id, Carbon $target_date)
    {
        $student = Student::find($student_id);
        //指定の日(target_date)が休塾期間中、退塾日後、卒塾日後の生徒を取得
        $leave_students = \JJSS::getLeaves($student->school_id, $target_date);
        if(in_array($student_id, $leave_students) === true) {
            // 追加コマがない場合は子別ノート生成を停止
            $change = StudentAttendChange
                ::where('attend_date', $target_date)
                ->where('student_id', $student_id)
                ->first();
            if (! $change) {
                return false;
            }
        }

        $last_student_note = self::getLastStudentNote($student_id, $target_date);
        $student_monthly = self::getStudentMonthly($student_id, $target_date);
        if (empty($student_monthly)) {
            // 日常の個別ノート行の引き継ぎ
            $rows = self::getStudentNoteRowsFromLastStudentNote($last_student_note, $target_date);
        } else {
            // 月間計画・特別講習期間計画を元にして、個別ノート行を取得する
            $rows = self::getStudentNoteRowsFromStudentMonthly($student_monthly, $last_student_note, $target_date);
        }

        $plan_attend = self::getPlanAttendTime($student_id, $target_date);
        DB::beginTransaction();
        try {
            // 子別ノート 生成
            $note = new StudentNote;
            $note->student_id = $student_id;
            $note->note_date = $target_date;
            $note->checked_flag = false;
            $note->plan_attend_start_time = $plan_attend['start_time'];
            $note->plan_attend_end_time = $plan_attend['end_time'];
            $note->remarks = '';
            $note->save();

            // 子別ノート行 生成
            foreach ($rows as $r) {
                $row = new StudentNoteRow;
                $row->student_note_id = $note->id;
                $row->text_id = $r->text_id;
                $row->additional_text_name = $r->additional_text_name;
                $row->reverse_test = $r->reverse_test;
                $row->text_unit_id = $r->text_unit_id;
                $row->display_text_unit = $r->display_text_unit;
                $row->save();
            }

            DB::commit();
            return true;

        } catch (\PDOException $e) {
            DB::rollBack();
            return false;
        }
    }

    private static function getStudentMonthly(int $student_id, Carbon $target_date)
    {
        // 対象日付に該当する期間を取得
        $term = Term::where('start_date', '<=', $target_date)
                    ->where('end_date', '>=', $target_date)
                    ->first();
        if (! $term) return null;

        // 対象生徒・対象期間に該当する月間計画・特別講習期間計画を取得
        $student_monthly = StudentMonthly::where('student_id', $student_id)
                                         ->where('term_id', $term->id)
                                         ->first();
        return $student_monthly;
    }

    private static function removeStudentNote(int $student_id, Carbon $target_date)
    {
        $student_note = self::getTodayStudentNote($student_id, $target_date);

        DB::beginTransaction();
        try {
            // 削除
            foreach($student_note->student_note_rows as $student_note_row) {
                $student_note_row->delete();
            }
            $student_note->delete();

            DB::commit();
            return true;

        } catch (\PDOException $e) {
            DB::rollBack();
            return false;
        }
    }

    public static function recreateStudentNote(int $student_id, Carbon $target_date)
    {
        return self::removeStudentNote($student_id, $target_date) && self::createStudentNote($student_id, $target_date);
    }

    public static function getStudentNoteRowsFromLastStudentNote($last_student_note, Carbon $target_date)
    {
        $rows = [];

        // 前回の子別ノート行がなければ空を返す
        if (!isset($last_student_note->student_note_rows)) {
            return $rows;
        }

        // 初期化
        $old_rows = $last_student_note->student_note_rows;
        // 前回通塾からの経過日数取得
        $from_date = Carbon::parse($last_student_note->note_date);
        $past_days = $from_date->diffInDays($target_date);

        // A. 英単語特訓について処理
        $english_word_kamoku_rows = self::getEnglishWordKamokuStudentNoteRows($old_rows, $past_days);

        // B. 通常科目について処理
        $standard_rows = self::getStandardStudentNoteRows($old_rows);

        // C. 追加登録・過去問について処理
        $original_rows = self::getOriginalStudentNoteRows($old_rows);

        // A〜Cデータを科目ID-テキストID-回数順でソート
        $rows = self::sortStudentNoteRows($english_word_kamoku_rows, $standard_rows, $original_rows);

        #dd($rows);
        return $rows;
    }

    private static function getEnglishWordKamokuStudentNoteRows($old_rows, $past_days)
    {
        $rows = [];
        $adds = 0;

        // 最も学習が進んでいる回は？
        $last_row_text_id = 0;
        $last_row_text_unit_id = 0;
        $last_row = null;

        foreach ($old_rows as $r) {
            // 英単語特訓のみを処理対象とする
            if (isset($r->text) && $r->text->kamoku_id == self::ENGLISH_WORD_KAMOKU_ID) {
                if ($last_row_text_id < $r->text_id) {
                    // テキストが切り替わったとき
                    $last_row_text_id = $r->text_id;
                    $last_row_text_unit_id = $r->text_unit_id;
                    $last_row = $r;
                } elseif ($last_row_text_id == $r->text_id && $last_row_text_unit_id < $r->text_unit_id) {
                    // テキストの中で学習が進んでいるとき
                    $last_row_text_unit_id = $r->text_unit_id;
                    $last_row = $r;
                }

                // 合否が 合=1でない ( 否=0,S(指導のみ)=2,'(空)'(手付かず)='' ) 場合は残す
                if ($r->result_flag != 1) {
                    $rows[] = self::getRemainStudentNoteRow($r);
                    $adds++;  // ノート行の追加分を記録
                    if ($past_days <= $adds) break;  // 経過日数以上になったらそこまで
                }
            }
        }
        #dd($rows, $last_row_text_id, $last_row_text_unit_id, $last_row);

        // 1件でも処理していたら ＆ すでに経過日数分以上でなければ(経過日数分未満なら)
        if ($last_row && $past_days > $adds) {
            $next_row = $last_row;
            $repeat = $past_days - $adds;
            for ($i = 1; $i <= $repeat; $i++) {
                // 英単語特訓が完了のときは関連CTの初回指導を追加
                if (self::isFinalTextUnitInText($next_row->text_id, $next_row->text_unit_id)) {
                    $rows[] = self::getEnglishCTStudentNoteRow($next_row->text_id);
                }

                // 次の子別ノート行を取得
                $next_row = self::getNextStudentNoteRow($next_row);
                if (! empty($next_row)) {
                    $rows[] = $next_row;
                    $adds++;  // ノート行の追加分を記録
                    if ($past_days <= $adds) break;  // 経過日数以上になったらそこまで
                }
            }
        }

        #dd($rows);
        return $rows;
    }

    private static function getEnglishCTStudentNoteRow($text_id)
    {
        // TODO: textsテーブルで定義すべき
        $relates = [
            28 => 35,  // データベース1700特訓 -> データベース1700CT
            29 => 36,  // データベース3000特訓 -> データベース3000CT
            30 => 37,  // データベース4500特訓 -> データベース4500CT
            31 => 38,  // データベース5500特訓 -> データベース5500CT
            32 => 39,  // ターゲット1900特訓 -> ターゲット1900CT
            33 => 40,  // ターゲット1000特訓 -> ターゲット1000CT
            34 => 41,  // システム英単語特訓 -> システム英単語CT
        ];

        $row = new StudentNoteRow;
        $row->text_id = $relates[$text_id];
        $row->additional_text_name = null;
        $row->reverse_test = 0;
        $row->text_unit_id = 0;
        $row->display_text_unit = '初回指導';

        return $row;
    }

    private static function getStandardStudentNoteRows($old_rows)
    {
        $rows = [];

        // テキストID別 最も学習が進んでいる回は？
        $last_row_text_unit_ids = [];
        $last_rows = [];
        $passes = [];

        foreach ($old_rows as $r) {
            // 英単語特訓は無視
            if (isset($r->text) && $r->text->kamoku_id == self::ENGLISH_WORD_KAMOKU_ID) continue;
            // 追加登録・過去問は無視
            if ($r->text_id == 0) continue;

            $text_id = $r->text_id;

            if (!isset($last_row_text_unit_ids[$text_id]) || $last_row_text_unit_ids[$text_id] < $r->text_unit_id) {
                // テキストの中で学習が進んでいるとき
                $last_row_text_unit_ids[$text_id] = $r->text_unit_id;
                $last_rows[$text_id] = $r;
            }

            // 合否が 合=1でない ( 否=0,S(指導のみ)=2,'(空)'(手付かず)='' ) 場合は残す
            if ($r->result_flag != 1) {
                $rows[] = self::getRemainStudentNoteRow($r);
            } else {
                // 合格の場合
                $passes[$text_id] = (isset($passes[$text_id]) ? $passes[$text_id] : 0) + 1;
            }
        }

        // 1件でも処理していたら
        if (count($last_rows)) {
            foreach ($last_rows as $text_id => $last_row) {
                $is_final = self::isFinalTextUnitInText($last_row->text_id, $last_row->text_unit_id);
                // 合格 かつテキスト完了 かつ逆テストである
                if ($last_row->result_flag == 1 && $is_final && $last_row->reverse_test) {
                    // ならこれはここで終わり
                    continue;
                }
                // 合格 かつテキスト完了 かつ逆テストあり かつ逆テストでない
                elseif ($last_row->result_flag == 1 && $is_final && $last_row->text->has_reverse_test && !$last_row->reverse_test) {
                    // 逆テストの1回目を持ってくる
                    $first_text_unit = TextUnit::where([
                        'text_id' => $last_row->text_id,
                        'unit_num' => '1',
                    ])->first();
                    $row = clone $last_row;
                    $row->reverse_test = 1;
                    $row->text_unit_id = (isset($first_text_unit)) ? $first_text_unit->id : 1;
                    $row->display_text_unit = '1';
                    $rows[] = $row;
                }

                $next_row = $last_row;
                $repeats = isset($passes[$text_id]) ? $passes[$text_id] : 0;
                for ($i = 1; $i <= $repeats; $i++) {
                    // 次の子別ノート行を取得
                    $next_row = self::getNextStudentNoteRow($next_row);
                    if (! empty($next_row)) {
                        $rows[] = $next_row;
                    }
                }
            }
        }

        #dd($rows);
        return $rows;
    }

    private static function getOriginalStudentNoteRows($old_rows)
    {
        $rows = [];

        foreach ($old_rows as $r) {
            // ここでは通常テキストは処理対象外
            if ($r->text_id != 0) continue;
            // 合格したものは次のテキスト取得を試す（回数が半角数字のものは次の回をとる）
            if ($r->result_flag === 1 || $r->result_flag === true) {
                $next_row = self::getNextStudentNoteRow($r);
                if ($next_row) {
                    $rows[] = $next_row;
                }
            } else {
                // 合否が 合=1でない ( 否=0,S(指導のみ)=2,'(空)'(手付かず)='' ) 場合は残す
                $rows[] = self::getRemainStudentNoteRow($r);
            }
        }

        return $rows;
    }

    private static function sortStudentNoteRows($A_rows, $B_rows, $C_rows)
    {
        // merge
        $rows = array_merge($A_rows, $B_rows);

        // sort
        uasort($rows, function($a, $b){
            if ($a->text->kamoku->sort == $b->text->kamoku->sort) {
                if ($a->text->id == $b->text->id) {
                    if ($a->reverse_test == $b->reverse_test) {
                        return ($a->text_unit_id < $b->text_unit_id) ? -1 : 1;
                    } else {
                        if ($a->reverse_test < $b->reverse_test) {
                            return -1;
                        } else {
                            return ($a->text_unit_id < $b->text_unit_id) ? -1 : 1;
                        }
                    }
                }
                return ($a->text->id < $b->text->id) ? -1 : 1;
            }
            return ($a->text->kamoku->sort < $b->text->kamoku->sort) ? -1 : 1;
        });

        // 配列キーの振り直し
        $rows = array_values($rows);

        // DEBUG
        #foreach($rows as $r) {
        #    echo $r->text->kamoku->sort .":". $r->text_id . ":" . $r->reverse_test .":" . $r->display_text_unit. ' ';
        #    var_dump($r->text_unit_id);
        #    echo '<br>';
        #}
        #dd();

        // 追加登録…過去問は最後に
        foreach ($C_rows as $r) {
            $rows[] = $r;
        }

        return $rows;
    }

    private static function getStudentNoteRowsFromStudentMonthly($student_monthly, $last_student_note, Carbon $target_date)
    {
        // 初期化
        if ($last_student_note) {
            $old_rows = $last_student_note->student_note_rows;
            // 前回通塾からの経過日数取得
            $from_date = Carbon::parse($last_student_note->note_date);
            $past_days = $from_date->diffInDays($target_date);
        } else {
            // 子別ノートがなくても月間計画から子別ノート生成を行う
            $old_rows = [];
            $past_days = 1;
        }

        // A. 英単語特訓について処理 (前回子別ノート参照)
        // 現状、英単語特訓については、前回子別ノートを参照して処理するものとする。
        // 月間計画上で「学習しない」などが設定されていても無視
        // 学習全回数を踏まえて終わりになる
        $english_word_kamoku_rows = self::getEnglishWordKamokuStudentNoteRowsFromStudentMonthly($student_monthly, $old_rows, $past_days);

        // B. 通常科目について処理
        $standard_rows = self::getStandardStudentNoteRowsFromStudentMonthly($student_monthly, $last_student_note, $target_date);

        // C. 追加登録・過去問について処理 (前回子別ノート参照)
        $original_rows = self::getOriginalStudentNoteRows($old_rows);
#dd($english_word_kamoku_rows, $standard_rows, $original_rows);

        // A〜Cデータを科目ID-テキストID-回数順でソート
        $rows = self::sortStudentNoteRows($english_word_kamoku_rows, $standard_rows, $original_rows);

        #dd($rows);
        return $rows;
    }

    private static function getEnglishWordKamokuStudentNoteRowsFromStudentMonthly($student_monthly, $old_rows, $past_days)
    {
        $rows = [];
        $adds = 0;

        // 最も学習が進んでいる回は？
        $last_row_text_id = 0;
        $last_row_text_unit_id = 0;
        $last_row = null;

        foreach ($old_rows as $r) {
            // 英単語特訓のみを処理対象とする
            if (isset($r->text) && $r->text->kamoku_id == self::ENGLISH_WORD_KAMOKU_ID) {
                if ($last_row_text_id < $r->text_id) {
                    // テキストが切り替わったとき
                    $last_row_text_id = $r->text_id;
                    $last_row_text_unit_id = $r->text_unit_id;
                    $last_row = $r;
                } elseif ($last_row_text_id == $r->text_id && $last_row_text_unit_id < $r->text_unit_id) {
                    // テキストの中で学習が進んでいるとき
                    $last_row_text_unit_id = $r->text_unit_id;
                    $last_row = $r;
                }

                // 合否が 合=1でない ( 否=0,S(指導のみ)=2,'(空)'(手付かず)='' ) 場合は残す
                if ($r->result_flag != 1) {
                    $rows[] = self::getRemainStudentNoteRow($r);
                    $adds++;  // ノート行の追加分を記録
                    if ($past_days <= $adds) break;  // 経過日数以上になったらそこまで
                }
            }
        }
        #dd($rows, $last_row_text_id, $last_row_text_unit_id, $last_row);

        // 関連する月間計画テキストを取得
        $smts = $student_monthly->student_monthly_texts;

        // 1件でも処理していたら ＆ すでに経過日数分以上でなければ(経過日数分未満なら)
        if ($last_row && $past_days > $adds) {
            $next_row = $last_row;
            $repeat = $past_days - $adds;

            // 月間計画の対象テキストがあれば取得
            $smt = self::getTargetStudentMonthlyText($smts, $last_row->text_id);

            for ($i = 1; $i <= $repeat; $i++) {
                // 英単語特訓が完了のときは関連CTの初回指導を追加
                if (self::isFinalTextUnitInText($next_row->text_id, $next_row->text_unit_id, $smt)) {
                    $rows[] = self::getEnglishCTStudentNoteRow($next_row->text_id);
                }

                // 次の子別ノート行を取得
                $next_row = self::getNextStudentNoteRow($next_row, $smt);
                if (! empty($next_row)) {
                    // 月間計画に対象テキストの計画が含まれれば…
                    if ($smt) {
                        // 現在の回数は、text_all_kaisuuを超えたか？超えれば終了
                        if ($next_row->display_text_unit > $smt->text_all_kaisuu) break;
                    }

                    $rows[] = $next_row;
                    $adds++;  // ノート行の追加分を記録
                    if ($past_days <= $adds) break;  // 経過日数以上になったらそこまで
                }
            }
        }

        return $rows;
    }

    private static function getTargetStudentMonthlyText($student_monthly_texts, $text_id)
    {
        foreach ($student_monthly_texts as $smt) {
            if ($smt->text_id == $text_id) return $smt;
        }
        return null;
    }

    private static function getStandardStudentNoteRowsFromStudentMonthly($student_monthly, $last_student_note, $target_date)
    {
        $rows = [];

        // 今日の曜日を取得
        $week_num = $target_date->dayOfWeek;
        // 今日は来塾日？振替してる？
        $change_attend_date = self::getChangeAttendDate($student_monthly->student_id, $target_date);
        if ($change_attend_date) {
            // 振替ある場合は元の曜日で上書き
            $furikae_date = Carbon::parse($change_attend_date->before_date);
            $week_num = $furikae_date->dayOfWeek;
        }

        // 月間計画に登録されているテキストごとに処理
        $smts = $student_monthly->student_monthly_texts;
        foreach ($smts as $smt) {
            // 英単語特訓は無視
            if ($smt->kamoku_id == self::ENGLISH_WORD_KAMOKU_ID) continue;
            // 追加登録・過去問は無視
            if ($smt->text_id == 0) continue;

            // 今日の曜日から学習回数を取得。0なら今日は学習しない科目として飛ばす
            $kaisuu = self::getMaikaiKaisuu($smt, $week_num);
            if ($kaisuu == 0) continue;

            // 件数
            $i = 0;

            // 子別ノート中の対象テキストの最新の授業情報
            $last_note_rows = self::getLastStudentNoteRowsByTargetTextID($student_monthly->student_id, $smt);
            if ($last_note_rows !== null) {
                // 学習記録があるのでその回数・状態を元に処理
                foreach ($last_note_rows as $r) {
                    // 合否が 合=1なら ( 否=0,S(指導のみ)=2,'(空)'(手付かず)='' ) 飛ばす
                    if ($r->result_flag == 1) continue;

                    // これは「学習しない」になっていないか
                    $smtu = StudentMonthlyTextUnit::where('student_monthly_id', $student_monthly->id)
                                                  ->where('text_id', $r->text_id)
                                                  ->where('text_unit_num', self::getRawNumber($r->display_text_unit))
                                                  ->first();
                    if ($smtu && $smtu->learning_flag == 1) continue;  // 「学習しない」ものは飛ばす

                    $rows[] = self::getRemainStudentNoteRow($r);
                    $i++;
                    if ($i >= $kaisuu) break;  // 学習回数分の追加が完了
                }
                if ($i >= $kaisuu) continue;  // 学習回数分の追加が完了…次のテキストへ

                // まだ1日の学習回数分に達していないので、このあとの処理で月間計画から子別ノート行を取得
            } else {
                // このテキストの学習履歴なし。
                // このテキストでの学習は今月から始まるのか、そして学習を始めてよいのかをチェック
                if (self::isStartThisTextThisMonth($smt)) {
                    // 英単語CTなら処理せず次のテキストへ(英単語特訓の終了で生成されるため)
                    if ($smt->text->kamoku_id == self::ENGLISH_WORD_CT_KAMOKU_ID) continue;
                    // 学習を始められないなら次のテキストへ
                    if (! self::canStartThisText($smt, $student_monthly->student_id)) continue;
                }
            }

            // 学習記録がない、よって月間計画の指定を元に授業情報取得
            $smtus = $smt->student_monthly_text_units;
            foreach ($smtus as $smtu) {
                if ($smtu->learning_flag == 1) continue;  // 「学習しない」ものは飛ばす

                // すでに「再」で含まれていないか
                $existInRows = function($rows, $smtu) {
                    foreach($rows as $row) {
                        // text_id一致、回数も一致(「再」などを外して)
                        if (
                            $row->text_id == $smtu->text_id &&
                            self::getRawNumber($row->display_text_unit) == $smtu->text_unit_name
                        ) {
                            return true;
                        }
                    }
                    return false;
                };
                if ($existInRows($rows, $smtu)) continue;  //

                // すでに学習済みのものは飛ばす
                $isStudied = function($smtu) {
                    $student_id = $smtu->student_monthly_text->student_monthly->student_id;
                    $histories = StudentNoteRow::select()
                                             ->with([
                                                 'student_note'
                                             ])
                                             ->whereHas('student_note', function($q) use ($student_id){
                                                $q->where('student_id', $student_id)
                                                  ->where('checked_flag', true)
                                                  ->whereNotNull('actual_attend_start_time');
                                             })
                                             ->where('text_id', $smtu->text_id)
                                             ->where('reverse_test', $smtu->student_monthly_text->reverse_test)
                                             ->where('result_flag', 1)
                                             ->get();
                    foreach ($histories as $h) {
                        $text_unit_name = ($smtu->text_unit_name == '初回') ? '初回指導' : $smtu->text_unit_name;
                        if (self::getRawNumber($h->display_text_unit) == $text_unit_name) return true;
                    }
                    return false;
                };
                if ($isStudied($smtu)) continue;

                // student_monthly_text_units -> student_note_rowsへ変換
                if ($smtu->text_unit_num == 0) {
                    // 初回指導
                    $text_unit_id = 0;
                    $display_text_unit = '初回指導';
                } else {
                    $text_unit = TextUnit::where('text_id', $smtu->text_id)
                                         ->where('unit_num', $smtu->text_unit_num)
                                         ->first();
                    $text_unit_id = $text_unit->id;
                    $display_text_unit = $text_unit->name;
                }
                $row = new StudentNoteRow;
                $row->text_id = $smt->text_id;
                $row->additional_text_name = null;
                $row->reverse_test = $smt->reverse_test;
                $row->text_unit_id = $text_unit_id;
                $row->display_text_unit = $display_text_unit;

                $rows[] = $row;
                $i++;
                if ($i >= $kaisuu) break;  // 学習回数分の追加が完了
            }
        }

#dd($rows);
        return $rows;
    }

    // このテキストはこの期間で開始予定か？
    private static function isStartThisTextThisMonth($student_monthly_text)
    {
        foreach ($student_monthly_text->student_monthly_text_units as $smtu) {
            // 初回または1回目があるなら
            if ($smtu->text_unit_num == 0 || $smtu->text_unit_num == 1) return true;
        }
        return false;
    }

    private static function canStartThisText($student_monthly_text, $student_id)
    {
        // このテキストは通常テスト(=0)か、逆テスト(=1)か？
        if ($student_monthly_text->reverse_test == 0) {
            // 通常のテキストなら、texts.next_text_idを参考にして最終回の合格があるかチェック… 合格ないなら次のテキストへ
            $before_text = Text::where('next_text_id', $student_monthly_text->text_id)
                               ->first();
            // そもそも前のテキストがないなら学習開始OK
            if (! $before_text) return true;

            $last_text_id = $before_text->id;  // 前のテキストのID
            $reverse_test_flag = $before_text->has_reverse_test;  // 前のテキストの逆テスト有無を考慮

        } else {
            // 逆テストならその順テストの中で最終回の合格があるかチェック
            $last_text_id = $student_monthly_text->text_id;  // このテキストのID
            $reverse_test_flag = 0;  // 順テスト

        }

        // 直前または同テキストでの最終回が学習完了しているかチェック
        $last_text_unit = TextUnit::where('text_id', $last_text_id)
                                  ->orderBy('unit_num', 'desc')
                                  ->first();
        $row = StudentNoteRow::select()
                             ->with([
                                 'student_note',
                             ])
                             ->whereHas('student_note', function($q) use ($student_id){
                                 $q->where('student_id', $student_id);
                             })
                             ->where('reverse_test', $reverse_test_flag)  // 順テスト・逆テストを考慮して
                             ->where('text_unit_id', $last_text_unit->id)  // 最終回が
                             ->where('result_flag', 1)  // 合格
                             ->first();
        // 前のテキストでの最終回学習が完了していないため学習開始NG
        if (! $row) return false;
        return true;  // 学習開始OK!
    }

    private static function getMaikaiKaisuu($student_monthly_text, $week_num)
    {
        // 毎回の学習回数を取得。
        $maikai_kaisuu = $student_monthly_text->maikai_kaisuu;
        if ($maikai_kaisuu) return $maikai_kaisuu;

        // 特別講習期間は土(=6),日(=0)稼働の場合もあり。その場合は「1回」を返す
        if ($week_num == 0 || $week_num == 6) return 1;

        // 毎回回数がセットされていなかったので曜日別の学習回数を取得。ここでnullなら0回扱い。
        $maikai_kaisuu_key = 'maikai_kaisuu_'. $week_num;
        $maikai_kaisuu = (int) $student_monthly_text->$maikai_kaisuu_key;

        return $maikai_kaisuu;
    }

    private static function getLastStudentNoteRowsByTargetTextID($student_id, $student_monthly_text)
    {
        $text_id = $student_monthly_text->text_id;
        $reverse_test = $student_monthly_text->reverse_test;
        // 対象テキストIDで子別ノート行が最後にあった日を特定
        $student_note = StudentNote::select()
            ->with([
                'student_note_rows',
            ])
            ->whereHas('student_note_rows', function($q) use ($text_id, $reverse_test){
                $q->where('text_id',  $text_id)
                  ->where('reverse_test', $reverse_test);
            })
            ->where('student_id', $student_id)
            ->where('checked_flag', true)
            //->whereNotNull('actual_attend_start_time')
            ->orderBy('note_date', 'desc')
            ->first();

        // 対象テキストIDでの授業はこれまでに行われていない
        if (! $student_note) return null;

        // 対象テキストの授業情報を取得
        $rows = [];
        foreach($student_note->student_note_rows as $row) {
            if ($row->text_id != $text_id) continue;
            $rows[] = $row;
        }

        return $rows;
    }


    private static function isFinalTextUnitInText($text_id, $text_unit_id, $student_monthly_text = null)
    {
        $text = Text::find($text_id);
        if ($text->units_by_student == 1 && $text_unit_id == 199) {
            // テキスト別生徒別のテキストで回数199になった場合は終了とする
            return true;
        } elseif ($text->units_by_student == 1) {
            // 月間計画が指定されていれば、終了回数を参照して確認
            if ($student_monthly_text && $text_unit_id >= $student_monthly_text->text_all_kaisuu) {
                // 終了回数に達しているので、終了とする
                return true;
            }
            return false;
        }

        $final_text_unit = TextUnit::where([
            'text_id' => $text_id,
        ])->orderBy('unit_num', 'desc')->first();

        if ($final_text_unit->id == $text_unit_id) {
            // 最終のtext_units.idと回数IDが一致する場合は終了
            return true;
        }

        return false;
    }

    private static function getNextStudentNoteRow($last_row, $student_monthly_text = null)
    {
        $row = null;

        if (! $last_row) {
            return $row;
        }

        // これはどんな子別ノート行？
        $text = Text::find($last_row->text_id);
        if (isset($text) && $text->kamoku_id == self::ENGLISH_WORD_KAMOKU_ID) {
            // 英単語特訓の子別ノート行
            if (!self::isFinalTextUnitInText($last_row->text_id, $last_row->text_unit_id, $student_monthly_text)) {
                // このテキストの最終回ではない
                $row = clone $last_row;
                $row->text_unit_id = $row->text_unit_id + 1;
                $row->display_text_unit = (string)$row->text_unit_id;
            } else {
                // このテキストの最終回だった
                // 次のテキストはある？
                $next_text = self::getNextTextByLastTextID($last_row->text_id);
                if ($next_text) {
                    // ある
                    $row = new StudentNoteRow;
                    $row->text_id = $next_text->id;
                    $row->additional_text_name = null;
                    $row->reverse_test = 0;
                    $row->text_unit_id = 0;
                    $row->display_text_unit = 'カウント';
                } else {
                    // ない
                    $row = null;
                }
            }
        } elseif ($last_row->text_id == 0) {
            // 追加登録・過去問の子別ノート行
            $text_unit_number = self::getRawNumber($last_row->display_text_unit);
            if (ctype_digit($text_unit_number)) {
                $row = clone $last_row;
                $row->display_text_unit = (string) ((int)$text_unit_number + 1);
            }
        } else {  //
            // 通常科目の子別ノート行
            if (!self::isFinalTextUnitInText($last_row->text_id, $last_row->text_unit_id, $student_monthly_text)) {
                // このテキストの最終回ではない
                if ($last_row->text_unit_id == 0) {
                    // 直前のものが'初回指導'
                    $next_text_unit = TextUnit::where([
                        'text_id' => $last_row->text_id,
                        'unit_num' => '1',
                    ])->first();
                } else {
                    // 1回目以降
                    $current_text_unit = TextUnit::find($last_row->text_unit_id);
                    if ($current_text_unit) {
                        $next_text_unit = TextUnit::where([
                            'text_id' => $last_row->text_id,
                            'unit_num' => (string) ($current_text_unit->unit_num + 1),
                        ])->first();
                    } else {
                        $next_text_unit = null;
                    }
                }
                $row = clone $last_row;
                $row->text_unit_id = ($next_text_unit) ? $next_text_unit->id : $row->text_unit_id + 1;
                $row->display_text_unit = ($next_text_unit) ? (string)$next_text_unit->name : (string)$row->text_unit_id;

            } else {
                // 次のテキストはある？
                $next_text = self::getNextTextByLastTextID($last_row->text_id);
                if ($next_text) {
                    // ある
                    $row = new StudentNoteRow;
                    $row->text_id = $next_text->id;
                    $row->additional_text_name = null;
                    $row->reverse_test = 0;
                    $row->text_unit_id = 0;
                    $row->display_text_unit = '初回指導';
                } else {
                    // ない
                    $row = null;
                }
            }
        }

        return $row;
    }

    public static function getLastStudentNote(int $student_id, Carbon $target_date)
    {
        return StudentNote
            ::where('student_id', $student_id)
            ->where('note_date', '<', $target_date)
            ->where('checked_flag', '=', true)
            ->whereNotNull('actual_attend_start_time')
            ->orderBy('note_date', 'desc')
            ->first();
    }

    public static function getTodayStudentNote(int $student_id, Carbon $target_date)
    {
        return StudentNote
            ::where('student_id', $student_id)
            ->where('note_date', $target_date)
            ->first();
    }

    public static function getPlanAttendTime(int $student_id, Carbon $target_date)
    {
        // 今日の子別ノートがあるならこれを取得
        $student_note = self::getTodayStudentNote($student_id, $target_date);
        if ($student_note) {
            return [
                'start_time' => $student_note->plan_attend_start_time,
                'end_time' => $student_note->plan_attend_end_time,
            ];
        }

        // 振替・追加コマか
        $change = StudentAttendChange
            ::where('attend_date', $target_date)
            ->where('student_id', $student_id)
            ->first();
        if ($change) {
            return [
                'start_time' => $change->start_time,
                'end_time' => $change->end_time,
            ];
        }

        // 通常日程か特別期間か
        $school_id = Student::find($student_id)->school_id;
        $date_type = \JJSS::getTypeOfDate($school_id, $target_date);
        // 休日の場合はエラー表示
        if ($date_type == \JJSS::DATE_TYPE['basic']) {
            // 曜日に基づくカラムを指定できるようにする
            $week = \JJSS::week($target_date);
            $week_start_time_column = $week . '_start_time';
            $week_end_time_column = $week . '_end_time';

            $attend = StudentAttendBase::where('start_date', '<=', $target_date)
                ->where(function ($q) use ($target_date) {
                    $q->whereNull('end_date')
                        ->orWhere('end_date', '>=', $target_date);
                })
                ->where('student_id', $student_id)
                ->first();
            return [
                'start_time' => $attend->$week_start_time_column,
                'end_time' => $attend->$week_end_time_column,
            ];
        } elseif ($date_type == \JJSS::DATE_TYPE['special']) {
            $attend = StudentAttendSpecial::where('attend_date', $target_date)
                ->where('student_id', $student_id)
                ->first();
            return [
                'start_time' => $attend->start_time,
                'end_time' => $attend->end_time,
            ];
        }
    }

    protected static function getRemainStudentNoteRow($r)
    {
        // 合否が 否=0 の場合、「再」を付加
        if ($r->result_flag === 0 || $r->result_flag === false) {
            $r->display_text_unit = self::getRetryNumber($r->display_text_unit);
        }
        return $r;
    }

    protected static function getRawNumber(string $display_text_unit)
    {
        if (strpos($display_text_unit, 'カウント') === 0) return 0;  // 英単語特訓用の「カウント」は0を返す
        if (strpos($display_text_unit, '初回指導') === 0) return 0;  // 通常テキスト用の「初回指導」は0を返す
        $temp = explode('再', $display_text_unit);  // 「再」を考慮し、その前部分のみを返す
        return $temp[0];
    }
    protected static function getRetryNumber(string $display_text_unit)
    {
        $temp = explode('再', $display_text_unit);
        if (count($temp) == 1) {
            return $temp[0]. '再';
        } else {
            if ($temp[1] == '') {
                return $temp[0]. '再2';
            } else {
                return $temp[0]. '再'. ((int)$temp[1] + 1);
            }
        }
    }

    protected static function getNextTextByLastTextID($last_text_id)
    {
        $last_text = Text::find($last_text_id);
        if ($last_text->next_text_id) {
            return Text::find($last_text->next_text_id);
        } else {
            return null;
        }
    }

    public static function hasChangeBeforeDate(int $student_id, Carbon $target_date)
    {
        $change_befores = StudentAttendChange
            ::where('student_id', $student_id)
            ->where('before_date', $target_date)
            ->first();
        return (boolean) $change_befores;
    }

    private static function getChangeAttendDate(int $student_id, Carbon $target_date)
    {
        $change_after = StudentAttendChange
            ::where('student_id', $student_id)
            ->where('attend_date', $target_date)
            ->first();
        return $change_after;
    }
}
