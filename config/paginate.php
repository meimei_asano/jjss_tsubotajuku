<?php

return [

    // items per page
    'items' => [
        'student' => 30,
        'school' => 30,
        'teacher' => 30,
        'text' => 30,
        'term' => 30,
        'note_changing' => 30,

        'student_attend_change_tsuika' => 30,
        'student_attend_change_special_tsuika' => 30,
        'student_change_course' => 30,
        'student_sold_text' => 30,
        'student_request' => 30,

        'mendan' => 30,

        'invoice' => 50,
        'plan_monthly' => 50,
    ],

];
