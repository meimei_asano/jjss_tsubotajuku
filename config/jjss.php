<?php

return [

    // 学年
    'grade' => [
        '7' => '中1',
        '8' => '中2',
        '9' => '中3',
        '10' => '高1',
        '11' => '高2',
        '12' => '高3',
        'G' => '既卒生', // graduated
    ],

    // 雇用形態
    'koyou_keitai' => [
        '正社員' => '正社員',
        'アルバイト' => 'アルバイト',
    ],

    // 権限
    'permission' => [
        '校長' => '校長',
        '講師' => '講師',
        'マネージャー' => 'マネージャー',
        '本部スタッフ' => '本部スタッフ',
        '管理者' => '管理者',
    ],

    // 9タイプ
    'ninetypes' => [
        '1' => '完全主義者',
        '2' => '献身家',
        '3' => '達成者',
        '4' => '芸術家',
        '5' => '研究者',
        '6' => '堅実家',
        '7' => '楽天家',
        '8' => '統率者',
        '9' => '調停者',
    ],

    // ９タイプのコツ
    'ninetype_points' => [
        '1' => '次の行動の手順を明確に伝える',
        '2' => '会話の中に”ありがとう”をできるだけ挟む',
        '3' => '今やっていることがいかに挑戦しがいのあることかを伝える。',
        '4' => '特別感を強調する。「普通は話さないんだけど、君には〜」',
        '5' => '物事は論理的に伝える。ストレートではなく、さりげなく褒めると良い。',
        '6' => '"一緒に"〜しよう／過去の成功例の話など',
        '7' => '今やっていることが君にとってどう"オイシい"のかを伝える。',
        '8' => '指示や説得ではなく、相談やお願いをする。味方であることを強調する。',
        '9' => '目標を”一緒に”立てて、それを二人の約束とする。'
    ],

    //曜日
    'weeks' => [
        '0' => '日',
        '1' => '月',
        '2' => '火',
        '3' => '水',
        '4' => '木',
        '5' => '金',
        '6' => '土',
    ],

    'weeks_alpha' => [
        '1' => 'mon',
        '2' => 'tue',
        '3' => 'wed',
        '4' => 'thu',
        '5' => 'fri',
    ],

    //合格、不合格
    'learning_result' => [
        '0' => '否',
        '1' => '合',
        '2' => 'S',
    ],

    //追加・振替コマ
    'attend_change_types' => [
      'tsuika' => '追加コマ',
      'furikae' => '振替',
    ],

    //性別
    'gender' => [
        '男' => '男',
        '女' => '女',
    ],

    //生徒情報csv export項目
    'student_export_columns' => [
        '生徒コード' => 'student_code',
        '名前' => 'name',
        'カナ' => 'kana',
        '所属校舎' => 'school->name',
        '担当講師' => 'teacher->name',
        '学校' => 'gakkou_name',
        '学年' => 'grade.grade_code',//★要：加工
        'コース' => 'courses_today.course.name',
        '通塾予定（月）' => 'courses_today.mon_start_time',//★要：加工
        '通塾予定（火）' => 'courses_today.tue_start_time',//★要：加工
        '通塾予定（水）' => 'courses_today.wed_start_time',//★要：加工
        '通塾予定（木）' => 'courses_today.thu_start_time',//★要：加工
        '通塾予定（金）' => 'courses_today.fri_start_time',//★要：加工
        '学習科目' => 'kamokus',//★要：加工
        '９タイプ' => 'ninetypes',//★要：加工
        '入塾年月日' => 'nyujyuku_date',
        '退塾年月日' => 'taijyuku_date',
        '卒塾年月日' => 'sotsujyuku_date',
        '生年月日' => 'birth_date',
        '郵便番号' => 'zip_code',
        '都道府県' => 'pref',
        '住所１' => 'address1',
        '住所２' => 'address2',
        '自宅電話番号' => 'home_tel',
        '本人電話番号' => 'student_tel',
        '保護者指名' => 'hogosya_name',
        '保護者カナ' => 'hogosya_kana',
        '保護者携帯' => 'hogosya_tel',
        '保護者メールアドレス' => 'hogosya_email',
        //'志望校',
        //'志望学部',
        '引落申込み用紙受取日' => 'hikiotoshi_uketori_date',
        '引落開始日' => 'hikiotoshi_start_date',
        '銀行コード' => 'bank_code',
        '銀行名' => 'bank_name',
        '支店コード' => 'branch_code',
        '支店名' => 'branch_name',
        '預金種目' => 'deposit_type',
        '通帳番号' => 'account_number',
        '名義人名' => 'account_holder',
        '顧客番号' => 'customer_number',
        '委託者番号' => 'itaku_number',

    ],

    // student_request type
    'request_type' => [
        '休塾' => '休塾',
        '卒塾' => '卒塾',
        '退塾' => '退塾',
    ],

    //消費税率
    'tax_rate' => 0.10,

    //預金種目
    'deposit_types' => [
        1 => '普通',
        2 => '当座',
        5 => '貯蓄',
    ],
];
